#ifndef track_h
#define track_h

#include "parallel_tracking.hh"
#include "emitt_data.h"

class BEAM;
class BEAMLINE;
class BIN;
class ELEMENT;

// what follows was in placeti3.cc

void bunch_track_0(BEAMLINE *beamline,BEAM *beam,int start,int stop);
void bunch_track_0_noacc(BEAMLINE *beamline,BEAM *beam,int start,int stop);
void bunch_track(BEAMLINE *beamline,BEAM *beam,int start,int stop);
void bunch_track_gradient_0(BEAMLINE *beamline,BEAM *beam,int start,int stop, double gradient);
void bunch_track_emitt_start();
void bunch_track_emitt_end(BEAM *bunch, double s);
void bunch_track_emitt(BEAMLINE *beamline,BEAM *beam,int start,int stop);
void bunch_track_test(BEAMLINE *beamline,BEAM *beam,int start,int stop);


// what follows was in placeti4.cc

void bunch_track_line_emitt_new(BEAMLINE *beamline,BEAM *beam, int start=-1, int end=-1);
void bunch_track_line_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch);

// new functions for MPI tracking

extern void bunch_track_emitt_parallel(ParallelTracking &parallel_tracking, BEAMLINE *beamline,BEAM *beam,
				       int start,int stop);

#endif /* track_h */
