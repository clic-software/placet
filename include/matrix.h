#ifndef matrix_h
#define matrix_h

#include "rmatrix.h"

/** methods for R_MATRIX objects */

/// multiply matrix a with size na*ma with matrix b with size nb*mb into matrix c 
void matrix_mult(double a[],int na,int ma,double b[],int nb,int mb,double c[]);

/** method that calculates r1*r2 and stores in r3 (using temporary variables to allow r3 to be one of the arguments) */
static inline void mult_M_M(R_MATRIX *r1,R_MATRIX *r2,R_MATRIX *r3)
{
  double tmp11=r1->r11*r2->r11+r1->r12*r2->r21;
  double tmp12=r1->r11*r2->r12+r1->r12*r2->r22;
  double tmp21=r1->r21*r2->r11+r1->r22*r2->r21;
  r3->r22=r1->r21*r2->r12+r1->r22*r2->r22;
  r3->r11=tmp11;
  r3->r12=tmp12;
  r3->r21=tmp21;
}

void M_mult_4(R_MATRIX *r_xx,R_MATRIX *r_xy,R_MATRIX *r_yx,R_MATRIX *r_yy,
              R_MATRIX *sigma,R_MATRIX *sigma_xy,R_MATRIX *sigma_xx);

/// copy matrix (array) a with size n into matrix (array) b
void matrix_copy(double a[],double b[],int n);

#endif /* matrix_h */
