#ifndef _h_short_range
#define _h_short_range

#include <valarray>
#include <utility>
#include <string>

#include "spline.h"

class BEAM;

/**
 * @brief short range wakefield definition
 * 
 * two use cases both with tk_ShortRangeWake
 *  
 * - wakes_prepare in wakes.cc for usage in cavities
 * this precalculates the right values for the bunch structure (nr of slices)
 *
 * - apply_wakefield_spline in general ELEMENT
 * more generic, does not assume anything on BEAM profile
 *
 * @author Daniel Schulte <Daniel.Schulte@cern.ch>
 */

class SHORT_RANGE {
  /// spline for each direction
  SPLINE *spline_x,*spline_y,*spline_z;
  /** spline representation:
   * 0: kick is calculated by adding up small kicks
   * for a tilted bunch this is an approximation. Spline in units [V/mm/pC] vs [m]
   * 1: full bunch representation. Spline in units [V/mm/pC] vs [m]
   * 2: convolved wakes
   */
  int type;
  /// length of bunch length in splines (used when type is == 0)
  double spline_bunch_length;

  /// private default constructor (not implemented)
  SHORT_RANGE();
  /// private copy constructor (not implemented)
  SHORT_RANGE(const SHORT_RANGE&);
  /// private assign operator (not implemented)
  SHORT_RANGE& operator=(const SHORT_RANGE&);

 public:
  SHORT_RANGE(SPLINE *sx,SPLINE *sy,SPLINE *sz,int fb=1,double sbl=0.0);
  ~SHORT_RANGE();
  double wake_x(double dist);
  double wake_y(double dist);
  double wake_z(double dist);

  /** spline that corresponds to the wakefield [V / mm / pC] vs [m], 
      which are typical Gdfidl units
  */ 
  /// apply short range wakefield
  // better in BEAM class? - JS
  void apply_wakefield_spline(BEAM* beam, double length )const;

  /// return the z slicing and the convolved wakes Wx/y/z in GeV. It assumes that the particles in the bunch are sorted by z
  std::pair<std::valarray<double>, std::valarray<double> > wake_x_convolve(BEAM *bunch, const std::vector<int> &index, double length, size_t Nslices = 64 ) const;
  std::pair<std::valarray<double>, std::valarray<double> > wake_y_convolve(BEAM *bunch, const std::vector<int> &index, double length, size_t Nslices = 64 ) const;
  std::pair<std::valarray<double>, std::valarray<double> > wake_z_convolve(BEAM *bunch, const std::vector<int> &index, double length, size_t Nslices = 64 ) const;

};

int ShortRange_Init(Tcl_Interp *interp);
SHORT_RANGE* get_short_range(const char * short_range_name);

#endif
