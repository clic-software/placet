import re

class function():
    '''
     Class which holds a C function declaration..
    '''
    def __init__(self,name,_type,full_string):
        self.name=name
        self._type=_type
        self.in_args=[]
        self.out_args=[]
        self.declaration=full_string
    def __repr__(self):
        return "<"+self._type+":"+self.name+">"
    def add_input_argument(self,argument):
        '''
        Add an input argument to the list
        '''
        self.in_args.append(argument)
    def add_output_argument(self,argument):
        '''
        Add an output argument to the list
        '''
        self.out_args.append(argument)
    def call_string(self):
        '''
        Returns the string to call this function
        '''
        call=self.name+'('
        for a in self.in_args:
            call+=a.name+','
        call.strip(',')
        call+=')'
        return call

class argument():
    '''
    Class which holds a C argument
    '''
    def __init__(self,name,_type,ispointer):
        self.name=name
        self._type=_type
        self.is_pointer=ispointer
    def __repr__(self):
        return "<"+self._type+":"+self.name+">"

def append_args(func,args):
    '''
    Append arguments to the function
    '''
    for arg in args.split(','):
        asp=arg.split()

        name=asp[-1] # name..
        type_end=-1
        if '&' in name:
            name=name.strip('&')
            ispointer=True
        elif '&' == asp[-2]:
            type_end=-2
            ispointer=True
        else:
            ispointer=False
        t=''
        for i in asp[:type_end]:
            t+=' '+i
        t=t.strip()
        if name[:3]=='in_':
            func.add_input_argument(argument(name,t,ispointer))
        elif name[:4]=='out_':
            func.add_output_argument(argument(name,t,ispointer))
        else:
            raise ValueError("Cannot determine argument type: "+name)

def add_function(command,functions):
    '''
    Add a function to the list of functions based
    on the command (declaration)
    '''
    name=command.split('(')[0].split()[2] # function name..
    t=command.split('(')[0].split()[1] # function type..
    fcall=command.split('extern')[1].split(t)[1].strip()
    functions.append(function(name,t,fcall))

    args=command.split('(')[1].split(')')[0]
    append_args(functions[-1],args)

def create_function(function,fout):
    '''
    Create a new function in fout
    '''
    fout.write('extern '+function._type+' '+function.declaration+'\n')
    fout.write('{\n')

    if len(function.in_args)>0:
        fout.write('FIFO')
        for in_arg in function.in_args:
            fout.write(' << '+in_arg.name)
        fout.write(';\n')

    fout.write(function.call_string()+'\n')

    if len(function.out_args)>0:
        fout.write('FIFO')
        for out_arg in function.out_args:
            fout.write(' >> '+out_arg.name)
        fout.write(';\n')

    fout.write('};\n\n')

def main():
    fin=file('placet_api.hh')
    functions=[]

    content=''
    for l in fin:
        if len(l.strip())==0 or l.strip()[0]=='#':
            continue
        content+=' '.join(l.split())


    for cmd in content.split(';'):
        if re.search('^extern',cmd): # this is a function..
            add_function(cmd,functions)

    fout=file('placet_swig_interface.cc','w')
    for f in functions:
        create_function(f,fout)

if __name__=="__main__":
    main()

