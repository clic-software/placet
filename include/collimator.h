#ifndef collimator_h
#define collimator_h

#include "element.h"
#include <fstream>
#include <string>

class collimatortable;
class SPLINE;

class COLLIMATOR : public ELEMENT {

  // members that can be set with attributes
  bool correct_offset;
  int nmax;
  double dz,dzi,zmin,zmax;
  double *wtx,*wty,*rhox,*rhoy;
  SPLINE *spx,*spy;

  /// initial height in [m]
  double in_height;
  /// final height in [m]
  double fin_height;
  /// width in [m]
  double width;
  /// length of taper [m]
  double taper_length;
  /// length of flat part [m]
  double flat_length;
  /// Electrical conductivity of the collimator material [1/Ohm/m]
  double sigma;
  /// Relaxation time of the collimator material [s]
  double tau;
  /// bunch charge [e]
  double Nb;
 
  /// use of giovanni method (if true) or daniel method (if false)
  bool giovanni;
  /// vertical collimator (if true) or horizontal collimator (if false)
  bool vertical;

  /// use wake lookup table for resistive regime (if true)
  bool wake_meth;
  /// filename of wake lookup table
  std::string wake_tname;
  /// collimator lookup table
  collimatortable *Ctable;

  /// output file stream for giovanni and adina implementation
  std::ofstream output_file;

  void step_in(BEAM *beam ) { if (correct_offset) ELEMENT::step_in(beam); }
  void step_out(BEAM *beam ) { if (correct_offset) ELEMENT::step_out(beam); }

  // useful constants calculated from properties, for giovanni implementation
  /// problem scale of initial height
  double s0_in;
  /// problem scale of initial height
  double s0_fin;
  /// for pole finding (only for wake_meth false)
  double cap_gam;
  /// resistive depth
  double lambda;
  /// tapering angle
  double angle;
  /// number of steps for the taper
  int imax;

  // calculation methods
  /// resisitive intermediate regime calculation
  void resistive_intermediate(int slice, double zp, double diffz, double blength, double* Np, double* deltayarray,double &kickyrwt_a,double &kickyrwf_a,double &kickyrwt2_a,double &kickyrwf2_a,double &kickyrwt_b,double &kickyrwf_b,double &kickyrwt2_b,double &kickyrwf2_b)const;
  /// resisitive long range regime calculation
  void resistive_longrange(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray, double &kickyrwt,double &kickyrwf)const;
  /// resisitive short range regime calculation
  void resistive_shortrange(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray, double &kickyrwt,double &kickyrwf)const;
  /// resisitive table calculation
  void resistive_table(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray, double &kickyrwt,double &kickyrwf)const;

  /// geometric inductive regime calculation
  double geometric_inductive(double yp,double Np,double deltay,double diffz,double gam)const;
  /// geometric intermediate regime calculation
  double geometric_intermediate(double yp,double Np,double deltay,double diffz,double gam,double sz0)const;
  /// geometric diffractive regime calculation
  double geometric_diffractive(double yp,double Np,double deltay,double diffz,double gam,double sz0)const;

public:
  
  COLLIMATOR(int &argc, char **argv );

  //  COLLIMATOR(double bb, double gg, double ww, double LT, double Lflat, double sig_ch, double tau_ch, double _charge, int corr=1, bool vert=true, int wake_method=0, const char* output_filename=NULL);
  //  COLLIMATOR(SPLINE*);
  //  COLLIMATOR(SPLINE*,SPLINE*);
  ~COLLIMATOR();
  void giovanni_step_4d_0(BEAM*);
  void giovanni_step(BEAM*);
  void daniel_step_4d_0(BEAM*);
  void daniel_step(BEAM*);
  void step_4d_0(BEAM *beam ) { if (giovanni) giovanni_step_4d_0(beam); else daniel_step_4d_0(beam); }
  void step_4d(BEAM *beam ) { if (giovanni) giovanni_step(beam); else daniel_step(beam); }
  //virtual int list(Tcl_Interp *interp);
  //virtual void list(FILE *file);
  
};

int tk_Collimator(ClientData clientdata,Tcl_Interp *interp,int argc,
		  char *argv[]);

#endif /* collimator_h */
