#ifndef manual_h
#define manual_h

#include <map>
#include <tcl.h>

namespace MANUAL {

  enum CATEGORY {
    BEAMLINE,
    ELEMENT,
    BEAM,
    TRACKING,
    TOOLS,
    MISC,
    INVISIBLE
  };

  enum SUBCATEGORY {
    CREATE,
    INSPECT,
    MODIFY,
    NONE
  };

  typedef std::pair<int,int> CATEGORY_KEY;

  extern std::map<CATEGORY_KEY, const char *> section_titles;
  
}

extern int tk_Manual(ClientData clientdata, Tcl_Interp *interp, int argc, char *argv[]);

#endif /* manual_h */
