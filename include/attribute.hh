#ifndef attribute_hh
#define attribute_hh

/* This class allows to store "Attributes" of different types in one common type */

#include <ostream>

#include "option_info.hh"
#include "matrixnd.hh"
#include "stream.hh"

class Attribute {
  option_type _type;
  union {
    int _int; // 32 bits
    size_t _uint; // 32 bits
    bool _bool; // whatever bits it is
    double _double; // whatever bits it is
  };
  // non-POD types
  MatrixNd _matrix;
  std::string _string;
  std::complex<double> _complex;

public:
  Attribute(int v=0 ) : _type(OPT_INT), _int(v) {}
  Attribute(bool v ) : _type(OPT_BOOL), _bool(v) {}
  Attribute(size_t v ) : _type(OPT_UINT), _uint(v) {}
  Attribute(double v ) : _type(OPT_DOUBLE), _double(v) {}
  Attribute(MatrixNd v) : _type(OPT_MATRIX), _matrix(v) {}
  Attribute(const char *s ) : _type(OPT_STRING), _string(s) {}
  Attribute(std::string v ) : _type(OPT_STRING), _string(v) {}
  Attribute(std::complex<double> v ) : _type(OPT_COMPLEX), _complex(v) {}

  const option_type &type() const { return _type; }

  std::string get_type_name() const
  {
    switch(_type) {
    case OPT_INT:      return "integer"; 
    case OPT_BOOL:     return "boolean";
    case OPT_UINT:     return "unsigned integer";
    case OPT_DOUBLE:   return "double";
    case OPT_MATRIX:   return "matrix";
    case OPT_STRING:   return "string";
    case OPT_COMPLEX:  return "complex";
    default:           return "[undefined]";
    }
  }

  operator int&() { return _int; }
  operator bool&() { return _bool; }
  operator size_t&() { return _uint; }
  operator double&() { return _double; }
  operator MatrixNd&() { return _matrix; }
  operator std::string&() { return _string; }
  operator std::complex<double>&() { return _complex; }

  operator const int&() const { return _int; }
  operator const bool&() const { return _bool; }
  operator const size_t&() const { return _uint; }
  operator const double&() const { return _double; }
  operator const MatrixNd&() const { return _matrix; }
  operator const std::string&() const { return _string; }
  operator const std::complex<double>&() const { return _complex; }

  int &get_int() { return _int; }
  bool &get_bool() { return _bool; }
  size_t &get_uint() { return _uint; }
  double &get_double() { return _double; }
  MatrixNd &get_matrix() { return _matrix; }
  std::string &get_string() { return _string; }
  std::complex<double> &get_complex() { return _complex; }  

  const int &get_int() const { return _int; }
  const bool &get_bool() const { return _bool; }
  const size_t &get_uint() const { return _uint; }
  const double &get_double() const { return _double; }
  const MatrixNd &get_matrix() const { return _matrix; }
  const std::string &get_string() const { return _string; }
  const std::complex<double> &get_complex() const { return _complex; }  

  friend OStream &operator << (OStream &stream, const Attribute &attr )
  {
    stream << int(attr._type);
    switch(attr._type) {
    case OPT_INT:      stream << attr._int; break;
    case OPT_BOOL:     stream << attr._bool; break;
    case OPT_UINT:     stream << attr._uint; break;
    case OPT_DOUBLE:   stream << attr._double; break;
    case OPT_MATRIX:   stream << attr._matrix; break;
    case OPT_STRING:   stream << attr._string; break;
    case OPT_COMPLEX:  stream << attr._complex; break;
    case OPT_CHAR_PTR:
    case OPT_NONE:     break;
    }
    return stream;
  }
  friend IStream &operator >> (IStream &stream, Attribute &attr )
  {
    int type;
    stream >> type;
    attr._type = option_type(type);
    switch(attr._type) {
    case OPT_INT:      stream >> attr._int; break;
    case OPT_BOOL:     stream >> attr._bool; break;
    case OPT_UINT:     stream >> attr._uint; break;
    case OPT_DOUBLE:   stream >> attr._double; break;
    case OPT_MATRIX:   stream >> attr._matrix; break;
    case OPT_STRING:   stream >> attr._string; break;
    case OPT_COMPLEX:  stream >> attr._complex; break;
    case OPT_CHAR_PTR:
    case OPT_NONE:     break;
    }
    return stream;
  }
  friend std::ostream &operator << (std::ostream &stream, const Attribute &attr )
  {
    switch(attr._type) {
    case OPT_INT:      stream << attr._int; break;
    case OPT_BOOL:     stream << attr._bool; break;
    case OPT_UINT:     stream << attr._uint; break;
    case OPT_DOUBLE:   stream << attr._double; break;
    case OPT_MATRIX:   stream << attr._matrix; break;
    case OPT_STRING:   stream << attr._string; break;
    case OPT_COMPLEX:  stream << attr._complex; break;
    case OPT_CHAR_PTR:
    case OPT_NONE:     break;
    }
    return stream;
  }
};

#endif /* attribute_hh */
