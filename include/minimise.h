#ifndef minimise_h
#define minimise_h

class BEAM;
struct BUMP;

void bump_emitt_print();
void bump_beam_print(BUMP *,BEAM *,double *);
void bump_emitt_init_2(BEAM *,BUMP *);
double bump_emitt_2(double , double);
void bump_minimise(BEAM *,BUMP *,double *);

#endif
