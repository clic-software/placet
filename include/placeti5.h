#ifndef placeti5_h
#define placeti5_h

class BEAMLINE;
class BEAM;
class BIN;

void ballistic_init();
void ballistic_set_atl(int do_atl,double time);
void ballistic_set(int ntn,int ntc,int nta,int do_rf,int nquad,
		   int measure_online,double gain);
void ballistic_correct(BEAMLINE *beamline,double quad[],double quad0[],
		  double position_x[],double position[],BIN **bin1,int nbin1,
		  BIN **bin2,int nbin2,BEAM *tb0,BEAM *tb1,BEAM *tb1_x,
		  BEAM *workbunch,BEAM *testbunch,int do_emitt,int do_log,
		       int measure_online,int no_acc);
double test_ballistic_2(BEAMLINE *,double *,double *,double *,BEAM *,BEAM *,
			BEAM *,int *,int *,int ,int ,int ,void (*)(BEAMLINE*),
			char *,const char *,char *);
int fill_factor(BEAMLINE *beamline,char *name);
int divide_linac(BEAMLINE *beamline,char *name,int ncav,double length);
int divide_linac2(BEAMLINE *beamline,char *name,int /*ncav*/,double length);
void beam_position_store_2(BEAM *beam,char *name);
void beam_position_store_absolute(BEAM *beam,char *name);
void beam_save_all(BEAM *beam,char *name,int header,int axis);
void beam_save_all_bunches(BEAM *beam,char *name,int header,int axis);
void beam_save_all_bin(BEAM *beam,char *name,int header,int axis);
void beam_load_all(BEAM *beam,char *name,int header,int axis,
		   int read_structure);
void beam_load_all_bin(BEAM *beam,char *name,int header,int axis,
		       int read_structure);
void beam_rotate_straight(BEAM *beam);
void beam_save_gp(BEAM *beam,char *name,int axis,int bunch,
		  double fixed_energy);
void beam_get_size(BEAM *beam,char *name);
void beam_position_load(BEAM *beam,char *name);
void beamline_correct_z(BEAMLINE *beamline);
//void wavelength_extract(BEAM *beam,double lambda,double *cr,double *sr);
void wavelength_spectrum_extract(BEAM *beam,double l0,double l1,int n,
				 double beta, int flag,char *filename);
void beam_save_gp_2(BEAM *,char *,int ,double ,double ,double ,double ,double );

#endif /* placeti5_h */
