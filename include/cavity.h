#ifndef cavity_h
#define cavity_h

#include <vector>

#include "element.h"
//#include "bpm.h"

class INJECTOR_DATA;
struct R_MATRIX;

extern INJECTOR_DATA injector_data;

class CAVITY : public ELEMENT {

  struct {
    double x;
    double y;
    double phase;
  } book;

  struct _DIPOLE_KICK {
    double x;
    double y;
    _DIPOLE_KICK() : x(0.), y(0.) {}
    _DIPOLE_KICK(double _x, double _y ) : x(_x), y(_y) {}
  } _dipole_kick;

  //  BPM bpm; // position reading based on wakefield monitor
  // use separate struct for BPM for memory reduction, could be changed in future
  struct CavityBPM { // position reading based on wakefield monitor
    struct {
      double x;
      double y;
    } pos,offset;
    // double resolution; // resolution, currently not used, see BPM class on how to implement this if needed
    void set_x_position(double x){pos.x=x;}
    void set_y_position(double y){pos.y=y;}
    double get_x_reading_exact()const{return pos.x;}
    double get_y_reading_exact()const{return pos.y;}
    double get_x_reading()const{return pos.x;}
    double get_y_reading()const{return pos.y;}
    CavityBPM(){
      pos.x=0.0;
      pos.y=0.0;
      offset.x=0.0;
      offset.y=0.0;
      // resolution=0.0;
    }
  } bpm;

  int field;

  double gradient, phase, lambda;

  double v_tesla;

  bool pi_mode;

  std::vector<int> modes;

  WAKE_DATA *wake_data;

  std::vector<double> yz;

  void bookshelf(BEAM * );

  // void wake_kick_many(BEAM *beam );

  void step_partial(BEAM* beam, double l, void (ELEMENT::*step_function)(BEAM*));

protected:
 
  // void sigma_step(R_MATRIX *sigma, double delta );
  void sigma_step_x(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy, double delta );

  void step_rf(BEAM *);
  void step_rf_0(BEAM*);

  void step_4d(BEAM*);  
  void step_4d_0(BEAM*);
  void step_6d_0(BEAM *beam );

  // void step_many(BEAM *beam );
  void step_many_x(BEAM *beam );
  // void step_many_0(BEAM *beam );
  void step_many_0_x(BEAM *beam );

  void step_twiss(BEAM *beam,FILE *file,double step0,int iel, double s0,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step0,int iel, double s0,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));

  void init_attributes();

  void apply_short_range_wakefield(BEAM* beam) {}

public:

  CAVITY(int &argc, char **argv );
  CAVITY(double length,int field,double phase_deg );
  CAVITY(double length,int field,double gradient,double phase_deg );

  void fill_z_mode_xy(double *x, double *y, int n);

  void structure_banana(double bowx,double bowy, double offsetx, double offsety,int noreset=0);

  void set_field(int t ) { field = t; }
  void set_lambda(double l ) { lambda = l;}
  void set_phase(double p ) { phase = p; }
  void set_gradient(double g ) { gradient = g; }
  
  int    get_field() const { return field; }
  double get_phase() const { return phase; }
  double get_lambda() const { return lambda; }
  double get_gradient() const { return gradient; }
  
  void set_dipole_kick_x(double x) { _dipole_kick.x=x; }
  void set_dipole_kick_y(double y) { _dipole_kick.y=y; }

  void set_wake_data(WAKE_DATA *w_data ) { wake_data = w_data; }
  void set_modes(const std::vector<int> &m ) { modes=m; }
  void set_modes(int *m, size_t n ) { modes.resize(n); for(size_t i=0;i<n;i++) modes[i]=m[i]; }

  void set_rosenzweig_fucus(bool rosen) { pi_mode = rosen; } 
  
  bool is_cavity() const { return true; }

  CAVITY *cavity_ptr() { return this; }
  const CAVITY *cavity_ptr() const { return this; }

  /* BPM *get_bpm_ptr() { return &bpm; } */
  /* const BPM *get_bpm_ptr() const { return &bpm; } */

  void track_rf_0(BEAM *beam );

  void set_bookshelf(double x, double y ) { book.x=x; book.y=y; }
  void set_book_phase(double phase1 ) { book.phase=phase1; }
  double get_book_x() const {return book.x;}
  double get_book_y() const {return book.y;}
  double get_book_phase() const {return book.phase;}

  double get_bpm_x_reading_exact() const { return bpm.offset.x+bpm.get_x_reading_exact(); }
  double get_bpm_y_reading_exact() const { return bpm.offset.y+bpm.get_y_reading_exact(); }
  double get_bpm_x_reading() const { return bpm.offset.x+bpm.get_x_reading(); }
  double get_bpm_y_reading() const { return bpm.offset.y+bpm.get_y_reading(); }
  double get_bpm_offset_x() const { return bpm.offset.x; } 
  double get_bpm_offset_y() const { return bpm.offset.y; }

  virtual Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

  void set_bpm_offset_x(double x ) { bpm.offset.x=x; }
  void set_bpm_offset_y(double y ) { bpm.offset.y=y; }

  void set_v_tesla(double v ) { v_tesla=v; }
  double get_v_tesla() const { return v_tesla; }

  void add_yz(int n) { yz.resize(n, 0.0); }
  void add_yz(int i, double x ) { yz[i]+=x; }
  
  std::string aml_string(std::string = "") const;
  
  size_t size_yz() const { return yz.size(); }

  friend OStream &operator<<(OStream &stream, const CAVITY &cav );
};

extern void wakefield_init(int ,int);

#endif
