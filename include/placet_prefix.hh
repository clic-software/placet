#ifndef placet_prefix_hh
#define placet_prefix_hh

#include <string>

/**
 * @brief get the prefix where support files are installed
 *
 * By default, all supporting files/modules are installed in
 * the installation prefix. This can be overridden with the
 * environment variable PLACET_PREFIX.
 *
 * This is used by the testing targets, so that
 * they don't need to depend on the installation target.
 * 
 * @author Yngve Inntjore Levinsen
 * @date June, 2013
 */

extern const char *get_placetdir();
extern std::string get_placet_sharepath(const std::string &extra_path="");

#endif /* placet_prefix_hh */
