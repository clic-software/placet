#ifndef beam_h
#define beam_h

#include "placet.h"
#include "particle.h"
#include "bunch.h"

#include "stream.hh"

struct FIELD;
struct R_MATRIX;

struct BUNCHINFO{
  double z,ch;
};

enum BEAM_TYPE{
  DriveBeam,InjectorBeam,MainBeam
};

struct DRIVE_BEAM_PARAM{
  int n_bunch,n_slice,n_macro;
  /// resist: include resistive wall (not implemented)
  int n_ramp,resist,ramp_step,n_energy,n_z0;
  int *bucket;
  double charge,e0,distance,espread,sigma_z,ecut,envelope_wgt,envelope,
    last_wgt,*eslice_wgt,*d_energy,*z0,*ch,*slice_z_pos,*slice_wgt;
  double alphax,alphay,betax,betay,emittx,emitty,s_ramp,phi0;
  double *slice_init_energy;
};



struct MAIN_BEAM_PARAM{
  int n_bunch,n_slice,n_macro;
  double charge,e0,distance,espread,sigma_z,ecut,phase,last_wgt;
  double alphax,alphay,betax,betay,emittx,emitty;
  double *long_range;
};


struct INJECT_BEAM_PARAM{
  int n_bunch,n_slice,n_macro;
  double charge,e0,distance,espread,sigma_z,ecut,attenuation,overlapp,phase;
  double alphax,alphay,betax,betay,emittx,emitty,last_wgt,*d_energy,*eslice_wgt;
  BUNCHINFO *bunch;
};

struct DRIVE_DATA{
  BEAM_TYPE beamtype;
  union {
    DRIVE_BEAM_PARAM *drive;
    MAIN_BEAM_PARAM *main;
    INJECT_BEAM_PARAM *inject;
  } param;
  int longrange_max,bunches_per_train,empty_buckets,bpm_samples,do_filter;
  int *bpm_min,*bpm_max;
  double *af,*bf,*charge,empty_bucket_number;
  double *ypos,*yposb,*ypos2,*yposb2,*yoff,*xpos,*xposb,*xoff,*along,*blong,
    *rf_kick;
  double *al,*bl,*bunch_z;
  double *factor_long,*factor_kick;
};

struct QUAD_KICK_DATA{
  double **c,**s;
  double **a;
  int n_cell,n_part,n_bunch;
};


// CSRWAKE: set by sbend (reset by subsequent sbends)
struct CSRWAKE_DATA {
  double *terminal_dE_ds;     // terminal wake
  int *terminal_nlambda;   // terminal binning
  int nbins;             
  double attenuation_length; // CSR drift attenuation length, DEFAULT "attenuation_length = 1.5*overtaking_length", but can be overwritted by user in SBend
  double distance_from_sbend; // distance from last sbend
  bool wake_enabled;    // disactivated if false
};

class BEAM {
 public:

  int slices_per_bunch,macroparticles,bunches;

  bool particle_beam;
  
  /* enum BEAM_TYPE {PARTICLE_BEAM,MATRIX_BEAM,SLICE_BEAM} beam_type; */

  // FOR SLICED BEAM!

  inline int  get_index(int _bunch, int _slice, int _particle ) const   { return (_bunch * slices_per_bunch + _slice) * macroparticles + _particle; }

  int get_bunch_number(int index ) const                                { return index / (macroparticles * slices); }
  int get_slice_number(int index ) const                                { return (index / macroparticles) % slices; }
  int get_macroparticle_number(int index ) const                        { return index % macroparticles; }

  /// slice number for both particle beam and sliced beam
  int get_slice_number_generic(int index ) const;

  void get_bsm_number(int index, int &b, int &ss, int &m ) const
  {
    m=index%macroparticles;
    index/=macroparticles;
    ss=index%slices;
    b=index/slices;
  }
  
  /*   int get_number_of_macroparticles_per_slice() const                          { return macroparticles; } */
  /*   int get_number_of_particles_per_bunch() const                               { return slices * macroparticles; } */
  /*   int get_number_of_slices_per_bunch() const                                  { return slices; } */
  /*   int get_number_of_macroparticles() const                                    { return bunches * slices * macroparticles; } */

  inline PARTICLE  &get_particle(int _bunch, int _slice, int _particle ) const { return particle[get_index(_bunch, _slice, _particle)]; }
  inline PARTICLE  &get_particle(int _index ) const                            { return particle[_index]; }

  /// Used by ELEMENT::track to know if it should warn about lost beam
  bool is_lost;
  /// Used by BPM::step to know if it should warn about lost beam
  bool is_lost_bpm;

  int n_max;
  /// number of particles per slice
  int *particle_number,*particle_number_tmp;
  int slices,n_field,which_field;
  /// weight factors for longitudinal wake and transverse wake
  double factor,transv_factor;
  /// weight factor for last bunch. The last bunch can be simulated as multiple bunches, e.g. for long drive beam train.
  // Better represented as integer ? -- JS
  double last_wgt;
  PARTICLE *particle,*p_tmp;
  R_MATRIX *sigma,*sigma_xx,*sigma_xy;
  FIELD *field;
  /// array to store the central x and y value per slice divided by the number of slices. Used in wakekick, dipole kick and cavity, not updated in normal tracking
  double **rho_y,**rho_x;
  /// z position (of the slices)
  double *z_position;
  double **s_long,**c_long;
  DRIVE_DATA *drive_data;
  QUAD_KICK_DATA *quad_kick_data;
  friend OStream &operator<<(OStream &stream, const BEAM &beam );
  friend IStream &operator>>(IStream &stream, BEAM &beam );
  int nhalo; // number of halo particles
#ifdef HTGEN
  PARTICLE *particle_sec, *psec_tmp;
  int *particle_number_sec; //number of halo particles per slice
  bool read_halo(const char *filename);
  void write_halo(const char *filename);
#endif
  CSRWAKE_DATA *csrwake;
  double s; /// position of the beam along the beamline
};

BEAM* beam_make_dispersion(BEAM *,double *,int ,int);

#ifdef HTGEN
extern int tk_BeamReadHalo(ClientData clientdata,Tcl_Interp *interp,int argc, char* argv[]);
extern int tk_BeamWriteHalo(ClientData clientdata,Tcl_Interp *interp,int argc, char* argv[]);
extern int tk_BeamClearHalo(ClientData clientdata,Tcl_Interp *interp,int argc, char* argv[]);
#endif

#endif /* beam_h */
