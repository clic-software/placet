#ifndef solenoid_h
#define solenoid_h

#include "element.h"

class BEAM;
struct PARTICLE;
struct R_MATRIX;

class SOLENOID : public ELEMENT {

  double strength;
  double thin_length; // if length == 0.0 and thin_length != 0 uses this value instead of length for the computation of a thin kick

 public:

 SOLENOID(int &argc, char **argv ) : strength(0.0), thin_length(0.0)
    {
      attributes.add("bz", "Strength of the solenoid [T]", OPT_DOUBLE, &strength);
      attributes.add("thin_length", "Length to be used in case of thin solenoid", OPT_DOUBLE, &thin_length);
      set_attributes(argc, argv);
    }

 SOLENOID(double l, double s ) : ELEMENT(l), strength(s), thin_length(0.0)
    {
      attributes.add("bz", "Strength of the solenoid [T]", OPT_DOUBLE, &strength);
      attributes.add("thin_length", "Length to be used in case of thin solenoid", OPT_DOUBLE, &thin_length);
    }

 SOLENOID() : ELEMENT(), strength(0.0), thin_length(0.0)
    {
      attributes.add("bz", "Strength of the solenoid [T]", OPT_DOUBLE, &strength);
      attributes.add("thin_length", "Length to be used in case of thin solenoid", OPT_DOUBLE, &thin_length);
    }

  void set_strength(double s ) { strength=s; }
  double get_strength() const { return strength; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_4d_0(BEAM*);
  void step_4d(BEAM*);

  bool is_solenoid() const { return true; }

  Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

  SOLENOID *solenoid_ptr() { return this; }
  const SOLENOID *solenoid_ptr() const { return this; }

  void GetMagField(PARTICLE *particle, double *bfield) {};


 private:
  void particle_step_end(PARTICLE* particle, double bz) const;
  void particle_step(PARTICLE* particle, double length, double bz) const;
  void sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,double energy,double length,double bz) const;
  void sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,double energy,double bz) const;
};

#endif /* solenoid_h */
