#ifndef bunch_h
#define bunch_h

class BEAM;
struct PARTICLE;

extern double bunch_get_offset(const BEAM *bunch, double PARTICLE::*);
extern double bunch_get_offset_y(const BEAM *bunch);
extern double bunch_get_offset_x(const BEAM *bunch);
extern double bunch_get_offset_yp(const BEAM *bunch);
extern double bunch_get_offset_xp(const BEAM *bunch);
extern double bunch_get_length(const BEAM *beam, int bunch = 0 );
extern std::vector<double> bunch_get_moments(const BEAM *bunch );
extern std::pair<double,double> bunch_get_energy(const BEAM *bunch );

#endif
