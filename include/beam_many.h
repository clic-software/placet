#ifndef beam_many_h
#define beam_many_h

#include <tcl.h>

class BEAM;

void rotate_x(BEAM* beam,double theta_x);
void rotate_y(BEAM* beam,double theta_y);
int beam_read_many(BEAM* beam,char *name,int binary);
void beam_write(BEAM* beam,char *name,int binary,int axisx,int axisy, int loss,double theta_x,double theta_y);
void beam_write_many(BEAM* beam,char *name,int binary,int axisx,int axisy, int loss, int dist,double theta_x,double theta_y);
int tk_BeamRead(ClientData clientdata,Tcl_Interp *interp,int argc,
		char *argv[]);
int tk_BeamDump(ClientData clientdata,Tcl_Interp *interp,int argc,
		 char *argv[]);
int tk_BeamDelete(ClientData clientdata,Tcl_Interp *interp,int argc,
		 char *argv[]);

#endif // beam_many_h
