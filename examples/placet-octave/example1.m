B = placet_get_number_list("default", "bpm");
C = placet_get_number_list("default", "quadrupole");

A = placet_get_response_matrix("default", "beam0", B, C);

placet_test_no_correction("default", "beam0", "Clic", 1);

b = placet_get_bpm_readings("default", B);
b = b(:,2);

c = -A \ b;

c = [zeros(size(c)),c];

placet_vary_corrector("default", C, c);

placet_test_no_correction("default", "beam0", "None", 1);

[R,S] = placet_get_bpm_readings("default", B);

plot(S, b, S, R(:,2));
