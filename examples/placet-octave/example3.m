B = placet_get_number_list("default", "bpm");
C = placet_get_number_list("default", "quadrupole");

A = placet_get_response_matrix("default", "beam0", B, C);

placet_test_simple_correction("default", "beam0", "Clic", C, A);
placet_test_no_correction("default", "beam0", "None")

[R,S] = placet_get_bpm_readings("default", B);

plot(S, R(:,2));
