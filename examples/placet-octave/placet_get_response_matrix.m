## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_response_matrix (@var{beamline}, @var{beam}, @var{bpms}, @var{correctors})
## Return the response matrix for 'beamline' and 'beam0', using selected 'bpms' and 'correctors'.
## @end deftypefn

function R = placet_get_response_matrix(beamline, beam, B, C)

  if (nargin == 4 && isstr(beamline) && isstr(beam) && isvector(B) && isvector(C))
    
    Res = placet_element_get_attribute("default", B, "resolution");

    placet_element_set_attribute("default", B, "resolution", 0.0);

    placet_test_no_correction(beamline, beam, "Zero");
    
    R0 = placet_get_bpm_readings(beamline, B);

    R = zeros(rows(R0), columns(C));

    for i = 1:columns(C)

      placet_vary_corrector(beamline, C(i), [0,  1]);
      placet_test_no_correction(beamline, beam, "None");
      placet_vary_corrector(beamline, C(i), [0, -1]);

      R1 = placet_get_bpm_readings(beamline, B) - R0;

      R(:,i) = R1(:,2);

    endfor
  
    placet_element_set_attribute("default", B, "resolution", Res);

  else  
  
    help placet_get_response_matrix
  
  endif
  
endfunction
