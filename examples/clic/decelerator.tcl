#!../placet

#
# Example file for PLACET tutorial, 29-nov-2006  ( CLIC Beam Dynamics Meeting)
# E. Adli, AB/ABP
#
# This example file shows how to create  "from scratch" a Drive Beam deceleration simulation, corresponding a TBL with 15 GHz PETS
#


#
#   CONSTANTS
#

# define SI constants
set SI_c 2.998e8
set SI_e 1.602e-19





#
# Part 1
#
# Define the beam parameters (for a sliced beam)
#

# # of bunches
set n_bunches 300
# # of slices per bunch
set n_slices 51
# # of macroparticles per slice
set n_macros 1
# distance betweeen bunches [m]
set d_bunch 0.02
# bunch rms length [um]
set sigma_bunch 400
# gaussian cutoff [sigma]
set gauss_cut 3
# bunch charge [# of e]
set charge 1.4575e10
# set beam initial energy [MeV]
set e0 0.150

# NB: [10e-7m] Define inital emittances (distribution of a macroparticle)  
set emitt_x 1500.0
set emitt_y 1500.0

# longitudinal distribution of slices (gaussian)
set slice_dist [GaussList -n_slices $n_slices -min [expr -$gauss_cut] -max $gauss_cut -sigma $sigma_bunch -charge 1]
# energy distribution of macropartilces (gaussian)
set energy_dist [GaussList -n_slices $n_macros -min -3 -max 3 -sigma [expr $e0*0.01] -charge 1]

# The actual beam will be created after defining the lattice, in Part 3, because it needs to be matched to the lattice







#
# Part 2
# 
# Define the Decelerating Cavity
#


# [m]
set cavitylength 0.8

# Define the longitudinal mode 
set beta_l 0.74
set RQ 575
set lambda_l 0.01

# Define the transverse mode
#                 lambda_t [m]     w_t [V/(pC*m^2)]     Q [-]         beta_t [-]
lappend mode1_t " 0.01             1000.0               5.0           0.5"

# Divide the cavity into n steps ( y, y' is calculated for every steps, several steps needed for large y')
set n_steps 1

# [um]  NB: only used to calculate field non-uniformity (e.g. value does not matter in this example)
set cavityaperture 10e3

CavityDefine -name DecCavity \
    -length $cavitylength \
    -transverse $mode1_t \
    -beta_group_l $beta_l \
    -r_over_q $RQ \
    -lambda_longitudinal $lambda_l \
    -rf_kick 0 \
    -rf_long 0 \
    -rf_a0 [expr $cavityaperture] \
    -rf_order 4 \
    -rf_size 0 \
    -steps $n_steps

puts "\nDecelerating cavity created with the following paramaters:"
puts "    length \[m\]: $cavitylength"
puts "    steps \[\#\]: $n_steps"
puts "    rf order (order of field non-uniformity) \[\\#\]: 4"
puts "    rf size (amplitude of the different orders \[\\#\]: 0"
puts "    Non-uni. RF kick on? (non-uniformity of long.field accounted for)  \[0\\1\]: 0"
puts "    Non-uni. RF long. effect on? (non-uniformity of long.field accounted for)  \[0\\1\]: 0"
puts "    half-aperture of structure \[um\]: [expr $cavityaperture]"
puts "    longitudinal wakefield mode:"
puts "      - lambda_l \[m\]: $lambda_l" 
puts "      - group_velocity, beta_l \[-\]: $beta_l"
puts "      - R' / Q _l \[circuit Ohms per meter \(Ohm/m\)\]: $RQ"
puts "    transverse wakefield mode 1:"
puts "      - lambda_t \[m\]: [lindex [lindex $mode1_t 0] 0]"
puts "      - group_velocity, beta_t \[-\]: [lindex [lindex $mode1_t 0] 3]"
puts "      - amplitude_t \[V\\\(m\^2*pC\)\]: [lindex [lindex $mode1_t 0] 1]"
puts "      - Q_factor_t \[-\]: [lindex [lindex $mode1_t 0] 2]"
puts ""

# Calculate cavity output power
set pow [Power -beta $beta_l -length $cavitylength \
	 -lambda $lambda_l \
	 -r_over_q $RQ \
	 -distance $d_bunch \
	 -bunchlength $sigma_bunch \
	 -charge $charge  \
	 -xmin 2.0 -xmax [expr 4.0]]



# Calculate the maximum deceleration per cavity [GeV] of a macroparticle, in order to reduce focusing strength as the beam is moving through the lattice
set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
		     -lambda $lambda_l \
		     -r_over_q $RQ \
		     -distance $d_bunch \
		     -bunchlength $sigma_bunch \
		   -charge $charge  ]]


puts "Resulting Cavity Output power:"
puts "    Output power per cavity \[\MW\]: [lindex $pow 0]"
puts "    maximal de0 per cavity \[\GeV\]: $de0"
puts ""










#
# Part 3
#
# Define the lattice
#

# Define geometric parameters (one unit consists of one cavity, one bpm and one qpole. The qpole strength is reduced as the beam is decelerated)
set quadrupolelength 0.15
set bpmlength 0.15
set coupler_drift 0.07
set qpole_drift 0.115
set girderlength [expr $cavitylength + $quadrupolelength + $bpmlength + 2*$qpole_drift + $coupler_drift ]

# Normalized quadrupole strength; gives phase advance of ~ 90 deg 
set k_qpole 7

puts "Producing lattice..."
puts "   Girderlength (unitlength): $girderlength"
puts "   Qpolelength: $quadrupolelength"
puts "   Qpole driftlength: $qpole_drift"
puts "   Qpolestrength, normalized: $k_qpole \[m^-2]"
puts "   BPMlength: $bpmlength"
puts "   Cavity length: $cavitylength"
puts "   Cavity coupler length: $coupler_drift"
puts "   Dec per cavity (1 per quadrupole), de0: $de0 \[GeV\]"
puts ""

set bpm_count 0

# This routine will be called every time the beam goes through the middle of a quadrupole (see lattice definition below)
# Put your own routine here if desired (e.g. a beam dump)
proc myUserProcedure {bpm_count} {
    # the bpm_count variable is updated very time the beam hits a new quadrupole center
    # fill in action to be performed in the middle of each quadrupole
    #puts $bpm_count
}

proc put_bpm {} {
    global bpm_count
    TclCall -script "myUserProcedure $bpm_count"
    incr bpm_count
}

set bpm_count 0

#
# Define the lattice: 8 identical FODO cells, starting in the middle of an F qpole, and with focusing strength adjusted so that the lowest energy particle has constant phase-advance
#

set e $e0
set first 1

for {set i 0} {$i < 8} {incr i } {
    Girder
    if {$first == 0} {
	# Bpm element: acts as drift space during tracking
	Bpm -length $bpmlength
	Drift -length $qpole_drift
	# F quadrupole
	Quadrupole -length [expr 1.0*$quadrupolelength] \
 		   -strength [expr -1.0 * $k_qpole * $quadrupolelength * $e]
	Drift -length $qpole_drift
    } else {
	# FIRST PASS (to start at a symmetry point in lattice)

	# Save initial beam
	TclCall -script {BeamSaveAll -file beam_initial.dat}
	# F quadrupole
	put_bpm
	Quadrupole -length [expr 0.5*$quadrupolelength] \
	           -strength [expr -0.5 * $k_qpole * $quadrupolelength * $e]
	Drift -length $qpole_drift
	set first 0
    }

    # PETS
    DecCavity -length $cavitylength	
    Drift -length $coupler_drift

    # Focusing adjustment
    set e [expr $e - $de0]

    # D quadrupole
    Bpm -length $bpmlength
    Drift -length $qpole_drift
    Quadrupole -length [expr 1.0*$quadrupolelength] \
 	       -strength [expr 1.0 * $k_qpole * $quadrupolelength * $e]
    Drift -length $qpole_drift

    # PETS
    DecCavity -length $cavitylength
    Drift -length $coupler_drift

    # Focusing adjustment
    set e [expr $e - $de0]
}


Girder
Bpm -length $bpmlength
Drift -length $qpole_drift
# F quadrupole
Quadrupole -length [expr 0.5*$quadrupolelength] \
	-strength [expr -0.5 * $k_qpole * $quadrupolelength * $e]
put_bpm
# Final beam save
TclCall -script {BeamSaveAll -file beam_final.dat}

# Fix beamline
BeamlineSet -name tbl
puts "...BeamLine \'tbl' created OK."
puts ""

# Match initial Twiss parameters to initial quadrupoles
array set twiss_match [MatchFodo -l1 $quadrupolelength \
		           -l2 $quadrupolelength \
		           -K1 [expr -$k_qpole*$quadrupolelength] \
	                   -K2 [expr $k_qpole*$quadrupolelength] \
	                   -L [expr $girderlength]]

puts "Initial Twiss parameters:"
puts "  beta_x: $twiss_match(beta_x)"
puts "  beta_y: $twiss_match(beta_y)"
puts "  mu_x: [expr 180.0*$twiss_match(mu_x)/acos(-1.0)]"
puts "  mu_y: [expr 180.0*$twiss_match(mu_y)/acos(-1.0)]"
puts ""








#
# Create the beam, mathced to the lattice
#

# Create drive beam
DriveBeam beam1 -bunches $n_bunches \
    -macroparticles $n_macros \
    -e0 $e0 \
    -slice_list $slice_dist \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y 

puts "\nDriveBeam \'tbl' created with the following paramaters:"
puts "    bunches \[\#\]: $n_bunches"
puts "    slices per bunch \[\#\]: $n_slices"
puts "    bunchlength, sigma \[\um\]: $sigma_bunch"
puts "    macroparticles per slice \[\#\]: $n_macros"
puts "      - transverse energy distribution \[ {energy, weight} \]: $energy_dist"
puts "    particles per bunch (charge) \[\#\]: $charge"
puts "      -> charge per bunch \[C\]: [expr $charge*$SI_e]"
puts "    distance between bunches \[m\]: $d_bunch"
puts "      -> resulting f \[GHz\]: [expr (($SI_c / $d_bunch)*1e-9)]"
puts "      -> resulting I \[A\]: [expr ($charge*$SI_e*$SI_c/$d_bunch) ]"
puts "    initial nominal energy \[GeV\]: $e0"
puts "    initial emittance x \[um\]: [expr $emitt_x*0.1]"
puts "    initial emittance y \[um\]: [expr $emitt_y*0.1]"
puts ""








#
# Part 4
#
# Perform the tracking
#


# Define beam initial offset values as beam rms size ( based on initial emittance and beta )
set sigma_y [expr sqrt($emitt_y*1e-7*$twiss_match(beta_y)*0.511e-3/$e0)*1e6]
puts "Offset (sigma_y): $sigma_y"
puts ""
set sigma_yp [expr sqrt($emitt_y*1e-7/$twiss_match(beta_y)*0.511e-3/$e0)*1e6]

FirstOrder 0
BeamlineUse -name tbl

# Set static offset for initial beam [um] - commented for this example
#BeamSetToOffset -beam beam1 -y $sigma_y

# Do tracking (TestNoCorrection: standard tracking, w/o any automatic alignment procedures)
puts "Starting tracking..."
puts ""
TestNoCorrection -beam beam1 -emitt_file emitt.dat  -survey None








#
# Part 5
#
# Data analysis, graphing etc
#

# Use: Octave, GnuPlot etc to analyse the saved beam 

# Example of file handling:
# Saving placet parameters for later analysis
set filename [format "placet_params.dat"]
set fileId [open $filename "w"]
puts $fileId $n_bunches
puts $fileId $n_slices
puts $fileId $n_macros
puts $fileId $e0
puts $fileId $charge
close $fileId








#
# Extra "tutorial" information
#
# 
#

#
# an example of parameter looping in TCL
#

#set paramlist {1 2 3 4 5}
#foreach param $paramlist {
#    puts $param
#    BeamSetToOffset -beam beam1 -y [expr 100*$param]
#    TestNoCorrection -beam beam1 -emitt_file emitt.$param.dat -survey None
#}

#
# how to take command line arguments in TCL, call placet with e.g. :  placet 2 3
#

#set paramlist [list [lindex $argv 0] [lindex $argv 1]]






#
# PART 6 - LATTICE MANIPULATION AND BEAM BASED ALIGNMENT USING PLACET OCTAVE
#

# all values in micrometers
set sigma_quad 100
set sigma_BPM_offset 100
set sigma_BPM_res 10

SurveyErrorSet \
    -quadrupole_x 0 \
    -quadrupole_y $sigma_quad \
    -quadrupole_yp 0 \
    -quadrupole_roll 0 \
    -bpm_x 0 \
    -bpm_y $sigma_BPM_offset

Octave {
    # Reset machine (element offset etc. set to 0 - perfect machine)
    placet_test_no_correction("tbl", "beam1", "Zero");

    # get list of quadrupoles
    elem_Q = placet_get_number_list("tbl", "quadrupole")
    elem_B = placet_get_number_list("tbl", "bpm")

    # get response matrix from selected correctors (here: quads on movers) to selected BPMs
    R0 = placet_get_response_matrix_attribute("tbl", "beam1", elem_B, "y", elem_Q, "y", "None")
    plot(R0); xlabel('BPM'); ylabel('um'); grid;
    pause;

    # define BPM resolution [um]
    placet_element_set_attribute("tbl", elem_B, "resolution", $sigma_BPM_res);

    # do tracking in octave - apply "Clic" type survey
    [E0, BEAM] = placet_test_no_correction("tbl", "beam1", "Clic");
    
    # get the BPM reading before correction
    y0_before = placet_element_get_attribute("tbl", elem_B, "reading_y")
    plot(y0_before/1e3, '-xr');  xlabel('quad nr [-]'); ylabel('y [mm]');
    pause;

    # calculate dispersion before correction
    dp_p = 0.01;
    quad_scaling = 1/(1+dp_p);
    k_Q = placet_element_get_attribute("tbl", elem_Q, "strength");
    placet_element_set_attribute("tbl", elem_Q, "strength", k_Q*quad_scaling);
    placet_test_no_correction("tbl", "beam1", "None");
    y1_before = placet_element_get_attribute("tbl", elem_B, "reading_y"); 
    placet_element_set_attribute("tbl", elem_Q, "strength", k_Q);
    Dy_before = (y0_before-y1_before) / dp_p
    plot(Dy_before/1e3, '-or');  xlabel('quad nr [-]'); ylabel('Dy [mm]');
    pause;

    # calculate correction: all-to-all (equals 1-to-1 in this case)
    d_theta = -pinv(R0) * y0_before'

    # apply correction to correctors (here: move quads in "y")
    placet_element_vary_attribute("tbl", elem_Q, "y", d_theta);

    # do tracking of corrected machine - important: no new survey, "None"
    [E0, BEAM] = placet_test_no_correction("tbl", "beam1", "None");
    
    # get the BPM reading after correction
    y0_after = placet_element_get_attribute("tbl", elem_B, "reading_y") 
    plot(y0_before/1e3, '-xr');  xlabel('quad nr [-]'); ylabel('y [mm]');
    hold on;
    plot(y0_after/1e3, '-xb');
    hold off;
    pause;

    # calculate dispersion after correction
    dp_p = 0.01;
    quad_scaling = 1/(1+dp_p);
    k_Q = placet_element_get_attribute("tbl", elem_Q, "strength");
    placet_element_set_attribute("tbl", elem_Q, "strength", k_Q*quad_scaling);
    placet_test_no_correction("tbl", "beam1", "None");
    y1_after = placet_element_get_attribute("tbl", elem_B, "reading_y"); 
    placet_element_set_attribute("tbl", elem_Q, "strength", k_Q);
    Dy_after = (y0_after-y1_after) / dp_p
    plot(Dy_before/1e3, '-or');  xlabel('quad nr [-]'); ylabel('Dy [mm]');
    hold on;
    plot(Dy_after/1e3, '-ob');
    pause;

    # save results (in octave ascii format)
    save -text results_bba.dat R0 y0_before y0_after Dy_before Dy_after
}

Octave






