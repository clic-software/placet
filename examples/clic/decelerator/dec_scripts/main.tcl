#!../placet

#
# Main placet deceleration simulation file
# E. Adli, AB/ABP
#    

# Inputs arguments (*must* be given when calling this script)
# E.g. "placet tbl myPath 1 1 1 1"

# path to simulation definition files (root path)
set deceleratorrootpath [lindex $argv 0]
# path to common simulation files
# loop parameter 1
set loop_param1 [lindex $argv 1]
set loop_param1_n [lindex $argv 2]
# loop parameter 2
set loop_param2 [lindex $argv 3]
set loop_param2_n [lindex $argv 4]
# external (batch) paramter
set ext_param1 [lindex $argv 5]

# default parameters 
if {$argc < 1} {
    set deceleratorrootpath ".."
}
if {$argc < 2} {
    set loop_param1 1
}
if {$argc < 3} {
    set loop_param1_n 1
}
if {$argc < 4} {
    set loop_param2 0
}
if {$argc < 5} {
    set loop_param2_n 1
}
if {$argc < 6} {
    set ext_param1 1
}


#set commonscriptpath [lindex $argv 1]
set commonscriptpath "$deceleratorrootpath/common_scripts"

#
puts "\'deceleratorrootpath\': $deceleratorrootpath ,   commonscriptpath\': $commonscriptpath ,     \'loop_param1\': $loop_param1 ,  \'loop_param2\': $loop_param2 \'ext_param1\': $ext_param1  "


#
#   CONSTANTS
#

# define SI constants
set SI_c 2.998e8
set SI_e 1.602e-19
set SI_pi 3.14159265
Octave {
    SI_c = $SI_c
    SI_e = $SI_e
}

#
#   INITIALIZATION OF RANDOM NUMBERS
#

Random rand_gauss -type gaussian
Random rand_linear -type linear


#
#   INCLUDES
#

# common octave .m routines are found in this path
Octave {
    addpath("$commonscriptpath");
}

# include major parameter options and error settings
source "$deceleratorrootpath/dec_scripts/user_options.tcl"

# include some misc procs
source "$deceleratorrootpath/dec_scripts/procs_misc.tcl"
source "$deceleratorrootpath/dec_scripts/proc_power.tcl"



#
# Part 1
#
# Define the beam parameters (for a sliced beam)
#

# DEFINE BEAM
source "$deceleratorrootpath/dec_scripts/def_beam_parameters.tcl"






#
# Part 2
# 
# Define the Decelerating Cavity
#

# DEFINE CAVITY
source "$deceleratorrootpath/dec_scripts/def_pets.tcl"

# 0: calculate de0 as in tracking  (1: more realistic, but difference is small)
set bunch_drain 0

# calculate power Form Factor
set form_factor [expr [form_factor  $slice_dist $lambda_l]]
puts "\nForm factor (calc'ed from slice distribution): $form_factor"


#
# do power scans, using placet power routines (if activates a file is dumped and script exited)
#
if { $do_power_scans } {
    Octave {
	filename = "power.txt";
	fid = fopen (filename, "wt");
    }
    for {set prun 1} {$prun <= 1301} {incr prun} {
	set f_extract [expr (0.001+($prun-1)/100.0)*1.0e9]
	set lambda_extract [expr $SI_c / $f_extract]

	set f_bunch [expr (12.0/12.0/2.0)*1e9]
	set lambda_bunch [expr $SI_c / $f_bunch]

	set f_res [expr (12.0-0.1)*1e9]
	set lambda_res [expr $SI_c / $f_res]

	set pow_fd [Power -beta $beta_l -length $cavitylength \
			-lambda_ext [expr $lambda_extract ] \
			-n_bunches 200 \
			-n_slices $n_slices \
			-gauss_cut $gauss_cut \
			-lambda $lambda_res \
			-Q_long [expr $fundamental_mode_Q*100] \
			-r_over_q $RQ \
			-distance $lambda_bunch \
			-bunchlength [expr $sigma_bunch*0.001] \
			-charge $charge  \
			-slices_list $slice_dist \
			-xmin 2.0 -xmax [expr 8.00]]
	set pow [lindex $pow_fd 0]
	set phase [lindex $pow_fd 1]
	#puts "POWER at f: $pow,  PHASE at f: $phase" 
	Octave {
	    fprintf(fid, "%.4f    %.4f   %.4f\n", $f_extract, $pow, $phase);
	}
    }
    Octave {
	fclose(fid);
    }
    exit
}


#
# adjust charge and energy to reach required P and eta_eff
#
if { $activate_power_optimization } {

# output power
set pow_fd [Power -beta $beta_l -length $cavitylength \
	     -lambda_ext [expr $d_bunch/$n_harmonic] \
	     -n_bunches 200 \
	     -n_slices $n_slices \
	     -gauss_cut $gauss_cut \
	     -lambda $lambda_l \
	     -Q_long $fundamental_mode_Q \
	     -r_over_q $RQ \
	     -distance $d_bunch \
	     -bunchlength $sigma_bunch \
	     -charge $charge  \
	     -slices_list $slice_dist \
	     -xmin 2.0 -xmax [expr 4.00]]
set pow [lindex $pow_fd 0]

#set pow_td [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch   0]/1e6]

# Calculate the maximum deceleration per cavity [GeV] of a macroparticle, in order to reduce focusing strength as the beam is moving through the lattice
set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
		   -n_bunches $n_bunches \
		   -n_slices $n_slices \
		   -gauss_cut $gauss_cut \
		   -lambda $lambda_l \
		   -Q_long $fundamental_mode_Q \
		   -r_over_q $RQ \
		   -distance $d_bunch \
		   -bunchlength $sigma_bunch \
		   -bunch_drain_active $bunch_drain \
		   -slices_list $slice_dist \
		   -charge $charge  ]]

set de0_drain [expr [MaxField -beta $beta_l -length $cavitylength \
		   -n_bunches $n_bunches \
		   -n_slices $n_slices \
		   -gauss_cut $gauss_cut \
		   -lambda $lambda_l \
		   -Q_long $fundamental_mode_Q \
		   -r_over_q $RQ \
		   -distance $d_bunch \
		   -bunchlength $sigma_bunch \
		   -bunch_drain_active 1 \
		   -slices_list $slice_dist \
		   -charge $charge  ]]

puts "Initial calculated power: $pow"
puts "Initial calculated de0: $de0"
puts "  Resulting charge: $charge"
puts "  Resulting clength: $cavitylength"

#
#Iterate to get correct power (NB: iteration loop below must be commented out for simulation...)
#

set n_iterations 0
set P0 $requested_PETS_power
 while {abs([lindex $pow 0]-$P0)>1.0e-3} {
     incr n_iterations     
     if { $n_iterations <= 10 } {
	 #     set cavitylength [expr $cavitylength*(sqrt($P0/[lindex $pow 0]))]
	 set charge [expr $charge*sqrt($P0/[lindex $pow 0])]
     } else {
	 if { [lindex $pow 0] < $P0  } {
	     set cavitylength [expr $cavitylength + 0.0001];
	     #	set charge [expr $charge + 5e9];
	 } else {
	     set cavitylength [expr $cavitylength - 0.0001];
	 }
     } 

     set pow_fd [Power -beta $beta_l -length $cavitylength \
		     -lambda_ext [expr $d_bunch/$n_harmonic] \
		     -n_bunches 200 \
		     -n_slices $n_slices \
		     -gauss_cut $gauss_cut \
		     -lambda $lambda_l \
		     -Q_long $fundamental_mode_Q \
		     -r_over_q $RQ \
		     -distance $d_bunch \
		     -bunchlength $sigma_bunch \
		     -charge $charge  \
		     -slices_list $slice_dist \
		     -xmin 2.0 -xmax [expr 4.00]]
     set pow [lindex $pow_fd 0]

     set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
			-n_bunches $n_bunches \
			-n_slices $n_slices \
			-gauss_cut $gauss_cut \
			-lambda $lambda_l \
			-Q_long $fundamental_mode_Q \
			-r_over_q $RQ \
			-distance $d_bunch \
			-bunchlength $sigma_bunch \
			-bunch_drain_active $bunch_drain \
			-slices_list $slice_dist \
			-charge $charge  ]]

set de0_drain [expr [MaxField -beta $beta_l -length $cavitylength \
		   -n_bunches $n_bunches \
		   -n_slices $n_slices \
		   -gauss_cut $gauss_cut \
		   -lambda $lambda_l \
		   -Q_long $fundamental_mode_Q \
		   -r_over_q $RQ \
		   -distance $d_bunch \
		   -bunchlength $sigma_bunch \
		   -bunch_drain_active 1 \
		   -slices_list $slice_dist \
		   -charge $charge  ]]

#puts "Calculated power: $pow"
#puts "Calculated de0: $de0"
#puts "  Resulting charge: $charge"
#puts "  Resulting clength: $cavitylength"

# for graphing
#puts "$cavitylength $charge [lindex $pow 0] [lindex $pow 1] $de0"
puts "$cavitylength $charge $pow  $de0"
#puts "$charge [lindex $pow 0] [lindex $pow 1] $de0"
}


#
# END ITERATION STEP
#



#Iterate to get correct Max deceleration (NB: iteration loop below must be commented out for simulation...)
# while {abs($de0-0.0018750)>1e-8} {
# #  set e0 [expr $e0*sqrt(0.0018750/$de0)]
#    set charge [expr $charge*sqrt(0.0018750/$de0)]

# set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
# 		   -n_bunches $n_bunches \
# 		   -n_slices $n_slices \
# 		   -gauss_cut $gauss_cut \
# 		   -lambda $lambda_l \
#		   -Q_long $fundamental_mode_Q \
# 		   -r_over_q $RQ \
# 		   -distance $d_bunch \
# 		   -bunchlength $sigma_bunch \
# 		   -bunch_drain_active $bunch_drain \
#                  -slices_list $slice_dist \
# 		   -charge $charge  ]]

# puts "Resulting de0: $de0"
# puts "     Resulting charge: $charge"
# # puts "  Resulting e0: $e0"
# }
#end iteration


# FD calc
set pow_fd [Power -beta $beta_l -length $cavitylength \
	     -lambda_ext [expr $d_bunch/$n_harmonic] \
	     -n_bunches 200 \
	     -n_slices $n_slices \
	     -gauss_cut $gauss_cut \
	     -lambda $lambda_l \
	     -Q_long $fundamental_mode_Q \
	     -r_over_q $RQ \
	     -distance $d_bunch \
	     -bunchlength $sigma_bunch \
	     -charge $charge  \
	     -slices_list $slice_dist \
	     -xmin 2.0 -xmax [expr 4.00]]

set pow_fd_noloss [Power -beta $beta_l -length $cavitylength \
	     -lambda_ext [expr $d_bunch/$n_harmonic] \
	     -n_bunches 200 \
	     -n_slices $n_slices \
	     -gauss_cut $gauss_cut \
	     -lambda $lambda_l \
	     -Q_long 1e20 \
	     -r_over_q $RQ \
	     -distance $d_bunch \
	     -bunchlength $sigma_bunch \
	     -charge $charge  \
	     -slices_list $slice_dist \
	     -xmin 2.0 -xmax [expr 4.00]]

set pow [lindex $pow_fd 0]
set pow_noloss [lindex $pow_fd_noloss 0]

set pow_td_nodamp [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch    1]/1e6]
set pow_td [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch   0]/1e6]
puts ""
puts "Final (iterated) Cavity Output power:"
puts "    Output power per cavity, frequency domain \[\MW\]: $pow, with phase \[rad\]: [lindex $pow_fd 1]"
puts "    - [expr ($pow_noloss/$pow-1)*100]\% ohmic losses" 
puts "    Output power per cavity, time domain (analytic) \[\MW\]: $pow_td"
puts "    Output power per cavity, time domain (analytic), with zero wall losses \[\MW\]: $pow_td_nodamp"
puts "    maximal de0 per cavity \[\MeV\]: [expr $de0_drain*1000]   (without bunch drain, used to scale mangnets: [expr $de0*1000] )"
puts ""


# Enforce 90% energy 
#set e0 [expr (8*299*$de0)/0.90]
set e0 [expr ($n_PETS*$de0)/$requested_E_spread]
set e0_drain [expr ($n_PETS*$de0_drain)/$requested_E_spread]
#set e0 2.364729 #1b
#set e0 2.364729 
#set e0 2.369233 #3b
#set e0 2.31806 #5b

puts "[expr $requested_E_spread*100]% final energy spread enforced by setting e0 \[\GeV\] to : $e0"
puts ""

} else {
# MaxField and Power calc w/o power optimization

if { $use_old_power_routines } {

set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
		   -lambda $lambda_l \
		   -r_over_q $RQ \
		   -distance $d_bunch \
		   -bunchlength $sigma_bunch \
		   -charge $charge  ]]

# FD calc
set pow_fd [Power -beta $beta_l -length $cavitylength \
	     -lambda $lambda_l \
	     -r_over_q $RQ \
	     -distance $d_bunch \
	     -bunchlength $sigma_bunch \
	     -charge $charge  \
	     -xmin 2.0 -xmax [expr 4.00]]

set pow [lindex $pow_fd 0]

set pow_td     [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch   0]/1e6]

set pow_td_nodamp [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch   1]/1e6]
} else {


set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
		   -n_bunches $n_bunches \
		   -n_slices $n_slices \
		   -gauss_cut $gauss_cut \
		   -lambda $lambda_l \
		   -Q_long $fundamental_mode_Q \
		   -r_over_q $RQ \
		   -distance $d_bunch \
		   -bunchlength $sigma_bunch \
		   -bunch_drain_active $bunch_drain \
		   -slices_list $slice_dist \
		   -charge $charge  ]]

set de0_drain [expr [MaxField -beta $beta_l -length $cavitylength \
		   -n_bunches $n_bunches \
		   -n_slices $n_slices \
		   -gauss_cut $gauss_cut \
		   -lambda $lambda_l \
		   -Q_long $fundamental_mode_Q \
		   -r_over_q $RQ \
		   -distance $d_bunch \
		   -bunchlength $sigma_bunch \
		   -bunch_drain_active 1 \
		   -slices_list $slice_dist \
		   -charge $charge  ]]

set pow_fd [Power -beta $beta_l -length $cavitylength \
	     -lambda_ext [expr $d_bunch/$n_harmonic] \
	     -n_bunches 200 \
	     -n_slices $n_slices \
	     -gauss_cut $gauss_cut \
	     -lambda $lambda_l \
	     -Q_long $fundamental_mode_Q \
	     -r_over_q $RQ \
	     -distance $d_bunch \
	     -bunchlength $sigma_bunch \
	     -charge $charge  \
	     -slices_list $slice_dist \
	     -xmin 2.0 -xmax [expr 4.00]]

set pow_fd_noloss [Power -beta $beta_l -length $cavitylength \
	     -lambda_ext [expr $d_bunch/$n_harmonic] \
	     -n_bunches 200 \
	     -n_slices $n_slices \
	     -gauss_cut $gauss_cut \
	     -lambda $lambda_l \
	     -Q_long 1e20 \
	     -r_over_q $RQ \
	     -distance $d_bunch \
	     -bunchlength $sigma_bunch \
	     -charge $charge  \
	     -slices_list $slice_dist \
	     -xmin 2.0 -xmax [expr 4.00]]

set pow [lindex $pow_fd 0]
set pow_noloss [lindex $pow_fd_noloss 0]

set pow_td     [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch   0]/1e6]

set pow_td_nodamp [expr [power_td $beta_l  $cavitylength  $n_harmonic  $lambda_l  $RQ  $sigma_bunch  $charge  $d_bunch   1]/1e6]
}


#EA TEMP EA TEMP
#set pow 147.1
#set powno_damp 148.669
#set pow_fd {147.383 0.0129436}
#set charge 50180317967.3
#set de0 0
#set e0 2.49479906667
#EA TEMP EA TEMP - END

puts "\n(Non-iterated values):"
puts "    Non-iterated power per cavity, frequency domain \[\MW\]: $pow, with phase \[rad\]: [lindex $pow_fd 1]"
puts "    - [expr ($pow_noloss/$pow-1)*100]\% ohmic losses" 
puts "    Non-iterated power per cavity, time domain (analytic) \[\MW\]: $pow_td"
puts "    Non-iterated power per cavity, time domain (analytic), with zero wall losses \[\MW\]: $pow_td_nodamp"
puts "    Non-iterated maximal de0 per cavity \[\MeV\]: [expr $de0_drain*1000]   (without bunch drain, used to scale mangnets: [expr $de0*1000] )"
puts ""

# puts "   Non-iterated calculated power, with damping \[MW\]: $pow"
# puts "   Non-iterated calculated power, w/o damping \[MW\]: $pow_td_nodamp"
# puts "   Non-iterated calculated power, FD \[MW\]: [lindex $pow_fd 0], with phase \[rad\]: [lindex $pow_fd 1]"
# puts "   Non-iterated calculated de0 \[MeV\]: [expr $de0*1000]"

}
# END activate_power_optimiziation





# set energy spread, if defined (must done set here, in case of new e0)

# energy slicewise distribution of macroparticles (gaussian)
set energy_dist_gauss [GaussList -n_slices $n_macros -min -3 -max 3 -sigma [expr $e0*$sigma_E_upon_E] -charge 1]
set energy_dist $energy_dist_gauss

# AUTOMATIC UPSCALING OF ENERGY
# if { $sigma_E_upon_E > 0 } {
#     puts "setting new e0 to counter energy spread effect  e0 * (1+ 3*(dE/E) )"
#     set e0 [expr $e0 * (1+ 3*$sigma_E_upon_E)];
# }
# if { $sigma_E_upon_E > 0 } {
#     puts "new e0: $e0"
# }


# EA FORCE N % of current
set charge [expr $charge*$I_scaling_factor] 

# if I-scaling AND "lattice knows" this, recalculate de0
if { $I_scaling_scale_lattice } {
    set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
		       -n_bunches $n_bunches \
		       -n_slices $n_slices \
		       -gauss_cut $gauss_cut \
		       -lambda $lambda_l \
		       -r_over_q $RQ \
		       -distance $d_bunch \
		       -bunchlength $sigma_bunch \
		       -bunch_drain_active $bunch_drain \
		       -slices_list $slice_dist \
		       -charge $charge  ]]
}


# EA FORCE N % of energy
set e0 [expr $e0*$E_scaling_factor] 
#set e0 [expr $e0*(1+$sigma_E_upon_E)]  

# Define cavity

#set f2_l [expr (35.475+0.025*$loop_param1)*1e9]
set f2_l [expr (11.68+0.020*$loop_param1)*1e9]
#set f2_l [expr (1*11.992)*1e9]
lappend modes_l_all "[expr $lambda_l]  [expr $RQ*1.0]  [expr $fundamental_mode_Q]  [expr $beta_l*1.0]"
#lappend modes_l_all "[expr ($SI_c/$f2_l)*1.0]  [expr $RQ*0.02/($f2_l/12.0e9)*1.0e0]  [expr 7000.0e0]  [expr 0.15]"
#lappend modes_l_all "[expr $lambda_l]  [expr $RQ*1.0e-20]  [expr 7000.0]  [expr $beta_l*1.0]"
# lappend modes_l_all "$lambda_l  [expr $RQ*1.0e-10]  0.0  $beta_l"
# lappend modes_l_all "$lambda_l  [expr $RQ*1.0e-10]  0.0  $beta_l"

CavityDefine -name DecCavity \
    -length $cavitylength \
    -transverse_mode_list $modes_t \
    -rf_kick $rf_kick \
    -rf_long $rf_long \
    -rf_a0 [expr $cavityaperture] \
    -rf_order $rf_order \
    -rf_size $rf_size \
    -steps $n_steps \
    -longitudinal_mode_list $modes_l_all
#    -beta_group_l $beta_l \
#    -r_over_q $RQ \
#    -lambda_longitudinal $lambda_l \

# wedges: long mode disappears (in reality: detuneed), transverse modes increase Q
lappend modes_l_all_wedge "[expr $lambda_l]  [expr $RQ*1.0e-20]  [expr 7000.0e20*(1.0)]  [expr $beta_l*1.0]"

CavityDefine -name DecCavityWedge \
    -length $cavitylength \
    -transverse_mode_list $modes_t_wedge \
    -rf_kick $rf_kick \
    -rf_long $rf_long \
    -rf_a0 [expr $cavityaperture] \
    -rf_order $rf_order \
    -rf_size $rf_size \
    -steps $n_steps \
    -longitudinal_mode_list $modes_l_all_wedge



puts "\nDecelerating cavity created with the following paramaters:"
puts "    active length \[m\]: $cavitylength"
puts "    steps \[\#\]: $n_steps"
puts "    rf order (order of field non-uniformity) \[\\#\]: $rf_order"
puts "    rf size (amplitude of the different orders \[\\#\]: $rf_size"
puts "    Non-uni. RF kick on? (non-uniformity of long.field accounted for)  \[0\\1\]: $rf_kick"
puts "    Non-uni. RF long. effect on? (non-uniformity of long.field accounted for)  \[0\\1\]: $rf_long"
puts "    half-aperture of structure \[um\]: [expr $cavityaperture]"
puts "    fundamental wakefield mode (extraction mode):"
puts "      - lambda_l \[m\]: $lambda_l" 
puts "        -> resulting frequency \[GHz\]: [expr $SI_c/($lambda_l*1e9)]" 
if {[info exists detun]} {puts "        -> detuning \[MHz\]: [expr $detun/1e6]" }
puts "      - group_velocity, beta_l \[-\]: $beta_l"
puts "      - R' / Q _l \[Circuit-ohms per meter\]: $RQ"
puts "      - Q-factor_l \[-\]: $fundamental_mode_Q"
set n_modes_T [expr [llength $modes_t]]
puts "    # of transverse wakefield modes: $n_modes_T"
for {set i 0} {$i < $n_modes_T} {incr i } {
    puts "    - transverse wakefield mode $i :"
    puts "      - lambda_t \[m\]: [lindex [lindex $modes_t $i] 0]"
    puts "        -> resulting frequency \[GHz\]: [expr $SI_c/([lindex [lindex $modes_t $i] 0]*1e9)]" 
    puts "      - group_velocity, beta_t \[-\]: [lindex [lindex $modes_t $i] 3]"
    puts "      - amplitude_t \[V\\\(m\^2*pC\)\]: [lindex [lindex $modes_t $i] 1]"
    puts "      - Q_factor_t, effective \[-\]: [expr ([lindex [lindex $modes_t $i] 2]) * (1-[lindex [lindex $modes_t $i] 3])]"
    puts "      - Q_factor_t, excluding (1-beta_t) \[-\]: [lindex [lindex $modes_t $i] 2]"
    puts "        -> resulting wake damping factor, k ( exp(-k*n) )  \[-\]: [expr (1/(1-[lindex [lindex $modes_t $i] 3]))*($SI_pi/[lindex [lindex $modes_t $i] 0])*($d_bunch/[lindex [lindex $modes_t $i] 2]) ]"
    puts "        -> \# of leading bunches with wake >10% \[-\]: [expr floor(-log(0.10) / [expr (1/(1-[lindex [lindex $modes_t $i] 3]))*($SI_pi/[lindex [lindex $modes_t $i] 0])*($d_bunch/[lindex [lindex $modes_t $i] 2]) ]) ]"
    set lambda_T1 [expr [lindex [lindex $modes_t $i] 0]]
    set w_T1 [expr [lindex [lindex $modes_t $i] 1]]
    set Q_T1 [expr [lindex [lindex $modes_t $i] 2]]
    set beta_T1 [expr [lindex [lindex $modes_t $i] 3]]
    Octave {
	pvar_n_modes_T = $n_modes_T;
	pvar_f_T($i+1) = 2*pi*$SI_c / $lambda_T1;
	pvar_w_T($i+1) = $w_T1;
	pvar_Q_T($i+1) = $Q_T1;
	pvar_beta_T($i+1) = $beta_T1;
    }
}
    puts " "
    puts " "


# Define coupler (only if decleared)
if {[info exists couplerlength]} {

CavityDefine -name DecCavityCoupler \
    -length $couplerlength \
    -transverse $modes_coupler_t \
    -beta_group_l $beta_coupler_l \
    -r_over_q $RQ_coupler \
    -lambda_longitudinal $lambda_coupler_l \
    -rf_kick $rf_kick \
    -rf_long $rf_long \
    -rf_a0 [expr $cavityaperture] \
    -rf_order $rf_order \
    -rf_size $rf_size \
    -steps $n_steps_coupler

puts "\nCavity coupler created with the following paramaters:"
puts "    length \[m\]: $couplerlength"
puts "    steps \[\#\]: $n_steps_coupler"
puts "    rf order (order of field non-uniformity) \[\\#\]: $rf_order"
puts "    rf size (amplitude of the different orders \[\\#\]: $rf_size"
puts "    Non-uni. RF kick on? (non-uniformity of long.field accounted for)  \[0\\1\]: $rf_kick"
puts "    Non-uni. RF long. effect on? (non-uniformity of long.field accounted for)  \[0\\1\]: $rf_long"
puts "    half-aperture of structure \[um\]: [expr $cavityaperture]"
puts "    longitudinal wakefield mode:"
puts "      - lambda_l \[m\]: $lambda_coupler_l" 
puts "      - group_velocity, beta_l \[-\]: $beta_coupler_l"
puts "      - R' / Q _l \[Linac-ohms per meter \(Ohm/m\)\]: $RQ_coupler"
if {[expr [llength $modes_coupler_t]] > 0} {
puts "    transverse wakefield mode 1:"
puts "      - lambda_t \[m\]: [lindex [lindex $modes_coupler_t 0] 0]"
puts "        -> resulting frequency \[GHz\]: [expr $SI_c/([lindex [lindex $modes_coupler_t 0] 0]*1e9)]" 
puts "      - group_velocity, beta_t \[-\]: [lindex [lindex $modes_coupler_t 0] 3]"
puts "      - amplitude_t \[V\\\(m\^2*pC\)\]: [lindex [lindex $modes_coupler_t 0] 1]"
puts "      - Q_factor_t \[-\]: [lindex [lindex $modes_coupler_t 0] 2]"
puts "        -> resulting wake damping factor, k ( exp(-k*n) )  \[-\]: [expr (1/(1-[lindex [lindex $modes_coupler_t 0] 3]))*($SI_pi/[lindex [lindex $modes_coupler_t 0] 0])*($d_bunch/[lindex [lindex $modes_coupler_t 0] 2]) ]"
puts "        -> \# of leading bunches with wake >5% \[-\]: [expr floor(-log(0.05) / [expr (1/(1-[lindex [lindex $modes_coupler_t 0] 3]))*($SI_pi/[lindex [lindex $modes_coupler_t 0] 0])*($d_bunch/[lindex [lindex $modes_coupler_t 0] 2]) ]) ]"
}
if {[expr [llength $modes_coupler_t]] > 1} {
puts "    transverse wakefield mode 2:"
puts "      - lambda_t \[m\]: [lindex [lindex $modes_coupler_t 1] 0]"
puts "        -> resulting frequency \[GHz\]: [expr $SI_c/([lindex [lindex $modes_coupler_t 1] 0]*1e9)]" 
puts "      - group_velocity, beta_t \[-\]: [lindex [lindex $modes_coupler_t 1] 3]"
puts "      - amplitude_t \[V\\\(m\^2*pC\)\]: [lindex [lindex $modes_coupler_t 1] 1]"
puts "      - Q_factor_t \[-\]: [lindex [lindex $modes_coupler_t 1] 2]"
puts "        -> resulting wake damping factor, k ( exp(-k*n) )  \[-\]: [expr (1/(1-[lindex [lindex $modes_coupler_t 1] 3]))*($SI_pi/[lindex [lindex $modes_coupler_t 1] 0])*($d_bunch/[lindex [lindex $modes_coupler_t 1] 2]) ]"
puts "        -> \# of leading bunches with wake >5% \[-\]: [expr floor(-log(0.05) / [expr (1/(1-[lindex [lindex $modes_coupler_t 1] 3]))*($SI_pi/[lindex [lindex $modes_coupler_t 1] 0])*($d_bunch/[lindex [lindex $modes_coupler_t 1] 2]) ]) ]"
}
puts ""

}



set macro_sigma_offset_y 0
set aperture_x 1e9
set aperture_y 1e9
set aperture_shape none
if { $macroparticle_init_y } {
    # offset macroparticles in y, in order to e.g. simulate losses (of macroparticles)
    #   we use an energy dist, in order to create the macroparticles, with the correct weight (using the already existing routines),
    #     then we offset them in y with the new option macro_offset_y in the DriveBeam command
    
    # energy slicewise distribution of macroparticles (gaussian)
    set sigma_cut_y 4
    set n_step_y 1.0
    #  more accurate distribution leads to negligible difference in losses
    #set sigma_cut_y 5
    #set n_step_y 0.5
    set n_macros [expr round(2*$sigma_cut_y/$n_step_y+1)]
    set energy_dist_gauss [GaussList -n_slices $n_macros -min -$sigma_cut_y -max $sigma_cut_y  -sigma [expr 1.0e-9] -charge 1]
    set energy_dist $energy_dist_gauss
    puts "gauss list for macro offset: $energy_dist_gauss"
}
if { $track_with_macro_losses} {
    # set aperture for loss calulations
    set aperture_x [expr $cavityaperture*1.0e-6*$loss_aperture_frac]
    set aperture_y [expr $cavityaperture*1.0e-6*$loss_aperture_frac]
    # 1 plane: set losses at a0/sqrt(2)
    if { ~$do_x_alignment } {
	# for 1D: scale aperture down by factor (beta_max+beta_min)/beta_max )  - here "hardcoded" to value 1.167 (does not change much)
	# we also set losses at 1/2 a0, to be consistent with our metric !
       set aperture_x [expr $aperture_x/sqrt(1.167)]
       set aperture_y [expr $aperture_y/sqrt(1.167)]
       #set aperture_x [expr $aperture_x/sqrt(2)]
       #set aperture_y [expr $aperture_y/sqrt(2)]
    }
    set aperture_shape circular
}






#
# Part 3
#
# Lattice
#

# This routine will be called every time the beam goes through the middle of a quadrupole (see lattice definition below)
proc BpmSpectrum1 {i} {
    global twiss_match bpmspectrumname param2_n
    if {$i%2==0} {
	set beta $twiss_match(beta_y)
    } else {
	set beta $twiss_match(beta_x)
    }
    PlotBeamSpectrum -lambda0 [expr 0.01] \
	-lambda1 [expr 0.03] \
	-beta $beta \
	-n 1001 \
	-file "$bpmspectrumname.$i.$param2_n"
    #	PlotBeamSpectrum -lambda0 [expr 0.98*$ll] 
    #	    -lambda1 [expr 1.02*$ll] 
}

proc put_bpm {} {
    global bpm_count
    #EATEST TclCall -script "BpmSpectrum1 $bpm_count"
    #TclCall -script "BpmSpectrum1 $bpm_count"
    incr bpm_count
}

set bpm_count 0

# DEFINE LATTICE
source "$deceleratorrootpath/dec_scripts/def_lattice.tcl"

# Fix beamline
BeamlineSet -name $mysimname
puts "...BeamLine \'$mysimname' created OK."
puts ""

### TEST SECTION
#DivideBeamline -n_cav 10 -length 20
#BeamlineSet -name $autobeamline
### TEST SECTION OFF

# Define beamline to use
BeamlineUse -name $mysimname

# Match initial Twiss parameters to initial quadrupoles
array set twiss_match [MatchFodo -l1 $quadrupolelength \
		           -l2 $quadrupolelength \
		           -K1 [expr -$k_qpole*$quadrupolelength] \
	                   -K2 [expr $k_qpole*$quadrupolelength] \
	                   -L [expr $unitlength]]

puts "Initial Twiss parameters (matched):"
puts "  beta_x: $twiss_match(beta_x)"
puts "  beta_y: $twiss_match(beta_y)"
puts "  alpha_x: $twiss_match(alpha_x)"
puts "  alpha_y: $twiss_match(alpha_y)"
puts "  mu_x: [expr 180.0*$twiss_match(mu_x)/acos(-1.0)]"
puts "  mu_y: [expr 180.0*$twiss_match(mu_y)/acos(-1.0)]"
puts ""

if { $use_arbitrary_initial_beam_ellipse } {
    puts "  OVERRIDING matched initial matched twiss with user defined initial parameters :"
set twiss_match(beta_x) $beta_x_initial
set twiss_match(beta_y) $beta_y_initial
set twiss_match(alpha_x) $alpha_x_initial
set twiss_match(alpha_y) $alpha_y_initial
puts "User-defined twiss parameters :"
puts "  beta_x: $twiss_match(beta_x)"
puts "  beta_y: $twiss_match(beta_y)"
puts "  alpha_x: $twiss_match(alpha_x)"
puts "  alpha_y: $twiss_match(alpha_y)"
puts ""
}

# Introduce artificial mismatch
set twiss_match(beta_y) [expr $twiss_match(beta_y)+$beta_y_offset]
set twiss_match(alpha_y) [expr $twiss_match(alpha_y)+$alpha_y_offset]

puts "Initial Twiss parameters (after mismatch):"
puts "  beta_x: $twiss_match(beta_x)"
puts "  beta_y: $twiss_match(beta_y)"
puts "  alpha_x: $twiss_match(alpha_x)"
puts "  alpha_y: $twiss_match(alpha_y)"
puts ""

# complete macroparticle init_y
if { $macroparticle_init_y } {
    set sigma_y [expr sqrt($emitt_y_sigma_calc*1e-7*$twiss_match(beta_y)*0.511e-3/$e0)*1e6]
    set macro_sigma_offset_y [expr $sigma_y*1.0]
}






#
# Create the beam, mathced to the lattice
#

# Create drive beam
DriveBeam $beamname1 -bunches $n_bunches \
    -macroparticles $n_macros \
    -macro_offset_y $macro_sigma_offset_y \
    -e0 $e0 \
    -e0_list $slice_energy_dist \
    -slice_list $slice_dist \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y [expr $twiss_match(alpha_y)] \
    -beta_y [expr $twiss_match(beta_y)] \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x [expr $emitt_x] \
    -emitt_y $emitt_y \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight 

#
# calc some useful numbers and save them in multisim
#
set e_min_final [expr $e0*(1-$requested_E_spread)]
set gamma_final [expr $e_min_final / 0.511e-3]
set gamma_initial [expr $e0 / 0.511e-3]
if { $set_emitt_x_0 } {
    set r_min [expr 3*sqrt($emitt_y/1e7 * $twiss_match(beta_x) / $gamma_final + $emitt_y/1e7 * $twiss_match(beta_y) / $gamma_final)] 
    set r_initial [expr 3*sqrt($emitt_y/1e7 * $twiss_match(beta_x) / $gamma_initial + $emitt_y/1e7 * $twiss_match(beta_y) / $gamma_initial)] 
} else {
    set r_min [expr 3*sqrt($emitt_x/1e7 * $twiss_match(beta_x) / $gamma_final + $emitt_y/1e7 * $twiss_match(beta_y) / $gamma_final)] 
    set r_initial [expr 3*sqrt($emitt_x/1e7 * $twiss_match(beta_x) / $gamma_initial + $emitt_y/1e7 * $twiss_match(beta_y) / $gamma_initial)] 
}

Octave {
    pvar_e0 = $e0;
    pvar_de0 = $de0;
    pvar_e_min_final = $e_min_final;
    pvar_gamma_final = $gamma_final;
    pvar_r_min = $r_min;
    pvar_gamma_initial = $gamma_initial;
    pvar_r_initial = $r_initial;
    # analytical estimation of field, power and energy loss
    temp_omega_rf = 2*pi*$SI_c/($lambda_l)
    temp_I = $charge*$SI_e*$SI_c/$d_bunch
    temp_eta_ohm = sqrt($pow / $pow_noloss)
    temp_RQ_linac = 2*$RQ;
    pvar_E_beam_ana = 0.5 * temp_RQ_linac * temp_omega_rf * $cavitylength / $beta_l / $SI_c * temp_I * $form_factor * temp_eta_ohm
    pvar_P_ana = (pvar_E_beam_ana)^2 / temp_RQ_linac / temp_omega_rf * $beta_l * SI_c
    pvar_U_max_ana = 0.5 * $cavitylength * pvar_E_beam_ana
    pvar_U_mean_ana = pvar_U_max_ana * $form_factor
    pvar_P_fd = $pow;
}

puts "\nDriveBeam \'$beamname1' created with the following paramaters:"
puts "    bunches \[\#\]: $n_bunches"
puts "    bunch last weight \[\#\]: $last_bunch_weight"
puts "    slices per bunch \[\#\]: $n_slices"
puts "    rms bunchlength, sigma \[\um\]: $sigma_bunch"
puts "    gaussian sigma cut (if applicable) \[\\# of sigmas\]: $gauss_cut"
puts "    initial nominal energy, e0, (average) \[GeV\]: $e0"
puts "      - final minimum energy \[GeV\]: $e_min_final"
puts "      - energy distribution of bunch \[GeV\]: $slice_energy_dist"
puts "    emittance x \[um\]: [expr $emitt_x/10]"
puts "    emittance y \[um\]: [expr $emitt_y/10]"
puts "      - r_0, minimum beam envelope (perfect beam and machine, em_x = em_y) \[mm\]: [expr $r_min*1e3]"
puts "    rms energy spread per slice \[\#\]: $sigma_E_upon_E"
puts "      - energy distribution per slice \[ {energy, weight} \]: $energy_dist"
puts "    macroparticles per slice \[\#\]: $n_macros"
puts "    macroparticles sigma offset y \[\#\]: $macro_sigma_offset_y"
puts "    particles per bunch (charge) \[\#\]: $charge"
puts "      -> charge per bunch \[C\]: [expr $charge*$SI_e]"
puts "    distance between bunches \[m\]: $d_bunch"
puts "      -> resulting f \[GHz\]: [expr (($SI_c / $d_bunch)*1e-9)]"
puts "      -> resulting I \[A\]: [expr ($charge*$SI_e*$SI_c/$d_bunch) ]"
puts "      -> resulting P (including [expr ($pow_noloss/$pow-1)*100]\% ohmic losses) \[MW\]: $pow"
puts "      -> resulting U_max \[MV\]: [expr $de0*1e3]"
if {[info exists pow]} {
    set ss_pow_ext_eff [expr (($pow)*1e6*$n_PETS)/($e0*1e9*($charge*$SI_e*$SI_c/$d_bunch))]
    if {[info exists e0_drain]} {
	set ss_pow_ext_eff [expr (($pow)*1e6*$n_PETS)/($e0_drain*1e9*($charge*$SI_e*$SI_c/$d_bunch))]
	puts "      -> resulting steady-state Power Extraction Efficiency, eps \[\%\]: [expr $ss_pow_ext_eff]"	
    } else {
	set ss_pow_ext_eff [expr (($pow)*1e6*$n_PETS)/($e0*1e9*($charge*$SI_e*$SI_c/$d_bunch))]
	puts "      -> resulting steady-state Power Extraction Efficiency, eps \[\%\]: [expr $ss_pow_ext_eff]"	
    }
}
set n_bunch_steady_state [expr ($cavitylength/$d_bunch) * (1 - $beta_l) / $beta_l ]
puts "      -> resulting n_steadystate (bunches ahead seen) \[-\]: $n_bunch_steady_state"
set t_bunch_steady_state [expr ($n_bunch_steady_state*($d_bunch/$SI_c))]
puts "         -> corresponding fill-time (drain-time) \[ns\]: [expr $t_bunch_steady_state*1e9]"
puts "    initial normalized emittance x \[um\]: [expr $emitt_x*0.1]"
puts "    initial normalized emittance y \[um\]: [expr $emitt_y*0.1]"
puts "    longitudinal charge distribution \[ {pos weight} \]: $slice_dist"
puts ""



# Create alternative low current main beam (with missing bunches)

# create bunch list (for eventual missing bunches)
set bunch_list { }
for {set i 0} {$i < [expr $n_bunches]} {incr i } {
    if { [lsearch $missing_bunches_main [expr $i % $modulator_main]] < 0 } {
	set bunch_full [list $i 0 1]
	lappend bunch_list "$i 0 1"
    } else {
	lappend bunch_list "$i 0 0"
    }
}
#puts $bunch_list
DriveBeam $mainbeamM1 -bunches $n_bunches \
    -macro_offset_y $macro_sigma_offset_y \
    -macroparticles $n_macros \
    -e0 $e0 \
    -e0_list $slice_energy_dist \
    -slice_list $slice_dist \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -bunch_list $bunch_list \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight


if {[info exists disp_free_steering]} {
if { $disp_free_steering } {

# Create drive test beam EQUAL TO MAIN BEAM  (when we want to  e.g. jitter test beam nom, but not main beam)
DriveBeam $testbeamNOM -bunches $n_bunches \
    -macroparticles $n_macros \
    -e0 $e0 \
    -slice_list $slice_dist \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight


# Create drive test beam
DriveBeam $testbeamE1 -bunches $n_bunches \
    -macroparticles $n_macros \
    -slice_list $slice_dist \
    -e0 [expr ($energyfraction1/100.0)*$e0] \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight

# Create drive test beam 2
DriveBeam $testbeamE2 -bunches $n_bunches \
    -macroparticles $n_macros \
    -slice_list $slice_dist \
    -e0 [expr ($energyfraction2/100.0)*$e0] \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight


# Create drive test beam current 1
#    -charge [expr ($currentfraction1/100.0)*$charge] 
DriveBeam $testbeamI1 -bunches $n_bunches \
    -macroparticles $n_macros \
    -slice_list $slice_dist \
    -e0 $e0 \
    -charge [expr ($currentfraction1/100.0)*$charge] \
    -energy_distribution $energy_dist \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight


# Create drive test beam current 2
DriveBeam $testbeamI2 -bunches $n_bunches \
    -macroparticles $n_macros \
    -slice_list $slice_dist \
    -e0 $e0 \
    -charge [expr ($currentfraction2/100.0)*$charge] \
    -energy_distribution $energy_dist \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight


# Create drive test beam missing bunches

# create bunch list (for eventual missing bunches)
set bunch_list { }
for {set i 0} {$i < [expr $n_bunches]} {incr i } {
    if { [lsearch $missing_bunches_test [expr $i % $modulator_test]] < 0 } {
	set bunch_full [list $i 0 1]
	lappend bunch_list "$i 0 1"
    } else {
	lappend bunch_list "$i 0 0"
    }
}
#puts $bunch_list
DriveBeam $testbeamM1 -bunches $n_bunches \
    -macroparticles $n_macros \
    -slice_list $slice_dist \
    -e0 $e0 \
    -charge $charge \
    -energy_distribution $energy_dist \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y \
    -bunch_list $bunch_list \
    -particles $n_particle_beam \
    -last_wgt $last_bunch_weight

puts "Test DriveBeam \'$testbeamE1' created with [expr ($energyfraction1/100.0)] of the nominal energy.\n"
puts "Test DriveBeam \'$testbeamE2' created with [expr ($energyfraction2/100.0)] of the nominal energy.\n"
puts "Test DriveBeam \'$testbeamI1' created with [expr ($currentfraction1/100.0)] of the nominal current.\n"
puts "Test DriveBeam \'$testbeamI2' created with [expr ($currentfraction2/100.0)] of the nominal current.\n"
puts "Test DriveBeam \'$testbeamM1' created with { $missing_bunches_test } out of every $modulator_test bunches missing.\n"
}
}







#
# Part 4
#
# Perform the tracking
#
 
source "$deceleratorrootpath/dec_scripts/def_tracking.tcl"








