set fundamental_mode_f 11.994e9

#
# if charge ramp is selected, define # of bunches from this definition (beam will be modified at start of tracking, see def_lattice.tcl)
#
if { $do_charge_ramp } {
Octave {
    # load "$deceleratorrootpath/dec_scripts/charge_ramp.dat"
    load "$deceleratorrootpath/dec_scripts/charge_tuneup.txt"
    disp(['CHARGE RAMP ACTIVATED: Setting number of bunches to length of ramp up file ' num2str(length(ramp_up))]);
    Tcl_SetVar("n_bunches", length(ramp_up));
}
}

#
# if z modification, load file with z positions (only the number of bunches already defined for the beam will take on these positions)
#
if { $do_modify_initial_z } {
Octave {
    load "$deceleratorrootpath/dec_scripts/zz_init.dat"
    disp(['Z INIT ACTIVATED: Adjusting initial z of bunches according to input file']);
}
}



#
# set initial current and energy
#
set charge [expr $initial_current / $fundamental_mode_f / $SI_e]
set e0  [expr $initial_energy/1.0]
set use_old_power_routines 0

if { $mysimname == "clic12" } {
    if { [expr $n_bunches < 12] } {
	set activate_power_optimization 0
    }
}

if { $mysimname == "tbl12" } {
    set charge [expr $initial_current / $fundamental_mode_f / $SI_e/$tbl_length_scaling]
}   


# if disp_free steering: create test beams as well
set disp_free_steering 1

set energyfraction1 120
set energyfraction2 100.01
set currentfraction1 67
set currentfraction2 96
#set modulator_test 9
#set missing_bunches_test {0 3 6}
set modulator_test 24
set missing_bunches_test {1 3 5 7 9 11}

#set missing_bunches {0 1 2}
#set modulator 12
#set missing_bunches {0 4 8}
#set modulator_test 200
#set missing_bunches_test {0 3  5 11 12 21 22 23 24 25 40 41 42 43 44 45 46 47 48 49 50 71 72 73 74 75 76 77 78 79 80 81 82 83}

#set modulator_main $loop_param2
#set missing_bunches_main {}
#set missing_bunches_main_input {}
#for {set i 0} {$i < [expr $loop_param2-1]} {incr i } {
#   lappend missing_bunches_main [expr $i]
#}

set modulator_main 24
set missing_bunches_main {1 3 5 7 9 11}
#set missing_bunches_main {1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23}
# for main / test in same pulse
#set modulator_main 96
#set missing_bunches_main {49 51 53 55 57 59 73 75 77 79 81 83}

puts "EA: $missing_bunches_main"

set mainbeamM1 "mainbeamM1"
set testbeamNOM "testbeamnominal"
set testbeamE1 "testbeam$energyfraction1"
set testbeamE2 "testbeam$energyfraction2"
set testbeamI1 "testbeamcur$currentfraction1"
set testbeamI2 "testbeamcur$currentfraction2"
set testbeamM1 "testbeammissing"

# name of beam
set beamname1 mybeam1
# distance betweeen bunches [m]
set n_harmonic 1
#set d_bunch [expr 0.025*$n_harmonic]
# bunch to bunch phase-slip [rad], or delta_f
#%set phase_slip 0.01
#set df [expr $fundamental_mode_f*$phase_slip / 2 / $SI_pi]
set df [expr 10e6*($loop_param1-1)]
set df 0.0
set d_bunch [expr ($SI_c/($fundamental_mode_f-$df))*$n_harmonic]
# bunch rms length [um]
set sigma_bunch $sigma_z
# gaussian cutoff [sigma]
set gauss_cut 3


# Placet internal: emittance is in units of 10-7 m   (!)
set emitt_x [expr $emitt_x*1e7]
set emitt_y [expr $emitt_y*1e7]
# Need to keep emitt numbers for sigma calc's, even if tracked beam starts with 0 emittance
set emitt_x_sigma_calc [expr $emitt_x]
set emitt_y_sigma_calc [expr $emitt_y]
# NB: [10e-7m] Define inital normalized emittance (of a macroparticle)  
if { $set_zero_macroparticle_emittance } {
    set emitt_x 1500.0e-20
    set emitt_y 1500.0e-20
    set is_first_order 1
} else {
    set is_first_order 0
}    


if { $set_emitt_x_0 } {
    set emitt_x 1.0e-20
}


#EATEST
#set n_slices 2
#set n_bunches 5
#set emitt_y 0.00001



if { $longdisttype == "gaussian" } {
    # longitudinal distribution of slices (gaussian)
    set slice_dist [GaussList -n_slices $n_slices -min [expr -$gauss_cut] -max $gauss_cut -sigma $sigma_bunch -charge 1]
    # flat energy profile (governed by e0)
   set slice_energy_dist {}
    # Induce manual slice energy spread for gaussian bunch
    #set slice_energy_dist { { 2.374  }  { 2.377  }  { 2.379  }  { 2.38  }  { 2.381  }  { 2.382  }  { 2.382  }  { 2.384  }  { 2.385  }  { 2.386  }  { 2.387  }  { 2.389  }  { 2.39  }  { 2.391  }  { 2.392  }  { 2.393  }  { 2.394  }  { 2.395  }  { 2.396  }  { 2.397  }  { 2.398  }  { 2.399  }  { 2.4  }  { 2.401  }  { 2.402  }  { 2.403  }  { 2.404  }  { 2.405  }  { 2.406  }  { 2.407  }  { 2.408  }  { 2.409  }  { 2.409  }  { 2.41  }  { 2.411  }  { 2.413  }  { 2.413  }  { 2.414  }  { 2.415  }  { 2.416  }  { 2.417  }  { 2.418  }  { 2.418  }  { 2.42  }  { 2.421  }  { 2.422  }  { 2.423  }  { 2.422  }  { 2.424  }  { 2.424  }  { 2.424  } }

    # gaussian macroparticle rms energy spread
    #set sigma_E_upon_E 0.0
} elseif { $longdisttype == "uniform" } {
    # uniform bunch
    set uniform_halflength $sigma_bunch
    set weight [expr 0.9973 / $n_slices]
    set pos_inc [expr 2*$uniform_halflength / ($n_slices-1)]
    set slice_dist {}
    set pos [expr -$uniform_halflength]
    for {set i 0} {$i < $n_slices} {incr i } {
	lappend slice_dist "[expr $pos] [expr $weight]"
	incr pos $pos_inc
    }
    # uniform rms energy spread
    #set sigma_E_upon_E 0.0
    set slice_energy_dist {}
} elseif { $longdisttype == "import" } {
    Octave {
	puts "  \n...IMPORTING CHARGE AND ENERGY DISTRIBUTION...\n"
	load 'long_dist.dat';
	
	# ASSUMING "PLACET units"
	#z_n = z_n * 1e6; # um
	#E_n = E_n / 1e9; # GeV
	#_rms_n = E_rms_n / 1e9; # GeV

	# FLIP TO PLACET DEF (first slice start of bunch)
	#E_n = flipud(fliplr(E_n));
	#E_rms_n = flipud(fliplr(E_rms_n));

	# TEMP
	# FORCE LARGER ENERGY SPREAD
	#E_n = (E_n * 4) - 3*( sum(E_n) / length(E_n) );
	# TEMP END

	slice_list_string = '';
	E_list_string = '';
	E_rms_list_string = '';
	# Convert from Octave ARRAYS to TCL List of Lists
	for slice=1:length(weight_n)
	  slice_list_string = strcat(slice_list_string, ' { ');
	  slice_list_string = strcat(slice_list_string, [num2str( z_n(slice) ) ' ']);
	  slice_list_string = strcat(slice_list_string, [num2str( weight_n(slice) ) ' ']);
	  slice_list_string = strcat(slice_list_string, ' } ');
	  E_list_string = strcat(E_list_string, ' { ');
	  E_list_string = strcat(E_list_string, [num2str( E_n(slice) ) ' ']);
	  E_list_string = strcat(E_list_string, ' } ');
	  E_rms_list_string = strcat(E_rms_list_string, ' { ');
	  E_rms_list_string = strcat(E_rms_list_string, [num2str( z_n(slice) ) ' ']);
	  E_rms_list_string = strcat(E_rms_list_string, [num2str( E_rms_n(slice) ) ' ']);
	  E_rms_list_string = strcat(E_rms_list_string, ' } ');
	endfor	   

	# Calculating average values
	E_list_avg = sum(E_n) / length(E_n);
	E_rms_list_avg = sum(E_rms_n) / length(E_rms_n);

	Tcl_SetVar("slice_dist", slice_list_string);
	Tcl_SetVar("slice_energy_dist", E_list_string);
	Tcl_SetVar("slice_energy_dist_avg", E_list_avg);
	Tcl_SetVar("energy_rms_dist", E_rms_list_string);
	Tcl_SetVar("energy_rms_dist_avg", E_rms_list_avg );
#	Tcl_SetVar("energy_rms_dist_avg", E_rms_avg );
    }
#    puts "slice_dist: $slice_dist"
#    puts "slice_energy_dist: $slice_energy_dist"
#    puts "slice_energy_dist_avg: $slice_energy_dist_avg"
#    puts "energy_rms_dist: $energy_rms_dist"
#    puts "energy_rms_dist_avg: $energy_rms_dist_avg"

    # set e0 (used for calc of rel. spread, and for FODO scaling) to avg E
    set e0 [expr $slice_energy_dist_avg]

    # energy distribution over macroparticles [fraction of nominal energy, per slice]  
    set sigma_E_upon_E [expr $energy_rms_dist_avg / $slice_energy_dist_avg]
}


    # energy distribution over macroparticles [fraction of nominal energy]   (increases the number of macropartices)
    # # of macroparticles per slice
    if { $sigma_E_upon_E > 0 } {
	set n_macros $n_E_macros_per_slice 
	#set n_macros 41  
    } else {
	set n_macros 1
    }    
    puts "EA: sigma_E_upon_E: $sigma_E_upon_E\n"

#set slice_dist { {-1600 0.0588} {-1400 0.0588} {-1200 0.0588} {-1000 0.0588} {-800 0.0588} {-600 0.0588} {-400 0.0588} {-200 0.0588} {0 .0588} {200 0.0588} {400 0.0588} {600 0.0588} {800 0.0588}  {1000 0.0588} {1200 0.0588} {1400 0.0588} {1600 0.0588}   }
#set n_slices 17
# uniform LONG bunch
#set slice_dist { {-3200 0.0588} {-2800 0.0588} {-2400 0.0588} {-2000 0.0588} {-1600 0.0588} {-1200 0.0588} {-800 0.0588} {-400 0.0588} {0 .0588} {400 0.0588} {800 0.0588} {1200 0.0588} {1600 0.0588}  {2000 0.0588} {2400 0.0588} {2800 0.0588} {3200 0.0588}   }
#puts "slice dist $slice_dist"

