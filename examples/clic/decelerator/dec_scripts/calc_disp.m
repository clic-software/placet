Octave {

    disp('Starting calc disp');

    
Q = placet_get_number_list("$mysimname", "quadrupole");
B = placet_get_number_list_real_bpm("$mysimname");
Bs = placet_element_get_attribute("$mysimname", B, "s");
E0 = placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, B(end));
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");

E1 = placet_test_no_correction("$mysimname", "$testbeamE1", "None", 1, 0, B(end));
y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");

dp_p = ($energyfraction1-100) / 100;

Dy = (y0-y1) / dp_p;

save -text disp${pa_suffix}.dat Dy y0 y1 E0 E1

disp('Finished calc disp');
}
