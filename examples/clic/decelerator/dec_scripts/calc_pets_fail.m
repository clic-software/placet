#set bpm_resolution [lindex $argv 0] 
#set wgt1 [lindex $argv 1]
#BeamlineSet -name "test"

set nm $n_machines

set svd_cut_SC 1.0

Octave {
    disp('Starting PETS FAIL');

  # main beam, used for tracking AFTER corrections
  beamname1 = Tcl_GetVar("beamname1")
  # nominal test beam, copy of main beam, but with eventual beam jitter
#  testbeamNOMname = Tcl_GetVar("testbeamNOM")
  # test beam, with eventual bem, jitter
#  testbeamname1 = Tcl_GetVar("testbeam1")
#  testbeamname2 = Tcl_GetVar("testbeam2")
  
    B = placet_get_number_list("$mysimname", "bpm");
    C = placet_get_number_list("$mysimname", "quadrupole");

    placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);

    if (exist("$deceleratorrootpath/precalc/responsematrices/R0_${mysimname}_${Rsuffix}.dat", "file")) 
    disp('Loading previously calculated R0...');
    load $deceleratorrootpath/precalc/responsematrices/R0_${mysimname}_${Rsuffix}.dat;
  else
    disp('Recalculating R0...');
    R0 = placet_get_response_matrix("$mysimname", beamname1, B, C, "Zero");
    save -text R0_${mysimname}_${Rsuffix}.dat R0
  endif
  

#   if (exist("R2.dat", "file"))
#     load R2.dat;
#   else
#     R2 = placet_get_response_matrix("$mysimname", testbeamname2, B, C, "Zero") - R0;
#     save -text R2.dat R2
#   endif

    tB = B;
    tC = C;

    tR=R0;
    [U,S,V]=svd(tR);
   n_diag = min(size(S,2));
    n_start = round(n_diag*$svd_cut_SC)+1;
    for n=n_start:n_diag,
    S(n,n) = 0;
    endfor
    tR_pinv = -V*pinv(S)*U';


  E = 0;
  E_fail = 0;
    envelope_lattice_max = zeros(length(C)+1,1); # column of max envelope over all machines, along lattice
  envelopes = zeros($nm); # max envelope for each machine (disgregarding end point of lattice)

  for i=1:$nm
  i 
  
    Tcl_Eval("my_survey"); # this is your "survey" command
#   manual set of some elements
#    b_temp = placet_element_get_attribute("$mysimname", B, "y")';
#    b_temp(10) = 500;
#    b_temp(11) = 500;
#    placet_element_set_attribute("$mysimname", B, "y", b_temp);

  # Octave visualization - display quadrupole position BEFORE correction
  C = placet_get_number_list("$mysimname", "quadrupole");
  B = placet_get_number_list("$mysimname", "bpm");
  C_after = placet_element_get_attribute("$mysimname", C, "y")';
  B_after = placet_element_get_attribute("$mysimname", B, "y")';
  save -text "Cbefore_OCTSC.dat" C_after
  save -text "Bbefore_OCTSC.dat" B_after



    E_uncorr = placet_test_no_correction("$mysimname", beamname1, "None", 1, 0, tB(end));
    tb0 = placet_element_get_attribute("$mysimname", tB, "reading_y")';

    
  max_E_NC = max(E_uncorr(:, 8))

    tb = [tb0];
    tc = tR_pinv * tb;
    tc = [zeros(size(tc)),tc];
    placet_vary_corrector("$mysimname", tC, tc);


    E_current_machine = placet_test_no_correction("$mysimname", beamname1, "None");
    E += E_current_machine;
    envelope_lattice_max = max(envelope_lattice_max, E_current_machine(:,8));
    envelope_max(i)  = max( E_current_machine(1:size(E_current_machine,1)-1, 8));

  # inhibit PETS
   #P = placet_get_number_list("$mysimname", "cavity_pets");
   #p_fail_n = 1000;
   #p_fail = ceil(length(P).*rand(p_fail_n,1))
   #placet_element_set_attribute("$mysimname", P(p_fail_n), "e0", -1.0);

# P = placet_get_number_list("$mysimname", "cavity_pets");
# placet_element_set_attribute("$mysimname", P(159), "e0", -1.0);

   
   E_current_machine = placet_test_no_correction("$mysimname", beamname1, "None");
   E_fail += E_current_machine;
   tb0 = placet_element_get_attribute("$mysimname", tB, "reading_y")';
   
    save -text tb0.dat tb0

  endfor

  E /= $nm;
  E_fail /= $nm;
  
  #max_E_SC = max(E(1:(end-1), 8))
  max_E_fail = max(E_fail(:, 8))
  

  E(:,8) = envelope_lattice_max;
  p = 0.9 % max envelope percentile
  envelope_max = sort(envelope_max)
  env_p = envelope_max(round(length(envelope_max)*p)) 

  if( ~$macroparticle_init_y ) 
    save -text emitt_OCTSC.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run E
    save -text emitt_OCTSC.p.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run env_p
  end% if

  # Octave visualization - display quadrupole position AFTER correction
  C = placet_get_number_list("$mysimname", "quadrupole");
  B = placet_get_number_list("$mysimname", "bpm");
  C_after = placet_element_get_attribute("$mysimname", C, "y")';
  B_after = placet_element_get_attribute("$mysimname", B, "y")';
  save -text "Cafter_OCTSC.dat" C_after
  save -text "Bafter_OCTSC.dat" B_after

   P = placet_get_number_list("$mysimname", "cavity_pets");
   placet_element_set_attribute("$mysimname", P(159), "e0", +1.0);

   disp('Finishing PETS fail routine');
}








