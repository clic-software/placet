function S = myp()
% prints epsc2 to file
%grid;
%__gnuplot_set__ terminal postscript color eps lw 4 48;
%__gnuplot_set__ terminal postscript color eps lw 3 "Helvetica" 36
%__gnuplot_set__ pointsize  2
%print('/Users/eadli/work/myMatlab/plot.eps','-depsc2','-FHelvetica:20');
print('/Users/eadli/work/myMatlab/plot.eps','-depsc2','-FHelvetica:28');
%print('/Users/eadli/work/myMatlab/plot.eps','-depslatex','-FHelvetica:20');
%print('/Users/eadli/work/myMatlab/plot.eps','-d','-FHelvetica:20');
%print('/Users/eadli/work/myMatlab/plot.eps','-depsc2','-FHelvetica:32');

%pause(0.1);
%__gnuplot_set__ pointsize  1;
%__gnuplot_set__ terminal x11;
