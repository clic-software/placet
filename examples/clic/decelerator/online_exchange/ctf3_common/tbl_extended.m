function [quads, dipols, hcors, vcors, hmovers, vmovers,  mons] = tbl_extended()
% merge TBL relevant devices from TL2, TL2' and TBL

[tl2_quads, tl2_dipols, tl2_hcors, tl2_vcors, tl2_mons ] = tl2();
[tl2p_quads, tl2p_dipols, tl2p_hcors, tl2p_vcors, tl2p_mons ] = tl2prime();
[tbl_quads, tbl_dipols, tbl_hcors, tbl_vcors, tbl_hmovers, tbl_vmovers, tbl_mons ] = tbl();

quads = [tl2_quads(length(tl2_quads)-3:length(tl2_quads)),  tbl_quads];
dipols = [tl2p_dipols(1), tbl_dipols];
hcors = [tl2_hcors(length(tl2_hcors)), tbl_hcors];
vcors = [tl2_vcors(length(tl2_vcors)), tbl_vcors];
mons = [tl2_mons(length(tl2_mons)), tbl_mons];
hmovers = [tbl_hmovers];
vmovers = [tbl_vmovers];