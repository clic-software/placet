function  writevmdevice(name, value)


vmdb = importdata('virtual.currents',' ',0);
vmndevs = length(vmdb.textdata);

n=0;
for j=1:vmndevs
  vmname = char(vmdb.textdata(j));
  if (strcmpi(name,vmname))
    n = j;
    break
  end
end

if (n == 0)
  error('Can not find device %s in the list of devices. Java Control Error.\n',name);
end;

vmdb.data(n) = value;



of = fopen('virtual.currents','w');

for j=1:vmndevs
  vmname = char(vmdb.textdata(j));
  fprintf(of,'%s %f\n',vmname,vmdb.data(j));
end

fclose(of);
