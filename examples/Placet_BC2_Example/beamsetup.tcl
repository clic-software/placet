#
# particle beam setup
# currently without wake fields
# 
# general parameters have to be store before
# in the array beamparams=
# [betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#  uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
#
# ATTENTION: The units used here do not match the units used by Placet internally!
#            Hence, at some places the units have to be matched.


proc randn {nsigma} {
    set r 1e12
    while {abs($r)>$nsigma} {
        set v1 [expr { 2. * rand() - 1 }]
        set v2 [expr { 2. * rand() - 1 }]
        set rsq [expr { $v1*$v1 + $v2*$v2 }]
        while { $rsq > 1. } {
 	    set v1 [expr { 2. * rand() - 1 }]
 	    set v2 [expr { 2. * rand() - 1 }]
 	    set rsq [expr { $v1*$v1 + $v2*$v2 }]
        }
        set fac [expr { sqrt( -2. * log( $rsq ) / $rsq ) }]
        set r [expr { $v1 * $fac }]
	if {abs($r)>$nsigma} {
            set r [expr { $v2 * $fac }]
	}
    }
    return $r
}
 

proc create_particles {outfile bparray} {
    upvar $bparray beamparams
    
    set ex [expr $beamparams(emitnx)/($beamparams(energy)/510998.9)]
    set ey [expr $beamparams(emitny)/($beamparams(energy)/510998.9)]
    set bx $beamparams(betax)
    set by $beamparams(betay)
    set ax $beamparams(alphax)
    set ay $beamparams(alphay)
    set sx [expr sqrt($ex*$bx)/1e-6]
    set sxpu [expr sqrt($ex/$bx)/1e-6]
    set sy [expr sqrt($ey*$by)/1e-6]
    set sypu [expr sqrt($ey/$by)/1e-6]
    set sz [expr $beamparams(sigmaz)/1e-6]
    set energy [expr $beamparams(energy)/1e9]
    set seunc [expr abs($beamparams(uncespread))*$energy]
    set ec [expr $beamparams(echirp)*$energy*1e-6]
    set nsigma $beamparams(nsigma)
    
    set xxpc [expr -$ax*$sxpu/$sx]
    set yypc [expr -$ay*$sypu/$sy]
    
    set f [open particles.tmp w]
    set n [expr $beamparams(nslice)*$beamparams(nmacro)]
    for {set i 0} {$i<$n} {incr i} {
	set x [expr [randn $nsigma]*$sx]
        set xp [expr [randn $nsigma]*$sxpu+$x*$xxpc]
	set y [expr [randn $nsigma]*$sy]
        set yp [expr [randn $nsigma]*$sypu+$y*$yypc]
	set z [expr [randn $nsigma]*$sz]
        set e [expr [randn $nsigma]*$seunc+$z*$ec+$energy]
	puts $f "$e $x $y $z $xp $yp"
    }
    close $f

    exec cp particles.tmp $outfile
#    exec sort -b -g -k 4,4 particles.tmp > particles.in
}


proc wake_kick_zero {filename sigmamin sigmamax sigmaz nslice} {
# create a list of longitudinal and transverse wake kicks
# required by the InjectorBeam command
# all kicks are set to 0
    set file [open $filename w]
    puts $file $nslice
    puts $file "1.0"
    set gl [GaussList -min $sigmamin -max $sigmamax -sigma $sigmaz -charge 1.0 -n_slices $nslice]
    foreach x $gl {
        puts $file "[lindex $x 0] 0 [lindex $x 1]"
    }
    set maxi [expr ($nslice+1)*$nslice/2]
    for {set i 0} {$i<$maxi} {incr i} {
        puts $file "0"
    }
    close $file
}


proc make_particle_beam {name bparray} {
    upvar $bparray beamparams
#    puts [array get beamparams]

# the following is only required to set up the internal structure which stores the beam,
# by using BeamRead at the end all particle data is overwritten,
# however wake data persists, but in this case it is not used
    set sg $beamparams(nsigma)
    wake_kick_zero beamwake.dat [expr -1*$sg] $sg [expr 1e6*$beamparams(sigmaz)] $beamparams(nslice)

    WakeSet wakelong {}
    InjectorCavityDefine -lambda 1.0 -wakelong wakelong

    InjectorBeam $name -bunches 1 \
	    -macroparticles $beamparams(nmacro) \
	    -particles [expr $beamparams(nslice)*$beamparams(nmacro)] \
	    -energyspread $beamparams(uncespread) \
	    -ecut $beamparams(nsigma) \
	    -e0 $beamparams(energy) \
	    -file beamwake.dat \
	    -chargelist {1.0} \
	    -charge $beamparams(charge) \
	    -phase 0.0 \
	    -overlapp 0.0 \
	    -distance 0.0 \
	    -beta_x $beamparams(betax) \
	    -alpha_x $beamparams(alphax) \
	    -emitt_x $beamparams(emitnx) \
	    -beta_y $beamparams(betay) \
	    -alpha_y $beamparams(alphay) \
	    -emitt_y $beamparams(emitny)
    
# now create and load the real particles

    create_particles particles.in beamparams
    BeamRead -file particles.in -beam $name
}

