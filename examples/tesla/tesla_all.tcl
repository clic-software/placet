#
# Define the initial beam parameters
#

set e_initial 4.59423
set e0 $e_initial

array set match {beta_x 89.309 beta_y 50.681 alpha_x -1.451 alpha_y 0.873}

set match(emitt_x) 80.0
set match(emitt_y) 0.2
set match(sigma_z) 300.0
set match(e_spread) 2.8
set charge 2e10

#
# Read the linac
#

source tesla_linac.tcl

#
# Define energy before BDS (to set correct BDS magnet strengths) and load BDS
# not used right now
#

set e0 [expr 251.663-5.0+$e_initial+0.027]

#source tesla_bds.tcl

#
# Fix the beam line
#

BeamlineSet

#
# Load the beam definition (single bunch wakes etc)
#

source tesla_beam.tcl

#
# Create a beam beam0 with 25 slices and 7 different energies per slice
# and a beam beamt with 300 such bunches and assuming the remaining 2500
# look like bunch 300
#

make_beam_slice beam0 25 7
make_beam_train35 beamt 2800 300 25 7

#
# Placeholder for other commands
#

proc BeamlineView {} {
    BeamDump -file electron.ini
}

#
# Define misalignment procedure
#

proc my_survey {} {
    Clic
}

#
# Define errors for command "Clic"
#

SurveyErrorSet -quadrupole_y 100.0 \
    -cavity_y 100.0 \
    -cavity_yp 0.0 \
    -bpm_y 10.0

#
# Run 100 different machines with the misalignment and one-to-one correction,
# mean emittance growth can be found in file
# "dummy" column 1 is quadrupole number, column 2 is horizontal emittance,
# column 6 is vertical emittance (both in 10^{-7}m)
#

TestSimpleCorrection -beam beam0 -machines 100 -emitt_file dummy \
    -survey my_survey

#
# Run 100 different machines with the misalignment and no correction,
# mean emittance growth can be found in file
# "dummy2" column 1 is quadrupole number, column 2 is horizontal emittance,
# column 6 is vertical emittance (both in 10^{-7}m)
#

TestNoCorrection -beam beam0 -machines 100 -emitt_file dummy2 \
    -survey my_survey

#
# Run one train through the last machine
#

TestNoCorrection -beam beamt -machines 1 -emitt_file dummy3 -survey None
