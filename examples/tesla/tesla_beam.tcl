set ch {1.0}

#
# transverse wakefield
# s is given in micro metres
# return value is in V/pCm^2
#

proc w_transv {s} {
    return [expr 1.0*(1290.0*sqrt($s*1e-6)-2600.0*$s*1e-6)]
}

#
# longitudinal wakefield
# s is given in micro metres
# return value is in V/pCm
#

proc w_long {s} {
    return [expr 315.0*(1.165*exp(-sqrt($s/3.65e3))-0.165)/(8.0)]
}

source wake_calc.tcl

#
# Define the beams
#

source make_beam.tcl
