# Example of an unmatched and asymmetric FODO cell

### #      betax          alfax       betax         alfay
### twiss0 170.71067812   2.41421356  29.28932188  -0.41421356
### twiss1   5.85786438   0.41421356  34.14213562  -2.41421356
### length 90
### # drift,quad,drift,quad,drift , drift : L in [m], quad : f in [m]
### # last drift fixed by the code to fit total 'length' (see 2 lines above)
### seq 50 -30 30 10

########## DEFINE BEAMLINE

set e_initial 9.0
set e0 $e_initial

BeamlineNew
Girder
  Drift -length 50 ; # 1
  Quadrupole -length 1.0 -strength [expr $e_initial / (-30.0)] ; # 2
  Drift -length 30
  Quadrupole -length 1.0 -strength [expr $e_initial / (+10.0)]
  Drift -length 30
BeamlineSet -name beamline

######### DEFINE BEAM

array set twiss0 {
  beta_x 170.71067812
  beta_y 29.28932188 
  alpha_x 2.41421356  
  alpha_y -0.41421356
}

set script_dir .

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

array set match [array get twiss0]

# emittance is in 10^-7 m.rad
array set match {
  emitt_x 1.0
  emitt_y 1.0
}

set match(e_spread) 2.0 ; # percent
set match(charge) 2.56e9 ; # number of particles
set charge $match(charge)
set match(sigma_z) 0.0 ; # um

set n_slice 20
set n 1000
set n_total [expr $n_slice * $n]

#
# Create the beam
#

make_beam_many beam0 $n_slice $n

FirstOrder 1

########## DO STUFF

Octave {
    [emitt,beam] = placet_test_no_correction(beamline, "beam0", "None");

    save -text beam.out beam
}
