import pylab as p

ild=p.loadtxt('ildantinobuck.txt')
sid=p.loadtxt('SiD+antiDID_2005.txt')

def plot(c):
    p.figure()
    p.plot(ild[:,0],ild[:,c],label='ild')
    p.plot(sid[:,0],sid[:,c],label='SiD')
    p.legend()
def semilogy(c):
    p.figure()
    p.semilogy(ild[:,0],ild[:,c],label='ild')
    p.semilogy(sid[:,0],sid[:,c],label='SiD')
    p.legend()

for c in range(1,4):
    #plot(c)
    semilogy(c)

p.show()
