##
# In this example we first track the beam up to the solenoid map.
# After dumping the beam, we then proceed to track without synrad, and without
# solenoid map on through the solenoid part of the beamline.
# We then make use of backwards tracking, with solenoid on. synrad still off.
# Finally we track forward with both synrad and solenoid
#
# This gives us:
#  - The ideal beam distribution without any effects from synrad or solenoids.
#  - The beam effect of the solenoid ignoring the synrad, to be compared with
#  - The effect of solenoid and the synrad
#
# What we in the end are looking for is the effect of the synchrotron radiation on
# the luminosity.

set e_initial 1496.0
set e0 $e_initial
set script_dir .


array set args {
    step 0.001
    n_slice 30
    n 2000
}

array set args $argv

# Step length in the IR tracking routine
set step $args(step)
# Number of slices
set n_slice $args(n_slice)
# Number of particles per slice
set n $args(n)

set synrad 0
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1
set scale 1.0

source $script_dir/clic_basic_single.tcl

proc save_beam {name} {

    BeamDump -file $name 
}

source bds.coll.tcl
set name0 particles.out.coll 
TclCall -script {save_beam $name0}
BeamlineSet -name test.coll

array set match {
    alpha_x 0
    alpha_y 0
    beta_x  66.14532014 
    beta_y  17.92472388
}
##############################
# CHECK EMITTANCE BEFORE RUN #
##############################
set match(emitt_x) 6.800
set match(emitt_y) 0.200
set match(charge) 4e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

puts " generating the beam "

set n_total [expr $n_slice*$n] 
source $script_dir/clic_beam.tcl

#make_beam_halo $e0 $match(e_spread) $n_total
make_beam_particles $e0 $match(e_spread) $n_total

make_beam_many beam0 $n_slice $n
BeamRead -file particles.in -beam beam0

FirstOrder 1
puts " start tracking in collimation section "
BeamlineUse -name test.coll
TestNoCorrection -beam beam0 -emitt_file emitt.dat -survey Zero
puts " end tracking in collimation section "
puts " puts bds ffs "

BeamlineNew
source bds.ffs.tcl
set name1 particles.ffs.out
TclCall -script {save_beam $name1} 
BeamlineSet -name test.ffs
# new beam
make_beam_many beam1 $n_slice $n
#make_beam_many beam1 1 1 
BeamRead -file $name0 -beam beam1
BeamlineUse -name test.ffs
TestNoCorrection -beam beam1 -emitt_file emitt.dat -survey Zero

set solmap ildantinobuck.txt

if { $n_total == 1} { set writefirst 1 } else { set writefirst 0 }

# Track beam through without solenoids..
BeamlineNew
source bds.ffs.last.tcl
set name2 particles.ffs.last.out
TclCall -script {save_beam $name2} 
BeamlineSet -name test.ffs.last

puts " TestIntRegion forwards.. "
make_beam_many beam2 $n_slice $n
BeamRead -file $name1 -beam beam2
BeamlineUse -name test.ffs.last
set t_0 [clock seconds]
# TestNoCorrection -beam beam2 -emitt_file emitt.dat -survey Zero
TestIntRegion -beam beam2 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 0 -writefirst $writefirst
if { $writefirst } { exec mv singtrk.dat singtrk.1.dat }
set name_ideal particle.dist.no_solenoid.out
exec mv $name2 $name_ideal

puts " TestIntRegion forwards.. "
make_beam_many beam5 $n_slice $n
BeamRead -file $name1 -beam beam5
set t_1 [clock seconds]
TestIntRegion -beam beam5 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 1 -writefirst $writefirst
if { $writefirst } { exec mv singtrk.dat singtrk.4.dat }
set name_synrad particle.dist.only_synrad.out
exec mv $name2 $name_synrad


puts " TestIntRegion forwards single track... "
make_beam_many beam6 1 1
exec echo "1496. 0. 0. 0. 0. 0." > beam.in
BeamRead -file beam.in -beam beam6
TestIntRegion -beam beam6 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 0 -filename $solmap -writefirst $writefirst
if { $writefirst } { exec mv singtrk.dat singtrk.5.dat }
set name_strk particle.dist.strk_ref.out
exec mv $name2 $name_strk

# prepare backward...
set avang 0
set strk [open particle.dist.strk_ref.out r]
gets $strk l
set avang [lindex $l 5]
puts "Vertical angle at IP: $avang"
close $strk
set name_ideal1 particle.dist.no_solenoid_syn.out
exec awk -v ang=$avang {{print $1,$2,$3,$4,$5,$6+ang}} $name_ideal > $name_ideal1 

# track positron beam backwards...
make_beam_many beam3 $n_slice $n
BeamRead -file $name_ideal1 -beam beam3

puts " TestIntRegion backwards.. "
set t_2 [clock seconds]
TestIntRegion -beam beam3 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 0 -filename $solmap -writefirst $writefirst -backward 1
if { $writefirst } { exec mv singtrk.dat singtrk.2.dat }
set name_bef_ffs particle.dist.beforeffs.out
exec mv $name2 $name_bef_ffs


make_beam_many beam4 $n_slice $n
BeamRead -file $name_bef_ffs -beam beam4
puts " TestIntRegion forwards.. "
set t_3 [clock seconds]
TestIntRegion -beam beam4 -emitt_file emitt.dat -survey Zero -angle 0.01 -step $step -synrad 1 -filename $solmap -writefirst $writefirst
if { $writefirst } { exec mv singtrk.dat singtrk.3.dat }
set name_final particle.dist.solenoid_and_synrad.out
set avang [expr -$avang]
exec awk -v ang=$avang {{print $1,$2,$3,$4,$5,$6+ang}} $name2 > $name_final

puts " Final Reference Energy = $e0 "
set t_4 [clock seconds]

puts "\n--------------   TIME   --------------------"
puts "       First forward sim took [expr ($t_1-$t_0)/60.] minutes"
puts "      Second forward sim took [expr ($t_2-$t_1)/60.] minutes"
puts "            Backward sim took [expr ($t_3-$t_2)/60.] minutes"
puts "       Third forward sim took [expr ($t_4-$t_3)/60.] minutes"
puts "Total sim time for irtracking [expr ($t_4-$t_0)/60.] minutes"
puts "--------------------------------------------\n"

if { $n > 499 && $n_slice > 9 } { source guinea.tcl }

