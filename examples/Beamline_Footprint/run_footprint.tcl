proc HCORRECTOR { a b c d e f } { Dipole $a $b $c $d  }
proc VCORRECTOR { a b c d e f } { Dipole $a $b $c $d }
proc Marker { a b } { Drift $a $b }

#
# It shows the three-dimensional footprint of the ILC turnaround
#

source eturnaround.tcl
BeamlineSet

BeamlineSaveFootprint -file eturnaround.dat

exec echo "splot 'eturnaround.dat' u 1:2:3 w lp" | gnuplot -persist
