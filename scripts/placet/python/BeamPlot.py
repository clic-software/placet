#! /usr/bin/env python

#  This file is part of PLACET
#
#  Copyright (C) 2013 Jochem Snuverink <jochem.snuverink@rhul.ac.uk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file COPYING.LIB.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

import matplotlib as mpl
#mpl.use('Agg')
import pylab as pl
import numpy as np

# default plotting
mpl.rc('axes', grid=True)
mpl.rc('legend',numpoints=1,fontsize=14)

class BeamPlot :
    '''
    Class for plotting the beam from a text file

    Example
    Python {
    import BeamPlot
    plotter=BeamPlot.BeamPlot()
    plotter.allPlots()
    }

    if used within PLACET use a non-interactive backend like Agg
    http://matplotlib.org/faq/usage_faq
    mpl.use('Agg')
    '''
    def __init__(self, directory="./", fileName="electron.ini", savePlots=False) :
        '''
        Input options
        fileName : name of beam file
        savePlots: save plots
        directory: directory in which to save the plots
        '''
        self.fileName  = fileName
        self.savePlots = savePlots
        self.dir = directory

        self.E   = []
        self.x   = []
        self.y   = []
        self.z   = []
        self.xp  = []
        self.yp  = []

        self._readFile()



    def _readFile(self) :
        '''
        Open beam file and read data
        '''
        f = open(self.dir+self.fileName)

        for l in f :
            t = l.split()
            if t [0] =='#' :
                continue
            d1    = float(t[0])
            d2    = float(t[1])
            d3    = float(t[2])
            d4    = float(t[3])
            d5    = float(t[4])
            d6    = float(t[5])

            self.E.append(d1)
            self.x.append(d2)
            self.y.append(d3)
            self.z.append(d4)
            self.xp.append(d5)
            self.yp.append(d6)

        self.E  = pl.array(self.E)
        self.x  = pl.array(self.x)
        self.y  = pl.array(self.y)
        self.z  = pl.array(self.z)
        self.xp = pl.array(self.xp)
        self.yp = pl.array(self.yp)

    def allPlots(self):
        '''
        Create all plots
        '''
        self.controlPlot()
        self.twoDimPlot()
        self.twoDimHist()
        self.twoDimPlotHist()
        self.twoDimContourMany()

    def controlPlot(self) :
        '''
        Control Plots
        '''
        self.oneDimPlot()
        self.oneDimHist()

    def oneDimPlot(self, plotname="placet_1d.png") :
        '''
        Simple 1-d distributions
        '''
        pl.figure(1, figsize=(10, 12))
        pl.clf()
        pl.subplot(3,2,1)
        pl.plot(self.E)
        pl.xlabel('particle number')
        pl.ylabel('$E$ [GeV]')
        pl.subplot(3,2,2)
        pl.plot(self.z)
        pl.xlabel('particle number')
        pl.ylabel('$z$ [m]')
        pl.subplot(3,2,3)
        pl.plot(self.x)
        pl.xlabel('particle number')
        pl.ylabel('$x$ [$\mu$m]')
        pl.subplot(3,2,4)
        pl.plot(self.y)
        pl.xlabel('particle number')
        pl.ylabel('$y$ [$\mu$m]')
        pl.subplot(3,2,5)
        pl.plot(self.xp)
        pl.xlabel('particle number')
        pl.ylabel('$xp$ [$\mu$rad]')
        pl.subplot(3,2,6)
        pl.plot(self.yp)
        pl.xlabel('particle number')
        pl.ylabel('$yp$ [$\mu$rad]')

        pl.subplots_adjust(wspace=0.25,hspace=0.25)
        if self.savePlots:
            pl.savefig(self.dir+plotname)

    def oneDimHist(self, plotname="placet_1dhist.png"):
        '''
        1-d histogram plots
        '''
        pl.figure(2, figsize=(10, 12))
        pl.clf()
        pl.subplot(3,2,1)
        pl.hist(self.E)
        pl.xlabel('$E$ [GeV]')
        pl.subplot(3,2,2)
        pl.hist(self.z)
        pl.xlabel('$z$ [m]')
        pl.subplot(3,2,3)
        pl.hist(self.x)
        pl.xlabel('$x$ [$\mu$m]')
        pl.subplot(3,2,4)
        pl.hist(self.y)
        pl.xlabel('$y$ [$\mu$m]')
        pl.subplot(3,2,5)
        pl.hist(self.xp)
        pl.xlabel('$xp$ [$\mu$rad]')
        pl.subplot(3,2,6)
        pl.hist(self.yp)
        pl.xlabel('$yp$ [$\mu$rad]')

        pl.subplots_adjust(wspace=0.25,hspace=0.25)
        if self.savePlots:
            pl.savefig(self.dir+plotname)

    def twoDimPlot(self, plotname="placet_2d.png"):
        '''
        2-d scatter plots
        '''
        pl.figure(3, figsize=(10, 12))
        pl.clf()
        
        pl.subplot(2,2,1)
        pl.plot(self.x,self.y,'.')
        pl.xlabel('$x$ [$\mu$m]')
        pl.ylabel('$y$ [$\mu$m]')

        pl.subplot(2,2,2)
        pl.plot(self.xp,self.yp,'.')
        pl.xlabel('$xp$ [$\mu$rad]')
        pl.ylabel('$yp$ [$\mu$rad]')

        pl.subplot(2,2,3)
        pl.plot(self.x,self.xp,'.')
        pl.xlabel('$x$ [$\mu$m]')
        pl.ylabel('$xp$ [$\mu$rad]')

        pl.subplot(2,2,4)
        pl.plot(self.y,self.yp,'.')
        pl.xlabel('$y$ [$\mu$m]')
        pl.ylabel('$yp$ [$\mu$rad]')

        pl.subplots_adjust(wspace=0.25,hspace=0.25)
        if self.savePlots:
            pl.savefig(self.dir+plotname)

    def twoDimHist(self, plotname="placet_2dhist.png"):
        '''
        2-d histogram plots
        '''
        # 2 hist
        pl.figure(4, figsize=(10, 12))
        pl.clf()

        pl.subplot(2,2,1)
        pl.hist2d(self.x, self.y, (50,50)) #, cmap=cm.jet)
        pl.xlabel('$x$ [$\mu$m]')
        pl.ylabel('$y$ [$\mu$m]')
        pl.colorbar()

        pl.subplot(2,2,2)
        pl.hist2d(self.xp,self.yp, (50,50))
        pl.xlabel('$xp$ [$\mu$rad]')
        pl.ylabel('$yp$ [$\mu$rad]')
        pl.colorbar()

        pl.subplot(2,2,3)
        pl.hist2d(self.x,self.xp, (50,50))
        pl.xlabel('$x$ [$\mu$rad]')
        pl.ylabel('$xp$ [$\mu$rad]')
        pl.colorbar()

        pl.subplot(2,2,4)
        pl.hist2d(self.y,self.yp, (50,50))
        pl.xlabel('$y$ [$\mu$m]')
        pl.ylabel('$yp$ [$\mu$rad]')
        pl.colorbar()

        pl.subplots_adjust(wspace=0.25,hspace=0.25)
        if self.savePlots:
            pl.savefig(self.dir+plotname)
        
    def twoDimPlotHist(self, plotname="placet_2dplothist", extension=".png"):
        '''
        2-d scatter plots with histograms
        plots in style of: http://matplotlib.org/examples/pylab_examples/scatter_hist.html
        '''
        # definitions for the axes
        left, width = 0.13, 0.55
        bottom, height = 0.1, 0.55
        left_h = left+width+0.07
        bottom_h = bottom+height+0.07
        height_h = 0.23
        width_h = 0.2
        
        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom_h, width, height_h]
        rect_histy = [left_h, bottom, width_h, height]

        pl.figure(5, figsize=(8,8))
        pl.clf()

        # start with a rectangular Figure
        axScatter = pl.axes(rect_scatter)
        pl.xlabel('$x$ [$\mu$m]')
        pl.ylabel('$y$ [$\mu$m]')

        axHistx = pl.axes(rect_histx)
        axHisty = pl.axes(rect_histy)

        # the scatter plot:
        axScatter.plot(self.x, self.y,'.')

        # now determine nice limits by hand:
        #binwidth = 0.25
        #xymax = max( [max(np.fabs(self.x)), max(np.fabs(self.y))] )
        #lim = ( int(xymax/binwidth) + 1) * binwidth
        
        #axScatter.set_xlim( (-lim, lim) )
        #axScatter.set_ylim( (-lim, lim) )

        #bins = np.arange(-lim, lim + binwidth, binwidth)
        axHistx.hist(self.x)
        axHisty.hist(self.y, orientation='horizontal')

        #axHistx.set_xlim( axScatter.get_xlim() )
        #axHisty.set_ylim( axScatter.get_ylim() )
        if self.savePlots:
            pl.savefig(self.dir+plotname+"xy"+extension)
        
        pl.figure(6, figsize=(8,8))
        pl.clf()
        axScatter = pl.axes(rect_scatter)
        pl.xlabel('$xp$ [$\mu$rad]')
        pl.ylabel('$yp$ [$\mu$rad]')
        axHistx = pl.axes(rect_histx)
        axHisty = pl.axes(rect_histy)
        axScatter.plot(self.xp, self.yp,'.')
        axHistx.hist(self.xp)
        axHisty.hist(self.yp, orientation='horizontal')
        if self.savePlots:
            pl.savefig(self.dir+plotname+"xpyp"+extension)

        pl.figure(7, figsize=(8,8))
        pl.clf()
        axScatter = pl.axes(rect_scatter)
        pl.xlabel('$x$ [$\mu$m]')
        pl.ylabel('$xp$ [$\mu$rad]')
        axHistx = pl.axes(rect_histx)
        axHisty = pl.axes(rect_histy)
        axScatter.plot(self.x, self.xp,'.')
        axHistx.hist(self.x)
        axHisty.hist(self.xp, orientation='horizontal')
        if self.savePlots:
            pl.savefig(self.dir+plotname+"xxp"+extension)

        pl.figure(8, figsize=(8,8))
        pl.clf()
        axScatter = pl.axes(rect_scatter)
        pl.xlabel('$y$ [$\mu$m]')
        pl.ylabel('$yp$ [$\mu$rad]')
        axHistx = pl.axes(rect_histx)
        axHisty = pl.axes(rect_histy)
        axScatter.plot(self.y, self.yp,'.')
        axHistx.hist(self.y)
        axHisty.hist(self.yp, orientation='horizontal')
        if self.savePlots:
            pl.savefig(self.dir+plotname+"yyp"+extension)

    def twoDimContourMany(self,
                         horizontal=True,
                         vertical=True,
                         longitudinal=True,
                         plotname="placet_2dcontour",
                         extension=".png"):
        '''
        Plot a page of multiple contour plots (phase space)

        :param horizontal: Include x,x'
        :param vertical: Include y,y'
        :param longitudinal: Include E,z

        '''
        # figure out how many plots we need..
        ar=np.array([horizontal,vertical,longitudinal])
        # number of parameters (2 per axis)
        n_axes=len(ar[ar])
        # in order to not have the labels above each other, 
        # need large figures:
        figure=pl.figure(9, figsize=(7*n_axes,7*n_axes))

        # plot counter
        n_plot=1
        if horizontal and vertical:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.x,self.y,"x [um]", "y [um]",figure=figure)
            n_plot+=1
        if horizontal:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.x,self.xp,"x [um]", "x' [urad]",figure=figure)
            n_plot+=1
        if horizontal and longitudinal:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.x,self.E,"x [um]", "E [GeV]",figure=figure)
            n_plot+=1
        if vertical:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.y,self.yp,"y [um]", "y' [urad]",figure=figure)
            n_plot+=1
        if horizontal and vertical:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.xp,self.yp,"x' [urad]", "y' [urad]",figure=figure)
            n_plot+=1
        if vertical and longitudinal:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.y,self.E,"y [um]", "E [GeV]",figure=figure)
            n_plot+=1
        if horizontal and longitudinal:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.x,self.z,"x [um]", "z [um]",figure=figure)
            n_plot+=1
        if vertical and longitudinal:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.y,self.z,"y [um]", "z [um]",figure=figure)
            n_plot+=1
        if longitudinal:
            pl.subplot(n_axes,n_axes,n_plot)
            self._twoDimContour(self.z,self.E,"z [um]", "E [GeV]",figure=figure)

        if self.savePlots:
            pl.savefig(self.dir+plotname+extension)

    def _twoDimContour(self, x, y, x_label='', y_label='', bins=50, figure=None):
        '''
        2-d contour plot

        Used by twoDimContourMany()
        '''

        if not figure:
            figure=pl.figure()

        # define the "step length" when searching for correct range..
        xmin,xmax=min(x),max(x)
        ymin,ymax=min(y),max(y)
        xdelt,ydelt=(xmax-xmin)*1e-3,(ymax-ymin)*1e-3
        xmax+=xdelt
        ymax+=ydelt

        # define the edges of the plots:
        xlen,ylen=len(x),len(y)
        while len(x[x>xmin])>0.995*xlen:
            xmin+=xdelt
        while len(x[x<xmax])>0.99*xlen:
            xmax-=xdelt
        while len(y[y>ymin])>0.995*ylen:
            ymin+=ydelt
        while len(y[y<ymax])>0.99*ylen:
            ymax-=ydelt

        # bins:
        xr=np.arange(xmin,xmax,1.*(xmax-xmin)/bins)
        yr=np.arange(ymin,ymax,1.*(ymax-ymin)/bins)

        H,xedges,yedges = np.histogram2d(x,y,bins=(xr,yr))

        pl.contour(xedges[1:],yedges[1:],np.transpose(H))
        pl.xlabel(x_label)
        pl.ylabel(y_label)



