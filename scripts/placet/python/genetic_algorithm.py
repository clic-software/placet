
'''
 Library to run GA optimization algorithm for tuning a lattice in PLACET...
 
 All results are stored in ga_data.json (and loaded if the file exists)
 
 In order to use this, you need the DEAP module which can be found at
 http://code.google.com/p/deap/
 
 Usually, you can install it with pip::
 
   pip install deap
'''

import sys,os,json,numpy
import placet_python as placet


def _element_vary_attribute(beamline, indices, attribute, values):
    if len(values)!=len(indices):
        raise ValueError("There need to be one value per index")

    # create a list in case a numpy.array has been given
    # experienced segfault on afs/SLC without this fix
    if isinstance(indices,numpy.ndarray):
        indices=indices.tolist()

    val0=numpy.array(placet.element_get_attribute(beamline, list(indices), attribute))
    placet.element_set_attribute(beamline, list(indices), attribute,list(val0+values))



class ga_optimizer:
    '''
    Class to hold a Genetic Algorithm optimizer

    :param beamline: name of beamline
    :param beam: name of test beam

    :param index_list: list of indices of the elements
    :param variable_list: corresponding list of variables ('x','y', or 'strength')
    :param sigma_list: corresponding std.dev of the movement of each variable
    :param accelerator: name of accelerator (for GUINEA-PIG)
    :param weights: Relative weights of BPM signal (negative), and luminosity (positive)
    :param evaluate_lumi: Use luminosity as an evaluation parameter
    :param evaluate_bpm: Use BPM signals as an evaluation parameter
    '''
    def __init__(self,
                 beamline,
                 beam,
                 index_list,
                 variable_list,
                 sigma_list,
                 n_per_slice=1700,
                 n_slices=50,
                 accelerator='clic',
                 weights=(-1e-4,1e-28),
                 evaluate_lumi=True,
                 evaluate_bpms=True):

        # make sure we have numpy arrays:
        index_list=numpy.array(index_list)
        sigma_list=numpy.array(sigma_list)

        self.beamline=beamline
        self.beam=beam

        # we split the lists in three...
        vlist=numpy.array(variable_list)
        self.mask_x=vlist=='x'
        self.mask_y=vlist=='y'
        self.mask_strength=vlist=='strength'

        self.index_list_x=index_list[self.mask_x]
        self.index_list_y=index_list[self.mask_y]
        self.index_list_strength=index_list[self.mask_strength]

        self.sigma_list_x=sigma_list[self.mask_x]
        self.sigma_list_y=sigma_list[self.mask_y]
        self.sigma_list_strength=sigma_list[self.mask_strength]

        self.any_x=(self.mask_x==True).any()
        self.any_y=(self.mask_y==True).any()
        self.any_strength=(self.mask_strength==True).any()

        # number of knobs..
        self.n_knobs=len(index_list)

        self.accelerator=accelerator

        # The name of the last element:
        self.last_element='IP'

        self.results=[]

        # number of particles:
        self.n_slices=n_slices
        self.n_per_slice=n_per_slice

        # evaluate only bpm signal or only lumi perhaps:
        self.evaluate_bpms=evaluate_bpms
        self.evaluate_lumi=evaluate_lumi

        # by default, we evaluate all bpms:
        self.bpm_s_min=None
        self.bpm_s_max=None

        # GA initialization..

        from deap import base, creator
        # weights are:
        #     sum of bpm signal (x and y)
        #     total luminosity
        creator.create("FitnessMulti", base.Fitness, weights=weights)
        creator.create("Individual", list, fitness=creator.FitnessMulti)

        # Initialization

        from deap import tools

        # number of knobs:
        IND_SIZE = self.n_knobs

        self.toolbox = base.Toolbox()
        # normal random distr:
        self.toolbox.register("attribute", numpy.random.randn)
        self.toolbox.register("individual", tools.initRepeat, creator.Individual,
                        self.toolbox.attribute, n=IND_SIZE)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)

        # default strategies:
        # indpb=0.2
        self.strategy_mate=tools.cxUniform
        # ("mutate", self.strategy_mutate, mu=0, sigma=1, indpb=0.1)
        self.strategy_mutate=tools.mutGaussian
        # ("select", self.strategy_tournament, tournsize=TOURNSIZE)
        self.strategy_tournament=tools.selTournament

        # output filename:
        self.filename='ga_data.json'

        # print verbose info
        self.verbose=True

    def set_num_slices(self, n_slices):
        '''
        Set the number of slices
        '''
        self.n_slices=n_slices

    def set_particles_per_slice(self, n_per_slice):
        '''
        Set the number of particles per slice
        '''
        self.n_per_slice=n_per_slice

    def set_last_element(self, last_element):
        '''
        Set the name of the last element
        '''
        self.last_element=last_element

    def set_verbose(verbose):
        '''
        set to False to reduce the printing to stdout
        '''
        self.verbose=verbose

    def set_bpmrange(self,s_min,s_max):
        '''
        when evaluating bpm signal,
        only consider bpms between s_min
        and s_max
        '''
        self.bpm_s_min=s_min
        self.bpm_s_max=s_max

    def _write_pop(self,population):
        '''
        Append data to the result list
        '''
        d=[]
        for ind in population:
            d.append(dict(values=ind[:],fitness=ind.fitness.values))
        self.results.append(d)

    def _print_iter(self,i):
        if self.verbose:
            print "-"*50
            print " -",' '*44,'- '
        print "  --",' '*12,"New round","%3d" %i,' '*13,"--  "
        if self.verbose:
            print " -",' '*44,'- '
            print "-"*50

    def _set_position(self,knob_set):
        '''
        Move each knob by the values specified by knob_set
        '''

        kset=numpy.array(knob_set)

        if self.any_x:
            pos_x=kset[self.mask_x]*self.sigma_list_x
            _element_vary_attribute(self.beamline, self.index_list_x, 'x', pos_x)
        if self.any_y:
            pos_y=kset[self.mask_x]*self.sigma_list_y
            _element_vary_attribute(self.beamline, self.index_list_y, 'y', pos_y)
        if self.any_strength:
            pos_strength=kset[self.mask_x]*self.sigma_list_strength
            _element_vary_attribute(self.beamline, self.index_list_strength, 'strength', pos_strength)

    def _evaluate(self,knob_set):
        '''
        Evaluate the current set of movements..

        Sum of all bpm readings is first value.
        If beam is lost, all bpm values after lost beam takes that of
        the last BPM with signal.

        If beam passes through, evaluates total luminosity..
        '''

        import guinea

        # Move the elements to test position:
        self._set_position(knob_set)

        # Track beam:
        IP = placet.get_name_number_list(self.beamline, self.last_element)[0]
        E,B=placet.test_no_correction(self.beamline, self.beam, "None",1,0,IP)

        # get bpm readings:
        bpm_meas,bpm_s,bpm_i=placet.get_bpm_readings(self.beamline)
        bpm_x=bpm_meas[:,0]
        bpm_y=bpm_meas[:,1]

        # Reset element positions:
        self._set_position(-numpy.array(knob_set))

        # if beam did not survive until IP, find out where it is lost:
        n_total=self.n_per_slice*self.n_slices

        if len(B)<n_total or not self.evaluate_lumi: # beam was lost

            # find out at which BPM beam was lost..
            max_x=numpy.where(bpm_x==max(bpm_x))[0][0]
            max_y=numpy.where(bpm_y==max(bpm_y))[0][0]
            i_lost=max(max_x,max_y) # beam was lost at "last" position

            # set bpm signal after lost beam to
            # that of the last BPM with signal
            # (to not have lost beam early being benefitial)
            bpm_x[i_lost:]=bpm_x[i_lost]
            bpm_y[i_lost:]=bpm_y[i_lost]

            # no luminosity..
            l_peak,l_tot=0.0,0.0

        else:
            # we calculate the luminosity...
            numpy.savetxt("electron.ini",B)
            l_peak,l_tot = guinea.get_lumi( self.n_slices, self.n_per_slice, distr_file="electron.ini",accelerator=self.accelerator)
            os.remove("electron.ini")


        if self.evaluate_bpms:
            if not self.bpm_s_max==None: # we only want to consider a region of the bpms:
                bpm_mask=(bpm_s[0]>self.bpm_s_min) & (bpm_s[0]<self.bpm_s_max)
                bpm_x=bpm_x[bpm_mask]
                bpm_y=bpm_y[bpm_mask]
            bpm_sum_signal=sum(abs(bpm_x))+sum(abs(bpm_y))
        else:
            bpm_sum_signal=0.0

        if self.verbose:
            print("GA: BPM sum, Total lumi: %.2e, %.2e"% (bpm_sum_signal, l_tot))

        return bpm_sum_signal,l_tot


    def run_ga(self, NPOP, NGEN, TOURNSIZE, XOVER_PROB=0.7, MUT_PROB=0.25):
        '''

        Run an optimization.

        :param NPOP: Population size
        :param NGEN: Number of generations
        :param TOURNSIZE: Tournament size
        :param XOVER_PROB: crossover probability
        :param MUT_PROB: mutation probability
        '''

        # Operators

        self.toolbox.register("mate", self.strategy_mate, indpb=0.2)
        self.toolbox.register("mutate", self.strategy_mutate, mu=0, sigma=1, indpb=0.1)
        self.toolbox.register("select", self.strategy_tournament, tournsize=TOURNSIZE)
        self.toolbox.register("evaluate", self._evaluate)

        print("GA: toolbox initialized")
        import random
        pop = self.toolbox.population(n=NPOP)

        #TODO: set one individual to 0 setting..

        if self.verbose:
            print("GA: initial evaluation")
        # Evaluate the entire population
        fitnesses = map(self.toolbox.evaluate, pop)

        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit
        self._write_pop(pop)

        print("GA: starting iterations")
        for g in range(NGEN):
            self._print_iter(g)

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = map(self.toolbox.clone, offspring)

            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < XOVER_PROB:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values

            for mutant in offspring:
                if random.random() < MUT_PROB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values

            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            # The population is entirely replaced by the offspring
            pop[:] = offspring
            self._write_pop(pop)
            file(self.filename,'w').write(json.dumps(self.results,sort_keys=True,indent=2))

        # The best individual:
        from deap import tools
        best_ind = tools.selBest(pop, 1)[0]
        # Set the beamline to new best position:
        self._set_position(best_ind)

        return pop
