# R = response matrix
# b = bpm readings
# l = number of desired correctors
# returns : P (selected) correctors 
#           X correction

function [P,X] = MICADO(R,b,l)
  l = min(l,columns(R));
  P = 1:columns(R);
  X = [];
  for j=1:l
    r2_min = inf;
    for corrector_index=j:columns(R)
      R_j = R(:, [1:(j-1), corrector_index ]);
      x_j = - R_j \ b;
      r = b + R_j * x_j;
      r2 = dot(r,r);
      if r2 < r2_min
	r2_min = r2;
	best_corrector_index = corrector_index;
        X = x_j;
      end
    end
    if best_corrector_index != j
      tmp = R(:,best_corrector_index);
      R(:,best_corrector_index) = R(:,j);
      R(:,j) = tmp;
      tmp = P(best_corrector_index);
      P(best_corrector_index) = P(j);
      P(j) = tmp;
    end
  end
  P = P(1:l);
endfunction
