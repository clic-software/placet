#
# figure of merit for placet_optimize()
#
function merit=__merit_function_constraint__(x, beamline, user_function, correctors, leverages, constraints)
  for i=1:length(correctors)
    a = min(constraints(i,:));
    b = max(constraints(i,:));
    if a != b
      x(i) = sigmoid(x(i), a, b);
    endif
  endfor
  if rows(leverages)==1
    placet_element_set_attribute(beamline, correctors(i), leverages, x);
  else
    for i=1:length(correctors)
      placet_element_set_attribute(beamline, correctors(i), deblank(leverages(i,:)), x(i));
    end
  endif
  merit=feval(user_function, beamline)
end
