## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_response_matrix (@var{beamline}, @var{beam}, @var{bpms}, @var{correctors}, @var{survey})
## Return the response matrix for 'beamline' and 'beam0', using selected 'bpms' and 'correctors'.
## @end deftypefn

function [R,b0]=placet_get_response_matrix(beamline, beam, B, C, survey)
  if nargin==5 && ischar(beamline) && ischar(beam) && isvector(B) && isvector(C) && ischar(survey)
    Res=placet_element_get_attribute(beamline, B, "resolution");
    placet_element_set_attribute(beamline, B, "resolution", 0.0);
    placet_test_no_correction(beamline, beam, survey);
    b0=placet_get_bpm_readings(beamline, B);
    b0=b0(:,2);
    R=zeros(size(b0,1), size(C,2));
    for i=1:size(C,2)
      placet_vary_corrector(beamline, C(i), [0,  1]);
      placet_test_no_correction(beamline, beam, "None");
      placet_vary_corrector(beamline, C(i), [0, -1]);
      b1=placet_get_bpm_readings(beamline, B);
      b1=b1(:,2);
      R(:,i)=(b1-b0);
    endfor
    placet_element_set_attribute(beamline, B, "resolution", Res);
  else  
    help placet_get_response_matrix
  endif
endfunction
