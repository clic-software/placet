#
# figure of merit for placet_optimize()
#
function merit=__merit_function__(x, beamline, user_function, correctors, leverages, initial_step, initial_state)
  if rows(leverages) == 1
    for i=1:length(correctors)
      placet_element_set_attribute(beamline, correctors(i), leverages, initial_state(i) + x(i) * initial_step(i));
    end
  else
    for i=1:length(correctors)
      placet_element_set_attribute(beamline, correctors(i), deblank(leverages(i,:)), initial_state(i) + x(i) * initial_step(i));
    end
  endif
  merit=feval(user_function, beamline)
end
