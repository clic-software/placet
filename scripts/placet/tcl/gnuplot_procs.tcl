#Gnuplot procedures to have access to graph and canvas
#coordinates from the mouse and provide graph zooming.
#
#                                            October 2005, 
#                                            Rogelio Tomas
#
#You only need to use:
#       my_gnuplot <graph> <plot_procedure> <namespace>
#which firstly runs the user defined <plot_procedure>
#and then attaches the plot to the canvas <graph>.
#<namespace> is the corresonding namespace where the
#functions have been defined.
#
#For zooming:
#Click left button and drag for zooming an area of the plot.
#Click middle button to restore initial plot.
#
#See an example in twiss.tcl


#Gnuplot required proc:
proc user_gnuplot_coordinates {win id x1s y1s x2s y2s x1e y1e x2e y2e x1m y1m x2m y2m} {
    puts "$win $id $x1s $y1s $x2s $y2s $x1e $y1e $x2e $y2e $x1m $y1m $x2m $y2m"
}

#Do the plot, attach the plot, assign the border cooridinates
#to the plot and update
proc my_gnuplot { the_graph the_command the_namespace} {
     $the_command
     ${the_namespace}::gnuplot $the_graph
     assign_borders $the_graph $the_command $the_namespace
     update
}

#Relating graph and canvas coordinates
proc assign_borders {graph sourceorigin the_namespace} {
        
    global plotarea axisranges x1 y1 g1
# There is a scale factor that has to be firstly computed (see sextupoles.tk for instance)
    set cm(x) [expr [winfo width $graph]-2*[$graph cget -border]-2*[$graph cget -highlightthickness]]
    if {$cm(x) <= 1} {set cm(x) [$graph cget -width]}
    set cm(y) [expr [winfo height $graph]-2*[$graph cget -border]-2*[$graph cget -highlightthickness]]
    if {$cm(y) <= 1} {set cm(y) [$graph cget -height]}
    set xy(1) x
    set xy(2) x
    set xy(3) y
    set xy(4) y

    set ii 1

#    puts [gnuplot_plotarea]
#    puts [gnuplot_axisranges]

    foreach xx [split [${the_namespace}::gnuplot_plotarea]] {
#xleft, xright, ytop, ybot in canvas screen coordinates
	set plotarea($graph.$ii) [expr $cm($xy($ii))*$xx/1000]
	set ii [expr $ii+1]
    }
    set ii 1
    foreach xx [split [${the_namespace}::gnuplot_axisranges]] { 
#x1min, x1max, y1min, y1max, x2min, x2max, y2min, y2max in plot coordinates
    	set axisranges($graph.$ii) $xx
	set ii [expr $ii+1]
    }
# ALso do the mouse binding here:
   bind $graph <ButtonPress-2> "\
       set ${the_namespace}::range($graph) *:*
       set ${the_namespace}::range($graph.x) *:*
      my_gnuplot $graph $sourceorigin $the_namespace
   "
   bind $graph <ButtonPress-1> "\
       set x1 %x
       set y1 %y
       set g1 $graph
       $graph create oval \[expr %x-5\] \[expr %y-5\] %x  %y -fill yellow -outline blue 
    "

   bind $graph <ButtonRelease-1> "\
       set x2 %x
       set y2 %y
      set g2 $graph
      if { \$x1 != \$x2  && \$y1 != \$y2} {
          if {\$y2 < \$y1} {set tt \$y1
                     set y1 \$y2
                     set y2 \$tt}
          if {\$x2 < \$x1} {set tt \$x1
                     set x1 \$x2
                     set x2 \$tt}
          set factor \[expr ($axisranges($graph.4)-$axisranges($graph.3))/($plotarea($graph.3)-$plotarea($graph.4))\]
          set ${the_namespace}::range($graph) \[expr $axisranges($graph.3)+(\$y2-$plotarea($graph.4))*\$factor\]:\[expr $axisranges($graph.3)+(\$y1-$plotarea($graph.4))*\$factor \]
          set factor \[expr ($axisranges($graph.2)-$axisranges($graph.1))/($plotarea($graph.2)-$plotarea($graph.1))\]
          set  ${the_namespace}::range($graph.x) \[expr $axisranges($graph.1)+(\$x1-$plotarea($graph.1))*\$factor\]:\[expr $axisranges($graph.1)+(\$x2-$plotarea($graph.1))*\$factor \]

          my_gnuplot $graph $sourceorigin $the_namespace

  } else {
         set factor \[expr ($axisranges($graph.4)-$axisranges($graph.3))/($plotarea($graph.3)-$plotarea($graph.4))\]
         set ycoor \[expr $axisranges($graph.3)+(\$y1-$plotarea($graph.4))*\$factor\]
         set factor \[expr ($axisranges($graph.2)-$axisranges($graph.1))/($plotarea($graph.2)-$plotarea($graph.1))\]
         set xcoor \[expr $axisranges($graph.1)+(\$x1-$plotarea($graph.1))*\$factor\]
        puts \"Graph coordinates:\$xcoor, \$ycoor   Canvas coordinates:\$x1, \$y1\"
     }
  "
}
