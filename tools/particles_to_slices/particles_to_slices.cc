#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <list>

using namespace std;

struct PARTICLE {
  
  double x, xp, y, yp, z, energy;
	
};

static istream &operator >> (istream &stream, PARTICLE &p )
{
  return stream >> p.energy >> p.x >> p.y >> p.z >> p.xp >> p.yp;
}

int main(int argc, char **argv )
{
  if (argc>=4) {
    int nbunches = atoi(argv[1]);
    int nslices = atoi(argv[2]);
    int nmacroparticles = atoi(argv[3]);
    
    PARTICLE *beam;
    size_t    beam_size;
    {
      list<PARTICLE> plist;
      PARTICLE particle;			
      while (cin >> particle)	plist.push_back(particle);
      beam = new PARTICLE[beam_size = plist.size()];
      copy(plist.begin(), plist.end(), beam);
    }		
    
    double *ch,*x,*xp,*y,*yp,*z, *e;
    double *emin,*emax;
    double *corr;
    
    emin=new double[nslices];
    emax=new double[nslices];
    x=new double[nmacroparticles*nslices];
    xp=new double[nmacroparticles*nslices];
    y=new double[nmacroparticles*nslices];
    yp=new double[nmacroparticles*nslices];
    z=new double[nmacroparticles*nslices];
    e=new double[nmacroparticles*nslices];
    ch=new double[nmacroparticles*nslices];
    corr=new double[nmacroparticles*nslices*10];
    
    int particles_per_bunch=beam_size/nbunches;
    
    for (int ib=0;ib<nbunches;++ib) {
      int m=ib*particles_per_bunch;
      double zmin;
      double zmax;
      {
	double zmean=0;
	for (int i=0;i<particles_per_bunch;++i) {
	  zmean+=beam[m++].z;
	}
	zmean/=particles_per_bunch;
	m=ib*particles_per_bunch;
	double tmp1=0;
	double tmp2=0;
	for (int i=0;i<particles_per_bunch;++i) {
	  double tmp3=beam[m++].z-zmean;
	  tmp1+=tmp3*tmp3;
	  tmp2+=tmp3;
	}
	double zrms=sqrt((tmp1-tmp2*tmp2/particles_per_bunch)/(particles_per_bunch-1));
	zmin=zmean-3*zrms;
	zmax=zmean+3*zrms;
      }
      
      double dzi=(zmax-zmin)/nslices;
      for (int j=0;j<nslices;++j) {
	emin[j]= std::numeric_limits<double>::infinity();
	emax[j]=-std::numeric_limits<double>::infinity();
      }
      
      m=ib*particles_per_bunch;
      for (int i=0;i<particles_per_bunch;++i) {
	PARTICLE &particle=beam[m];
	int j=(int)((particle.z-zmin)/dzi);
	if ((j>=0)&&(j<nslices)) {
	  if (particle.energy<emin[j]) emin[j]=particle.energy;
	  if (particle.energy>emax[j]) emax[j]=particle.energy;
	}
	m++;
      }
      
      for (int j=0;j<nmacroparticles*nslices;++j) {
	ch[j]=0.0;
	e[j]=0.0;
	x[j]=0.0;
	xp[j]=0.0;
	y[j]=0.0;
	yp[j]=0.0;
	for (int i=0;i<10;++i) {
	  corr[j*10+i]=0.0;
	}
      }
      
      m=ib*particles_per_bunch;
      for (int i=0;i<particles_per_bunch;++i) {
	PARTICLE &particle=beam[m];
	int j=(int)((particle.z-zmin)/dzi);
	if ((j>=0)&&(j<nslices)) {
	  int k=(int)(nmacroparticles*(particle.energy-emin[j])/(emax[j]-emin[j]));
	  if (k==nmacroparticles) k=nmacroparticles-1;
	  ch[j*nmacroparticles+k]+=1;
	  x[j*nmacroparticles+k]+=particle.x;
	  xp[j*nmacroparticles+k]+=particle.xp;
	  y[j*nmacroparticles+k]+=particle.y;
	  yp[j*nmacroparticles+k]+=particle.yp;
	  e[j*nmacroparticles+k]+=particle.energy;
	}
	m++;
      }
      
      for (int j=0;j<nmacroparticles*nslices;++j) {
	if (ch[j]!=0) {
	  x[j]/=ch[j];
	  xp[j]/=ch[j];
	  y[j]/=ch[j];
	  yp[j]/=ch[j];
	  e[j]/=ch[j];
	} else {
	  e[j]=15.0;
	}
      }
      
      m=ib*particles_per_bunch;
      for (int i=0;i<particles_per_bunch;++i) {
	PARTICLE &particle=beam[m];
	int j=(int)((particle.z-zmin)/dzi);
	if ((j>=0)&&(j<nslices)) {
	  int k=(int)(nmacroparticles*(particle.energy-emin[j])/(emax[j]-emin[j]));
	  if (k==nmacroparticles) k=nmacroparticles-1;
	  double x1,xp1,y1,yp1;
	  x1=particle.x-x[j*nmacroparticles+k];
	  xp1=particle.xp-xp[j*nmacroparticles+k];
	  y1=particle.y-y[j*nmacroparticles+k];
	  yp1=particle.yp-yp[j*nmacroparticles+k];
	  corr[(j*nmacroparticles+k)*10+0]+=x1*x1;
	  corr[(j*nmacroparticles+k)*10+1]+=x1*xp1;
	  corr[(j*nmacroparticles+k)*10+2]+=x1*y1;
	  corr[(j*nmacroparticles+k)*10+3]+=x1*yp1;
	  corr[(j*nmacroparticles+k)*10+4]+=xp1*xp1;
	  corr[(j*nmacroparticles+k)*10+5]+=xp1*y1;
	  corr[(j*nmacroparticles+k)*10+6]+=xp1*yp1;
	  corr[(j*nmacroparticles+k)*10+7]+=y1*y1;
	  corr[(j*nmacroparticles+k)*10+8]+=y1*yp1;
	  corr[(j*nmacroparticles+k)*10+9]+=yp1*yp1;
	}
	m++;
      }
      
      for (int j=0;j<nmacroparticles*nslices;++j) {
	if (ch[j]!=0) {
	  corr[j*10+0]/=ch[j];
	  corr[j*10+1]/=ch[j];
	  corr[j*10+2]/=ch[j];
	  corr[j*10+3]/=ch[j];
	  corr[j*10+4]/=ch[j];
	  corr[j*10+5]/=ch[j];
	  corr[j*10+6]/=ch[j];
	  corr[j*10+7]/=ch[j];
	  corr[j*10+8]/=ch[j];
	  corr[j*10+9]/=ch[j];
	}
      }
      
      if (argc>=5) zmin=-(zmax-zmin)/2;		
      for (int i=0;i<nmacroparticles*nslices;++i) {
	fprintf(stdout,"%.15g %.15g %.15g ",	zmin+(2*(i/nmacroparticles)+1)*dzi/2,
		ch[i]/particles_per_bunch,
		e[i]);
	fprintf(stdout,"%.15g %.15g ",		x[i],
		xp[i]);
	fprintf(stdout,"%.15g %.15g ",		y[i],
		yp[i]);
	fprintf(stdout,"%.15g %.15g %.15g ",	corr[i*10+0],
		corr[i*10+1],
		corr[i*10+4]);
	fprintf(stdout,"%.15g %.15g %.15g",	corr[i*10+7],
		corr[i*10+8],
		corr[i*10+9]);
	fprintf(stdout," %.15g %.15g %.15g %.15g",corr[i*10+2],
		corr[i*10+3],
		corr[i*10+5],
		corr[i*10+6]);
	fprintf(stdout,"\n");
      }
    }	
    
    delete []emin;
    delete []emax;
    delete []x;
    delete []xp;
    delete []y;
    delete []yp;
    delete []z;
    delete []e;
    delete []ch;
    delete []corr;
    
    delete []beam;
    
  } else cerr << "usage:\tparticles_to_slices NBUNCHES NSLICES NMACROPARTICLES [CENTERZ=0]\n\n";
  
  return 0;
}
