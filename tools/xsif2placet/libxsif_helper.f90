
 INTEGER*4 FUNCTION Get_Item( i )
 USE XSIF_Elements, ONLY : Item
 INTEGER*4, INTENT(IN) :: i

    Get_Item= Item(i)

 END FUNCTION
 
 INTEGER*4 FUNCTION Get_Ietyp( i )
 USE XSIF_Elements, ONLY : Ietyp
 INTEGER*4, INTENT(IN) :: i

    Get_Ietyp= Ietyp(i)

 END FUNCTION

 INTEGER*4 FUNCTION Get_Npos1()
 USE XSIF_Elements, ONLY : Npos1

    Get_Npos1= NPos1

 END FUNCTION

 INTEGER*4 FUNCTION Get_Npos2()
 USE XSIF_Elements, ONLY : Npos2

    Get_Npos2= NPos2

 END FUNCTION

 INTEGER*4 FUNCTION Get_Ietype( i )
 USE XSIF_Elements, ONLY : Ietyp, Item
 INTEGER*4, INTENT(IN) :: i

    Get_Ietype= Ietyp(Item(i))

 END FUNCTION

 REAL*4 FUNCTION Get_Iedat( i, j )
 USE XSIF_Elements, ONLY : Iedat, Item, Pdata
 INTEGER*4, INTENT(IN) :: i, j

   Get_Iedat= Pdata(Iedat(Item(i),1)+j)

 END FUNCTION

 SUBROUTINE Get_Kelabl( i, lab )
 USE XSIF_Elements, ONLY : Kelem, Item
 INTEGER*4, INTENT(IN) :: i
 character(len= *), intent(out) :: lab

 integer :: l
   lab= kelem(Item(i))
   l= 1+len_trim(lab)
   lab(l:l)= char(0)

 END subroutine
