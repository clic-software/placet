#ifndef element_ids_hh
#define element_ids_hh

namespace ELEMENT_ID {
  enum {
    QUADRUPOLE,
    MULTIPOLE,
    DIPOLE,
    SBEND,
    DRIFT,
    BPM,
    SPECIAL
  };
}

#endif /* element_ids_hh */
