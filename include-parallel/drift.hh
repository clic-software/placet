#ifndef drift_hh
#define drift_hh

#include <limits>

#include "element.hh"

class Drift : public Element {
public:

  explicit Drift(const std::string &name, double length=0.0 ) : Element(name, length) {}
  explicit Drift(double length=0.0 ) : Element(length) {}

  int get_id() const { return ELEMENT_ID::DRIFT; }

  void transport(std::vector<Particle> &beam ) const
  {
    if (length < std::numeric_limits<double>::epsilon()) return;
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      particle.x += length*particle.xp;
      particle.y += length*particle.yp;
    }
  }

  void transport_synrad (std::vector<Particle> &beam ) const
  {
    transport(beam);
  }

};

#endif /* drift_hh */
