#if defined(SWIGOCTAVE)

// const vector of indexes as input argument
%typemap(in) const std::vector<size_t> & {
%#if SWIG_OCTAVE_PREREQ(6,0,0)
  if ($input.is_real_matrix() || $input.is_real_scalar() || $input.is_range())
%#else
  if ($input.is_real_type())
%#endif
  {
%#if SWIG_OCTAVE_PREREQ(6,0,0)
    if (($1 = new std::vector<size_t>($input.numel())))
%#else
    if (($1 = new std::vector<size_t>($input.length())))
%#endif
    {
      // In Octave, we have decided to treat size_t as doubles instead of int32
      // Array<int> array = $input.int_vector_value();
      Matrix array = $input.matrix_value();
%#if SWIG_OCTAVE_PREREQ(6,0,0)
      for (int i=0; i<array.numel(); i++)
        (*$1)[i] = size_t(array(i));
%#else
      for (int i=0; i<array.length(); i++)
        (*$1)[i] = size_t(array(i));
%#endif
    }
  }
}
   
%typemap(freearg) const std::vector<size_t> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<size_t> & {
  octave_value obj = $input;
%#if SWIG_OCTAVE_PREREQ(6,0,0)
  $1 = obj.is_real_matrix() || obj.is_real_scalar() || obj.is_range();
%#else
  $1 = obj.is_real_type();
%#endif
}

%typemap(argout,noblock=1) const std::vector<size_t> & {
}

// vector of indexes as input argument
%typemap(in) std::vector<size_t> {
  
%#if SWIG_OCTAVE_PREREQ(6,0,0)
  if ($input.is_real_matrix() || $input.is_real_scalar() || $input.is_range())
%#else
  if ($input.is_real_type())
%#endif
  {
%#if SWIG_OCTAVE_PREREQ(6,0,0)
    $1 = std::vector<size_t>($input.numel());
%#else
    $1 = std::vector<size_t>($input.length());
%#endif
   // In Octave, we have decided to treat size_t as doubles instead of int32
    // Array<int> array = $input.int_vector_value();
    Matrix array = $input.matrix_value();
%#if SWIG_OCTAVE_PREREQ(6,0,0)
   for (int i=0; i<array.numel(); i++)
      ($1)[i] = size_t(array(i));
%#else
   for (int i=0; i<array.length(); i++)
      ($1)[i] = size_t(array(i));
%#endif
  }
}

%typemap(freearg) std::vector<size_t> {
}

%typemap(typecheck) std::vector<size_t> {
  octave_value obj = $input;
%#if SWIG_OCTAVE_PREREQ(6,0,0)
  $1 = obj.is_real_matrix() || obj.is_real_scalar() || obj.is_range();
%#else
  $1 = obj.is_real_type();
%#endif
}

%typemap(argout,noblock=1) std::vector<size_t> {
}

// vector of size_t as output argument
%typemap(in, numinputs=0) std::vector<size_t> & (std::vector<size_t> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<size_t> & {
  dim_vector dv;
  dv.resize(2);
  dv.elem(0)=1;
  dv.elem(1)=$1->size();
  // In Octave, we have decided to treat size_t as doubles instead of int32
  // uint32NDArray res(dv);
  Matrix res(dv);
  for (size_t i=0; i<$1->size(); i++)
    res(i) = double((*$1)[i]);
  $result->append(res);
}

%typemap(freearg,noblock=1) std::vector<size_t> & {
}

#endif
