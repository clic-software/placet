#if defined(SWIGOCTAVE)

// Vector of strings as input argument
%typemap(in) const std::vector<std::string> & {
  if ($input.is_string()) {
    if (($1 = new std::vector<std::string>(1))) {
      (*$1)[0] = $input.string_value();
    }
%#if SWIG_OCTAVE_PREREQ(6,0,0)
  } else if ($input.iscell()) {
    if (($1 = new std::vector<std::string>($input.numel()))) {
      string_vector array = $input.string_vector_value();
      for (int i=0; i<array.numel(); i++)
        (*$1)[i] = array(i);
    }
%#else
  } else if ($input.is_cell()) {
    if (($1 = new std::vector<std::string>($input.length()))) {
      string_vector array = $input.all_strings();
      for (int i=0; i<array.length(); i++)
        (*$1)[i] = array(i);
    }
%#endif
  }
}

%typemap(freearg) const std::vector<std::string> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<std::string> & {
  octave_value obj = $input;
%#if SWIG_OCTAVE_PREREQ(6,0,0)
  $1 = obj.is_string() || obj.iscell();
%#else
  $1 = obj.is_string() || obj.is_cell();
%#endif
}

%typemap(argout,noblock=1) const std::vector<std::string> & {
}

// Vector of strings as output argument
%typemap(in, numinputs=0) std::vector<std::string> & (std::vector<std::string> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<std::string> & {
  std::vector<std::string> &strings = *$1;
  string_vector ret(strings.size()); 
  for (size_t i=0; i<strings.size(); i++)
    ret(i) = strings[i];
  $result->append(ret);
}

%typemap(freearg,noblock=1) std::vector<std::string> & {
}

#endif
