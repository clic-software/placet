#include "file_buffered_stream.hh"
#include "placet_api_server.hh"

static File_Buffered_OStream OSTREAM;
static File_IStream ISTREAM;

int init(int ifiledes, int ofiledes )
{
  ISTREAM.open(ifiledes);
  OSTREAM.open(ofiledes);
  return ISTREAM && OSTREAM ? 0 : 1;
}

int finalize()
{
  if (ISTREAM)
    ISTREAM.close();
  if (OSTREAM)
    OSTREAM.close();
  return 0;
}

void placet_test_no_correction(MatrixNd &out_emitt, MatrixNd &out_beam, MatrixNd &out_particles_id, const std::string &in_beamline, const std::string &in_beam, const std::string &in_survey, int in_nmachines, int in_start, int in_end, const std::string &in_format )
{
  OSTREAM << int(PLACET_TEST_NO_CORRECTION);
  OSTREAM << in_beamline;
  OSTREAM << in_beam;
  OSTREAM << in_survey;
  OSTREAM << in_nmachines;
  OSTREAM << in_start;
  OSTREAM << in_end;
  OSTREAM << in_format;
  OSTREAM.flush();
  ISTREAM >> out_emitt;
  ISTREAM >> out_beam;
  ISTREAM >> out_particles_id;
}

void placet_test_no_correction(MatrixNd &out_emitt, MatrixNd &out_beam, MatrixNd &out_particles_id, const std::string &in_beamline, const std::string &in_beam, const std::string &in_survey, const std::string &in_format )
{
  OSTREAM << int(PLACET_TEST_NO_CORRECTION2);
  OSTREAM << in_beamline;
  OSTREAM << in_beam;
  OSTREAM << in_survey;
  OSTREAM << in_format;
  OSTREAM.flush();
  ISTREAM >> out_emitt;
  ISTREAM >> out_beam;
  ISTREAM >> out_particles_id;
}

void placet_get_bpm_readings(MatrixNd &out_readings, MatrixNd &out_positions, std::vector<size_t> &out_indexes, MatrixNd &out_transmission, const std::string &in_beamline, const std::vector<size_t> &in_bpms, bool in_exact )
{
  OSTREAM << int(PLACET_GET_BPM_READINGS);
  OSTREAM << in_beamline;
  OSTREAM << in_bpms;
  OSTREAM << in_exact;
  OSTREAM.flush();
  ISTREAM >> out_readings;
  ISTREAM >> out_positions;
  ISTREAM >> out_indexes;
  ISTREAM >> out_transmission;
}

void placet_get_number_list(std::vector<size_t> &out_indexes, const std::string &in_beamline, const std::vector<std::string> &in_types )
{
  OSTREAM << int(PLACET_GET_NUMBER_LIST);
  OSTREAM << in_beamline;
  OSTREAM << in_types;
  OSTREAM.flush();
  ISTREAM >> out_indexes;
}

void placet_get_name_number_list(std::vector<size_t> &out_indexes, const std::string &in_beamline, const std::string &in_pattern )
{
  OSTREAM << int(PLACET_GET_NAME_NUMBER_LIST);
  OSTREAM << in_beamline;
  OSTREAM << in_pattern;
  OSTREAM.flush();
  ISTREAM >> out_indexes;
}

void placet_element_set_attribute(const std::string &in_beamline, const std::vector<size_t> &in_indexes, const std::vector<std::string> &in_attributes, const std::vector<Attribute> &in_value )
{
  OSTREAM << int(PLACET_ELEMENT_SET_ATTRIBUTE);
  OSTREAM << in_beamline;
  OSTREAM << in_indexes;
  OSTREAM << in_attributes;
  OSTREAM << in_value;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_element_get_attribute(std::vector<Attribute> &out_value, const std::string &in_beamline, const std::vector<size_t> &in_indexes, const std::vector<std::string> &in_attributes )
{
  OSTREAM << int(PLACET_ELEMENT_GET_ATTRIBUTE);
  OSTREAM << in_beamline;
  OSTREAM << in_indexes;
  OSTREAM << in_attributes;
  OSTREAM.flush();
  ISTREAM >> out_value;
}

void placet_element_get_attributes(std::vector<std::map<std::string, Attribute> > &out_attributes, const std::string &in_beamline, const std::vector<size_t> &in_indexes )
{
  OSTREAM << int(PLACET_ELEMENT_GET_ATTRIBUTES);
  OSTREAM << in_beamline;
  OSTREAM << in_indexes;
  OSTREAM.flush();
  ISTREAM >> out_attributes;
}

void placet_vary_corrector(const std::string &in_beamline, const std::vector<size_t> &in_indexes, const MatrixNd &in_delta )
{
  OSTREAM << int(PLACET_VARY_CORRECTOR);
  OSTREAM << in_beamline;
  OSTREAM << in_indexes;
  OSTREAM << in_delta;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_get_beam(MatrixNd &out_beam, double &out_position, MatrixNd &out_particles_id, const std::string &in_name )
{
  OSTREAM << int(PLACET_GET_BEAM);
  OSTREAM << in_name;
  OSTREAM.flush();
  ISTREAM >> out_beam;
  ISTREAM >> out_position;
  ISTREAM >> out_particles_id;
}

void placet_set_beam(const std::string &in_name, const MatrixNd &in_beam )
{
  OSTREAM << int(PLACET_SET_BEAM);
  OSTREAM << in_name;
  OSTREAM << in_beam;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_set_beam(const MatrixNd &in_beam )
{
  OSTREAM << int(PLACET_SET_BEAM2);
  OSTREAM << in_beam;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_beamline_refresh(const std::string &in_beamline )
{
  OSTREAM << int(PLACET_BEAMLINE_REFRESH);
  OSTREAM << in_beamline;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_get_response_matrix_fit(MatrixNd &out_Rxx, MatrixNd &out_Rxy, MatrixNd &out_Ryx, MatrixNd &out_Ryy, MatrixNd &out_Rxxx, MatrixNd &out_Rxxy, MatrixNd &out_Rxyy, MatrixNd &out_Ryxx, MatrixNd &out_Ryyx, MatrixNd &out_Ryyy, const std::string &in_beamline, const std::string &in_beam, const std::string &in_survey, const std::vector<size_t> &in_bpms, const std::vector<size_t> &in_correctors )
{
  OSTREAM << int(PLACET_GET_RESPONSE_MATRIX_FIT);
  OSTREAM << in_beamline;
  OSTREAM << in_beam;
  OSTREAM << in_survey;
  OSTREAM << in_bpms;
  OSTREAM << in_correctors;
  OSTREAM.flush();
  ISTREAM >> out_Rxx;
  ISTREAM >> out_Rxy;
  ISTREAM >> out_Ryx;
  ISTREAM >> out_Ryy;
  ISTREAM >> out_Rxxx;
  ISTREAM >> out_Rxxy;
  ISTREAM >> out_Rxyy;
  ISTREAM >> out_Ryxx;
  ISTREAM >> out_Ryyx;
  ISTREAM >> out_Ryyy;
}

void placet_get_transfer_matrix_fit(MatrixNd &out_X, MatrixNd &out_R, std::vector<MatrixNd> &out_T, const std::string &in_beamline, const std::string &in_beam, const std::string &in_survey, int in_start, int in_end )
{
  OSTREAM << int(PLACET_GET_TRANSFER_MATRIX_FIT);
  OSTREAM << in_beamline;
  OSTREAM << in_beam;
  OSTREAM << in_survey;
  OSTREAM << in_start;
  OSTREAM << in_end;
  OSTREAM.flush();
  ISTREAM >> out_X;
  ISTREAM >> out_R;
  ISTREAM >> out_T;
}

void placet_get_transfer_matrix(MatrixNd &out_R, const std::string &in_beamline, int in_start, int in_end )
{
  OSTREAM << int(PLACET_GET_TRANSFER_MATRIX);
  OSTREAM << in_beamline;
  OSTREAM << in_start;
  OSTREAM << in_end;
  OSTREAM.flush();
  ISTREAM >> out_R;
}

void placet_get_transfer_matrix_elements_fit(MatrixNd &out_elements, const std::string &in_beamline, const std::string &in_beam, const std::string &in_survey, const std::vector<std::string> &in_elements, int in_start, std::vector<size_t> in_ends )
{
  OSTREAM << int(PLACET_GET_TRANSFER_MATRIX_ELEMENTS_FIT);
  OSTREAM << in_beamline;
  OSTREAM << in_beam;
  OSTREAM << in_survey;
  OSTREAM << in_elements;
  OSTREAM << in_start;
  OSTREAM << in_ends;
  OSTREAM.flush();
  ISTREAM >> out_elements;
}

void placet_girder_get_length(MatrixNd &out_lengths, const std::string &in_beamline )
{
  OSTREAM << int(PLACET_GIRDER_GET_LENGTH);
  OSTREAM << in_beamline;
  OSTREAM.flush();
  ISTREAM >> out_lengths;
}

void placet_girder_move(const std::string &in_beamline, const MatrixNd &in_delta )
{
  OSTREAM << int(PLACET_GIRDER_MOVE);
  OSTREAM << in_beamline;
  OSTREAM << in_delta;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_Tcl_GetVar(std::string &out_value, const std::string &in_varname1, const std::string &in_varname2 )
{
  OSTREAM << int(PLACET_TCL_GETVAR);
  OSTREAM << in_varname1;
  OSTREAM << in_varname2;
  OSTREAM.flush();
  ISTREAM >> out_value;
}

void placet_Tcl_SetVar(const std::string &in_varname1, const std::string &in_varname2, const Attribute &in_value )
{
  OSTREAM << int(PLACET_TCL_SETVAR);
  OSTREAM << in_varname1;
  OSTREAM << in_varname2;
  OSTREAM << in_value;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_Tcl_SetVar(const std::string &in_varname1, const Attribute &in_value )
{
  OSTREAM << int(PLACET_TCL_SETVAR2);
  OSTREAM << in_varname1;
  OSTREAM << in_value;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_Tcl_Eval(const std::string &in_script )
{
  OSTREAM << int(PLACET_TCL_EVAL);
  OSTREAM << in_script;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}

void placet_Tcl_EvalFile(const std::string &in_filename )
{
  OSTREAM << int(PLACET_TCL_EVALFILE);
  OSTREAM << in_filename;
  OSTREAM.flush();
  int dummy;
  ISTREAM >> dummy;
}
