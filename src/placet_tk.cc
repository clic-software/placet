#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cctype>
#include <cmath>
#include <sstream>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "minimise_tk.h"
#include "beam.h"
#include "beamenergy.h"
#include "beamline.h"
#include "bin.h"
#include "bpm.h"
#include "multipole.h"
#include "cavity.h"
#include "correct_tk.h"
#include "sbend.h"
#include "twostream.h"
#include "focus.h"
#include "slices_to_particles.h"
#include "reference.h"
#include "element.h"
#include "girder.h"
#include "graph.h"
#include "histogram.h"
#include "interface.h"
#include "lattice.h"
#include "malloc.h"
#include "spline.h"
#include "mb_inj.h"
#include "move_sine.h"
#include "placeti3.h"
#include "placeti4.h"
#include "placeti5.h"
#include "position.h"
#include "random.hh"
#include "rfkick.h"
#include "short_range.h"
#include "spectrum.h"
#include "survey.h"
#include "survey_tk.h"
#include "structure.h"
#include "tools.h"
#include "track.h"
#include "step.h"
#include "wakes.h"
#include "wakes_tk.h"
#include "dipole.h"
#include "aperture.h"
#include "quadrupole.h"
#include "quadbpm.h"

#include "lumi.h"
#include "power.h"
#include "beam_many.h"

#include "select.h"
#include "laser.h"
#include "peder.h"
#include "daniel.h"
#include "andrea.h"
#include "photon.h"
#include "phase_advance.h"
#include "irtrk_tk.h"
#include "about.h"

#ifdef HTGEN
#include "background.h"
#endif

/* data definitions */

#include "placet_tk.h"

//#include "file_stream.hh"
#include "static_rf_align.h"

#include "help.h"

#include "bump.cc"
#include "measured_tk.c"

INTER_DATA_STRUCT inter_data;

std::vector<COMMAND_HELPER> help_head;

Tcl_HashTable BeamTable,QuadTable,SurveyTable,WakeTable,ModeTable,NameTable;
Tcl_Interp *beamline_survey_hook_interp;
static std::string global_survey_name;

/// default output format
char *default_format = "%s %ex %sex %x 0 %ey %sey %Env 0 %n";

#ifdef EARTH_FIELD
EARTH_FIELD_STRUCT earth_field;
#endif
QUAD_WAKE_STRUCT quad_wake;
extern PRINT_DATA print_data;
extern PetsWake drive_data;

static struct{
  Tcl_Interp *interp;
  int error;
} store_interp;

void interp_append(char *s)
{
  Tcl_AppendResult(store_interp.interp,s,NULL);
}

void check_error(Tcl_Interp *interp,char *msg,int error)
{
  if (error==TCL_OK) Tcl_AppendResult(interp,"Error in ",msg,":\n",NULL);
}

/** checks if the beamline is ready for tracking */

int check_beamline_ready(Tcl_Interp *interp,char *cmd,int error)
{
  if (!inter_data.beamline_set){
    check_error(interp,cmd,error);
    Tcl_AppendResult(interp,"You have to define the beamline before invoking ",
		     cmd,"\n",NULL);
    return TCL_ERROR;
  }
  return TCL_OK;
}

int beamline_bin_divide_list(Tcl_Interp *interp,BEAMLINE* /*beamline*/,BIN **bin,
			     int *nbin,char *bin_list)
{
  int error;
  char **v,**v1,**vq,**vbpm,**vqx,**vbpmx;
  int n,n1,i,j;
  int nq,nbpm,nqx,nbpmx;
  int *quad,*bpm,*quadx,*bpmx;

  if( (error=Tcl_SplitList(interp,bin_list,&n,&v)) ) {
    placet_printf(ERROR,"error1\n");
    return error;
  }
  //  bin=(BIN**)xmalloc(sizeof(BIN*)*n);
  *nbin=n;
  for (i=0;i<n;i++){
    if(error=Tcl_SplitList(interp,v[i],&n1,&v1)) {
      placet_printf(ERROR,"error2\n");
      return error;
    }
    if (n1!=4) { return TCL_ERROR;}
    if(error=Tcl_SplitList(interp,v1[0],&nq,&vq)) return error;  
    if(error=Tcl_SplitList(interp,v1[1],&nbpm,&vbpm)) return error;  
    if(error=Tcl_SplitList(interp,v1[2],&nqx,&vqx)) return error;  
    if(error=Tcl_SplitList(interp,v1[3],&nbpmx,&vbpmx)) return error;  
    quad=(int*)alloca(sizeof(int)*nq);
    for (j=0;j<nq;j++){
      quad[j]=strtol(vq[j],NULL,10);
    }
    bpm=(int*)alloca(sizeof(int)*nbpm);
    for (j=0;j<nbpm;j++){
      bpm[j]=strtol(vbpm[j],NULL,10);
    }
    quadx=(int*)alloca(sizeof(int)*nqx);
    for (j=0;j<nqx;j++){
      quadx[j]=strtol(vqx[j],NULL,10);
    }
    bpmx=(int*)alloca(sizeof(int)*nbpmx);
    for (j=0;j<nbpmx;j++){
      bpmx[j]=strtol(vbpmx[j],NULL,10);
    }
    bin[i]=bin_make_x(nq,nbpm,nqx,nbpmx);
    bin_set_elements_x(bin[i],quad,nq,bpm,nbpm,quadx,nqx,bpmx,nbpmx,nq,nqx);
  }
  return TCL_OK;
}

Tcl_Command
Placet_CreateCommand(Tcl_Interp *interp,
				  const char *cmdName,
				  Tcl_CmdProc *proc,
				  ClientData clientData,
				  Tcl_CmdDeleteProc *deleteProc, std::string texinfo_description )
{
  help_head.push_back(COMMAND_HELPER(interp, cmdName, texinfo_description));
  return Tcl_CreateCommand(interp,cmdName,proc,clientData,deleteProc);
}

Tcl_Command
Placet_CreateCommand(MANUAL::CATEGORY category, MANUAL::SUBCATEGORY subcategory, Tcl_Interp *interp,
				  const char *cmdName,
				  Tcl_CmdProc *proc,
				  ClientData clientData,
				  Tcl_CmdDeleteProc *deleteProc, std::string texinfo_description )
{
  help_head.push_back(COMMAND_HELPER(interp, cmdName, category, subcategory, texinfo_description));
  return Tcl_CreateCommand(interp,cmdName,proc,clientData,deleteProc);
}

/**********************************************************************/
/*                      beam_set_range_offset_y (not a command)       */
/**********************************************************************/

void beam_set_range_offset_y(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].y+=offset;
  }
}

/**********************************************************************/
/*                      beam_set_range_offset_yp (not a command)      */
/**********************************************************************/

void beam_set_range_offset_yp(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].yp+=offset;
  }
}

/**********************************************************************/
/*                      beam_set_to_range_offset_y (not a command)    */
/**********************************************************************/

void beam_set_to_range_offset_y(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].y=offset;
  }
}

/**********************************************************************/
/*                      beam_set_to_range_offset_yp (not a command)   */
/**********************************************************************/

void beam_set_to_range_offset_yp(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].yp=offset;
  }
}

/**********************************************************************/
/*                      beam_set_range_offset_sine_y (not a command)  */
/**********************************************************************/

void beam_set_range_offset_sine_y(BEAM *beam,int nmin,int nmax,double offset,
				  double lambda,double phi0)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].y+=offset
      *sin(phi0+TWOPI*1e-6
	   *beam->z_position[(int)(i/beam->macroparticles)]/lambda);
  }
}

/**********************************************************************/
/*                      beam_set_range_offset_sine_yp (not a command) */
/**********************************************************************/

void beam_set_range_offset_sine_yp(BEAM *beam,int nmin,int nmax,double offset,
				   double lambda,double phi0)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].yp+=offset
      *sin(phi0+TWOPI*1e-6
	   *beam->z_position[(int)(i/beam->macroparticles)]/lambda);
  }
}

#ifdef TWODIM

/**********************************************************************/
/*                      beam_set_range_offset_x (not a command)       */
/**********************************************************************/

void beam_set_range_offset_x(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].x+=offset;
  }
}

/**********************************************************************/
/*                      beam_set_range_offset_xp (not a command)      */
/**********************************************************************/

void beam_set_range_offset_xp(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].xp+=offset;
  }
}

/**********************************************************************/
/*                      beam_set_to_range_offset_x (not a command)    */
/**********************************************************************/

void beam_set_to_range_offset_x(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].x=offset;
  }
}

/**********************************************************************/
/*                      beam_set_to_range_offset_xp (not a command)   */
/**********************************************************************/

void beam_set_to_range_offset_xp(BEAM *beam,int nmin,int nmax,double offset)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].xp=offset;
  }
}

/**********************************************************************/
/*                      beam_set_range_offset_sine_x (not a command)  */
/**********************************************************************/

void beam_set_range_offset_sine_x(BEAM *beam,int nmin,int nmax,double offset,double lambda,double phi0)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].x+=offset
      *sin(phi0+TWOPI*1e-6
	   *beam->z_position[(int)(i/beam->macroparticles)]/lambda);
  }
}

/**********************************************************************/
/*                      beam_set_range_offset_sine_xp (not a command) */
/**********************************************************************/

void beam_set_range_offset_sine_xp(BEAM *beam,int nmin,int nmax,double offset,double lambda,double phi0)
{
  int i;
  for (i=nmin;i<nmax;i++){
    beam->particle[i].xp+=offset
      *sin(phi0+TWOPI*1e-6
	   *beam->z_position[(int)(i/beam->macroparticles)]/lambda);
  }
}

#endif

/**********************************************************************/
/*                      beam_set_to_macro_offset_y (not a command)    */
/*                      we assume macros distributed with 3 sigma     */
/**********************************************************************/

void beam_set_to_macro_offset_xy(BEAM *beam, double sigma_offset_x, double sigma_offset_y, double* e_list, double* /*w_list*/)
{
  //NB: for the moment this parameters are hard-coded (and must correspond to the one used to generate the gauss list in the main script!)
  double sigma_list = 1.0e-9;  
  

  int n_macro = beam->macroparticles;
  for (int i_b=0;i_b<beam->bunches;i_b++) {
    for (int i_s=0;i_s<beam->slices_per_bunch;i_s++) {
      int i_z=i_b*beam->slices_per_bunch+i_s;
      for (int i_m=0;i_m<beam->macroparticles;i_m++) {
	int i_part=i_z*n_macro+i_m;
	// set an offset in y of the macroparticle, following the 
	double offset_x = sigma_offset_x * (e_list[i_m] / sigma_list);
	beam->particle[i_part].x += offset_x;
	double offset_y = sigma_offset_y * (e_list[i_m] / sigma_list);
	beam->particle[i_part].y += offset_y;
	//placet_printf(INFO,"EA: offseting part %d, wgt: %g,  with offset_y of %g  (%g sigmas)\n", i_part,beam->particle[i_m].wgt, offset_y, e_list[i_m] / sigma_list);
      }
    }
  }
}


/**********************************************************************/
/*                  REAL last weight  */
/**********************************************************************/

void beam_set_real_last_weight(BEAM *beam, double real_last_wgt)
{

  placet_printf(INFO,"EA: entering real last wgt.\n");
  int n_macro = beam->macroparticles;
  //for (int i_b=0;i_b<beam->bunches;i_b++) {
  int i_b=beam->bunches-1;
  for (int i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    int i_z=i_b*beam->slices_per_bunch+i_s;
    for (int i_m=0;i_m<beam->macroparticles;i_m++) {
      int i_part=i_z*n_macro+i_m;
      beam->particle[i_part].wgt *= real_last_wgt;
      placet_printf(INFO,"EA: putting real last weight (times %g) on i_part: %d\n", real_last_wgt, i_part);
    }
  }
}

/* 
   returns the particle number defined by the string s
*/

int
eval_beam_position(Tcl_Interp *interp,BEAM *beam,char *s,int *np)
{
  int error;
  int i,n,nb=0,ns=0,nm=0,ntmp;
  char **v;
  if(error=Tcl_SplitList(interp,s,&n,&v)) return error;
  if (beam->particle_beam) {
    if (n>2) {
      Tcl_AppendResult(interp,"Error: too many arguments for particle beam in address ",s,NULL);
      return TCL_ERROR;
    }
    if (n<=0) {
      Tcl_AppendResult(interp,"Error: not enough arguments in address ",s,
		       NULL);
      return TCL_ERROR;
    }
    
    if (strcmp(v[0],"start")==0) {
      *np=0;
      return TCL_OK;
    }
    
    if (strcmp(v[0],"end")==0) {
      ntmp=beam->slices_per_bunch*beam->bunches;
      *np=0;
      for (i=0;i<ntmp;++i) {
	*np+=beam->particle_number[i];
      }
      return TCL_OK;
    }
    
    if(error=Tcl_GetInt(interp,v[0],&nb)) return error;
    if ((nb<0)||(nb>beam->bunches)) {
      Tcl_AppendResult(interp,"Error: bunch number is wrong ",v[0],NULL);
      return TCL_ERROR;
    }
    if (n>=2) {
      if(error=Tcl_GetInt(interp,v[1],&ns)) return error;
    }
    if ((ns<0)||(ns>beam->slices_per_bunch)) {
      Tcl_AppendResult(interp,"Error: slice number is wrong ",v[1],NULL);
      return TCL_ERROR;
    }
    ntmp=nb*beam->slices_per_bunch+ns;
    *np=0;
    for (i=0;i<ntmp;++i) {
      *np+=beam->particle_number[i];
    }
  }
  else {
    if (n>3) {
      Tcl_AppendResult(interp,"Error: too many arguments in address ",s,NULL);
      return TCL_ERROR;
    }
    if (n<=0) {
      Tcl_AppendResult(interp,"Error: not enough arguments in address ",s,
		       NULL);
      return TCL_ERROR;
    }
    
    if (strcmp(v[0],"start")==0) {
      *np=0;
      return TCL_OK;
    }
    
    if (strcmp(v[0],"end")==0) {
      *np=beam->slices_per_bunch*beam->bunches*beam->macroparticles;
      return TCL_OK;
    }
    
    if(error=Tcl_GetInt(interp,v[0],&nb)) return error;
    if ((nb<0)||(nb>beam->bunches)) {
      Tcl_AppendResult(interp,"Error: bunch number is wrong ",v[0],NULL);
      return TCL_ERROR;
    }
    if (n>=2) {
      if(error=Tcl_GetInt(interp,v[1],&ns)) return error;
    }
    if ((ns<0)||(ns>beam->slices_per_bunch)) {
      Tcl_AppendResult(interp,"Error: slice number is wrong ",v[1],NULL);
      return TCL_ERROR;
    }
    if (n>=3) {
      if(error=Tcl_GetInt(interp,v[2],&nm)) return error;
    }
    if ((nm<0)||(nm>beam->macroparticles)) {
      Tcl_AppendResult(interp,"Error: macroparticle number is wrong ",v[2],
		       NULL);
      return TCL_ERROR;
    }
    *np=(nb*beam->slices_per_bunch+ns)*beam->macroparticles+nm;
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

BEAM* get_beam(const char * beamname) {
  Tcl_HashEntry * entry=Tcl_FindHashEntry(&BeamTable,beamname);
  if (entry==NULL) {
    placet_cout << ERROR << "Beam '" << beamname << "' not found" << endmsg;
    exit(1);
  }
  BEAM* beam=(BEAM*)Tcl_GetHashValue(entry);
  return beam;
}

BEAMLINE* get_beamline(const char * beamlinename) {
  BEAMLINE *beamline = NULL;
  if (beamlinename) {
    std::map<std::string,BEAMLINE*>::iterator it = inter_data.blist.find(std::string(beamlinename));
    if (it==inter_data.blist.end()) {
      placet_cout << INFO << "Beamline '" << beamlinename << "' not found" << endmsg;
    } else {
      beamline = (*it).second;
    }
  } else {
    beamline = inter_data.beamline;
    placet_cout << INFO << "Using default beamline '" << beamline->get_name() << "'" << endmsg;
  }
  
  return beamline;
}

WAKE_DATA* get_wake(const char * wakename) {
  Tcl_HashEntry *entry=Tcl_FindHashEntry(&WakeTable,wakename);
  if (entry==NULL) {
    placet_cout << ERROR << "WakeSet '" << wakename << "' not found" << endmsg;
    exit(1);
  }
  WAKE_DATA* wake=(WAKE_DATA*)Tcl_GetHashValue(entry);
  return wake;
}

  /**********************************************************************/
  /*                      Tcl_survey (not a command)                    */
  /**********************************************************************/


  int Tcl_Survey(ClientData clientdata,Tcl_Interp * /*interp*/,int /*argc*/,
		 char * /*argv*/[])
  {
    void* (*survey)(BEAMLINE*);
    survey=(void* (*)(BEAMLINE *))clientdata;
    survey(inter_data.beamline);
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      survey_add (not a command)                    */
  /**********************************************************************/

  void survey_add(Tcl_Interp *interp,char name[],void (survey)(BEAMLINE*))
  {
    Tcl_HashEntry *entry;
    int ok;
    entry=Tcl_CreateHashEntry(&SurveyTable,name,&ok);
    if (ok){
      Tcl_SetHashValue(entry,survey);
      Placet_CreateCommand(interp,name,&Tcl_Survey,(ClientData)survey,NULL);
      return;
    }
    exit(-1);
  }

  /**********************************************************************/
  /*                      beamline_survey_name (not a command)          */
  /**********************************************************************/

  void beamline_survey_name(BEAMLINE * /*beamline*/)
  {
    int error;
    if (error=Tcl_Eval(beamline_survey_hook_interp, const_cast<char*>(global_survey_name.c_str()))) {
      placet_cout << ERROR << "an error occurred in \"" << global_survey_name.c_str() << "\": "
         << Tcl_GetStringResult(beamline_survey_hook_interp) << endmsg;
      exit(-1);
    }
    return;
  }

  void* survey_find(const char *name )
  {
    //  Tcl_HashEntry *entry;

    global_survey_name=name;
    return (void*)&beamline_survey_name;
  }

  /**********************************************************************/
  /*                      survey_init (not a command)                   */
  /**********************************************************************/

  void survey_init(Tcl_Interp *interp)
  {
    //  survey_add(interp,"Tesla",&beamline_survey_tesla);
    //  survey_add(interp,"Clic",&beamline_survey_clic);
    survey_add(interp,"Nlc",&beamline_survey_nlc);
    survey_add(interp,"Zero",&beamline_set_zero);
    survey_add(interp,"None",&beamline_survey_none);
    survey_add(interp,"Inject",&beamline_survey_inject);
    survey_add(interp,"Inject2",&beamline_survey_inject2);
    survey_add(interp,"QuadRoll",&beamline_survey_quadroll);
    survey_add(interp,"Earth",&beamline_survey_earth);
    survey_add(interp,"AtlZero",&beamline_survey_atl);
    survey_add(interp,"Atl",&beamline_survey_atl_move);
    survey_add(interp,"AtlZero2",&beamline_survey_atlzero_2);
    survey_add(interp,"Atl2",&beamline_survey_atl_2);
    // survey_add(interp,"SurveyHook",&beamline_survey_hook);
    survey_init_atl(3e3);
  }

  /**********************************************************************/
  /*                      wake_eval (not a command)                     */
  /**********************************************************************/

  double wake_eval(WAKE_DATA *wake,double x)
  {
    double sum=0.0;
    for (int i=0;i<wake->n;i++)
      sum+=wake->a0[i]*exp(-PI*x/(wake->Q[i]*wake->lambda[i]))*sin(TWOPI*x/wake->lambda[i]);
    return sum;
  }

/******   Tcl/Tk commands   ******/

namespace {

  /**********************************************************************/
  /*                      Beam (not a command)                          */
  /**********************************************************************/

  int tk_Beam(ClientData clientdata,Tcl_Interp * /*interp*/,int /*argc*/,
	      char *argv[])
  {
    BEAM *beam=(BEAM*)clientdata;
    if (!strcmp(argv[1],"track")){
      bunch_track_line_emitt_new(inter_data.beamline,beam);
    }
    /*
    if (!strcmp(argv[1],"reset")){
      int ibunch,islice,ipart,k;
      double betax,betay,alphax,alphay,epsx,epsy,e0,de,ecut,delta,sigma_e;
      // switch on beamtype not working because not set
      switch(beam->drive_data->beamtype){
      case MainBeam:
	betax=beam->drive_data->param.main->betax;
	betay=beam->drive_data->param.main->betay;
	alphax=beam->drive_data->param.main->alphax;
	alphay=beam->drive_data->param.main->alphay;
	epsx=beam->drive_data->param.main->emittx;
	epsy=beam->drive_data->param.main->emitty;
	e0=beam->drive_data->param.main->e0;
	ecut=beam->drive_data->param.main->ecut;
	sigma_e=beam->drive_data->param.main->espread;
	break;
      case InjectorBeam:
	betax=beam->drive_data->param.inject->betax;
	betay=beam->drive_data->param.inject->betay;
	alphax=beam->drive_data->param.inject->alphax;
	alphay=beam->drive_data->param.inject->alphay;
	epsx=beam->drive_data->param.inject->emittx;
	epsy=beam->drive_data->param.inject->emitty;
	e0=beam->drive_data->param.inject->e0;
	ecut=beam->drive_data->param.inject->ecut;
	sigma_e=beam->drive_data->param.inject->espread;
	break;
      case DriveBeam:
	betax=beam->drive_data->param.drive->betax;
	betay=beam->drive_data->param.drive->betay;
	alphax=beam->drive_data->param.drive->alphax;
	alphay=beam->drive_data->param.drive->alphay;
	epsx=beam->drive_data->param.drive->emittx;
	epsy=beam->drive_data->param.drive->emitty;
	e0=beam->drive_data->param.drive->e0;
	ecut=beam->drive_data->param.drive->ecut;
	sigma_e=beam->drive_data->param.drive->espread;
	break;
      }
      epsx*=EMASS/e0*1e12*EMITT_UNIT;
      epsy*=EMASS/e0*1e12*EMITT_UNIT;
      if (beam->macroparticles>1){
	de=2.0*ecut/beam->macroparticles;
      }
      else{
	de=0.0;
	ecut=0.0;
      }
      k=0;
      for (ibunch=0;ibunch<beam->bunches;ibunch++){
	for (islice=0;islice<beam->slices_per_bunch;islice++){
	  for (ipart=0;ipart<beam->macroparticles;ipart++){
#ifdef TWODIM
// 	    bunch_set_slice_x(beam,ibunch,islice,ipart,0.0);
// 	    bunch_set_slice_xp(beam,ibunch,islice,ipart,0.0);
	    bunch_set_sigma_xx(beam,k,betax,alphax,epsx);
	    bunch_set_sigma_xy(beam,k);
	    beam->particle[k].x=0.0;
	    beam->particle[k].xp=0.0;
#endif
//  	    bunch_set_slice_y(beam,ibunch,islice,ipart,zero_point);
//  	    bunch_set_slice_yp(beam,ibunch,islice,ipart,0.0);
	    bunch_set_sigma_yy(beam,k,betay,alphay,epsy);
	    delta=-ecut+((double)ipart+0.5)*de;
// 	    bunch_set_slice_energy(beam,ibunch,islice,ipart,e0+delta*sigma_e);
	    beam->particle[k].energy=e0+delta*sigma_e;
	    beam->particle[k].y=0.0;
	    beam->particle[k].yp=0.0;
	    k++;
	  }
	}
      }
    */
    else {    
      placet_cout << WARNING << argv[1] << " is not implemented for BEAM" << endmsg;
    }
    return TCL_OK;
  }


  /**********************************************************************/
  /*                      DipoleNumberList                              */
  /**********************************************************************/

  int tk_DipoleNumberList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			  char *argv[])
  {
    int error,i;
    ELEMENT **element;
    char buffer[100];

    Tk_ArgvInfo table[]={
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
      return error;
    }

    element=inter_data.beamline->element;
    for (i=0;i<inter_data.beamline->n_elements;i++){
      if (dynamic_cast<DIPOLE*>(element[i])){
	snprintf(buffer,100,"%d",i);
	Tcl_AppendElement(interp,buffer);
      }
    }
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      DipoleGetStrengthList                         */
  /**********************************************************************/

  int tk_DipoleGetStrengthList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			       char *argv[])
  {
    int error,i;
    ELEMENT **element;
    char buffer[100];

    Tk_ArgvInfo table[]={
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
      return error;
    }

    element=inter_data.beamline->element;
    for (i=0;i<inter_data.beamline->n_elements;i++){
      if (DIPOLE *dipole=dynamic_cast<DIPOLE*>(element[i])){
	snprintf(buffer,100,"%g",dipole->get_strength_y());
	Tcl_AppendElement(interp,buffer);
      }
    }
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      DipoleSetStrengthList                         */
  /**********************************************************************/

  int tk_DipoleSetStrengthList(ClientData /*clientdata*/,Tcl_Interp *interp,
			       int argc,char *argv[])
  {
    int error,i,j,n;
    char **v;
    double value;
    ELEMENT **element;

    Tk_ArgvInfo table[]={
      {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
       (char*)"This command accepts a list, it sets the strengths of all dipoles to the corresponding values of the list."},
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };

    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
      return error;
    }

    if (argc!=2){
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Usage: ",argv[0]," value_list\n",NULL);
      return TCL_ERROR;
    }
    if (error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
    j=0;
    element=inter_data.beamline->element;
    for (i=0;i<inter_data.beamline->n_elements;i++) {
      if (DIPOLE *dipole=dynamic_cast<DIPOLE*>(element[i])) {
	if (error=Tcl_GetDouble(interp,v[j],&value)) return error;
	dipole->set_strength_y(value);
	j++;
	if (j==n) {
	  break;
	}
      }
    }
    i++;
    while (i<inter_data.beamline->n_elements) {
      if (dynamic_cast<DIPOLE*>(element[i])) {
	Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
			 "The number of values in the list is smaller than the number of dipoles\n",NULL);
	return TCL_ERROR;
      }
      i++;
    }
    if (j!=n) {
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "The number of values in the list is larger than the number of dipoles\n",NULL);
      return TCL_ERROR;
    }
    Tcl_Free((char*)v);
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      DipoleSetStrength                             */
  /**********************************************************************/

  int tk_DipoleSetStrength(ClientData /*clientdata*/,Tcl_Interp *interp,
			   int argc,char *argv[])
  {
    int error=0,j=-1;
    double value_x,value_y;
    int add=0,sine=0,cosine=0,/*whitenoise=0,*/end=1;
    Tk_ArgvInfo table[]={
      {(char*)"-add",TK_ARGV_INT,(char*)NULL,(char*)&add,
       (char*)"If not zero add to current value"},
      {(char*)"-sine",TK_ARGV_INT,(char*)NULL,(char*)&sine,
       (char*)"If not zero use sine with wavelength and rel_strength"},
      {(char*)"-cosine",TK_ARGV_INT,(char*)NULL,(char*)&cosine,
       (char*)"If not zero use cosine with wavelength and rel_strength"},
      //    {(char*)"-whitenoise",TK_ARGV_INT,(char*)NULL,(char*)&whitenoise,
      //     (char*)"If not zero use whitenoise with gaussian mean and variance"},
      {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&end,
       (char*)"If not zero start sine at end of beamline"},
      {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
       (char*)"This command sets the strength of the dipole number specified.\nUsage: DipoleSetStrength dipole_number strength"},    
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };

    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
      return error;
    }

    if (argc!=4){
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Usage: ",argv[0]," dipole_number value_x value_y\n",NULL);
      return TCL_ERROR;
    }
    error=Tcl_GetInt(interp,argv[1],&j);
    if(error) {
      return error;
    }

    if ((j<0)||(j>=inter_data.beamline->n_elements)) {
      char buf[40];
      snprintf(buf,40,"Element #%d does not exist",j);
      Tcl_SetResult(interp,buf,TCL_VOLATILE);
      return TCL_ERROR;
    }

    DIPOLE *dipole;
    if ((dipole=dynamic_cast<DIPOLE*>(inter_data.beamline->element[j]))==NULL) {
      char buf[40];
      snprintf(buf,40,"Element #%d is not a Dipole\n",j);
      Tcl_SetResult(interp,buf,TCL_VOLATILE);
      return TCL_ERROR;
    }


    error=Tcl_GetDouble(interp,argv[2],&value_x);
    if (error) {
      return error;
    }
    error=Tcl_GetDouble(interp,argv[3],&value_y);
    if (error) {
      return error;
    }
    double s = dipole->get_s();
    if (sine!=0 || cosine!=0) {
      double wavelength = (sine!=0) ? sine : cosine;
      double rel_strength_x = value_x;
      double rel_strength_y = value_y;
      double phase = 0.;
      if (end) {
	phase =  6.283185307*(inter_data.beamline->get_length()-s)/wavelength;
      }
      else {phase = 6.283185307*s/wavelength;}

      double factor = 0.;
      if (sine) {factor = std::sin(phase);}
      else if (cosine) {factor = std::cos(phase);}

      value_x = rel_strength_x*factor;
      value_y = rel_strength_y*factor;
    }

    if (add) {
      dipole->add_strength_x(value_x);
      dipole->add_strength_y(value_y);
    }
    else {
      dipole->set_strength_x(value_x);
      dipole->set_strength_y(value_y);
    }
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      DivideBeamline2                               */
  /**********************************************************************/

  int tk_DivideBeamline2(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
  {
    int error;
    char *file_name=NULL;
    int n=0;
    double length=0.0;
    Tk_ArgvInfo table[]={
      {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
       (char*)"Name of the file where to store the results"},
      {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
       (char*)"Expected length per drive beam"},
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
	!=TCL_OK){
      return error;
    }
    if (argc!=1) {
      Tcl_SetResult(interp,(char*)"Too many arguments to DivideBeamline2",
		    TCL_VOLATILE);
      return TCL_ERROR;
    }

    if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
    if (n<1) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"n_cav must be larger than 0",NULL);
    }
    if (divide_linac2(inter_data.beamline,file_name,n,length)){
      return TCL_ERROR;
    }
    return TCL_OK;
  }


  /**********************************************************************/
  /*                      ClicSurvey                                    */
  /**********************************************************************/

  int tk_Clic(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	      char *argv[])
  {
    int error;
    int first=0,last=-1;
    Tk_ArgvInfo table[]={
      {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&first,
       (char*)"First element to misalign"},
      {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&last,
       (char*)"First element after region to misalign"},
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
	!=TCL_OK){
      return error;
    }
    if (argc!=1) {
      Tcl_SetResult(interp,(char*)"Too many arguments to Clic",
		    TCL_VOLATILE);
      return TCL_ERROR;
    }

    if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
    beamline_survey_clic(inter_data.beamline,first,last);
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      QuadrupoleSettingCmd                          */
  /**********************************************************************/

  /* set a quadrupole out of a set to a given value */

  int tk_QuadrupoleSettingCmd(ClientData clientdata,Tcl_Interp *interp,int argc,
			      char *argv[])
  {
    double *quad,value;
    char no[100];
    int i,error;
    if (argc!=3) {
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Usage: ",argv[0]," number value\n",NULL);
      return TCL_ERROR;
    }
    quad=(double*)clientdata;
    error=Tcl_GetInt(interp,argv[1],&i);
    if (i<inter_data.beamline->n_quad){
      error=Tcl_GetDouble(interp,argv[2],&value);
      if (!error){
	quad[i]=-value/inter_data.beamline->quad[i]->get_length();
      }
    }
    else{
      snprintf(no,100,"%d",inter_data.beamline->n_quad);
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "quadrupole number must be smaller than ",no,"\n",NULL);
      return TCL_ERROR;
    }
    if (error) return error;
    return TCL_OK;
  }

  /**********************************************************************/
  /*                      Wake_Eval                                     */
  /**********************************************************************/

  int tk_WakeEval(ClientData clientdata,Tcl_Interp *interp,int /*argc*/,
		  char *argv[])
  {
    char *point,buffer[20];
    double x=strtod(argv[1],&point);
    double sum=wake_eval((WAKE_DATA*)clientdata,x);
    snprintf(buffer,20,"%g",sum);
    Tcl_SetResult(interp,buffer,TCL_VOLATILE);
    return TCL_OK;
  }

/**********************************************************************/
/*                      BeamAddOffset                                 */
/**********************************************************************/

int tk_BeamAddOffset(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error;
  char *name=NULL;
  int n1=0,n2=0;
  char *sn1="start",*sn2="end";
  double offset_x=0.0,offset_y=0.0,offset_xp=0.0,offset_yp=0.0,theta=0.0;
  BEAM *beam;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam to offset"},
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_x,
     (char*)"horizontal offset in micro-metres"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_y,
     (char*)"vertical offset in micro-metres"},
    {(char*)"-angle_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_xp,
     (char*)"horizontal angle in micro-radian"},
    {(char*)"-angle_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_yp,
     (char*)"vertical angle in micro-radian"},
    {(char*)"-rotate",TK_ARGV_FLOAT,(char*)NULL,(char*)&theta,
     (char*)"rotation angle (roll) in radian (added after offsets)"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,(char*)&sn1,
     (char*)"first particle to be offset"},
    {(char*)"-end",TK_ARGV_STRING,(char*)NULL,(char*)&sn2,
     (char*)"last particle to be offset"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamAddOffset>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (name) {
    beam=get_beam(name);
  }
  else {
    beam=inter_data.bunch;
  }
  if (error=eval_beam_position(interp,beam,sn1,&n1)) return error;
  if (error=eval_beam_position(interp,beam,sn2,&n2)) return error;

  placet_cout << INFO << "BeamAddOffset, start " << n1 << " end " << n2
	      << " offset_x "  << offset_x  << " offset_y " << offset_y
	      << " offset_xp " << offset_xp << " offset_yp " << offset_yp
	      << endmsg;
  beam_set_range_offset_y(beam,n1,n2,offset_y);
  beam_set_range_offset_yp(beam,n1,n2,offset_yp);
#ifdef TWODIM
  beam_set_range_offset_x(beam,n1,n2,offset_x);
  beam_set_range_offset_xp(beam,n1,n2,offset_xp);
  bunch_rotate(beam,theta);
#endif
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamGetSize                                   */
/**********************************************************************/

int tk_BeamGetSize(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File to append beam sizes to"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  beam_get_size(inter_data.bunch,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineInfo                                  */
/**********************************************************************/

int tk_BeamlineInfo(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error,i,n;
  ELEMENT **element;
  int ncav=0,nquad=0,nbpm=0,ndrift=0;
  double length=0.0;
  char buffer[30];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if (argc!=1){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],NULL);
    return TCL_ERROR;
  }
  element=inter_data.beamline->element;
  n=inter_data.beamline->n_elements;
  for (i=0;i<n;i++){
    if (element[i]->is_cavity())       ncav++;
    else if (element[i]->is_quad())    nquad++;
    else if (element[i]->is_drift())   ndrift++;
    else if (element[i]->is_bpm())     nbpm++;
    length+=element[i]->get_length();
  }
  Tcl_AppendElement(interp,"n_cavity");
  snprintf(buffer,30,"%d",ncav);
  Tcl_AppendElement(interp,buffer);
  Tcl_AppendElement(interp,"n_quadrupole");
  snprintf(buffer,30,"%d",nquad);
  Tcl_AppendElement(interp,buffer);
  Tcl_AppendElement(interp,"n_bpm");
  snprintf(buffer,30,"%d",nbpm);
  Tcl_AppendElement(interp,buffer);
  Tcl_AppendElement(interp,"n_drift");
  snprintf(buffer,30,"%d",ndrift);
  Tcl_AppendElement(interp,buffer);
  Tcl_AppendElement(interp,"n_element");
  snprintf(buffer,30,"%d",n);
  Tcl_AppendElement(interp,buffer);
  Tcl_AppendElement(interp,"length");
  snprintf(buffer,30,"%.15g",length);
  Tcl_AppendElement(interp,buffer);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineList                                  */
/**********************************************************************/

int tk_BeamlineList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error=TCL_OK;
  char *name=NULL;
  char *binname=NULL;
  
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-binary_file",TK_ARGV_STRING,(char*)NULL,(char*)&binname,
     (char*)"Name of the file where to store the results (binary data)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  file=open_file(name);
  inter_data.beamline->list(file);
  close_file(file);
  /*ALNEW  if (binname)
    {
    File_OStream file(binname);
    file << *reinterpret_cast<const BEAMLINE*>(inter_data.beamline);
    }*/
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamLoad                                      */
/**********************************************************************/

int tk_BeamLoad(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  int error;
  char *file_name=NULL,*beam_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File containing initial beam"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beam_name,
     (char*)"name of the beam"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (beam_name){
    // check if beam exists
    get_beam(beam_name);
  }
  else{
    error=TCL_ERROR;
  }

  if (error) return error;

  /* run command */

  beam_position_load(inter_data.bunch,file_name);
  return TCL_OK;
}

/*
  Write the RMS spot sizes into a file
*/

int tk_BeamWriteRms(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,NULL,
     (char*)"Print the longitudinal RMS length and average position"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  int i,n1,n2;
  double sum_z=0.0,sum_wgt=0.0,z_mean=0.0;
  BEAM *beam;

  beam=inter_data.bunch;
  n1=(beam->bunches-1)*beam->slices_per_bunch*beam->macroparticles;
  n2=(beam->bunches)*beam->slices_per_bunch*beam->macroparticles;
  for (i=n1;i<n2;i++){
    sum_wgt+=beam->particle[i].wgt;
    z_mean+=beam->z_position[i]*beam->particle[i].wgt;
  }
  z_mean/=sum_wgt;
  for (i=n1;i<n2;i++){
    sum_z+=(beam->z_position[i]-z_mean)*(beam->z_position[i]-z_mean)
      *beam->particle[i].wgt;
  }
  char buf[40];
  snprintf(buf,40,"%g %.16g\n",sqrt(sum_z/sum_wgt),z_mean);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamSave                                      */
/**********************************************************************/

int tk_BeamSave(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File containing initial beam"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  beam_position_store_2(inter_data.bunch,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamSaveAbsolute                              */
/**********************************************************************/

int tk_BeamSaveAbsolute(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File containing initial beam"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  beam_position_store_absolute(inter_data.bunch,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamSaveAll                                   */
/**********************************************************************/

int
tk_BeamSaveAll(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error;
  char *file_name=NULL,*name=NULL;
  int head=0,axis=0,bin=0,bunches=0;
  BEAM *beam;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File containing beam to be saved"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"Name of the beam to be saved"},
    {(char*)"-header",TK_ARGV_INT,(char*)NULL,
     (char*)&head,
     (char*)"If set to 1 write a header into the file"},
    {(char*)"-axis",TK_ARGV_INT,(char*)NULL,
     (char*)&axis,
     (char*)"If set to 1 subtract the mean position and offset"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,
     (char*)&bin,
     (char*)"If set to 1 binary data is saved"},
    {(char*)"-bunches",TK_ARGV_INT,(char*)NULL,
     (char*)&bunches,
     (char*)"If set to 1 each bunch is saved in a different file"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (name) {
    beam=get_beam(name);
  }
  else {
    beam=inter_data.bunch;
  }
  if (beam->particle_beam) {
    Tcl_SetResult(interp,"The beam is not a sliced beam",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (bin) {
    beam_save_all_bin(beam,file_name,head,axis);
  }
  else {
    if(bunches) beam_save_all_bunches(beam,file_name,head,axis);
    else beam_save_all(beam,file_name,head,axis);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamLoadAll                                   */
/**********************************************************************/

int
tk_BeamLoadAll(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error;
  char *file_name=NULL,*name=NULL;
  int head=0,axis=0,bin=0,read_structure=0;
  BEAM *beam;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File containing beam to be loaded"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"Name of the beam to be loaded"},
    {(char*)"-header",TK_ARGV_INT,(char*)NULL,
     (char*)&head,
     (char*)"If set to 1 the file contains a header line"},
    {(char*)"-axis",TK_ARGV_INT,(char*)NULL,
     (char*)&axis,
     (char*)"If set to 1 subtract the mean position and offset"},
    {(char*)"-read_structure",TK_ARGV_INT,(char*)NULL,
     (char*)&read_structure,
     (char*)"If set to 1 the beam structure (weights and z-positions) are also read"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,
     (char*)&bin,
     (char*)"If set to 1 the data is in binary format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (name) {
    beam=get_beam(name);
  }
  else {
    beam=inter_data.bunch;
  }
  if (beam->particle_beam) {
    Tcl_SetResult(interp,"The beam is not a sliced beam",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (bin) {
    beam_load_all_bin(beam,file_name,head,axis,read_structure);
  }
  else {
    beam_load_all(beam,file_name,head,axis,read_structure);
  }
  if (read_structure) {
    wakes_prepare(beam);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamSaveGp                                    */
/**********************************************************************/

int tk_BeamSaveGp(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  char *file_name=NULL;
  int axis=0,bunch=0;
  double fixed_energy=-1.0;
  double x,xp,y,yp;
  char *position=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File containing initial beam"},
    {(char*)"-offset",TK_ARGV_INT,(char*)NULL,
     (char*)&axis,
     (char*)"If not 0 the average beam position and angle is not corrected"},
    {(char*)"-bunch",TK_ARGV_INT,(char*)NULL,
     (char*)&bunch,
     (char*)"Number of the bunch to be stored"},
    {(char*)"-fixed_energy",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&fixed_energy,
     (char*)"If larger than 0.0 the energy should be fixed to this value"},
    {(char*)"-position",TK_ARGV_STRING,(char*)NULL,
     (char*)&position,
     (char*)"If specified give position with respect to these values"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (position) {
    x=strtod(position,&position);
    xp=strtod(position,&position);
    y=strtod(position,&position);
    yp=strtod(position,&position);
    beam_save_gp_2(inter_data.bunch,file_name,bunch,x,xp,y,yp,fixed_energy);
  }
  else {
    beam_save_gp(inter_data.bunch,file_name,axis,bunch,fixed_energy);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamGetPosition                               */
/**********************************************************************/

int tk_BeamGetPosition(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error;
  int i,j,n,nb,m;
  double x=0.0,xp=0.0,y=0.0,yp=0.0,wgt=0.0;
  BEAM *beam;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  beam=inter_data.bunch;
  beam_rotate_straight(beam);

  n=beam->slices/beam->bunches;
  nb=beam->bunches;
  m=0;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
#ifdef TWODIM
      x+=beam->particle[m].x*beam->particle[m].wgt;
      xp+=beam->particle[m].xp*beam->particle[m].wgt;
#endif
      y+=beam->particle[m].y*beam->particle[m].wgt;
      yp+=beam->particle[m].yp*beam->particle[m].wgt;
      wgt+=beam->particle[m].wgt;
      m++;
    }
  }
  x/=wgt;
  xp/=wgt;
  y/=wgt;
  yp/=wgt;
  char buf[100];
  snprintf(buf,100,"%g %g %g %g\n",x,xp,y,yp);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamSaveMad                                   */
/**********************************************************************/

int tk_BeamSaveMad(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  char *file_name=NULL;
  int axis=1,bunch=0,np=0,i;
  double fixed_energy=-1.0;
  char *name0="mad",*name;
  char *n[2]={"mad","merlin"};
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Name of the file in which the beam should be stored"},
    {(char*)"-program",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"Name of the program for which the beam should be stored"},
    {(char*)"-particles",TK_ARGV_INT,(char*)NULL,
     (char*)&np,
     (char*)"Number of particles to be stored"},
    {(char*)"-offset",TK_ARGV_INT,(char*)NULL,
     (char*)&axis,
     (char*)"If not 0 the average beam position and angle is not corrected"},
    {(char*)"-bunch",TK_ARGV_INT,(char*)NULL,
     (char*)&bunch,
     (char*)"Number of the bunch to be stored"},
    {(char*)"-fixed_energy",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&fixed_energy,
     (char*)"If larger than 0.0 the energy should be fixed to this value"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */
  name=name0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  for (i=0;i<(int)strlen(name);i++){
    name[i]=tolower(name[i]);
  }
  for (i=0;i<2;i++){
    if (strcmp(n[i],name)==0) {
      data_MAD_track(inter_data.bunch,file_name,np,axis,bunch,fixed_energy,
		     i+1);
      return TCL_OK;
    }
  }
  return TCL_ERROR;
}

/**********************************************************************/
/*                      BeamSetOffsetSine                             */
/**********************************************************************/

int tk_BeamSetOffsetSine(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char *name=NULL,*sn1="start",*sn2="end";
  int n1=0,n2=0;
  BEAM *beam;
  double offset_x=0.0,offset_y=0.0,offset_xp=0.0,offset_yp=0.0,
    lambda=0.0,phi_0=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam to offset"},
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,(char*)&lambda,
     (char*)"wavelength in metres"},
    {(char*)"-phi_0",TK_ARGV_FLOAT,(char*)NULL,(char*)&phi_0,
     (char*)"initial phase in radian"},
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_x,
     (char*)"horizontal offset in micro-metres"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_y,
     (char*)"vertical offset in micro-metres"},
    {(char*)"-angle_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_xp,
     (char*)"horizontal angle in micro-radian"},
    {(char*)"-angle_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_yp,
     (char*)"vertical angle in micro-radian"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,(char*)&sn1,
     (char*)"first particle to be offset"},
    {(char*)"-end",TK_ARGV_STRING,(char*)NULL,(char*)&sn2,
     (char*)"last particle to be offset"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamSetOffsetSine>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (name) {
    beam=get_beam(name);
  }
  else {
    beam=inter_data.bunch;
  }
  if (error=eval_beam_position(interp,beam,sn1,&n1)) return error;
  if (error=eval_beam_position(interp,beam,sn2,&n2)) return error;
  if(lambda<=0.0){
    Tcl_SetResult(interp,"Wavelength lambda needs to be larger than zero in <BeamSetOffsetSine>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  beam_set_range_offset_sine_y(beam,n1,n2,offset_y,lambda,phi_0);
  beam_set_range_offset_sine_yp(beam,n1,n2,offset_yp,lambda,phi_0);
#ifdef TWODIM
  beam_set_range_offset_sine_x(beam,n1,n2,offset_x,lambda,phi_0);
  beam_set_range_offset_sine_xp(beam,n1,n2,offset_xp,lambda,phi_0);
#endif
  return TCL_OK;
}


/**********************************************************************/
/*                      BeamlineSetSine                               */
/**********************************************************************/

int tk_BeamlineSetSine(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error;
  char *elements=NULL,**v;
  int i,girders=0,n,*nel=NULL;
  double offset_y=0.0,lambda=0.0,phi_0;
  Tk_ArgvInfo table[]={
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,(char*)&lambda,
     (char*)"wavelength in metres"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,(char*)&phi_0,
     (char*)"initial phase in radian"},
    {(char*)"-girders",TK_ARGV_INT,(char*)NULL,(char*)&girders,
     (char*)"move only the girders"},
    {(char*)"-elements",TK_ARGV_INT,(char*)NULL,(char*)&elements,
     (char*)"list of elements to be used,\nif no list is given all elements will be moved"},
    {(char*)"-amplitude_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_y,
     (char*)"vertical amplitude in micro-metres"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlineMoveSine>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if(lambda<=0.0){
    Tcl_SetResult(interp,
		  "Wavelength lambda needs to be larger than zero in <BeamlineMoveSine>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (elements&&girders) {
    Tcl_SetResult(interp,
		  "Currently you cannot move the girders and specify a list of elements at the same time in <BeamlineMoveSine>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (girders) {
    beamline_move_sine(inter_data.beamline,offset_y,TWOPI/lambda,phi_0);
  }
  else {
    if (elements) {
      if(error=Tcl_SplitList(interp,elements,&n,&v)) {
	placet_printf(ERROR,"error in -elements\n");
	return error;
      }
      nel=(int*)alloca(sizeof(int)*(n+1));
      for (i=0;i<n;i++){
	if (error=Tcl_GetInt(interp,v[i],nel+i)){
	  return error;
	}
      }
      nel[n]=-1;
      beamline_move_sine_list(inter_data.beamline,nel,
			      offset_y,TWOPI/lambda,phi_0);
      Tcl_Free((char*)v);
    }
    else {
      beamline_move_sine2(inter_data.beamline,offset_y,TWOPI/lambda,phi_0);
    }
  }
  return TCL_OK;
}


/**********************************************************************/
/*                      BeamSetToOffset                               */
/**********************************************************************/

int tk_BeamSetToOffset(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error;
  char *name="workbeam";
  char *sn1="start",*sn2="end";
  int n1=0,n2=0,i;
  BEAM *beam;
  double offset_x=0.0,offset_y=0.0,offset_xp=0.0,offset_yp=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam to offset"},
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_x,
     (char*)"horizontal offset in micro-metres"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_y,
     (char*)"vertical offset in micro-metres"},
    {(char*)"-angle_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_xp,
     (char*)"horizontal angle in micro-radian"},
    {(char*)"-angle_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_yp,
     (char*)"vertical angle in micro-radian"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,(char*)&sn1,
     (char*)"first particle to be offset"},
    {(char*)"-end",TK_ARGV_STRING,(char*)NULL,(char*)&sn2,
     (char*)"last particle to be offset"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamSetToOffset>:\n",
		  TCL_VOLATILE);
    for (i=1;i<argc;i++) {
      Tcl_AppendResult(interp,argc," ",NULL);
    }
    return TCL_ERROR;
  }

  if (!name) {
    Tcl_SetResult(interp,"No beam has been defined in BeamSetToOffset\n",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (strcmp(name,"workbeam")==0) {
    beam=inter_data.bunch;
  }
  else {
    beam=get_beam(name);
  }
  if (error=eval_beam_position(interp,beam,sn1,&n1)) {
    return error;
  }
  if (error=eval_beam_position(interp,beam,sn2,&n2)) {
    return error;
  }
  beam_set_to_range_offset_y(beam,n1,n2,offset_y);
  beam_set_to_range_offset_yp(beam,n1,n2,offset_yp);
#ifdef TWODIM
  beam_set_to_range_offset_x(beam,n1,n2,offset_x);
  beam_set_to_range_offset_xp(beam,n1,n2,offset_xp);
#endif
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamModulateEnergy                            */
/**********************************************************************/

int tk_BeamModulateEnergy(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			  char *argv[])
{
  int error;
  char *name=NULL,*e_list=NULL,**v;
  char *sn1="start",*sn2="end";
  int n1=0,n2=0,i,n,k=0;
  BEAM *beam;
  double *e;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam to offset"},
    {(char*)"-energy",TK_ARGV_STRING,(char*)NULL,(char*)&e_list,
     (char*)"list of energies to be applied"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,(char*)&sn1,
     (char*)"first particle to be modulated"},
    {(char*)"-end",TK_ARGV_STRING,(char*)NULL,(char*)&sn2,
     (char*)"last particle to be modulated"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamModulateEnergy>:\n",
		  TCL_VOLATILE);
    for (i=1;i<argc;i++) {
      Tcl_AppendResult(interp,argc," ",NULL);
    }
    return TCL_ERROR;
  }

  if (!name) {
    Tcl_SetResult(interp,"No beam has been defined in BeamModulateEnergy\n",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  beam=get_beam(name);
  if (error=eval_beam_position(interp,beam,sn1,&n1)) {
    return error;
  }
  if (error=eval_beam_position(interp,beam,sn2,&n2)) {
    return error;
  }
  if(error=Tcl_SplitList(interp,e_list,&n,&v)) return error;
  e=(double*)alloca(sizeof(double)*n);
  for(i=0;i<n;++i){
    if(error=Tcl_GetDouble(interp,v[i],e+i)) return error;
  }

  for(i=n1;i<n2;++i){
    beam->particle[i].energy=e[k];
    ++k;
    if (k>=n) {
      k=0;
    }
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      BnsPlot                                       */
/**********************************************************************/

int tk_BnsPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error;
  char file_default[]="bns.dat",*file_name;
  char *beam_name=NULL;
  double kick=878.0/3.332*1e3;
  double charge=4e9;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,(char*)&charge,
     (char*)"Number of particles in the beam"},
    {(char*)"-kick",TK_ARGV_FLOAT,(char*)NULL,(char*)&kick,
     (char*)"Kick factor at 2 sigmaz [V/(pCm^2)]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=file_default;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to BnsPlot",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  BEAM* beam = get_beam(beam_name);
  bns_plot(inter_data.beamline,beam,file_name,charge,kick);
  return TCL_OK;
}

/**********************************************************************/
/*                      BpmPositionListZ                              */
/**********************************************************************/

int tk_BpmPositionListZ(ClientData /*clientdata*/,Tcl_Interp *interp,
			int argc,char *argv[])
{

  int error,i;
  char no[100];
  double s=0.0;
  Tcl_DString res;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," number\n",NULL);
    return TCL_ERROR;
  }
  Tcl_DStringInit(&res);
  for (i=0;i<inter_data.beamline->n_elements;i++) {
    if (inter_data.beamline->element[i]->is_bpm()) {
      s+=0.5*inter_data.beamline->element[i]->get_length();
      snprintf(no,100,"%g",s);
      Tcl_DStringAppendElement(&res,no);
      s+=0.5*inter_data.beamline->element[i]->get_length();
    }
    else {
      s+=inter_data.beamline->element[i]->get_length();
    }
  }
  Tcl_DStringResult(interp,&res);
  return TCL_OK;
}
/**********************************************************************/
/*                      ApertureLosses                                */
/**********************************************************************/

int tk_ApertureLosses(ClientData /*clientdata*/,Tcl_Interp *interp,
		      int argc,char *argv[])
{

  int error,i;
  char *file_name=NULL;
  double s=0.0,sum=0.0,charge=1.0;
  FILE *file;

  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-charge_norm",TK_ARGV_FLOAT,(char*)NULL,(char*)&charge,
     (char*)"Charge to which to normalise the losses"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," number\n",NULL);
    return TCL_ERROR;
  }
  
  file=open_file(file_name);
  for (i=0;i<inter_data.beamline->n_elements;i++) {
    if (APERTURE *ap=inter_data.beamline->element[i]->aperture_ptr()) {
      s+=0.5*inter_data.beamline->element[i]->get_length();
      sum+=ap->get_weight();
      fprintf(file,"%g %g %g\n",s,ap->get_weight()/charge,sum/charge);
      /////////	fprintf(file,"%g %g %g %g\n",s,ap->get_weight()/charge,sum/charge,ap->v1);
      s+=0.5*inter_data.beamline->element[i]->get_length();
    }
    else {
      s+=inter_data.beamline->element[i]->get_length();
    }
  }
  close_file(file);
  return TCL_OK;
}

/**********************************************************************/
/*                      BunchesSetOffsetRandom                        */
/**********************************************************************/

int tk_BunchesSetOffsetRandom(ClientData /*clientdata*/,Tcl_Interp *interp,
			      int argc,char *argv[])
{
  int error;
  char *name;
  int n1=0,n2=0,j,ns,nb;
  BEAM *beam;
  double offset_x=0.0,offset_y=0.0,offset_xp=0.0,offset_yp=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam to offset"},
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_x,
     (char*)"horizontal offset in micro-metres"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_y,
     (char*)"vertical offset in micro-metres"},
    {(char*)"-angle_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_xp,
     (char*)"horizontal angle in micro-radian"},
    {(char*)"-angle_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&offset_yp,
     (char*)"vertical angle in micro-radian"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&n1,
     (char*)"first bunch to be offset"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&n2,
     (char*)"last bunch to be offset"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BunchesSetOffsetRandom>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=get_beam(name); 
  nb=beam->bunches;
  ns=beam->slices_per_bunch*beam->macroparticles;
  if (n1<=0) {
    n1=0;
  }
  if (n2>=nb) {
    n2=nb;
  }
  for (j=n1;j<n2;j++){
    beam_set_range_offset_y(beam,j*ns,(j+1)*ns,offset_y*Misalignments.Gauss());
    beam_set_range_offset_yp(beam,j*ns,(j+1)*ns,offset_yp*Misalignments.Gauss());
#ifdef TWODIM
    beam_set_range_offset_x(beam,j*ns,(j+1)*ns,offset_x*Misalignments.Gauss());
    beam_set_range_offset_xp(beam,j*ns,(j+1)*ns,offset_xp*Misalignments.Gauss());
#endif
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      CavityDefine                                  */
/**********************************************************************/

int tk_CavityDefine(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error,i,n=0,ne, ne_long;
  char *rf_list=NULL,**v,**v_long,**v2, **v2_long;
  char *ml=NULL,*ml_long=NULL, *name="DecCavity";
  PetsWake *tmp;
  double beta_group_l_onemode, r_over_q_onemode, lambda_longitudinal_onemode;
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the PETS"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&drive_data.l_cav,
     (char*)"Standard length of the cavities (needed for the drive beam)"},
    {(char*)"-transverse_mode_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&ml,
     (char*)"List of transverse wakefield modes"},
    {(char*)"-longitudinal_mode_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&ml_long,
     (char*)"List of longitudinal wakefield modes"},
    /*
      {(char*)"-beta_group_t",TK_ARGV_FLOAT,(char*)NULL,
      (char*)&drive_data.beta_group_t,
      (char*)"Group velocity for the transverse wakefield"},
    */
    {(char*)"-beta_group_l",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&beta_group_l_onemode,
     (char*)"Group velocity for the longitudinal wakefield"},
    /*
      {(char*)"-transverse_wake",TK_ARGV_FLOAT,(char*)NULL,
      (char*)&drive_data.a0,
      (char*)"Transverse delta wakefield in [V/(m^2 pC)]"},
    */
    {(char*)"-r_over_q",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&r_over_q_onemode,
     (char*)"R/Q value for the longitudinal wakefield in Circuit Ohms"},
    /*
      {(char*)"-Q",TK_ARGV_FLOAT,(char*)NULL,
      (char*)&drive_data.q_value,
      (char*)"Q value for the transverse wakefield"},
      {(char*)"-lambda_transverse",TK_ARGV_FLOAT,(char*)NULL,
      (char*)&drive_data.lambda_transverse,
      (char*)"Wavelength of the transverse wakefield"},
    */
    {(char*)"-lambda_longitudinal",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda_longitudinal_onemode,
     (char*)"Wavelength of the longitudinal wakefield"},
    {(char*)"-steps",TK_ARGV_INT,(char*)NULL,
     (char*)&drive_data.steps,
     (char*)"Number of steps for wake tracking"},
    {(char*)"-rf_kick",TK_ARGV_INT,(char*)NULL,
     (char*)&drive_data.rf_kick,
     (char*)"Take into account RF-kick"},
    {(char*)"-rf_long",TK_ARGV_INT,(char*)NULL,
     (char*)&drive_data.rf_long,
     (char*)"Take into account longitudinal RF-kick"},
    {(char*)"-rf_a0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&drive_data.rf_a0,
     (char*)"Aperture to be used for RF-kick"},
    {(char*)"-rf_order",TK_ARGV_INT,(char*)NULL,
     (char*)&drive_data.rf_order,
     (char*)"Basic order of the RF-kick"},
    {(char*)"-rf_size",TK_ARGV_STRING,(char*)NULL,
     (char*)&rf_list,
     (char*)"Amplitudes to be used for RF-kick"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* initialisation of variables */

  drive_data.steps=1;
  drive_data.rf_kick=0;
  drive_data.rf_long=0;

  //rf_data.lambda=lambda_longitudinal_onemode; // to be checked before decomment it

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to CavityDefine",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  //
  // Extract transverse modes
  //
  if(error=Tcl_SplitList(interp,ml,&drive_data.n_mode,&v)) {
    return error;
  }
  if (drive_data.n_mode<1) {
    Tcl_AppendResult(interp,"Error in CavityDefine: no transverse wakefields given",NULL);
    return TCL_ERROR;
  }
  drive_data.lambda_transverse=(double*)malloc(sizeof(double)
					       *drive_data.n_mode);
  drive_data.q_value=(double*)malloc(sizeof(double)
				     *drive_data.n_mode);
  drive_data.k_transverse=(double*)malloc(sizeof(double)
					  *drive_data.n_mode);
  drive_data.a0=(double*)malloc(sizeof(double)
				*drive_data.n_mode);
  drive_data.beta_group_t=(double*)malloc(sizeof(double)
					  *drive_data.n_mode);
  for (i=0;i<drive_data.n_mode;++i) {
    if (error=Tcl_SplitList(interp,v[i],&ne,&v2)){
      return error;
    }
    if (error=Tcl_GetDouble(interp,v2[0],drive_data.lambda_transverse+i)){
      return error;
    }
    if (error=Tcl_GetDouble(interp,v2[1],drive_data.a0+i)){
      return error;
    }
    if (error=Tcl_GetDouble(interp,v2[2],drive_data.q_value+i)){
      return error;
    }
    if (error=Tcl_GetDouble(interp,v2[3],drive_data.beta_group_t+i)){
      return error;
    }
    drive_data.k_transverse[i]=TWOPI/drive_data.lambda_transverse[i]*1e-6;
  }



  //
  // Extract longitudinal modes
  //
  if( ml_long != NULL ) {
    if(error=Tcl_SplitList(interp,ml_long,&drive_data.n_mode_long,&v_long)) {
      return error;
    }
  }
  if ( (drive_data.n_mode_long<1) || (ml_long == NULL) ) {
    // ensure backwards compability (individual one mode parameters, instead of list of modes)
    drive_data.lambda_longitudinal=(double*)malloc(sizeof(double));
    drive_data.q_value_long=(double*)malloc(sizeof(double));
    drive_data.k_longitudinal=(double*)malloc(sizeof(double));
    drive_data.r_over_q=(double*)malloc(sizeof(double));
    drive_data.beta_group_l=(double*)malloc(sizeof(double));
    drive_data.lambda_longitudinal[0] = lambda_longitudinal_onemode;
    drive_data.q_value_long[0] = 1e12; 
    drive_data.beta_group_l[0] = beta_group_l_onemode;
    drive_data.k_longitudinal[0] = TWOPI/lambda_longitudinal_onemode*1e-6;
    drive_data.r_over_q[0] = r_over_q_onemode;
    drive_data.n_mode_long = 1;
  } else {
    drive_data.lambda_longitudinal=(double*)malloc(sizeof(double)
						   *drive_data.n_mode_long);
    drive_data.q_value_long=(double*)malloc(sizeof(double)
					    *drive_data.n_mode_long);
    drive_data.k_longitudinal=(double*)malloc(sizeof(double)
					      *drive_data.n_mode_long);
    drive_data.r_over_q=(double*)malloc(sizeof(double)
					*drive_data.n_mode_long);
    drive_data.beta_group_l=(double*)malloc(sizeof(double)
					    *drive_data.n_mode_long);

    for (i=0;i<drive_data.n_mode_long;++i) {
      if (error=Tcl_SplitList(interp,v_long[i],&ne_long,&v2_long)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,v2_long[0],drive_data.lambda_longitudinal+i)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,v2_long[1],drive_data.r_over_q+i)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,v2_long[2],drive_data.q_value_long+i)){
	return error;
      }
      if (error=Tcl_GetDouble(interp,v2_long[3],drive_data.beta_group_l+i)){
	return error;
      }
      drive_data.k_longitudinal[i]=TWOPI/drive_data.lambda_longitudinal[i]*1e-6;
    }
  }


  if(drive_data.rf_kick||drive_data.rf_long){
    drive_data.rf_a0_i=1.0/drive_data.rf_a0;
    if(error=Tcl_SplitList(interp,rf_list,&n,&v)){
      return error;
    }
    drive_data.rf_size=(double*)xmalloc(sizeof(double)*n);
    for(i=0;i<n;i++){
      if(error=Tcl_GetDouble(interp,v[i],&drive_data.rf_size[i])){
	return error;
      }
    }
    Tcl_Free((char*)v);
  }
  drive_data.n_rf=n;
  if (name) {
    tmp=(PetsWake*)malloc(sizeof(PetsWake));
    *tmp=drive_data;
    Placet_CreateCommand(interp,name,&tk_DecCavity,tmp,NULL);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      CavityGetGradientList                         */
/**********************************************************************/

int tk_CavityGetGradientList(ClientData /*clientdata*/,Tcl_Interp *interp,
			     int argc,char *argv[])
{
  int error,j_el=0,n_el;
  char no[100];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command returns a list with the gradient of all cavities."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  n_el=inter_data.beamline->n_elements;
  for (j_el=0;j_el<n_el;j_el++){
    if (CAVITY *cav=inter_data.beamline->element[j_el]->cavity_ptr()) {
      snprintf(no,100,"%20g\n",cav->get_gradient());
      Tcl_AppendResult(interp,no,NULL);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      CavityGetPhaseList                            */
/**********************************************************************/

int tk_CavityGetPhaseList(ClientData /*clientdata*/,Tcl_Interp *interp,
			  int argc,char *argv[])
{
  int error,j_el=0,n_el;
  char no[100];
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command returns a list with the phase of all cavities."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  n_el=inter_data.beamline->n_elements;
  for (j_el=0;j_el<n_el;j_el++){
    if (CAVITY *cav=inter_data.beamline->element[j_el]->cavity_ptr()) {
      snprintf(no,100,"%20g\n",180.0*cav->get_phase()/M_PI);
      Tcl_AppendResult(interp,no,NULL);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      CavityGetTypeList                             */
/**********************************************************************/

int tk_CavityGetTypeList(ClientData /*clientdata*/,Tcl_Interp *interp,
			 int argc,char *argv[])
{
  int error,j_el=0,n_el;
  char no[100];
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command returns a list with the gradient of all cavities."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  n_el=inter_data.beamline->n_elements;
  for (j_el=0;j_el<n_el;j_el++){
    if (CAVITY *cav=inter_data.beamline->element[j_el]->cavity_ptr()) {
      snprintf(no,100,"%d\n",cav->get_field());
      Tcl_AppendResult(interp,no,NULL);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      CavitySetGradientList                         */
/**********************************************************************/

int tk_CavitySetGradientList(ClientData /*clientdata*/,Tcl_Interp *interp,
			     int argc,char *argv[])
{
  int error,j,n,n_el,j_el=0;
  char **v;
  double value;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command takes a list, it sets the gradients of all cavities to the corresponding values of the list."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," value_list\n",NULL);
    return TCL_ERROR;
  }
  if (error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
  n_el=inter_data.beamline->n_elements;
  for (j=0;j<n;j++){
    if (error=Tcl_GetDouble(interp,v[j],&value)) return error;
    if (j_el>=n_el) {
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Too many values in list\n",NULL);
      return TCL_ERROR;
    }
    while (!inter_data.beamline->element[j_el]->is_cavity()) {
      j_el++;
      if (j_el>=n_el) {
	Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
			 "Too many values in list\n",NULL);
	return TCL_ERROR;
      }
    }
    if (CAVITY *cav=inter_data.beamline->element[j_el]->cavity_ptr()) {
      cav->set_gradient(value);
    }
    j_el++;
    //    placet_printf(INFO,"step %d %d %g\n",j,j_el,value);
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      CavitySetPhaseList                            */
/**********************************************************************/

int tk_CavitySetPhaseList(ClientData /*clientdata*/,Tcl_Interp *interp,
			  int argc,char *argv[])
{
  int error,j,n,n_el,j_el=0;
  char **v;
  double value;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command takes a list, it sets the phases of all cavities to the corresponding values of the list."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," value_list\n",NULL);
    return TCL_ERROR;
  }
  if (error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
  n_el=inter_data.beamline->n_elements;
  for (j=0;j<n;j++){
    if (error=Tcl_GetDouble(interp,v[j],&value)) return error;
    if (j_el>=n_el) {
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Too many values in list\n",NULL);
      return TCL_ERROR;
    }
    while (!inter_data.beamline->element[j_el]->is_cavity()) {
      j_el++;
      if (j_el>=n_el) {
	Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
			 "Too many values in list\n",NULL);
	return TCL_ERROR;
      }
    }
    if (CAVITY *cav=inter_data.beamline->element[j_el]->cavity_ptr()) {
      cav->set_phase(PI/180.0*value);
    }
    j_el++;
    //    placet_printf(INFO,"step %d %d %g\n",j,j_el,value);
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      CavitySetTypeList                             */
/**********************************************************************/

int tk_CavitySetTypeList(ClientData /*clientdata*/,Tcl_Interp *interp,
			 int argc,char *argv[])
{
  int error,j,n,n_el,j_el=0;
  char **v;
  int value;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command takes a list, it sets the field type of all cavities to the corresponding values of the list."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," value_list\n",NULL);
    return TCL_ERROR;
  }
  if (error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
  n_el=inter_data.beamline->n_elements;
  for (j=0;j<n;j++){
    if (error=Tcl_GetInt(interp,v[j],&value)) return error;
    if (j_el>=n_el) {
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Too many values in list\n",NULL);
      return TCL_ERROR;
    }
    while (!inter_data.beamline->element[j_el]->is_cavity()) {
      j_el++;
      if (j_el>=n_el) {
	Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
			 "Too many values in list\n",NULL);
	return TCL_ERROR;
      }
    }
    if (CAVITY *cav=inter_data.beamline->element[j_el]->cavity_ptr()) {
      cav->set_field(value);
    }
    j_el++;
    //    placet_printf(INFO,"step %d %d %g\n",j,j_el,value);
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      CheckAutophase                                */
/**********************************************************************/


/* Interface to command check_autophase in Tcl: CheckAutophase */

int tk_CheckAutophase(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  char *file_name="autophase.dat";
  char *format = default_format;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"results file format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to CheckAutophase",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  check_autophase(inter_data.beamline,inter_data.bunch,file_name,format);
  return TCL_OK;
}

/**********************************************************************/
/*                      DipoleSetZero                                 */
/**********************************************************************/

int tk_DipoleSetZero(ClientData /*clientdata*/,Tcl_Interp * /*interp*/,
		     int /*argc*/,char * /*argv*/[])
{
  int i,n;
  n=inter_data.beamline->n_elements;
  for (i=0;i<n;i++){
    if (DIPOLE *dip=inter_data.beamline->element[i]->dipole_ptr()) {
      dip->set_strength_x(0.0);
      dip->set_strength_y(0.0);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      DispersionPlot                                */
/**********************************************************************/

int tk_DispersionPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  char file_default[]="twiss.dat",*file_name;
  char *beam_name=NULL,*energy_list=NULL,**v,*point;
  int i,i_ref,n;
  double *e;
  BEAM *beam2;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-reference",TK_ARGV_INT,(char*)NULL,(char*)&i_ref,
     (char*)"Reference slice"},
    {(char*)"-energy",TK_ARGV_STRING,(char*)NULL,(char*)&energy_list,
     (char*)"Energy list"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=file_default;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to DispersionPlot",
		    TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if(error=Tcl_SplitList(interp,energy_list,&n,&v)) return error;
  e=(double*)alloca(sizeof(double)*n);
  for(i=0;i<n;i++){
    e[i]=strtod(v[i],&point);
  }
  Tcl_Free((char*)v);
  BEAM* beam = get_beam(beam_name);
  beam2=beam_make_dispersion(beam,e,n,i_ref);
  dispersion_plot(inter_data.beamline,beam2,file_name);
  beam_delete(beam2);
  return TCL_OK;
}

/**********************************************************************/
/*                      DivideBeamline                                */
/**********************************************************************/

int tk_DivideBeamline(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  char *file_name=NULL;
  int n=0;
  double length=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-n_cav",TK_ARGV_INT,(char*)NULL,(char*)&n,
     (char*)"Number of cavities per drivebeam"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"Expected length per drivebeam"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to DivideBeamline",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if (n<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"n_cav must be larger than 0",NULL);
  }
  if (divide_linac(inter_data.beamline,file_name,n,length)){
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      DriveBeam                                     */
/**********************************************************************/

int tk_DriveBeam(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error,ok,n,n2,i;
  Tcl_HashEntry *entry;
  //  char *file_name = (char*)"beam.dat";
  char *e_dis=NULL,**v,*bunch_list=NULL,
    **v2,*slice_list=NULL;
  DRIVE_BEAM_PARAM param;
  char *slice_energy_list = NULL;
  int n_max=0;
  double macro_offset_y = 0.0;
  double real_last_wgt = 1.0;

  Tk_ArgvInfo table[]={
    {(char*)"-macroparticles",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_macro,
     (char*)"Number of particles per slice"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.distance,
     (char*)"Distance between bunches"},
    {(char*)"-envelope",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.envelope,
     (char*)"Beam Envelope in sigma"},
    {(char*)"-envelope_wgt",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.envelope_wgt,
     (char*)"Weight of particles on the envelope"},
    {(char*)"-energy_distribution",TK_ARGV_STRING,(char*)NULL,
     (char*)&e_dis,
     (char*)"Relative energy Distribution with a slice (only activated if  -macroparticles > 1)"},
    {(char*)"-bunches",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_bunch,
     (char*)"Number of bunches"},
    {(char*)"-ramp",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_ramp,
     (char*)"Number of bunches"},
    {(char*)"-ramp_start",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.s_ramp,
     (char*)"Number of bunches"},
    {(char*)"-ramp_step",TK_ARGV_INT,(char*)NULL,
     (char*)&param.ramp_step,
     (char*)"Number of bunches"},
    {(char*)"-slice_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&slice_list,
     (char*)"List of the slice position and weight"},
    {(char*)"-real_last_wgt",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&real_last_wgt,
     (char*)"Set weight of the last bunch for real initial beam [NB: results in wrong single bunch effect]"},
    {(char*)"-last_wgt",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.last_wgt,
     (char*)"Weight of the last bunch for BPM readings (you get emittance measurements)"},
    {(char*)"-macro_offset_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&macro_offset_y,
     (char*)"rms y offset for macroparticles of each slice"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.e0,
     (char*)"Beam energy at entrance [GeV]"},
    {(char*)"-e0_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&slice_energy_list,
     (char*)"List of beam slices energies at entrance [GeV] {e0 e1...} [OVERRIDES -e0 if present]"},
#ifdef TWODIM
    {(char*)"-beta_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betax,
     (char*)"Horizontal beta function at entrance (required)"},
    {(char*)"-alpha_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphax,
     (char*)"Horizontal alpha at entrance (required)"},
    {(char*)"-emitt_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emittx,
     (char*)"Horizontal emittance at entrance (required)"},
    {(char*)"-phi_0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.phi0,
     (char*)"axial rotation angle [radian]"},
#endif
    {(char*)"-beta_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betay,
     (char*)"Vertical beta function at entrance (required)"},
    {(char*)"-alpha_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphay,
     (char*)"Vertical alpha at entrance (required)"},
    {(char*)"-emitt_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emitty,
     (char*)"Vertical emittance at entrance (required)"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.charge,
     (char*)"Bunch charge"},
    {(char*)"-bunch_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&bunch_list,
     (char*)"List of bunch properties"},
    {(char*)"-particles",TK_ARGV_INT,(char*)NULL,
     (char*)&n_max,
     (char*)"Number of particles for particle beam (used when converting from slices to particles)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /*
    {(char*)"-energyspread",TK_ARGV_FLOAT,(char*)NULL,
    (char*)&param.espread,
    (char*)"Energy spread of intial beam"},
    {(char*)"-ecut",TK_ARGV_FLOAT,(char*)NULL,
    (char*)&param.ecut,
    (char*)"Cut of the energy spread of intial beam"},
    {(char*)"-resistive_wall",TK_ARGV_INT,(char*)NULL,
    (char*)&param.resist,
    (char*)"include resistive wall (not active in the moment)"},
  */
  /* default values */

  param.z0=NULL;
  param.n_z0=0;
  param.envelope=0.0;
  param.envelope_wgt=0.0;
  param.resist=0;
  param.n_ramp=0;
  param.ramp_step=1;
  param.s_ramp=1.0;
  param.charge=0.95e11;
  param.e0=1.5;
  param.n_slice=11;
  param.n_bunch=1;
  param.n_macro=1;
  param.espread=0.0;
  param.ecut=3.0;
  param.distance=-1e30;
  param.alphax=0.0;
  param.alphay=0.0;
  param.betax=0.0;
  param.betay=0.0;
  param.emittx=0.0;
  param.emitty=0.0;
  param.phi0=0.0;
  param.slice_init_energy=NULL;
  param.last_wgt = 1.0;
  n_max = 0;

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  /* perform checks */

  if (argc!=2){
    Tcl_AppendResult(interp,"Wrong number of arguments to ",argv[0],"\n",NULL);
    Tcl_AppendResult(interp,"Usage: ",argv[0]," beamname /options/\n",NULL);
    Tcl_AppendResult(interp,"leftover arguments:\n",NULL);
    for (i=1;i<argc;i++){
      Tcl_AppendResult(interp,argv[i],"\n",NULL);
    }
    return TCL_ERROR;
  }

  if (param.n_macro<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-macroparticles must be at least 1\n",NULL);
    error=TCL_ERROR;
  }

  if (param.n_bunch<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-bunches must be at least 1\n",NULL);
    error=TCL_ERROR;
  }

  if (param.n_slice<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-slices must be at least 1\n",NULL);
    error=TCL_ERROR;
  }

  if (param.distance<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-distance must be defined and larger than 0\n",NULL);
    error=TCL_ERROR;
  }

  if ((param.espread>0.0)&&(param.n_macro==1)) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"espread>0.0 but macroparticles=1\n",NULL);
    error=TCL_ERROR;
  }

  if (slice_list==NULL) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-slice_list must be defined\n",NULL);
    return TCL_ERROR;
  }
  else {
    if (error=Tcl_SplitList(interp,slice_list,&n,&v)) return error;
    param.slice_z_pos=(double*)xmalloc(sizeof(double)*n);
    param.slice_wgt=(double*)xmalloc(sizeof(double)*n);
    for (i=0;i<n;i++){
      if (error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
      if (n2!=2) {
	Tcl_AppendResult(interp,"each element of slice_list must contain exactly two values",NULL);
	return TCL_ERROR;
      }
      if (error=Tcl_GetDouble(interp,v2[0],param.slice_z_pos+i)) return error;
      if (error=Tcl_GetDouble(interp,v2[1],param.slice_wgt+i)) return error;
      Tcl_Free((char*)v2);
    }
    param.n_slice=n;
    Tcl_Free((char*)v);
  }

  if (e_dis){
    if(error=Tcl_SplitList(interp,e_dis,&n,&v)) return error;
    param.d_energy=(double*)xmalloc(sizeof(double)*n);
    param.eslice_wgt=(double*)xmalloc(sizeof(double)*n);
    param.n_energy=n;
    for (i=0;i<n;i++){
      if (error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
      if (n2!=2) {
	return TCL_ERROR;
      }
      if (error=Tcl_GetDouble(interp,v2[0],param.d_energy+i)) return error;
      if (error=Tcl_GetDouble(interp,v2[1],param.eslice_wgt+i)) return error;
      placet_printf(INFO,"wgt_energy %g %g\n",param.d_energy[i],param.eslice_wgt[i]);
    }
    Tcl_Free((char*)v);
  }
  else{
    param.d_energy=(double*)xmalloc(sizeof(double));
    param.eslice_wgt=(double*)xmalloc(sizeof(double));
    param.n_energy=1;
    param.d_energy[0]=0.0;
    param.eslice_wgt[0]=1.0;
  }
  if (bunch_list) {
    if (param.n_ramp!=0) {
      placet_printf(WARNING,"WARNING: ramp defined together with bunch_list");
      placet_printf(WARNING,"         ramp will be set to 0");
      param.n_ramp=0;
    }
    if(error=Tcl_SplitList(interp,bunch_list,&n,&v)) return error;
    if (n<param.n_bunch) {
      placet_printf(INFO,"The bunch list must contain at least as many bunches as specified in n_bunch\n");
      return TCL_ERROR;
    }
    param.z0=(double*)xmalloc(sizeof(double)*n);
    param.ch=(double*)xmalloc(sizeof(double)*n);
    param.bucket=(int*)xmalloc(sizeof(int)*n);
    param.n_z0=n;
    for (i=0;i<n;i++) {
      if(error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
      if (error=Tcl_GetInt(interp,v2[0],param.bucket+i)) return error;
      if (error=Tcl_GetDouble(interp,v2[1],param.z0+i)) return error;
      if (error=Tcl_GetDouble(interp,v2[2],param.ch+i)) return error;
      //      param.z0[i]=strtod(v[i],&point);
    }
    Tcl_Free((char*)v);
  }
  else {
    param.bucket=NULL;
  }
#ifdef TWODIM
  if (param.betax<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emittx<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }
#endif
  if (param.betay<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emitty<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  //
  // define the beam energy as an energy distribution per slice
  // 
  if (slice_energy_list==NULL) {
  } else {
    if (error=Tcl_SplitList(interp,slice_energy_list,&n,&v)) return error;
    if( n < 1 ) {
      // no error if a list with 0 elements is defined ( will use e0 instad - default value if necessary) )
    } else {
      if( n != param.n_slice ) {
	check_error(interp,argv[0],error);
	placet_printf(INFO,"EA: n: %d, n_slice: %d", n, param.n_slice);
	Tcl_AppendResult(interp,"ERROR: The energy distribution must contain the same number of elements as the charge distribution\n",NULL);
	return TCL_ERROR;
      }
      param.slice_init_energy=(double*)xmalloc(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
	if (n2!=1) {
	  Tcl_AppendResult(interp,"each element of the slice_energy_list must contain exactly one value",NULL);
	  return TCL_ERROR;
	}
	if (error=Tcl_GetDouble(interp,v2[0],param.slice_init_energy+i)) return error;
	//Tcl_Free((char*)v2);
      }
      Tcl_Free((char*)v);
      
      //placet_printf(INFO,"EA: slices_energy_list: %g %g\n", param.slice_init_energy[0],param.slice_init_energy[1] );  
    }
  }

  if (error) return error;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  /* run command */
  //  if ((param.n_macro>1)||(drive_data.steps>=1)){
  //    inter_data.bunch=make_multi_bunch_drive_rf(inter_data.beamline,&param,
  //					       &drive_data, n_max);
  //  }
  //  else{
  //  //    inter_data.bunch=make_multi_bunch_drive(inter_data.beamline,file_name,
  //  //					    &param);
  inter_data.bunch=make_multi_bunch_drive_rf(inter_data.beamline,&param,
					     &drive_data, n_max);
  //  }

  // scatter macroparticles (make a distributed beam of macroparticles for e.g. loss studies)
  if( macro_offset_y > 0.0 ) {
    beam_set_to_macro_offset_xy(inter_data.bunch, 0, macro_offset_y, param.d_energy, param.eslice_wgt);
  };

  // add real last wgt to last macroparticle (to simulate a loong train)
  if( real_last_wgt > 1.0 ) {
    beam_set_real_last_weight(inter_data.bunch, real_last_wgt);
  };


  /* create new command to manipulate beam */

  Placet_CreateCommand(interp,argv[1],&tk_Beam,inter_data.bunch,
		       NULL);	// 

  /* store beam in beam table */

  entry=Tcl_CreateHashEntry(&BeamTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,inter_data.bunch);
    return TCL_OK;
  }
  check_error(interp,argv[0],error);
  Tcl_AppendResult(interp,"Failed to create beam ",argv[1],NULL);
  return TCL_ERROR;
}

/*
  This routine aligns the BPMs to the last beam position
*/

int tk_BpmRealign(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  double sx=0.0,sy=0.0;
  int nb=-1;
  Tk_ArgvInfo table[]={
    {(char*)"-error_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sx,
     (char*)"Error in horizontal plane"},
    {(char*)"-error_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sy,
     (char*)"Error in vertical plane"},
    {(char*)"-bunch",TK_ARGV_INT,(char*)NULL,
     (char*)&nb,
     (char*)"Which bunch to use as a reference (-1: mean of all bunches)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (nb<0) {
    bpm_freeze(inter_data.beamline,sx,sy);
  }
  else {
    bpm_freeze_bunch(inter_data.beamline,sx,sy,nb);
  }
  return TCL_OK;
}


/**********************************************************************/
/*                      EarthField                                    */
/**********************************************************************/

int tk_EarthField(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  double bx=0.0,by=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-bx",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&bx,
     (char*)"Horizontal earth magnetic field [T]"},
    {(char*)"-by",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&by,
     (char*)"Vertical earth magnetic field [T]"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"This command defines the earth magnetic field."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <EarthField>",TCL_VOLATILE);
    return TCL_ERROR;
  }
#ifndef EARTH_FIELD
  Tcl_SetResult(interp,"Error: <EarthField> cannot be used, recompile with EARTH_FIELD defined",TCL_VOLATILE);
  return TCL_ERROR;
#else
  earth_field.x=by*0.3e6; // vert. magnetic field works on hor. direction!
  earth_field.y=bx*0.3e6;
  return TCL_OK;
#endif
}

/**********************************************************************/
/*                      EnergySpreadPlot                              */
/**********************************************************************/

int tk_EnergySpreadPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error, head=0;
  char file_default[]="espread.dat",*file_name;
  char *beam_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)"-header",TK_ARGV_INT,(char*)NULL,
     (char*)&head,
     (char*)"If set to 1 write a header into the file"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=file_default;
  beamline_survey_hook_interp=interp;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to EnergySpreadPlot",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  BEAM* beam = get_beam(beam_name);
  energy_spread_plot(inter_data.beamline,beam,file_name,head);
  return TCL_OK;
}

/**********************************************************************/
/*                      EnvelopeCut                                   */
/**********************************************************************/

int tk_EnvelopeCut(ClientData /*clientdata*/,Tcl_Interp * /*interp*/,int /*argc*/,
		   char *argv[])
{
  char *point;
  switches.envelope_cut=strtod(argv[1],&point);
  return TCL_OK;
}

/**********************************************************************/
/*                      FirstOrder                                    */
/**********************************************************************/

int tk_FirstOrder(ClientData /*clientdata*/,Tcl_Interp * /*interp*/,int /*argc*/,
		  char *argv[])
{
  char *point;
  placet_switch.first_order=strtol(argv[1],&point,10);
  return TCL_OK;
}

/**********************************************************************/
/*                      LongBumps                                     */
/**********************************************************************/

int tk_LongBumps(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  char *point;
  if (argc>1) {
    bump_long=strtol(argv[1],&point,10);
  }
  char buf[20];
  snprintf(buf,20,"%d",bump_long);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      Help                                          */
/**********************************************************************/

int tk_Help(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	    char *argv[])
{
  int error = TCL_OK;
  Tk_ArgvInfo table[] = {
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"This command list all available commands."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error = Tk_ParseArgv(interp,NULL,&argc,argv,table,0)) != TCL_OK){
    return error;
  }
  if (argc != 1 && argc != 2) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  if (argc == 1) {
    for (std::vector<COMMAND_HELPER>::const_iterator itr=help_head.begin(); itr!=help_head.end(); ++itr) {
      placet_printf(INFO,"%s\n",(*itr).name.c_str());
    }
  } else {
    std::vector<COMMAND_HELPER>::const_iterator itr = std::find(help_head.begin(), help_head.end(), argv[1]);
    if (itr != help_head.end())
      (*itr).display_help_text();
    else {
      Tcl_AppendResult(interp,"Command: `",argv[1],"' unknown\n",NULL);
      return TCL_ERROR;  
    }
  }
  return TCL_OK;
}
/**********************************************************************/
/*                      Help                                          */
/**********************************************************************/

int tk_Help2(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	     char *argv[])
{
  int error=TCL_OK;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"This command list all available commands."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  for (std::vector<COMMAND_HELPER>::const_iterator itr=help_head.begin(); itr!=help_head.end(); ++itr) {
    (*itr).display_help_text();
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      InjectorBeam                                  */
/**********************************************************************/

int tk_InjectorBeam(ClientData /*clientdata*/,Tcl_Interp *interp,
		    int argc,char *argv[])
{
  int error,ok,i,n_t,j,bunches_per_train=41,n_max,n,n2,silent=0;
  char *file_name=NULL,*chargelist=NULL,**v,*beamload_name=NULL,*e_dis=NULL;
  char **v2;
  INJECT_BEAM_PARAM param;
  SPLINE *beaml=NULL;
  double z,*ch,wake_t,wake_l;

  Tk_ArgvInfo table[]={
    {(char*)"-macroparticles",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_macro,
     (char*)"Number of macroparticles per slice"},
    {(char*)"-silent",TK_ARGV_INT,(char*)NULL,
     (char*)&silent,
     (char*)"Suppress output at beam creation"},
    {(char*)"-energyspread",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.espread,
     (char*)"Energy spread of initial beam, minus value is linear spread, positive gaussian spread"},
    {(char*)"-ecut",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.ecut,
     (char*)"Cut of the energy spread of initial beam"},
    {(char*)"-energy_distribution",TK_ARGV_STRING,(char*)NULL,
     (char*)&e_dis,
     (char*)"Energy distribution of initial beam"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the single bunch parameters"},
    {(char*)"-bunches",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_bunch,
     (char*)"Number of bunches"},
    {(char*)"-chargelist",TK_ARGV_STRING,(char*)NULL,
     (char*)&chargelist,
     (char*)"List of bunch charges (required)"},
    {(char*)"-slices",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_slice,
     (char*)"Number of slices"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.e0,
     (char*)"Beam energy at entrance"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.charge,
     (char*)"Bunch charge"},
    {(char*)"-particles",TK_ARGV_INT,(char*)NULL,
     (char*)&n_max,
     (char*)"Number of particles for particle beam"},
    {(char*)"-last_wgt",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.last_wgt,
     (char*)"Weight of the last bunch for the emittance"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.distance,
     (char*)"Bunch distance"},
    {(char*)"-overlapp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.overlapp,
     (char*)"Bunch overlap"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.phase,
     (char*)"Bunch phase"},
    {(char*)"-wake_scale_t",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&wake_t,
     (char*)"Wakefield scaling transverse"},
    {(char*)"-wake_scale_l",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&wake_l,
     (char*)"Wakefield scaling longitudinal"},
#ifdef TWODIM
    {(char*)"-beta_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betax,
     (char*)"Horizontal beta function at entrance (required)"},
    {(char*)"-alpha_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphax,
     (char*)"Horizontal alpha at entrance (required)"},
    {(char*)"-emitt_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emittx,
     (char*)"Horizontal emittance at entrance (required)"},
#endif
    {(char*)"-beta_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betay,
     (char*)"Vertical beta function at entrance (required)"},
    {(char*)"-alpha_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphay,
     (char*)"Vertical alpha at entrance (required)"},
    {(char*)"-emitt_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emitty,
     (char*)"Vertical emittance at entrance (required)"},
    {(char*)"-beamload",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamload_name,
     (char*)"Spline containing the longtudinal beam loading"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* default values */

  param.charge=9.5e10;
  param.e0=0.05;
  param.n_slice=11;
  param.n_bunch=1;
  param.n_macro=1;
  param.espread=0.0;
  param.ecut=3.0;
  param.alphax=0.0;
  param.alphay=0.0;
  param.betax=0.0;
  param.betay=0.0;
  param.emittx=0.0;
  param.emitty=0.0;
  param.attenuation=1.0;
  param.distance=0.0;
  param.overlapp=0.0;
  param.phase=0.0;
  param.last_wgt=1.0;
  n_max=0;
  wake_t=1.0;
  wake_l=1.0;

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  // scd tmp
  // if (inter_data.beamline->n_phases==0) {
  //   inter_data.beamline->phase[0]=0.0;
  //   inter_data.beamline->n_phases=1;
  // }

  /* perform checks */

  if (argc!=2){
    Tcl_AppendResult(interp,"Wrong number of arguments to ",argv[0],"\n",NULL);
    for (i=0;i<argc;i++) {
      Tcl_AppendResult(interp,argv[0]," ",NULL);
    }
    Tcl_AppendResult(interp,"\nUsage: ",argv[0]," beamname /options/\n",NULL);
    return TCL_ERROR;
  }

  if (param.n_macro<1) {
    Tcl_AppendResult(interp,"-macroparticles must be at least 1",NULL);
    error=TCL_ERROR;
  }

#ifdef TWODIM
  if (param.betax<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emittx<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }
#endif
  if (param.betay<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emitty<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (chargelist==NULL) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"chargelist has to be specified\n",NULL);
    error=TCL_ERROR;
  }
  if (error!=TCL_OK) return error;
  
  error=Tcl_SplitList(interp,chargelist,&bunches_per_train,&v);
  if (!error) {
    ch=(double*)alloca(sizeof(double)*bunches_per_train);
    for (i=0;i<bunches_per_train;i++){
      if (error=Tcl_GetDouble(interp,v[i],&ch[i])){
	return error;
      }
    }
  }
  Tcl_Free((char*)v);

  VERBOSITY output_mode = get_verbosity();
  if(silent==1){
    set_verbosity(ERROR);
  }

  if (e_dis){
    if(error=Tcl_SplitList(interp,e_dis,&n,&v)) return error;
    param.d_energy=(double*)xmalloc(sizeof(double)*n);
    param.eslice_wgt=(double*)xmalloc(sizeof(double)*n);
    param.n_macro=n;
    for (i=0;i<n;i++){
      if (error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
      if (n2!=2) {
	return TCL_ERROR;
      }
      if (error=Tcl_GetDouble(interp,v2[0],param.d_energy+i)) return error;
      if (error=Tcl_GetDouble(interp,v2[1],param.eslice_wgt+i)) return error;
      placet_printf(VERBOSE,"wgt_energy %g %g\n",param.d_energy[i],param.eslice_wgt[i]);
      Tcl_Free((char*)v2);
    }
    Tcl_Free((char*)v);
  }
  else {
    param.d_energy=NULL;
  }

  if (beamload_name) {
    beaml = get_spline(beamload_name);
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  /* run command */

  param.attenuation=exp(-PI*param.distance
			/(injector_data.lambda[0]*injector_data.q_value));
  // New to allow last ramp
  //  n_t=(int)((param.n_bunch+bunches_per_train-1)/bunches_per_train);
  //  param.bunch=(BUNCHINFO*)malloc(sizeof(BUNCHINFO)*n_t*bunches_per_train);
  n_t=(int)(param.n_bunch/bunches_per_train);
  param.bunch=(BUNCHINFO*)malloc(sizeof(BUNCHINFO)*param.n_bunch);
  z=0.0;
  for (j=0;j<n_t;j++){
    for (i=0;i<bunches_per_train;i++){
      param.bunch[j*bunches_per_train+i].z=z*1e6;
      param.bunch[j*bunches_per_train+i].ch=ch[i];
      z+=param.distance;
    }
    z-=param.distance+param.overlapp;
  }
  // Also new - remaining bunches, not a full train
  for (i=n_t*bunches_per_train;i<param.n_bunch;i++){
    param.bunch[i].z=z*1e6;
    param.bunch[i].ch=ch[i-n_t*bunches_per_train];
    z+=param.distance;
  }
  if (file_name!=NULL) {
    // Also new
    inter_data.bunch=
      make_multi_bunch_inject(inter_data.beamline,file_name,
			      beaml,&param,param.n_bunch,n_max);
    inter_data.bunch->factor*=wake_l;
    inter_data.bunch->transv_factor*=wake_t;
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Need to specify a filename for the single bunch parameters\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;
  /* create new command to manipulate beam */

  Placet_CreateCommand(interp,argv[1],&tk_Beam,
		       inter_data.bunch,NULL);

  set_verbosity(output_mode);

  /* store beam in table */
  
  Tcl_HashEntry *entry=Tcl_CreateHashEntry(&BeamTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,inter_data.bunch);
    return TCL_OK;
  }
  check_error(interp,argv[0],error);
  Tcl_AppendResult(interp,"Failed to create beam ",argv[1],NULL);

  return TCL_ERROR;
}

/**********************************************************************/
/*                      TestBeam                                      */
/**********************************************************************/

int tk_TestBeam(ClientData /*clientdata*/,Tcl_Interp *interp,
		int argc,char *argv[])
{
  int error,ok,i,k,n_t,j,bunches_per_train=41,n_max;
  Tcl_HashEntry *entry;
  char *file_name=NULL,*chargelist=NULL,**v,*beamload_name=NULL;
  INJECT_BEAM_PARAM param;
  SPLINE *beaml=NULL;
  double alphax,alphay,betax,betay,epsx,epsy;
  double z,*ch,wake_t,wake_l;
  //    {(char*)"-bunches_per_train",TK_ARGV_INT,(char*)NULL,
  //   (char*)&bunches_per_train,
  //   (char*)"Number of bunches per train"},
  Tk_ArgvInfo table[]={
    {(char*)"-macroparticles",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_macro,
     (char*)"Number of macroparticles per slice"},
    {(char*)"-energyspread",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.espread,
     (char*)"Energy spread of initial beam"},
    {(char*)"-ecut",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.ecut,
     (char*)"Cut of the energy spread of initial beam"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File with beam parameters"},
    {(char*)"-bunches",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_bunch,
     (char*)"Number of bunches"},
    {(char*)"-chargelist",TK_ARGV_STRING,(char*)NULL,
     (char*)&chargelist,
     (char*)"List of bunch charges (required)"},
    {(char*)"-slices",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_slice,
     (char*)"Number of slices"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.e0,
     (char*)"Beam energy at entrance"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.charge,
     (char*)"Bunch charge"},
    {(char*)"-particles",TK_ARGV_INT,(char*)NULL,
     (char*)&n_max,
     (char*)"Number of particles for particle beam"},
    {(char*)"-last_wgt",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.last_wgt,
     (char*)"Weight of the last bunch for the emittance"},
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.distance,
     (char*)"Bunch charge"},
    {(char*)"-overlapp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.overlapp,
     (char*)"Bunch charge"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.phase,
     (char*)"Bunch phase"},
    {(char*)"-wake_scale_t",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&wake_t,
     (char*)"Wakefield scaling transverse"},
    {(char*)"-wake_scale_l",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&wake_l,
     (char*)"Wakefield scaling longitudinal"},
#ifdef TWODIM
    {(char*)"-beta_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betax,
     (char*)"Horizontal beta function at entrance (required)"},
    {(char*)"-alpha_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphax,
     (char*)"Horizontal alpha at entrance (required)"},
    {(char*)"-emitt_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emittx,
     (char*)"Horizontal emittance at entrance (required)"},
#endif
    {(char*)"-beta_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betay,
     (char*)"Vertical beta function at entrance (required)"},
    {(char*)"-alpha_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphay,
     (char*)"Vertical alpha at entrance (required)"},
    {(char*)"-emitt_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emitty,
     (char*)"Vertical emittance at entrance (required)"},
    {(char*)"-beamload",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamload_name,
     (char*)"Spline containing the longtudinal beam loading"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  size_t size;

  size=xmalloc_size();
  /* default values */

  param.charge=9.5e10;
  param.e0=0.05;
  param.n_slice=11;
  param.n_bunch=1;
  param.n_macro=1;
  param.espread=0.0;
  param.ecut=3.0;
  param.alphax=0.0;
  param.alphay=0.0;
  param.betax=0.0;
  param.betay=0.0;
  param.emittx=0.0;
  param.emitty=0.0;
  param.attenuation=1.0;
  param.distance=0.0;
  param.phase=0.0;
  param.last_wgt=1.0;
  n_max=0;
  wake_t=1.0;
  wake_l=1.0;

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  // scd tmp
  // if (inter_data.beamline->n_phases==0) {
  //   inter_data.beamline->phase[0]=0.0;
  //   inter_data.beamline->n_phases=1;
  // }

  /* perform checks */

  if (argc!=2){
    Tcl_AppendResult(interp,"Wrong number of arguments to ",argv[0],"\n",NULL);
    for (i=0;i<argc;i++){
      Tcl_AppendResult(interp,argv[0]," ",NULL);
    }
    Tcl_AppendResult(interp,"\nUsage: ",argv[0]," beamname /options/\n",NULL);
    return TCL_ERROR;
  }

  if (param.n_macro<1) {
    Tcl_AppendResult(interp,"-macroparticles must be at least 1",NULL);
    error=TCL_ERROR;
  }

#ifdef TWODIM
  if (param.betax<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emittx<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }
#endif
  if (param.betay<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emitty<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (chargelist==NULL) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"chargelist has to be specified\n",NULL);
    error=TCL_ERROR;
  }
  if (error) return error;
  error=Tcl_SplitList(interp,chargelist,&bunches_per_train,&v);
  if (!error) {
    ch=(double*)alloca(sizeof(double)*bunches_per_train);
    for (i=0;i<bunches_per_train;i++){
      if (error=Tcl_GetDouble(interp,v[i],&ch[i])){
	return error;
      }
    }
  }
  Tcl_Free((char*)v);

  if (beamload_name) {
    beaml = get_spline(beamload_name);
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  /* run command */

  param.attenuation=exp(-PI*param.distance
			/(injector_data.lambda[0]*injector_data.q_value));
  n_t=bunches_per_train;
  param.bunch=(BUNCHINFO*)malloc(sizeof(BUNCHINFO)*param.n_bunch);
  z=0.0;
  for (j=0;j<n_t/2;j+=2){
    param.bunch[j].z=z*1e6;
    param.bunch[j].ch=ch[j];
    z+=param.distance;
    param.bunch[j+1].z=(z+0.5*injector_data.lambda[0])*1e6;
    param.bunch[j+1].ch=-ch[j+1];
    z+=param.distance;
  }
  for(;j<param.n_bunch;j+=2){
    param.bunch[j].z=z*1e6;
    param.bunch[j].ch=ch[n_t-1];
    z+=param.distance;
    param.bunch[j+1].z=(z+0.5*injector_data.lambda[0])*1e6;
    param.bunch[j+1].ch=-ch[n_t-1];
    z+=param.distance;
  }
  if (file_name!=NULL) {
    // Also new
    inter_data.bunch=
      make_multi_bunch_inject(inter_data.beamline,file_name,
			      beaml,&param,param.n_bunch,n_max);
    inter_data.bunch->factor*=wake_l;
    inter_data.bunch->transv_factor*=wake_t;
    betax=param.betay;
    betay=param.betax;
    alphax=param.alphay;
    alphay=param.alphax;
    epsx=param.emittx;
    epsy=param.emitty;
    
    epsx*=EMASS/param.e0*1e12*EMITT_UNIT;
    epsy*=EMASS/param.e0*1e12*EMITT_UNIT;

    for(j=1;j<param.n_bunch;j+=2){
      for (i=0;i<inter_data.bunch->slices_per_bunch;++i){
	for (k=0;k<inter_data.bunch->macroparticles;++k){
#ifdef TWODIM
	  bunch_set_slice_x(inter_data.bunch,j,i,k,0.0);
	  bunch_set_slice_xp(inter_data.bunch,j,i,k,0.0);
	  bunch_set_sigma_xx(inter_data.bunch,j,i,k,betax,alphax,epsx);
	  bunch_set_sigma_xy(inter_data.bunch,j,i,k);
#endif
	  bunch_set_slice_y(inter_data.bunch,j,i,k,zero_point);
	  bunch_set_slice_yp(inter_data.bunch,j,i,k,0.0);
	  bunch_set_sigma_yy(inter_data.bunch,j,i,k,betay,alphay,epsy);
	  inter_data.bunch->particle[(j*inter_data.bunch->slices_per_bunch+i)
				     *inter_data.bunch->macroparticles+k].energy
	    *=-1.0;
	}
      }
    }
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Need to specify a filename for the single bunch parameters\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;
  /* create new command to manipulate beam */

  Placet_CreateCommand(interp,argv[1],&tk_Beam,
		       inter_data.bunch,NULL);

  /* store beam in table */

  entry=Tcl_CreateHashEntry(&BeamTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,inter_data.bunch);
    placet_printf(INFO,"%lu\n",xmalloc_size()-size);
    return TCL_OK;
  }
  check_error(interp,argv[0],error);
  Tcl_AppendResult(interp,"Failed to create beam ",argv[1],NULL);

  return TCL_ERROR;
}

/**********************************************************************/
/*                      InjectorCavityDefine                          */
/**********************************************************************/

int tk_InjectorCavityDefine(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error;
  char *name_wakelong=NULL, *name=NULL;
  // char *name_wakeshort=NULL,*name_wakeshort_proc=NULL;
  double lambda;
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the cavity type"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&drive_data.l_cav,
     (char*)"Standard length of the cavities (needed for the drive beam)"},
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda,
     (char*)"Wavelength [m]"},
    {(char*)"-wakelong",TK_ARGV_STRING,(char*)NULL,
     (char*)&name_wakelong,
     (char*)"Name of the longrange wakefield"},
    // {(char*)"-wakeshort",TK_ARGV_STRING,(char*)NULL,
    //  (char*)&name_wakeshort,
    //  (char*)"Name of the shortrange wakefield (spline)"},
    // {(char*)"-wakeshort_proc",TK_ARGV_STRING,(char*)NULL,
    //  (char*)&name_wakeshort_proc,
    //  (char*)"Name of the shortrange wakefield (procedure)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* initialisation of variables */

  lambda=0.48;
  injector_data.q_value=300.0;
  injector_data.band=0.1;
  injector_data.steps=0;
  injector_data.shift=0.0;
  injector_data.a0=2500.0*(0.625/2.85)*(0.625/2.85)*(0.625/2.85);
  injector_data.gradient=1.0;

  rf_data.lambda=lambda;

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to InjectorCavityDefine",TCL_VOLATILE);
    return TCL_ERROR;
  }

  injector_data.lambda[0]=lambda;

  if (name) {
    injector_data.name=(char*)malloc(strlen(name)+1);
    strcpy(injector_data.name,name);
  }
  if (name_wakelong) {
    injector_data.wake=get_wake(name_wakelong);
    injector_data.steps=injector_data.wake->n;
  } else {
    WAKE_DATA* empty_wake=(WAKE_DATA*)malloc(sizeof(WAKE_DATA));
    empty_wake->n=0;
    injector_data.wake=empty_wake;
  }
  /*
    check short range wakefield to be used later
  */
  /*
    if(!name_wakeshort){
    if (!name_wakeshort_proc) {
    Tcl_AppendResult(interp,"-wakeshort not specified",NULL);
    return TCL_ERROR;
    }
    else {
    }
    }
    else {
    injector_data.wsr=get_spline(name_wakeshort);
    }
  */

  if (injector_data.wake->n!=injector_data.steps){
    //placet_printf(INFO,"%d %d\n",injector_data.wake->n,injector_data.steps);
    Tcl_AppendResult(interp,"WakeSet ",name_wakelong," contains different number of modes than cells in structure",NULL);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      MainBeam                                      */
/**********************************************************************/

int tk_MainBeam(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  int error,ok,i,n;
  Tcl_HashEntry *entry;
  char *file_name=NULL,*g_list=NULL,*b_list=NULL,*long_list=NULL,**v;
  MAIN_BEAM_PARAM param;
  Tk_ArgvInfo table[]={
    {(char*)"-macroparticles",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_macro,
     (char*)"Number of macroparticles per slice"},
    {(char*)"-energyspread",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.espread,
     (char*)"Energy spread of intial beam [GeV]"},
    {(char*)"-ecut",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.ecut,
     (char*)"Cut of the energy spread of intial beam [-espread]"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"File from which to read the beam wakefield (overrides -slices)"},
    {(char*)"-bunches",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_bunch,
     (char*)"Number of bunches"},
    {(char*)"-slices",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_slice,
     (char*)"Number of slices"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.e0,
     (char*)"Beam energy at entrance [GeV]"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.phase,
     (char*)"Beam phase at entrance [degree]"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.charge,
     (char*)"Bunch charge [particles]"},
    {(char*)"-last_wgt",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.last_wgt,
     (char*)"Weight of the last bunch for the emittance"},
#ifdef TWODIM
    {(char*)"-beta_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betax,
     (char*)"Horizontal beta function at entrance (required) [m]"},
    {(char*)"-alpha_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphax,
     (char*)"Horizontal alpha at entrance (required)"},
    {(char*)"-emitt_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emittx,
     (char*)"Normalised horizontal emittance at entrance (required) [1e-7 m]"},
#endif
    {(char*)"-beta_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.betay,
     (char*)"Vertical beta function at entrance (required) [m]"},
    {(char*)"-alpha_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.alphay,
     (char*)"Vertical alpha at entrance (required)"},
    {(char*)"-emitt_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.emitty,
     (char*)"Normalised vertical emittance at entrance (required) [1e-7 m]"},
    {(char*)"-longrange_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&long_list,
     (char*)"Longrange wakefield list [V/pC/m^2]"},
    {(char*)"-g_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&g_list,
     (char*)"gradient list (normalised)"},
    {(char*)"-b_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&b_list,
     (char*)"beam loading list (normalised)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* default values */

  param.phase=0.0;
  param.charge=0.4e10;
  param.e0=9.0;
  param.n_slice=11;
  param.n_bunch=1;
  param.n_macro=1;
  param.espread=0.0;
  param.ecut=3.0;
  param.alphax=0.0;
  param.alphay=0.0;
  param.betax=0.0;
  param.betay=0.0;
  param.emittx=0.0;
  param.emitty=0.0;
  param.last_wgt=1.0;
  param.long_range=NULL;

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  /* perform checks */

  if (argc!=2){
    Tcl_AppendResult(interp,"Wrong number of arguments to ",argv[0],"\n",NULL);
    Tcl_AppendResult(interp,"Usage: ",argv[0]," beamname /options/\n",NULL);
    Tcl_AppendResult(interp,"Unparsed:\n",NULL);
    for(i=1;i<argc;i++){
      Tcl_AppendResult(interp,argv[i]," ",NULL);
    }
    Tcl_AppendResult(interp,"\n\n",NULL);
    return TCL_ERROR;
  }

  if (param.n_macro<1) {
    Tcl_AppendResult(interp,"-macroparticles must be at least 1",NULL);
    error=TCL_ERROR;
  }

#ifdef TWODIM
  if (param.betax<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emittx<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_x>0.0 required\n",NULL);
    error=TCL_ERROR;
  }
#endif
  if (param.betay<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"beta_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (param.emitty<=0.0) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"emitt_y>0.0 required\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  if (long_list) {
    if(error=Tcl_SplitList(interp,long_list,&n,&v)) return error;
    param.long_range=(double*)malloc(sizeof(double)*n);
    for (i=0;i<n;i++){
      if (error=Tcl_GetDouble(interp,v[i],param.long_range+i)){
	return error;
      }
    }
    Tcl_Free((char*)v);
  }
  
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  /* run command */

  if (file_name!=NULL) {
    inter_data.bunch=make_multi_bunch(inter_data.beamline,file_name,
				      &param);
  }
  else{
    Tcl_AppendResult(interp,"A filename has to be given for -file",NULL);
    return TCL_ERROR;
  }

  /* create new command to manipulate beam */

  Placet_CreateCommand(interp,argv[1],&tk_Beam,inter_data.bunch,
		       NULL);

  /* store beam in table */

  entry=Tcl_CreateHashEntry(&BeamTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,inter_data.bunch);
    return TCL_OK;
  }
  check_error(interp,argv[0],error);
  Tcl_AppendResult(interp,"Failed to create beam ",argv[1],NULL);
  return TCL_ERROR;
}

/**********************************************************************/
/*                      MainGradient                                  */
/**********************************************************************/


int tk_MainGradient(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  Tk_ArgvInfo table[]={
    {(char*)"-gradient",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&rf_data.gradient,
     (char*)"Gradient in GeV/m"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"MainGradient is called only with option -gradient",TCL_VOLATILE);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      MatchDoublet                                  */
/**********************************************************************/

int tk_MatchDoublet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  double l1=0.0,l2=0.0,k1=0.0,k2=0.0,d1=0.0,d2=0.0;
  double alphax,alphay,betax,betay,mux,muy;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MatchDoublet options"},
    {(char*)"-l1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l1,
     (char*)"Length of first quadrupole in meter (required)"},
    {(char*)"-l2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l2,
     (char*)"Length of second quadrupole in meter (required)"},
    {(char*)"-K1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&k1,
     (char*)"Integrated strength of first quadrupole (required)"},
    {(char*)"-K2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&k2,
     (char*)"Integrated strength of the second quadrupole"},
    {(char*)"-L1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&d1,
     (char*)"Distance between central quadrupoles in meter (required)"},
    {(char*)"-L2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&d2,
     (char*)"Distance between central quadrupoles in meter (required)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to MatchDoublet",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  /* perform checks */

  if (l1<=0.0){
    Tcl_AppendResult(interp,"l1 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (l2<=0.0){
    Tcl_AppendResult(interp,"l2 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (d1<=0.0){
    Tcl_AppendResult(interp,"L1 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (d2<=0.0){
    Tcl_AppendResult(interp,"L2 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (k1*k2>=0.0){
    Tcl_AppendResult(interp,"K1 * K2 must be smaller than 0.0\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  /* implementation */

  k1/=l1;
  k2/=l2;
  if (indoublet(k1,k2,l1,l2,d1,d2,&alphax,&alphay,&betax,&betay,&mux,&muy)){
    Tcl_AppendResult(interp,"Parameters give no stable solution\n",NULL);
    return TCL_ERROR;
  }
  Tcl_ResetResult(interp);
  Tcl_AppendResult(interp,print_double("beta_x %g ",betax),NULL);
  Tcl_AppendResult(interp,print_double("alpha_x %g ",alphax),NULL);
  Tcl_AppendResult(interp,print_double("mu_x %g ",mux),NULL);
  Tcl_AppendResult(interp,print_double("beta_y %g ",betay),NULL);
  Tcl_AppendResult(interp,print_double("alpha_y %g ",alphay),NULL);
  Tcl_AppendResult(interp,print_double("mu_y %g ",muy),NULL);
  
  return TCL_OK;
}

/**********************************************************************/
/*                      MatchFodo                                     */
/**********************************************************************/

int tk_MatchFodo(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  double l1=0.0,l2=0.0,k1=0.0,k2=0.0,d=0.0;
  double alphax,alphay,betax,betay,mux,muy;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MatchFodo options"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"The routine will return a list of the type"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"{beta_x 1.0 beta_y 2.0 alpha_x 0 alpha_y 0 mu_x 1.2 mu_y 1.2}"},
    {(char*)"-l1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l1,
     (char*)"Length of first quadrupole in meter (required)"},
    {(char*)"-l2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l2,
     (char*)"Length of second quadrupole in meter (required)"},
    {(char*)"-K1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&k1,
     (char*)"Integrated strength of first quadrupole (required)"},
    {(char*)"-K2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&k2,
     (char*)"Integrated strength of second quadrupole (required)"},
    {(char*)"-L",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&d,
     (char*)"Distance between quadrupoles in meter (required)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to MatchFodo",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  /* perform checks */

  if (l1<=0.0){
    Tcl_AppendResult(interp,"l1 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (l2<=0.0){
    Tcl_AppendResult(interp,"l2 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (k1*k2>=0.0){
    Tcl_AppendResult(interp,"K1 * K2 must be smaller than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (d<=0.0){
    Tcl_AppendResult(interp,"L must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  /* implementation */

  k1/=l1; k2/=l2;
  if (infodo(k1,l1,k2,l2,d,&alphax,&alphay,&betax,&betay,&mux,&muy)){
    Tcl_AppendResult(interp,"Parameters give no stable solution\n",NULL);
    return TCL_ERROR;
  }
  Tcl_ResetResult(interp);
  Tcl_AppendResult(interp,print_double("beta_x %g ",betax),NULL);
  Tcl_AppendResult(interp,print_double("alpha_x %g ",alphax),NULL);
  Tcl_AppendResult(interp,print_double("mu_x %g ",mux),NULL);
  Tcl_AppendResult(interp,print_double("beta_y %g ",betay),NULL);
  Tcl_AppendResult(interp,print_double("alpha_y %g ",alphay),NULL);
  Tcl_AppendResult(interp,print_double("mu_y %g ",muy),NULL);
  
  return TCL_OK;
}

/**********************************************************************/
/*                      MatchTriplet                                  */
/**********************************************************************/

int tk_MatchTriplet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  double l1=0.0,l2=0.0,l3=0.0,k1=0.0,k2=0.0,d=0.0;
  double alphax,alphay,betax,betay,mux,muy;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MatchTriplet options"},
    {(char*)"-l1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l1,
     (char*)"Length of central quadrupole in meter (required)"},
    {(char*)"-l2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l2,
     (char*)"Length of outer quadrupoles in meter (required)"},
    {(char*)"-l_sep",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&l3,
     (char*)"Distance between central and outer quadrupoles in meter (required)"},
    {(char*)"-K1",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&k1,
     (char*)"Integrated strength of central quadrupole (required)"},
    {(char*)"-K2",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&k2,
     (char*)"Integrated strength of the outer quadrupoles"},
    {(char*)"-L",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&d,
     (char*)"Distance between central quadrupoles in meter (required)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to MatchTriplet",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  /* perform checks */

  if (l1<=0.0){
    Tcl_AppendResult(interp,"l1 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (l2<=0.0){
    Tcl_AppendResult(interp,"l2 must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (l3<=0.0){
    Tcl_AppendResult(interp,"l_sep must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (d<=0.0){
    Tcl_AppendResult(interp,"L must be larger than 0.0\n",NULL);
    error=TCL_ERROR;
  }
  if (k1*k2>=0.0){
    Tcl_AppendResult(interp,"K1 * K2 must be smaller than 0.0\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  /* implementation */

  k1/=l1;
  k2/=l2;
  if (intripl(k1,k2,l1,l2,l3,d,&alphax,&alphay,&betax,&betay,&mux,&muy)){
    Tcl_AppendResult(interp,"Parameters give no stable solution\n",NULL);
    return TCL_ERROR;
  }
  Tcl_ResetResult(interp);
  Tcl_AppendResult(interp,print_double("beta_x %g ",betax),NULL);
  Tcl_AppendResult(interp,print_double("alpha_x %g ",alphax),NULL);
  Tcl_AppendResult(interp,print_double("mu_x %g ",mux),NULL);
  Tcl_AppendResult(interp,print_double("beta_y %g ",betay),NULL);
  Tcl_AppendResult(interp,print_double("alpha_y %g ",alphay),NULL);
  Tcl_AppendResult(interp,print_double("mu_y %g ",muy),NULL);
  
  return TCL_OK;
}

/************************************************************************/
/*                      PhaseAdvance		                        */
/*								      	*/
/* Added on the 18th Dec 2008 by Juergen Pfingstner		      	*/
/* Calculated the phase overall advance of a given beam line	      	*/  
/************************************************************************/

int tk_PhaseAdvance(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  // Handle the parameter of the command: First write an description
  // table (Tk_ArgvInfo), then call the command Tk_ParseArgv to 
  // handle the data
  int error;
  char *file_result=NULL;
  char *file_detail=NULL;
  char *beam_name=NULL;
  char *beamline_name=NULL;
  Tk_ArgvInfo table[]=
    {
      {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamline_name,
       (char*)"Name of the beamline to be saved"},
      {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
       (char*)"Name of the beam to use for the calculation"},
      {(char*)"-file_result",TK_ARGV_STRING,(char*)NULL,(char*)&file_result,
       (char*)"Name of the file where to store the results (default is no output)"},
      {(char*)"-file_detail",TK_ARGV_STRING,(char*)NULL,(char*)&file_detail,
       (char*)"Name of the file where to store the phase advance as a function of the lattice elements (default is no output)"},
      {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK)
    {
      return error;
    }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to PhaseAdvance",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  // Check if the beam line is ready and if the given beam has a hash entry. 
  // If yes call the function phase_advance.
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  
  BEAMLINE *beamline = get_beamline(beamline_name);
  if (!beamline) {
    beamline = inter_data.beamline;
    if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  }
  
  BEAM* beam = get_beam(beam_name);
  store_interp.interp=interp;
  phase_advance(beamline, beam, file_result, file_detail);
  
  return TCL_OK;
}

/**********************************************************************/
/*                      PlotBeamSpectrum                              */
/**********************************************************************/

int tk_PlotBeamSpectrum(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error;
  double l0,l1,beta=1.0;
  int n,flag=0;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-lambda0",TK_ARGV_FLOAT,(char*)NULL,(char*)&l0,
     (char*)"Minimal wavelength to be plotted"},
    {(char*)"-lambda1",TK_ARGV_FLOAT,(char*)NULL,(char*)&l1,
     (char*)"Maximal wavelength to be plotted"},
    {(char*)"-n",TK_ARGV_INT,(char*)NULL,(char*)&n,
     (char*)"Number of steps used for plot"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&flag,
     (char*)"Division type of axis"},
    {(char*)"-beta",TK_ARGV_FLOAT,(char*)NULL,(char*)&beta,
     (char*)"Reference beta-function to transform angles to offsets [m]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  wavelength_spectrum_extract(inter_data.bunch,l0,l1,n,beta,flag,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      PlotFillFactor                                */
/**********************************************************************/

int tk_PlotFillFactor(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if (fill_factor(inter_data.beamline,file_name)){
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleGetStrength                         */
/**********************************************************************/

int tk_QuadrupoleGetStrength(ClientData /*clientdata*/,Tcl_Interp *interp,
			     int argc,char *argv[])
{
  int error,j=-1;
  char no[100];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command returns the strength of the quadrupole number specified."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," number\n",NULL);
    return TCL_ERROR;
  }
  if (error=Tcl_GetInt(interp,argv[1],&j)) return error;
  if ((j<0)||(j>=inter_data.beamline->n_quad)) {
    snprintf(no,100,"%d",inter_data.beamline->n_quad);
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "quadrupole number must be smaller than ",no,"\n",
		     "and larger or equal to 0\n",NULL);
    return TCL_ERROR;
  }
  char buf[100];
  snprintf(buf,100,"Quadrupole Strength of Quad %d %20g",j,-(inter_data.beamline->quad[j]->get_strength()));
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleGetStrengthList                     */
/**********************************************************************/

int tk_QuadrupoleGetStrengthList(ClientData /*clientdata*/,Tcl_Interp *interp,
				 int argc,char *argv[])
{
  int error,j,n;
  char no[100];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command returns a list with the strength of all quadrupoles."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  n=inter_data.beamline->n_quad;
  for (j=0;j<n;j++){
    snprintf(no,100,"%20g\n",-(inter_data.beamline->quad[j]->get_strength()));
    Tcl_AppendResult(interp,no,NULL);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleNumberList                          */
/**********************************************************************/

int tk_QuadrupoleNumberList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error,i;
  ELEMENT **element;
  char buffer[100];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  element=inter_data.beamline->element;
  for (i=0;i<inter_data.beamline->n_elements;i++){
    if (element[i]->is_quad()){
      snprintf(buffer,100,"%d",i);
      Tcl_AppendElement(interp,buffer);
    }
  }
  return TCL_OK;
}
/**********************************************************************/
/*                      BpmNumberList                                 */
/**********************************************************************/

int tk_BpmNumberList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error,i;
  ELEMENT **element;
  char buffer[100];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  element=inter_data.beamline->element;
  for (i=0;i<inter_data.beamline->n_elements;i++){
    if (element[i]->is_bpm()){
      snprintf(buffer,100,"%d",i);
      Tcl_AppendElement(interp,buffer);
    }
  }
  return TCL_OK;
}
/**********************************************************************/
/*                      QuadrupolePositionListZ                       */
/**********************************************************************/

int tk_QuadrupolePositionListZ(ClientData /*clientdata*/,Tcl_Interp *interp,
			       int argc,char *argv[])
{
  int i;
  char no[100];
  double s=0.0;
  Tcl_DString res;

  if (argc!=1){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," number\n",NULL);
    return TCL_ERROR;
  }
  Tcl_DStringInit(&res);
  for (i=0;i<inter_data.beamline->n_elements;i++) {
    if (inter_data.beamline->element[i]->is_quad()) {
      s+=0.5*inter_data.beamline->element[i]->get_length();
      snprintf(no,100,"%g",s);
      Tcl_DStringAppendElement(&res,no);
      s+=0.5*inter_data.beamline->element[i]->get_length();
    }
    else {
      s+=inter_data.beamline->element[i]->get_length();
    }
  }
  Tcl_DStringResult(interp,&res);
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleSetStrength                         */
/**********************************************************************/

int tk_QuadrupoleSetStrength(ClientData /*clientdata*/,Tcl_Interp *interp,
			     int argc,char *argv[])
{
  int error,j=-1;
  char no[100];
  double value;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command sets the strength of the quadrupole number specified.\nUsage: QuadrupoleSetStrength quadrupole_number strength"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=3){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," quadrupole_number value\n",NULL);
    return TCL_ERROR;
  }
  error=Tcl_GetInt(interp,argv[1],&j);
  if (error) {
    return error;
  }
  if ((j<0)||(j>=inter_data.beamline->n_quad)) {
    snprintf(no,100,"%d",inter_data.beamline->n_quad);
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "quadrupole number must be smaller than ",no,"\n",
		     "and larger or equal to 0\n",NULL);
    return TCL_ERROR;
  }
  error=Tcl_GetDouble(interp,argv[2],&value);
  if (error) {
    return error;
  }
  inter_data.beamline->quad[j]->set_strength(-value);
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleSetStrengthList                     */
/**********************************************************************/

int tk_QuadrupoleSetStrengthList(ClientData /*clientdata*/,Tcl_Interp *interp,
				 int argc,char *argv[])
{
  int error,j,n;
  char no[100],**v;
  double value;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command accepts a list, it sets the strengths of all quadrupoles to the corresponding values of the list."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," value_list\n",NULL);
    return TCL_ERROR;
  }
  if (error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
  if (n!=inter_data.beamline->n_quad) {
    snprintf(no,100,"%d",inter_data.beamline->n_quad);
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "number of quadrupoles number must be equal to ",no,"\n",
		     NULL);
    return TCL_ERROR;
  }
  for (j=0;j<n;j++){
    if (error=Tcl_GetDouble(interp,v[j],&value)) return error;
    inter_data.beamline->quad[j]->set_strength(-value);
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleSetting                             */
/**********************************************************************/

int tk_QuadrupoleSetting(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error,ok;
  Tcl_HashEntry *entry;
  double *quad;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to QuadrupoleSettings",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  entry=Tcl_CreateHashEntry(&QuadTable,argv[1],&ok);
  if (entry){
    quad=(double*)malloc(sizeof(double)*inter_data.beamline->n_quad);
    Tcl_SetHashValue(entry,quad);
    Placet_CreateCommand(interp,argv[1],&tk_QuadrupoleSettingCmd,quad,NULL);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"QuadrupoleSetting ",argv[1]," already exists\n",NULL);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      ElementAddOffset                              */
/**********************************************************************/

int tk_ElementAddOffset(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			char *argv[])
{
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,NULL,
     (char*)"Add an offset to an element"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  struct ELEMENT::OFFSET offset = { 0.0, 0.0, 0.0, 0.0, 0.0 };
  option_set attributes;
  attributes.add("x", "Horizontal offset [um]", OPT_DOUBLE, &offset.x);
  attributes.add("y", "Vertical offset [um]", OPT_DOUBLE, &offset.y);
  attributes.add("xp", "Horizontal offset in angle [urad]", OPT_DOUBLE, &offset.xp);
  attributes.add("yp", "Vertical offset in angle [urad]", OPT_DOUBLE, &offset.yp);
  attributes.add("roll", "Roll angle [urad]", OPT_DOUBLE, &offset.roll, ux2x, x2ux);
  // backward compatibility
  attributes.add("angle_x", OPT_DOUBLE, &offset.xp);
  attributes.add("angle_y", OPT_DOUBLE, &offset.yp);
  attributes.parse_args(argc, argv, true, 2);
  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to <ElementAddOffset>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  int n=atoi(argv[1]);
  if (!inter_data.beamline->element_in_range(n)){return TCL_ERROR;}
  ELEMENT *element=inter_data.beamline->element[n];
  element->offset.x+=offset.x;
  element->offset.y+=offset.y;
  element->offset.xp+=offset.xp;
  element->offset.yp+=offset.yp;
  element->offset.roll+=offset.roll;
  return TCL_OK;
}

/**********************************************************************/
/*                      ElementInfo                                   */
/**********************************************************************/

int tk_ElementInfo(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error,n;
  int print=0;
  Tk_ArgvInfo table[]={
    {(char*)"-print",TK_ARGV_INT,(char*)NULL,
     (char*)&print,
     (char*)"If not zero, parameters are printed"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if (argc<2){
    Tcl_SetResult(interp,"Not enough arguments to <ElementInfo>", TCL_VOLATILE);
    return TCL_ERROR;
  }
  if ((error=Tcl_GetInt(interp,argv[1],&n))) return error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to <ElementInfo>", TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if (!inter_data.beamline->element_in_range(n)){return TCL_ERROR;}
  inter_data.beamline->element[n]->list(interp);
  if (print) {placet_cout << ALWAYS << inter_data.beamline->element[n]->list() << endmsg;}

  return TCL_OK;
}

/**********************************************************************/
/*                      ElementSetLength                              */
/**********************************************************************/

/*
  Change the length of an element on the fly
*/

int tk_ElementSetLength(ClientData /*clientdata*/,Tcl_Interp *interp,
			int argc,char *argv[])
{
  int error,j=-1;
  double value;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command sets the length of the element with the number specified.\nUsage: ElementSetLength element_number length"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=3){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," element_number value\n",NULL);
    return TCL_ERROR;
  }
  error=Tcl_GetInt(interp,argv[1],&j);
  if (error) {
    return error;
  }
  if (!inter_data.beamline->element_in_range(j)){return TCL_ERROR;}

  error=Tcl_GetDouble(interp,argv[2],&value);
  if (error) {
    return error;
  }
  inter_data.beamline->element[j]->set_length(value);
  return TCL_OK;
}

/**********************************************************************/
/*                      ElementSetToOffset                            */
/**********************************************************************/

int tk_ElementSetToOffset(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			  char *argv[])
{
  int error;
  ELEMENT::OFFSET offset;
  option_set attributes;
  attributes.add("x", "Horizontal offset [um]", OPT_DOUBLE, &offset.x);
  attributes.add("y", "Vertical offset [um]", OPT_DOUBLE, &offset.y);
  attributes.add("xp", "Horizontal offset in angle [urad]", OPT_DOUBLE, &offset.xp);
  attributes.add("yp", "Vertical offset in angle [urad]", OPT_DOUBLE, &offset.yp);
  attributes.add("roll", "Roll angle [urad]", OPT_DOUBLE, &offset.roll, ux2x, x2ux);
  // backward compatibility
  attributes.add("angle_x", "Same as -xp [backward compatibility]", OPT_DOUBLE, &offset.xp);
  attributes.add("angle_y", "Same as -yp [backward compatibility]", OPT_DOUBLE, &offset.yp);
  if (strcmp(argv[1], "-help")==0) {
    placet_cout << ALWAYS << "This command sets the offsets of an element. Options are:\n" << attributes << endmsg;
    return TCL_OK;
  }
  if (argc<=2){
    Tcl_SetResult(interp,"Not enough arguments to <ElementSetToOffset>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  int n=atoi(argv[1]);
  if (!inter_data.beamline->element_in_range(n)){return TCL_ERROR;}
  ELEMENT *element=inter_data.beamline->element[n];
  offset=element->offset;
  attributes.parse_args(argc, argv);
  element->offset=offset;
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadWake                                      */
/**********************************************************************/

int tk_QuadWake(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  int error,ok;
  Tcl_HashEntry *entry;
  char *name=NULL;
  double q=-1.0,lambda=-1.0,loss=0.0;
  static int number=0;
  WAKE_MODE *wake;
  Tk_ArgvInfo table[]={
    {(char*)"-Q",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&q,
     (char*)"Q-factor of mode"},
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda,
     (char*)"wavelength of mode"},
    {(char*)"-loss",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&loss,
     (char*)"loss factor of mode"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_SetResult(interp,"Must give exactly one name in QuadWake",TCL_VOLATILE);
    return TCL_ERROR;
  }
  name=argv[1];
  wake=(WAKE_MODE*)xmalloc(sizeof(WAKE_MODE));
  wake->Q=q;
  wake->a0=loss;
  wake->lambda=lambda;
  wake->k=TWOPI/lambda;
  wake->which=2;
  wake->number=number++;
  if (name){
    entry=Tcl_CreateHashEntry(&ModeTable,name,&ok);
    if (ok){
      Tcl_SetHashValue(entry,wake);
      quad_wake.on=1;
      return TCL_OK;
    }
  }
  xfree(wake);
  return TCL_ERROR;
}

/**********************************************************************/
/*                      SetRfGradient                                 */
/**********************************************************************/

int tk_SetRfGradient(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error=0;
  char *point,**v;
  BEAM *beam;
  int i,j,nb,np,m,n,l;
  double *de0,derf,debeam,lambda,*s_beam,*c_beam,c;

  if (argc>3){
    Tcl_SetResult(interp,"Too many arguments to <SetRfGradient>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<3){
    Tcl_SetResult(interp,"Not enough arguments to <SetRfGradient>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=get_beam(argv[1]);
  if(error=Tcl_SplitList(interp,argv[2],&n,&v)) return error;
  np=beam->slices_per_bunch;
  nb=beam->bunches;
  de0=beam->field[0].de;
  lambda=injector_data.lambda[0];
  for (l=0;l<1;l++){
    s_beam=beam->s_long[l];
    c_beam=beam->c_long[l];
    for (j=0;j<nb;j++){
      m=j*np;
      if (j<n) {
	derf=strtod(v[j],&point);
	debeam=strtod(point,&point);
      }
      for (i=0;i<np;i++) {
	c=cos(beam->z_position[m+i]*1e-6/lambda*TWOPI);
	c_beam[m+i]=derf*c;
	s_beam[m+i]=derf*sin(beam->z_position[m+i]*1e-6/lambda*TWOPI);	
	if (l==0) {
	  de0[m+i]+=debeam*c;
	}
      }
    }
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      SetRfGradientSingle                           */
/**********************************************************************/

int tk_SetRfGradientSingle(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error=0;
  char *point,**v;
  BEAM *beam;
  int i,j,nb,np,m,n,l;
  double *de0,derf,debeam,lambda,*s_beam,*c_beam,c,
    dephase,z_pos;

  if (argc>4){
    Tcl_SetResult(interp,"Too many arguments to <SetRfGradientSingle>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<4){
    Tcl_SetResult(interp,"Not enough arguments to <SetRfGradientSingle>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=get_beam(argv[1]);
  l=strtol(argv[2],&point,10);
  if(error=Tcl_SplitList(interp,argv[3],&n,&v)) return error;
  np=beam->slices_per_bunch;
  nb=beam->bunches;
  de0=beam->field[0].de;
  lambda=injector_data.lambda[0];
  s_beam=beam->s_long[l];
  c_beam=beam->c_long[l];
  for (j=0;j<nb;j++){
    m=j*np;
    if (j<n) {
      derf=strtod(v[j],&point);
      debeam=strtod(point,&point);
      dephase=strtod(point,&point);
    }
    for (i=0;i<np;i++) {
      z_pos=(dephase/180.0+beam->z_position[m+i]*1e-6/lambda*2.0)*PI;
      c=cos(z_pos);
      c_beam[m+i]=derf*c;
      s_beam[m+i]=derf*sin(z_pos);
      if (l==0) {
	de0[m+i]+=debeam*cos(beam->z_position[m+i]*1e-6/lambda*TWOPI);
      }
    }
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      SurveyErrorSet                                */
/**********************************************************************/

int tk_SurveyErrorSet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error=TCL_OK;
  Tk_ArgvInfo table[]={
    {(char*)"-quadrupole_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.quad_error_x,
     (char*)"Horizontal quadrupole position error [micro m]"},
    {(char*)"-quadrupole_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.quad_error,
     (char*)"Vertical quadrupole position error [micro m]"},
    {(char*)"-quadrupole_xp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.quad_error_angle_x,
     (char*)"Horizontal quadrupole angle error [micro radian]"},
    {(char*)"-quadrupole_yp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.quad_error_angle,
     (char*)"Vertical quadrupole angle error [micro radian]"},
    {(char*)"-quadrupole_roll",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.quad_roll,
     (char*)"Quadrupole roll around longitudinal axis [micro radian]"},
    {(char*)"-cavity_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_x,
     (char*)"Horizontal structure position error [micro m]"},
    {(char*)"-cavity_realign_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_realign_x,
     (char*)"Horizontal structure position error after realignment [micro m]"},
    {(char*)"-cavity_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error,
     (char*)"Vertical structure position error [micro m]"},
    {(char*)"-cavity_realign_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_realign,
     (char*)"Vertical structure position error after realignment [micro m]"},
    {(char*)"-cavity_xp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_angle_x,
     (char*)"Horizontal structure angle error [micro radian]"},
    {(char*)"-cavity_yp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_angle,
     (char*)"Vertical structure angle error [micro radian]"},
    {(char*)"-cavity_dipole_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_dipole_x,
     (char*)"Horizontal dipole kick [rad*GeV]"},
    {(char*)"-cavity_dipole_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.cav_error_dipole_y,
     (char*)"Vertical dipole kick [rad*GeV]"},
    {(char*)"-piece_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.piece_error_x,
     (char*)"Horizontal structure piece error [micro m]"},
    {(char*)"-piece_xp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.piece_error_angle_x,
     (char*)"Horizontal structure piece angle error [micro radian]"},
    {(char*)"-piece_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.piece_error,
     (char*)"Vertical structure piece error [micro m]"},
    {(char*)"-piece_yp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.piece_error_angle,
     (char*)"Vertical structure piece angle error [micro radian]"},
    {(char*)"-bpm_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.bpm_error_x,
     (char*)"Horizontal BPM position error [micro m]"},
    {(char*)"-bpm_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.bpm_error,
     (char*)"Vertical BPM position error [micro m]"},
    {(char*)"-bpm_xp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.bpm_error_angle_x,
     (char*)"Horizontal BPM angle error [micro radian]"},
    {(char*)"-bpm_yp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.bpm_error_angle,
     (char*)"Vertical BPM angle error [micro radian]"},
    {(char*)"-bpm_roll",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.bpm_roll,
     (char*)"BPM roll around longitudinal axis [micro radian]"},
    {(char*)"-sbend_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.sbend.x,
     (char*)"Horizontal sbend position error [micro m]"},
    {(char*)"-sbend_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.sbend.y,
     (char*)"Vertical sbend position error [micro m]"},
    {(char*)"-sbend_xp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.sbend.xp,
     (char*)"Horizontal sbend angle error [micro radian]"},
    {(char*)"-sbend_yp",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.sbend.yp,
     (char*)"Vertical sbend angle error [micro radian]"},
    {(char*)"-sbend_roll",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&survey_errors.sbend.roll,
     (char*)"Sbend roll around longitudinal axis [micro radian]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  survey_errors.quad_roll*=1e6;
  survey_errors.bpm_roll*=1e6;
  survey_errors.sbend.roll*=1e6;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  placet_printf(INFO,"quadrupole_x %g \n",survey_errors.quad_error_x);
  placet_printf(INFO,"quadrupole_xp %g \n",survey_errors.quad_error_angle_x);
  placet_printf(INFO,"quadrupole_y %g \n",survey_errors.quad_error);
  placet_printf(INFO,"quadrupole_yp %g \n",survey_errors.quad_error_angle);
  placet_printf(INFO,"quadrupole_roll %g \n",survey_errors.quad_roll);

  placet_printf(INFO,"cavity_x %g \n",survey_errors.cav_error_x);
  placet_printf(INFO,"cavity_xp %g \n",survey_errors.cav_error_angle_x);
  placet_printf(INFO,"cavity_dipole_x %g \n",survey_errors.cav_error_dipole_x);
  placet_printf(INFO,"cavity_realign_x %g \n",survey_errors.cav_error_realign_x);
  placet_printf(INFO,"cavity_y %g \n",survey_errors.cav_error);
  placet_printf(INFO,"cavity_yp %g \n",survey_errors.cav_error_angle);
  placet_printf(INFO,"cavity_dipole_y %g \n",survey_errors.cav_error_dipole_y);
  placet_printf(INFO,"cavity_realign_y %g \n",survey_errors.cav_error_realign);
  placet_printf(INFO,"piece_x %g \n",survey_errors.piece_error_x);
  placet_printf(INFO,"piece_y %g \n",survey_errors.piece_error);
  placet_printf(INFO,"piece_xp %g \n",survey_errors.piece_error_angle_x);
  placet_printf(INFO,"piece_yp %g \n",survey_errors.piece_error_angle);

  placet_printf(INFO,"bpm_x %g \n",survey_errors.bpm_error_x);
  placet_printf(INFO,"bpm_xp %g \n",survey_errors.bpm_error_angle_x);
  placet_printf(INFO,"bpm_y %g \n",survey_errors.bpm_error);
  placet_printf(INFO,"bpm_yp %g \n",survey_errors.bpm_error_angle);
  placet_printf(INFO,"bpm_roll %g \n",survey_errors.bpm_roll);

  placet_printf(INFO,"sbend_x %g \n",survey_errors.sbend.x);
  placet_printf(INFO,"sbend_xp %g \n",survey_errors.sbend.xp);
  placet_printf(INFO,"sbend_y %g \n",survey_errors.sbend.y);
  placet_printf(INFO,"sbend_yp %g \n",survey_errors.sbend.yp);
  placet_printf(INFO,"sbend_roll %g \n",survey_errors.sbend.roll);

  survey_errors.quad_roll*=1e-6;
  survey_errors.bpm_roll*=1e-6;
  survey_errors.sbend.roll*=1e-6;

  return TCL_OK;
}

/**********************************************************************/
/*                      SurveyErrorSet                                */
/**********************************************************************/

int tk_SurveyErrorClear(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  survey_errors = { 0.0 };
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupoleStepSize                            */
/**********************************************************************/

int tk_QuadrupoleStepSize(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			  char *argv[])
{
  int error=TCL_OK;
  char buf[100];
  Tk_ArgvInfo table[]={
    {(char*)"-quadrupole_step_size",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.quad_step_size,
     (char*)"Step size for the quadrupole correction [micro radian]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  snprintf(buf,100,"quadrupole step size is %g microns\n",errors.quad_step_size);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      TempView                                      */
/**********************************************************************/

/*
  to be modified and documented
*/

int tk_TempView(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  int i,error;
  CALLBACK *callback;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (inter_data.beamline->n_elements==0) return TCL_OK;

  if (inter_data.beamline->n_quad>0) { 
    callback=(CALLBACK*)malloc(sizeof(CALLBACK));
    callback->funct=&testshow2; // testshow2 defined in graph.cc
    callback->interp=interp;
    for (i=0;i<inter_data.beamline->n_quad;i++){
      inter_data.beamline->quad[i]->callback=callback;
    }
  }

  callback=(CALLBACK*)malloc(sizeof(CALLBACK));
  callback->funct=&testshow; // testshow defined in graph.cc
  callback->interp=interp;
  inter_data.beamline->element[inter_data.beamline->n_elements-1]->callback=
    callback;
  
  return TCL_OK;
}

int tk_TempView2(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;

  CALLBACK *callback;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  callback=(CALLBACK*)malloc(sizeof(CALLBACK));
  callback->funct=&testshow;
  callback->interp=interp;
  inter_data.beamline->element[inter_data.beamline->n_elements-1]->callback=
    callback;
  
  return TCL_OK;
}

/**********************************************************************/
/*                      TestBallisticCorrection                       */
/**********************************************************************/

int tk_TestBallisticCorrection(ClientData /*clientdata*/,
			       Tcl_Interp *interp,
			       int argc,char *argv[])
{
  int error;
  int nquad=1,loop=1,i,response=0,n,do_atl=0,repeat=1;
  char *file_name=NULL,*bumplist=NULL;
  char *format = default_format;
  void (*survey)(BEAMLINE*);
  char *beamname=NULL,*bumpbeamname=NULL,*testbeamname=NULL,
    *survey_name,*survey0=(char*)"Clic",*bumpquadname=NULL;
  double *quad_bumps=NULL,time=0.0,gain=1.0;
  char **v;
  BEAM *beam=NULL,*bumpbeam=NULL,*testbeam=NULL;
  int iter=1,b_loop=1,f_loop=1,do_rf=0,no_acc=0;
  int *bumps=NULL,*btype=NULL;
  Tcl_HashEntry *entry;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Command to simulate the ballistic correction method"},
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-repeat",TK_ARGV_INT,(char*)NULL,(char*)&repeat,
     (char*)"Number of iterations of the correction"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binloop",TK_ARGV_INT,(char*)NULL,(char*)&loop,
     (char*)"Number of iterations per bin"},
    {(char*)"-b_loop",TK_ARGV_INT,(char*)NULL,(char*)&b_loop,
     (char*)"Number of ballistic iterations per main iteration"},
    {(char*)"-f_loop",TK_ARGV_INT,(char*)NULL,(char*)&f_loop,
     (char*)"Number of few-to-few iterations per main iteration"},
    {(char*)"-response",TK_ARGV_INT,(char*)NULL,(char*)&response,
     (char*)"Response coefficients to use. This value determines the\n\
knowledge of the response coefficients used for the correction.\n\
If it is 0 perfect knowledge is assumed. For 1 and 2 the response\n\
coefficients are calculated from the strength of the\n\
first quadrupole in each bin and the distance to the other elements.\n\
The wakefield effects are not included.\n"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&errors.jitter_y,
     (char*)"Vertical beam jitter during correction in [um]. \n\
The jitter is assumed to be Gaussian."},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&errors.jitter_x,
     (char*)"Horizontal beam jitter during correction in [um].\n\
The jitter is assumed to be Gaussian."},
    {(char*)"-offset_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&errors.offset_y,
     (char*)"Vertical beam offset at linac entrance in [um]"},
    {(char*)"-offset_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&errors.offset_x,
     (char*)"Horizontal beam offset at linac entrance in [um]"},
    {(char*)"-quad_strength",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&ballistic_data.last_quad,
     (char*)"Relative strength of the first quadrupole during the ballistic correction step"},
    {(char*)"-field_error_type",TK_ARGV_INT,(char*)NULL,
     (char*)&errors.do_field,
     (char*)"Field error type (0: off, 1: relative, 2: absolute)"},
    {(char*)"-field_zero_error",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.field_zero,
     (char*)"Field error size for switched off quadrupoles"},
    {(char*)"-field_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.field_res,
     (char*)"Relative field error for switched on quadrupoles"},
    {(char*)"-field_position_error",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.position_error,
     (char*)"Field position error size for switched off quadrupoles"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"Resolution of BPMs [micro m]"},
    {(char*)"-quadrupole_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.quad_move_res,
     (char*)"Resolution of quadrupole movement [micro m]"},
    {(char*)"-quadrupole_stepsize",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.quad_step_size,
     (char*)"The step size of the quadrupole movement [micro m]"},
    {(char*)"-gain",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain,
     (char*)"Gain for each ballistic correction step"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-bumpbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&bumpbeamname,
     (char*)"Name of the beam to be used for emittance bumps.\n\
Between emittance bumps a one-to-one correction is performed. This beam can\n\
therefore also be used without any bump to simply apply this correction.\n\
If no name is specified but emittance tuning bumps are specified the original\n\
beam will be used.\n"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to use for evaluating the effect of the\n\
correction, defaults to the beam used for the correction or the\n\
one for the bump tuning if specified. Can be used to evaluate the effect of\n\
additional error sources, for example a jitter in the RF-phase after\n\
correction for.\n"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used, defaults to Clic"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the emittance results, defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)"-size_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&ballistic_data.size_file,
     (char*)"Filename for the beam size during correction, defaults to NULL (no output)"},
    {(char*)"-bumplist",TK_ARGV_STRING,(char*)NULL,
     (char*)&bumplist,
     (char*)"List of bump positions, defaults to NULL (no bumps)"},
    {(char*)"-bumpquads",TK_ARGV_STRING,(char*)NULL,
     (char*)&bumpquadname,
     (char*)"Quadrupole settings to be used with the bumps. Can be used to specify\n\
a list with the quadrupole strengths to be used for the one-to-one correction and\n\
the tuning of the emittance bumps after the ballistic correction. This allows to\n\
align the BPMs with a lattice different from the one in normal operation.\n"},
    {(char*)"-atl",TK_ARGV_INT,(char*)NULL,
     (char*)&do_atl,
     (char*)"Determines that the emittance should be evaluated after -time\n\
A number of different options are available\n\
0: do not simulate ground motion (default)\n\
1: do not correct after -time\n\
2: use the emittance bumps as simple feedbacks\n\
3: do a simple one-to-one correction\n\
4: use one-to-one correction and recalibrate the emittance bumps\n\
5: do a full realignment\n"},
    {(char*)"-time",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&time,
     (char*)"Time after which to evaluate the ground motion effect. [s]"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,
     (char*)&do_rf,
     (char*)"Determines whether the structures should be realigned to the beam."},
    {(char*)"-no_acc",TK_ARGV_INT,(char*)NULL,
     (char*)&no_acc,
     (char*)"Determines whether acceleration is switched off during correction"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  ballistic_init();
  survey_name=survey0;
  //  errors.quad_step_size=0.0;
  errors.quad_move_res=0.0;
  errors.bpm_resolution=0.1;
  errors.do_jitter=0;
  errors.jitter_y=0.0;
  errors.jitter_x=0.0;
  errors.offset_y=0.0;
  errors.offset_x=0.0;
  errors.do_field=0;
  errors.field_res=0.0;
  errors.field_zero=0.0;
  errors.field_zero_rms=0.01;
  errors.do_position=0;
  errors.position_error=0.0;
  ballistic_data.size_file=NULL;
  ballistic_data.last_quad=0.5;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TestBallisticCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestBallisticCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }
  if(error){
    return error;
  } 
  if (bumpbeamname!=NULL){
    bumpbeam=get_beam(bumpbeamname);
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-machines must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (nquad<1){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be at least 1\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (loop<1){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binloop must be at least 1\n",
		     NULL);
    error=TCL_ERROR;
  }
  if ((response<-1)||(response>2)){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-response must be in [-1..2]\n",
		     NULL);
    error=TCL_ERROR;
  }

  if (bumpquadname){
    entry=Tcl_FindHashEntry(&QuadTable,bumpquadname);
    if (entry){
      quad_bumps=(double*)Tcl_GetHashValue(entry);
    }
    else{
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"-bumpquads ",bumpquadname," not found\n",NULL);
      error=TCL_ERROR;
    }
  }
  else{
    quad_bumps=quad_set0;
  }
  if (bumplist){
    if(error=Tcl_SplitList(interp,bumplist,&n,&v)){
      return error;
    }
    if (n%2) {
      Tcl_AppendResult(interp,"bumplist is incorrect\n",NULL);
      error=TCL_ERROR;
    }
    n/=2;
    bumps=(int*)malloc(sizeof(int)*(n+1));
    btype=(int*)malloc(sizeof(int)*(n+1));
    for (i=0;i<n;i++){
      if (error=Tcl_GetInt(interp,v[i*2],bumps+i)){
	return error;
      }
      if (error=Tcl_GetInt(interp,v[i*2+1],btype+i)){
	return error;
      }
      if (i>0) {
	if (bumps[i]<=bumps[i-1]){
	  check_error(interp,argv[0],error);
	  Tcl_AppendResult(interp,
			   "-bumplist elements must be in ascending order\n",
			   NULL);
	  error=TCL_ERROR;
	}
      }
    }
    Tcl_Free((char*)v);
    bumps[i]=-1;
    if (!bumpbeam) bumpbeam=beam;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }
  if (error) return error;

  if (errors.jitter_y>0.0) {
    errors.do_jitter=1;
  }
  else{
    errors.jitter_y=0.0;
  }
  if (errors.jitter_x>0.0) errors.do_jitter=1;
  if (errors.do_field==2) errors.field_zero*=0.3;
  if (errors.position_error>0.0) errors.do_position=1;

  ballistic_set(b_loop,f_loop,loop,do_rf,nquad,response,gain);
  ballistic_set_atl(do_atl,time);
  for (i=0;i<inter_data.beamline->n_quad;i++){
    quad_set0[i]=inter_data.beamline->quad[i]->get_strength()/
      inter_data.beamline->quad[i]->get_length();
  }
  test_ballistic_2(inter_data.beamline,quad_set0,quad_set1,quad_bumps,
		   beam,bumpbeam,testbeam,bumps,btype,iter,repeat,no_acc,
		   survey,file_name,format,NULL);
  if (bumps){
    free(btype);
    free(bumps);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      TestFeedback                                  */
/**********************************************************************/

/* to be implemented */
int tk_TestFeedback(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  int nbin,i,nfl;
  void (*survey)(BEAMLINE*);
  int nquad,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  int *feedback,*nnquad,*nnbpm;
  char *file_name=NULL,**v,**w;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,sn[]="Atl";
  char *feedbacklist=NULL;
  BIN **bin;
  BEAM *beam=NULL,*testbeam=NULL,*tb,*workbeam;
  int iter=1;
  double jitter_y=0.0,jitter_x=0.0;
  double *gain;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluating the corrected beamline"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to <Clic>"},
    {(char*)"-feedbacklist",TK_ARGV_STRING,(char*)NULL,
     (char*)&feedbacklist,
     (char*)"List of feedback is a string : quadr_number (from QuadrupoleNumberList) number_quad number_bpm gain. Defaults to NULL (no bumps)"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"emittance file format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  errors.bpm_resolution=0.0;
  survey_name=sn;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to Feedback",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (error) return error;

  feedback=(int*)malloc(sizeof(int)*(nbin+1));
  nnquad=(int*)malloc(sizeof(int)*(nbin+1));
  nnbpm=(int*)malloc(sizeof(int)*(nbin+1));
  gain=(double*)malloc(sizeof(double)*(nbin+1));

  if (feedbacklist){
    if(error=Tcl_SplitList(interp,feedbacklist,&nbin,&v)){
      return error;
    }

    for (i=0;i<nbin;i++){
      if(error=Tcl_SplitList(interp,v[i],&nfl,&w)){
        return error;
      }      

      if (error=Tcl_GetInt(interp,w[0],feedback+i)){
	return error;
      }

      if (error=Tcl_GetInt(interp,w[1],nnquad+i)){
	return error;
      }

      if (error=Tcl_GetInt(interp,w[2],nnbpm+i)){
	return error;
      }

      if (nnbpm[i]<nnquad[i]){
	check_error(interp,argv[0],error);
	Tcl_AppendResult(interp,"\n The number of BPM must be G.E. to the number of Quadrupoles, for each feedback bin\n",NULL);
	error=TCL_ERROR;
	return error;
      }
     
      if (error=Tcl_GetDouble(interp,w[3],gain+i)){
	return error;
      }


      /*if (error=Tcl_GetInt(interp,v[i],feedback+i)){
	return error;
	}*/
      if (i>0) {
	if (feedback[i]<=feedback[i-1]){
	  check_error(interp,argv[0],error);
	  Tcl_AppendResult(interp,"-feedbacklist elements must be in ascending order\n",NULL);
	  error=TCL_ERROR;
          return error;
	}
      }
    }
    feedback[i]=-1;
    Tcl_Free((char*)v);
    Tcl_Free((char*)w);
  }
  else{
    placet_printf(WARNING,"warning:\nno feedbacks specified\n");
    feedback[0]=-1;
    nbin=0;
  }

  errors.do_jitter=0;
  errors.jitter_x=jitter_x;
  errors.jitter_y=jitter_y;

  if ((jitter_x>0.0)||(jitter_y>0.0)) errors.do_jitter=1;
  /*  errors.bpm_resolution=0.0;*/

  bin=(BIN**)malloc(sizeof(BIN*)*nbin);
  for (i=0;i<nbin;i++){
    bin_define_2(inter_data.beamline,feedback[i],nnquad[i],nnbpm[i],
		 bpm,&nbpm,quad,&nquad);
    bin[i]=bin_make(nquad,nbpm); 
    bin_set_elements(bin[i],quad,nquad,bpm,nbpm,nquad);
    bin_set_gain(bin[i],gain[i]);
  }
  tb=bunch_remake(beam);
  workbeam=bunch_remake(beam);
  beam_copy(beam,tb);
  simple_bin_fill(inter_data.beamline,bin,nbin,tb,workbeam);
  beam_delete(workbeam);
  beam_delete(tb);
  test_simple_correction(inter_data.beamline,bin,nbin,beam,testbeam,iter,
			 survey,file_name,format);
  //test_feedback_3(inter_data.beamline,beam,7,0,iter,3e3,file_name); // moved to obsolete/placeti4.cc
  free(feedback);
  free(nnquad);
  free(nnbpm);
  free(gain);
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;

}

/// Independent feedbacks : command is TestFeedbackIndep

int tk_TestIndependentFeedback(ClientData /*clientdata*/,Tcl_Interp *interp,
			       int argc,char *argv[])
{
  int error;
  int nbin,i,j,n,m,nfl,k,mark_q,mark_b,njoin,mjoin,pjoin;
  void (*survey)(BEAMLINE*);
  int nquad,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  int *feedback,*nnquad,*nnbpm;
  int vquad[BIN_MAX_QUAD],vbpm[BIN_MAX_BPM]; 
  int tnquad,tnbpm;
  char *file_name=NULL,*file_name2=NULL,**v,**w,**v2;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,sn[]="Atl";
  char *feedbacklist=NULL,*join_list=NULL;
  BIN **bin;
  BEAM *beam=NULL,**testbeam=NULL;
  int iter=1,single=0;
  double jitter_y=0.0,jitter_x=0.0;
  double *gain,*dtau,*wgt;

  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of iterrations (machines) for independent feedback corrections"},
    {(char*)"-single_bin",TK_ARGV_INT,(char*)NULL,(char*)&single,
     (char*)"If 1, consider all feedbacks in a single bin"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-beamlist",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"List of names of the beams to be used during the correction"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to <Clic>"},
    {(char*)"-feedbacklist",TK_ARGV_STRING,(char*)NULL,
     (char*)&feedbacklist,
     (char*)"List of feedback : quadr_number (from QuadrupoleNumberList) number_bpm gain. Defaults to NULL (no bumps)"},
    {(char*)"-joinlist",TK_ARGV_STRING,(char*)NULL,
     (char*)&join_list,
     (char*)"List of feedback to be joined"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)"-emitt_vs_iter_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name2,
     (char*)"Filename for the results of emittance vs machines, defaults to NULL (no output)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=sn;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TestIndependentFeedback",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    if (error=Tcl_SplitList(interp,testbeamname,&n,&v)){
      return error;
    }
    wgt=(double*)alloca(sizeof(double)*n);
    testbeam=(BEAM**)alloca(sizeof(BEAM*)*n);
    for (i=0;i<n;i++){
      if (error=Tcl_SplitList(interp,v[i],&m,&v2)){
	return error;
      }
      if (m!=2) {
	return TCL_ERROR;
      }
      testbeam[i]=get_beam(v2[0]);
      wgt[i]=strtod(v2[1],NULL);
      placet_printf(INFO,"beam[%d]=%s wgt[%i]=%g\n",i,v2[0],i,wgt[i]);
    }
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  feedback=(int*)xmalloc(sizeof(int)*(nbin+1));
  nnquad=(int*)xmalloc(sizeof(int)*(nbin+1));
  nnbpm=(int*)xmalloc(sizeof(int)*(nbin+1));
  gain=(double*)xmalloc(sizeof(double)*(nbin+1));
  dtau=(double*)xmalloc(sizeof(double)*(nbin+1));

  if (feedbacklist){
    if(error=Tcl_SplitList(interp,feedbacklist,&nbin,&v)){
      return error;
    }

    for (i=0;i<nbin;i++){
      if(error=Tcl_SplitList(interp,v[i],&nfl,&w)){
        return error;
      }      

      if (error=Tcl_GetInt(interp,w[0],feedback+i)){
	return error;
      }

      if (error=Tcl_GetInt(interp,w[1],nnquad+i)){
	return error;
      }

      if (error=Tcl_GetInt(interp,w[2],nnbpm+i)){
	return error;
      }
 
      if (nnbpm[i]<nnquad[i]){
	check_error(interp,argv[0],error);
	Tcl_AppendResult(interp,"\n The number of BPM must be G.E. to the number of Quadrupoles, for each feedback bin\n",NULL);
	error=TCL_ERROR;
	return error;
      }  
     
      if (error=Tcl_GetDouble(interp,w[3],gain+i)){
	return error;
      }

      if (nfl>4) {
	if (error=Tcl_GetDouble(interp,w[4],dtau+i)){
	  return error;
	}
	if (dtau[i]>0.0) {
	  dtau[i]=exp(-1.0/dtau[i]);
	}
	else {
	  dtau[i]=0.0;
	}
      }
      else {
	dtau[i]=0.0;
      }

      if (i>0) {
	if (feedback[i]<=feedback[i-1]){
	  check_error(interp,argv[0],error);
	  Tcl_AppendResult(interp,
			   "-feedbacklist elements must be in ascending order\n",NULL);
	  error=TCL_ERROR;
	  return error;
	}
      }
    }
    feedback[i]=-1; // i==nbin-1, last bin
    Tcl_Free((char*)v);
    Tcl_Free((char*)w);
  }
  else{
    placet_printf(WARNING,"warning:\nno feedbacks specified\n");
    feedback[0]=-1;
    nbin=0;
  }

  errors.do_jitter=0;
  errors.jitter_x=jitter_x;
  errors.jitter_y=jitter_y;
  if ((jitter_x>0.0)||(jitter_y>0.0)) errors.do_jitter=1;

  bin=(BIN**)xmalloc(sizeof(BIN*)*nbin);

  if(single) {
    if (join_list) {
      if(error=Tcl_SplitList(interp,join_list,&njoin,&v)){
	return error;
      }
      pjoin=0;
      for (i=0;i<njoin;i++){
	if (error=Tcl_GetInt(interp,v[i],&mjoin)){
	  return error;
	}
	tnquad = 0;
	tnbpm = 0;    
	mark_q = 0;
	mark_b = 0;
	for (j=0;j<mjoin;j++){
	  if (pjoin==nbin) {
	    placet_fprintf(ERROR,stderr,
		    "too many feedbacks are joined %d %d\n",
		    pjoin,nbin);
	    return TCL_ERROR;
	  }
	  bin_define_2(inter_data.beamline,feedback[pjoin],
		       nnquad[pjoin],nnbpm[pjoin],
		       bpm,&nbpm,quad,&nquad);
	  tnquad += nquad;
	  tnbpm += nbpm;
		  
	  for(k=0;k<nquad;k+=1){
	    vquad[mark_q] = quad[k];
	    mark_q++;
	  }
		  
	  for(k=0;k<nbpm;k+=1){
	    vbpm[mark_b] = bpm[k];
	    mark_b++;
	  } 
	  pjoin++;
	}
	bin[i]=bin_make(tnquad,tnbpm);
	if (i==njoin-1){
	  vbpm[tnbpm-1]=inter_data.beamline->n_elements-3;
	}
	bin_set_elements(bin[i],vquad,tnquad,vbpm,tnbpm,tnquad);
	bin_set_gain(bin[i],gain[pjoin-mjoin]);
	bin_set_dtau(bin[i],dtau[pjoin-mjoin]);
	placet_printf(INFO,"FEEDBACK: %G %G %d %d %d %d %d %d\n",
	       gain[pjoin-mjoin],dtau[pjoin-mjoin],pjoin,mjoin,tnquad,
	       tnbpm,vbpm[tnbpm-1],inter_data.beamline->n_elements);
      }
      nbin = njoin;
    }
    else {
      tnquad = 0;
      tnbpm = 0;    
      mark_q = 0;
      mark_b = 0;
      for (i=0;i<nbin;i++){
	bin_define_2(inter_data.beamline,feedback[i],nnquad[i],nnbpm[i],
		     bpm,&nbpm,quad,&nquad);
	tnquad += nquad;
	tnbpm += nbpm;
	      
	for(k=0;k<nquad;k+=1){
	  vquad[mark_q] = quad[k];
	  mark_q++;
	}
	      
	for(k=0;k<nbpm;k+=1){
	  vbpm[mark_b] = bpm[k];
	  mark_b++;
	} 
      }
      bin[0]=bin_make(tnquad,tnbpm);
      bin_set_elements(bin[0],vquad,tnquad,vbpm,tnbpm,tnquad);
      bin_set_gain(bin[0],gain[0]);
      bin_set_dtau(bin[0],dtau[0]);
      nbin = 1;
    }
  }
  else {
    for (i=0;i<nbin;i++){
      bin_define_2(inter_data.beamline,feedback[i],nnquad[i],nnbpm[i],
		   bpm,&nbpm,quad,&nquad);
      bin[i]=bin_make(nquad,nbpm); 
      if (i==nbin-1){
	bpm[nbpm-1]=inter_data.beamline->n_elements-3;
	if (inter_data.beamline->element[bpm[nbpm-1]]->is_bpm()) {
	  placet_printf(INFO,"careful\n");
	}
	else {
	  placet_printf(INFO,"good\n");
	}
      }
      bin_set_elements(bin[i],quad,nquad,bpm,nbpm,nquad);
      bin_set_gain(bin[i],gain[i]);
      bin_set_dtau(bin[i],dtau[i]);
    }
  }
  
  //  tb=bunch_remake(beam);
  //  workbeam=bunch_remake(beam);
  //  beam_copy(beam,tb);
  //  simple_bin_fill(inter_data.beamline,bin,nbin,tb,workbeam);
  //  beam_delete(workbeam);
  //  beam_delete(tb);
  if (testbeam) {
    test_simple_correction_indep_list(inter_data.beamline,bin,nbin,beam,
				      testbeam,wgt,n,
				      iter,survey,file_name,format,file_name2);
  }
  else {
    test_simple_correction_indep(inter_data.beamline,bin,nbin,beam,NULL,
				 iter,survey,file_name,format,file_name2);
  }
 
  free(feedback);
  free(nnquad);
  free(nnbpm);
  free(gain);
  free(dtau);

  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);

  return TCL_OK;
}


/**********************************************************************/
/*                      IPFeedback                                    */
/**********************************************************************/

int tk_IPFeedback_0(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  char *beamname1=NULL,*beamname2=NULL;
  char *testbeamname1=NULL,*testbeamname2=NULL;
  double ym1=0.0,ym2=0.0,ypm1=0.0,ypm2=0.0,lum_rel;
  double gain_pos=1.0,gain_ang=1.0,beam_pos_res=0.0;

  Tk_ArgvInfo table[]={
    {(char*)"-beam1",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname1,
     (char*)"Name of the first beam used for feedback (from BeamSaveAll)"},
    {(char*)"-beam2",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname2,
     (char*)"Name of the second beam used for feedback (from BeamSaveAll)"},
    {(char*)"-testbeam1",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname1,
     (char*)"Name of the first testbeam used for collision (from BeamSaveAll)"},
    {(char*)"-testbeam2",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname2,
     (char*)"Name of the second testbeam used for collision (from BeamSaveAll)"},
    {(char*)"-gain_pos",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain_pos,
     (char*)"gain of the feedback position at IP"},
    {(char*)"-gain_ang",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain_ang,
     (char*)"gain of the feedback angle at IP"},
    {(char*)"-beam_pos_res",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&beam_pos_res,
     (char*)"Resolution on the beam position at IP in microns"},
    /*
      {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
      (char*)&errors.bpm_resolution,
      (char*)"BPM resolution [micro meter]"},
      {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
      (char*)&survey_name,
      (char*)"Type of survey to be used for IP, defaults to <Clic>"},
      {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
      (char*)&file_name,
      (char*)"Filename for the results defaults to NULL (no output)"},
    */   
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  //survey_name=sn;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to IPFeedback_0",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  /// Read Beams and define the corrections (mean position ym and 
  /// mean angle ypm) to do for collision

  std::pair<double,double> ym_ipcor = IP_corrections_0(beamname1);
  ym1 = ym_ipcor.first;
  ypm1 = ym_ipcor.second;

  //placet_printf(INFO,"\nTest B %g %g \n",ym1,ypm1);

  ym_ipcor = IP_corrections_0(beamname2);
  ym2 = ym_ipcor.first;
  ypm2 = ym_ipcor.second;

  //placet_printf(INFO,"\nTest C %g %g \n",ym2,ypm2);

  /// Compute the relative luminosity (l0-l/l0) on the testbeams, 
  /// after the predefined corrections have been applied

    
  lum_rel = IP_collision_0(testbeamname1,testbeamname2,ym1,ypm1,ym2,ypm2,
			   gain_pos,gain_ang,beam_pos_res);  
    
  char buf[25];
  snprintf(buf,25,"%20g\n",lum_rel);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
    
  return TCL_OK;
}

/*
  Define the scaterring of the BPMs relative to the cavities
*/

int tk_BpmToCavScatter(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[]) {
  int error;
  double scat_x=0.0,scat_y=0.0;

  Tk_ArgvInfo table[]={
    {(char*)"-scatter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_x,
     (char*)"Sigma of Gaussian scattering in x view [micro meter]"},
    {(char*)"-scatter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_y,
     (char*)"Sigma of Gaussian scattering in y view [micro meter]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to BpmToCavScatter",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  

  // Define the scattering
  cav_scatter(inter_data.beamline,scat_y,scat_x);

  return TCL_OK;     
}

/*
  Scatter the girder intersection and then move the CAVs.
  Allow a mismatch of the girder extremities at the connection point (flo_y
  and flo_x scattering around the girder intersection).
*/

int tk_InterGirderMove(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[]) {

  int error;
  double scat_x=0.0,scat_y=0.0,flo_y=0.0,flo_x=0.0;
  int cav_only=1;
  
  Tk_ArgvInfo table[]={
    {(char*)"-scatter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_x,
     (char*)"Sigma of Gaussian scattering in x view of the intersections between girders [micro meter]"},
    {(char*)"-scatter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_y,
     (char*)"Sigma of Gaussian scattering in y view of the intersections between girders [micro meter]"},
    {(char*)"-flo_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&flo_x,
     (char*)"Sigma of Gaussian scattering in x view of the girder connection around the intersection point [micro meter]"},
    {(char*)"-flo_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&flo_y,
     (char*)"Sigma of Gaussian scattering in y view of the girder connection around the intersection point [micro meter]"},
    {(char*)"-cav_only",TK_ARGV_INT,(char*)NULL,(char*)&cav_only,
     (char*)"If not zero move only the cavities on the girder"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to InterGirderMove",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  

  // Do the scattering of the CAV due to the girder intersection scattering
  inter_girder_move(inter_data.beamline,scat_y,scat_x,flo_y,flo_x,cav_only);

  return TCL_OK;     
}
//

/*
  Scatter the girders
*/

int tk_ScatterGirder(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[]) {

  int error;
  double scat_x=0.0,scat_y=0.0,scat_xp=0.0,scat_yp=0.0;
  int cav_only=0;
  
  Tk_ArgvInfo table[]={
    {(char*)"-scatter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_x,
     (char*)"Sigma of Gaussian scattering in x [micro meter]"},
    {(char*)"-scatter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_y,
     (char*)"Sigma of Gaussian scattering in y [micro meter]"},
    {(char*)"-scatter_xp",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_xp,
     (char*)"Sigma of Gaussian scattering in x' [micro rad]"},
    {(char*)"-scatter_yp",TK_ARGV_FLOAT,(char*)NULL,(char*)&scat_yp,
     (char*)"Sigma of Gaussian scattering in y' [micro rad]"},
    {(char*)"-cav_only",TK_ARGV_INT,(char*)NULL,(char*)&cav_only,
     (char*)"If not zero move only the cavities on the girder"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to ScatterGirder",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  

  // Do the scattering of the CAV due to the girder intersection scattering
  scatter_girder(inter_data.beamline,scat_y,scat_yp,scat_x,scat_xp,cav_only);

  return TCL_OK;     
}
//

/**********************************************************************/
/*                      TestFreeCorrection                            */
/**********************************************************************/

int tk_TestFreeCorrection(ClientData /*clientdata*/,
			  Tcl_Interp *interp,int argc,
			  char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,nbin_rf,do_rf=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*survey_name,
    survey0[]="Clic";
  BIN **bin,**bin_rf;
  BEAM *beam=NULL;
  int iter=1,n;
  double *q0,*q1,*q2;
  char **v,*lq0=NULL,*lq1=NULL,*lq2=NULL,*bin_store=NULL,*bin_load=NULL;
  double jitter_y=0.0,jitter_x=0.0,wgt0=1.0,wgt1=-1.0,pwgt=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,
     (char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"Align the RF after the correction?"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to Clic"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)"-wgt0",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt0,
     (char*)"Weight for the BPM position"},
    {(char*)"-wgt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt1,
     (char*)"Weight for the BPM resolution"},
    {(char*)"-pwgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pwgt,
     (char*)"Weight for the old quadrupole position."},
    {(char*)"-quad_set0",TK_ARGV_STRING,(char*)NULL,
     (char*)&lq0,
     (char*)"List of quadrupole strengths to be used"},
    {(char*)"-quad_set1",TK_ARGV_STRING,(char*)NULL,
     (char*)&lq1,
     (char*)"List of quadrupole strengths to be used"},
    {(char*)"-quad_set2",TK_ARGV_STRING,(char*)NULL,
     (char*)&lq2,
     (char*)"List of quadrupole strengths to be used"},
    {(char*)"-load_bins",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_load,
     (char*)"File with bin information to be loaded"},
    {(char*)"-save_bins",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_store,
     (char*)"File with bin information to be loaded"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestFreeCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (lq0) {
    if (error=Tcl_SplitList(interp,lq0,&n,&v)) return error;
    if (n!=inter_data.beamline->n_quad) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"length of argument to -quad_set0 does not match number of quadrupoles\n",NULL);
    }
    else {
      q0=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],q0+i)) return error;
      }
    }
    Tcl_Free((char*)v);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q0[i]/=-inter_data.beamline->quad[i]->get_length();
    }
  }
  else {
    q0=quad_set0;
  }
  if (lq1) {
    if (error=Tcl_SplitList(interp,lq1,&n,&v)) return error;
    if (n!=inter_data.beamline->n_quad) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"length of argument to -quad_set1 does not match number of quadrupoles\n",NULL);
    }
    else {
      q1=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],q1+i)) return error;
      }
    }
    Tcl_Free((char*)v);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q1[i]/=-inter_data.beamline->quad[i]->get_length();
    }
  }
  else {
    q1=quad_set1;
  }
  if (lq2) {
    if (error=Tcl_SplitList(interp,lq2,&n,&v)) return error;
    if (n!=inter_data.beamline->n_quad) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"length of argument to -quad_set2 does not match number of quadrupoles\n",NULL);
    }
    else {
      q2=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],q2+i)) return error;
      }
    }
    Tcl_Free((char*)v);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q2[i]/=-inter_data.beamline->quad[i]->get_length();
    }
  }
  else {
    q2=quad_set2;
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  corr.pwgt=pwgt;
  corr.w=wgt1;
  corr.w0=wgt0;

  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide(inter_data.beamline,nquad,noverlap,bin,
		      &nbin);
  if (do_rf) {
    bin_rf=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
    beamline_bin_divide(inter_data.beamline,12,0,bin_rf,
			&nbin_rf);
    test_free_correction(inter_data.beamline,
			 q0,q1,q2,
			 bin,nbin,beam,bin_rf,nbin_rf,
			 jitter_y,iter,survey,
			 file_name,format);
    for (i=0;i<nbin_rf;i++){
      bin_delete(bin_rf[i]);
    }
    free(bin_rf);
  }
  else{
    test_free_correction(inter_data.beamline,
			 q0,q1,q2,
			 bin,nbin,beam,NULL,0,jitter_y,iter,
			 survey,file_name,format);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestFreeCorrection                            */
/**********************************************************************/

int tk_TestTrainCorrection(ClientData /*clientdata*/,
			   Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,nbin_rf,do_rf=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*survey_name,
    survey0[]="Clic";
  BIN **bin,**bin_rf;
  BEAM *beam=NULL;
  int iter=1;
  char *bin_store=NULL,*bin_load=NULL;
  double jitter_y=0.0,jitter_x=0.0,wgt0=1.0,wgt1=-1.0,pwgt=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,
     (char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"Align the RF after the correction?"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)"-wgt0",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt0,
     (char*)"Weight for the BPM position"},
    {(char*)"-wgt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt1,
     (char*)"Weight for the BPM resolution"},
    {(char*)"-pwgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pwgt,
     (char*)"Weight for the old position"},
    {(char*)"-load_bins",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_load,
     (char*)"File with bin information to be loaded"},
    {(char*)"-save_bins",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_store,
     (char*)"File with bin information to be loaded"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestTrainCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  corr.pwgt=pwgt;
  corr.w=wgt1;
  corr.w0=wgt0;

  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide(inter_data.beamline,nquad,noverlap,bin,
		      &nbin);
  if (do_rf) {
    bin_rf=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
    beamline_bin_divide(inter_data.beamline,12,0,bin_rf,
			&nbin_rf);
    test_train_correction(inter_data.beamline,
			  bin,nbin,beam,bin_rf,nbin_rf,
			  jitter_y,iter,survey,
			  file_name,format);
    for (i=0;i<nbin_rf;i++){
      bin_delete(bin_rf[i]);
    }
    free(bin_rf);
  }
  else{
    test_train_correction(inter_data.beamline,
			  bin,nbin,beam,NULL,0,
			  jitter_y,iter,survey,
			  file_name,format);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestFreeCorrection2                           */
/**********************************************************************/

int tk_TestFreeCorrection2(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,nbin2,i,interleave=0,do_rf=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,survey0[]="Clic";
  BIN **bin,**bin2;
  BEAM *beam=NULL,*testbeam=NULL;
  int iter=1,step_iter=2;
  double jitter_y=0.0,jitter_x=0.0,shift_y=0.0;
  double quads1[MAX_QUAD],quads2[MAX_QUAD];
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,(char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-quad_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.quad_move_res,
     (char*)"quadrupole resolution [micro meter]"},
    {(char*)"-quad_shift",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&shift_y,
     (char*)"shift of quadrupole centre [micro meter]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluating the corrected beamline"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"align the RF after correction?"},
    {(char*)"-iterations",TK_ARGV_INT,(char*)NULL,(char*)&step_iter,
     (char*)"Number of iterations for correction"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  beamline_survey_hook_interp=interp;
  survey_name=survey0;
  errors.quad_move_res=0.0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestFreeCorrection2",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }

  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (interleave){
    // check bin size
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide(inter_data.beamline,nquad,noverlap,bin,&nbin);
  for(i=0;i<inter_data.beamline->n_quad;i++){
    quads1[i]=quad_set1[i];
    quads2[i]=quad_set2[i];
  }
  for(i=0;i<nbin;i++){
    quads1[i*nquad]=quad_set0[i*nquad];
    quads2[i*nquad]=quad_set0[i*nquad];
  }
  
  if (testbeamname){
    bin2=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
    //    beamline_bin_divide(inter_data.beamline,nquad,noverlap,bin2,&nbin2);
    beamline_bin_divide_1(inter_data.beamline,1,0,bin2,&nbin2);
    test_free_correction_new(inter_data.beamline,quad_set0,quads1,quads2,
			     bin,nbin,beam,bin2,nbin2,testbeam,jitter_y,
			     shift_y,iter,step_iter,survey,file_name,format);
    for (i=0;i<nbin2;i++){
      bin_delete(bin2[i]);
    }
    xfree(bin2);
  }
  else{
    test_free_correction_new(inter_data.beamline,quad_set0,quads1,quads2,
			     bin,nbin,beam,NULL,0,NULL,jitter_y,
			     shift_y,iter,step_iter,survey,file_name,format);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  xfree(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestFreeCorrectionDipole                      */
/**********************************************************************/

int tk_TestFreeCorrectionDipole(ClientData /*clientdata*/,
				Tcl_Interp *interp,int argc,
				char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,do_rf=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*beamname1=NULL,*testbeamname=NULL,*survey_name,
    survey0[]="Clic";
  BIN **bin;
  BEAM *beam=NULL,*beam1=NULL,*testbeam=NULL;
  int iter=1;
  double jitter_y=0.0,jitter_x=0.0,wgt0=1.0,wgt1=-1.0,pwgt=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,
     (char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"Align the RF after the correction?"},
    {(char*)"-beam0",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-beam1",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname1,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for emittance evaluation"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"emittance file format"},
    {(char*)"-wgt0",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt0,
     (char*)"Weight for the BPM position"},
    {(char*)"-wgt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt1,
     (char*)"Weight for the BPM resolution"},
    {(char*)"-pwgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pwgt,
     (char*)"Weight for the old position"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestFreeCorrectionDipole",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam0 must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (beamname1!=NULL){
    beam1=get_beam(beamname1);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam1 must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  corr.pwgt=pwgt;
  corr.w=wgt1;
  corr.w0=wgt0;

  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide_dipole(inter_data.beamline,nquad,noverlap,bin,
			     &nbin);
  test_free_correction_dipole(inter_data.beamline,bin,nbin,
			      beam,beam1,testbeam,iter,survey,file_name,format);
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  xfree(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestNoCorrection                              */
/**********************************************************************/

int tk_TestNoCorrection(ClientData /*clientdata*/,
			Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error;
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*survey_name,sv[]="Zero";
  void (*survey)(BEAMLINE*);
  BEAM *beam=NULL;
  double bpm_res=0.0;
  int iter=1;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beamname,
     (char*)"Name of the beam to be used for tracking"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,(char*)&survey_name,
     (char*)"Type of prealignment survey to be used"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"emittance file format"},
    {(char*)"-bpm_res",TK_ARGV_FLOAT,(char*)NULL,(char*)&bpm_res,
     (char*)"BPM resolution"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=sv;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestNoCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (error) return error;

  test_no_correction(inter_data.beamline,beam,NULL,iter,
		     survey,file_name,format);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestNoCorrection2                             */
/**********************************************************************/

int tk_TestNoCorrection2(ClientData /*clientdata*/,
			 Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char *file_name=NULL,*file_name2=NULL;
  char *format = default_format;
  char *beamname=NULL,*survey_name,sv[]="Clic";
  void (*survey)(BEAMLINE*);  
  BEAM *beam=NULL;
  int iter=1,inemitt=1;
  double jitter_y=0.0,jitter_x=0.0;

  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-init_emitt_store",TK_ARGV_INT,(char*)NULL,
     (char*)&inemitt,
     (char*)"Relative to the file -emitt_file. If option is specified, 3 values MUST BE given in the order for 3 calls to this function:\n10 (open file and initialise), 20 (averaging) and 30 (close file).\nThese are relative to at least 3 different calls to the command. If only 2 calls are done (so averaging over 2 cases), use only the values 10 and 30."},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beamname,
     (char*)"Name of the beam to be used for tracking"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,(char*)&survey_name,
     (char*)"Type of prealignment survey to be used, defaults to NLC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"emittance file format"},
    {(char*)"-emitt_vs_iter_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name2,
     (char*)"Filename for the results vs machine number, defaults to NULL (no output)"}, 
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=sv;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestNoCorrection2",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  errors.jitter_x=jitter_x;

  if ((jitter_y>0.0)||(jitter_x>0.0)) errors.do_jitter=1;

  test_no_correction_2(inter_data.beamline,beam,NULL,iter,inemitt,
		       survey,file_name,format,file_name2);


  return TCL_OK;
}


/**********************************************************************/
/*                      TestQuadrupoleJitter                          */
/**********************************************************************/

int tk_TestQuadrupoleJitter(ClientData /*clientdata*/,
			    Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error;
  //  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *beamname=NULL;
  BEAM *beam=NULL;
  int iter=1,nstep=2;
  double a0=1e-4,a1=1e-2;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-steps",TK_ARGV_INT,(char*)NULL,(char*)&nstep,
     (char*)"Number of steps to simulate for the jitter amplitude"},
    {(char*)"-a0",TK_ARGV_FLOAT,(char*)NULL,(char*)&a0,
     (char*)"Minimal jitter amplitude [micro m]"},
    {(char*)"-a1",TK_ARGV_FLOAT,(char*)NULL,(char*)&a1,
     (char*)"Maximal jitter amplitude [micro m]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results, defaults to NULL (no output)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TestQuadrupoleJitter",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-machines must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  test_quadrupole_jitter(inter_data.beamline,beam,a0,a1,nstep,iter,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestSimpleAlignment                           */
/**********************************************************************/

int tk_TestSimpleAlignment(ClientData /*clientdata*/,
			   Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,do_rf=0;
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,survey0[]="Nlc";
  BIN **bin;
  void (*survey)(BEAMLINE*);
  BEAM *beam=NULL,*testbeam=NULL;
  int iter=1;
  double jitter_y=0.0,pos_wgt=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of iterations for the tracking"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,(char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-position_wgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pos_wgt,
     (char*)"Weight for the quadrupole position should be (sigma_bpm/sigma_quad)^2"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-do_rf",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"Align the cavities as well"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluating the corrected beamline"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to Nlc"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  errors.bpm_resolution=0.0;
  survey_name=survey0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestSimpleAlignment",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-machines must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide(inter_data.beamline,nquad,noverlap,bin,&nbin);
  test_simple_alignment(inter_data.beamline,bin,nbin,beam,testbeam,iter,
			survey,file_name,format,pos_wgt,0.0,do_rf,NULL);
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestSimpleCorrection                          */
/**********************************************************************/

int tk_TestSimpleCorrection(ClientData /*clientdata*/,
			    Tcl_Interp *interp,int argc,
			    char *argv[])
{

  int error;
  int nquad=1,noverlap=0,nbin,i,interleave=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL,*bin_list=NULL;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,survey0[]="Clic";
  char *bin_file_out=NULL,*bin_file_in=NULL;
  BIN **bin;
  BEAM *beam=NULL,*testbeam=NULL;
  int iter=1,start=0,end=-1;
  double jitter_y=0.0,jitter_x=0.0;
  char *correctors_str=NULL;
  int *correctors, ncorrectors;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&start,
     (char*)"First element to be corrected"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&end,
     (char*)"Last element but one to be corrected (<0: go to the end)"},
    {(char*)"-interleave",TK_ARGV_INT,(char*)NULL,(char*)&interleave,
     (char*)"Used to switch on interleaved bins (correcting only focusing quadrupoles)"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,(char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluating the corrected beamline"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)"-bin_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_list,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-bin_file_out",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_file_out,
     (char*)"Filename for the bin information defaults to NULL (no output)"},
    {(char*)"-bin_file_in",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_file_in,
     (char*)"Filename for the bin information defaults to NULL (no input)"},
    {(char*)"-correctors",TK_ARGV_STRING,(char*)NULL,
     (char*)&correctors_str,
     (char*)"List of correctors to be used"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestSimpleCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (interleave){
    // check bin size
  }
  if (correctors_str) {
    char **v;
    if (error=Tcl_SplitList(interp,correctors_str,&ncorrectors,&v)) return error;
    correctors=new int[ncorrectors];
    for (i=0;i<ncorrectors;i++){
      if (error=Tcl_GetInt(interp,v[i],&correctors[i])) return error;
    }
    Tcl_Free((char*)v);
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  if (bin_file_in) {
    nbin=binarray_read_file(bin_file_in,&bin);
    if (nbin<0){
      return TCL_ERROR;
    }
    nbin=-nbin;
    test_simple_correction(inter_data.beamline,bin,nbin,beam,testbeam,iter,
			   survey,file_name,format);
  }
  else {
    bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
    if (bin_list) {
      if (error=beamline_bin_divide_list(interp,inter_data.beamline,bin,
					 &nbin,bin_list)) {
	return error;
      }
    }
    else {
      if (interleave){
#ifdef TWODIM
	beamline_bin_divide_interleave(inter_data.beamline,bin,&nbin);
#else
	beamline_bpm_switch_off(inter_data.beamline,interleave);
	beamline_bin_divide_1(inter_data.beamline,nquad,noverlap,bin,&nbin);
	beamline_bpm_switch_on(inter_data.beamline);
#endif
      } else {
	printf ("doing end\n");
	if (correctors_str) {
	  placet_printf(INFO,"using generic correctors\n");
	  beamline_bin_divide_limit_correctors(inter_data.beamline,start,end,
					       correctors,ncorrectors,nquad,
					       noverlap,bin,&nbin);
	} else {
	  beamline_bin_divide_limit(inter_data.beamline,start,end,nquad,noverlap,
				    bin,&nbin);
	}
	/*	      beamline_bin_divide(inter_data.beamline,nquad,noverlap,
	  bin,&nbin);*/
      }
    }
    test_simple_correction(inter_data.beamline,bin,nbin,beam,testbeam,iter,
			   survey,file_name,format);
  }
  if (bin_file_out) {
    binarray_write_file(bin_file_out,bin,nbin);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestSimpleCorrectionDipole                    */
/**********************************************************************/

int tk_TestSimpleCorrectionDipole(ClientData /*clientdata*/,
				  Tcl_Interp *interp,
				  int argc,char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,interleave=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,survey0[]="Clic";
  BIN **bin;
  BEAM *beam=NULL,*testbeam=NULL;
  int iter=1;
  double jitter_y=0.0,jitter_x=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-interleave",TK_ARGV_INT,(char*)NULL,(char*)&interleave,
     (char*)"Used to switch on interleaved bins (correcting only focusing quadrupoles)"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,(char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluating the corrected beamline"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
      {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
       (char*)&format,
       (char*)"emittance file format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestSimpleCorrectionDipole",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE*))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (interleave){
    // check bin size
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  if (interleave){
#ifdef TWODIM
    beamline_bin_divide_dipole_interleave(inter_data.beamline,bin,&nbin);
#else
    beamline_bpm_switch_off(inter_data.beamline,interleave);
    beamline_bin_divide_1(inter_data.beamline,nquad,noverlap,bin,&nbin);
    beamline_bpm_switch_on(inter_data.beamline);
#endif
  }
  else{
    beamline_bin_divide_dipole(inter_data.beamline,nquad,noverlap,bin,&nbin);
  }
  test_simple_correction_dipole(inter_data.beamline,bin,nbin,beam,testbeam,
				iter,survey,file_name,format);
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  xfree(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TwissMain                                     */
/**********************************************************************/

int tk_TwissMain(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  char *name=NULL;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"File to produce"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"TwissMain is called only with option -file",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (!name) {
    Tcl_SetResult(interp,"You must specify a file via the option -file",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  file=open_file(name);
  fprintf(file,"1\n0.0\n0.0 0.0 1.0\n0.0\n");
  close_file(file);
  return TCL_OK;
}

/**********************************************************************/
/*                      TwissMainSpread                               */
/**********************************************************************/

int tk_TwissMainSpread(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error,n_slice=1,i;
  char *name=NULL;
  double e_spread=0.0,g_spread=0.0;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"File to produce"},
    {(char*)"-slices",TK_ARGV_INT,(char*)NULL,
     (char*)&n_slice,
     (char*)"File to produce"},
    {(char*)"-energy_spread",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&e_spread,
     (char*)"File to produce"},
    {(char*)"-gradient_spread",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&g_spread,
     (char*)"File to produce"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"TwissMainSpread is called only with options",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (!name) {
    Tcl_SetResult(interp,"You must specify a file via the option -file",TCL_VOLATILE);
    return TCL_ERROR;
  }
  file=open_file(name);
  fprintf(file,"%d\n0.0\n",n_slice);
  for(i=0;i<n_slice;i++){
    fprintf(file,"0.0 %g %g\n",-g_spread*i/n_slice,1.0/n_slice);
  }
  for(i=0;i<(n_slice*(n_slice+1))/2;i++){
    fprintf(file,"0.0\n");
  }
  close_file(file);
  return TCL_OK;
}

/**********************************************************************/
/*                      TwissPlot                                     */
/**********************************************************************/

int tk_TwissPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  char *file_default=NULL,*file_name;
  char *beam_name=NULL;
  int n1=-1,n2=-1;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&n1,
     (char*)"first particle for twiss computation"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&n2,
     (char*)"last particle for twiss computation"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=file_default;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TwissPlot",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  BEAM* beam=get_beam(beam_name);
  if(n1==-1&&n2==-1) {
    n1 = beam->slices/2;
    n2 = beam->slices/2;
  } else {
    if (n1 > beam->slices || n2 > beam->slices) {
      placet_cout << ERROR << " wrong particle number " << endmsg;
      return TCL_ERROR;
    }
  }
  if (file_name){
    twiss_plot(inter_data.beamline,beam,file_name,n1,n2,&fprint_twiss);
  }
  else{
    store_interp.interp=interp;
    twiss_plot(inter_data.beamline,beam,file_default,n1,n2,&fprint_twiss);
  }
  
  return TCL_OK;
}

/**********************************************************************/
/*                      TwissPlotStep                                 */
/**********************************************************************/

int tk_TwissPlotStep(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error;
  char file_default[]="twiss.dat",*file_name;
  char *beam_name=NULL;
  double step=-0.02;
  int n1=-1,n2=-1;
  char *list_str=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)"-step",TK_ARGV_FLOAT,(char*)NULL,(char*)&step,
     (char*)"Step size to be taken for the calculation.\n If less than 0 the parameters will be plotted only in the centres of the quadrupoles"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&n1,
     (char*)"First particle for twiss computation"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&n2,
     (char*)"Last particle for twiss computation"},
    {(char*)"-list",TK_ARGV_STRING,(char*)NULL,
     (char*)&list_str, (char*)"Save the twiss parameters only at the selected elements"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=file_default;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TwissPlotStep",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  BEAM* beam=get_beam(beam_name);
  if(n1==-1&&n2==-1) {
    n1 = beam->slices/2;
    n2 = beam->slices/2;
  } else {
    if ( n1 > beam->slices || n2 > beam->slices ) {
      placet_cout << ERROR << " wrong particle number " << endmsg;
      return TCL_ERROR;
    }
  }

  int *list=NULL, nlist=0;
  if (list_str) {
    char **v;
    if (error=Tcl_SplitList(interp,list_str,&nlist,&v)) return error;
    list=new int[nlist];
    for (int i=0;i<nlist;i++){
      if (error=Tcl_GetInt(interp,v[i],&list[i])) return error;
    }
    Tcl_Free((char*)v);
  }

  if (step>=0.0) {
    twiss_plot_step(inter_data.beamline,beam,file_name,step,n1,n2,
		    &fprint_twiss,list,nlist);
  }
  else {
    placet_cout << INFO << "Stepsize for TwissPlotStep negative or not set, using TwissPlot instead" << endmsg;
    if (file_name){
      twiss_plot(inter_data.beamline,beam,file_name,n1,n2,
		 &fprint_twiss,list,nlist);
    }
    else{
      store_interp.interp=interp;
      twiss_plot(inter_data.beamline,beam,file_default,n1,n2,
		 &fprint_twiss,list,nlist);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      ViewAll                                       */
/**********************************************************************/

int tk_ViewAll(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error,i;
  CALLBACK *callback;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (inter_data.beamline->n_elements<=0) return TCL_OK;

  callback=(CALLBACK*)malloc(sizeof(CALLBACK));
  callback->funct=&testshow2; // testshow2 defined in graph.cc
  callback->interp=interp;
  for (i=0;i<inter_data.beamline->n_elements;i++){
    inter_data.beamline->element[i]->callback=callback;
  }

  callback=(CALLBACK*)malloc(sizeof(CALLBACK));
  callback->funct=&testshow; // testshow defined in graph.cc
  callback->interp=interp;
  inter_data.beamline->element[inter_data.beamline->n_elements-1]->callback=
    callback;
  
  return TCL_OK;
}

/**********************************************************************/
/*                      WakefieldSet                                  */
/**********************************************************************/

int tk_WakefieldSet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  Tk_ArgvInfo table[]={
    {(char*)"-transverse",TK_ARGV_INT,(char*)NULL,
     (char*)&wakefield_data.transv,
     (char*)"switch for transverse wakefield"},
    {(char*)"-longitudinal",TK_ARGV_INT,(char*)NULL,
     (char*)&wakefield_data.longit,
     (char*)"switch for longitudinal wakefield"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <WakefieldSet>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      WakeSet                                       */
/**********************************************************************/


int tk_WakeSet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error,nlist;
  char **list;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,NULL,
     (char*)"This routine sets the long range wakefields modes"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if(error=Tcl_SplitList(interp,argv[2],&nlist,&list)){
    return error;
  }
  int n=nlist/3;
  WAKE_DATA *wake=new WAKE_DATA;
  wake->Q=new double[n];
  wake->a0=new double[n];
  wake->lambda=new double[n];
  wake->k=new double[n];
  wake->n=n;
  if (argv[1]) {
    wake->name=argv[1];
  }
  placet_cout << INFO << "WakeSet, set long range wakefields for RF cavities" << endmsg;
  for (int i=0;i<n;i++){
    char *point;
    wake->lambda[i]=strtod(list[i*3],&point);
    wake->a0[i]=strtod(list[i*3+1],&point);
    wake->Q[i]=strtod(list[i*3+2],&point);
    wake->k[i]=TWOPI/wake->lambda[i];
    placet_cout << INFO << "WakeSet, iteration " << i << " lambda " << wake->lambda[i] << " a0 " << wake->a0[i] << " Q " << wake->Q[i] << endmsg;
  }
  Tcl_Free((char*)list);
  Placet_CreateCommand(interp,argv[1],&tk_WakeEval,wake,NULL);
  int ok;
  Tcl_HashEntry *entry=Tcl_CreateHashEntry(&WakeTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,wake);
    return TCL_OK;
  }
  return TCL_ERROR;
}

/**********************************************************************/
/*                      WireSurvey                                    */
/**********************************************************************/

int tk_WireSurvey(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error=TCL_OK;
  char *name=NULL;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the file where the wire positions are stored"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,NULL,
     (char*)"This routine reads the positions of the wire ends from a file and aligns the girders accordingly"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  if (name){
    file=read_file(name);
    if (wire_survey(file,inter_data.beamline)) {
      close_file(file);
      return TCL_ERROR;
    }
    close_file(file);
  } else {
    Tcl_AppendResult(interp,"You need to specify a file in ",argv[0],"!",NULL);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      Bookshelf                                     */
/**********************************************************************/

int tk_Bookshelf(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error=TCL_OK;
  double x=0.0,y=0.0;
  int i;
  Tk_ArgvInfo table[]={
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,(char*)&x,
     (char*)"RMS amplitude in x [rad]"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,(char*)&y,
     (char*)"RMS amplitude in y [rad]"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,NULL,
     (char*)"This routine bookshelfs the accelerating structures"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  for (i=0;i<inter_data.beamline->n_elements;++i){
    inter_data.beamline->element[i]->set_bookshelf(Misalignments.Gauss(x),Misalignments.Gauss(y));
  }
  return TCL_OK;
}

/*

Commands for luminosity and emittance computation

*/


int tk_Lumi_0(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,char *argv[])
{
  int error,center=1;
  double lum_rel;
  char *beam_1=NULL,*beam_2=NULL;

  /** Enter the first and second beam files at IP to 
      compute the luminosity **/

  Tk_ArgvInfo table[]={
    {(char*)"-beam1",TK_ARGV_STRING,(char*)NULL,(char*)&beam_1,
     (char*)"First beam at IP"},
    {(char*)"-beam2",TK_ARGV_STRING,(char*)NULL,(char*)&beam_2,
     (char*)"Second beam at IP"},
    {(char*)"-center",TK_ARGV_INT,(char*)NULL,(char*)&center,
     (char*)"If 0, compute the luminosity after beam are brought back to the beamline"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  }; 

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc != 1) {
    Tcl_SetResult(interp,"Wrong #args : see Lumi_0",TCL_VOLATILE);
    return TCL_ERROR;
  }

  lum_rel = Comp_Lumi_0(beam_1,beam_2,center);
  char buf[25];
  snprintf(buf,25,"%20g\n",lum_rel);  
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  
  return TCL_OK;
}

int tk_Emitt_0(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,char *argv[])
{
  int error,center=1;
  double emitt1,emitt2;
  char *beam_1=NULL;

  /** Enter the beam file to compute the emittance **/

  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_1,
     (char*)"beam file"},
    {(char*)"-center",TK_ARGV_INT,(char*)NULL,(char*)&center,
     (char*)"If 0, compute the emittance around the beam position and angle centres"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  }; 

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc != 1) {
    Tcl_SetResult(interp,"Wrong #args : we need 1 beam file !",TCL_VOLATILE);
    return TCL_ERROR;
  }

  /*double emitt_rel =*/ Comp_Emitt_0(beam_1,center,&emitt1,&emitt2);

  char buf[50];
  snprintf(buf,50,"%20g %20g\n",emitt1,emitt2);  
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestQuadrupoleSensitivity                     */
/**********************************************************************/

int tk_TestQuadrupoleSensitivity(ClientData /*clientdata*/,
				 Tcl_Interp *interp,
				 int argc,char *argv[])
{
  int error;
  char *file_name=NULL;
  char *beamname=NULL;
  BEAM *beam=NULL;
  //  int iter=1, nstep=2;
  //  double a0=1e-4,a1=1e-2;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TestQuadrupoleSensitivity",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    return TCL_ERROR;
  }

  test_quadrupole_sens(inter_data.beamline,beam,file_name);
  return TCL_OK;
}

int tk_SimpleFeedback(ClientData /*clientdata*/,
		      Tcl_Interp *interp,
		      int argc,char *argv[])
{
  int error;
  int init=0;
  double gain=0.0;
  static int n=-1;
  int i;
  static double *y0=NULL,*y=NULL,*x0=NULL,*x=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-init",TK_ARGV_INT,(char*)NULL,
     (char*)&init,
     (char*)"If not zero initialise routine"},
    {(char*)"-gain",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain,
     (char*)"gain"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to SimpleFeedback",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (init) {
    n=inter_data.beamline->n_elements;
    y0=(double*)realloc(y0,sizeof(double)*n);
    y=(double*)realloc(y,sizeof(double)*n);
    x0=(double*)realloc(x0,sizeof(double)*n);
    x=(double*)realloc(x,sizeof(double)*n);
    for (i=0;i<n;++i){
      y0[i]=inter_data.beamline->element[i]->offset.y;
      x0[i]=inter_data.beamline->element[i]->offset.x;
    }
  }
  else {
    if (n<0) {
      return TCL_ERROR;
    }
    for (i=0;i<n;++i){
      inter_data.beamline->element[i]->offset.x+=-gain*(inter_data.beamline->element[i]->offset.x-x0[i]);
      inter_data.beamline->element[i]->offset.y+=-gain*(inter_data.beamline->element[i]->offset.y-y0[i]);
    }
  }
  return TCL_OK;
}
} // end anonymous namespace

void placet_init()
{
  RANDOM_NEW::reset("all"); 	// sets all RNGs to default seeds, initialisation not needed
  rf_init();
  corr_init();
  errors_init();
  bpm_init(0,0);
  wakefield_init(1,1);
  bump_init(1,0,0);
  inter_data.beamline=new BEAMLINE();
  inter_data.beamline_set=0;
  inter_data.track_photon=0;
  inter_data.synrad=false;
  inter_data.thin_lens=0;
  inter_data.longitudinal=false;
  corr.pwgt=0.0;
  zero_point=0.0;
  data_nstep=1;
  drive_data.rf_kick=0;
  drive_data.rf_long=0;
  switches_init();
  bump_data.maxshift=1e6;
  bump_data.ncav=1;
#ifdef EARTH_FIELD
  earth_field.x=0.0;
  earth_field.y=0.0;
#endif
  placet_switch.first_order=0;
  quad_wake.on=0;
  bump_long=0;
  print_data.num_warnings=0;
  print_data.num_errors=0;
}

void placet_init2(Tcl_Interp *interp)
{
  survey_init(interp);
}

int Placet_Init(Tcl_Interp *interp)
{
  int error;
  /* initialisation of C part */

  placet_init();

  //  Tcl_StaticPackage(interp, "Tk", Tk_Init, (Tcl_PackageInitProc *) NULL);
  /* Variables for error sources */

  Survey_Init(interp);

  Tcl_LinkVar(interp,"quadrupole_error",(char*)&survey_errors.quad_error,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"piece_error",(char*)&survey_errors.piece_error,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"drift_error",(char*)&survey_errors.drift_error,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"cavity_error",(char*)&survey_errors.cav_error,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"bpm_error",(char*)&survey_errors.bpm_error,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"quadrupole_roll",(char*)&survey_errors.quad_roll,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"quadrupole_move_error",(char*)&errors.quad_move_res,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"bpm_resolution",(char*)&errors.bpm_resolution,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"atl_time",(char*)&survey_errors.time,
	      TCL_LINK_DOUBLE);
  Tcl_LinkVar(interp,"envelope_cut",(char*)&switches.envelope_cut,
	      TCL_LINK_DOUBLE);

  Tcl_LinkVar(interp,"emitt_axis",(char*)&switches.emitt_axis,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"emitt_start",(char*)&switches.e_start,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"emitt_range",(char*)&switches.e_range,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"bump_quad_distance",(char*)&bump_data.quad_dist,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"bump_emitt_distance",(char*)&bump_data.emitt_dist,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"bump_cavity_number",(char*)&bump_data.ncav,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"bump3_qn",(char*)&bump_data.nw,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"bump3_qd",(char*)&bump_data.ns,
	      TCL_LINK_INT);

  Tcl_LinkVar(interp,"bump_max_shift",(char*)&bump_data.maxshift,
	      TCL_LINK_DOUBLE);

  /* definition of commands */

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"MultipoleNumberList",&tk_MultipoleNumberList,
		       NULL,NULL);

  Placet_CreateCommand(interp,"ShortRangeWake",&tk_ShortRangeWake,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"MultipoleGetStrengthList",
		       &tk_MultipoleGetStrengthList,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"MultipoleSetStrengthList",
		       &tk_MultipoleSetStrengthList,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"AddElement",&tk_AddElement,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"BeamlineSetSine",&tk_BeamlineSetSine,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"SaveAllPositions",&tk_SaveAllPositions,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"ReadAllPositions",&tk_ReadAllPositions,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"ReadAllMotion",&tk_ReadAllMotion,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"ReadQuadMotion",&tk_ReadQuadMotion,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"MoveGirder",&tk_MoveGirder,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"GroundMotionATL",&tk_GroundMotionATL,NULL,NULL);
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"GroundMotionInit",&tk_GroundMotionInit,NULL,NULL);
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"GroundMotion",&tk_GroundMotion,NULL,NULL);
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"AddPreIsolator",&tk_AddPreIsolator,NULL,NULL);
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"AddGMFilter",&tk_AddGMFilter,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"WriteGirderLength",&tk_WriteGirderLength,NULL,
		       NULL);

  //  Placet_CreateCommand(interp,"IonPlot",&tk_IonPlot,NULL,
  //		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BpmPositionPlot",
		       &tk_BpmPositionPlot,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"QuadrupolePositionPlot",
		       &tk_QuadrupolePositionPlot,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamEnergyPlot",
		       &tk_BeamEnergyPlot,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamEnergyPrint",
		       &tk_BeamEnergyPrint,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamEnergyProfilePlot",
		       &tk_BeamEnergyProfilePlot,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"WakeSet",&tk_WakeSet,NULL,NULL);

  Placet_CreateCommand(interp,"QuadrupoleStepSize",
		       &tk_QuadrupoleStepSize,NULL,NULL);

  Placet_CreateCommand(interp,"SurveyErrorSet",
		       &tk_SurveyErrorSet,NULL,NULL);

  Placet_CreateCommand(interp,"SurveyErrorClear",
		       &tk_SurveyErrorClear,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::MODIFY, interp,"FirstOrder",&tk_FirstOrder,NULL,
		       NULL);
  
  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"EnvelopeCut",&tk_EnvelopeCut,
		       NULL,NULL);

  Placet_CreateCommand(interp,"Help",&tk_Help,NULL,NULL);
  Placet_CreateCommand(interp,"Help2",&tk_Help2,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::MODIFY, interp,"BpmSetToOffset",
		       &tk_BpmSetToOffset,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"ElementSetToOffset",
		       &tk_ElementSetToOffset,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::MODIFY, interp,"ElementInfo",
		       &tk_ElementInfo,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"ElementAddOffset",
		       &tk_ElementAddOffset,NULL,NULL);

  Placet_CreateCommand(interp,"QuadWake",&tk_QuadWake,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"TwissMain",&tk_TwissMain,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"TwissMainSpread",
		       &tk_TwissMainSpread,NULL,NULL);

  Placet_CreateCommand(interp,"MainGradient",&tk_MainGradient,
		       NULL,NULL);

  Placet_CreateCommand(interp,"WakefieldSet",&tk_WakefieldSet,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::MODIFY, interp,"EarthField",&tk_EarthField,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"BpmRealign",&tk_BpmRealign,NULL,
		       NULL);

  Placet_CreateCommand(interp,"LongBumps",&tk_LongBumps,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"BeamlineBinDivide",
		       &tk_BeamlineBinDivide,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BeamlineList",&tk_BeamlineList,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::MODIFY, interp,"BunchesSetOffsetRandom",
		       &tk_BunchesSetOffsetRandom,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::MODIFY, interp,"BeamSetToOffset",
		       &tk_BeamSetToOffset,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::MODIFY, interp,"BeamModulateEnergy",
		       &tk_BeamModulateEnergy,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::MODIFY, interp,"BeamAddOffset",&tk_BeamAddOffset,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::MODIFY, interp,"BeamSetOffsetSine",
		       &tk_BeamSetOffsetSine,NULL,NULL);

  Placet_CreateCommand(interp,"SetRfGradient",&tk_SetRfGradient,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestRfAlignment",&tk_TestRfAlignment,
		       NULL,NULL);

  Placet_CreateCommand(interp,"SetRfGradientSingle",
		       &tk_SetRfGradientSingle,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"QuadrupoleNumberList",
		       &tk_QuadrupoleNumberList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BpmNumberList",
		       &tk_BpmNumberList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"PlotBeamSpectrum",
		       &tk_PlotBeamSpectrum,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamLoad",&tk_BeamLoad,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamWriteRms",&tk_BeamWriteRms,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamSave",&tk_BeamSave,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamSaveAll",&tk_BeamSaveAll,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamLoadAll",&tk_BeamLoadAll,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamSaveGp",&tk_BeamSaveGp,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamSaveMad",&tk_BeamSaveMad,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamGetPosition",&tk_BeamGetPosition,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamGetSize",&tk_BeamGetSize,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamSaveAbsolute",
		       &tk_BeamSaveAbsolute,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BpmPlot",&tk_BpmPlot,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BeamlinePositionPlot",
		       &tk_BeamlinePositionPlot,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestQuadrupoleJitter",
		       &tk_TestQuadrupoleJitter,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestQuadrupoleSensitivity",
		       &tk_TestQuadrupoleSensitivity,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestSpectrum",&tk_TestSpectrum,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestSpectrumSimpleCorrection",
		       &tk_TestSpectrumSimpleCorrection,NULL,NULL,"");

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BeamlineInfo",&tk_BeamlineInfo,
		       NULL,NULL); 

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"TempView",&tk_TempView,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"TempView2",&tk_TempView2,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"ViewAll",&tk_ViewAll,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamlineShow",&tk_BeamlineShow,
		       NULL,NULL);


  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamShow",&tk_BeamShow,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"MatchFodo",&tk_MatchFodo,NULL,
		       NULL, "This command calculates the matched twiss parameters in the centre of a quadrupole of a FODO cells. \
It returns a list in the form\n\
@tex\n\
$$\n\
{\\{ alpha\\_x {\\it value} beta\\_x {\\it value} mu\\_x {\\it value} alpha\\_y {\\it value} beta\\_y {\\it value} mu\\_y {\\it value}\\}}\n\
$$\n\
@end tex\n\
. Here, \n\
@iftex\n\
@tex\n\
{\\tt beta\\_x} and {\\tt beta\\_y}\n\
@end tex\n\
@end iftex\n\
are the horizontal and vertical beta-functions in metre,\n\
@iftex\n\
@tex\n\
{\\tt alpha\\_x} and {\\tt alpha\\_y}\n\
@end tex\n\
@end iftex\n\
are the \n\
@iftex\n\
@tex\n\
$\\alpha$\n\
@end tex\n\
@end iftex\n\
-parameters and\n\
@iftex\n\
@tex\n\
{\\tt mu\\_x} and {\\tt mu\\_y}\n\
@end tex\n\
@end iftex\n\
are the phase advances in radian.");

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"MatchTriplet",&tk_MatchTriplet,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"MatchDoublet",&tk_MatchDoublet,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"PlotFillFactor",
		       &tk_PlotFillFactor,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"DivideBeamline",
		       &tk_DivideBeamline,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"DivideBeamline2",
		       &tk_DivideBeamline2,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestNoCorrection",
		       &tk_TestNoCorrection,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestFeedback",&tk_TestFeedback,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestIndependentFeedback",
		       &tk_TestIndependentFeedback,NULL,NULL);
  /* OBSOLETE
     Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestStaticRfAlign",&tk_TestStaticRfAlign,NULL,
     NULL);
  */

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestSimpleCorrection",
		       &tk_TestSimpleCorrection,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestNoCorrection2",
		       &tk_TestNoCorrection2,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestSimpleCorrectionDipole",
		       &tk_TestSimpleCorrectionDipole,NULL,
                       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"DipoleSetZero",
		       &tk_DipoleSetZero,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::INSPECT, interp,"DipoleNumberList",
		       &tk_DipoleNumberList,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::INSPECT, interp,"DipoleGetStrengthList",
		       &tk_DipoleGetStrengthList,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"DipoleSetStrengthList",
		       &tk_DipoleSetStrengthList,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestFreeCorrection",
		       &tk_TestFreeCorrection,NULL,NULL,"Tests Dispersion-Free Steering.");

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestTrainCorrection",
		       &tk_TestTrainCorrection,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestMeasuredCorrection",
		       &tk_TestMeasuredCorrection,NULL,NULL,"Tests the Wakefield-Free Steering.");

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestFreeCorrection2",
		       &tk_TestFreeCorrection2,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestFreeCorrectionDipole",
		       &tk_TestFreeCorrectionDipole,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestSimpleAlignment",
		       &tk_TestSimpleAlignment,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestBallisticCorrection",
		       &tk_TestBallisticCorrection,NULL,NULL,"Tests the ballistic correction.");

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"CheckAutophase",
		       &tk_CheckAutophase,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"DispersionPlot",
		       &tk_DispersionPlot,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"TwissPlot",&tk_TwissPlot,NULL,
		       NULL);
  
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"TwissPlotStep",&tk_TwissPlotStep,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BnsPlot",&tk_BnsPlot,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"EnergySpreadPlot",
		       &tk_EnergySpreadPlot,NULL,NULL,"Plots the energy spread along the beamline into a file.");
  
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"QuadrupoleSetting",
		       &tk_QuadrupoleSetting,NULL,NULL);
  
  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BpmReadings",&tk_BpmReadings,
		       NULL,NULL);
  
  Placet_CreateCommand(interp,"ApertureLosses",&tk_ApertureLosses,
		       NULL,NULL);
  
  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"CavityDefine",&tk_CavityDefine,
		       NULL,NULL);
  
  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"InjectorCavityDefine",
		       &tk_InjectorCavityDefine,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::CREATE, interp,"DriveBeam",&tk_DriveBeam,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::CREATE, interp,"InjectorBeam",&tk_InjectorBeam,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestBeam",&tk_TestBeam,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::CREATE, interp,"MainBeam",&tk_MainBeam,NULL,
		       NULL,"Creates a main beam.");

  Placet_CreateCommand(interp,"Clic",&tk_Clic,NULL,
		       NULL);

  Placet_CreateCommand(interp,"SetupMainBeamline",
		       &tk_SetupMainBeamline,NULL,NULL);

  Placet_CreateCommand(interp,"SetupSmoothBeamline",
		       &tk_SetupSmoothBeamline,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"QuadrupoleSetStrength",
		       &tk_QuadrupoleSetStrength,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"ElementSetLength",
		       &tk_ElementSetLength,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::MODIFY, interp,"DipoleSetStrength",
		       &tk_DipoleSetStrength,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::INSPECT, interp,"QuadrupoleGetStrength",
		       &tk_QuadrupoleGetStrength,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"QuadrupoleSetStrengthList",
		       &tk_QuadrupoleSetStrengthList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"QuadrupoleGetStrengthList",
		       &tk_QuadrupoleGetStrengthList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"CavitySetGradientList",
		       &tk_CavitySetGradientList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"CavityGetGradientList",
		       &tk_CavityGetGradientList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"CavitySetPhaseList",
		       &tk_CavitySetPhaseList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"CavityGetPhaseList",
		       &tk_CavityGetPhaseList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"CavitySetTypeList",
		       &tk_CavitySetTypeList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"CavityGetTypeList",
		       &tk_CavityGetTypeList,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BpmPositionListZ",
		       &tk_BpmPositionListZ,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"QuadrupolePositionListZ",
		       &tk_QuadrupolePositionListZ,NULL,NULL);

  Placet_CreateCommand(MANUAL::TOOLS, MANUAL::NONE, interp,"PhaseAdvance",
		       &tk_PhaseAdvance,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp, "TestBumpCorrection",&tk_TestBumpCorrection,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Girder",&tk_Girder,NULL,
		       NULL,"Creates a new girder to place the elements created in the following.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Drift",&tk_Drift,NULL,
		       NULL, "Creates a drift and places it on the current girder.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Bpm",&tk_Bpm,NULL,
		       NULL,"Creates a BPM and places it on the current girder.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp, "CavityBpm", tk_CavityBpm, NULL,
                       NULL,"Creates a Cavity BPM and places it on the current girder.");

  Placet_CreateCommand(interp,"ReferencePoint",&tk_ReferencePoint,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Quadrupole",&tk_Quadrupole,NULL,
		       NULL, "Creates a quadrupole and places it on the current girder");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Dipole",&tk_Dipole,NULL,
		       NULL,"Creates a correction dipole and places it on the current girder.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Solenoid",&tk_Solenoid,NULL,
		       NULL,"Creates a solenoid and places it on the current girder.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Aperture",&tk_Aperture,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Multipole",&tk_Multipole,NULL,
		       NULL,"Creates a multipole and places it on the current girder.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"RfMultipole",&tk_RfMultipole,NULL,
		       NULL,"Creates a rf multipole and places it on the current girder.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"QuadBpm",&tk_QuadBpm,NULL,
		       NULL,"Creates a quadrupole with a BPM in its centre and places it on the current girder.");

  /*
    Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"DecCavity",&tk_DecCavity,NULL,
    NULL, "Creates a decelerating cavity and places it on the current girder.");
  */

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"AccCavity",&tk_AccCavity,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"AddBPMsToBeamline",&tk_AddBPMsToBeamline,NULL,NULL,
		       "Adds BPMs before (after) each quadrupole.");

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"AddDipolesToBeamline",&tk_AddDipolesToBeamline,NULL,NULL,
		       "Adds dipoles after (before) each quadrupole.");

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"AddDipoleGridToBeamline",&tk_AddDipoleGridToBeamline,NULL,NULL,
		       "Adds a grid of dipoles to the beamline.");

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Sbend",&tk_Sbend,NULL,
		       NULL,"Creates a bend and places it on the current girder.");

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"SurveyBanana",&tk_SurveyBanana,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"SurveyScatterCells",&tk_SurveyScatterCells,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Twostream",&tk_Twostream,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Focus",&tk_Focus,NULL,
		       NULL);

  //  Placet_CreateCommand(interp,"ExternalTransport",&tk_Mad,NULL,
  //		       NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::MODIFY, interp,"SlicesToParticles",&tk_SlicesToParticles,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Cavity",&tk_Cavity,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::CREATE, interp,"BeamlineSet",&tk_BeamlineSet,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::CREATE, interp,"BeamlineNew",&tk_BeamlineNew,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::CREATE, interp,"BeamlineUse",&tk_BeamlineUse,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::CREATE, interp,"BeamlineDelete",&tk_BeamlineDelete,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"Rmatrix",&tk_Rmatrix,NULL,NULL);

  Placet_CreateCommand(MANUAL::ELEMENT, MANUAL::CREATE, interp,"TclCall",&tk_TclCall,NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BeamlinePrint",&tk_BeamlinePrint,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BeamlinePrintSurvey",&tk_BeamlinePrintSurvey,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"WireSurvey",&tk_WireSurvey,
		       NULL,NULL);

  Placet_CreateCommand(interp,"Bookshelf",&tk_Bookshelf,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::INSPECT, interp,"BpmDisplay",&tk_BpmDisplay,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamRead",&tk_BeamRead,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamDump",&tk_BeamDump,
		       NULL,NULL);

  Placet_CreateCommand(MANUAL::BEAM, MANUAL::INSPECT, interp,"BeamDelete",&tk_BeamDelete,
		       NULL,NULL);

  Placet_CreateCommand(interp,"MoveInitSine",&tk_MoveInitSine,NULL,
		       NULL);

  Placet_CreateCommand(interp,"MoveStepSine",&tk_MoveStepSine,NULL,
		       NULL);

  Placet_CreateCommand(interp,"SimpleFeedback",&tk_SimpleFeedback,NULL,
		       NULL);

  Placet_CreateCommand(interp,"InterGirderMove",&tk_InterGirderMove,NULL,
		       NULL);

  Placet_CreateCommand(MANUAL::BEAMLINE, MANUAL::MODIFY, interp,"ScatterGirder",&tk_ScatterGirder,NULL,
		       NULL);

  Placet_CreateCommand(interp,"BpmToCavScatter",&tk_BpmToCavScatter,NULL,
		       NULL);

  Placet_CreateCommand(interp,"Lumi_0",&tk_Lumi_0,NULL,NULL);

  Placet_CreateCommand(interp,"Emitt_0",&tk_Emitt_0,NULL,NULL);

  Placet_CreateCommand(interp,"IPFeedback_0",&tk_IPFeedback_0,NULL,NULL);

  Placet_CreateCommand(MANUAL::TRACKING, MANUAL::NONE, interp,"TestIntRegion",&tk_TestIntRegion,NULL,NULL);

  Placet_CreateCommand(interp, "About", tk_About, (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);

  Power_Init(interp);

  StoreParticles_Init(interp);
  LaserMeasure_Init(interp);
  Correct_Init(interp);
  Peder_Init(interp);
  Daniel_Init(interp);
#ifdef HTGEN
  Background_Init(interp);
#endif
  TrackPhoton_Init(interp);
  Andrea::Init(interp);  

  /*
    Placet_CreateCommand(interp,"ReadMad",&tk_ReadMad,NULL,
    NULL);*/

  /* loading of other packages */

  Rndm_Init(interp);

  /* table initialisation */

  Tcl_InitHashTable(&WakeTable,TCL_STRING_KEYS);
  Tcl_InitHashTable(&ModeTable,TCL_STRING_KEYS);
  Tcl_InitHashTable(&BeamTable,TCL_STRING_KEYS);
  Tcl_InitHashTable(&QuadTable,TCL_STRING_KEYS);
  Tcl_InitHashTable(&SurveyTable,TCL_STRING_KEYS);
  Tcl_InitHashTable(&NameTable,TCL_STRING_KEYS);

  Spline_Init(interp);
  ShortRange_Init(interp);
  placet_init2(interp);
  fflush(stdout);

  if (error=Tools_Init(interp)) return error;
  if (error=Histogram_Init(interp)) return error;
  if (error=Minimise_Init(interp)) return error;
  Tcl_PkgProvide(interp,"Placet","0.9");
  return TCL_OK;
}
