#include <tcl.h>
#include <tk.h>
#include "witness.c"
#include "collimator.h"
#include "curvature.h"
#include "energyloss.h"

int Daniel_Init(Tcl_Interp *interp)
{
  Witness_Init(interp);
  Placet_CreateCommand(interp,"Collimator",&tk_Collimator,NULL,NULL);
  Placet_CreateCommand(interp,"Curvature",&tk_Curvature,NULL,NULL);
  Placet_CreateCommand(interp,"EnergyLoss",&tk_EnergyLoss,NULL,NULL);
  return TCL_OK;
}
