#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc,char *argv[])
{
    FILE *fin,*fout;
    char buffer[1000],*point;
    double x,xp,y,yp,e,z,z0=0.0;
    double e0;

    if (argc>2) {
      z0=strtod(argv[2],NULL);
    }
    e0=strtod(argv[1],NULL);
    fin=fopen("madtrack.out","r");
    fout=fopen("particles.out","w");
    while(point=fgets(buffer,1000,fin)){
	if (strncmp(buffer,"RETURN",6)==0) break;
	point=fgets(buffer,1000,fin);
	point+=11;
	x=strtod(point,&point);
	point+=12;
	xp=strtod(point,&point);
	point=fgets(buffer,1000,fin);
	point+=11;
	y=strtod(point,&point);
	point+=12;
	yp=strtod(point,&point);
	point=fgets(buffer,1000,fin);
	point+=11;
	z=strtod(point,&point);
	point+=12;
	e=strtod(point,&point);
	
	fprintf(fout,"%g %g %g %g %g %g\n",e0*(1.0+e),x*1e6,y*1e6,-z*1e6-z0,
		xp*1e6/(1.0+e),yp*1e6/(1.0+e));

	/*
	fprintf(fout,"%g %g %g %g %g %g\n",e0*(1.0+e),x*1e6,y*1e6,-z*1e6-z0,
		xp*1e6/(1.0+e),yp*1e6/(1.0+e));
	*/
    }
    fclose(fin);
    fclose(fout);
    exit(0);
}
