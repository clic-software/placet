#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cctype>
#include <cmath>
#include <tcl.h>
#include <tk.h>
#include <vector>
#include "select.h"

#include "placet.h"
#include "placet_tk.h"
#include "rmatrix.h"
#include "structures_def.h"
#include "random.hh"

void
one_particle_ellipse(double betax,double alphax,double rx,
		     double betay,double alphay,double ry,
		     double phasex,double phasey,PARTICLE *p)
{
  double s,c;
  sincos(phasex,&s,&c);
  p->x=s*sqrt(betax*rx);
  p->xp=c*sqrt(rx/betax)-p->x*alphax/betax;
  sincos(phasey,&s,&c);
  p->y=s*sqrt(ry*betay);
  p->yp=c*sqrt(ry/betay)-p->y*alphay/betay;
}

void
one_particle_from_twiss(double betax,double alphax,double rx,
			double betay,double alphay,double ry,
			double e0,double se,
			PARTICLE *p)
{
  p->x=Select.Gauss()*sqrt(betax*rx);
  p->xp=Select.Gauss()*sqrt(rx/betax)-p->x*alphax/betax;
  p->y=Select.Gauss()*sqrt(ry*betay);
  p->yp=Select.Gauss()*sqrt(ry/betay)-p->y*alphay/betay;
  p->energy=e0+Select.Gauss()*se;
}

void
one_particle(R_MATRIX *sxx,R_MATRIX * /*sxy*/,R_MATRIX *syy,PARTICLE *p0,
	     PARTICLE *p)
{
  p->wgt=1.0;
  p->x=Select.Gauss()*sqrt(sxx->r11);
  p->xp=Select.Gauss()*sqrt(sxx->r22-sxx->r12*sxx->r12/sxx->r11)
    +p->x*sxx->r12/sxx->r11;
  p->y=Select.Gauss()*sqrt(syy->r11);
  p->yp=Select.Gauss()*sqrt(syy->r22-syy->r12*syy->r12/syy->r11)
    +p->y*syy->r12/syy->r11;
  p->x+=p0->x;
  p->xp+=p0->xp;
  p->y+=p0->y;
  p->yp+=p0->yp;
  p->energy=p0->energy;
}

void
beam_to_particles(FILE *f,Tcl_Channel *ch,
		  BEAM *b,int bunch,int np,int tt,double e0)
{
  int j,n,n1,k=0;
  double sumw=0.0;
  PARTICLE p;
  char buffer[1023];

  if (b->particle_beam) {
    np=b->slices;
    if (e0<=0.0) {
      for(int i=0;i<np;++i){
	sumw+=b->particle[i].wgt;
	e0+=b->particle[i].energy*b->particle[i].wgt;
      }
      e0/=sumw;
    }
    for (int i=0;i<b->slices_per_bunch;++i){
      for (j=0;j<b->particle_number[i];++j) {
	p=b->particle[k++];
	switch(tt) {
	case 0:
	  snprintf(buffer,1023,"%.15g %.15g %.15g %.15g %.15g %.15g\n",p.energy,p.x,p.y,
		  b->z_position[i],p.xp,p.yp);
	  break;
	case 1:
	  snprintf(buffer,1023,"START, X=%.15g,PX=%.15g,&\nY=%.15g,PY=%.15g,&\nt=%.15g,deltap=%.15g\n",
		  p.x*1e-6,p.xp*1e-6,p.y*1e-6,p.yp*1e-6,
		  b->z_position[i]*1e-6,(p.energy-e0)/e0);
	  break;
	default:
	  placet_cout << ERROR << "beam_to_particles: flag not existing: " << tt << endmsg;
	  exit(1);
	  break;
	}
	if (ch) {
	  Tcl_Write(*ch,buffer,-1);
	}
	else {
	  fputs(buffer,f);
	}
      }
    }
  }
  else {
    //    Select.SetSeed(seed);
    n=b->slices_per_bunch*b->macroparticles;
    n1=n*bunch;
    std::vector<double> weights;
    for(int i =0; i<n; i++)
    {
      weights.push_back(b->particle[i+n1].wgt);
    }
    Select.DiscreteDistributionSet(weights); 
    if (e0<=0.0) {
      for(int i=n1;i<n1+n;++i){
	sumw+=b->particle[i].wgt;
	e0+=b->particle[i].energy*b->particle[i].wgt;
      }
      e0/=sumw;
    }
    for (int i=0;i<np;++i){
      n=Select.DiscreteDistribution()+n1;
      one_particle(b->sigma_xx+n,b->sigma_xy+n,b->sigma+n,b->particle+n,&p);
      switch(tt) {
      case 0:
	snprintf(buffer,1023,"%.15g %.15g %.15g %.15g %.15g %.15g\n",p.energy,p.x,p.y,
		b->z_position[n/b->macroparticles],p.xp,p.yp);
	break;
      case 1:
	snprintf(buffer,1023,"START, X=%.15g,PX=%.15g,&\nY=%.15g,PY=%.15g,&\nt=%.15g,deltap=%.15g\n",
		p.x*1e-6,p.xp*1e-6,p.y*1e-6,p.yp*1e-6,
		-b->z_position[n/b->macroparticles]*1e-6,(p.energy-e0)/e0);
	break;
      default:
	placet_cout << ERROR << "beam_to_particles: flag not existing: " << tt << endmsg;
	exit(1);
	break;
      }
      if (ch) {
	Tcl_Write(*ch,buffer,-1);
      }
      else {
	fputs(buffer,f);
      }
    }
  }
}

int
Tcl_StoreParticles(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  int i,n,wf;
  char *fn=NULL,*form="placet",*fc=NULL;
  char *nc[2]={"placet","mad"};
  FILE *f;
  int b1=0,tt=0;
  double e0=0.0;
  Tcl_Channel ch;
  Tk_ArgvInfo table[]={
    {(char*)"-file_name",TK_ARGV_STRING,(char*)NULL,
     (char*)&fn,
     (char*)"Name of the file in which the particles are stored"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&fc,
     (char*)"Name of the Tcl file in which the particles are stored"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&form,
     (char*)"Name of the file format"},
    {(char*)"-particles",TK_ARGV_INT,(char*)NULL,
     (char*)&n,
     (char*)"Number of particle to store"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&e0,
     (char*)"Reference energy (used for MAD)"},
    {(char*)"-bunch",TK_ARGV_INT,(char*)NULL,
     (char*)&b1,
     (char*)"Number of the bunch to store"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <StoreParticles>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  for (i=0;i<(int)strlen(form);i++){
    form[i]=tolower(form[i]);
  }
  for (i=0;i<2;i++){
    if (strcmp(nc[i],form)==0) {
      tt=i;
      break;
    }
  }
  if (fc) {
    if (fn) {
      Tcl_AppendResult(interp,"You cannot specify -file and file_name in ",
		       argv[0],NULL);
      return TCL_ERROR;
    }
    ch=Tcl_GetChannel(interp,fc,&wf);
    beam_to_particles((FILE*)NULL,&ch,inter_data.bunch,b1,n,tt,e0);
  }
  else {
    f=open_file(fn);
    //beam_to_particles(f,(Tcl_Channel*)NULL,inter_data.bunch,b1,n,1,tt,e0);
    beam_to_particles(f,(Tcl_Channel*)NULL,inter_data.bunch,b1,n,tt,e0);
    close_file(f);
  }
  return TCL_OK;
}

int
Tcl_ParticlesOnEllipse(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  int i,n1,n2,fill=0;
  BEAM *beam;
  char *name=NULL,*sn1="start",*sn2="end";
  int tail=0;
  double alphax=0.0,alphay=0.0,betax=1.0,betay=1.0,rx=0.0,ry=0.0,rxx,ryy;
  double dphx,dphy,phx,phy,energy=-1.0,sigma_energy=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"Name of the beam"},
    {(char*)"-fill",TK_ARGV_INT,(char*)NULL,
     (char*)&fill,
     (char*)"If not 0 the particles will be selected from a Gaussian"},
    {(char*)"-emitt_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&rx,
     (char*)"Emittance in x"},
    {(char*)"-emitt_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&ry,
     (char*)"Emittance in y"},
    {(char*)"-beta_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&betax,
     (char*)"Beta-function in x direction"},
    {(char*)"-beta_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&betay,
     (char*)"Beta-function in y direction"},
    {(char*)"-alpha_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&alphax,
     (char*)"Correlation in x"},
    {(char*)"-alpha_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&alphay,
     (char*)"Correlation in y"},
    {(char*)"-energy",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&energy,
     (char*)"Reference energy"},
    {(char*)"-sigma_energy",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sigma_energy,
     (char*)"Reference energy"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,(char*)&sn1,
     (char*)"first particle to be offset"},
    {(char*)"-end",TK_ARGV_STRING,(char*)NULL,(char*)&sn2,
     (char*)"last particle to be offset"},
    {(char*)"-repeat",TK_ARGV_INT,(char*)NULL,(char*)&tail,
     (char*)"If not zero repeat the procedure"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ParticlesOnEllipse>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (name) {
    beam=get_beam(name);
  }
  else {
    beam=inter_data.bunch;
  }

  if (tail) {
    n1=1;
    n2=tail;
    while (n2<=beam->slices) {
      if (fill) {
	rxx=rx*EMITT_UNIT/1e-12*EMASS/beam->particle[n1-1].energy;
	ryy=ry*EMITT_UNIT/1e-12*EMASS/beam->particle[n1-1].energy;
	for (i=n1;i<n2;++i) {
	  one_particle_from_twiss(betax,alphax,rxx,betay,alphay,ryy,
				  energy,sigma_energy,beam->particle+i);
	}
      }
      else {
	dphx=TWOPI/(n2-n1);
	dphy=TWOPI/(n2-n1);
	phx=0.0;
	phy=0.0;
	rxx=rx*EMITT_UNIT/1e-12*EMASS/beam->particle[n1-1].energy;
	ryy=ry*EMITT_UNIT/1e-12*EMASS/beam->particle[n1-1].energy;
	for (i=n1;i<n2;++i) {
	  one_particle_ellipse(betax,alphax,rxx,betay,alphay,ryy,
			       phx,phy,beam->particle+i);
	  //	  beam->particle[i].energy=energy;
	  phx+=dphx;
	  phy+=dphy;
	}
      }
      n1+=tail;
      n2+=tail;
    }
  }
  else {
    if (error=eval_beam_position(interp,beam,sn1,&n1)) {
      return error;
    }
    if (error=eval_beam_position(interp,beam,sn2,&n2)) {
      return error;
    }
    if (fill) {
      rx*=EMITT_UNIT/1e-12*EMASS/energy;
      ry*=EMITT_UNIT/1e-12*EMASS/energy;
      for (i=n1;i<n2;++i) {
	one_particle_from_twiss(betax,alphax,rx,betay,alphay,ry,
				energy,sigma_energy,beam->particle+i);
      }
    }
    else {
      dphx=TWOPI/(n2-n1);
      dphy=TWOPI/(n2-n1);
      phx=0.0;
      phy=0.0;
      rx*=EMITT_UNIT/1e-12*EMASS/energy;
      ry*=EMITT_UNIT/1e-12*EMASS/energy;
      for (i=n1;i<n2;++i) {
	one_particle_ellipse(betax,alphax,rx,betay,alphay,ry,
			     phx,phy,beam->particle+i);
	beam->particle[i].energy=energy;
	phx+=dphx;
	phy+=dphy;
      }
    }
  }
  return TCL_OK;
}
  
void
StoreParticles_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"StoreParticles",Tcl_StoreParticles,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"ParticlesOnEllipse",Tcl_ParticlesOnEllipse,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
}
