
#define CAVSTEP wake->steps
#include <cstdlib>
#include <limits>

#include "beam.h"
#include "distribute.h"
#include "rfkick.h"
#include "matrix.h"
#include "sort.h"

#include "particle.h"


/*
  The routine steps through a PETS cavity using first second transport
*/



#ifdef SECOND_ORDER
void CAVITY_PETS::step_4d(BEAM *bunch )
#else
  void CAVITY_PETS::step_4d_0(BEAM *bunch )
#endif

{
#ifdef HTGEN
  //sorting halo particles
  //CompPartZ is defined in element.h
  int sum_particle_number_sec = 0; //sum of haloparticles until the examined slice (see secondary loop) - needed for calculating the correct particle index
  size_t nhalo = bunch->nhalo; 
    qsort(bunch->particle_sec,nhalo,sizeof(PARTICLE),cmp_func_t(&particle_cmp));
#endif  
  
  // include PETS transverse end kick (1: means include end fringe fields and nonlin. rf kicks, 0: exclude edge fringe fields and nonlin. rf kicks)
  int EA_edge_focus_active = 1;

#ifdef LONGITUDINAL
  if (bunch->macroparticles==1) {
    //placet_printf(INFO,"EA: DO LONG!\n");
    for (int i=0;i<bunch->slices;i++){
      bunch->z_position[i]+=geometry.length*0.5e-6
	*(bunch->particle[i].yp*bunch->particle[i].yp
	  +bunch->particle[i].xp*bunch->particle[i].xp);
      bunch->z_position[i]+=geometry.length*0.5*1e6
	*(EMASS*EMASS)/(bunch->particle[i].energy
			*bunch->particle[i].energy);
    }
  }
#endif
  
  
  if (bunch->macroparticles==1) {
    sort_beam(bunch);
    wake_table.t_old=t;
  }

  
  /*
    Prepare the rotation matrix for non-uniformity effects
  */
  double s_cav;
  double c_cav;
  if((wake->rf_kick)||(wake->rf_long)){
    sincos(v1,&s_cav,&c_cav);
  } else {
    s_cav=0.0;
    c_cav=1.0;
  }
  c_cav*=wake->rf_a0_i;
  s_cav*=wake->rf_a0_i;
  
  wake_table.init(wake, geometry.length, bunch, wake->n_mode_long, wake->n_mode, CAVSTEP );
  
  int n_macro=bunch->macroparticles;
  double length=geometry.length/(double)CAVSTEP;
  double half_length=0.5*length;
  double length_i=1.0/length;
  
  // factor_long: prop.to. amp. of long. wake functions
  for (int i_m=0;i_m<wake->n_mode_long;++i_m) {
    if( ref_energy < 0 && 0 ) {
      // temp trigger: e0 < 0 implies PETS inhibited lonitudinal mode !  (e0 is not used for any other purposes for the PETS)
      bunch->drive_data->factor_long[i_m*bunch->bunches]=-TWOPI*1e-9*(bunch->drive_data->param.drive->charge*ECHARGE)*C_LIGHT/wake->lambda_longitudinal[i_m]*wake->r_over_q[i_m] * 1e-20;
      placet_printf(INFO,"EA: PETS inhibited (0 long. mode, double Q trans. mode)\n");
    } else {
      bunch->drive_data->factor_long[i_m*bunch->bunches]=-TWOPI*1e-9*(bunch->drive_data->param.drive->charge*ECHARGE)*C_LIGHT/wake->lambda_longitudinal[i_m]*wake->r_over_q[i_m];
    }
    bunch->drive_data->factor_long[i_m*bunch->bunches]/=(1.0-wake->beta_group_l[i_m]);
    
    // damping factor from one bunch to the next [for subsequent bunches]
    for (int i=1;i<bunch->bunches;i++){
      bunch->drive_data->factor_long[i+i_m*bunch->bunches]=
	exp(-(bunch->drive_data->bunch_z[i]-bunch->drive_data->bunch_z[i-1])
	    *1e-6/
	    wake->lambda_longitudinal[i_m]*PI/wake->q_value_long[i_m]
	    *1.0/(1.0-wake->beta_group_l[i_m]));
    }
  }
  
  // along: prop to amp of trans. wake along bunch train, for each mode consequetivly
  // amp. of trans. wake [first bunch in each train]
  for (int i_m=0;i_m<wake->n_mode;++i_m) {
    bunch->drive_data->along[i_m*bunch->bunches]=wake->a0[i_m]
      *(bunch->drive_data->param.drive->charge*ECHARGE*1e12)*1e-6*1e-3;
    
    // damping factor from one bunch to the next [for subsequent bunches]
    for (int i=1;i<bunch->bunches;i++){
      if( ref_energy < 0 && 0  ) {
     // temp trigger: e0 < 0 implies PETS inhibited lonitudinal mode !  (e0 is not used for any other purposes for the PETS)
	bunch->drive_data->along[i+i_m*bunch->bunches]=
	  exp(-(bunch->drive_data->bunch_z[i]-bunch->drive_data->bunch_z[i-1])
	      *1e-6/
	    wake->lambda_transverse[i_m]*PI/wake->q_value[i_m]*2.0
	      *1.0/(1.0-wake->beta_group_t[i_m]));
      } else {
	bunch->drive_data->along[i+i_m*bunch->bunches]=
	  exp(-(bunch->drive_data->bunch_z[i]-bunch->drive_data->bunch_z[i-1])
	      *1e-6/
	      wake->lambda_transverse[i_m]*PI/wake->q_value[i_m]
	      *1.0/(1.0-wake->beta_group_t[i_m]));
      }
    }
  }

  
  // START PRIMARY LOOP - BUNCHES
  // approximation: for long range wake, bunches are treated as point-like objects
  for (int i_b=0;i_b<bunch->bunches;i_b++) {
    // build up table of long range wakes
    // INPUT:  rho_S_long, 2D array: individual bunches already alculated  x  Modes
    // OUTPUT: S_long: sum of long range wakes from bunches already calculated (array of Modes)
    for (int ix=0;ix<wake->n_mode_long;++ix) {
      // uncomment to remove damping from longitudinal mode
      //       distribute_cav( wake_table.rho_S_long+ix*(CAVSTEP+1)*bunch->bunches,
      // 		      wake_table.rho_C_long+ix*(CAVSTEP+1)*bunch->bunches,
      // 		      wake_table.S_long+ix*(CAVSTEP+1),
      // 		      wake_table.C_long+ix*(CAVSTEP+1),
      // 		      CAVSTEP,
      // 		      wake_table.f_long[ix],  // drain out term
      // 		      bunch->drive_data->bunch_z,  // bunch position ( e.g. 25mm -> NB: particle beam?)
      // 		      i_b);  // bunch nr
      distribute_cav_Q(wake_table.rho_S_long+ix*(CAVSTEP+1)*bunch->bunches,
		       wake_table.rho_C_long+ix*(CAVSTEP+1)*bunch->bunches,
		       wake_table.S_long+ix*(CAVSTEP+1),
		       wake_table.C_long+ix*(CAVSTEP+1),
		       CAVSTEP,
		       wake_table.f_long[ix],
                       bunch->drive_data->bunch_z,
		       bunch->drive_data->factor_long+ix*bunch->bunches,
		       i_b);
    }
    
    //EA SINGLE MODE ONLY (only first mode used)
    //scd temp - RF seeding voltage of the cavity
    for (int nhelp=0;nhelp<CAVSTEP;nhelp++){
      wake_table.C_long[nhelp]+=rf_seed/bunch->drive_data->factor_long[0];
    }
    //scd end
    
    for (int ix=0;ix<wake->n_mode;++ix) {
      // trans. wake y
      distribute_cav_Q(wake_table.rho_S_transv+ix*(CAVSTEP+1)*bunch->bunches,
		       wake_table.rho_C_transv+ix*(CAVSTEP+1)*bunch->bunches,
		       wake_table.S_transv+ix*(CAVSTEP+1),
		       wake_table.C_transv+ix*(CAVSTEP+1),
		       CAVSTEP,
		       wake_table.f_transv[ix],
                       bunch->drive_data->bunch_z,
		       bunch->drive_data->along+ix*bunch->bunches,
		       i_b);
      // trans. wake x
      // needs to be changed (EA: this was an old comment?)
      distribute_cav_Q(wake_table.rho_S_transv_x+ix*(CAVSTEP+1)*bunch->bunches,
		       wake_table.rho_C_transv_x+ix*(CAVSTEP+1)*bunch->bunches,
		       wake_table.S_transv_x+ix*(CAVSTEP+1),
                       wake_table.C_transv_x+ix*(CAVSTEP+1),
		       CAVSTEP,
                       wake_table.f_transv[ix],
                       bunch->drive_data->bunch_z,
		       bunch->drive_data->along+ix*bunch->bunches,
		       i_b);
    }
    for (int nhelp=0;nhelp<wake->n_mode_long*(CAVSTEP+1);nhelp++){
      wake_table.S_b_long[nhelp]=0.0;
      wake_table.C_b_long[nhelp]=0.0;
    }
    for (int nhelp=0;nhelp<wake->n_mode*(CAVSTEP+1);nhelp++){
      wake_table.S_b_transv[nhelp]=0.0;
      wake_table.C_b_transv[nhelp]=0.0;
      wake_table.S_b_transv_x[nhelp]=0.0;
      wake_table.C_b_transv_x[nhelp]=0.0;
    }
    
    // START SECONDARY LOOP - SLICES
    // approximation: drain-out time is ignored within bunch
    
    for (int i_s=0;i_s<bunch->slices_per_bunch;i_s++) {
      int i_z=i_b*bunch->slices_per_bunch+i_s;

      // sincos( x, sin(x), cos(x) )
      // calculate sine and cos term for current slice
      for (int i_m=0;i_m<wake->n_mode_long;++i_m){
	sincos(bunch->z_position[i_z]*wake->k_longitudinal[i_m], wake_table.sl+2*i_m, wake_table.sl+2*i_m+1);
      }
      
      // calculate sine and cos trans term for current slice
      for (int i_m=0;i_m<wake->n_mode;++i_m){
	sincos(bunch->z_position[i_z]*wake->k_transverse[i_m], wake_table.st+2*i_m, wake_table.st+2*i_m+1);
      }
    
      // START THIRD LOOP (CAVSTEPS)
      for (int i_step=0;i_step<CAVSTEP;i_step++) {
	double tmp=0.0;
	double tmpy=0.0;
	double tmpx=0.0;
	
	for (int i_m=0;i_m<bunch->macroparticles;i_m++){
	  tmp+=bunch->particle[i_z*n_macro+i_m].wgt;
	  tmpy+=bunch->particle[i_z*n_macro+i_m].wgt
	    *bunch->particle[i_z*n_macro+i_m].y;
	  tmpx+=bunch->particle[i_z*n_macro+i_m].wgt
	    *bunch->particle[i_z*n_macro+i_m].x;
	}

	/*
	  change for variable z-position
	*/
	double S_s_long=0;
	double C_s_long=0;
	double de=0;
	
 	/* energy loss */  // ( bunch energy loss is the same for all steps in a cavity )
	// S_s_long: self-field,  S_b_long: intra-bunch field (sum from all macros in previous slices),  S_long: long-range field
	for (int i_mode=0;i_mode<wake->n_mode_long;++i_mode) {
	  S_s_long=tmp*wake_table.sl[2*i_mode];
	  C_s_long=tmp*wake_table.sl[2*i_mode+1];
	  
	  de+=((wake_table.S_long[i_step+i_mode*(CAVSTEP+1)]+wake_table.S_b_long[i_step+i_mode*(CAVSTEP+1)]+0.5*S_s_long)*wake_table.sl[2*i_mode]+
	       (wake_table.C_long[i_step+i_mode*(CAVSTEP+1)]+wake_table.C_b_long[i_step+i_mode*(CAVSTEP+1)]+0.5*C_s_long)*wake_table.sl[2*i_mode+1])
	    *bunch->drive_data->factor_long[i_mode*bunch->bunches];

	  // add individiual particles wake to intra-bunch wake (used for next slice only)
	  wake_table.S_b_long[i_step+i_mode*(CAVSTEP+1)]+=S_s_long;
	  wake_table.C_b_long[i_step+i_mode*(CAVSTEP+1)]+=C_s_long;
	}
	
	double kicky=0.0;
	double kickx=0.0;
	
	// transverse wake effects
        for (int i_mode=0;i_mode<wake->n_mode;++i_mode) {
	  wake_table.S_b_transv[i_step+i_mode*(CAVSTEP+1)]+=tmpy*wake_table.st[2*i_mode+1];
	  wake_table.C_b_transv[i_step+i_mode*(CAVSTEP+1)]-=tmpy*wake_table.st[2*i_mode];
	  wake_table.S_b_transv_x[i_step+i_mode*(CAVSTEP+1)]+=tmpx*wake_table.st[2*i_mode+1];
	  wake_table.C_b_transv_x[i_step+i_mode*(CAVSTEP+1)]-=tmpx*wake_table.st[2*i_mode];
	  /* transverse kick */
	  
	  kicky+=((wake_table.S_transv[i_step+i_mode*(CAVSTEP+1)]
		   +wake_table.S_b_transv[i_step+i_mode*(CAVSTEP+1)])*wake_table.st[2*i_mode]
		  +(wake_table.C_transv[i_step+i_mode*(CAVSTEP+1)]
		    +wake_table.C_b_transv[i_step+i_mode*(CAVSTEP+1)])*wake_table.st[2*i_mode+1])
	    *length*bunch->drive_data->along[i_mode*bunch->bunches];
	  // to be modified
	  kickx+=((wake_table.S_transv_x[i_step+i_mode*(CAVSTEP+1)]
		   +wake_table.S_b_transv_x[i_step+i_mode*(CAVSTEP+1)])*wake_table.st[2*i_mode]
		  +(wake_table.C_transv_x[i_step+i_mode*(CAVSTEP+1)]
		    +wake_table.C_b_transv_x[i_step+i_mode*(CAVSTEP+1)])*wake_table.st[2*i_mode+1])
	    *length*bunch->drive_data->along[i_mode*bunch->bunches];
	}
	
	
	/* factor for nonlinear RF-kick */
	//EA SINGLE MODE ONLY (only first mode used)
	double rf_ampl=0;
	//	double rf_ampl2=0;
	if(wake->rf_kick){
	  rf_ampl=-(wake_table.S_long[i_step]+wake_table.S_b_long[i_step])
	    *bunch->drive_data->bl[i_z]+
	    (wake_table.C_long[i_step]+wake_table.C_b_long[i_step])
	    *bunch->drive_data->al[i_z];
	  //rf_ampl2=rf_ampl*fabs(v1)*bunch->drive_data->factor_kick[0];
	  
	  rf_ampl*=bunch->drive_data->factor_kick[0];
	}

	
	// START QUATRO LOOP - MACROPARTILCES
	for (int i_m=0;i_m<bunch->macroparticles;i_m++) {
	  int i_part=i_z*n_macro+i_m;
	  
	  // IGNORE LOST PARTICLES AND "NON-BUCKET" PARTICLES
	  if (fabs(bunch->particle[i_part].energy)>std::numeric_limits<double>::epsilon() && bunch->particle[i_part].wgt > 2*std::numeric_limits<double>::epsilon() )  {
	    //placet_printf(INFO,"EA: ignoring lost particle in PETS!\n");
	    
	    double de0=de;
	    //placet_printf(INFO,"EA: calc'ed energy loss per macroparticle: %g\n", de0);
	  

	    /* rf long */
	    if(wake->rf_long){
	      double enhance=1.0;
	      for(int i_rf=0;i_rf<wake->n_rf;i_rf++){
		rf_long2p(wake->rf_order*(i_rf+1),
			  wake->rf_size[i_rf],
			  half_length,v1,bunch->particle[i_part].x,
			  bunch->particle[i_part].xp,
			  bunch->particle[i_part].y,
			  bunch->particle[i_part].yp,
			  c_cav,s_cav,
			&tmpx);
		enhance+=tmpx;
	      }
	      //	  placet_printf(INFO,"%g %g %g %g\n",rf_x,tmpx,c_cav,bunch->particle[i_part].x);
	      de0*=enhance;
	    }
	    
	    
	    //
	    // apply first half of PETS wake
	    //
	    double delta=half_length*de0/bunch->particle[i_part].energy;
	    //placet_printf(INFO,"EA: calc'ed half delta energy: %g\n", delta);
	    
	    
#ifdef END_FIELDS
	    double gammap=delta*length_i;
	    if( EA_edge_focus_active ) {
	      //placet_printf(INFO,"EA: 1 -=  apply kick %g\n", gammap*bunch->particle[i_part].y);
	      // defocusing ENTRANCE EDGE KICK: yp = -(g/(2E))*L*y),  g < 0
	      bunch->particle[i_part].yp-=gammap*bunch->particle[i_part].y;
	    }
#endif
	    double tmp2;
	    if (delta>0.01) {
	      // log1p(x) def log_e(1+x)
	      tmp2=log1p(delta)/delta;
	    }
	    else {
	      // more exact than log1p when delta << 1 (supposedly)
	      tmp2=1.0-delta*(0.5+delta*0.3308345316809);
	    }
	    tmp=1.0/(1.0+delta);
	    
	    // ADIABATIC UNDAMPING
	    bunch->particle[i_part].y+=half_length*bunch->particle[i_part].yp*tmp2;
	    // placet_printf(INFO,"EA: 3 *= apply kick %g\n", tmp);
	    // ADIABATIC UNDAMPING (defocusing effect: yp = yp*1/(1+delta)), delta < 0)
	    bunch->particle[i_part].yp*=tmp;
	    
#ifdef END_FIELDS
	    if( EA_edge_focus_active ) {
	    bunch->particle[i_part].xp-=gammap*bunch->particle[i_part].x;
	    }
#endif
	    bunch->particle[i_part].x+=half_length*bunch->particle[i_part].xp*tmp2;
	    bunch->particle[i_part].xp*=tmp;
	    
	    // deacceleration
	    bunch->particle[i_part].energy+=half_length*de0;
	    
	    /* include the rotation due to the endfields */
#ifdef END_FIELDS
	    
	    double lndelta;
	    if (delta>0.01) {
	      lndelta=0.5*log1p(2.0*delta);
	    }
	    else {
	      lndelta=delta*(1.0-delta*(1.0+delta*1.3233381267236));
	    }
	    //	lndelta=0.5*log(1.0+2.0*delta);
	    //#ifdef SECOND_ORDER
	    R_MATRIX r1;
	    r1.r11=1.0-lndelta;
	    r1.r12=length*lndelta/delta;
	    r1.r21=-delta/(length*(1.0+2.0*delta))*lndelta;
	    r1.r22=(1.0+lndelta)/(1.0+2.0*delta);
	    R_MATRIX r2;
	    r2.r11=r1.r11;
	    r2.r12=r1.r21;
	    r2.r21=r1.r12;
	    r2.r22=r1.r22;
	    //#endif
#else
	    
	    if (delta>0.01) {
	      lndelta=0.5*log1p(2.0*delta);
	    }
	    else {
	      lndelta=delta*(1.0-delta*(1.0+delta*1.3233381267236));
	    }
	    //#ifdef SECOND_ORDER
	    r1.r12=length*lndelta/delta;
	    r2.r21=r1.r12;
	    //#endif

	    //#ifdef SECOND_ORDER
	    r1.r22=1.0/(1.0+2.0*delta);
	    r2.r22=r1.r22;
	    //#endif
#endif

#ifdef SECOND_ORDER
	    bunch->sigma[i_part] = r1 * bunch->sigma[i_part] * r2;
	    bunch->sigma_xx[i_part] = r1 * bunch->sigma_xx[i_part] * r2;
	    bunch->sigma_xy[i_part] = r1 * bunch->sigma_xy[i_part] * r2;
#endif
	  
	    //kicks and suchlike
	  

	    /* kick from nonlinearity */
	    if(wake->rf_kick){
	      double rf_x=0.0;
	      double rf_y=0.0;
	      for(int i_rf=0;i_rf<wake->n_rf;i_rf++){	
		rf_kick2p(wake->rf_order*(i_rf+1),
			wake->rf_size[i_rf]*rf_ampl
			  /bunch->particle[i_part].energy,
			  half_length,v1,bunch->particle[i_part].x,
			  bunch->particle[i_part].xp,
			  bunch->particle[i_part].y,
			  bunch->particle[i_part].yp,
			  c_cav,s_cav,
			  &tmpx,&tmpy);
		rf_x+=tmpx;
		rf_y+=tmpy;
	      }
	      bunch->particle[i_part].x+=0.5*half_length*rf_x;
	      if( EA_edge_focus_active ) {
		bunch->particle[i_part].xp+=rf_x;
	      }
	      bunch->particle[i_part].y+=0.5*half_length*rf_y;
	      if( EA_edge_focus_active ) {
		//placet_printf(INFO,"EA: 4 += apply kick %g\n", rf_y);
	      bunch->particle[i_part].yp+=rf_y;
	      }
	    }
	    
	    /* wakefield kick */
	  
	    // EA: this is the (only) kick due to the transverse wakes
	    bunch->particle[i_part].yp+=kicky/bunch->particle[i_part].energy;
	    bunch->particle[i_part].xp+=kickx/bunch->particle[i_part].energy;
	    
	  
	  
	    //
	    // apply first half of PETS wake
	    //
	    delta=half_length*de0/bunch->particle[i_part].energy;
#ifdef END_FIELDS
	    gammap=delta/length;
#endif
	    
	    if (delta>0.01) {
	      tmp2=log1p(delta)/delta;
	    }
	    else {
	      tmp2=1.0-delta*(0.5+delta*0.3308345316809);
	    }
	    tmp=1.0/(1.0+delta);
	    
	    bunch->particle[i_part].y+=half_length*bunch->particle[i_part].yp*tmp2;
	    
#ifdef END_FIELDS
	    if( EA_edge_focus_active ) {
	      //placet_printf(INFO,"EA: 9 += apply kick %g\n", gammap*bunch->particle[i_part].y);
	      // focusing EXIT EDGE KICK: yp = (g/(2E))*L*y),  g < 0
	      bunch->particle[i_part].yp+=gammap*bunch->particle[i_part].y;
	    }
#endif
	    // placet_printf(INFO,"EA: 10 *= apply kick %g\n", tmp);
	    // ADIABATIC UNDAMPING (defocusing effect: yp = yp*1/(1+delta)), delta < 0)
	    bunch->particle[i_part].yp*=tmp;
	    bunch->particle[i_part].x+=half_length*bunch->particle[i_part].xp*tmp2;
#ifdef END_FIELDS
	    if( EA_edge_focus_active ) {
	      bunch->particle[i_part].xp+=gammap*bunch->particle[i_part].x;
	    }
#endif
	    bunch->particle[i_part].xp*=tmp;
	    bunch->particle[i_part].energy+=half_length*de0;
	  }
	  // END LOST PARTICLE CHECK
	}          
	// END QUATRO LOOP (MACROPARTICLES)
	
	
#ifdef HTGEN		
	// START QUATRO LOOP - HALOPARTICLES
	for (int i_m=0;i_m<bunch->particle_number_sec[i_z];i_m++) {
	  int i_part_sec=sum_particle_number_sec+i_m; 
	  // IGNORE LOST PARTICLES AND "NON-BUCKET" PARTICLES
	  if (fabs(bunch->particle_sec[i_part_sec].energy)>std::numeric_limits<double>::epsilon() && bunch->particle_sec[i_part_sec].wgt > 2*std::numeric_limits<double>::epsilon() )  {
	  double de0=de;
	  //
	  // apply first half of PETS wake
	  //
	  double delta=half_length*de0/bunch->particle_sec[i_part_sec].energy;
	  
#ifdef END_FIELDS
	  double gammap=delta*length_i;
	  if( EA_edge_focus_active ) {
	    // defocusing ENTRANCE EDGE KICK: yp = -(g/(2E))*L*y),  g < 0
	    bunch->particle_sec[i_part_sec].yp-=gammap*bunch->particle_sec[i_part_sec].y;
	  }
#endif
	  double tmp2;
          if (delta>0.01) {
	    // log1p(x) def log_e(1+x)
	    tmp2=log1p(delta)/delta;
	  }
	  else {
	    // more exact than log1p when delta << 1 (supposedly)
	    tmp2=1.0-delta*(0.5+delta*0.3308345316809);
	  }
	  tmp=1.0/(1.0+delta);
	  
	  // ADIABATIC UNDAMPING   
	  bunch->particle_sec[i_part_sec].y+=half_length*bunch->particle_sec[i_part_sec].yp*tmp2;
	  // ADIABATIC UNDAMPING (defocusing effect: yp = yp*1/(1+delta)), delta < 0)
	  bunch->particle_sec[i_part_sec].yp*=tmp;
	  
#ifdef END_FIELDS
	  if( EA_edge_focus_active ) {
	    bunch->particle_sec[i_part_sec].xp-=gammap*bunch->particle_sec[i_part_sec].x;
	  }
#endif
	  bunch->particle_sec[i_part_sec].x+=half_length*bunch->particle_sec[i_part_sec].xp*tmp2;
	  bunch->particle_sec[i_part_sec].xp*=tmp;

          // deacceleration
	  bunch->particle_sec[i_part_sec].energy+=half_length*de0;
	  
	  /* include the rotation due to the endfields */
#ifdef END_FIELDS
	  
          double lndelta;
	  if (delta>0.01) {
	    lndelta=0.5*log1p(2.0*delta);
	  }
	  else {
	    lndelta=delta*(1.0-delta*(1.0+delta*1.3233381267236));
	  }
	  //	lndelta=0.5*log(1.0+2.0*delta);
	  //#ifdef SECOND_ORDER
          R_MATRIX r1;
	  r1.r11=1.0-lndelta;
	  r1.r12=length*lndelta/delta;
	  r1.r21=-delta/(length*(1.0+2.0*delta))*lndelta;
	  r1.r22=(1.0+lndelta)/(1.0+2.0*delta);
          R_MATRIX r2;
	  r2.r11=r1.r11;
	  r2.r12=r1.r21;
	  r2.r21=r1.r12;
	  r2.r22=r1.r22;
	  //#endif
#else
	  
	  if (delta>0.01) {
	    lndelta=0.5*log1p(2.0*delta);
	  }
	  else {
	    lndelta=delta*(1.0-delta*(1.0+delta*1.3233381267236));
	  }
	  //#ifdef SECOND_ORDER
	  r1.r12=length*lndelta/delta;
	  r2.r21=r1.r12;
	  //#endif
	  
	  //#ifdef SECOND_ORDER
	  r1.r22=1.0/(1.0+2.0*delta);
	  r2.r22=r1.r22;
	  //#endif
#endif
	  
	  /* wakefield kick */
	  
	  // EA: this is the (only) kick due to the transverse wakes
	  bunch->particle_sec[i_part_sec].yp+=kicky/bunch->particle_sec[i_part_sec].energy;
	  bunch->particle_sec[i_part_sec].xp+=kickx/bunch->particle_sec[i_part_sec].energy;
	  
	  //
	  // apply first half of PETS wake
	  //
	  delta=half_length*de0/bunch->particle_sec[i_part_sec].energy;
#ifdef END_FIELDS
	  gammap=delta/length;
#endif
	  
	  if (delta>0.01) {
	    tmp2=log1p(delta)/delta;
	  }
	  else {
	    tmp2=1.0-delta*(0.5+delta*0.3308345316809);
	  }
	  tmp=1.0/(1.0+delta);
	  
	  bunch->particle_sec[i_part_sec].y+=half_length*bunch->particle_sec[i_part_sec].yp*tmp2;
	  
#ifdef END_FIELDS
	  if( EA_edge_focus_active ) {
	    // focusing EXIT EDGE KICK: yp = (g/(2E))*L*y),  g < 0
	    bunch->particle_sec[i_part_sec].yp+=gammap*bunch->particle_sec[i_part_sec].y;
	  }
#endif
	  // ADIABATIC UNDAMPING (defocusing effect: yp = yp*1/(1+delta)), delta < 0)
	  bunch->particle_sec[i_part_sec].yp*=tmp;
	  bunch->particle_sec[i_part_sec].x+=half_length*bunch->particle_sec[i_part_sec].xp*tmp2;
#ifdef END_FIELDS
	  if( EA_edge_focus_active ) {
	    bunch->particle_sec[i_part_sec].xp+=gammap*bunch->particle_sec[i_part_sec].x;
	  }
#endif
	  bunch->particle_sec[i_part_sec].xp*=tmp;
	  bunch->particle_sec[i_part_sec].energy+=half_length*de0;
	  }
	  // END LOST PARTICLE CHECK
	}          
	// END QUATRO LOOP (HALOPARTICLES)
#endif	
	
      }
      // END THIRD LOOP (CAVSTEPS)

      double tmpy=0.0;
      double tmpx=0.0;
      for (int i_m=0;i_m<bunch->macroparticles;i_m++) {
	tmpy+=bunch->particle[i_z*n_macro+i_m].wgt
	  *bunch->particle[i_z*n_macro+i_m].y;
	tmpx+=bunch->particle[i_z*n_macro+i_m].wgt
	  *bunch->particle[i_z*n_macro+i_m].x;
      }
      for (int i_mode=0;i_mode<wake->n_mode;++i_mode) {
	wake_table.S_b_transv[CAVSTEP+i_mode*(CAVSTEP+1)]+=tmpy*wake_table.st[2*i_mode+1];
	wake_table.C_b_transv[CAVSTEP+i_mode*(CAVSTEP+1)]-=tmpy*wake_table.st[2*i_mode];
	wake_table.S_b_transv_x[CAVSTEP+i_mode*(CAVSTEP+1)]+=tmpx*wake_table.st[2*i_mode+1];
	wake_table.C_b_transv_x[CAVSTEP+i_mode*(CAVSTEP+1)]-=tmpx*wake_table.st[2*i_mode];
      }
#ifdef HTGEN
      //update sum of haloparticles
      sum_particle_number_sec += bunch->particle_number_sec[i_z];
#endif 
    }
    // END SECONDARY LOOP (SLICES)
    

    // update calc' basis for long range wake with the new bunch information ( S_b_long[0] now contains the total contribution of bunch / # of steps)
    for (int ix=0;ix<wake->n_mode_long;++ix) {
      for (int nhelp=0;nhelp<CAVSTEP+1;nhelp++) {
	/* R/Q is constant along the structure so wake_table.S_b_long does not change */
	wake_table.rho_S_long[ix*(CAVSTEP+1)*bunch->bunches+i_b*(CAVSTEP+1)+nhelp]=wake_table.S_b_long[ix*(CAVSTEP+1)];
	wake_table.rho_C_long[ix*(CAVSTEP+1)*bunch->bunches+i_b*(CAVSTEP+1)+nhelp]=wake_table.C_b_long[ix*(CAVSTEP+1)];
      }
    }

    for (int ix=0;ix<wake->n_mode;++ix) {
      for (int nhelp=0;nhelp<CAVSTEP+1;nhelp++) {
	/* R/Q is constant along the structure */
	wake_table.rho_S_transv[ix*(CAVSTEP+1)*bunch->bunches+i_b*(CAVSTEP+1)+nhelp]=
	  wake_table.S_b_transv[ix*(CAVSTEP+1)+nhelp];
	wake_table.rho_C_transv[ix*(CAVSTEP+1)*bunch->bunches+i_b*(CAVSTEP+1)+nhelp]=
	  wake_table.C_b_transv[ix*(CAVSTEP+1)+nhelp];
	wake_table.rho_S_transv_x[ix*(CAVSTEP+1)*bunch->bunches+i_b*(CAVSTEP+1)+nhelp]=
	  wake_table.S_b_transv_x[ix*(CAVSTEP+1)+nhelp];
	wake_table.rho_C_transv_x[ix*(CAVSTEP+1)*bunch->bunches+i_b*(CAVSTEP+1)+nhelp]=
	  wake_table.C_b_transv_x[ix*(CAVSTEP+1)+nhelp];
      }
    }
  }
  // END PRIMARY LOOP (BUNCHES)
}

