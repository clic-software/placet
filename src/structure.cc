#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "structure.h"

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "random.hh"
#include "cavity.h"

/*extern struct{
  double beta_group_l,beta_group_t,a0,l_cav,lambda_transverse,
    lambda,gradient,q_value,shift,band;
  int steps;
  WAKE_DATA *wake;
} injector_data;
*/

void
survey_banana(BEAMLINE *beamline,double bowx,double bowy,double offsetx,double offsety, int noreset)
{
  int i;
  for(i=0;i<beamline->n_elements;i++){
    if (CAVITY *cavity=beamline->element[i]->cavity_ptr()) {
      cavity->structure_banana(Cavity.Gauss()*bowx,Cavity.Gauss()*bowy,Cavity.Gauss()*offsetx,Cavity.Gauss()*offsety,noreset);
    }
  }
}

int tk_SurveyBanana(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error,noreset=0;
  double bowx=0.0,bowy=0.0,offsetx=0.0,offsety=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-x_bow",TK_ARGV_FLOAT,(char*)NULL,(char*)&bowx,
     (char*)"bending in the horizontal plane"},
    {(char*)"-y_bow",TK_ARGV_FLOAT,(char*)NULL,(char*)&bowy,
     (char*)"bending in the vertical plane"},
    {(char*)"-x_offset",TK_ARGV_FLOAT,(char*)NULL,(char*)&offsetx,
     (char*)"bending in the horizontal plane"},
    {(char*)"-y_offset",TK_ARGV_FLOAT,(char*)NULL,(char*)&offsety,
     (char*)"bending in the vertical plane"},
    {(char*)"-noreset",TK_ARGV_INT,(char*)NULL,(char*)&noreset,
     (char*)"if set to 1, it does not zeroth the position of the cells"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <SurveyBanana>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  survey_banana(inter_data.beamline,bowx,bowy,offsetx,offsety,noreset);
  return TCL_OK;
}


void
structure_scatter_cells(CAVITY *cavity,double /*rmsx*/,double rmsy,double /*offsetx*/,double offsety)
{
  int i,n;
  double y,ysum=0.0;

  n=injector_data.steps;
  if ((int)cavity->size_yz()!=n) {
    cavity->add_yz(n);
  }
  for (i=0;i<n;i++){
    y=rmsy*Cavity.Gauss();
    cavity->add_yz(i, y);
    ysum+=y;
  }
  ysum/=n;
  ysum-=offsety;
  for (i=0;i<n;i++){
    cavity->add_yz(i, -ysum);
  }
}

void survey_scatter_cells(BEAMLINE *beamline,double rmsx,double rmsy,double offsetx,double offsety )
{
  for(int i=0;i<beamline->n_elements;i++){
    if (CAVITY *cavity=beamline->element[i]->cavity_ptr()) {
      structure_scatter_cells(cavity,Cavity.Gauss()*rmsx,Cavity.Gauss()*rmsy,Cavity.Gauss()*offsetx,Cavity.Gauss()*offsety);
    }
  }
}

int tk_SurveyScatterCells(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			  char *argv[])
{
  int error;
  double bowx=0.0,bowy=0.0,offsetx=0.0,offsety=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-x_rms",TK_ARGV_FLOAT,(char*)NULL,(char*)&bowx,
     (char*)"scattering in the horizontal plane"},
    {(char*)"-y_rms",TK_ARGV_FLOAT,(char*)NULL,(char*)&bowy,
     (char*)"scattering in the vertical plane"},
    {(char*)"-x_offset",TK_ARGV_FLOAT,(char*)NULL,(char*)&offsetx,
     (char*)"scattering in the horizontal plane"},
    {(char*)"-y_offset",TK_ARGV_FLOAT,(char*)NULL,(char*)&offsety,
     (char*)"scattering in the vertical plane"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <SurveyScatterCells>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  survey_scatter_cells(inter_data.beamline,bowx,bowy,offsetx,offsety);
  return TCL_OK;
}
