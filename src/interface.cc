#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beam.h"

/* Function prototypes */

#include "rmatrix.h"
#include "random.hh"

void data_MAD_track(BEAM *beam,char *name,int n_particles,int axis,
		    int bunch_number,double energy0,int which)
{
  FILE *file;
  int i,j,j_slice,n,nb,m = 0, np = 0;
  int slices,j_slice_first,j_slice_last;
  double y = 0.0, yp = 0.0, wgt = 0.0, weight, x = 0.0,xp = 0.0;
  double x_slice, xp_slice, y_slice, yp_slice;
  double z_rndm, x_rndm, xp_rndm, y_rndm, yp_rndm;
  double energy_average = 0.0, z_pos, energy, energy_permille;
  double *wgt_comul;
  double energy_other_slice, energy_slice, weight_other_slice, weight_slice;
  double x_gauss_x, x_gauss_y, y_gauss_x, y_gauss_y;  
  double radius_x, radius_y, theta_x, theta_y;
  double det_sigma_xx, det_sigma, emittance_x, emittance_y;
  double alpha_x = 0.0,beta_x = 0.0,alpha_y = 0.0,beta_y = 0.0;

  slices = beam->slices;

  /* Allocate dynamic space for the array wgt_comul containing the accumulated weights */

  wgt_comul = new double[slices];


  nb = beam->bunches;
  n = slices/nb;
  j_slice_first = 0;
  j_slice_last = slices - 1;
  if (bunch_number > 0) {
    j_slice_first = (bunch_number - 1)*n;
    j_slice_last = j_slice_first + n - 1;}
  placet_printf(INFO,"slices,n,nb,j_slice_first,j_slice_last = %d, %d, %d %d, %d\n",slices,n,nb,
          j_slice_first,j_slice_last);
  /* open file */

  file=open_file(name);

  /* get average slice position, slope, energy, alpha and beta. Compute comulative distribution */

  m=0;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
#ifdef TWODIM
      weight = beam->particle[m].wgt;
      x += beam->particle[m].x*weight;
      xp += beam->particle[m].xp*weight;
#endif
      y += beam->particle[m].y*weight;
      yp += beam->particle[m].yp*weight;
      wgt += weight;
      wgt_comul[m] = wgt; 
      energy_average += beam->particle[m].energy*weight;
      det_sigma_xx = beam->sigma_xx[m].r11*beam->sigma_xx[m].r22 -
                     beam->sigma_xx[m].r12*beam->sigma_xx[m].r12;
      det_sigma = beam->sigma[m].r11*beam->sigma[m].r22 -
                  beam->sigma[m].r12*beam->sigma[m].r12;
      emittance_x = sqrt(det_sigma_xx);
      emittance_y = sqrt(det_sigma);
      alpha_x = alpha_x - beam->sigma_xx[m].r12/emittance_x;
      beta_x = beta_x + beam->sigma_xx[m].r11/emittance_x;
      alpha_y = alpha_y - beam->sigma[m].r12/emittance_y;
      beta_y = beta_y + beam->sigma[m].r11/emittance_y;      
      m++;
    }
  }

  /* normalise */

  x /= wgt;
  xp /= wgt;
  y /= wgt;
  yp /= wgt;
  energy_average /= (double)wgt;
  alpha_x /= slices;
  beta_x /= slices;
  alpha_y /= slices;
  beta_y /= slices;
  placet_printf(INFO,"alpha_x = %g,  beta_x = %g\n alpha_y = %g,  beta_y = %g\n",
	 alpha_x, beta_x, alpha_y, beta_y);
 
  /* set average energy to energy0 if energy0 > 0 */

  if (energy0 > 0)
    energy_average = energy0;

  /* normalise wgt_comul */

  for (j=0; j<m; j++){
    wgt_comul[j] /= wgt;
  }

  /* loop on the required n_particles */

  while (np < n_particles){

  /* get a random number between 0 and 1 (uniform distribution) */

    z_rndm = Select.Uniform();


  /* find the slice with the comulative weight just above z_rnd */

    j = 0;
    while (z_rndm > wgt_comul[j]) j++;
    j_slice = j;

  /* check that the slice is in the desired range */

    if (j_slice >= j_slice_first && j_slice <= j_slice_last) {

  /* compute z position */

      z_pos = beam->z_position[j_slice];

  /* interpolate energy */

      energy_slice = beam->particle[j_slice].energy;
      weight_slice = wgt_comul[j_slice];

      if (j_slice > 0) {
	energy_other_slice = beam->particle[j_slice - 1].energy; 
	weight_other_slice = wgt_comul[j_slice - 1];
      }
      else {
	energy_other_slice = beam->particle[j_slice + 1].energy; 
	weight_other_slice = wgt_comul[j_slice + 1];
      }
	energy = (energy_slice*(z_rndm - weight_other_slice) -
                  energy_other_slice*(z_rndm - weight_slice))/
	         (weight_slice - weight_other_slice);

      energy_permille = (energy - energy_average)*1000.0/energy_average;

  /* fetch the centre coordinates of the x & y phases ellipses for the selected slice */

#ifdef TWODIM
      x_slice = beam->particle[j_slice].x;
      xp_slice = beam->particle[j_slice].xp;
#endif
      y_slice = beam->particle[j_slice].y;
      yp_slice = beam->particle[j_slice].yp;

  /* substract mean values if axis == 0 */

      if (axis == 0) {
	x_slice -= x;
	xp_slice -= xp;
	y_slice -= y;
	yp_slice -= yp;
      }
  /* get the random point inside the normalised circle in phase space.
     Coordinates are polar ( gaussian distribution between 0 and sigma for the radius 
     and uniform distribution between 0 and 2 pi for the azimuth). */

      x_gauss_x = Radiation.Gauss();
      x_gauss_y = Radiation.Gauss();
      y_gauss_x = Radiation.Gauss();
      y_gauss_y = Radiation.Gauss();      
      radius_x = sqrt(x_gauss_x*x_gauss_x + x_gauss_y*x_gauss_y);
      radius_y = sqrt(y_gauss_x*y_gauss_x + y_gauss_y*y_gauss_y);
      theta_x = atan2(x_gauss_y,x_gauss_x);
      theta_y = atan2(y_gauss_y,y_gauss_x);

  /* get the emittances */

      det_sigma_xx = beam->sigma_xx[j_slice].r11*beam->sigma_xx[j_slice].r22 -
                 beam->sigma_xx[j_slice].r12*beam->sigma_xx[j_slice].r12;
      det_sigma = beam->sigma[j_slice].r11*beam->sigma[j_slice].r22 -
              beam->sigma[j_slice].r12*beam->sigma[j_slice].r12;
      emittance_x = sqrt(det_sigma_xx);
      emittance_y = sqrt(det_sigma);
      alpha_x = -beam->sigma_xx[j_slice].r12/emittance_x;
      beta_x = beam->sigma_xx[j_slice].r11/emittance_x;
      alpha_y = -beam->sigma[j_slice].r12/emittance_y;
      beta_y = beam->sigma[j_slice].r11/emittance_y;
      //      gamma = energy/5.11006e-4;


  /* transform to the true phase space */

      x_rndm = radius_x*cos(theta_x)*beam->sigma_xx[j_slice].r11/sqrt(emittance_x);
      xp_rndm = radius_x*(-cos(theta_x)*beam->sigma_xx[j_slice].r12 + 
                      sin(theta_x)*emittance_x)/sqrt(emittance_x);
      y_rndm = radius_y*cos(theta_y)*beam->sigma[j_slice].r11/sqrt(emittance_y);
      yp_rndm = radius_y*(-cos(theta_y)*beam->sigma[j_slice].r12 + 
                      sin(theta_y)*emittance_y)/sqrt(emittance_y);

  /* add the offsets */

      x_rndm += x_slice;
      xp_rndm += xp_slice;
      y_rndm += y_slice;
      yp_rndm += yp_slice;

  /* save on file */

      switch (which) {
      case 1:
	fprintf(file,"%g %g %g %g %g %g \n",
		x_rndm*.001, xp_rndm*.001, y_rndm*.001, yp_rndm*.001, energy_permille, z_pos*.001);
	break;
      case 2:
	fprintf(file,"%g %g %g %g %g %g \n",
		energy,x_rndm*sqrt(emittance_x),y_rndm*sqrt(emittance_y),
		z_pos,xp_rndm*sqrt(emittance_x),yp_rndm*sqrt(emittance_y));
	break;
      }
      np++;
    }
  }

  /* close the file and free the dynamic space for wgt_comul */

  close_file(file);

  /* free space used by the array wgt_comul */

  delete [] wgt_comul;
}
