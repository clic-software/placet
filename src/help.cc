#include <algorithm>
#include <sstream>
#include <fstream>
#include <cstdio>
#include <cctype>
#include <string>
#include <unistd.h>

#include "help.h"

#include "placet_cout.hh"
#include "placet_prefix.hh"

Tcl_Interp *COMMAND_HELPER::interp;

static inline void replace_all(std::string &str, const std::string &sub, const std::string &other )
{
  size_t idx=0;
  while((idx = str.find(sub, idx)) !=  std::string::npos) {
    str.replace( idx, sub.size(), other );
    idx+=other.size();
  }
}

static bool looks_like_texinfo (const std::string& msg, size_t &p1)
{
  p1 = msg.find ('\n');
  std::string t = msg.substr (0, p1);
  if (p1 == std::string::npos)
    p1 = 0;
  size_t p2 = t.find ("-*- texinfo -*-");
  if (p2 == std::string::npos)
    p2 = t.find ("-*-texinfo-*-");
  return p2 != std::string::npos;
}

void display_help_text (std::ostream& os, const std::string& msg)
{
  // Look for "-*- texinfo -*-" in first line of help message.  If it
  // is present, use makeinfo to format the rest of the message before
  // sending it to the output stream.  Otherwise, just print the
  // message.
  size_t pos;
  if (looks_like_texinfo(msg,pos)) {
    os.flush();
    char tmp_file_name[] = "placet-texi.XXXXXX";
    int fd = mkstemp(tmp_file_name);
    if (fd != -1) {
      close(fd);
      int cols = 72;
      std::ostringstream buf;
      // Use double quotes to quote the sed patterns for Windows.
      buf << "sed -e \"s/^[#%][#%]* *//\" -e \"s/^ *@/@/\" | "
          << "\"makeinfo\""
          << " --fill-column " << cols
          << " --no-warn"
          << " --no-validate"
          << " --no-headers"
          << " --force"
          << " --output \"" << tmp_file_name << "\"";
      if (FILE *file = popen(buf.str().c_str(), "w")) {
        fputs(msg.substr(pos+1).c_str(), file);
        pclose(file);
        std::ifstream tmp_file(tmp_file_name);
        if (tmp_file) {
          if (&os == &std::cout) {
            tmp_file.close();
            buf.str("");
            buf << "/bin/more " << tmp_file_name;
            system(buf.str().c_str());
          } else {
            int c;
            while ((c=tmp_file.get ())!=EOF)
              os << (char) c;
            tmp_file.close();
          }
        } else {
	  placet_cout << ERROR << "help: Texinfo formatting filter exited abnormally\n";
	  placet_cout << ERROR << "help: raw Texinfo source of help text follows...";
	  placet_cout << ERROR << "help:\n\n%s\n\n" << msg.c_str () << endmsg;
        }
      }
      /*
      oprocstream filter (buf.str ());
      if (filter && filter.is_open ()) {
        filter << "@macro seealso {args}\n"
	       << "@sp 1\n"
	       << "@noindent\n"
	       << "See also: \\args\\.\n"
               << "@end macro\n";
        filter << msg.substr (pos+1) << std::endl;
        int status = filter.close ();
        std::ifstream tmp_file (tmp_file_name.c_str ());
        if (WIFEXITED (status) && WEXITSTATUS (status) == 0) {
          int c;
          while ((c = tmp_file.get ()) != EOF)
	    os << (char) c;
          tmp_file.close();
        } else {
	  warning ("help: Texinfo formatting filter exited abnormally");
	  warning ("help: raw Texinfo source of help text follows...");
	  warning ("help:\n\n%s\n\n", msg.c_str ());
        }
        file_ops::unlink (tmp_file_name);
      } else {
        os << msg;
      }
      */
      remove(tmp_file_name);
    } else {
      os << msg;
    }
  } else {
    os << msg;
  }
}

std::string COMMAND_HELPER::get_help_texinfo() const
{
  std::string command = name;
  command += " -help";
  Tcl_EvalEx(interp,const_cast<char*>(command.c_str()),-1,TCL_EVAL_DIRECT);
  std::string help_message = Tcl_GetStringResult(interp);
/*  int opipe[2];
  pipe(opipe);
  dup2(opipe[1],1);
  if (FILE *file=fdopen(opipe[0], "r")) {
    Tcl_EvalEx(interp,command.c_str(),-1,TCL_EVAL_DIRECT);
    help_message = Tcl_GetStringResult(interp);
    if (help_message=="") {
      char line[1024];
      while(fgets(line, 1024, file)!=NULL) {
        help_message += std::string(line);
      }
    }
    dup2(1,opipe[1]);
  }
  ::close(opipe[0]);
  */
  size_t pos = help_message.find("Generic options for all commands:");
  if (pos != std::string::npos) {
    help_message.erase(pos, strlen("Generic options for all commands:"));
  }
  std::ostringstream texi;
  pos = help_message.find("Command-specific options:");
  if (pos != std::string::npos) {
    pos += strlen("Command-specific options:");
    texi  
//          << "\\input texinfo   @c -*- texinfo -*-\n"
//          << "@node " << name << "\n"
//          << "@top PLACET Documentation\n"
          << "@deffn {Command} " << name << "\n\n";
          
    if (texinfo_description != "") {
      texi << texinfo_description << "\n\n";
    }  
    
    texi  << "Valid options for @code{" << name << "} are listed in the following table.\n\n"
          << "@table @code\n";
    for (size_t opt_pos1 = help_message.find("\n -", pos);;) {
      if (opt_pos1 == std::string::npos) 
        break;
      opt_pos1 += 2;
      int opt_pos2 = help_message.find(":",opt_pos1); 
      std::string option_name = help_message.substr(opt_pos1,opt_pos2-opt_pos1);
      opt_pos1 = opt_pos2;
      texi << "@item " << option_name << '\n';
      opt_pos1 = help_message.find("\n -",opt_pos1+option_name.size());
      std::string description = help_message.substr(opt_pos2+1,opt_pos1-opt_pos2-1);
      replace_all(description, "{", "@{");
      replace_all(description, "}", "@}");
      texi << description << std::endl;        
    }
    texi << "@end table\n";
    texi << "@end deffn\n";
  } else {
    // command without Tk Help message
  }
  return texi.str();
}

void COMMAND_HELPER::print_help_message() const
{
  placet_cout << ALWAYS << get_help_texinfo() << endmsg;
}
void COMMAND_HELPER::display_help_text() const
{
  ::display_help_text(std::cout, get_help_texinfo());
}

COMMAND_HELPER::COMMAND_HELPER(Tcl_Interp *_interp, const char *_name, MANUAL::CATEGORY _category, MANUAL::SUBCATEGORY _subcategory, const std::string &texi_desc ) : name(_name), category(_category), subcategory(_subcategory), texinfo_description(texi_desc)
{
  interp = _interp;
  if (texinfo_description == "") {
    std::string filename = _name;
    std::transform(filename.begin(), filename.end(), filename.begin(), (int(*)(int)) std::tolower);
    filename = get_placet_sharepath("doc/texi/" + filename + ".texi");
    std::ifstream file(filename.c_str());
    if (file) {
      std::ostringstream file_content;
      file_content << file.rdbuf();
      texinfo_description = file_content.str();    
    }
  }
}

COMMAND_HELPER::COMMAND_HELPER(Tcl_Interp *_interp, const char *_name, const std::string &texi_desc ) : name(_name), category(MANUAL::INVISIBLE), subcategory(MANUAL::NONE), texinfo_description(texi_desc)
{
  interp = _interp;
  if (texinfo_description == "") {
    std::string filename = _name;
    std::transform(filename.begin(), filename.end(), filename.begin(), (int(*)(int)) std::tolower);
    filename = get_placet_sharepath("doc/texi/" + filename + ".texi");
    std::ifstream file(filename.c_str());
    if (file) {
      std::ostringstream file_content;
      file_content << file.rdbuf();
      texinfo_description = file_content.str();
    }
  }
}
