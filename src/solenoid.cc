#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "placeti3.h"
#include "matrix.h"
#include "rmatrix.h"
#include "solenoid.h"

/* the routine calculates the effects of the end field of a solenoid on the
   particle */

void SOLENOID::particle_step_end(PARTICLE *particle,double bz)const
{
  const double k = 0.5 * bz / particle->energy * 0.299792458; // 1/m, T/(GV/c) = 0.299792458 / m
  particle->xp -= k*particle->y;
  particle->yp += k*particle->x;
}

/* the routine steps the particle PARTICLE a distance length in a solenoidal
   field bz assuming no acceleration */

void SOLENOID::particle_step(PARTICLE *particle,double length,double bz)const
{
  const double xp = particle->xp;
  const double yp = particle->yp;
  const double k_bar = bz / particle->energy * 0.299792458; // T/(GV/c) = 0.299792458 / m

  /* if phase advance is not significant do normal structure */
  if (k_bar==0.0){
    particle->x+=xp*length;
    particle->y+=yp*length;
    return;
  }

  if (length!=0.0) {
    const double phi = k_bar * length; // rad
    const double s = sin(phi);
    const double c = cos(phi);
    const double lambda_bar = 1.0/k_bar;
    particle->x+=lambda_bar*(s*xp-(1.0-c)*yp);
    particle->y+=lambda_bar*(s*yp+(1.0-c)*xp);
    particle->xp=c*xp-s*yp;
    particle->yp=c*yp+s*xp;
  } else if (thin_length != 0.0) {
    const double phi = k_bar * thin_length; // rad
    const double s = sin(phi);
    const double c = cos(phi);
    particle->xp=c*xp-s*yp;
    particle->yp=c*yp+s*xp;
  }
}

void SOLENOID::sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
			  double energy,double length,double bz) const
{
  const double k_bar= bz / energy * 0.299792458; // 1/m, T/(GV/c) = 0.299792458 / m
  const double phi = length * k_bar; // rad
  /* if phase advance is not significant do normal structure */
  if (phi==0.0){
    drift_sigma_matrix(*sigma,length);
    drift_sigma_matrix(*sigma_xx,length);
    drift_sigma_matrix(*sigma_xy,length);
    return;
  }

  R_MATRIX r1,r2,r3;
  const double s = sin(phi);
  const double c = cos(phi);
  const double lambda_bar = 1.0/k_bar;
  r1.r11=1.0;
  r1.r12=s*lambda_bar;
  r1.r21=0.0;
  r1.r22=c;

  r2.r11=0.0;
  r2.r12=-lambda_bar*(1.0-c);
  r2.r21=0.0;
  r2.r22=-s;

  r3.r11=0.0;
  r3.r12=lambda_bar*(1.0-c);
  r3.r21=0.0;
  r3.r22=s;

  M_mult_4(&r1,&r2,&r3,&r1,sigma,sigma_xy,sigma_xx);
  return;
}

void SOLENOID::sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
			      double energy,double bz) const
{
  const double k = 0.5 * bz / energy * 0.299792458; // T/(GV/c) = 0.299792458 / m

  R_MATRIX r1,r2,r3;
  r1.r11=1.0;
  r1.r12=0.0;
  r1.r21=0.0;
  r1.r22=1.0;

  r2.r11=0.0;
  r2.r12=0.0;
  r2.r21=-k;
  r2.r22=0.0;

  r3.r11=0.0;
  r3.r12=0.0;
  r3.r21=k;
  r3.r22=0.0;
  M_mult_4(&r1,&r2,&r3,&r1,sigma,sigma_xy,sigma_xx);
  return;
}

void SOLENOID::step_4d(BEAM *beam)
{
  for(int i=0;i<beam->slices;i++){
    if (!beam->particle[i].is_lost()) {
      particle_step_end(beam->particle+i,strength);
      particle_step(beam->particle+i,geometry.length,strength);
      particle_step_end(beam->particle+i,-strength);
      sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		     beam->particle[i].energy,strength);
      sigma_step(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		 beam->particle[i].energy,geometry.length,strength);
      sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		     beam->particle[i].energy,-strength);
    }
  }
}

void SOLENOID::step_4d_0(BEAM *beam)
{
  int i;
  for(i=0;i<beam->slices;i++){
    if (!beam->particle[i].is_lost()) {
      particle_step_end(beam->particle+i,strength);
      particle_step(beam->particle+i,geometry.length,strength);
      particle_step_end(beam->particle+i,-strength);
    }
  }
}

void SOLENOID::step_twiss(BEAM *beam,FILE *file,double step,
			  int j,double s,int n1,int n2,
			  void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  int nstep=int(geometry.length/step)+1;
  double _length=geometry.length/nstep;
  if( callback0 ) callback0(file,beam,j,s,n1,n2);
  for (int istep=0;istep<nstep;istep++){
    s+=_length;
    for (int i=0;i<beam->slices;i++){
      if (!beam->particle[i].is_lost()) {
	sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		       beam->particle[i].energy,strength);
	sigma_step(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		   beam->particle[i].energy,_length,strength);
	sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		       beam->particle[i].energy,-strength);
      }
    }
    if( callback0 ) callback0(file,beam,j,s,n1,n2);
  }
}

void SOLENOID::step_twiss_0(BEAM *beam,FILE *file,double step,
			    int j,double s,int n1,int n2,
			    void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  int nstep=int(geometry.length/step)+1;
  double _length=geometry.length/nstep;
  if( callback0 ) callback0(file,beam,j,s,n1,n2);
  for (int istep=0;istep<nstep;istep++){
    s+=_length;
    for (int i=0;i<beam->slices;i++){
      if (!beam->particle[i].is_lost()) {
	particle_step_end(beam->particle+i,strength);
	particle_step(beam->particle+i,_length,strength);
	particle_step_end(beam->particle+i,-strength);
      }
    }
    if( callback0 ) callback0(file,beam,j,s,n1,n2);
  }
}

Matrix<6,6> SOLENOID::get_transfer_matrix_6d(double _energy ) const 
{
  Matrix<6,6> R(0.0);
  if (fabs(strength)>std::numeric_limits<double>::epsilon()) {
    if (_energy == -1.0)
      _energy = ref_energy;
    double k = strength / 2 / _energy * 0.299792458;
    double s,c;
    sincos(k*geometry.length,&s,&c);
    R[0][0]=R[1][1]=R[2][2]=R[3][3]=c*c;
    R[0][1]=R[2][3]=s*c/k;
    R[0][2]=R[1][3]=s*c;
    R[0][3]=s*s/k;
    R[1][0]=-k*s*c;
    R[1][2]=-k*s*s;
    R[2][0]=-s*c;
    R[2][1]=-s*s/k;
    R[3][0]=k*s*s;
    R[3][1]=-s*c;
    R[3][2]=-k*s*c;
  } else {
    R[0][0]=1.0;
    R[0][1]=geometry.length;
    R[1][1]=1.0;
    R[2][2]=1.0;
    R[2][3]=geometry.length;
    R[3][3]=1.0;
  }
  R[4][4]=1.0;
  R[5][5]=1.0;
  return R;
}
