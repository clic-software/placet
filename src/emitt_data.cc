#include <algorithm>
#include <fstream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cfloat>
#include <limits>

#include "structures_def.h"
#include "quadrupole.h"
#include "placeti3.h"
#include "rmatrix.h"

#include "emitt_data.h"

SWITCHES_STRUCT switches;
EMITT_DATA emitt_data;

void switches_init()
{
  switches.envelope_cut=3.0;
  switches.emitt_axis=0;
  switches.e_start=0;
  switches.e_range=0;
}

namespace {
double max_r(const BEAM *bunch)
{
  double ymax=0.0;
  double a=switches.envelope_cut;
  if (bunch->particle_beam) {
    for (int i=0;i<bunch->slices;i++){
      if( bunch->particle[i].wgt > 2*std::numeric_limits<double>::epsilon()) {
	double tmp=bunch->particle[i].y;
	double tmpx=bunch->particle[i].x;
	tmp=tmp*tmp+tmpx*tmpx;
	if (tmp>ymax)
	  ymax=tmp;
      }
    }
  } else {
    if (placet_switch.first_order)
      a=0.0;
    for (int i=0;i<bunch->slices;i++){
      double tmp=fabs(bunch->particle[i].y)+a*sqrt(bunch->sigma[i].r11);
      double tmpx=fabs(bunch->particle[i].x)+a*sqrt(bunch->sigma_xx[i].r11);
      tmp=tmp*tmp+tmpx*tmpx;
      if (tmp>ymax)
	ymax=tmp;
    }
  }
  return sqrt(ymax);
}

double max_r_el(const BEAM *bunch,ELEMENT* el)
{
  double ymax=0.0;
  double a=switches.envelope_cut;
  //  int i_max = 0;
  if (placet_switch.first_order||bunch->particle_beam) {
    for (int i=0;i<bunch->slices;i++){
      if( bunch->particle[i].wgt > 2*std::numeric_limits<double>::epsilon()) {
	double tmp=fabs(bunch->particle[i].y-el->offset.y);
	double tmpx=fabs(bunch->particle[i].x-el->offset.x);
	tmp=tmp*tmp+tmpx*tmpx;
	if (tmp>ymax) {
	  ymax=tmp;
	  //i_max = i;
	}
      }
    }
  } else {
    for (int i=0;i<bunch->slices;i++){
      if( bunch->particle[i].wgt > 2*std::numeric_limits<double>::epsilon()) {
	double tmp=fabs(bunch->particle[i].y-el->offset.y)
	  +a*sqrt(bunch->sigma[i].r11);
	double tmpx=fabs(bunch->particle[i].x-el->offset.x)
	  +a*sqrt(bunch->sigma_xx[i].r11);
	tmp=tmp*tmp+tmpx*tmpx;
	if (tmp>ymax) {
	  ymax=tmp;
	  //	    i_max = i;
	}
      }
    }
  }
  //placet_printf(INFO,"max value at slice: %d\n", i_max);
  //placet_printf(INFO,"max value was: %g\n", sqrt(ymax));
  return sqrt(ymax);
}
}

EMITT_DATA::EMITT_DATA():y_off(0.0),x_off(0.0){}

void EMITT_DATA::append(const BEAM *beam)
{
  EMITT_VALUE tmp;
  tmp.value_x = emitt_x(beam);
  tmp.value_y = emitt_y(beam);
  values.push_back(tmp);
}

void EMITT_DATA::store_init(int n )
{
  data.clear();
  x_off=0.0;
  y_off=0.0;
  values.clear();
}

void EMITT_DATA::store(int i, const BEAM *bunch, double s )
{
  if (i>=data.size())
    data.resize(i+1);
  data[i] += EMITT_DATA_LINE(bunch, s);
  x_off=0.0;
  y_off=0.0;
}

void EMITT_DATA::store_el(int i, const BEAM *bunch, ELEMENT *el )
{
  if (i>=data.size())
    data.resize(i+1);
  data[i] += EMITT_DATA_LINE(bunch, el);
  x_off=0.0;
  y_off=0.0;
}

void EMITT_DATA::store_delete()
{
  data.clear();
  values.clear();
}

void EMITT_DATA_LINE::init(const BEAM *bunch )
{
  if (switches.e_range) {
    double tmp;
    ex = switches.emitt_axis ? 
      emitt_x_axis_range(bunch,switches.e_start, switches.e_start+switches.e_range,&tmp) : 
      emitt_x_range(bunch,switches.e_start, switches.e_start+switches.e_range,&tmp);
    ey = switches.emitt_axis ? 
      emitt_y_axis_range(bunch,switches.e_start, switches.e_start+switches.e_range,&tmp) : 
      emitt_y_range(bunch,switches.e_start, switches.e_start+switches.e_range,&tmp);
  } else {
    ex = switches.emitt_axis ? emitt_x_axis(bunch) : emitt_x(bunch);
    ey = switches.emitt_axis ? emitt_y_axis(bunch) : emitt_y(bunch);
  }
  ex2 = ex * ex;
  ey2 = ey * ey;
  Env = element ? max_r_el(bunch,element) : max_r(bunch);
  std::vector<double> t = bunch_get_moments(bunch);
  x   = t[0];
  y   = t[1];
  xp  = t[2];
  yp  = t[3];
  x2  = t[4];
  y2  = t[5];
  xp2 = t[6];
  yp2 = t[7];
  xxp = t[8];
  yyp = t[9];
  sx = sqrt(x2 - x*x);
  sy = sqrt(y2 - y*y);
  sz = bunch_get_length(bunch);
  sxp = sqrt(xp2 - xp*xp);
  syp = sqrt(yp2 - yp*yp);
  std::pair<double,double> u = bunch_get_energy(bunch);
  energy = u.first;
  espread = u.second;
}

/// New classes
EMITT_DATA_LINE &EMITT_DATA_LINE::operator += (const EMITT_DATA_LINE &b )
{
  ex += b.ex;
  ey += b.ey;
  ex2 += b.ex2;
  ey2 += b.ey2;
  x += b.x;
  y += b.y;
  xp += b.xp;
  yp += b.yp;
  x2 += b.x2;
  y2 += b.y2;
  xp2 += b.xp2;
  yp2 += b.yp2;
  xxp += b.xxp;
  yyp += b.yyp;
  Env = std::max(Env, b.Env);
  sx += b.sx;
  sy += b.sy;
  sz += b.sz;
  sxp += b.sxp;
  syp += b.syp;
  energy += b.energy;
  espread += b.espread;
  n += b.n;
  s = b.s;
  element = b.element;
  return *this;
}

std::vector<double> EMITT_DATA_LINE::get_values_as_vector(const char *format ) const
{
  std::vector<double> retval;
  for(const char *ptr0 = format; const char *ptr = strchr(ptr0, '%'); ptr0 = ptr) {
    /// print any constants in 'format', if present
    {
      char *endptr;
      std::string nptr(ptr0, size_t(ptr-ptr0));
      double value = strtod(nptr.c_str(), &endptr);
      if (value!=0.0 || endptr!=nptr.c_str()) {
	retval.push_back(value);
      }
    }
    double value = 0.0;
    if      (strncmp(ptr+1, "Env", 3)==0) { value = Env; ptr+=4; }
    else if (strncmp(ptr+1, "sex", 3)==0) { value = n==0? 0.0: sqrt(std::max(0.0,n*ex2-ex*ex))/n; ptr+=4; }
    else if (strncmp(ptr+1, "sey", 3)==0) { value = n==0? 0.0: sqrt(std::max(0.0,n*ey2-ey*ey))/n; ptr+=4; }
    else if (strncmp(ptr+1, "xp2", 3)==0) { value = n==0? 0.0: xp2/n; ptr+=4; }
    else if (strncmp(ptr+1, "yp2", 3)==0) { value = n==0? 0.0: yp2/n; ptr+=4; }
    else if (strncmp(ptr+1, "xxp", 3)==0) { value = n==0? 0.0: xxp/n; ptr+=4; }
    else if (strncmp(ptr+1, "yyp", 3)==0) { value = n==0? 0.0: yyp/n; ptr+=4; }
    else if (strncmp(ptr+1, "sxp", 3)==0) { value = n==0? 0.0: sxp/n; ptr+=4; }
    else if (strncmp(ptr+1, "syp", 3)==0) { value = n==0? 0.0: syp/n; ptr+=4; }
    else if (strncmp(ptr+1, "x2",  2)==0) { value = n==0? 0.0: x2/n; ptr+=3; }
    else if (strncmp(ptr+1, "y2",  2)==0) { value = n==0? 0.0: y2/n; ptr+=3; }
    else if (strncmp(ptr+1, "sx",  2)==0) { value = n==0? 0.0: sx/n; ptr+=3; }
    else if (strncmp(ptr+1, "sy",  2)==0) { value = n==0? 0.0: sy/n; ptr+=3; }
    else if (strncmp(ptr+1, "sz",  2)==0) { value = n==0? 0.0: sz/n; ptr+=3; }
    else if (strncmp(ptr+1, "ex",  2)==0) { value = n==0? 0.0: ex/n; ptr+=3; }
    else if (strncmp(ptr+1, "ey",  2)==0) { value = n==0? 0.0: ey/n; ptr+=3; }
    else if (strncmp(ptr+1, "xp",  2)==0) { value = n==0? 0.0: xp/n; ptr+=3; }
    else if (strncmp(ptr+1, "yp",  2)==0) { value = n==0? 0.0: yp/n; ptr+=3; }
    else if (strncmp(ptr+1, "dE",  2)==0) { value = n==0? 0.0: espread/n; ptr+=3; }
    else if (ptr[1] == 'E') { value = n==0 ? 0.0: energy/n; ptr+=2; }
    else if (ptr[1] == 'x') { value = n==0 ? 0.0: x/n; ptr+=2; }
    else if (ptr[1] == 'y') { value = n==0 ? 0.0: y/n; ptr+=2; }
    else if (ptr[1] == 's') { value = s; ptr+=2; }
    else if (ptr[1] == 'n') { value = n; ptr+=2; }
    else {
      ptr++;
      continue;
    }
    retval.push_back(value);
  }
  return retval;
}

void EMITT_DATA_LINE::print(std::ostream &stream, const char *format ) const
{
  for(const char *ptr0 = format; const char *ptr = strchr(ptr0, '%'); ptr0 = ptr) {
    stream.write(ptr0, size_t(ptr-ptr0));
    if      (strncmp(ptr+1, "name", 4)==0) { stream << (element ? (std::string("'") + element->get_name() + std::string("'")) : "''"); ptr+=5; }
    else if (strncmp(ptr+1, "Env", 3)==0) { stream << Env; ptr+=4; }
    else if (strncmp(ptr+1, "sex", 3)==0) { stream << (n==0? 0.0: sqrt(std::max(0.0,n*ex2-ex*ex))/n); ptr+=4; }
    else if (strncmp(ptr+1, "sey", 3)==0) { stream << (n==0? 0.0: sqrt(std::max(0.0,n*ey2-ey*ey))/n); ptr+=4; }
    else if (strncmp(ptr+1, "xp2", 3)==0) { stream << (n==0? 0.0: xp2/n); ptr+=4; }
    else if (strncmp(ptr+1, "yp2", 3)==0) { stream << (n==0? 0.0: yp2/n); ptr+=4; }
    else if (strncmp(ptr+1, "xxp", 3)==0) { stream << (n==0? 0.0: xxp/n); ptr+=4; }
    else if (strncmp(ptr+1, "yyp", 3)==0) { stream << (n==0? 0.0: yyp/n); ptr+=4; }
    else if (strncmp(ptr+1, "sxp", 3)==0) { stream << (n==0? 0.0: sxp/n); ptr+=4; }
    else if (strncmp(ptr+1, "syp", 3)==0) { stream << (n==0? 0.0: syp/n); ptr+=4; }
    else if (strncmp(ptr+1, "x2",  2)==0) { stream << (n==0? 0.0: x2/n); ptr+=3; }
    else if (strncmp(ptr+1, "y2",  2)==0) { stream << (n==0? 0.0: y2/n); ptr+=3; }
    else if (strncmp(ptr+1, "sx",  2)==0) { stream << (n==0? 0.0: sx/n); ptr+=3; }
    else if (strncmp(ptr+1, "sy",  2)==0) { stream << (n==0? 0.0: sy/n); ptr+=3; }
    else if (strncmp(ptr+1, "sz",  2)==0) { stream << (n==0? 0.0: sz/n); ptr+=3; }
    else if (strncmp(ptr+1, "ex",  2)==0) { stream << (n==0? 0.0: ex/n); ptr+=3; }
    else if (strncmp(ptr+1, "ey",  2)==0) { stream << (n==0? 0.0: ey/n); ptr+=3; }
    else if (strncmp(ptr+1, "xp",  2)==0) { stream << (n==0? 0.0: xp/n); ptr+=3; }
    else if (strncmp(ptr+1, "yp",  2)==0) { stream << (n==0? 0.0: yp/n); ptr+=3; }
    else if (strncmp(ptr+1, "dE",  2)==0) { stream << (n==0? 0.0: espread/n); ptr+=3; }
    else if (ptr[1] == 'E') { stream << (n==0? 0.0: energy/n); ptr+=2; }
    else if (ptr[1] == 'x') { stream << (n==0? 0.0: x/n); ptr+=2; }
    else if (ptr[1] == 'y') { stream << (n==0? 0.0: y/n); ptr+=2; }
    else if (ptr[1] == 's') { stream << s; ptr+=2; }
    else if (ptr[1] == 'n') { stream << n; ptr+=2; }
    else {
      ptr++;
      continue;
    }
  }
}

/****************************************************
 * function: write_header           	            *
 * Juergen Pfingstner, 18. Dec. 2008		    *
 * Andrea Latina, 11 June 2013                      *
 *						    *
 * Content: Write a header in the emitt             *
 *          output file to make it more readable.   *
 ****************************************************/

void EMITT_DATA::write_header(std::ostream &stream, const char *format )
{
  stream << "# Collects the emittance information of the beam in the following columns (format string was '" << format << "' )\n";
  size_t column = 1;
  for(const char *ptr0 = format; const char *ptr = strchr(ptr0, '%'); ptr0 = ptr) {
    /// print any constants in 'format', if present
    {
      char *endptr;
      std::string nptr(ptr0, size_t(ptr-ptr0));
      double value = strtod(nptr.c_str(), &endptr);
      if (value!=0.0 || endptr!=nptr.c_str()) {
	stream << "# " << column++ << ".) The number '" << value << "'" << std::endl;
      }
    }
    if      (strncmp(ptr+1, "name", 4)==0) { stream << "# " << column++ << ".) Name of the element" << std::endl; ptr+=5; }
    else if (strncmp(ptr+1, "Env", 3)==0) { stream << "# " << column++ << ".) Beam envelope defined as sqrt( max(x_i^2 + y_i^2) ) [um]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "sex", 3)==0) { stream << "# " << column++ << ".) sigma_epsilon_x [10^-7 m]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "sey", 3)==0) { stream << "# " << column++ << ".) sigma_epsilon_y [10^-7 m]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "xp2", 3)==0) { stream << "# " << column++ << ".) average xp*xp [urad^2]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "yp2", 3)==0) { stream << "# " << column++ << ".) average yp*yp [urad^2]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "xxp", 3)==0) { stream << "# " << column++ << ".) average x*xp [um*urad]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "yyp", 3)==0) { stream << "# " << column++ << ".) average y*yp [um*urad]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "sxp", 3)==0) { stream << "# " << column++ << ".) sigma_xp [urad]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "syp", 3)==0) { stream << "# " << column++ << ".) sigma_yp [urad]" << std::endl; ptr+=4; }
    else if (strncmp(ptr+1, "x2",  2)==0) { stream << "# " << column++ << ".) average x*x [um^2]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "y2",  2)==0) { stream << "# " << column++ << ".) average y*y [um^2]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "sx",  2)==0) { stream << "# " << column++ << ".) sigma_x [um]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "sy",  2)==0) { stream << "# " << column++ << ".) sigma_y [um]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "sz",  2)==0) { stream << "# " << column++ << ".) sigma_z [um]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "ex",  2)==0) { stream << "# " << column++ << ".) emittance_x [10^-7 m]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "ey",  2)==0) { stream << "# " << column++ << ".) emittance_y [10^-7 m]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "xp",  2)==0) { stream << "# " << column++ << ".) average xp [urad]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "yp",  2)==0) { stream << "# " << column++ << ".) average yp [urad]" << std::endl; ptr+=3; }
    else if (strncmp(ptr+1, "dE",  2)==0) { stream << "# " << column++ << ".) energy spread [GeV]" << std::endl; ptr+=3; }
    else if (ptr[1] == 'E') { stream << "# " << column++ << ".) Average energy [GeV]" << std::endl; ptr+=2; }
    else if (ptr[1] == 'x') { stream << "# " << column++ << ".) Average x [um]" << std::endl; ptr+=2; }
    else if (ptr[1] == 'y') { stream << "# " << column++ << ".) Average y [um]" << std::endl; ptr+=2; }
    else if (ptr[1] == 's') { stream << "# " << column++ << ".) Longitudinal position of the element [m]" << std::endl; ptr+=2; }
    else if (ptr[1] == 'n') { stream << "# " << column++ << ".) Number of machines" << std::endl; ptr+=2; }
    else {
      ptr++;
      continue;
    }
  }
}

void EMITT_DATA::print(const char *name, const char *format )
{
  std::ofstream file(name);
  file.precision(DBL_DIG);
  write_header(file, format);
  for (size_t i=0; i<data.size(); i++) {
    data[i].print(file, format);
    file << std::endl;
  }
  if (values.size()>0) {
    file << "# set emitt_x {";
    for (size_t i=0; i<values.size(); i++)
      file << ' ' << values[i].value_x;
    file << " }" << std::endl;
    file << "# set emitt_y {";
    for (size_t i=0; i<values.size(); i++)
      file << ' ' << values[i].value_y;
    file << " }" << std::endl;
  } else {
    file << "# set emitt_x {}" << std::endl;
    file << "# set emitt_y {}" << std::endl;
  }
}
