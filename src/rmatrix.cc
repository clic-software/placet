#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "rmatrix.h"
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "rmatrix.h"
#include "matrix.h"

RMATRIX::RMATRIX(double length,double *r ) : ELEMENT(length), correct_offset(1)
{
  if (r)
    memcpy(matrix,r,sizeof(double)*16);
}

void RMATRIX::step_4d(BEAM *beam)
{
  R_MATRIX r_xx,r_yy,r_xy,r_yx;

  r_xx.r11=matrix[0];
  r_xx.r12=matrix[1];
  r_xx.r21=matrix[4];
  r_xx.r22=matrix[5];

  r_xy.r11=matrix[2];
  r_xy.r12=matrix[3];
  r_xy.r21=matrix[6];
  r_xy.r22=matrix[7];

  r_yx.r11=matrix[8];
  r_yx.r12=matrix[9];
  r_yx.r21=matrix[12];
  r_yx.r22=matrix[13];

  r_yy.r11=matrix[10];
  r_yy.r12=matrix[11];
  r_yy.r21=matrix[14];
  r_yy.r22=matrix[15];

  ///
  for (int i=0;i<beam->slices;i++) {
#ifdef TWODIM
    double hx=matrix[0]*beam->particle[i].x+
      matrix[1]*beam->particle[i].xp+
      matrix[2]*beam->particle[i].y+
      matrix[3]*beam->particle[i].yp;
    
    double hxp=matrix[4]*beam->particle[i].x+
      matrix[5]*beam->particle[i].xp+
      matrix[6]*beam->particle[i].y+
      matrix[7]*beam->particle[i].yp;
    
    double hy=matrix[8]*beam->particle[i].x+
      matrix[9]*beam->particle[i].xp+
      matrix[10]*beam->particle[i].y+
      matrix[11]*beam->particle[i].yp;
    
    double hyp=matrix[12]*beam->particle[i].x+
      matrix[13]*beam->particle[i].xp+
      matrix[14]*beam->particle[i].y+
      matrix[15]*beam->particle[i].yp;
#else
    double hy=matrix[10]*beam->particle[i].y+
      matrix[11]*beam->particle[i].yp;
    
    double hyp=matrix[14]*beam->particle[i].y+
      matrix[15]*beam->particle[i].yp;
#endif
#ifdef TWODIM
    beam->particle[i].x=hx;
    beam->particle[i].xp=hxp;
#endif
    beam->particle[i].y=hy;
    beam->particle[i].yp=hyp;
    //    beam->sigma[i]
    
#ifdef TWODIM
    M_mult_4(&r_xx,&r_xy,&r_yx,&r_yy,beam->sigma+i,beam->sigma_xy+i,
             beam->sigma_xx+i);
#else
    r_xx.r11=r_yy.r11;
    r_xx.r12=r_yy.r21;
    r_xx.r21=r_yy.r12;
    r_xx.r22=r_yy.r22;
    mult_M_M(&r_yy,beam->sigma+i,beam->sigma+i);
    mult_M_M(beam->sigma+i,&r_xx,beam->sigma+i);
#endif
  }
}

void RMATRIX::step_4d_0(BEAM *beam)
{
  for (int i=0;i<beam->slices;i++) {
#ifdef TWODIM
    double hx=matrix[0]*beam->particle[i].x+
      matrix[1]*beam->particle[i].xp+
      matrix[2]*beam->particle[i].y+
      matrix[3]*beam->particle[i].yp;

    double hxp=matrix[4]*beam->particle[i].x+
      matrix[5]*beam->particle[i].xp+
      matrix[6]*beam->particle[i].y+
      matrix[7]*beam->particle[i].yp;
    
    double hy=matrix[8]*beam->particle[i].x+
      matrix[9]*beam->particle[i].xp+
      matrix[10]*beam->particle[i].y+
      matrix[11]*beam->particle[i].yp;

    double hyp=matrix[12]*beam->particle[i].x+
      matrix[13]*beam->particle[i].xp+
      matrix[14]*beam->particle[i].y+
      matrix[15]*beam->particle[i].yp;
#else
    double hy=matrix[10]*beam->particle[i].y+
      matrix[11]*beam->particle[i].yp;

    double hyp=matrix[14]*beam->particle[i].y+
      matrix[15]*beam->particle[i].yp;
#endif
#ifdef TWODIM
    beam->particle[i].x=hx;
    beam->particle[i].xp=hxp;
#endif
    beam->particle[i].y=hy;
    beam->particle[i].yp=hyp;
  }
}

void RMATRIX::step_twiss(BEAM *beam, FILE * /*file*/, double /*step*/, int /*j*/, double /*s0*/, int /*n1*/,int /*n2*/, void (* /*callback0*/)(FILE*,BEAM*,int,double,int,int))
{
  R_MATRIX r_xx,r_yy,r_xy,r_yx;

  r_xx.r11=matrix[0];
  r_xx.r12=matrix[1];
  r_xx.r21=matrix[4];
  r_xx.r22=matrix[5];


  r_xy.r11=matrix[2];
  r_xy.r12=matrix[3];
  r_xy.r21=matrix[6];
  r_xy.r22=matrix[7];


  r_yx.r11=matrix[8];
  r_yx.r12=matrix[9];
  r_yx.r21=matrix[12];
  r_yx.r22=matrix[13];
 
  r_yy.r11=matrix[10];
  r_yy.r12=matrix[11];
  r_yy.r21=matrix[14];
  r_yy.r22=matrix[15];
 
  for (int i=0;i<beam->slices;i++) {
#ifdef TWODIM
    M_mult_4(&r_xx,&r_xy,&r_yx,&r_yy,beam->sigma+i,beam->sigma_xy+i,
             beam->sigma_xx+i);
#else
    r_xx.r11=r_yy.r11;
    r_xx.r12=r_yy.r21;
    r_xx.r21=r_yy.r12;
    r_xx.r22=r_yy.r22;
    mult_M_M(&r_yy,beam->sigma+i,beam->sigma+i);
    mult_M_M(beam->sigma+i,&r_xx,beam->sigma+i);
#endif
  }
}

void RMATRIX::step_twiss_0(BEAM *beam, FILE *file, double step, int j, double s0,int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  int nstep=int(geometry.length/step)+1;
  double l=geometry.length;
  geometry.length/=nstep;
  if (callback0) callback0(file,beam,j,s0,n1,n2);
  for (int istep=0;istep<nstep;istep++) {
    s0+=geometry.length;
    if (flags.thin_lens) {
      placet_cout << WARNING << " RMATRIX:: step_twiss_0: option thin_lens not implemented " << endmsg;
    } else {
      if (flags.longitudinal) {
        if (flags.synrad) placet_cout << WARNING << " RMATRIX:: step_twiss_0: step_6d_sr_0 not implemented " << endmsg;
        else              placet_cout << WARNING << " RMATRIX:: step_twiss_0: step_6d_0 not implemented " << endmsg;
      } else {
        if (flags.synrad) placet_cout << WARNING << " RMATRIX:: step_twiss_0: step_4d_sr_0 not implemented " << endmsg;
        else              RMATRIX::step_4d_0(beam);
      }
    }
    if (callback0) callback0(file,beam,j,s0,n1,n2);
  } 
  geometry.length=l;
}
