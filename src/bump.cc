#include "track.h"

int tk_TestBumpCorrection(ClientData /*clientdata*/,
			  Tcl_Interp *interp,int argc,
			  char *argv[])
{

  int error;
  int nbin,i,n; //nquad=1,noverlap=0
  void (*survey)(BEAMLINE*);
  char *file_name=NULL,*bumplist=NULL;
  char *format = default_format;
  char *beamname=NULL,*testbeamname=NULL,*survey_name,survey0[]="Clic";
  BEAM *beam=NULL,*testbeam=NULL,*tb,*tb1,*workbunch,*testbeam2;
  int iter=1;
  int *bumps=NULL,*btype=NULL;
  char **v;
  BUMP** bump_bin;
  BIN** bin;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-bumplist",TK_ARGV_STRING,(char*)NULL,
     (char*)&bumplist,
     (char*)"List of bump positions, defaults to NULL (no bumps)"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluation"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"emittance file format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestSimpleCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
    testbeam2=bunch_remake(testbeam);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }

  if (bumplist){
    if(error=Tcl_SplitList(interp,bumplist,&n,&v)){
      return error;
    }
    if (n%2) {
      Tcl_AppendResult(interp,"bumplist is incorrect\n",NULL);
      error=TCL_ERROR;
    }
    n/=2;
    bumps=(int*)malloc(sizeof(int)*(n+1));
    btype=(int*)malloc(sizeof(int)*(n+1));
    for (i=0;i<n;i++){
      if (error=Tcl_GetInt(interp,v[i*2],bumps+i)){
	return error;
      }
      if (error=Tcl_GetInt(interp,v[i*2+1],btype+i)){
	return error;
      }
      if (i>0) {
	if (bumps[i]<=bumps[i-1]){
	  check_error(interp,argv[0],error);
	  Tcl_AppendResult(interp,
			   "-bumplist elements must be in ascending order\n",
			   NULL);
	  error=TCL_ERROR;
	}
      }
    }
    free((char*)v);
    bumps[i]=-1;
  } else {
    Tcl_AppendResult(interp,
		     "-bumplist must be provided\n",
		     NULL);
    return TCL_ERROR;
  }

  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  tb=bunch_remake(beam);
  tb1=bunch_remake(beam);
  workbunch=bunch_remake(beam);
  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  bump_bin=(BUMP**)xmalloc(sizeof(BUMP*)*(n+10));
  beamline_bin_divide(inter_data.beamline,2,0,bin,&nbin);
  beam_copy(beam,tb);
  // scd temp 2004
  simple_bin_fill(inter_data.beamline,bin,nbin,tb,workbunch);
  // scd temp end 2004
  beam_copy(beam,tb);
  beam_copy(beam,tb1);
  beam_copy(beam,workbunch);
  bump_prepare_types(inter_data.beamline,bin,nbin,bump_bin,bumps,btype,beam,
		     tb,tb1,workbunch);

  emitt_data.store_init(inter_data.beamline->n_quad);
  for (i=0;i<iter;i++) {
    if (testbeam) {
      beam_copy(beam,tb);
      beam_copy(beam,tb1);
      survey(inter_data.beamline);
      bump_correct(inter_data.beamline,bin,nbin,bump_bin,bumps,tb,workbunch,
		   tb1,0);
      beam_copy(testbeam,testbeam2);
      bunch_track_emitt_start();
      bunch_track_emitt(inter_data.beamline,testbeam2,0,
			inter_data.beamline->n_elements);
      bunch_track_emitt_end(testbeam2,inter_data.beamline->get_length());
    }
    else {
      beam_copy(beam,tb);
      beam_copy(beam,tb1);
      survey(inter_data.beamline);
      bump_correct(inter_data.beamline,bin,nbin,bump_bin,bumps,tb,workbunch,
		   tb1,1);
    }
  }
  emitt_data.print(file_name, format);
  emitt_data.store_delete();
  if (testbeamname) {
    beam_delete(testbeam2);
  }
  beam_delete(tb);
  beam_delete(tb1);
  beam_delete(workbunch);
  for (i=0;i<n;i++){
    free(bump_bin[i]);
  }
  free(bump_bin);
  for (i=0;i<nbin;i++){
    free(bin[i]);
  }
  free(bin);
  return TCL_OK;
}

