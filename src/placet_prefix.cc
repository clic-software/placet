#include "placet_prefix.hh"
#include "defaults.h"

#include <stdlib.h>     /* std::string */

const char *get_placetdir()
{
    const char *pprefix = getenv("PLACETDIR");
    if (!pprefix)
      pprefix = PLACETDIR;
    return pprefix;
}

std::string get_placet_sharepath(const std::string &extra_path)
{
    std::string return_string = get_placetdir();
    return_string += "/share/placet/" + extra_path;
    return return_string;
};
