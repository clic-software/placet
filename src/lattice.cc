#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "aperture.h"
#include "beamline.h"
#include "bpm.h"
#include "cavbpm.h"
#include "lattice.h"
#include "cavity.h"
#include "cavity_pets.h"
#include "dipole.h"
#include "drift.h"
#include "girder.h"
#include "multipole.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "placeti5.h"
#include "quadbpm.h"
#include "quadrupole.h"
#include "rfmultipole.h"
#include "rmatrix.h"
#include "solenoid.h"
#include "tcl_call.h"

extern INTER_DATA_STRUCT inter_data;
extern RF_DATA rf_data;
extern INJECTOR_DATA injector_data;
extern Tcl_HashTable ModeTable;

/**********************************************************************/
/*                      check_beamline_changeable (not a command)     */
/**********************************************************************/

int check_beamline_changeable(Tcl_Interp *interp,char *cmd,int error)
{
  if (inter_data.beamline_set){
    check_error(interp,cmd,error);
    Tcl_AppendResult(interp,
		     "You already fixed the beamline\n",
		     NULL);
    return TCL_ERROR;
  }
  return TCL_OK;
}

int check_girder_existence(Tcl_Interp *interp,char *cmd,int error)
{
  if (inter_data.girder == NULL){
    check_error(interp,cmd,error);
    Tcl_AppendResult(interp,
		     "First you should define a Girder\n",
		     NULL);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      AccCavity                                     */
/**********************************************************************/

int tk_AccCavity(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  double length=0.0,v1=1.0,v5=0.0;
  int type=0,nm,i;
  bool pi_mode=false;
  char *w_l=NULL,**v,*w_t_long=NULL;
  Tcl_HashEntry *entry;
  WAKE_MODE *wake;
  const char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,
     (char*)"Name to be assigned to the element"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"length of the cavity in meter"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&type,
     (char*)"type of the cavity"},
    {(char*)"-gradient",TK_ARGV_FLOAT,(char*)NULL,(char*)&v1,
     (char*)"The relative gradient of the structure [GV/m]"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,(char*)&v5,
     (char*)"The relative phase of the structure [deg]"},
    {(char*)"-wake",TK_ARGV_STRING,(char*)NULL,(char*)&w_l,
     (char*)"The longrange transverse wakefields of the structure"},
    {(char*)"-wake_long",TK_ARGV_STRING,(char*)NULL,(char*)&w_t_long,
     (char*)"The longrange transverse wakefields of the structure"},
    {(char*)"-pi_mode",TK_ARGV_STRING,(char*)NULL,(char*)&pi_mode,
     (char*)"Enable Rosenzweig-Serafini focusing for a pi-mode cavity"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AccCavity>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error))	return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error))	return TCL_ERROR;

  // rf_data structure inizializalition for Main Beam
  rf_data.cavlength=length;
  rf_data.gradient=v1;
  rf_data.phase_shift=v5;

  CAVITY *cavity=new CAVITY(length,type,v1,v5);
  cavity->set_name(element_name);
  cavity->set_wake_data(injector_data.wake);
  if (cavity->get_lambda()==0.0) {
    cavity->set_lambda(injector_data.lambda[0]);
  }
  inter_data.girder->add_element(cavity);
  if (w_t_long) {
    cavity->set_wake_data(get_wake(w_t_long));
  }
  if (w_l) {
    Tcl_SplitList(interp,w_l,&nm,&v);
    std::vector<int> list(nm+1);
    for (i=0;i<nm;i++){
      entry=Tcl_FindHashEntry(&ModeTable,v[i]);
      if (entry){
	wake=(WAKE_MODE*)Tcl_GetHashValue(entry);
	list[i+1]=wake->number;
      }
      else{
	char buf[50];
	snprintf(buf,50,"Mode %s not defined",v[i]);
	Tcl_SetResult(interp,buf,TCL_VOLATILE);
	return TCL_ERROR;
      }
    }
    list[0]=nm;
    cavity->set_modes(list);
    free(v);
  }
  if (pi_mode) {
    cavity->set_rosenzweig_fucus(true);
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      AddDipoleGridToBeamline                       */
/**********************************************************************/

// adds dipole kicker grid, which can be used to simulate external magnetic fields
// with a given spacing in meters.
// the strength of the dipoles can be set with DipoleNumberList and DipoleSetStrength

int tk_AddDipoleGridToBeamline(ClientData /*clientdata*/,Tcl_Interp *interp,
			       int argc,char *argv[])
{
  int error=0;
  double spacing=1.;
  Tk_ArgvInfo table[]={
    {(char*)"-spacing",TK_ARGV_FLOAT,(char*)NULL,(char*)&spacing,
     (char*)"spacing between the dipoles in [m]"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command adds a grid of dipole kickers to the beamline, it sets the strengths of all dipoles to 0."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  placet_printf(INFO,"%s\n",Tcl_GetStringResult(interp));

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AddDipoleGridToBeamline>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;

  add_dipole_grid(inter_data.beamline,spacing);

  return TCL_OK;
}

void
add_dipole_grid(BEAMLINE* beamline, float spacing)
{
  /* 
     adds dipole kicker grid to lattice.
     it doesn't split element but puts the dipoles in at the right position 
     as much as possible. 
     the method can be used to simulate external magnetic field with a given
     spacing in meters. the strength of the dipoles can to be set with 
     DipoleNumberList and DipoleSetStrength to be used after the lattice has
     been defined and before the beamline has been set.
  */
  
  if (spacing <= 0) {placet_cout << ERROR << "in add_dipole_grid: spacing needs to be larger than 0!" << endmsg;}
  else {placet_cout << VERBOSE << "add dipole grid with spacing: " << spacing << endmsg;}

  int n_dipoles = 0; // the number of dipoles added
  int n_new_elements = 0; // the number of elements in the new beamline

  double position_beamline = 0.; // the position along the beamline

  GIRDER* girder = beamline->first;
  while (girder!=NULL) {
    ELEMENT* element=girder->element();
    double position_girder = 0.; // the position along the girder
    ELEMENT* element_prev = 0; // pointer to the previous element
    for (; element!=NULL; element=element->next) {

      const double length = element->get_length();
      n_new_elements++;

      const double end_element = position_beamline+length;
      int grid_pointnr = int(std::ceil(position_beamline/spacing));
      double grid_point = grid_pointnr*spacing;
      // if element fits before next grid_point go to next element:
      if (end_element <= grid_point) {
	position_beamline=end_element;
	position_girder+=length;
	element_prev = element;
	continue;
      }

      ELEMENT* element_next = element->next; // store next element to set it later

      // only add dipole kickers in drift space:
      // very delicate changing the splitting the magnets otherwise
      // and no copy constructors for any element type
      // for other elements add dipole after them (can be optimised maybe by adding them before and afterwards)

      if (!element->is_drift()) {
	DIPOLE* last_dipole = 0; // pointer to the last made dipole

	// if grid point is closer to start of element put it before element else after element:
	bool before = true;

	for (;grid_point < end_element; grid_point+=spacing) {

	  // new dipole with length 0 and strength 0
	  DIPOLE* dipole = new DIPOLE(0.,0.,0.);
	  char dipole_name[30];
	  snprintf(dipole_name,30,"%s%d","DIPOLE_GRID",n_dipoles); // give unique name
	  dipole->set_name(dipole_name);
	  n_dipoles++;

	  // if grid point is closer to start of element put it before element else after element:
	  if (before==true && (grid_point - position_beamline < end_element - grid_point)) {

	    // put dipole after previous dipole or else after prev element:
	    if (last_dipole) {last_dipole->next=dipole;}
	    else if (element_prev) {element_prev->next = dipole;}
	    
	  }
	  else {
	    if (before == true) {
	      before = false; // from now on put dipoles after the element
	      position_girder+=length;
	      position_beamline=end_element;

	      if (last_dipole) {last_dipole->next=element;}
	      element->next=dipole;
	    }
	    else {
	      last_dipole->next=dipole; // last_dipole is not 0 in this case
	    }
	  }

	  dipole->set_z(position_girder);
	  last_dipole=dipole;
	}

	if (before == true) { // advance position if not already done
	  position_girder+=length;
	  position_beamline=end_element;
	  last_dipole->next=element;
	  // continue loop from this element
	}
	else {
	  last_dipole->next=element_next;
	  element = last_dipole; // continue loop from last element
	}
      }

      else { // drift element

	// split drift
	double element_length = grid_point-position_beamline;
	element->set_z(position_girder+0.5*element_length);
	position_girder+=element_length;
	position_beamline+=element_length;
	element->set_length(element_length);

	DRIFT* drift = element->drift_ptr();
	
	while (position_beamline < end_element) {
      
	  // new dipole with length 0 and strength 0
	  DIPOLE* dipole = new DIPOLE(0.,0.,0.);
	  dipole->set_z(position_girder);
      
	  drift->next=dipole; // set dipole after last drift
	  
	  double drift_length = (end_element-position_beamline < spacing) ? (end_element-position_beamline) : spacing;
	  
	  drift = new DRIFT(drift_length);
	  dipole->next = drift;

	  drift->set_z(position_girder+0.5*drift_length);
	  position_girder+=drift_length;
	  position_beamline+=drift_length;
	
	  n_new_elements+=2;
	  n_dipoles++;
	  // repeat if necessary
	}

	drift->next = element_next;
	element = drift; // continue loop from last drift
      }
      element_prev = element; // store pointer to last element for next loop
    }
    girder=girder->next();
  }

  placet_cout << VERBOSE << "total length of beamline: " << position_beamline << std::endl;
  placet_cout << VERBOSE << "number of dipoles added: " << n_dipoles << " number of total elements: " << n_new_elements << endmsg;
}

/**********************************************************************/
/*                      AddBPMsToBeamline                             */
/**********************************************************************/

// adds BPMs before (or after) each quadrupole/multipole
// to be used before the beamline has been set

int tk_AddBPMsToBeamline(ClientData /*clientdata*/,Tcl_Interp *interp,
			       int argc,char *argv[])
{
  int error=0;
  int before=1; // used as bool
  Tk_ArgvInfo table[]={
    {(char*)"-before",TK_ARGV_INT,(char*)NULL,(char*)&before,
     (char*)"add BPMs before (after) the quadrupoles"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command adds BPMs before (after) each quadrupole"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  placet_printf(INFO,"%s\n",Tcl_GetStringResult(interp));

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AddBPMsToBeamline>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;

  add_bpms(inter_data.beamline,(bool)before);

  return TCL_OK;
}

/**********************************************************************/
/*                      AddDipolesToBeamline                             */
/**********************************************************************/

// adds Dipoles after (or before) each quadrupole/multipole
// to be used before the beamline has been set

int tk_AddDipolesToBeamline(ClientData /*clientdata*/,Tcl_Interp *interp,
			       int argc,char *argv[])
{
  int error=0;
  int before=0; // used as bool
  Tk_ArgvInfo table[]={
    {(char*)"-before",TK_ARGV_INT,(char*)NULL,(char*)&before,
     (char*)"add Dipoles after (before) the quadrupoles"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command adds Dipoles after (before) each quadrupole"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  placet_printf(INFO,"%s\n",Tcl_GetStringResult(interp));

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AddDipolesToBeamline>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;

  add_dipoles(inter_data.beamline,(bool)before);

  return TCL_OK;
}

void add_bpms(BEAMLINE* beamline, bool before)
{
  add_elements(beamline,before,true,false);
}

void add_dipoles(BEAMLINE* beamline, bool before)
{
  add_elements(beamline,before,false,true);
}

void add_elements(BEAMLINE* beamline, bool before, bool add_bpms, bool add_dipoles) 
{
  if (!add_bpms && !add_dipoles) {return;}
  if (add_bpms && add_dipoles) {
    placet_cout << ERROR << "add_elements: This use case is not implemented!" << endmsg;
    return;
  }

  int n_bpms = 0; // the number of bpms added
  int n_dipoles = 0; // the number of dipoles added
  int n_new_elements = 0; // the number of elements in the new beamline

  GIRDER* girder = beamline->first;
  while (girder!=NULL) {
    ELEMENT* element=girder->element();
    double position = 0.; // the position along the girder
    for (; element!=NULL; element=element->next) {

      const double length = element->get_length();
      position+=length;
      n_new_elements++;

      ELEMENT* element_next = element->next;
      if (!element_next && before) break;
      
      // continue if no quadrupole:
      if ( (before && !element_next->is_quad() && !element_next->is_multipole()) || (!before) && !element->is_quad() && !element->is_multipole()) {
	continue;
      }
      // new element
      ELEMENT* new_element=0;
      char el_name[20];
      if (add_bpms) {
	// new BPM
	BPM* bpm = new BPM(); // add bpm with length 0
	snprintf(el_name,20,"%s%d","BPM",n_bpms); // give unique name
	n_bpms++;
	new_element=bpm;
      } 
      if (add_dipoles) {
	// new DIPOLE
	DIPOLE* dipole = new DIPOLE(0.,0.,0.); // add dipole with length 0 and strength 0
	snprintf(el_name,20,"%s%d","DIPOLE",n_dipoles); // give unique name
	n_dipoles++;
	new_element=dipole;
      }
      new_element->set_name(el_name);
      new_element->set_z(position);
      n_new_elements++;
      // set next pointers (works for both before and after):
      element->next=new_element;
      new_element->next=element_next;
      
      // if new_element is first on girder then set element pointer of girder:
      if (before && girder->element() == element) {
	girder->set_first_element(new_element);
      }
	
      // continue from new_element
      element=new_element;
    }
    girder=girder->next();
  }
  placet_cout << VERBOSE << "number of BPMs added: " << n_bpms << " number of DIPOLEs added: " << n_dipoles << " number of total elements: " << n_new_elements << endmsg;
}

/**********************************************************************/
/*                      Aperture                                      */
/**********************************************************************/

int tk_Aperture(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;

  APERTURE *aperture = new APERTURE(argc, argv);

  inter_data.girder->add_element(aperture);
			 
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlinePrint                                 */
/**********************************************************************/

int tk_BeamlinePrint(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error;
  char *name=NULL;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"file to store the beamline data"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlinePrint>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  file=open_file(name);
  inter_data.beamline->print(file);
  close_file(file);
  
  return TCL_OK;
}

void
beamline_print_survey(FILE *f,BEAMLINE *b)
{
  double lsum=0.0;
  for (int i=0;i<b->n_elements;++i) {
    if (QUADRUPOLE *q=b->element[i]->quad_ptr()) {
      fprintf(f,"%12.6f %g %g\n",lsum,q->get_length(),q->get_strength()/q->get_length()*1e9/C_LIGHT);
    }
    lsum+=b->element[i]->get_length();
    //    b->element[i]->survey_print(f,&lsum);
  }
}

/**********************************************************************/
/*                      BeamlinePrintSurvey                           */
/**********************************************************************/

int tk_BeamlinePrintSurvey(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error;
  char *name=NULL;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"file to store the beamline data"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlinePrintSurvey>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  file=open_file(name);
  beamline_print_survey(file,inter_data.beamline);
  close_file(file);

  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineSet                                   */
/**********************************************************************/

int tk_BeamlineSet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  char *name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam line"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if (inter_data.beamline_set){
    Tcl_AppendResult(interp,"Error in BeamlineSet:\n",NULL);
    Tcl_AppendResult(interp,"Beamline is already defined\n",NULL);
    return TCL_ERROR;
  }
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlineSet>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  if (name){
    inter_data.beamline->set_name(name);
    // add beamline to global list
    std::pair<std::map<std::string,BEAMLINE*>::iterator,bool> it = 
      inter_data.blist.insert(make_pair(std::string(name),inter_data.beamline));
    if (it.second == false) {
      placet_cout << ERROR << "Beamline '" << name << "' already defined! - Exiting" << endmsg;
      exit(1);
    }
  }

  inter_data.beamline->set();
  // shift z position by half the girder length to center the positions around 0
  beamline_correct_z(inter_data.beamline);
  inter_data.beamline_set=1;
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineNew                                   */
/**********************************************************************/

int tk_BeamlineNew(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlineNew>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  // delete previous beamline if it was not not set
  if (inter_data.beamline_set==0 && inter_data.beamline) {
    delete inter_data.beamline;
  }

  inter_data.beamline_set=0;
  inter_data.beamline=new BEAMLINE();
  inter_data.synrad=false;
  inter_data.thin_lens=0;
  inter_data.longitudinal=false;
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineUse                                   */
/**********************************************************************/

int tk_BeamlineUse(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  char *name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam line"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlineUse>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  BEAMLINE* beamline = get_beamline(name);
  if (beamline) inter_data.beamline = beamline;
  else {
    Tcl_SetResult(interp,"BeamlineUse did not find your beamline",TCL_VOLATILE);
    return TCL_ERROR;
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineDelete                                */
/**********************************************************************/

int tk_BeamlineDelete(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  char *name=NULL;
  int error;
  BEAMLINE* beamline=0;
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"name of the beam line"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlineDelete>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  if (!name){
    Tcl_SetResult(interp,"BeamlineDelete expects a name",TCL_VOLATILE);
    return TCL_ERROR;
  }

  beamline = get_beamline(name);
  if (!beamline){
    Tcl_SetResult(interp,"BeamlineDelete did not find your beamline",TCL_VOLATILE);
    return TCL_ERROR;
  }

  delete beamline;
  // remove from map
  inter_data.blist.erase(std::string(name));

  return TCL_OK;
}

/**********************************************************************/
/*                      Bpm                                           */
/**********************************************************************/

int tk_Bpm(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	   char *argv[])
{
  if (element_help_message<BPM>(argc,argv, "Bpm:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  inter_data.girder->add_element( new BPM(argc, argv));
  return TCL_OK;
}

/**********************************************************************/
/*                      CavityBpm                                     */
/**********************************************************************/

int tk_CavityBpm(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  if (element_help_message<CAVBPM>(argc,argv, "CavityBpm",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  int error, kicks;
  char* wakekick=NULL;
  char ** v;
  Tk_ArgvInfo table[]={
    {(char*)"-wakekicks",TK_ARGV_STRING,(char*)NULL,(char*)&wakekick,
     (char*)"List of wakefield kicks [urad GeV / um] for each particle to itself and those behind it{k_{00}, k_{01} ... k_{0n}, k_{11}, etc.}"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  double * wk;
  if (wakekick){
    error = Tcl_SplitList(interp,wakekick,&kicks,&v);
    if (!error) {
      wk = (double*)alloca(sizeof(double)*kicks);
      for (int i=0; i<kicks; i++){
	if (error=Tcl_GetDouble(interp,v[i],&wk[i])) return error;
      }
    }
    Tcl_Free((char*)v);
  } else {
    kicks = 0;
  }

  inter_data.girder->add_element(new CAVBPM(argc, argv, kicks, wk));

  return TCL_OK;
}

/**********************************************************************/
/*                      TclCall                                       */
/**********************************************************************/

int tk_TclCall(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  if (element_help_message<TCLCALL>(argc,argv, "TclCall:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  inter_data.girder->add_element( new TCLCALL(argc, argv));
  return TCL_OK;
}

/**********************************************************************/
/*                      Cavity                                        */
/**********************************************************************/

int tk_Cavity(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  if (element_help_message<CAVITY>(argc,argv, "This command places a cavity in the current girder.\n\nCavity:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  CAVITY *cav = new CAVITY(argc, argv);
  cav->set_wake_data(injector_data.wake);
  if (cav->get_lambda()==0.0) {
    cav->set_lambda(injector_data.lambda[0]);
  }
  inter_data.girder->add_element(cav);
  // rf_data initialization for Main Beam
  rf_data.cavlength=cav->get_length();
  rf_data.gradient=cav->get_gradient();
  rf_data.phase_shift=cav->get_phase();
  return TCL_OK;
}

/**********************************************************************/
/*                      DecCavity                                     */
/**********************************************************************/

int tk_DecCavity(ClientData clientdata,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  double length=0.0,v1=0.0,rf_seed=0.0;
  CAVITY_PETS *element;
  char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"length of the cavity in meter"},
    {(char*)"-v1",TK_ARGV_FLOAT,(char*)NULL,(char*)&v1,
     (char*)"v1 value of the cavity"},
    {(char*)"-rf_seed",TK_ARGV_FLOAT,(char*)NULL,(char*)&rf_seed,
     (char*)"RF seeding voltage of the cavity"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <DecCavity>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) 	return TCL_ERROR;
 
  element=new CAVITY_PETS;
  element->set_name(element_name);
  element->set_length(length);
  element->set_phi(v1);
//  element->type=CAV_PETS;
  element->set_wake((PetsWake*)clientdata);
  element->set_rf_seed(rf_seed);
//  element->callback=NULL;
  inter_data.girder->add_element(element);
  return TCL_OK;
}

/**********************************************************************/
/*                      Dipole                                        */
/**********************************************************************/

int tk_Dipole(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	      char *argv[])
{
  if (element_help_message<DIPOLE>(argc,argv, "Dipole:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  DIPOLE *dipole = new DIPOLE(argc, argv);
  inter_data.girder->add_element(dipole);
  return TCL_OK;
}

/**********************************************************************/
/*                      Drift                                         */
/**********************************************************************/

int tk_Drift(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	      char *argv[])
{
  if (element_help_message<DRIFT>(argc,argv, "Drift:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  DRIFT *element = new DRIFT(argc,argv);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

/**********************************************************************/
/*                      Girder                                        */
/**********************************************************************/

int tk_Girder(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	      char *argv[])
{
  int error;
  double dist=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-distance",TK_ARGV_FLOAT,(char*)NULL,(char*)&dist,
     (char*)"Distance to the previous girder in meter"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Girder>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  inter_data.girder=new GIRDER(0.0);
  inter_data.beamline->girder_add(inter_data.girder,dist);
  return TCL_OK;
}

/**********************************************************************/
/*                      Multipole                                     */
/**********************************************************************/

int tk_Multipole(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{ 
  if (element_help_message<MULTIPOLE>(argc,argv, "Multipole:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  MULTIPOLE *element = new MULTIPOLE(argc,argv);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

/**********************************************************************/
/*                      RfMultipole                                     */
/**********************************************************************/

int tk_RfMultipole(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{ 
  if (element_help_message<RFMULTIPOLE>(argc,argv, "RfMultipole:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  RFMULTIPOLE *element = new RFMULTIPOLE(argc,argv);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadBpm                                       */
/**********************************************************************/

int tk_QuadBpm(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  if (element_help_message<QUADBPM>(argc,argv, "QuadBpm:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  QUADBPM *element = new QUADBPM(argc,argv);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

/**********************************************************************/
/*                      Quadrupole                                    */
/**********************************************************************/

int tk_Quadrupole(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  if (element_help_message<QUADRUPOLE>(argc,argv, "This command places a quadrupole in the current girder.\n\nQuadrupole:", interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  QUADRUPOLE *quad = new QUADRUPOLE(argc, argv);
  inter_data.girder->add_element(quad);
  return TCL_OK;
}

/**********************************************************************/
/*                      Rmatrix                                       */
/**********************************************************************/

int tk_Rmatrix(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
               char *argv[])
{
  int error;
  char* r=NULL;
  int n,i;
  char** v;
  double rm[16],l=0.0;
  const char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-R",TK_ARGV_STRING,(char*)NULL,(char*)&r,
     (char*)"Transfer matrix"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&l,
     (char*)"Length of the transfer region"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  }; 

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Rmatrix>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) 	return TCL_ERROR;

  if (error=Tcl_SplitList(interp,r,&n,&v)) return error;
  if (n!=16) {
    /*
      useful return value
     */
    return TCL_ERROR;
  }

  for (i=0;i<n;i++){
    if (error=Tcl_GetDouble(interp,v[i],rm+i)) return error;
  }

  ELEMENT *element;
  inter_data.girder->add_element(element=new RMATRIX(l,rm));
  element->set_name(element_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      SetupMainBeamline                             */
/**********************************************************************/

int tk_SetupMainBeamline(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error=TCL_OK;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (inter_data.beamline_set){
    Tcl_AppendResult(interp,"Error in SetupMainBeamline:\n",NULL);
    Tcl_AppendResult(interp,"Beamline is already defined\n",NULL);
    return TCL_ERROR;
  }
  if ((argc!=2)&&(argc!=3)){
    Tcl_AppendResult(interp,"Error in SetupMainBeamline:\n",NULL);
    Tcl_AppendResult(interp,"Wrong number of arguments\n",NULL);
    Tcl_AppendResult(interp,"Usage: SetupMainBeamline filename [beam_name]\n",NULL);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  if (argc==2) {
    setup_TeV(argv[1],"beam.dat",inter_data.beamline);
  }
  else {
    setup_TeV(argv[1],argv[2],inter_data.beamline);
  }
  inter_data.beamline_set=1;
  return TCL_OK;
}

/**********************************************************************/
/*                      SetupSmoothBeamline                           */
/**********************************************************************/

int tk_SetupSmoothBeamline(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error=TCL_OK;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (inter_data.beamline_set){
    Tcl_AppendResult(interp,"Error in SetupSmoothBeamline:\n",NULL);
    Tcl_AppendResult(interp,"Beamline is already defined\n",NULL);
    return TCL_ERROR;
  }
  if (argc!=2){
    Tcl_AppendResult(interp,"Error in SetupSmoothBeamline:\n",NULL);
    Tcl_AppendResult(interp,"Wrong number of arguments\n",NULL);
    Tcl_AppendResult(interp,"Usage: SetupSmoothBeamline filename\n",NULL);
    return TCL_ERROR;
  }
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  setup_smooth(argv[1],inter_data.beamline);
  inter_data.beamline_set=1;
  return TCL_OK;
}

/**********************************************************************/
/*                      Solenoid                                      */
/**********************************************************************/

int tk_Solenoid(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  if (element_help_message<SOLENOID>(argc,argv, "This command places a solenoid in the current girder.\n\nSolenoid:", interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  SOLENOID *quad = new SOLENOID(argc, argv);
  inter_data.girder->add_element(quad);
  return TCL_OK;
}

/**********************************************************************/
/*                      AddElement                                    */
/**********************************************************************/

int tk_AddElement(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
                  char *argv[])
{
  int error;
  char *w=NULL,*s=NULL;
  GIRDER *g=NULL;
  ELEMENT *e=NULL,*t;
  TCLCALL *tt;
  char **v;
  int i,j,n;
  int *p;
  Tk_ArgvInfo table[]={
    {(char*)"-where",TK_ARGV_STRING,(char*)NULL,(char*)&w,
     (char*)"Element before which to place the TclCall"},
    {(char*)"-script",TK_ARGV_STRING,(char*)NULL,(char*)&s,
     (char*)"Script to be used for the TclCall"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AddElement>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (!check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  if ((error=Tcl_SplitList(interp,w,&n,&v))) {
    return error;
  }

  p=(int*)alloca(sizeof(int)*n);
  for (j=0;j<n;++j) {
    if (strcmp(v[j],"end")==0) {
      p[j]=inter_data.beamline->n_elements-1;
    }
    else {
      if ((error=Tcl_GetInt(interp,v[j],p+j))) {
        return error;
      }
    }
  }
  inter_data.beamline->unset();
  g=inter_data.beamline->first;
  i=0;
  j=0;
  while (g) {
    e=g->element();
    while (e) {
      if (i==p[j]) {
        t=e->next;
        tt=new TCLCALL(s);
        e->next=tt;
        tt->next=t;
        ++j;
      }
      e=e->next;
      ++i;
    }
    g=g->next();
  }
  inter_data.beamline->set();
  return TCL_OK;
}
