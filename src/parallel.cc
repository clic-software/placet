#include <cstdlib>
//#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "lattice.h"
#include "girder.h"
#include "structures_def.h"

#include "wakes.h"

#include "parallel.h"

void BEGIN_PARALLEL::step_4d_0(BEAM * /*beam*/ )
{
//  placet_cout << DEBUG << __FUNCTION__ << endmsg;
}

END_PARALLEL::END_PARALLEL(int &argc, char **argv ) : reslice(false), center(false)
{
  attributes.add("reslice", OPT_BOOL, &reslice);
  attributes.add("center", OPT_BOOL, &center);
  set_attributes(argc, argv);
}

void END_PARALLEL::step_6d_0(BEAM *beam )
{
//  placet_cout << DEBUG << __FUNCTION__ << endmsg;
  qsort(beam->particle,beam->slices,sizeof(PARTICLE),cmp_func_t(&particle_cmp));
  double wgt0=0.0;
  for (int i=0;i<beam->slices;i++) {
    wgt0+=beam->particle[i].wgt;
  }
  for (int i=0;i<beam->slices_per_bunch*beam->bunches;i++) {
    beam->particle_number[i]=0;
  }
  if (reslice) {
    int particles_per_bunch=beam->slices/beam->bunches;
    for (int ib=0;ib<beam->bunches;ib++) {
      double zmin;
      double zmax;
      {
        double zmean=0;
        int m=ib*particles_per_bunch;
        for (int i=0;i<particles_per_bunch;i++) {
	  zmean+=beam->particle[m++].z;
        }
        zmean/=particles_per_bunch;
        double t1=0;
        double t2=0;
        m=ib*particles_per_bunch;
        for (int i=0;i<particles_per_bunch;i++) {
	  double t3=beam->particle[m++].z-zmean;
          t1+=t3*t3;
	  t2+=t3;
        }
        double zrms=sqrt((t1-t2*t2/particles_per_bunch)/(particles_per_bunch-1));
        zmin=zmean-3*zrms;
        zmax=zmean+3*zrms;
        
        placet_cout << VERYVERBOSE << "z rms: " << zrms << endmsg;
      }
      
      double dzi=(zmax-zmin)/beam->slices_per_bunch;
      if (center) zmin=-(zmax-zmin)/2;
      for (int i=0;i<beam->slices_per_bunch;i++) {
        beam->z_position[ib*beam->slices_per_bunch+i]=zmin+(2*i+1)*dzi/2;
      }
    }
  }

  double z_max=0.5*(beam->z_position[1]+beam->z_position[0]);
  for (int i=0,j=0;i<beam->slices;i++) {
    while (beam->particle[i].z>z_max) {
      j++;
      if (j==beam->slices_per_bunch*beam->bunches) {
	placet_printf(WARNING,"too large z %g\n",beam->particle[i].z);
	j=beam->slices_per_bunch*beam->bunches-1;
	break;
      }
      if (j<beam->slices_per_bunch*beam->bunches-1) {
	z_max=0.5*(beam->z_position[j+1]+beam->z_position[j]);
      } else {
	z_max=beam->z_position[j]+0.5*(beam->z_position[j]-beam->z_position[j-1]);
      }
    }
    beam->particle[i].wgt=wgt0/beam->slices;
    beam->particle_number[j]++;
  }

  if (reslice) {
    wakes_prepare(beam);
  }
}

int tk_BeginParallel(ClientData /*clientdata*/,Tcl_Interp *interp,int /*argc*/, char *argv[])
{
  int error=TCL_OK;
  if (check_beamline_changeable(interp,argv[0],error))	return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error))	return TCL_ERROR;
  inter_data.girder->add_element(new BEGIN_PARALLEL);
  inter_data.longitudinal=true;
  return TCL_OK;
}

int tk_EndParallel(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  int error=TCL_OK;
  if (check_beamline_changeable(interp,argv[0],error))	return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error))	return TCL_ERROR;
  inter_data.girder->add_element(new END_PARALLEL(argc, argv));
  inter_data.longitudinal=false;
  return TCL_OK;
}
