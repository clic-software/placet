#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beamline.h"
#include "structures_def.h"

/* Function prototypes */

#include "quadrupole.h"
#include "multipole.h"
//#include "solenoid.h"
#include "cavity.h"
//#include "dipole.h"
//#include "drift.h"
//#include "bpm.h"
#include "placet_tk.h"

#include "graph.h"
#include "rmatrix.h"

extern INTER_DATA_STRUCT inter_data;

/**********************************************************************/
/*                      element_position_plot (not a command)         */
/**********************************************************************/

void element_position_plot(BEAMLINE * beamline, char *fname, bool bpm, bool quad, bool position)
{
  FILE *file=open_file(fname);
  double s=0.0;
  for (int i=0,j=0;i<beamline->n_elements;i++){
    ELEMENT *element=beamline->element[i];
    s+=0.5*element->get_length();
    if ((bpm && element->is_bpm()) || (quad && element->is_quad())) {
      if (position) 
	fprintf(file,"%d %g %g\n",j++,element->offset.y,s);
      else
	fprintf(file,"%d %g\n",j++,element->offset.y);
    }
    s+=0.5*element->get_length();
  }
  close_file(file);
}

void bpm_plot_all(BEAMLINE *beamline,char *fname)
{
  element_position_plot(beamline,fname,true,false,false);
}

void bpm_position_plot(BEAMLINE *beamline,char *fname)
{
  element_position_plot(beamline,fname,true,false,true);
}

void quadrupole_position_plot(BEAMLINE *beamline,char *fname)
{
  element_position_plot(beamline,fname,false,true,true);
}

int testshow(BEAM *beam,int /*n*/,Tcl_Interp *i,char * /*data*/)
{
  inter_data.bunch=beam;
  Tcl_Eval(i,"BeamlineView");
  Tcl_Eval(i,"BeamView");
  return 0;
}

int testshow2(BEAM *beam,int /*n*/,Tcl_Interp *i,char * /*data*/)
{
  inter_data.bunch=beam;
  Tcl_Eval(i,"BeamViewFilm");
  return 0;
}




/******   Tcl/Tk commands   ******/

/**********************************************************************/
/*                      BeamlinePositionPlot                          */
/**********************************************************************/

int tk_BeamlinePositionPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int i,n,error;
  ELEMENT **element;
  char *name=NULL;
  double s=0.0;
  FILE *file;

  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"File where to store results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,
			  TK_ARGV_NO_DEFAULTS|TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }

  n=inter_data.beamline->n_elements;
  element=inter_data.beamline->element;
  file=open_file(name);
  /*
  for (i=0;i<n;i++){
    s+=0.5*element[i]->get_length();
    fprintf(file,"%g %g\n",s,element[i]->offset.y);
    s+=0.5*element[i]->get_length();
  }
  */
  for (i=0;i<n;i++){
    s+=0.5*element[i]->get_length();
    fprintf(file,"%.17g %.17g\n",s-0.5*element[i]->get_length(),element[i]->offset.y
	    -0.5*element[i]->get_length()*element[i]->offset.yp);
    fprintf(file,"%.17g %.17g\n\n",s+0.5*element[i]->get_length(),element[i]->offset.y
	    +0.5*element[i]->get_length()*element[i]->offset.yp);
    s+=0.5*element[i]->get_length();
  }
  
  close_file(file);
  
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineShow                                  */
/**********************************************************************/

int tk_BeamlineShow(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  static double smax=1000.0,ymean=100.0,yscale=0.3,sscale=30.0;
  static double dq=30.0,dc=10.0,db=30.0,dd=10.0;
  double ymin,ymax,ds,s=0.0;
  static int do_quad=1,do_bpm=1,do_cav=1,do_drift=0,do_dipole=0;
  static int i0=0;
  int error,j,plot,i,precise=1;
  ELEMENT *element;
  char buffer[512];
  Tk_ArgvInfo table[]={
    {(char*)"-y0",TK_ARGV_FLOAT,(char*)NULL,(char*)&ymean,
     (char*)"Zero line in pixels"},
    {(char*)"-yscale",TK_ARGV_FLOAT,(char*)NULL,(char*)&yscale,
     (char*)"Vertical scale [pixel/micro m]"},
    {(char*)"-sscale",TK_ARGV_FLOAT,(char*)NULL,(char*)&sscale,
     (char*)"Horizontal scale [pixel/m]"},
    {(char*)"-first",TK_ARGV_INT,(char*)NULL,(char*)&i0,
     (char*)"Number of first element to plot"},
    {(char*)"-quadrupoles",TK_ARGV_INT,(char*)NULL,(char*)&do_quad,
     (char*)"Flag whether to plot quadrupoles"},
    {(char*)"-bpms",TK_ARGV_INT,(char*)NULL,(char*)&do_bpm,
     (char*)"Flag wether to plot BPMs"},
    {(char*)"-dipoles",TK_ARGV_INT,(char*)NULL,(char*)&do_dipole,
     (char*)"Flag whether to plot dipoles"},
    {(char*)"-cavities",TK_ARGV_INT,(char*)NULL,(char*)&do_cav,
     (char*)"Flag whether to plot structures"},
    {(char*)"-drifts",TK_ARGV_INT,(char*)NULL,(char*)&do_drift,
     (char*)"Flag whether to plot drifts"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if (argc<3){
    Tcl_AppendResult(interp,"Number of arguments to BeamlineShow is too small",NULL);
    return TCL_ERROR;
  }
  if (strcmp(argv[1],"plot")==0){
    for (j=1;j<argc-1;j++){
      argv[j]=argv[j+1];
    }
    plot=1;
    argc--;
  }
  else{
    if (strcmp(argv[1],"configure")==0){
      for (j=1;j<argc-1;j++){
	argv[j]=argv[j+1];
      }
      plot=0;
      argc--;
    }
    else{
      Tcl_AppendResult(interp,"Command to BeamlineShow must be configure, cget or plot",NULL);
      return TCL_ERROR;
    }
  }

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_DEFAULTS))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_AppendResult(interp,"Wrong number of arguments to BeamlineShow",NULL);
    return TCL_ERROR;
  }

  if (plot){
    i=i0;
    snprintf(buffer,512,"%s delete all\n",argv[1]); 
    Tcl_Eval(interp,buffer);
    while (s<smax){
      if (i>=inter_data.beamline->n_elements) break;
      element=inter_data.beamline->element[i++];
      ds=element->get_length()*sscale;
      if (precise) {
        if (element->is_quad()) {
	  if (do_quad){
	    ymin=ymean-dq-yscale*element->offset.y;
	    ymax=ymean+dq-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %g %g %g %g -fill red\n",
		    argv[1],s,ymin,s+ds,ymax); 
	    Tcl_Eval(interp,buffer);
	  }
          
        } else if (element->is_solenoid()) {
          if (do_bpm){
	    ymin=ymean-db-yscale*element->offset.y;
	    ymax=ymean+db-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %g %g %g %g -fill orange\n",
		    argv[1],s,ymin,s+ds,ymax);
	    Tcl_Eval(interp,buffer);
	  }
        } else if (element->is_drift()) {
          if (do_drift){
	    ymin=ymean-dd-yscale*element->offset.y;
	    ymax=ymean+dd-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %g %g %g %g\n",
		    argv[1],s,ymin,s+ds,ymax); 
	    Tcl_Eval(interp,buffer);
	  }
	} else if (element->is_cavity() || element->is_cavity_pets()) {
	  if (do_cav){
	    ymin=ymean-dc-yscale*element->offset.y;
	    ymax=ymean+dc-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %g %g %g %g -fill green\n",
		    argv[1],s,ymin,s+ds,ymax); 
	    Tcl_Eval(interp,buffer);
	  }
        } else if (element->is_bpm()) {
          if (do_bpm){
	    ymin=ymean-db-yscale*element->offset.y;
	    ymax=ymean+db-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %g %g %g %g -fill blue\n",
		    argv[1],s,ymin,s+ds,ymax); 
	    Tcl_Eval(interp,buffer);
	  }
        } else if (element->is_dipole()) {
          if (do_dipole){
	    ymin=ymean-db-yscale*element->offset.y;
	    ymax=ymean+db-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %g %g %g %g -fill orange\n",
		    argv[1],s,ymin,s+ds,ymax);
	    Tcl_Eval(interp,buffer);
	  }
        }
      } else{
        if (element->is_quad()) {
	  if (do_quad){
	    ymin=ymean-dq-yscale*element->offset.y;
	    ymax=ymean+dq-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %d %d %d %d -fill red\n",
		    argv[1],(int)s,(int)ymin,(int)(s+ds),(int)ymax); 
	    Tcl_Eval(interp,buffer);
	  }
	} else if (element->is_solenoid()) {
	  if (do_bpm){
	    ymin=ymean-db-yscale*element->offset.y;
	    ymax=ymean+db-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %d %d %d %d -fill orange\n",
		    argv[1],(int)s,(int)ymin,(int)(s+ds),(int)ymax); 
	    Tcl_Eval(interp,buffer);
	  }
        } else if (element->is_drift()) {
	  if (do_drift){
	    ymin=ymean-dd-yscale*element->offset.y;
	    ymax=ymean+dd-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %d %d %d %d\n",
		    argv[1],(int)s,(int)ymin,(int)(s+ds),(int)ymax); 
	    Tcl_Eval(interp,buffer);
	  }
        } else if (element->is_cavity() || element->is_cavity_pets()) {
	  if (do_cav){
	    ymin=ymean-dc-yscale*element->offset.y;
	    ymax=ymean+dc-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %d %d %d %d -fill green\n",
		    argv[1],(int)s,(int)ymin,(int)(s+ds),(int)ymax); 
	    Tcl_Eval(interp,buffer);
	  }
	} else if (element->is_bpm()) {
	  if (do_bpm){
	    ymin=ymean-db-yscale*element->offset.y;
	    ymax=ymean+db-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %d %d %d %d -fill blue\n",
		    argv[1],(int)s,(int)ymin,(int)(s+ds),(int)ymax); 
	    Tcl_Eval(interp,buffer);
	  }
        } else if (element->is_dipole()) {
          if (do_dipole){
	    ymin=ymean-db-yscale*element->offset.y;
	    ymax=ymean+db-yscale*element->offset.y;
	    snprintf(buffer,512,"%s create rectangle %d %d %d %d -fill orange\n",
		    argv[1],int(s),int(ymin),int(s+ds),int(ymax));
	    Tcl_Eval(interp,buffer);
	  }
        }
      }
      s+=ds;
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamShow                                      */
/**********************************************************************/

int tk_BeamShow(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
  static double smax=1000.0,ymean=100.0,yscale=0.3,length=10.0,dist=50.0;
  double ymin,ymax,ds,s=0.0,sigma,ycor=0.0,wgtsum=0.0;
  static int i0=0;
  int error,j,plot,i,k,jpart;
  static int centre=0;
  char buffer[512];
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-y0",TK_ARGV_FLOAT,(char*)NULL,(char*)&ymean,
     (char*)"Zero line in pixels"},
    {(char*)"-yscale",TK_ARGV_FLOAT,(char*)NULL,(char*)&yscale,
     (char*)"Vertical scale [pixel/micro m]"},
    {(char*)"-slicelength",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"Horizontal scale [pixel/slice]"},
    {(char*)"-bunchdistance",TK_ARGV_FLOAT,(char*)NULL,(char*)&dist,
     (char*)"Distance between bunches [pixel]"},
    {(char*)"-first",TK_ARGV_INT,(char*)NULL,(char*)&i0,
     (char*)"Number of first element to plot"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"File name were to store the plot data"},
    {(char*)"-centre",TK_ARGV_INT,(char*)NULL,(char*)&centre,
     (char*)"If not 0 the centroid motion is removed"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_DEFAULTS))
      !=TCL_OK){
    return error;
  }
  if (argc<3){
    Tcl_AppendResult(interp,"Number of arguments to BeamShow is too small",
		     NULL);
    return TCL_ERROR;
  }
  if (strcmp(argv[1],"plot")==0){
    for (j=1;j<argc-1;j++){
      argv[j]=argv[j+1];
    }
    plot=1;
    argc--;
  }
  else{
    if (strcmp(argv[1],"configure")==0){
      for (j=1;j<argc-1;j++){
	argv[j]=argv[j+1];
      }
      plot=0;
      argc--;
    }
    else{
      Tcl_AppendResult(interp,"Command to BeamShow must be configure, cget or plot",NULL);
      return TCL_ERROR;
    }
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Wrong number of arguments to BeamShow",NULL);
    return TCL_ERROR;
  }

  if (plot){
    if (file_name) {
      snprintf(buffer,512,"puts -nonewline %s \"{\"",file_name);      
      Tcl_Eval(interp,buffer);
    }
    snprintf(buffer,512,"%s delete all\n",argv[1]);
    Tcl_Eval(interp,buffer);
    if (centre) {
      for (i=0;i<inter_data.bunch->slices;++i) {
	wgtsum+=fabs(inter_data.bunch->particle[i].wgt);
	ycor+=fabs(inter_data.bunch->particle[i].wgt)
	  *inter_data.bunch->particle[i].y;
      }
      ycor/=wgtsum;
    }
    i=i0;
    while (s<smax){
      if (i>=inter_data.bunch->bunches) break;
      k=inter_data.bunch->slices_per_bunch*inter_data.bunch->macroparticles*i;
      ds=length;
      for (j=0;j<inter_data.bunch->slices_per_bunch;j++){
	for (jpart=0;jpart<inter_data.bunch->macroparticles;jpart++){
	  sigma=sqrt(inter_data.bunch->sigma[k].r11);
	  ymin=ymean-yscale*(inter_data.bunch->particle[k].y-ycor+sigma);
	  ymax=ymean-yscale*(inter_data.bunch->particle[k].y-ycor-sigma);
	  snprintf(buffer,512,"%s create rectangle %d %d %d %d -fill red\n",
		  argv[1],(int)s,(int)ymin,(int)(s+ds),(int)ymax); 
	  Tcl_Eval(interp,buffer);
	  if (file_name) {
	    if (j==0) {
	      snprintf(buffer,512,"puts -nonewline %s \"{%d %d}\"",file_name,
		      (int)ymin,(int)ymax);
	    }
	    else {
	      snprintf(buffer,512,"puts -nonewline %s \" {%d %d} \"",file_name,
		      (int)ymin,(int)ymax);
	    }
	    Tcl_Eval(interp,buffer);
	  }
	  k++;
	}
	s+=ds;
      }
      i++;
      s+=dist;
    }
    if (file_name) {
      snprintf(buffer,512,"puts -nonewline %s \"} \"",file_name);      
      Tcl_Eval(interp,buffer);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                      BpmPlot                                       */
/**********************************************************************/

int tk_BpmPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to BpmPlot",
                  TCL_VOLATILE);
    return TCL_ERROR;
  }

  bpm_plot_all(inter_data.beamline,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      BpmPositionPlot                               */
/**********************************************************************/

int tk_BpmPositionPlot(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to BpmPositionPlot",
                  TCL_VOLATILE);
    return TCL_ERROR;
  }

  bpm_position_plot(inter_data.beamline,file_name);
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadrupolePositionPlot                        */
/**********************************************************************/

int tk_QuadrupolePositionPlot(ClientData /*clientdata*/,Tcl_Interp *interp,
			      int argc,char *argv[])
{
  int error;
  char *file_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to QuadrupolePositionPlot",
                  TCL_VOLATILE);
    return TCL_ERROR;
  }

  quadrupole_position_plot(inter_data.beamline,file_name);
  return TCL_OK;
}
