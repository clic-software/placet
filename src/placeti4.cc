#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "beamline.h"
#include "cavity.h"
#include "quadrupole.h"
#include "dipole.h"
#include "malloc.h"
#include "matrix.h"
#include "minimise.h"
#include "placeti3.h"
#include "placeti4.h"
#include "random.hh"
#include "bpm.h"
#include "track.h"
#include "quadbpm.h"

extern int data_nstep;
extern double zero_point;

extern EMITT_DATA emitt_data;
extern CORR corr;
extern ERRORS errors;
extern RF_DATA rf_data;
extern BUMP_DATA bump_data;
extern SURVEY_ERRORS_STRUCT survey_errors;

int bump_long;

void bump_calculate(BUMP *bump,BEAM *bunch,double s[])
{
  double k1=0.0,k2=0.0,h11=0.0,h12=0.0,h22=0.0;
  double tmp_a11=0.0,tmp_a12=0.0,tmp_a21=0.0,tmp_a22=0.0,wgt_sum=0.0;
  double *a11,*a12,*a21,*a22;
  double alpha,beta,gamma,wgt;
  int i,ns,nb;
  PARTICLE *particle;
  double y=0.0,yp=0.0;

  nb=bunch->bunches;
  ns=bunch->slices_per_bunch*bunch->macroparticles;
  alpha=bump->alpha;
  beta=bump->beta;
  gamma=bump->gamma;
  a11=bump->a11;
  a12=bump->a12;
  a21=bump->a21;
  a22=bump->a22;
  particle=bunch->particle;
//  n=bunch->slices;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    wgt_sum+=wgt;
    tmp_a11+=wgt*a11[i];
    tmp_a12+=wgt*a12[i];
    tmp_a21+=wgt*a21[i];
    tmp_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
    y+=wgt*particle[i].y;
    yp+=wgt*particle[i].yp;
#endif
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    wgt_sum+=wgt;
    tmp_a11+=wgt*a11[i];
    tmp_a12+=wgt*a12[i];
    tmp_a21+=wgt*a21[i];
    tmp_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
    y+=wgt*particle[i].y;
    yp+=wgt*particle[i].yp;
#endif
  }
  y/=wgt_sum;
  yp/=wgt_sum;
  tmp_a11/=wgt_sum;
  tmp_a12/=wgt_sum;
  tmp_a21/=wgt_sum;
  tmp_a22/=wgt_sum;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    k1+=gamma*(particle[i].y-y)*wgt*(a11[i]-tmp_a11)
      +beta*(particle[i].yp-yp)*wgt*(a21[i]-tmp_a21)
      +alpha*wgt*((particle[i].y-y)*(a21[i]-tmp_a21)
		  +(particle[i].yp-yp)*(a11[i]-tmp_a11));
    k2+=gamma*(particle[i].y-y)*wgt*(a12[i]-tmp_a12)
      +beta*(particle[i].yp-yp)*wgt*(a22[i]-tmp_a22)
      +alpha*wgt*((particle[i].y-y)*(a22[i]-tmp_a22)
		  +(particle[i].yp-yp)*(a12[i]-tmp_a12));
    h11+=wgt*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
	      +2.0*alpha*a11[i]*a21[i]);
    h22+=wgt*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
	      +2.0*alpha*a12[i]*a22[i]);
    h12+=wgt*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
	      +alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    k1+=gamma*(particle[i].y-y)*wgt*(a11[i]-tmp_a11)
      +beta*(particle[i].yp-yp)*wgt*(a21[i]-tmp_a21)
      +alpha*wgt*((particle[i].y-y)*(a21[i]-tmp_a21)
	  +(particle[i].yp-yp)*(a11[i]-tmp_a11));
    k2+=gamma*(particle[i].y-y)*wgt
	*(a12[i]-tmp_a12)
      +beta*(particle[i].yp-yp)*wgt
	*(a22[i]-tmp_a22)
      +alpha*wgt
	*((particle[i].y-y)*(a22[i]-tmp_a22)
	  +(particle[i].yp-yp)*(a12[i]-tmp_a12));
    h11+=wgt
	*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
	  +2.0*alpha*a11[i]*a21[i]);
    h22+=wgt
	*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
	  +2.0*alpha*a12[i]*a22[i]);
    h12+=wgt
	*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
	  +alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
  }
  h11-=gamma*tmp_a11*tmp_a11+beta*tmp_a21*tmp_a21+2.0*alpha*tmp_a11*tmp_a21;
  h22-=gamma*tmp_a12*tmp_a12+beta*tmp_a22*tmp_a22+2.0*alpha*tmp_a12*tmp_a22;
  h12-=gamma*tmp_a11*tmp_a12+beta*tmp_a21*tmp_a22
    +alpha*(tmp_a12*tmp_a21+tmp_a11*tmp_a22);
  s[0]=-(h22*k1-h12*k2)/(h11*h22-h12*h12);
  s[1]=-(h11*k2-h12*k1)/(h11*h22-h12*h12);
  //  placet_printf(INFO,"found %g %g\n",s[0],s[1]);
  bump_minimise(bunch,bump,s);
  //  placet_printf(INFO,"found> %g %g\n",s[0],s[1]);
}

void simple_correct_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		     BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  bunch_track_emitt_start();
  for (i=0;i<nbin;i++){
//    bunch_track(beamline,bunch,ipos,bin[i]->start);
//    emitt_data.store(i,bunch,beamline->get_length());
    bunch_track_emitt(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    ipos=bin[i]->start;
  }
  bunch_track_emitt(beamline,bunch,ipos,beamline->n_elements);
  bunch_track_emitt_end(bunch,beamline->get_length());
//  emitt_data.store(nbin,bunch,beamline->get_length());
}

void simple_correct_dipole_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			    BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  bunch_track_emitt_start();
  for (i=0;i<nbin;i++){
    bunch_track_emitt(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    ipos=bin[i]->start;
  }
  bunch_track_emitt(beamline,bunch,ipos,beamline->n_elements);
  bunch_track_emitt_end(bunch,beamline->get_length());
//  emitt_data.store(nbin,bunch,beamline->get_length());
}

void simple_correct(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
	       BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct(beamline,0,bin[i]);
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
}

void simple_correct_range(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			  BEAM *workbunch,int ipos,int end)
{
  int i;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct(beamline,0,bin[i]);
  }
  bunch_track(beamline,bunch,ipos,end);
}

/* does an NLC type correction of the sectors */

void simple_correct_nlc(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		   BEAM *workbunch,BEAM *probe)
{
  int ipos,i;
  ipos=0;
  bunch_track_emitt_start();
  if (probe){
    for (i=0;i<nbin;i++){
      bunch_track_emitt(beamline,probe,ipos,bin[i]->start);
      bunch_track_0(beamline,bunch,ipos,bin[i]->start);
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      ipos=bin[i]->start;
      bin_correct_nlc(beamline,0,bin[i]);
    }
    bunch_track_emitt(beamline,probe,ipos,beamline->n_elements);
    bunch_track_emitt_end(probe,beamline->get_length());
  }
  else{
    for (i=0;i<nbin;i++){
      bunch_track_emitt(beamline,bunch,ipos,bin[i]->start);
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      ipos=bin[i]->start;
      bin_correct_nlc(beamline,0,bin[i]);      
    }
    bunch_track_emitt(beamline,bunch,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch,beamline->get_length());
  }
}

void simple_correct_nlc_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			  BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
      bunch_track_0(beamline,bunch,ipos,bin[i]->start);
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      ipos=bin[i]->start;
      bin_correct_nlc(beamline,0,bin[i]);
  }
  bunch_track_0(beamline,bunch,ipos,beamline->n_elements);
}

void simple_correct_nlc_jitter(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *beam0,
			  BEAM *beam1,BEAM *workbeam,BEAM* probe)
{
  int ipos,i;
  ipos=0;
  bunch_track_emitt_start();
  if (probe){
    for (i=0;i<nbin;i++){
      bunch_track_emitt(beamline,probe,ipos,bin[i]->start);
      bunch_track_0(beamline,beam0,ipos,bin[i]->start);
      bunch_track_0(beamline,beam1,ipos,bin[i]->start);
      bunch_join_0(beam0,beam1,Misalignments.Gauss(1,3),workbeam);
      bin_measure(beamline,workbeam,0,bin[i]);
      ipos=bin[i]->start;
      bin_correct_nlc(beamline,0,bin[i]);
    }
    bunch_track_emitt(beamline,probe,ipos,beamline->n_elements);
    bunch_track_emitt_end(probe,beamline->get_length());
  }
  else{
    for (i=0;i<nbin;i++){
      bunch_track_emitt(beamline,beam0,ipos,bin[i]->start);
      bunch_track_0(beamline,beam1,ipos,bin[i]->start);
      bunch_join_0(beam0,beam1,Misalignments.Gauss(1,3),workbeam);
      bin_measure(beamline,workbeam,0,bin[i]);
      ipos=bin[i]->start;
      bin_correct_nlc(beamline,0,bin[i]);
    }
    bunch_track_emitt(beamline,beam0,ipos,beamline->n_elements);
    bunch_track_emitt_end(beam0,beamline->get_length());
  }
}

void simple_correct_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
	       BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    ipos=bin[i]->start;
  }
  bunch_track_0(beamline,bunch,ipos,beamline->n_elements);
}

void simple_correct_dipole_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    ipos=bin[i]->start;
  }
  bunch_track_0(beamline,bunch,ipos,beamline->n_elements);
}

void
simple_correct_jitter_indep(BEAMLINE *beamline,BIN **bin,
     int nbin,BEAM *bunch0,BEAM *bunch1,BEAM *workbunch,int do_emitt)
{
  int ipos,i,j;
  ipos=0;


  if (do_emitt){
    bunch_track_emitt_start();  
  }
 
// wrongxxx
  bunch_join(bunch0,bunch1,1.,workbunch);

  for (i=0;i<nbin;i++){

    if (do_emitt){
      bunch_track_emitt(beamline,workbunch,ipos,bin[i]->start);
    }
    else{
      bunch_track(beamline,workbunch,ipos,bin[i]->start);
    }
    bin_read_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
  }


  for (j=0;j<nbin;j++){
    bin_correct(beamline,0,bin[j]);
  }

  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  }
  

}

void
simple_correct_jitter_0_indep(BEAMLINE *beamline,BIN **bin,int nbin,
                BEAM *bunch0,BEAM *bunch1,BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;

  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch0,ipos,bin[i]->start);
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    bunch_join_0(bunch0,bunch1,1.,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
  } 

  for (j=0;j<nbin;j++){
    bin_correct(beamline,0,bin[j]);
  }

  bunch_track_0(beamline,bunch0,ipos,beamline->n_elements);
}

void
simple_correct_emitt_indep(BEAMLINE *beamline,BIN **bin,int nbin,
			   BEAM *bunch,BEAM * /*workbunch*/)
{
  bunch_track_emitt_start();
  bunch_track_emitt(beamline,bunch,0,beamline->n_elements);
  bunch_track_emitt_end(bunch,beamline->get_length());

  for (int j=0;j<nbin;j++){
    bin_read_measure(beamline,bunch,0,bin[j]); 
    bin_correct(beamline,0,bin[j]);
  }
}

void
simple_correct_0_indep(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
	       BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;


  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
  }

  for (j=0;j<nbin;j++){
    bin_correct(beamline,0,bin[j]);
  }

  bunch_track_0(beamline,bunch,ipos,beamline->n_elements);
}

void test_quadrupole_jitter(BEAMLINE *beamline,BEAM *bunch,double a0,double a1,
		       int nstep,int nk,char *name)
{
  int i,j,k,n,nq,ns;
  ELEMENT **element;
  //  double r[300000],p[1000],y[1000],e[1000];
  double *r,*p,*y,*e;
  double a,da,esum,esum2,emitt0,lumi;
  double sumy,sumx,sumyp,sumxp,sumy2,sumx2,tmp;
  FILE *file;
  BEAM *workbunch;

  da=pow(a1/a0,1.0/(double)(nstep-1));
  k=0;
  ns=bunch->slices;
  n=beamline->n_elements;
  nq=beamline->n_quad;
  r=(double*)xmalloc(sizeof(double)*nq*ns*2);
  p=(double*)xmalloc(sizeof(double)*nq);
  y=(double*)xmalloc(sizeof(double)*ns*2);
  e=(double*)xmalloc(sizeof(double)*nk);
  element=beamline->element;
  workbunch=bunch_remake(bunch);
  bunch_copy_0(bunch,workbunch);
  for (i=0;i<n;i++){
    if (!beamline->element[i]->is_quad()){
      element[i]->track(bunch);
    }
    else{
      bunch_copy_0(bunch,workbunch);
      element_set_offset_to(element[i],1.0,0.0);
      bunch_track_0(beamline,workbunch,i,n);
      element_set_offset_to(element[i],0.0,0.0);
      element[i]->track(bunch);
      for (j=0;j<ns;j++){
	r[nq*(j*2)+k]=workbunch->particle[j].y;
	r[nq*(j*2+1)+k]=workbunch->particle[j].yp;
      }
      k++;
      placet_printf(INFO,"%d\n",k);
    }
  }
  emitt0=emitt_y_axis(bunch);
  placet_printf(INFO,"emitt_y0=%g\n",emitt0);
  file=open_file(name);
  a=a0;
  for (i=0;i<nstep;i++){
    esum=0.0;
    esum2=0.0;
    sumy=0.0;
    sumx=0.0;
    sumy2=0.0;
    sumx2=0.0;
    sumyp=0.0;
    sumxp=0.0;
    for (k=0;k<nk;k++){
      for (j=0;j<nq;j++){
	p[j]=a*Misalignments.Gauss(1,3);
      }
      matrix_mult(r,2*ns,nq,p,nq,1,y);
      for (j=0;j<ns;j++){
	bunch->particle[j].y=y[2*j];
	bunch->particle[j].yp=y[2*j+1];
      }
      e[k]=emitt_y_axis(bunch);
      esum+=e[k];
      esum2+=e[k]*e[k];
      sumy+=fabs(bunch_get_offset_y(bunch));
      tmp=bunch_get_offset_y(bunch)/sigma_y(bunch);
      sumy2+=tmp*tmp;
      sumyp+=fabs(tmp);
#ifdef TWODIM
      sumx+=fabs(bunch_get_offset_x(bunch));
      tmp=bunch_get_offset_x(bunch)/sigma_x(bunch);
      sumx2+=tmp*tmp;
      sumxp+=fabs(tmp);
#endif
      placet_printf(INFO,"%g %g\n",e[k],esum/(double)(k+1));
    }
    lumi=0.0;
    for (j=0;j<nk;j++){
      for (k=0;k<nk;k++){
	lumi+=sqrt(2.0*emitt0/(e[j]+e[k]));
      }
    }
    lumi/=(double)(nk*nk);
    esum/=(double)nk;
    esum2/=(double)nk;
    sumy/=(double)nk;
    sumx/=(double)nk;
    sumyp/=(double)nk;
    sumxp/=(double)nk;
    sumy2/=(double)nk;
    sumx2/=(double)nk;
    fprintf(file,"%g %g %g %g %g %g %g %g %g %g\n",a,esum,
	    sqrt(std::max(0.0,esum2-esum*esum)),lumi,sumy,sumx,sumyp,sumxp,
	    sqrt(std::max(0.0,sumy2)),sqrt(std::max(0.0,sumx2)));
    a*=da;
  }
  close_file(file);
  beam_delete(workbunch);
  free(e);
  free(y);
  free(p);
  free(r);
}

void test_quadrupole_sens(BEAMLINE *beamline,BEAM *bunch,char *name)
{
  int i,j,k,n,nq,ns;
  ELEMENT **element;
  double *r,*p,*y,e;
  double emitt0, emitt0a,ea;
  FILE *file;
  BEAM *workbunch;

  k=0;
  ns=bunch->slices;
  n=beamline->n_elements;
  nq=beamline->n_quad;
  r=(double*)alloca(sizeof(double)*nq*ns*2);
  p=(double*)alloca(sizeof(double)*nq);
  y=(double*)alloca(sizeof(double)*ns*2);
  element=beamline->element;
  workbunch=bunch_remake(bunch);
  bunch_copy_0(bunch,workbunch);
  beamline->set_zero();
  for (i=0;i<n;i++){
    if (!beamline->element[i]->is_quad()){
      element[i]->track(bunch);
    }
    else{
      bunch_copy_0(bunch,workbunch);
      element_set_offset_to(element[i],1.0,0.0);
      bunch_track_0(beamline,workbunch,i,n);
      element_set_offset_to(element[i],0.0,0.0);
      element[i]->track(bunch);
      for (j=0;j<ns;j++){
	r[nq*(j*2)+k]=workbunch->particle[j].y;
	r[nq*(j*2+1)+k]=workbunch->particle[j].yp;
      }
      k++;
      placet_printf(INFO,"%d\n",k);
    }
  }
  emitt0=emitt_y(bunch);
  emitt0a=emitt_y_axis(bunch);
  placet_printf(INFO,"emitt_y0=%g\n",emitt0);
  file=open_file(name);
  for (i=0;i<nq;i++){
    for (j=0;j<nq;j++){
      if (j==i){
	p[j]=1.0;
      }
      else{
	p[j]=0.0;
      }
    }
    matrix_mult(r,2*ns,nq,p,nq,1,y);
    for (j=0;j<ns;j++){
      bunch->particle[j].y=y[2*j];
      bunch->particle[j].yp=y[2*j+1];
    }
    e=emitt_y(bunch);
    ea=emitt_y_axis(bunch);
    fprintf(file,"%d %g %g %g %g\n",i,e,(e-emitt0)/emitt0,ea,
  	    (ea-emitt0a)/emitt0a);
  }
  close_file(file);
  beam_delete(workbunch);
}

void
free_bin_fill_new(BEAMLINE *beamline,double *quad0,double *quad1,double *quad2,
		  BEAM *bunch0,BEAM *workbunch,BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<bin_number;i++){

    quadrupoles_set(beamline,quad0);
    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bin_response(beamline,bunch0,workbunch,0,bin[i]);
    
    quadrupoles_set(beamline,quad1);
    bin_response(beamline,bunch0,workbunch,1,bin[i]);

    quadrupoles_set(beamline,quad2);
    bin_response(beamline,bunch0,workbunch,2,bin[i]);
    
    bin_finish_new(bin[i],1);
    ipos=bin[i]->start;
    placet_printf(INFO,"bin: %d\n",i);
  }
}

void free_bin_fill(BEAMLINE *beamline,
		   double *quad0,double *quad1,double *quad2,
		   BEAM *bunch0,BEAM *workbunch,BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<bin_number;i++){

    quadrupoles_set(beamline,quad0);
    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bin_response(beamline,bunch0,workbunch,0,bin[i]);
    
    quadrupoles_set(beamline,quad1);
    bin_response(beamline,bunch0,workbunch,1,bin[i]);

    quadrupoles_set(beamline,quad2);
    bin_response(beamline,bunch0,workbunch,2,bin[i]);
    
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
}

void train_bin_fill(BEAMLINE *beamline,
		    BEAM *bunch0,BEAM *workbunch,BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<bin_number;i++){

    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bin_response_train(beamline,bunch0,workbunch,0,bin[i]);
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
}

void free_bin_fill_dipole(BEAMLINE *beamline,
		     BEAM *bunch0,BEAM *workbunch,
		     BEAM *bunch1,BEAM *workbunch1,
		     BEAM *bunch2,BEAM *workbunch2,
		     BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<bin_number;i++){

    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bin_response_dipole(beamline,bunch0,workbunch,0,bin[i]);
    
    bunch_track(beamline,bunch1,ipos,bin[i]->start);
    bin_response_dipole(beamline,bunch1,workbunch1,1,bin[i]);

    if (bunch2==NULL) {
      bin_response_dipole(beamline,bunch1,workbunch1,2,bin[i]);
    }
    else {
      bunch_track(beamline,bunch2,ipos,bin[i]->start);
      bin_response_dipole(beamline,bunch2,workbunch2,2,bin[i]);
    }
    
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
}

void free_correct_new(BEAMLINE *beamline,
		      double *quad0,double *quad1,double *quad2,
		      double /*position1*/[],double /*position2*/[],
		      BIN **bin,int bin_number,BEAM *bunch0,BEAM *workbunch,
		      int do_emitt)
{
  int i,ipos,j;
  ipos=0;
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track_test(beamline,bunch0,ipos,bin[i]->start);
    }
    for(j=0;j<1;j++){
      bunch_copy_0(bunch0,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      
      quadrupoles_bin_set(beamline,bin[i],quad1);
      bunch_copy_0(bunch0,workbunch);
      bin_measure(beamline,workbunch,1,bin[i]);
      
      quadrupoles_bin_set(beamline,bin[i],quad2);
      bunch_copy_0(bunch0,workbunch);
      bin_measure(beamline,workbunch,2,bin[i]);
      
      bin_correct(beamline,1,bin[i]);
      quadrupoles_bin_set(beamline,bin[i],quad0);
    }
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track_test(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void free_correct_jitter_new(BEAMLINE *beamline,
			     double *quad0,double *quad1,double *quad2,
			     double position1[],double position2[],
			     BIN **bin,int bin_number,BEAM *bunch0,
			     BEAM *bunch1,BEAM *workbunch,int do_emitt)
{
  int i,ipos;
  ipos=0;
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track_0(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    
    //    bunch_copy_0(bunch0,workbunch);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    
    quadrupoles_bin_set(beamline,bin[i],quad1);
    quadrupoles_bin_set_position_error(beamline,bin[i],position1,1);
    //    bunch_copy_0(bunch0,workbunch);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,1,bin[i]);
    
    quadrupoles_bin_set(beamline,bin[i],quad2);
    quadrupoles_bin_set_position_error(beamline,bin[i],position1,-1);
    quadrupoles_bin_set_position_error(beamline,bin[i],position2,1);
    //    bunch_copy_0(bunch0,workbunch);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,2,bin[i]);
    
    quadrupoles_bin_set_position_error(beamline,bin[i],position2,-1);
    bin_correct(beamline,1,bin[i]);
    quadrupoles_bin_set(beamline,bin[i],quad0);
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track_0(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void free_correct(BEAMLINE *beamline,double *quad0,double *quad1,double *quad2,
		  BIN **bin,int bin_number,BEAM *bunch0,BEAM *workbunch,
		  int do_emitt)
{
  int i,ipos;
  ipos=0;
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track_test(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_copy_0(bunch0,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    
    quadrupoles_bin_set(beamline,bin[i],quad1);
    bunch_copy_0(bunch0,workbunch);
    bin_measure(beamline,workbunch,1,bin[i]);
    
    quadrupoles_bin_set(beamline,bin[i],quad2);
    bunch_copy_0(bunch0,workbunch);
    bin_measure(beamline,workbunch,2,bin[i]);
    
    bin_correct(beamline,1,bin[i]);
    quadrupoles_bin_set(beamline,bin[i],quad0);
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track_test(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void free_correct_one_beam(BEAMLINE *beamline,BIN **bin,int bin_number,
			   BEAM *bunch0,BEAM *workbunch,int do_emitt)
{
  int i,ipos;
  ipos=0;
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track_test(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_copy_0(bunch0,workbunch);
    bin_measure_one_beam(beamline,workbunch,bin[i]);
    bin_correct(beamline,1,bin[i]);
    
    ipos=bin[i]->start;
  }
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track_test(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void free_correct_jitter_one_beam(BEAMLINE *beamline,BIN **bin,int bin_number,
				  BEAM *bunch0,BEAM *beam1,BEAM *workbunch,
				  int do_emitt)
{
  int i,ipos;
  ipos=0;
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    bunch_track_test(beamline,beam1,ipos,bin[i]->start);
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track_test(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_copy_0(bunch0,workbunch);
    bunch_join_0(bunch0,beam1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure_one_beam(beamline,workbunch,bin[i]);
    bin_correct(beamline,1,bin[i]);
    
    ipos=bin[i]->start;
  }
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track_test(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void free_correct_dipole(BEAMLINE *beamline,BIN **bin,int bin_number,
			 BEAM *bunch0,BEAM *workbunch,
			 BEAM *bunch1,BEAM *workbunch1,
			 int do_emitt)
{
  int i,ipos;
  ipos=0;
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track_0(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_copy_0(bunch0,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    
    bunch_copy_0(bunch1,workbunch1);
    bin_measure(beamline,workbunch1,1,bin[i]);

    bunch_copy_0(bunch1,workbunch1);
    bin_measure(beamline,workbunch1,2,bin[i]);

    bin_correct(beamline,1,bin[i]);
    
    ipos=bin[i]->start;
  }
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track_0(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void free_correct_jitter(BEAMLINE *beamline,double *quad0,double *quad1,
			 double *quad2,BIN **bin,int bin_number,BEAM *bunch0,
			 BEAM *bunch1,BEAM *workbunch,int do_emitt)
{
  int i,ipos;
  ipos=0;
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
      bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
      
      if (do_emitt){
	bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
      }
      else{
	bunch_track(beamline,bunch0,ipos,bin[i]->start);
      }
      bunch_track(beamline,bunch1,ipos,bin[i]->start);
      bunch_copy_0(bunch0,workbunch);
      bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);

      quadrupoles_bin_set(beamline,bin[i],quad1);
      bunch_copy_0(bunch0,workbunch);
      bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
      bin_measure(beamline,workbunch,1,bin[i]);

      quadrupoles_bin_set(beamline,bin[i],quad2);
      bunch_copy_0(bunch0,workbunch);
      bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
      bin_measure(beamline,workbunch,2,bin[i]);

      bin_correct(beamline,1,bin[i]);
      quadrupoles_bin_set(beamline,bin[i],quad0);

      ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  }
}

/*
  Variation of all cavity gradients
 */

void
cavity_set_gradient_list (BEAMLINE *beamline,double g [])
{
  int i;

  if (g) {
    for (i=0;i<beamline->n_elements;++i){
      if (CAVITY *cav=beamline->element[i]->cavity_ptr()) {
	cav->set_gradient(*g++);
      }
    }
  }
}

/*
  Preliminary changes to be verified
 */

void
measured_bin_fill_new(BEAMLINE *beamline,double *quad0,double *quad1,
		      double *quad2,
		      BEAM *b0,BEAM *b1,BEAM *b2,BEAM *workbunch,
		      double gl0[],double gl1[],double gl2[],
		      BIN **bin,int bin_number,double g1,double g2)
{
  int i,ipos;
  ipos=0;
  double tmp;

  tmp=errors.bpm_resolution;
  errors.bpm_resolution=0.0;
  for (i=0;i<bin_number;i++){

    if (quad0) quadrupoles_set(beamline,quad0);
    cavity_set_gradient_list(beamline,gl0);
    bunch_track_0(beamline,b0,ipos,bin[i]->start);
    bin_response_target(beamline,b0,workbunch,0,bin[i]);
    
    if (quad1) quadrupoles_set(beamline,quad1);
    cavity_set_gradient_list(beamline,gl1);
    bunch_track_gradient_0(beamline,b1,ipos,bin[i]->start,g1);
    bin_response_target(beamline,b1,workbunch,1,bin[i]);

    if (b2) {
      if (quad2) quadrupoles_set(beamline,quad2);
      cavity_set_gradient_list(beamline,gl2);
      bunch_track_gradient_0(beamline,b2,ipos,bin[i]->start,g2);
      bin_response_target(beamline,b2,workbunch,2,bin[i]);
    }
    else {
      bin_response_target(beamline,b1,workbunch,2,bin[i]);
    }
    
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
  errors.bpm_resolution=tmp;
}

void
measured_correct_new(BEAMLINE *beamline,
		     double *quad0,double *quad1,double *quad2,
		     BIN **bin,int bin_number,BEAM *b0,BEAM *b1,BEAM *b2,
		     BEAM *workbunch,int do_emitt,double jitter,
		     double gl0[],double gl1[],double gl2[],
		     double g1,double g2,int n_iter)
{
  int i,ipos,is,np,iter;
  double tmp1,tmp2,rat;
  /*
    xmeasured_correct(beamline,quad0,quad1,quad2,bin,bin_number,b0,b1,b2,
  		    workbunch,do_emitt,jitter,g1,g2);
		    return;
  */
  ipos=0;
  np=b0->slices/2;
  if (quad0) quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
/*
  Move all beam to beginning of current bin
 */    
    if (i>0){
      if (quad1) quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }
    cavity_set_gradient_list(beamline,gl1);
    bunch_track_gradient_0(beamline,b1,ipos,bin[i]->start,g1);

    if (b2) {
      if (i>0){
	if (quad2) quadrupoles_bin_set(beamline,bin[i-1],quad2);
      }
      cavity_set_gradient_list(beamline,gl2);
      bunch_track_gradient_0(beamline,b2,ipos,bin[i]->start,g2);
    }

    if (i>0) {
      if (quad0) quadrupoles_bin_set(beamline,bin[i-1],quad0);
    }
    cavity_set_gradient_list(beamline,gl0);
    if (do_emitt) {
      bunch_track_emitt(beamline,b0,ipos,bin[i]->start);
    }
    else {
      bunch_track_0(beamline,b0,ipos,bin[i]->start);
    }
    
    for (iter=0;iter<n_iter;iter++){      
      if (quad1) quadrupoles_bin_set(beamline,bin[i],quad1);
      cavity_set_gradient_list(beamline,gl1);
      bunch_copy_0(b1,workbunch);
      rat=sqrt(b0->sigma[np].r22/b0->sigma[np].r11);
      tmp1=jitter*Instrumentation.Gauss(1,3);
      tmp2=jitter*Instrumentation.Gauss(1,3)*rat;
      for (is=0;is<b1->slices;is++){
	workbunch->particle[is].y+=tmp1;
	workbunch->particle[is].yp+=tmp2;
      }
      bin_measure(beamline,workbunch,1,bin[i]);
      
      if (b2) {
	if (quad2) quadrupoles_bin_set(beamline,bin[i],quad2);
	cavity_set_gradient_list(beamline,gl2);
	bunch_copy_0(b2,workbunch);
	tmp1=jitter*Instrumentation.Gauss(1,3);
        tmp2=jitter*Instrumentation.Gauss(1,3)*rat;
	for (is=0;is<b2->slices;is++){
	  workbunch->particle[is].y+=tmp1;
	  workbunch->particle[is].yp+=tmp2;
	}
	bin_measure(beamline,workbunch,2,bin[i]);
      }
      else {
	/*scdcheck*/
	for (is=0;is<bin[i]->nbpm;is++){
	  bin[i]->b2[is]=bin[i]->b1[is];
	  bin[i]->b2_x[is]=bin[i]->b1_x[is];
	}
      }
      
      bunch_copy_0(b0,workbunch);
      tmp1=jitter*Instrumentation.Gauss(1,3);
      tmp2=jitter*Instrumentation.Gauss(1,3)*rat;
      for (is=0;is<b0->slices;is++){
	workbunch->particle[is].y+=tmp1;
	workbunch->particle[is].yp+=tmp2;
      }
      if (quad0) quadrupoles_bin_set(beamline,bin[i],quad0);
      cavity_set_gradient_list(beamline,gl0);
      bin_measure(beamline,workbunch,0,bin[i]);
      
      bin_correct(beamline,1,bin[i]);
      //    quadrupoles_bin_set(beamline,bin[i],quad0);
    }
    ipos=bin[i]->start;
  }
  if (quad0) quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt(beamline,b0,ipos,beamline->n_elements);
    bunch_track_emitt_end(b0,beamline->get_length());
  }
  else {
    bunch_track_0(beamline,b0,ipos,beamline->n_elements);
  }
}

void
measured_correct(BEAMLINE *beamline,
		 double *quad0,double *quad1,double *quad2,
		 BIN **bin,int bin_number,BEAM *b0,BEAM *b1,BEAM *b2,
		 BEAM *workbunch,int do_emitt,double jitter)
{
  measured_correct_new(beamline,quad0,quad1,quad2,bin,bin_number,b0,b1,b2,
    		       workbunch,do_emitt,jitter,NULL,NULL,NULL,0.8,0.8,1);
}

void measured_correct_n(BEAMLINE *beamline,double *quad0,double *quad1,
			double *quad2,BIN **bin,int bin_number,BEAM *b0,
			BEAM *b1,BEAM *b2,BEAM *workbunch,
			int do_emitt,double sigma)
{
  int i,ipos,ic,n=7,is;
  ipos=0;
  quadrupoles_set(beamline,quad0);
  for (i=0;i<bin_number;i++){
    
    bunch_track(beamline,b0,ipos,bin[i]->start);
    if (do_emitt){
      emitt_data.store(i,b0,beamline->get_length());
    }
    if (i>0){
      quadrupoles_bin_set_error(beamline,bin[i-1],quad1,sigma);
    }
    bunch_track_0(beamline,b1,ipos,bin[i]->start);
    if (i>0){
      quadrupoles_bin_set_error(beamline,bin[i-1],quad2,sigma);
    }
    bunch_track_0(beamline,b2,ipos,bin[i]->start);

    for (ic=0;ic<n;ic++){
      quadrupoles_bin_set_error(beamline,bin[i],quad0,sigma);
      bunch_copy_0(b0,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      
      quadrupoles_bin_set_error(beamline,bin[i],quad1,sigma);
      bunch_copy_0(b1,workbunch);
      bin_measure(beamline,workbunch,1,bin[i]);
      
      if (b2) {
	quadrupoles_bin_set_error(beamline,bin[i],quad2,sigma);
	bunch_copy_0(b2,workbunch);
	bin_measure(beamline,workbunch,2,bin[i]);
      }
      else {
	placet_cout << WARNING << "call to uninitialised value! contact developers to fix this method.." << endmsg;
	bin[i]->b2[is]=bin[i]->b1[is]; // is is uninitialised! JS 
	bin[i]->b2_x[is]=bin[i]->b1_x[is];
      }
      
      bin_correct(beamline,1,bin[i]);
      quadrupoles_bin_set_error(beamline,bin[i],quad0,sigma);
    }
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  bunch_track(beamline,b0,ipos,beamline->n_elements);
  if (do_emitt){
    emitt_data.store(bin_number,b0,beamline->get_length());
  }
}

void measured_correct_jitter(BEAMLINE *beamline,double *quad0,double *quad1,
			     double *quad2,BIN **bin,int bin_number,BEAM *b0,
			     BEAM *b0_1,BEAM *b1,BEAM *b1_1,BEAM *b2,
			     BEAM *b2_1,BEAM *workbunch,int do_emitt,
			     double /*gl0*/[],double /*gl1*/[],double /*gl2*/[],
			     double g1,double g2)
{
  int i,ipos,is;
  ipos=0;
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt) {
      bunch_track_emitt(beamline,b0,ipos,bin[i]->start);
    }
    else {
      bunch_track_0(beamline,b0,ipos,bin[i]->start);
    }
    bunch_track_0(beamline,b0_1,ipos,bin[i]->start);
    
    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }
    bunch_track_gradient_0(beamline,b1,ipos,bin[i]->start,g1);
    bunch_track_gradient_0(beamline,b1_1,ipos,bin[i]->start,g1); 
    quadrupoles_bin_set(beamline,bin[i],quad1);
    bunch_copy_0(b1,workbunch);
    bunch_join_0(b1,b1_1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,1,bin[i]);
    
    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad2);
    }
    if (b2) {
      bunch_track_gradient_0(beamline,b2,ipos,bin[i]->start,g2);
      bunch_track_gradient_0(beamline,b2_1,ipos,bin[i]->start,g2);
      quadrupoles_bin_set(beamline,bin[i],quad2);
      bunch_copy_0(b2,workbunch);
      bunch_join_0(b2,b2_1,Instrumentation.Gauss(1,3),workbunch);
      bin_measure(beamline,workbunch,2,bin[i]);
    }
    else {
      placet_cout << WARNING << "call to uninitialised value! contact developers to fix this method.." << endmsg;
      bin[i]->b2[is]=bin[i]->b1[is]; // is is uninitialised! JS
      bin[i]->b2_x[is]=bin[i]->b1_x[is];
    }

    quadrupoles_bin_set(beamline,bin[i],quad0);
    bunch_copy_0(b0,workbunch);
    bunch_join_0(b0,b0_1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    
    bin_correct(beamline,1,bin[i]);
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt(beamline,b0,ipos,beamline->n_elements);
    bunch_track_emitt_end(b0,beamline->get_length());
  }
  else {
    bunch_track_0(beamline,b0,ipos,beamline->n_elements);
  }
}

void measured_correct_jitter_n(BEAMLINE *beamline,double *quad0,double *quad1,
			  double *quad2,BIN **bin,int bin_number,BEAM *b0,
			  BEAM *b0_1,BEAM *b1,BEAM *b1_1,BEAM *b2,
			  BEAM *b2_1,BEAM *workbunch,int do_emitt,
			  double sigma)
{
  int i,ipos,j,n=7;
    ipos=0;
    quadrupoles_set(beamline,quad0);
    for (i=0;i<bin_number;i++){

      bunch_track(beamline,b0,ipos,bin[i]->start);
      bunch_track_0(beamline,b0_1,ipos,bin[i]->start);
      if (do_emitt){
	emitt_data.store(i,b0,beamline->get_length());
      }

//      if (i>0){
//	quadrupoles_bin_set_error(beamline,bin[i-1],quad1,sigma);
//      }
      bunch_track_0(beamline,b1,ipos,bin[i]->start);
      bunch_track_0(beamline,b1_1,ipos,bin[i]->start); 

//      if (i>0){
//	quadrupoles_bin_set_error(beamline,bin[i-1],quad2,sigma);
//      }
      bunch_track_0(beamline,b2,ipos,bin[i]->start);
      bunch_track_0(beamline,b2_1,ipos,bin[i]->start);

      for(j=0;j<n;j++){
	quadrupoles_bin_set_error(beamline,bin[i],quad1,sigma);
	bunch_copy_0(b1,workbunch);
	bunch_join_0(b1,b1_1,Instrumentation.Gauss(1,3),workbunch);
	bin_measure(beamline,workbunch,1,bin[i]);

	quadrupoles_bin_set_error(beamline,bin[i],quad2,sigma);
	bunch_copy_0(b2,workbunch);
	bunch_join_0(b2,b2_1,Instrumentation.Gauss(1,3),workbunch);
	bin_measure(beamline,workbunch,2,bin[i]);

	quadrupoles_bin_set(beamline,bin[i],quad0);
//	quadrupoles_bin_set_error(beamline,bin[i],quad0,sigma);
	bunch_copy_0(b0,workbunch);
	bunch_join_0(b0,b0_1,Instrumentation.Gauss(1,3),workbunch);
	bin_measure(beamline,workbunch,0,bin[i]);

	bin_correct(beamline,1,bin[i]);
      }
      ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  bunch_track(beamline,b0,ipos,beamline->n_elements);
  emitt_data.store(bin_number,b0,beamline->get_length());
}

void bpm_reset(BEAMLINE *beamline,BEAM *bunch)
{
  int i,n;
  ELEMENT **element;
  n=beamline->n_elements;
  element=beamline->element;
  bunch_track_0(beamline,bunch,0,n);
  for (i=0;i<n;i++){
    if (element[i]->is_bpm()){
      element_add_offset(element[i],element[i]->get_bpm_y_reading_exact(),0.0);
    }
  }
}

void simple_correct_jitter(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
		      BEAM *bunch1,BEAM *workbunch,int do_emitt)
{
  int ipos,i;
  ipos=0;
  if (do_emitt){
    bunch_track_emitt_start();  
  }
  for (i=0;i<nbin;i++){
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct(beamline,0,bin[i]);
  }
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void simple_correct_dipole_jitter(BEAMLINE *beamline,BIN **bin,int nbin,
			     BEAM *bunch0,BEAM *bunch1,BEAM *workbunch,
			     int do_emitt)
{
  int ipos,i;
  ipos=0;
  if (do_emitt){
    bunch_track_emitt_start();  
  }
  for (i=0;i<nbin;i++){
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct(beamline,0,bin[i]);
  }
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0,beamline->get_length());
  }
  else{
    bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void simple_correct_jitter_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			BEAM *bunch1,BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch0,ipos,bin[i]->start);
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct(beamline,0,bin[i]);
  }
  bunch_track_0(beamline,bunch0,ipos,beamline->n_elements);
}

void simple_correct_dipole_jitter_0(BEAMLINE *beamline,BIN **bin,int nbin,
			       BEAM *bunch0,BEAM *bunch1,BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch0,ipos,bin[i]->start);
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    bunch_join_0(bunch0,bunch1,Instrumentation.Gauss(1,3),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct(beamline,0,bin[i]);
  }
  bunch_track_0(beamline,bunch0,ipos,beamline->n_elements);
}

void test_no_spectrum(BEAMLINE *beamline,BEAM *bunch0,double l0,double l1,
		      int nstep,char name[],int do_girder)
{
  int i;
  FILE *file;
  double lambda,fact,a,emitt1,emitt2,emitt0,scale1,scale2;
  BEAM *bunch,*workbunch;

  bunch=bunch_remake(bunch0);
  workbunch=bunch_remake(bunch0);
  file=open_file(name);
  a=1.0;
  lambda=l0;
  fact=pow(l1/l0,1.0/(double)(nstep-1));
  beam_copy(bunch0,bunch);
  bunch_track(beamline,bunch,0,beamline->n_elements);
  emitt0=emitt_y(bunch);
  for (i=0;i<nstep;i++){
    bunch_copy_0(bunch0,bunch);
    beamline->set_zero();
    if (do_girder) {
	beamline_move_sine(beamline,a,TWOPI/lambda,0.0);
    }
    else {
	beamline_move_sine2(beamline,a,TWOPI/lambda,0.0);
    }
    bunch_track_0(beamline,bunch,0,beamline->n_elements);
    scale1=emitt_y_scale(bunch,1.06);
    emitt1=emitt_y(bunch);
    bunch_copy_0(bunch0,bunch);
    beamline->set_zero();
    if (do_girder) {
	beamline_move_sine(beamline,a,TWOPI/lambda,0.5*PI);
    }
    else {
	beamline_move_sine2(beamline,a,TWOPI/lambda,0.5*PI);
    }
    bunch_track_0(beamline,bunch,0,beamline->n_elements);
    scale2=emitt_y_scale(bunch,1.06);
    emitt2=emitt_y(bunch);
    fprintf(file,"%g %g %g %g %g %g %g\n",lambda,emitt1,emitt2,
	    sqrt(0.06*emitt0/(emitt1-emitt0)),
	    sqrt(0.06*emitt0/(emitt2-emitt0)),scale1,scale2);
    placet_printf(INFO,"%g %g %g %g %g\n",lambda,emitt1,emitt2,scale1,scale2);
    lambda*=fact;
  }
  close_file(file);
  beam_delete(workbunch);
  beam_delete(bunch);
}

void test_simple_spectrum(BEAMLINE *beamline,BIN **bin,int nbin,
		     BEAM *bunch0,double lambda1,double lambda2,int nstep,
		     char *file_name)
{
  int i,j;
  FILE *file;
  double lambda,fact,a,emitt1,emitt2,emitt0,scale1,scale2;
  BEAM *bunch,*workbunch;

  bunch=bunch_remake(bunch0);
  workbunch=bunch_remake(bunch0);
  file=open_file(file_name);
  a=1.0;
  lambda=lambda1;
  fact=pow(lambda2/lambda1,1.0/(double)(nstep-1));
  beam_copy(bunch0,bunch);
  bunch_track(beamline,bunch,0,beamline->n_elements);
  emitt0=emitt_y(bunch);
  beam_copy(bunch0,bunch);
  corr.pwgt=0.0;
  corr.w0=1.0;
  corr.w=0.0;
  errors.bpm_resolution=0.0;
  errors.quad_move_res=0.0;
  errors.quad_step_size=0.0;
  simple_bin_fill(beamline,bin,nbin,bunch,workbunch);
  for (i=0;i<nstep;i++){
    bunch_copy_0(bunch0,bunch);
    beamline->set_zero();
    beamline_move_sine(beamline,a,TWOPI/lambda,0.0);
    simple_correct_0(beamline,bin,nbin,bunch,workbunch);
    scale1=emitt_y_scale(bunch,1.06);
    emitt1=emitt_y(bunch);
    bunch_copy_0(bunch0,bunch);
    beamline->set_zero();
    beamline_move_sine(beamline,a,TWOPI/lambda,0.5*PI);
    simple_correct_0(beamline,bin,nbin,bunch,workbunch);
    scale2=emitt_y_scale(bunch,1.06);
    for (j=0;j<bunch->slices;j++){
      bunch->particle[j].y*=scale2;
      bunch->particle[j].yp*=scale2;
    }
    emitt2=emitt_y(bunch);
    fprintf(file,"%g %g %g %g %g %g %g\n",lambda,emitt1,emitt2,
	    sqrt(0.06*emitt0/(emitt1-emitt0)),
	    sqrt(0.06*emitt0/(emitt2-emitt0)),scale1,scale2);
    placet_printf(INFO,"%g %g %g %g %g\n",lambda,emitt1,emitt2,scale1,scale2);
    lambda*=fact;
  }
  close_file(file);
  beam_delete(workbunch);
  beam_delete(bunch);
}

void errors_init()
{
  errors.bpm_resolution=0.0;
  errors.quad_step_size=0.0;
  errors.quad_move_res=0.0;
  errors.do_jitter=0;
  errors.jitter_y=0.0;
  errors.jitter_x=0.0;
  errors.offset_y=0.0;
  errors.offset_x=0.0;
  errors.do_field=0;
  errors.field_res=0.0;
  errors.field_zero=0.0;
}

void align_rf(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,BEAM *tb,
	 BEAM *workbunch,BEAM *probe)
{
  int i,j,iter,ipos;

  ipos=0;
  placet_printf(INFO,"alignRF\n");
  for (i=0;i<nbin;i++){
    bunch_copy_0(bunch0,tb);
    for (j=ipos;j<bin[i]->bpm[bin[i]->nbpm-1];j++){
      if (CAVITY *cavity=beamline->element[j]->cavity_ptr()){
	for (iter=0;iter<3;iter++){
	  bunch_copy_0(tb,workbunch);
	  cavity->track_rf_0(workbunch);
	  cavity->offset.x+=cavity->get_bpm_x_reading();
	  cavity->offset.y+=cavity->get_bpm_y_reading();
	}
      }
      beamline->element[j]->track_0(tb);
    }
    bunch_copy_0(bunch0,tb);
    bunch_track_0(beamline,tb,ipos,bin[i]->start);
    // scd temp 2004
    bin_measure(beamline,tb,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    //    bin_measure_one_beam(beamline,tb,bin[i]);
    //    bin_correct(beamline,1,bin[i]);
    // scd end temp
    bunch_track(beamline,bunch0,ipos,bin[i]->quad[bin[i]->qlast-1]);
    ipos=bin[i]->quad[bin[i]->qlast-1];
  }
  bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  if (probe){
    bunch_track_line_emitt_new(beamline,probe);
  }
}

void beamline_set_zero(BEAMLINE* beamline)
{
  beamline->set_zero();
}

void beamline_survey_earth(BEAMLINE *beamline)
{
  double s=0.0;
  int i;

  for (i=0;i<beamline->n_elements;i++){
    s+=beamline->element[i]->get_length();
    beamline->element[i]->offset.y=0.5*s*s/6500e3*1e6;
    beamline->element[i]->offset.yp=0.0;
#ifdef TWODIM
    beamline->element[i]->offset.x=0.0;
    beamline->element[i]->offset.xp=0.0;
#endif
  }
}

void beamline_survey_clic(BEAMLINE *beamline,int first,int last)
{
  if (first<0) first=0;
  if (last>beamline->n_elements) last=beamline->n_elements;
  if (last<0) last=beamline->n_elements;
  for (int i=first;i<last;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_cavity() || element->is_cavity_pets()) {
      element->offset.y=survey_errors.cav_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.cav_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.cav_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.cav_error_angle_x
	*Misalignments.Gauss();
#endif
      if (CAVITY *cavity=element->cavity_ptr()) {
	  cavity->set_bpm_offset_y(survey_errors.cav_error_realign*Misalignments.Gauss());
	  cavity->set_bpm_offset_x(survey_errors.cav_error_realign_x*Misalignments.Gauss());
	  cavity->set_dipole_kick_y(survey_errors.cav_error_dipole_y*Misalignments.Gauss());
	  cavity->set_dipole_kick_x(survey_errors.cav_error_dipole_x*Misalignments.Gauss());
      }
    } else if (QUADBPM *bpm=element->quadbpm_ptr()) {
      element->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.quad_error_angle
	*Misalignments.Gauss();
      bpm->bpm_offset.y=survey_errors.bpm_error*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.quad_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
      bpm->bpm_offset.x=survey_errors.bpm_error*Misalignments.Gauss();
#endif
    } else if (element->is_quad()) {
      element->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.quad_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.quad_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
#endif  
    } else if (element->is_bpm()) {
      element->offset.y=survey_errors.bpm_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.bpm_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.bpm_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.bpm_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.bpm_roll*Misalignments.Gauss();
#endif
    } else if (element->is_sbend()) {
      element->offset.y=survey_errors.sbend.y*Misalignments.Gauss();
      element->offset.yp=survey_errors.sbend.yp*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.sbend.x*Misalignments.Gauss();
      element->offset.xp=survey_errors.sbend.xp*Misalignments.Gauss();
      element->offset.roll=survey_errors.sbend.roll*Misalignments.Gauss();
#endif
    } else if (element->is_dipole() || element->is_drift()) {
      if (DIPOLE *dipole=element->dipole_ptr()) {
        dipole->set_strength_y(0.0);
#ifdef TWODIM
        dipole->set_strength_x(0.0);
#endif
      }
      element->offset.y=survey_errors.drift_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.drift_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.drift_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.drift_error_angle_x*Misalignments.Gauss();
#endif
    }
    element->offset.y+=zero_point;
  }
}

void beamline_survey_none(BEAMLINE * /*beamline*/)
{
  return;
}

void beamline_survey_inject(BEAMLINE *beamline)
{
  int i,j;
  double tmp,tmp_x,tmp_p,tmp_xp,l;
  for (i=0;i<beamline->n_elements;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_cavity() || element->is_cavity_pets())
    {
      l=0.0;
      tmp=survey_errors.cav_error*Misalignments.Gauss()+zero_point;
      tmp_p=survey_errors.cav_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      tmp_x=survey_errors.cav_error_x*Misalignments.Gauss();
      tmp_xp=survey_errors.cav_error_angle_x*Misalignments.Gauss();
#endif
      for (j=0;j<NSTEP;j++){
	beamline->element[i+j]->offset.y=tmp+survey_errors.piece_error
	  *Misalignments.Gauss();
	beamline->element[i+j]->offset.yp=tmp_p
	  +survey_errors.piece_error_angle*Misalignments.Gauss();
#ifdef TWODIM
	beamline->element[i+j]->offset.x=tmp_x
	  +survey_errors.piece_error_x*Misalignments.Gauss();
	beamline->element[i+j]->offset.xp=tmp_xp
	  +survey_errors.piece_error_angle_x*Misalignments.Gauss();
#endif
	l+=beamline->element[i+j]->get_length();
      }
      tmp=-0.5*l;
      for (j=0;j<NSTEP;j++){
	tmp+=0.5*beamline->element[i]->get_length();
	beamline->element[i]->offset.y+=tmp*tmp_p;
#ifdef TWODIM
	beamline->element[i]->offset.x+=tmp*tmp_xp;
#endif
	tmp+=0.5*beamline->element[i]->get_length();
	i++;
      }
      i--;
    } else if (QUADBPM *bpm=element->quadbpm_ptr()) {
      bpm->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      bpm->offset.yp=survey_errors.quad_error_angle*Misalignments.Gauss();
      bpm->bpm_offset.y=survey_errors.bpm_error*Misalignments.Gauss();
#ifdef TWODIM
      bpm->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
      bpm->offset.xp=survey_errors.quad_error_angle_x*Misalignments.Gauss();
      bpm->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
      bpm->bpm_offset.x=survey_errors.bpm_error*Misalignments.Gauss();
#endif
      bpm->offset.y+=zero_point;
    } else if (element->is_quad()) {
      beamline->element[i]->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      beamline->element[i]->offset.yp=survey_errors.quad_error_angle
	*Misalignments.Gauss();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
      beamline->element[i]->offset.xp=survey_errors.quad_error_angle_x
	*Misalignments.Gauss();
      beamline->element[i]->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
#endif
      beamline->element[i]->offset.y+=zero_point;
    } else if (element->is_sbend()) {
      element->offset.y=survey_errors.sbend.y*Misalignments.Gauss();
      element->offset.yp=survey_errors.sbend.yp*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.sbend.x*Misalignments.Gauss();
      element->offset.xp=survey_errors.sbend.xp*Misalignments.Gauss();
      element->offset.roll=survey_errors.sbend.roll*Misalignments.Gauss();
#endif
    } else if (element->is_drift()) {
      beamline->element[i]->offset.y=survey_errors.bpm_error*Misalignments.Gauss();
      beamline->element[i]->offset.yp=survey_errors.bpm_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.bpm_error_x*Misalignments.Gauss();
      beamline->element[i]->offset.xp=survey_errors.bpm_error_angle_x*Misalignments.Gauss();
      beamline->element[i]->offset.roll=survey_errors.bpm_roll*Misalignments.Gauss();
#endif
      beamline->element[i]->offset.y+=zero_point;
    } else if (element->is_dipole() || element->is_drift()) {
      if (DIPOLE *dipole=element->dipole_ptr()) {
        dipole->set_strength_y(0.0);
        dipole->set_strength_x(0.0);
      }
      beamline->element[i]->offset.y=survey_errors.drift_error*Misalignments.Gauss();
      beamline->element[i]->offset.yp=survey_errors.drift_error_angle
	*Misalignments.Gauss();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.drift_error_x*Misalignments.Gauss();
      beamline->element[i]->offset.xp=survey_errors.drift_error_angle_x
	*Misalignments.Gauss();
#endif
      beamline->element[i]->offset.y+=zero_point;
    }
  }
}

void beamline_survey_inject2(BEAMLINE *beamline)
{
  int i,j,j1;
  double tmp,tmp_x,tmp_p,tmp_xp,l;
  for (i=0;i<beamline->n_elements;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_cavity() || element->is_cavity_pets()) {
      l=0.0;
      tmp=survey_errors.cav_error*Misalignments.Gauss()+zero_point;
      tmp_p=survey_errors.cav_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      tmp_x=survey_errors.cav_error_x*Misalignments.Gauss();
      tmp_xp=survey_errors.cav_error_angle_x*Misalignments.Gauss();
#endif
      j1=0; j=0;
      while (j<NSTEP){
	beamline->element[i+j1]->offset.y=tmp+survey_errors.piece_error*Misalignments.Gauss();
	beamline->element[i+j1]->offset.yp=tmp_p+survey_errors.piece_error_angle*Misalignments.Gauss();
#ifdef TWODIM
	beamline->element[i+j1]->offset.x=tmp_x+survey_errors.piece_error_x*Misalignments.Gauss();
	beamline->element[i+j1]->offset.xp=tmp_xp+survey_errors.piece_error_angle_x*Misalignments.Gauss();
#endif
	l+=beamline->element[i+j1]->get_length();
	if (beamline->element[i+j1]->is_cavity() || beamline->element[i+j1]->is_cavity_pets()) {
	  j++;
	}
	j1++;
      }
      j1=0; j=0;
      tmp=-0.5*l;
      while (j<NSTEP){
	tmp+=0.5*element->get_length();
	element->offset.y+=tmp*tmp_p;
#ifdef TWODIM
	element->offset.x+=tmp*tmp_xp;
#endif
	tmp+=0.5*element->get_length();
	if (element->is_cavity() || element->is_cavity_pets()) {
	  j++;
	}
	i++;
      }
      i--;
    } else if (QUADBPM *bpm=element->quadbpm_ptr()) {
      element->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.quad_error_angle*Misalignments.Gauss();
      bpm->bpm_offset.y=survey_errors.bpm_error*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.quad_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
      bpm->bpm_offset.x=survey_errors.bpm_error*Misalignments.Gauss();
#endif
      element->offset.y+=zero_point;
    } else if (element->is_quad()) {
      element->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.quad_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.quad_error_angle_x
	*Misalignments.Gauss();
      element->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
#endif
      element->offset.y+=zero_point;
    } else if (element->is_sbend()) {
      element->offset.y=survey_errors.sbend.y*Misalignments.Gauss();
      element->offset.yp=survey_errors.sbend.yp*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.sbend.x*Misalignments.Gauss();
      element->offset.xp=survey_errors.sbend.xp*Misalignments.Gauss();
      element->offset.roll=survey_errors.sbend.roll*Misalignments.Gauss();
#endif
    } else if (element->is_bpm()) {
      element->offset.y=survey_errors.bpm_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.bpm_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.bpm_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.bpm_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.bpm_roll*Misalignments.Gauss();
#endif
      element->offset.y+=zero_point;
    } else if (element->is_dipole() || element->is_drift()) {
      if (DIPOLE *dipole=element->dipole_ptr()) {
        dipole->set_strength_y(0.0);
        dipole->set_strength_x(0.0);
      }

      element->offset.y=survey_errors.drift_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.drift_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.drift_error_x*Misalignments.Gauss();
      element->offset.xp=survey_errors.drift_error_angle_x*Misalignments.Gauss();
#endif
      element->offset.y+=zero_point;
    }
  }
}

void survey_init_atl(double time)
{
  survey_errors.time=time;
}

void beamline_survey_atl(BEAMLINE *beamline)
{
  beamline->set_zero();
  beamline_move_atl(beamline,0.5e-6,survey_errors.time,true);
}

void beamline_survey_atl_move(BEAMLINE *beamline)
{
  beamline_move_atl(beamline,0.5e-6,survey_errors.time,true);
}

void beamline_survey_atl_2(BEAMLINE *beamline)
{
  beamline_move_atl(beamline,0.5e-6,survey_errors.time,false);
}

void beamline_survey_atlzero_2(BEAMLINE *beamline)
{
  beamline->set_zero();
  beamline_move_atl(beamline,0.5e-6,survey_errors.time,false);
}

void beamline_survey_nlc(BEAMLINE *beamline)
{
  int i,j;
  static int first=1;
  for (i=0;i<beamline->n_elements;i++){
    ELEMENT *element=beamline->element[i];
#ifdef TWODIM
    element->offset.roll=0.0;
#endif
    if (element->is_cavity() || element->is_cavity_pets()) {
      element->offset.x=survey_errors.cav_error_x*Misalignments.Gauss();
      element->offset.y=survey_errors.cav_error*Misalignments.Gauss();
      element->offset.xp=survey_errors.cav_error_angle_x*Misalignments.Gauss();
      element->offset.yp=survey_errors.cav_error_angle*Misalignments.Gauss();
      if (CAVITY *cavity=element->cavity_ptr()) {
        cavity->set_bpm_offset_x(survey_errors.cav_error_realign_x*Misalignments.Gauss());
        cavity->set_bpm_offset_y(survey_errors.cav_error_realign*Misalignments.Gauss());
	cavity->set_dipole_kick_x(survey_errors.cav_error_dipole_x*Misalignments.Gauss());
	cavity->set_dipole_kick_y(survey_errors.cav_error_dipole_y*Misalignments.Gauss());
      }
    } else if (QUADBPM *bpm=element->quadbpm_ptr()) {
      element->offset.y=survey_errors.quad_error*Misalignments.Gauss();
      bpm->bpm_offset.y=survey_errors.bpm_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.quad_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.quad_error*Misalignments.Gauss();
      element->offset.xp=survey_errors.quad_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
      bpm->bpm_offset.x=survey_errors.bpm_error_x*Misalignments.Gauss();
#endif
      if (first) first=0;
    } else if (element->is_quad()) {
      if (!first){
	element->offset.y=survey_errors.quad_error*Misalignments.Gauss();
	element->offset.yp=survey_errors.quad_error_angle*Misalignments.Gauss();
#ifdef TWODIM
	element->offset.x=survey_errors.quad_error_x*Misalignments.Gauss();
	element->offset.roll=survey_errors.quad_roll*Misalignments.Gauss();
	element->offset.xp=survey_errors.quad_error_angle_x*Misalignments.Gauss();
#endif
	j=i-1;
	while (j>=0) {
	  if (beamline->element[j]->is_bpm()) {
	    beamline->element[j]->offset.y=element->offset.y+survey_errors.bpm_error*Misalignments.Gauss();
	    element->offset.yp=survey_errors.bpm_error_angle*Misalignments.Gauss();
#ifdef TWODIM
	    beamline->element[j]->offset.x=element->offset.x+survey_errors.bpm_error_x*Misalignments.Gauss();
	    element->offset.xp=survey_errors.bpm_error_angle_x*Misalignments.Gauss();
#endif
	    break;
	  }
	  j--;
	}
      } else {
	first=0;
	element->offset.y=0.0;
#ifdef TWODIM
	element->offset.x=0.0;
#endif
      }
    } else if (element->is_bpm()) {
      element->offset.y=survey_errors.bpm_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.bpm_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.bpm_error*Misalignments.Gauss();
      element->offset.xp=survey_errors.bpm_error_angle_x*Misalignments.Gauss();
      element->offset.roll=survey_errors.bpm_roll*Misalignments.Gauss();
#endif
    } else if (element->is_sbend()) {
      element->offset.y=survey_errors.sbend.y*Misalignments.Gauss();
      element->offset.yp=survey_errors.sbend.yp*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.sbend.x*Misalignments.Gauss();
      element->offset.xp=survey_errors.sbend.xp*Misalignments.Gauss();
      element->offset.roll=survey_errors.sbend.roll*Misalignments.Gauss();
#endif
    } else if (element->is_dipole() || element->is_drift()) {
      if (DIPOLE *dipole=element->dipole_ptr()) {
        dipole->set_strength_y(0.0);
        dipole->set_strength_x(0.0);
      }
      element->offset.y=survey_errors.drift_error*Misalignments.Gauss();
      element->offset.yp=survey_errors.drift_error_angle*Misalignments.Gauss();
#ifdef TWODIM
      element->offset.x=survey_errors.drift_error*Misalignments.Gauss();
      element->offset.xp=survey_errors.drift_error_angle_x*Misalignments.Gauss();
#endif
    }
    element->offset.y+=zero_point;
  }
}

void beamline_survey_test_clic(BEAMLINE *beamline)
{
  beamline_survey_clic(beamline,0,-1);
  beamline_move_atl(beamline,0.5e-6,survey_errors.time,true);
}

void beamline_survey_test_nlc(BEAMLINE *beamline)
{
  beamline_survey_nlc(beamline);
  beamline_move_atl(beamline,0.5e-6,survey_errors.time,true);
}

void bunch_test_plot(BEAM *bunch,char *name)
{
  FILE *file;
  int i;

  file=open_file(name);
  for (i=0;i<bunch->slices;i++){
    fprintf(file,"%d %g %g %g\n",i,bunch->z_position[i],
	    bunch->drive_data->af[i],bunch->drive_data->bf[i]);
  }
  close_file(file);
}

double
test_simple_correction(BEAMLINE *beamline,BIN **bin,int nbin,
		       BEAM *bunch0,BEAM *probe,int niter,
		       void (*survey)(BEAMLINE*),char *name,const char *format)
{
    BEAM *tb,*tb1,*workbunch,*p_b=NULL;
    int i,j,do_emitt,do_jitter=0;
    double esum=0.0,emitt0,esum2=0.0,offset_y;

    do_jitter=errors.do_jitter;
    offset_y=errors.jitter_y;
    placet_printf(INFO,"do_jitter=%d\n",do_jitter);
    if (name==NULL){
	do_emitt=0;
    }
    else{
	do_emitt=1;
	emitt_data.store_init(beamline->n_quad);
    }
    tb=bunch_remake(bunch0);
    if (do_jitter){
	tb1=bunch_remake(bunch0);
    }
    workbunch=bunch_remake(bunch0);
    if (probe!=NULL){
	p_b=bunch_remake(probe);
    }
    bunch_copy_0(bunch0,tb);
    if (nbin<0) {
	nbin=-nbin;
    }
    else {
	simple_bin_fill(beamline,bin,nbin,tb,workbunch);
    }
    if (do_emitt==0) {
	beam_copy(bunch0,tb);
	bunch_track(beamline,tb,0,beamline->n_elements);
    }
    for (i=0;i<niter;i++){
	survey(beamline);
	/*
	  bunch_copy_0(bunch0,tb);
	  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
	*/
	if ((do_emitt)&&(probe==NULL)){
	    beam_copy(bunch0,tb);
	    if (do_jitter){
		bunch_copy_0(bunch0,tb1);
		for (j=0;j<tb1->slices;j++){
		    tb1->particle[j].y+=offset_y;
		}
		simple_correct_jitter(beamline,bin,nbin,tb,tb1,workbunch,1);
	    }
	    else{
		simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
	    }
	}
	else{
	    bunch_copy_0(bunch0,tb);
	    if (do_jitter){
		bunch_copy_0(bunch0,tb1);
		for (j=0;j<tb1->slices;j++){
		    tb1->particle[j].y+=offset_y;
		}
		simple_correct_jitter_0(beamline,bin,nbin,tb,tb1,workbunch);
	    }
	    else{
		simple_correct_0(beamline,bin,nbin,tb,workbunch);
	    }
	}
	emitt0=emitt_y(tb);
	if (probe!=NULL){
	    beam_copy(probe,p_b);
	    if (do_emitt){
		bunch_track_emitt_start();
		bunch_track_emitt(beamline,p_b,0,beamline->n_elements);
		bunch_track_emitt_end(p_b,beamline->get_length());
	    }
	    else{
		bunch_track_0(beamline,p_b,0,beamline->n_elements);
	    }
	    emitt0=emitt_y(p_b);
	}
	esum+=emitt0;
	esum2+=emitt0*emitt0;
	placet_printf(INFO,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
	       sqrt(std::max(0.0,esum2/(double)(i+1)-esum*esum
			/(double)((i+1)*(i+1)))));
    }
    esum/=(double)niter;
    esum2/=(double)niter;
    placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	   sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
// scd tmp
    // if (p_b){
    // 	bunch_save("bunchlong.dat",p_b);
    // }else{
    // 	bunch_save("bunchlong.dat",tb);
    // }
//  bunch_test_plot(tb,"be.dat");
    if (do_emitt){
      emitt_data.print(name,format);
      emitt_data.store_delete();
    }
    if (p_b){
	beam_delete(p_b);
    }
    if (do_jitter){
	beam_delete(tb1);
    }
    beam_delete(workbunch);
    beam_delete(tb);
    return esum;
}

double
test_simple_correction_indep(BEAMLINE *beamline,BIN **bin,int nbin,
			     BEAM *bunch0,BEAM *probe,int niter,
			     void (*survey)(BEAMLINE*),char *name,const char *format,char *name2)
{
  BEAM *tb,*tb1,*workbunch,*p_b=NULL;
  int i,j,do_emitt,do_jitter=0;
  int iem=0; 
  double esum=0.0,emitt0,esum2=0.0;
  double offset_y,offset_y2,offset_x,offset_x2;
  FILE *file2;

  do_jitter=errors.do_jitter;  
  placet_printf(INFO,"do_jitter=%d\n",do_jitter);

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  if (probe!=NULL){
    p_b=bunch_remake(probe);
  }
  bunch_copy_0(bunch0,tb);
  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }

  if(name2!=NULL){
    file2=open_file(name2);
  }


  for (i=0;i<niter;i++){
    survey(beamline);

    if (do_jitter){
      offset_y=errors.jitter_y*Instrumentation.Gauss(1,3);
      offset_y2=errors.jitter_y*Instrumentation.Gauss(1,3);      
      offset_x=errors.jitter_x*Instrumentation.Gauss(1,3);
      offset_x2=errors.jitter_x*Instrumentation.Gauss(1,3);   
    }

    if ((do_emitt)&&(probe==NULL)){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb->slices;j++){
	  tb1->particle[j].y+=offset_y2;
          tb->particle[j].y+=offset_y;
	  tb1->particle[j].x+=offset_x2;
          tb->particle[j].x+=offset_x;
	}

	simple_correct_jitter_indep(beamline,bin,nbin,tb,tb1,workbunch,1);
      }
      else{
	simple_correct_emitt_indep(beamline,bin,nbin,tb,workbunch);
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){ 
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb->slices;j++){
	  tb1->particle[j].y+=offset_y2;
          tb->particle[j].y+=offset_y;
	  tb1->particle[j].y+=offset_x2;
          tb->particle[j].y+=offset_x;
	}
	simple_correct_jitter_0_indep(beamline,bin,nbin,tb,tb1,workbunch);
      }
      else{
	simple_correct_0_indep(beamline,bin,nbin,tb,workbunch);
      }
    }

    emitt0=emitt_y(tb);

    if (probe!=NULL){
      beam_copy(probe,p_b);
      if (do_emitt){
  	bunch_track_emitt_start();
        bunch_track_emitt(beamline,p_b,0,beamline->n_elements);
        bunch_track_emitt_end(p_b,beamline->get_length());
      }
      else{
        bunch_track_0(beamline,p_b,0,beamline->n_elements);
      }
      emitt0=emitt_y(p_b);
    }

    iem++;
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
	   sqrt(std::max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));

    if(name2!=NULL){
      fprintf(file2,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
       sqrt(std::max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));
    }

  }
  if (iem > 0) {
    esum/=(double)iem;
    esum2/=(double)iem;
    placet_printf(INFO,"emitt= %g +/- %g\n",esum,
		  sqrt(std::max(0.0,esum2-esum*esum)/(double)iem));
  }

  if(name2!=NULL){
    close_file(file2);
  }
  
// scd tmp
// if (p_b){
//   bunch_save("bunchlong.dat",p_b);
// }else{
//   bunch_save("bunchlong.dat",tb);
// }
//  bunch_test_plot(tb,"be.dat");
  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (p_b){
    beam_delete(p_b);
  }
  if (do_jitter){
    beam_delete(tb1);
  }
  beam_delete(workbunch);
  beam_delete(tb);
  return esum;
}

double
test_simple_correction_indep_list(BEAMLINE *beamline,BIN **bin,int nbin,
				  BEAM *bunch0,
				  BEAM **probe,double *wgt0,int nbeam,
				  int niter,
				  void (*survey)(BEAMLINE*),char *name, const char *format,
				  char *name2)
{
  BEAM *tb,*workbunch,**p_b=NULL;
  int i,j,do_emitt;
  int iem=0; 
  double esum=0.0,emitt0,esum2=0.0;
  double wgt[nbeam],w;
  FILE *file2;

  wgt[0]=wgt0[0];
  for (i=1;i<nbeam;i++){
      wgt[i]=wgt0[i]+wgt[i-1];
  }
  for (i=0;i<nbeam-1;i++){
      wgt[i]/=wgt[nbeam-1];
  }
  wgt[nbeam-1]=1.0;

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb=bunch_remake(bunch0);
  workbunch=bunch_remake(bunch0);
  p_b=(BEAM**)alloca(sizeof(BEAM*)*nbeam);
  for (i=0;i<nbeam;i++){
    p_b[i]=bunch_remake(probe[i]);
  }
  bunch_copy_0(bunch0,tb);
  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }

  if(name2!=NULL){
    file2=open_file(name2);
  }

  for (i=0;i<niter;i++){
    survey(beamline);

    if (do_emitt){
      w=Misalignments.Uniform();
      j=0;
      while (w>wgt[j]) j++;
      placet_printf(INFO,"BEAM no %d\n",j);
      beam_copy(probe[j],p_b[j]);
      simple_correct_emitt_indep(beamline,bin,nbin,p_b[j],workbunch);
    }
    emitt0=emitt_y(p_b[j]);

    iem++;
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
	   sqrt(std::max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));

    if(name2!=NULL){
      fprintf(file2,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
       sqrt(std::max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));
    }

  }
  if (iem > 0) {
    esum/=(double)iem;
    esum2/=(double)iem;
    placet_printf(INFO,"emitt= %g +/- %g\n",esum,
		  sqrt(std::max(0.0,esum2-esum*esum)/(double)iem));
  }

  if(name2!=NULL){
    close_file(file2);
  }
  

  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  for (i=0;i<nbeam;i++){
      beam_delete(p_b[i]);
  }
  beam_delete(workbunch);
  beam_delete(tb);
  return esum;
}

double
test_simple_correction_dipole(BEAMLINE *beamline,BIN **bin,int nbin,
			      BEAM *bunch0,BEAM *probe,int niter,
			      void (*survey)(BEAMLINE*),char *name,const char *format)
{
  BEAM *tb,*tb1,*workbunch,*p_b=NULL;
  int i,j,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0,offset_y;

  do_jitter=errors.do_jitter;
  offset_y=errors.jitter_y;
  placet_printf(INFO,"do_jitter=%d\n",do_jitter);
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  if (probe!=NULL){
    p_b=bunch_remake(probe);
  }
  bunch_copy_0(bunch0,tb);
  simple_bin_fill_dipole(beamline,bin,nbin,tb,workbunch);
  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    if ((do_emitt)&&(probe==NULL)){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_dipole_jitter(beamline,bin,nbin,tb,tb1,workbunch,1);
      }
      else{
	simple_correct_dipole_emitt(beamline,bin,nbin,tb,workbunch);
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_dipole_jitter_0(beamline,bin,nbin,tb,tb1,workbunch);
      }
      else{
	simple_correct_dipole_0(beamline,bin,nbin,tb,workbunch);
      }
    }
    emitt0=emitt_y(tb);
    if (probe!=NULL){
      beam_copy(probe,p_b);
      if (do_emitt){
	bunch_track_emitt_start();
	bunch_track_emitt(beamline,p_b,0,beamline->n_elements);
	bunch_track_emitt_end(p_b,beamline->get_length());
      }
      else{
	bunch_track_0(beamline,p_b,0,beamline->n_elements);
      }
      emitt0=emitt_y(p_b);
    }
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
	   sqrt(std::max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
// scd tmp
// if (p_b){
//   bunch_save("bunchlong.dat",p_b);
// }else{
//   bunch_save("bunchlong.dat",tb);
// }
//  bunch_test_plot(tb,"be.dat");
  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (p_b){
    beam_delete(p_b);
  }
  if (do_jitter){
    beam_delete(tb1);
  }
  beam_delete(workbunch);
  beam_delete(tb);
  return esum;
}

void bump_correct_long(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP *bump[],
		       int loop[],BEAM *tb,BEAM *workbunch,BEAM *testbunch,
		       int do_emitt)
{
  int ib,j,i=0;
  double s[2];
  if (do_emitt){
    bunch_track_emitt_start();
  }
  placet_printf(INFO,"BPM_res = %g\n",errors.bpm_resolution);
  beam_copy(tb,workbunch);
  if (loop[0]>=0){
    simple_correct_range(beamline,bin,bump[0]->start_bin,workbunch,testbunch,
			 0,bump[0]->stop);
  }
  else{
    if (do_emitt){
      simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
    }
    else{
      simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
    }
    return;
  }
  ib=0;
  while (loop[ib]>=0){
    if (do_emitt){
      bunch_track_emitt(beamline,tb,i,bump[ib]->start);
    }
    else{
      for (;i<bump[ib]->start;i++){
	beamline->element[i]->track(tb);
      }
    }
    for (j=0;j<bump_data.iterations;j++){
      beam_copy(tb,workbunch);
      bump_step_meas(beamline,bump[ib],bin,nbin,workbunch,testbunch);
      bump_calculate(bump[ib],testbunch,s);
      bump_set(beamline,bump[ib],0,s[0]);
      bump_set(beamline,bump[ib],1,s[1]);
    }
    if (do_emitt){
      bump_step_emitt(beamline,bump[ib],tb,workbunch);
    }
    else{
      bump_step(beamline,bump[ib],tb,workbunch);
    }
    /*
for (i=0;i<tb->slices;i++){
  placet_printf(INFO,"%d %g %g %g %g %g %g\n",i,tb->particle[i].y,tb->particle[i].yp,
	 tb->particle[i].energy,tb->sigma[i].r11,tb->sigma[i].r21,
	 tb->sigma[i].r22);
}
    */
    i=bump[ib]->stop;
    beam_copy(tb,workbunch);
    if (loop[ib+1]>=0){
      simple_correct_range(beamline,bin+bump[ib]->start_bin,
			   bump[ib+1]->start_bin-bump[ib]->start_bin,
			     workbunch,testbunch,
			   bump[ib]->stop,bump[ib+1]->stop);
    }
    else{
      simple_correct_range(beamline,bin+bump[ib]->start_bin,
			   nbin-bump[ib]->start_bin,workbunch,testbunch,
			   bump[ib]->stop,beamline->n_elements);
    }
    ib++;
  }
  if (do_emitt){
    bunch_track_emitt(beamline,tb,i,beamline->n_elements);
    bunch_track_emitt_end(tb,beamline->get_length());
  }
  else{
    for (;i<beamline->n_elements;i++){
      beamline->element[i]->track(tb);
    }
  }
  placet_printf(INFO,"emitt: %g %g\n",emitt_x(tb),emitt_y(tb));
}

void
bump_correct(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP *bump[],
	     int loop[],
	     BEAM *tb,BEAM *workbunch,BEAM *testbunch,int do_emitt)
{
  int ib,j,i=0;
  double s[2];

  if (bump_long) {
      bump_correct_long(beamline,bin,nbin,bump,loop,tb,workbunch,testbunch,
			do_emitt);
      return;
  }
  if (do_emitt){
   bunch_track_emitt_start();
  }
  placet_printf(INFO,"BPM_res = %g\n",errors.bpm_resolution);
  beam_copy(tb,workbunch);
  if (loop[0]>=0){
    simple_correct_range(beamline,bin,bump[0]->start_bin,workbunch,testbunch,
			 0,bump[0]->stop);
    }
  else{
    if (do_emitt){
      simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
    }
    else{
      simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
    }
    return;
  }
  ib=0;
  while (loop[ib]>=0){
    if (do_emitt){
      bunch_track_emitt(beamline,tb,i,bump[ib]->start);
    }
    else{
      for (;i<bump[ib]->start;i++){
	beamline->element[i]->track(tb);
      }
    }
    for (j=0;j<bump_data.iterations;j++){
      beam_copy(tb,workbunch);
      bump_step(beamline,bump[ib],workbunch,testbunch);
      bump_calculate(bump[ib],workbunch,s);
      bump_set(beamline,bump[ib],0,s[0]);
      bump_set(beamline,bump[ib],1,s[1]);
      if (bump[ib]->type==3) {
	beam_copy(tb,workbunch);
	bump_step(beamline,bump[ib],workbunch,testbunch);
      }
    }
    if (do_emitt){
      bump_step_emitt(beamline,bump[ib],tb,workbunch);
    }
    else{
      bump_step(beamline,bump[ib],tb,workbunch);
    }
    /*
for (i=0;i<tb->slices;i++){
  placet_printf(INFO,"%d %g %g %g %g %g %g\n",i,tb->particle[i].y,tb->particle[i].yp,
	 tb->particle[i].energy,tb->sigma[i].r11,tb->sigma[i].r21,
	 tb->sigma[i].r22);
}
    */
    i=bump[ib]->stop;
    beam_copy(tb,workbunch);
    if (loop[ib+1]>=0){
      simple_correct_range(beamline,bin+bump[ib]->start_bin,
			   bump[ib+1]->start_bin-bump[ib]->start_bin,
			   workbunch,testbunch,
			   bump[ib]->stop,bump[ib+1]->stop);
      }
    else{
      simple_correct_range(beamline,bin+bump[ib]->start_bin,
			   nbin-bump[ib]->start_bin,workbunch,testbunch,
			   bump[ib]->stop,beamline->n_elements);
    }
    ib++;
  }
  if (do_emitt){
    bunch_track_emitt(beamline,tb,i,beamline->n_elements);
    bunch_track_emitt_end(tb,beamline->get_length());
  }
  else{
    for (;i<beamline->n_elements;i++){
      beamline->element[i]->track(tb);
    }
  }
  placet_printf(INFO,"emitt: %g %g",emitt_x(tb),emitt_y(tb));
}

void bump_prepare(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP **bump,
		  int loop[],int btype,BEAM *bunch,BEAM *tb,BEAM *workbunch,
		  BEAM *testbunch)
{
  int i=0,j;
  while (loop[i]>=0){
    beam_copy(bunch,tb);
    // scd temp 2004
    simple_correct(beamline,bin,nbin,tb,workbunch);
    // scd end temp 2004
    bump[i]=bump_make(bunch->slices);
    bump_define(beamline,bump[i],loop[i],btype);
    beam_copy(bunch,tb);
    bunch_track(beamline,tb,0,bump[i]->start);
    if (bump_long) {
	bump_fill_long(beamline,bump[i],bin,nbin,tb,workbunch,testbunch);
    }
    else {
	bump_fill(beamline,bump[i],tb,workbunch,testbunch);
    }
    placet_printf(INFO,"bump #%d\n",i);
    for (j=0;j<nbin;j++){
      if (bin[j]->start>bump[i]->stop) break;
    }
    bump[i]->start_bin=j;
    i++;
  }
}

void bump_prepare_types(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP **bump,
			int loop[],int btype[],BEAM *bunch,BEAM *tb,
			BEAM *workbunch,BEAM *testbunch)
{
  int i=0,j;
  while (loop[i]>=0){
    beam_copy(bunch,tb);
    // scd temp 2004
        simple_correct(beamline,bin,nbin,tb,workbunch);
    // scd end temp 2004
    bump[i]=bump_make(bunch->slices);
    bump_define(beamline,bump[i],loop[i],btype[i]);
    beam_copy(bunch,tb);
    bunch_track(beamline,tb,0,bump[i]->start);
    if (bump_long) {
	bump_fill_long(beamline,bump[i],bin,nbin,tb,workbunch,testbunch);
    }
    else {
	bump_fill(beamline,bump[i],tb,workbunch,testbunch);
    }
    placet_printf(INFO,"bump #%d\n",i);
    for (j=0;j<nbin;j++){
      if (bin[j]->start>bump[i]->stop) break;
    }
    bump[i]->start_bin=j;
    i++;
  }
}

void test_simple_alignment(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
		      BEAM* testbeam,
			   int niter,void (*survey)(BEAMLINE*),char *name,const char *format,
		      double w0,double w1,int do_rf,int loop[])
{
  BEAM *tb,*tb1,*tbx,*workbunch,*probe,*probe2;
  BUMP **bumps;
  int i,j,do_emitt,do_jitter,nbin_s,do_bumps=0,btype=1;
  double esum=0.0,emitt0,esum2=0.0,offset_y;
  BIN **bin_s;

  if (loop){
    while(loop[do_bumps]>=0) do_bumps++;
  }

  if (testbeam){
    probe=bunch_remake(testbeam);
  }
  else{
    probe=NULL;
  }
  probe2=probe;
  if(loop){
    probe2=NULL;
  }

  do_jitter=errors.do_jitter;
  offset_y=errors.jitter_y;

  bin_s=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  //  beamline_bin_divide_1(beamline,1,0,bin_s,&nbin_s);
  beamline_bin_divide(beamline,1,0,bin_s,&nbin_s);
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb=bunch_remake(bunch0);
  tbx=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  corr.pwgt=w0;
  bunch_copy_0(bunch0,tb);
//wakefield_init(1,0);
  simple_bin_fill_nlc(beamline,bin,nbin,tb,workbunch);
  if (do_rf){
    corr.pwgt=w1;
    bunch_copy_0(bunch0,tb);
    simple_bin_fill(beamline,bin_s,nbin_s,tb,workbunch);
  }
//wakefield_init(1,1);
  if (loop!=NULL){
    bumps=(BUMP**)xmalloc(sizeof(BUMP*)*do_bumps);
    bump_prepare(beamline,bin_s,nbin_s,bumps,loop,btype,bunch0,tb,tbx,
		 workbunch);
  }
  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    beam_copy(bunch0,tb);
    if(testbeam){
      beam_copy(testbeam,probe);
    }
    if (do_emitt){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_nlc_jitter(beamline,bin,nbin,tb,tb1,workbunch,probe2);
      }
      else{
	/*	simple_correct_emitt(beamline,bin,nbin,tb,workbunch);*/
	if (do_rf){
	  for (j=0;j<do_rf;j++){
	    corr.pwgt=w0;
	    beam_copy(bunch0,tb);
	    simple_correct_nlc_0(beamline,bin,nbin,tb,workbunch);
	    beam_copy(bunch0,tb);
	    bpm_reset(beamline,tb);
	    beam_copy(bunch0,tb);
	    corr.pwgt=w1;
	    align_rf(beamline,bin_s,nbin_s,tb,tbx,workbunch,probe2);
	  }
	}
	else{
	  corr.pwgt=w0;
	  beam_copy(bunch0,tb);
//	  simple_correct_nlc_n(beamline,bin,nbin,tb,workbunch);
	  simple_correct_nlc(beamline,bin,nbin,tb,workbunch,probe2);
	}
	if (loop){
	  beam_copy(bunch0,tb);
	  if (probe){
	    bump_correct(beamline,bin_s,nbin_s,bumps,loop,tb,
			 workbunch,tbx,0);
	    bunch_track_line_emitt_new(beamline,probe);
	  }
	  else{
	    bump_correct(beamline,bin_s,nbin_s,bumps,loop,tb,
			 workbunch,tbx,1);
	  }
	}
	/*
	beam_copy(bunch0,tb);
	bunch_track_line_emitt_new(beamline,tb);
	*/
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter_0(beamline,bin,nbin,tb,tb1,workbunch);
      }
      else{
	simple_correct_0(beamline,bin,nbin,tb,workbunch);
      }
    }
    /*
    if (testbeam){
      beam_copy(testbeam,probe);
      bunch_track_line_emitt_new(beamline,probe);
    }
    */
    emitt0=emitt_y(tb);
    if(testbeam){
      emitt0=emitt_y(probe);
    }
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (do_jitter){
    beam_delete(tb1);
  }
  if (testbeam) {
    beam_delete(probe);
  }
  beam_delete(workbunch);
  beam_delete(tbx);
  beam_delete(tb);
  free(bin_s);
  if (loop!=NULL) {
    xfree(bumps);
  } 
}

double
test_no_correction(BEAMLINE *beamline,BEAM *bunch0,BEAM *probe,
		   int niter,void (*survey)(BEAMLINE*),char *name,const char *format)
{

  BEAM *tb,*p_b;
  int i;
  double esumy=0.0,esumx=0.0,emittx=0.0,emitty=0.0,esumy2=0.0,esumx2=0.0;

  tb=bunch_remake(bunch0);
  if (probe!=NULL){
    p_b=bunch_remake(probe);
  }
  if (name){
    emitt_data.store_init(beamline->n_quad);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    if ((i==0)||(name)){
      beam_copy(bunch0,tb);
    }
    else{
      bunch_copy_0(bunch0,tb);
    }
    if ((name)&&(probe==NULL)){
      bunch_track_line_emitt_new(beamline,tb);
    }
    else{
      if (i==0){
	bunch_track(beamline,tb,0,beamline->n_elements);
      }
      else{
	bunch_track_0(beamline,tb,0,beamline->n_elements);
      }
    }
    emitty=emitt_y(tb);
    if (probe!=NULL){
      beam_copy(probe,p_b);
      bunch_track_line_emitt_new(beamline,p_b);
      emitty=emitt_y(p_b);
    }
    esumy=(esumy*i+emitty)/(double)(i+1);
    esumy2=(esumy2*i+emitty*emitty)/(double)(i+1);
    double semy = sqrt(std::max(0.0,(esumy2-esumy*esumy)/(double)(i+1)));
#ifdef TWODIM
    emittx=emitt_x(tb);
    esumx=(esumx*i+emittx)/(double)(i+1);
    esumx2=(esumx2*i+emittx*emittx)/(double)(i+1);
    double semx = sqrt(std::max(0.0,(esumx2-esumx*esumx)/(double)(i+1)));
#endif
    placet_cout << VERBOSE << "test_no_correction" << std::endl;
    if (i>0) placet_cout << INFO << " iteration: " << i << std::endl;
    placet_cout << INFO << "emitt_x " << emittx;
    if (i>0) placet_cout << INFO << " mean " << esumx << " error of mean " << semx;
    placet_cout << std::endl;
    placet_cout << INFO << "emitt_y " << emitty;
    if (i>0) placet_cout << INFO << " mean " << esumy << " error of mean " << semy;
    placet_cout << endmsg;
  }
  if(name){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  beam_delete(tb);
  if (probe!=NULL){
    beam_delete(p_b);
  }
  return esumy;
}

//nl

double
test_no_correction_2(BEAMLINE *beamline,BEAM *bunch0,BEAM *probe,
		     int niter,int init_emit,void (*survey)(BEAMLINE*),
		     char *name,const char *format,char *name2)
{
  BEAM *tb,*p_b;
  int i,j,do_jitter=0;
  int initialise=1,close_delete=1;
  double esumy=0.0,esumx=0.0,emittx=0.0,emitty=0.0,esumy2=0.0,esumx2=0.0;
  double offset_y,offset_x;
  FILE *file2;

  tb=bunch_remake(bunch0);
  //  tb1=bunch_remake(bunch0);
  if (probe!=NULL){
    p_b=bunch_remake(probe);
  }
 
  do_jitter=errors.do_jitter;
  placet_printf(INFO,"do_jitter=%d\n",do_jitter);

  /* 
  A) init_emit = 1 (Default) : initialise, store, close and delete 
                               emitt_store data 


  B1) init_emit = 10          : only initialise and store. First call 
                               in TestNoCorrection 

  B2) init_emit = 20          : only store  

  B3) init_emit = 30          : store, close and delete. Last call 
                               in TestNoCorrection                                    
  */

  if (init_emit == 10) {
    initialise = 1;
    close_delete = 0;
  } 
  else if (init_emit == 20) {
    initialise = 0;
    close_delete = 0;
  } 
  else if (init_emit == 30) {
    initialise = 0;
    close_delete = 1;
  } 



  if (name&&initialise){
    emitt_data.store_init(beamline->n_quad);
  }

  if(name2!=NULL){
    file2=open_file(name2);
  }

  for (i=0;i<niter;i++){
    survey(beamline);
    if ((i==0)||(name)){
      beam_copy(bunch0,tb);
    }
    else{
      bunch_copy_0(bunch0,tb);
    }

    if (do_jitter){
        offset_y=errors.jitter_y*Instrumentation.Gauss(1,3);
        offset_x=errors.jitter_x*Instrumentation.Gauss(1,3);
	for (j=0;j<tb->slices;j++){
	  tb->particle[j].y+=offset_y;
	  tb->particle[j].x+=offset_x; 
	}
    }

    if ((name)&&(probe==NULL)){
      bunch_track_line_emitt_new(beamline,tb);
    }
    else{
      if (i==0){
	bunch_track(beamline,tb,0,beamline->n_elements);
      }
      else{
	bunch_track_0(beamline,tb,0,beamline->n_elements);
      }
    }
    emitty=emitt_y(tb);
 
    if (probe!=NULL){
      beam_copy(probe,p_b);
      bunch_track_line_emitt_new(beamline,p_b);
      emitty=emitt_y(p_b);
    }
    esumy=(esumy*i+emitty)/(double)(i+1);
    esumy2=(esumy2*i+emitty*emitty)/(double)(i+1);
    double semy = sqrt(std::max(0.0,(esumy2-esumy*esumy)/(double)(i+1)));
#ifdef TWODIM
    emittx=emitt_x(tb);
    esumx=(esumx*i+emittx)/(double)(i+1);
    esumx2=(esumx2*i+emittx*emittx)/(double)(i+1);
    double semx = sqrt(std::max(0.0,(esumx2-esumx*esumx)/(double)(i+1)));
#endif
    placet_cout << VERBOSE << "test_no_correction_2" << std::endl;
    if (i>0) placet_cout << INFO << " iteration: " << i << std::endl;
    placet_cout << INFO << "emitt_x " << emittx;
    if (i>0) placet_cout << INFO << " mean " << esumx << " error of mean " << semx;
    placet_cout << std::endl;
    placet_cout << INFO << "emitt_y " << emitty;
    if (i>0) placet_cout << INFO << " mean " << esumy << " error of mean " << semy;
    placet_cout << endmsg;

    if(name2!=NULL){
      fprintf(file2,"%d %g %g %g\n",i,emitty,esumy/(double)(i+1),
       sqrt(std::max(0.0,esumy2/(double)(i+1)-esumy*esumy/(double)((i+1)*(i+1)))));
    }
  }
//scd tmp
// bunch_save("bunchlong.dat",tb);
//  bunch_test_plot(tb,"be.dat");
  if(name&&close_delete){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }

  if(name2!=NULL){
    close_file(file2);
  }
  beam_delete(tb);
  if (probe!=NULL){
    beam_delete(p_b);
  }
  return esumy;
}

//

double
test_measured_correction(BEAMLINE *beamline,int /*start*/,int /*end*/,
			 double quad0[],double quad1[],double quad2[],
			 BIN **bin,int nbin,
			 BEAM *bunch0,BEAM *bunch1,BEAM *bunch2,
			 BEAM *cbunch0,BEAM *cbunch1,BEAM *cbunch2,
			 double gl0_0[],double gl1_0[],double gl2_0[],int nc,
			 double g1,double g2,
			 int niter, int /*beamline_iter*/,int bin_iter,
			 void (*survey)(BEAMLINE*),
			 double field_error,double offset_y,char *name, const char *format)
{
  BEAM *tb0,*tb1,*tb2=NULL,*tb0_1,*tb1_1,*tb2_1=NULL,*workbunch;
  BEAM *ctb0,*ctb1,*ctb2=NULL,*cworkbunch;
  int i,j,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0;
  double eps=1e-200;
  int do_field_error=0;
  int ii;
  double *gl0=NULL,*gl1=NULL,*gl2=NULL,s0,s1,s2;
  //  double quad0p[MAX_QUAD],quad1p[MAX_QUAD],quad2p[MAX_QUAD];

  if (field_error>eps) do_field_error=1;
  if (offset_y>eps) do_jitter=1;
  if (offset_y<-eps) do_jitter=-1;

  placet_printf(INFO,"do_field_error= %d; do_jitter= %d;\n",do_field_error,do_jitter);

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb0=bunch_remake(bunch0);
  tb1=bunch_remake(bunch1);
  if (bunch2) {
    tb2=bunch_remake(bunch2);
  }
  if (do_jitter<0){
    tb0_1=bunch_remake(bunch0);
    tb1_1=bunch_remake(bunch1);
    if (bunch2) {
      tb2_1=bunch_remake(bunch2);
    }
  }
  workbunch=bunch_remake(bunch0);
  if (cbunch0) {
      ctb0=bunch_remake(cbunch0);
      bunch_copy_0(cbunch0,ctb0);
      cworkbunch=bunch_remake(cbunch0);
  }
  else {
      bunch_copy_0(bunch0,tb0);
  }
  if (cbunch1) {
      ctb1=bunch_remake(cbunch1);
      bunch_copy_0(cbunch1,ctb1);
  }
  else {
      bunch_copy_0(bunch1,tb1);
  }
  if (cbunch2) {
      ctb2=bunch_remake(cbunch2);
      bunch_copy_0(cbunch2,ctb2);
  }
  else {
    if (bunch2) {
      bunch_copy_0(bunch2,tb2);
    }
  }
  if (cbunch0) {
      measured_bin_fill_new(beamline,quad0,quad1,quad2,ctb0,ctb1,ctb2,
			    cworkbunch,gl0_0,gl1_0,gl2_0,bin,nbin,g1,g2);
  }
  else {
      measured_bin_fill_new(beamline,quad0,quad1,quad2,tb0,tb1,tb2,workbunch,
			    gl0_0,gl1_0,gl2_0,bin,nbin,g1,g2);
  }
/*
  for (i=0;i<nbin;i++) {
      bin[i]->gain=1.0;
      bin[i]->dtau=0.0;
  }
*/
  if (do_emitt==0) {
    beam_copy(bunch0,tb0);
    bunch_track(beamline,tb0,0,beamline->n_elements);
  }
  if (gl0_0) {
    gl0=(double*)alloca(sizeof(double)*nc);
    gl1=(double*)alloca(sizeof(double)*nc);
    gl2=(double*)alloca(sizeof(double)*nc);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    if (gl0_0) {
      for (ii=0;ii<nc;++ii) {
	if (ii%24==0) {
	  s0=(1.0+Instrumentation.Gauss(1,3)*0.0);
	  s1=(1.0+Instrumentation.Gauss(1,3)*0.0);
	  s2=(1.0+Instrumentation.Gauss(1,3)*0.0);
	}
	gl0[ii]=gl0_0[ii]*s0;
	gl1[ii]=gl1_0[ii]*s1;
	gl2[ii]=gl2_0[ii]*s2;
      }
    }
    if (do_emitt){
      beam_copy(bunch0,tb0);
      beam_copy(bunch1,tb1);
      if (bunch2) {
	beam_copy(bunch2,tb2);
      }
      if (do_jitter){
	if (do_jitter<0) {
	  bunch_copy_0(bunch0,tb0_1);
	  bunch_copy_0(bunch1,tb1_1);
	  if (bunch2) {
	    bunch_copy_0(bunch2,tb2_1);
	  }
	  for (j=0;j<tb0->slices;j++){
	    tb0_1->particle[j].y+=offset_y;
	    tb1_1->particle[j].y+=offset_y;
	    if (bunch2) {
	      tb2_1->particle[j].y+=offset_y;
	    }
	  }
	  if (do_field_error){
	    measured_correct_jitter_n(beamline,quad0,quad1,quad2,
				      bin,nbin,tb0,tb0_1,tb1,
				      tb1_1,tb2,tb2_1,workbunch,1,
				      field_error);
	  }
	  else{
	    measured_correct_jitter(beamline,quad0,quad1,quad2,
				    bin,nbin,tb0,tb0_1,tb1,
				    tb1_1,tb2,tb2_1,workbunch,1,gl0,gl1,gl2,
				    g1,g2);
	  }
	}
	else {
	  measured_correct_new(beamline,quad0,quad1,quad2,bin,nbin,
			       tb0,tb1,tb2,workbunch,1,offset_y,gl0,gl1,gl2,
			       g1,g2,bin_iter);
	}
      }
      else{
	if (do_field_error){
	  measured_correct_n(beamline,quad0,quad1,quad2,bin,
			     nbin,tb0,tb1,tb2,workbunch,1,
			     field_error);
	}
	else{
	  measured_correct_new(beamline,quad0,quad1,quad2,bin,nbin,
			       tb0,tb1,tb2,workbunch,1,0.0,gl0,gl1,gl2,g1,g2,
			       bin_iter);
	}
      }
    }
    else{
      bunch_copy_0(bunch0,tb0);
      bunch_copy_0(bunch1,tb1);
      if (bunch2) {
	bunch_copy_0(bunch2,tb2);
      }
      if (do_jitter){
	bunch_copy_0(bunch0,tb0_1);
	bunch_copy_0(bunch1,tb1_1);
	if (bunch2) {
	  bunch_copy_0(bunch2,tb2_1);
	}
	for (j=0;j<tb0->slices;j++){
	  tb0_1->particle[j].y+=offset_y;
	  tb1_1->particle[j].y+=offset_y;
	  if (bunch2) {
	    tb2_1->particle[j].y+=offset_y;
	  }
	}
	if (do_field_error){
	  measured_correct_jitter_n(beamline,quad0,quad1,quad2,
				    bin,nbin,tb0,tb0_1,tb1,
				    tb1_1,tb2,tb2_1,workbunch,1,
				    field_error);
	}
	else{
	  measured_correct(beamline,quad0,quad1,quad2,bin,nbin,
			   tb0,tb1,tb2,workbunch,0,offset_y);
	}
      }
      else{
	if (do_field_error){
	  measured_correct_n(beamline,quad0,quad1,quad2,bin,
			     nbin,tb0,tb1,tb2,workbunch,1,
			     field_error);
	}
	else{
	  measured_correct(beamline,quad0,quad1,quad2,bin,nbin,
			   tb0,tb1,tb2,workbunch,0,0.0);
	}
      }
    }
    //scd test if needed
    //    bpm_freeze(beamline,0.0,0.0);
    emitt0=emitt_y(tb0);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  beam_delete(tb1);
  if (bunch2) {
    beam_delete(tb2);
  }
  if (cbunch1) {
    beam_delete(ctb1);
  }
  if (cbunch2) {
    beam_delete(ctb2);
  }
  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (do_jitter<0){
    beam_delete(tb0_1);
    beam_delete(tb1_1);
    if (bunch2) {
      beam_delete(tb2_1);
    }
  }
  return esum;
}

void bpm_freeze(BEAMLINE *beamline,double sx,double sy)
{
  int i,n;
  ELEMENT **element;
  n=beamline->n_elements;
  element=beamline->element;
  for(i=0;i<n;i++){
    if (element[i]->is_bpm()){
      element_add_offset(element[i],element[i]->get_bpm_y_reading_exact()+sy*Instrumentation.Gauss(1,3),0.0);
      //element[i]->bpm_ptr()->set_y_position(0.0); // BD 07/10
#ifdef TWODIM
      element_add_offset_x(element[i],element[i]->get_bpm_x_reading_exact()+sy*Instrumentation.Gauss(1,3),0.0);
      //element[i]->bpm_ptr()->set_x_position(0.0); // BD 07/10
#endif
    }
    if (QUADBPM *bpm=element[i]->quadbpm_ptr()){
      bpm->bpm_offset.y+=bpm->get_bpm_y_reading_exact()+sy*Instrumentation.Gauss(1,3);
#ifdef TWODIM
      bpm->bpm_offset.x+=bpm->get_bpm_x_reading_exact()+sx*Instrumentation.Gauss(1,3);
#endif
    }
  }
}

void bpm_freeze_bunch(BEAMLINE *beamline,double sx,double sy,int ib)
{
  int i,n;
  ELEMENT **element;
  n=beamline->n_elements;
  element=beamline->element;
  for(i=0;i<n;i++){
    if (element[i]->is_bpm()){
      element_add_offset(element[i],
			 element[i]->bpm_ptr()->bunch_signal(ib)
			 +sy*Instrumentation.Gauss(1,3),0.0);
      element[i]->bpm_ptr()->set_y_position(0.0);
#ifdef TWODIM
      element_add_offset_x(element[i],
			   element[i]->bpm_ptr()->bunch_signal_x(ib)
			   +sx*Instrumentation.Gauss(1,3),0.0);
      element[i]->bpm_ptr()->set_x_position(0.0);
#endif
    }
    if (QUADBPM *bpm=element[i]->quadbpm_ptr()){
      bpm->bpm_offset.y+=bpm->get_bpm_y_reading_exact()+sy*Instrumentation.Gauss(1,3);
#ifdef TWODIM
      bpm->bpm_offset.x+=bpm->get_bpm_x_reading_exact()+sx*Instrumentation.Gauss(1,3);
#endif
    }
  }
}

void bpm_freeze_2(BEAMLINE *beamline,BIN **bin,int nbin)
{
  int i,j;
  ELEMENT **element;
  element=beamline->element;
  for (j=0;j<nbin;j++){
    for (i=0;i<bin[j]->nbpm-1;i++){
      element_add_offset(element[bin[j]->bpm[i]],
			 (element[bin[j]->bpm[i]])->get_bpm_y_reading_exact(),0.0);
    }
  }
}

void cav_scatter(BEAMLINE *beamline,double y,double x)
{
  int i;
  for (i=0;i<beamline->n_elements;i++){
    if (CAVITY *cavity=beamline->element[i]->cavity_ptr()) {
      cavity->set_bpm_offset_y(y*Cavity.Gauss(1,3));
      cavity->set_bpm_offset_x(x*Cavity.Gauss(1,3));
    }
  }
}

void test_free_correction_new(BEAMLINE *beamline,double quad0[],double quad1[],
			      double quad2[],BIN **bin,int nbin,BEAM *bunch0,
			      BIN **bin2,int nbin2,BEAM *testbeam,
			      double jitter_y,
			      double shift,int niter,int step_iter0,
			      void (*survey)(BEAMLINE*),char *name,const char *format)
{
  BEAM *tb0,*tb0_1,*workbunch,*probe,*probe_w;
  int i,j,do_emitt,do_jitter=0,k,step_iter;
  double esum=0.0,emitt0,esum2=0.0,offset_y=1.3;
  double *position1,*position2,tmp;

  step_iter=abs(step_iter0);
  position1=(double*)alloca(sizeof(double)*beamline->n_quad);
  position2=(double*)alloca(sizeof(double)*beamline->n_quad);
  if(jitter_y>0.0){
    do_jitter=1;
    offset_y=jitter_y;
  }
  corr.pwgt=0.0;
  corr.w=1.0;
  corr.w0=1.0;
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb0=bunch_remake(bunch0);
  if (do_jitter){
    tb0_1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb0);
  free_bin_fill_new(beamline,quad0,quad1,quad2,tb0,workbunch,bin,nbin);
  if (testbeam&&bin2){
    corr.pwgt=0.0;
    corr.w=0.0;
    corr.w0=0.0;
    probe=bunch_remake(testbeam);
    probe_w=bunch_remake(testbeam);
    //    wb=bunch_remake(testbeam);
    beam_copy(testbeam,probe);
    simple_bin_fill(beamline,bin2,nbin2,probe,probe_w);
    if (step_iter0>0) {
      do_emitt=0;
    }
  }
  if (do_emitt==0) {
    beam_copy(bunch0,tb0);
    bunch_track(beamline,tb0,0,beamline->n_elements);
  }
  placet_printf(INFO,"do_emitt=%d\n",do_emitt);
  for (i=0;i<niter;i++){
    survey(beamline);
    cav_scatter(beamline,survey_errors.cav_error_realign,
		survey_errors.cav_error_realign_x);
    for (j=0;j<beamline->n_quad;j++){
      tmp=Misalignments.Gauss(1,3)*shift;
      position1[j]=tmp*(quad0[j]-quad1[j])/quad0[j];
      position2[j]=tmp*(quad0[j]-quad2[j])/quad0[j];
    }
    for(k=0;k<step_iter;k++){
      corr.pwgt=0.0;
      corr.w=1.0;
      corr.w0=1.0;
      if ((do_emitt)&&(k+1==step_iter)){
	beam_copy(bunch0,tb0);
	if (do_jitter){
	  bunch_copy_0(bunch0,tb0_1);
	  for (j=0;j<tb0->slices;j++){
	    tb0_1->particle[j].y+=offset_y;
	  }
	  free_correct_jitter_new(beamline,quad0,quad1,quad2,position1,
				  position2,bin,nbin,tb0,tb0_1,workbunch,1);
	}
	else{
	  free_correct_new(beamline,quad0,quad1,quad2,position1,position2,
			   bin,nbin,tb0,workbunch,1);
	}
      }
      else{
	bunch_copy_0(bunch0,tb0);
	if (do_jitter){
	  bunch_copy_0(bunch0,tb0_1);
	  for (j=0;j<tb0->slices;j++){
	    tb0_1->particle[j].y+=offset_y;
	  }
	  free_correct_jitter_new(beamline,quad0,quad1,quad2,position1,
				  position2,bin,nbin,tb0,tb0_1,workbunch,0);
	}
	else{
	  free_correct_new(beamline,quad0,quad1,quad2,position1,position2,
			   bin,nbin,tb0,workbunch,0);
	}
      }
      emitt0=emitt_y(tb0);
      placet_printf(INFO,"%d %g\n",k,emitt0);
      bpm_freeze_2(beamline,bin,nbin);
      if((testbeam)&&((k+1!=step_iter)||(step_iter0>0))){
	beam_copy(testbeam,probe);
	/*
	  align_rf(beamline,bin2,nbin2,probe,probe_w,wb,NULL);
	  if (k+1==step_iter) {
	  beam_copy(testbeam,probe);
	  bunch_track_line_emitt_new(beamline,probe);
	  }
	*/
	//        simple_correct_emitt(beamline,bin2,nbin2,probe,probe_w);
	bunch_track_emitt_start();
	bunch_track_emitt(beamline,probe,0,beamline->n_elements);
	bunch_track_emitt_end(probe,beamline->get_length());
	emitt0=emitt_y(probe);
	placet_printf(INFO,"%d %g\n",k,emitt0);
      }
    }
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  if (name){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (do_jitter){
    beam_delete(tb0_1);
  }
}

void test_free_correction(BEAMLINE *beamline,
			  double quad0[],double quad1[],
			  double quad2[],BIN **bin,int nbin,
			  BEAM *bunch0,
			  BIN **rf_bin,int nrf,double offset_y,
			  int niter,void (*survey)(BEAMLINE*),
			  char *name,const char *format)
{
  BEAM *tb0,*tb0_1,*workbunch,*wb;
  int i,j,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0;

  if (offset_y>0.0) {
    do_jitter=1;
  }
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  if(rf_bin){
    do_emitt=0;
  }
  tb0=bunch_remake(bunch0);
  if (do_jitter){
    tb0_1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb0);
  free_bin_fill(beamline,quad0,quad1,quad2,tb0,workbunch,bin,nbin);
  if (rf_bin) {
    bunch_copy_0(bunch0,tb0);
    simple_bin_fill(beamline,rf_bin,nrf,tb0,workbunch);
    wb=bunch_remake(bunch0);
  }
  if (do_emitt==0) {
    beam_copy(bunch0,tb0);
    bunch_track(beamline,tb0,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    if (do_emitt){
      beam_copy(bunch0,tb0);
      if (do_jitter){
	bunch_copy_0(bunch0,tb0_1);
	for (j=0;j<tb0->slices;j++){
	  tb0_1->particle[j].y+=offset_y;
	}
	free_correct_jitter(beamline,quad0,quad1,quad2,bin,nbin,tb0,tb0_1,
				workbunch,1);
      }
      else{
	free_correct(beamline,quad0,quad1,quad2,bin,nbin,tb0,workbunch,1);
      }
    }
    else{
      bunch_copy_0(bunch0,tb0);
      if (do_jitter){
	bunch_copy_0(bunch0,tb0_1);
	for (j=0;j<tb0->slices;j++){
	  tb0_1->particle[j].y+=offset_y;
	}
	free_correct_jitter(beamline,quad0,quad1,quad2,bin,nbin,tb0,tb0_1,
				workbunch,0);
      }
      else{
	free_correct(beamline,quad0,quad1,quad2,bin,nbin,tb0,workbunch,0);
      }
    }
    bpm_freeze(beamline,0.0,0.0);
    if (rf_bin) {
      beam_copy(bunch0,tb0);
      align_rf(beamline,rf_bin,nrf,tb0,workbunch,wb,NULL);
      beam_copy(bunch0,tb0);
      bunch_track_line_emitt_new(beamline,tb0);
    }
    emitt0=emitt_y(tb0);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (do_jitter){
    beam_delete(tb0_1);
  }
  if (rf_bin){
    beam_delete(wb);
  }
}

void test_train_correction(BEAMLINE *beamline,
			   BIN **bin,int nbin,
			   BEAM *bunch0,
			   BIN **rf_bin,int nrf,double offset_y,
			   int niter,void (*survey)(BEAMLINE*),
			   char *name,const char *format)
{
  BEAM *tb0,*tb0_1,*workbunch,*wb;
  int i,j,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0;

  if (offset_y>0.0) {
    do_jitter=1;
  }
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  if(rf_bin){
    do_emitt=0;
  }
  tb0=bunch_remake(bunch0);
  if (do_jitter){
    tb0_1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb0);
  train_bin_fill(beamline,tb0,workbunch,bin,nbin);
  if (rf_bin) {
    bunch_copy_0(bunch0,tb0);
    simple_bin_fill(beamline,rf_bin,nrf,tb0,workbunch);
    wb=bunch_remake(bunch0);
  }
  if (do_emitt==0) {
    beam_copy(bunch0,tb0);
    bunch_track(beamline,tb0,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    if (do_emitt){
      beam_copy(bunch0,tb0);
      if (do_jitter){
	bunch_copy_0(bunch0,tb0_1);
	for (j=0;j<tb0->slices;j++){
	  tb0_1->particle[j].y+=offset_y;
	}
	free_correct_jitter_one_beam(beamline,bin,nbin,tb0,tb0_1,workbunch,1);
//	free_correct_one_beam(beamline,bin,nbin,tb0,workbunch,1);
      }
      else{
	free_correct_one_beam(beamline,bin,nbin,tb0,workbunch,1);
      }
    }
    else{
      bunch_copy_0(bunch0,tb0);
      if (do_jitter){
	bunch_copy_0(bunch0,tb0_1);
	for (j=0;j<tb0->slices;j++){
	  tb0_1->particle[j].y+=offset_y;
	}
	free_correct_jitter_one_beam(beamline,bin,nbin,tb0,tb0_1,workbunch,0);
      }
      else{
	free_correct_one_beam(beamline,bin,nbin,tb0,workbunch,0);
      }
    }
    bpm_freeze(beamline,0.0,0.0);
    if (rf_bin) {
      beam_copy(bunch0,tb0);
      align_rf(beamline,rf_bin,nrf,tb0,workbunch,wb,NULL);
      beam_copy(bunch0,tb0);
      bunch_track_line_emitt_new(beamline,tb0);
    }
    emitt0=emitt_y(tb0);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  if (do_emitt){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
  if (do_jitter){
    beam_delete(tb0_1);
  }
  if (rf_bin){
    beam_delete(wb);
  }
}

void test_free_correction_dipole(BEAMLINE *beamline,BIN **bin,
				 int nbin,BEAM *bunch0,BEAM *bunch1,
				 BEAM *testbeam,int niter,
				 void (*survey)(BEAMLINE*),
				 char *name,const char *format)
{
  BEAM *tb0,*workbunch,*tb1,*workbunch1,*testb;
  int i,do_emitt;
  double esum=0.0,emitt0,esum2=0.0;

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_data.store_init(beamline->n_quad);
  }
  tb0=bunch_remake(bunch0);
  workbunch=bunch_remake(bunch0);
  tb1=bunch_remake(bunch1);
  workbunch1=bunch_remake(bunch1);
  if (testbeam) {
    testb=bunch_remake(testbeam);
    do_emitt=0;
    if (name==NULL) {
      beam_copy(testbeam,testb);
      bunch_track(beamline,testb,0,beamline->n_elements);
    }
  }
  bunch_copy_0(bunch0,tb0);
  bunch_copy_0(bunch1,tb1);
  free_bin_fill_dipole(beamline,tb0,workbunch,tb1,workbunch1,NULL,NULL,
		       bin,nbin);
  if (do_emitt==0) {
    beam_copy(bunch0,tb0);
    bunch_track(beamline,tb0,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    if (do_emitt){
      beam_copy(bunch0,tb0);
      beam_copy_0(bunch1,tb1);
      free_correct_dipole(beamline,bin,nbin,tb0,workbunch,tb1,workbunch1,1);
    }
    else{
      bunch_copy_0(bunch0,tb0);
      beam_copy_0(bunch1,tb1);
      free_correct_dipole(beamline,bin,nbin,tb0,workbunch,tb1,workbunch1,0);
    }
    emitt0=emitt_y(tb0);
    if (testbeam) {
      if (name) {
	beam_copy(testbeam,testb);
	bunch_track_emitt_start();
	bunch_track_emitt(beamline,testb,0,beamline->n_elements);
	bunch_track_emitt_end(testb,beamline->get_length());
      }
      else {
	beam_copy_0(testbeam,testb);
	bunch_track_0(beamline,testb,0,beamline->n_elements);
      }
      emitt0=emitt_y(testb);
    }
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    placet_printf(INFO,"%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  placet_printf(INFO,"emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  beam_delete(workbunch1);
  beam_delete(tb1);
  if (testbeam) {beam_delete(testb);}
  if (name){
    emitt_data.print(name,format);
    emitt_data.store_delete();
  }
}

double check_autophase(BEAMLINE *beamline,BEAM *bunch0,char *name,const char *format)
{
  int i,nbin;
  BEAM *bunch;
  double emitt0,e;
  BIN **bin;

  if (name!=NULL){
    bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
    beamline_bin_divide(beamline,1,0,bin,&nbin);
    emitt_data.store_init(nbin);
  }
  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
  beamline->set_zero();
  bunch_track(beamline,bunch,0,beamline->n_elements);
  emitt0=emitt_y(bunch);
  beam_copy(bunch0,bunch);
  for (i=0;i<bunch0->slices;i++){
    bunch->particle[i].y=sqrt(bunch->sigma[i].r11)/2.0;
  }
  if (name!=NULL){
    bunch_track_line_emitt(beamline,bin,nbin,bunch);
  }
  else{
    bunch_track(beamline,bunch,0,beamline->n_elements);
  }
  e=emitt_y(bunch);
  placet_printf(INFO,"emitt= %g equivalent to %g percent growth\n",e,
	 (e/emitt0-1.0)*100.0);
  //  bunch_save("bunch.dat",bunch);
  beam_delete(bunch);

  if (name!=NULL){
    emitt_data.print(name,format);
    emitt_data.store_delete();
    free(bin);
  }
  return e;
}

void rf_init()
{
  rf_data.cavlength=0.49;
  rf_data.gradient=0.1;
  rf_data.lambda=0.01;
  rf_data.phase_shift=0.0;
}
