#include <cstdlib>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"

#include "lattice.h"
#include "girder.h"
#include "matrix.h"
#include "focus.h"

extern INTER_DATA_STRUCT inter_data;

int tk_Focus(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	     char *argv[])
{
  int error;
  double strength=0.0;
  FOCUS *element;
  Tk_ArgvInfo table[]={
    {(char*)"-strength",TK_ARGV_FLOAT,(char*)NULL,(char*)&strength,
     (char*)"strength of focus element"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Focus>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  element=new FOCUS(strength);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

void FOCUS::step_4d_0(BEAM* beam)
{
  for (int i=0;i<beam->slices;++i){
    beam->particle[i].yp-=beam->particle[i].y*strength/beam->particle[i].energy;
#ifdef TWODIM
    beam->particle[i].xp-=beam->particle[i].x*strength/beam->particle[i].energy;
#endif
  }
}

void FOCUS::step_4d(BEAM* beam)
{
  R_MATRIX r;

  r.r11=1.0;
  r.r12=0.0;
  r.r22=1.0;
  for (int i=0;i<beam->slices;++i){
    r.r21=-strength/beam->particle[i].energy;
    beam->particle[i].yp-=beam->particle[i].y*strength
      /beam->particle[i].energy;
    mult_M_M(beam->sigma+i,&r,beam->sigma+i);
#ifdef TWODIM
    beam->particle[i].xp-=beam->particle[i].x*strength
      /beam->particle[i].energy;
    mult_M_M(beam->sigma_xx+i,&r,beam->sigma_xx+i);
    mult_M_M(beam->sigma_xy+i,&r,beam->sigma_xy+i);
#endif    
  }  
}

void FOCUS::list(FILE* f)const
{
  fprintf(f,"Focus -length %g -strength %g\n",geometry.length,strength);
}
