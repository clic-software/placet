#include <limits>
#include "crab.h"
#include "girder.h"
#include "lattice.h"
#include "dipole_kick.h"
#include "wakekick.h"
#include "placeti3.h"
#include "placet_tk.h"
#include "structures_def.h"

extern WAKEFIELD_DATA_STRUCT wakefield_data;

void CRAB::set_wake_long(const char *name )
{
  if (name) {
    wake_data=get_wake(name);
  }
}

std::string CRAB::get_wake_long() const { return wake_data ? wake_data->name : "[NONE]"; }

/*
static inline double fast_sin(double x )
{
  x=fmod(x,2*M_PI);
  if (x>M_PI)
    x-=2*M_PI;
  else if (x<-M_PI) 
    x+=2*M_PI;
  double xx=x*x;
  double xxxx=xx*xx;
  return x-x*xx/6+x*xxxx/120;
}
*/
CRAB::CRAB(int &argc, char **argv ) : ELEMENT(), voltage(0.0), lambda(0.0), phase(0.0), wake_data(NULL)
{
  attributes.add("frequency", "Operating Frequency [GHz]", OPT_DOUBLE, &lambda, freq2lambda, lambda2freq);
  attributes.add("voltage", "Deflection Voltage [MV]", OPT_COMPLEX, &voltage);
  attributes.add("lambda", "Wavelength [m]", OPT_DOUBLE, &lambda);
  attributes.add("phase", "Phase of the center of kick [deg]", OPT_DOUBLE, &phase, deg2rad, rad2deg);
  attributes.add("wakelong", "Name of the longrange wakefield [STRING]", OPT_STRING, this, 
		 Set(&CRAB::set_wake_long),
		 Get(&CRAB::get_wake_long));
  set_attributes(argc, argv);
}



inline void CRAB::step_apply_wakekick(BEAM* beam) {
  if (wake_data) {
    if(wakefield_data.transv) {
      if ((beam->which_field==1)||
          (beam->which_field==3)||
          (beam->which_field==4)||
          (beam->which_field==123)) {
        wake_kick(this,beam,geometry.length);
      } else {
        dipole_kick(this,wake_data,beam);
      }
    }
  }
}
void CRAB::step_4d(BEAM *beam )
{
  /*
    Move beam to the centre of the structure
  */
  ELEMENT::drift_step_4d(beam,0.5);
  /*
    Apply the crab kick
  */
  std::complex<double> kick_factor=voltage*1e3/ref_energy; // urad
  double arg=2.*M_PI/(lambda*1e6);
  int n=beam->slices/beam->bunches/beam->macroparticles;
  int m=0;
  for (int j=0;j<beam->bunches;j++) {
    for (int i=0;i<n;i++) {
      double kick = std::imag(kick_factor * std::polar(1.0, arg*beam->z_position[j*n+i] + phase));
      for (int k=0;k<beam->macroparticles;k++) {
        beam->particle[m].xp+=kick;
        m++;
      }
    }
  }
  /*
    Apply the wakefield kick
  */
  CRAB::step_apply_wakekick(beam);
  /*
    Move beam to the end of the structure
  */
  ELEMENT::drift_step_4d(beam,0.5);
}

void CRAB::step_4d_0(BEAM *beam)
{
  /*
    Move beam to the centre of the structure
  */
  ELEMENT::drift_step_4d_0(beam,0.5);
  /*
    Apply the crab kick
  */
  std::complex<double> kick_factor = voltage*1e3/ref_energy;
  double arg=2.*M_PI/(lambda*1e6);
  for (int i_b=0,i=0;i_b<beam->bunches;i_b++) {
    for (int i_s=0;i_s<beam->slices_per_bunch;i_s++) {
      // double z_position=beam->z_position[i_s];
      for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
        // if particle.z << lambda, we can approximate the sin with fast_sin
        // double kick=kick_factor*fast_sin(arg*beam->particle[i].z+phase);
        // double kick = kick_factor*sin(arg*beam->particle[i].z+phase);
        double kick = std::imag(kick_factor * std::polar(1.0, arg*beam->particle[i].z + phase));
	beam->particle[i].xp+=kick;
	i++;
      }
    }
  }
  /*
    Apply the wakefield kick
  */
  CRAB::step_apply_wakekick(beam);
  /*
    Move beam to the end of the structure
  */
  ELEMENT::drift_step_4d_0(beam,0.5);
}

void CRAB::step_6d_0(BEAM *beam )
{
  /*
    Move beam to the centre of the structure
  */
  ELEMENT::drift_step_6d_0(beam,0.5);
  /*
    Apply the crab kick
  */
  double arg=2.*M_PI/(lambda*1e6);
  double arg2=arg*1e-6*ref_energy;
  for (int i_b=0,i=0;i_b<beam->bunches;i_b++) {
    for (int i_s=0;i_s<beam->slices_per_bunch;i_s++) {
      // double z_position=beam->z_position[i_s];
      for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
        std::complex<double> kick = voltage*1e3/beam->particle[i].energy *std::polar(1.0, arg*beam->particle[i].z + phase);
        beam->particle[i].xp+=std::imag(kick);
        beam->particle[i].energy+=std::real(kick)*arg2*beam->particle[i].x;
        i++;
      }
    }
  }
  /*
    Apply the wakefield kick
  */
  CRAB::step_apply_wakekick(beam);
  /*
    Move beam to the end of the structure
  */
  ELEMENT::drift_step_6d_0(beam,0.5);
} // step_6d_0 end

void CRAB::step_twiss(BEAM *beam,FILE *file,double step0,int iel,double s0,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int)){
  placet_cout << VERBOSE << " CrabCavity:: step_twiss:  twiss computed at the entrance and at the exit of the cavity " << endmsg;
  callback(file,beam,iel,s0,n1,n2);
  if (flags.thin_lens) {
    placet_cout << WARNING << " CrabCavity:: step_twiss: option thin_lens not implemented " << endmsg;
  } else {
    if (flags.longitudinal) {
      if (flags.synrad) placet_cout << WARNING << " CrabCavity:: step_twiss: step_6d_sr synrad not implemented " << endmsg;
      else              placet_cout << WARNING << " CrabCavity:: step_twiss: step_6d not implemented " << endmsg;
    } else {
      if (flags.synrad) {
	placet_cout << WARNING << " CrabCavity:: step_twiss: step_4d_sr synrad not implemented " << std::endl;
	placet_cout << WARNING << "                                       step_4d called instead " << endmsg;
	CRAB::step_4d(beam);
      } else           {   CRAB::step_4d(beam);}
    }   
  }
  s0+=geometry.length;
  callback(file,beam,iel,s0,n1,n2);
}

void CRAB::step_twiss_0(BEAM *beam,FILE *file,double step0,int iel,double s0,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int)){
  placet_cout << VERBOSE << " CrabCavity:: step_twiss_0:  twiss computed at the entrance and at the exit of the cavity " << endmsg;
  callback(file,beam,iel,s0,n1,n2);
  if (flags.thin_lens) {
    placet_cout << WARNING << " CrabCavity:: step_twiss_0: option thin_lens not implemented " << endmsg;
  } else {
    if (flags.longitudinal) {
      if (flags.synrad) {
        placet_cout << WARNING << " CrabCavity:: step_twiss_0: step_6d_sr_0 synrad not implemented " << std::endl;
        placet_cout << WARNING << "                                       step_6d_0 called instead " << endmsg;
      }
     CRAB::step_6d_0(beam);
    } else {
      if (flags.synrad) {
	placet_cout << WARNING << " CrabCavity:: step_twiss_0: step_4d_sr_0 synrad not implemented " << std::endl;
	placet_cout << WARNING << "                                       step_4d_0 called instead " << endmsg;
	CRAB::step_4d_0(beam);
      } else           {   CRAB::step_4d_0(beam);}
    }   
  }
  s0+=geometry.length;
  callback(file,beam,iel,s0,n1,n2);
}

/*IStream &operator>>(IStream &stream, CRAB &crab )
{
  stream >> static_cast<ELEMENT &>(crab)
	 >> crab.voltage
	 >> crab.phase
	 >> crab.lambda;
  std::string name;
  stream >> name;
  crab.set_wake_long(name.c_str());
  return stream;
}*/

/*OStream &operator<<(OStream &stream, const CRAB &crab )
{
  return stream << static_cast<const ELEMENT &>(crab)
		<< crab.voltage
		<< crab.phase
                << crab.lambda
		<< std::string(crab.wake_data->name);
}*/

int tk_CrabCavity(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  if (element_help_message<CRAB>(argc,argv, "CrabCavity:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  inter_data.girder->add_element(new CRAB(argc, argv));
  return TCL_OK;
}
