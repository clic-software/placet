#include "particle.h"

int particle_cmp(const PARTICLE *a, const PARTICLE *b )
{
  return a->z < b->z ? -1 : (a->z == b->z ? 0 : 1);
}
