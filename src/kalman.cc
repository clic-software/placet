#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>

//#include <ctype.h>

#include <signal.h>
#include <tcl.h>
#include <tk.h>

#include "matrixnd.hh"

using namespace std;

/* Subroutine */

struct CONTROLLER_DATA {

//	MatrixNd	A;
	MatrixNd	B;
	MatrixNd	C;

	MatrixNd	P;
	MatrixNd	Q;
	MatrixNd	R;
	
	VectorNd 	x;
	
	VectorNd	sensors;		// n-sensors components
	VectorNd	target;			// n-sensors components
	VectorNd	knobs;			// n-knobs components
	
	int	nsensors;
	int	nknobs;

	mutable VectorNd xm;
	mutable VectorNd u;

	mutable	MatrixNd K;
	mutable MatrixNd tB;
	mutable MatrixNd Pm;
	
	bool first_call;
	
	const VectorNd &kalman_step(const VectorNd &dy, double gain )
	{
//		xm = A * x + B * u;	// time update
		xm = x - gain * B * u;	// time update
		
		Pm = P + Q;
		K = Pm * inverse(Pm + R);
//		K = Pm * tC * inverse(C * Pm * tC + R);

		x = xm + K * (dy - xm);
		P = Pm - K * Pm;

//		x = xm + K * (dy - C * xm);
//		P = Pm - K * C * Pm;
		
		return x;
	}

	const VectorNd &get_feedback(const VectorNd &dy ) const
	{
		tB = transpose(B);
	
		MatrixNd tBB  = tB * B;
		VectorNd tBdy = tB * dy;

 		return u = solve(tBB, tBdy);
	}

//	friend OFile &operator << (OFile &stream, const CONTROLLER_DATA &c )	{ return stream << c.kalman << c.sensors << c.target << c.knobs << c.nsensors << c.nknobs; }
//	friend IFile &operator >> (IFile &stream, CONTROLLER_DATA &c )		{ return stream >> c.kalman >> c.sensors >> c.target >> c.knobs >> c.nsensors >> c.nknobs; }
};

static int ParseTclVector(Tcl_Interp *interp, const char *str, VectorNd &ret )
{
	int return_value = TCL_OK;
	char **val;
	int num;
		
	if (int error = Tcl_SplitList(interp, str, &num, &val))
	{
		Tcl_AppendResult(interp, "Cannot read list", NULL);

		return error;
	}
		
	ret.size(num);
		
	for (int i = 0; i < num; i++)
	{
		if (int error = Tcl_GetDouble(interp, val[i], &ret[i])) 
		{
			return_value = error;

			break;
		}
	}

	Tcl_Free((char*)val);
	
	return return_value;
}

static int ParseTclMatrix(Tcl_Interp *interp, const char *str, MatrixNd &ret )
{
	int return_value = TCL_OK;
	char **val1;
	int num1;
	if (int error = Tcl_SplitList(interp, str, &num1, &val1)) {
		Tcl_AppendResult(interp, "Cannot read list", NULL);
		return error;
	}

	for (int i=0; i<num1; i++) {
		char **val2;
		int num2;
		if (int error = Tcl_SplitList(interp, val1[i], &num2, &val2)) {
			Tcl_AppendResult(interp, "Cannot read list", NULL);
			return error;
		}
		
		if (i==0) ret.resize(num1,num2);
		for (int j=0; j<num2; j++) {
			if (int error = Tcl_GetDouble(interp, val2[j], &ret[i][j])) {
				return_value = error;
				break;
			}
		}
		Tcl_Free((char*)val2);
	}
	Tcl_Free((char*)val1);
	return return_value;
}

static int Tcl_KalmanCorrectionApply(ClientData clientData, Tcl_Interp *interp, int argc, char **argv )
{
	char *sensors_str = NULL;
	char *knobs_str = NULL;
	char *filename = NULL;
	double gain = 1.0;
	int reset = 0;
	int ret = 1;

	Tk_ArgvInfo table[] = {
		{(char *) "-save", TK_ARGV_STRING, (char *) NULL,
		 (char *) &filename,
		 (char *) "Save the controller status to $filename"},
		{(char *) "-knobs", TK_ARGV_STRING, (char *) NULL,
		 (char *) &knobs_str,
		 (char *) "List or knob values"},
		{(char *) "-measured", TK_ARGV_STRING, (char *) NULL,
		 (char *) &sensors_str,
		 (char *) "List of measured sensor values"},
		{(char *) "-return_value", TK_ARGV_INT, (char *) NULL,
		 (char *) &ret,
		 (char *) "Choice of return value: 1: correction, 2: estimate, 3: Jacobian, 4: transpose(Jacobian)"},
		{(char *) "-gain", TK_ARGV_FLOAT, (char *) NULL,
		 (char *) &gain,
		 (char *) "Gain to be applied to the calculated correction"},
		{(char *) "-reset", TK_ARGV_INT, (char *) NULL,
		 (char *) &reset,
		 (char *) "Reset the Kalman Filter's status"},
		{(char *) NULL, TK_ARGV_END, (char *) NULL, (char *) NULL, (char *) NULL}
	};
	
	int error;
	if ((error = Tk_ParseArgv(interp, NULL, &argc, argv, table, 0)) != TCL_OK) {
		return error;
	}

	if (argc > 1) {
		Tcl_SetResult(interp, "Too many arguments to <KalmanCorrectionApply>", TCL_VOLATILE);
		return TCL_ERROR;
	}
	
	CONTROLLER_DATA *controller_data = (CONTROLLER_DATA *)clientData;

	if (reset) {
	
		controller_data->first_call = true;
		
		return TCL_OK;
	}
	
/*	if (filename != NULL)
	{
		placet_printf(INFO,"Saving controller status into file '%d'...\n", filename);
	
		OFile file(filename);
		
		file << *controller_data;

		return TCL_OK;
	}
*/
	
	if (sensors_str == NULL) {
		Tcl_SetResult(interp, "<KalmanCorrectionApply> -return_value 1 requires -measured", TCL_VOLATILE);
		return TCL_ERROR;
	}

	if (sensors_str) {
		ParseTclVector(interp, sensors_str, controller_data->sensors);
		if ((int)controller_data->sensors.size() != controller_data->nsensors) {
			Tcl_SetResult(interp, "Number of -sensors does not match", TCL_VOLATILE);
			return TCL_ERROR;
		}	
	}	
	
	if (ret == 1) {
	
		if (controller_data->first_call) {
		
			controller_data->first_call = false;

			controller_data->P = IdentityNd(controller_data->nsensors);
			controller_data->Q = IdentityNd(controller_data->nsensors) * 1e-5;

			controller_data->x = controller_data->sensors-controller_data->target;
			controller_data->u = controller_data->get_feedback(controller_data->sensors-controller_data->target);
		
		} else {
		
			const MatrixNd 	&B = controller_data->B;
			MatrixNd 	tB = transpose(B);
	
			MatrixNd tBB  = tB * B;
			VectorNd tBdy = tB * controller_data->kalman_step(controller_data->sensors-controller_data->target, gain);

			controller_data->u = solve(tBB, tBdy);
		}
		
		for (unsigned int i = 0; i < controller_data->u.size(); i++) {
			char buffer[100];
			snprintf(buffer, 100, "%g", -gain * controller_data->u[i]);
			Tcl_AppendElement(interp, buffer);
		}
	}/* else if (ret == 2) {
		VectorNd f = controller_data->network(controller_data->sensors-controller_data->target);
		std::string result = "";	
		for (size_t i = 0; i < f.size(); i++) {
			char buffer[100];
			snprintf(buffer, 100, "%g ", f[i]);
			result += buffer;
		}
		Tcl_AppendResult(interp,result.c_str(),NULL);
	} else if (ret == 3 || ret == 4) {
		VectorNd x = VectorNd(controller_data->nsensors, 0.0);
		VectorNd f = controller_data->network(x);
		MatrixNd J = get_Jacobian(x, f, controller_data->network);
		size_t m = controller_data->nsensors;
		size_t n = f.size();
		if (ret == 4) {
			J = transpose(J);
			std::swap(m, n);
		}
		std::string result = "{\n";
		for (size_t j = 0; j < m; j++) {
			result += "\t{";
			for (size_t i = 0; i < n; i++) {
				char buffer[100];
				snprintf(buffer, 100, "%g\t", J[i][j]);
				result += buffer;
			}
			result += "}\n";
		}
		result += "}\n";
		Tcl_AppendResult(interp,result.c_str(),NULL);
	}
*/	
	return TCL_OK;
}

int Tcl_KalmanCorrectionInit(ClientData /*clientData*/, Tcl_Interp *interp, int argc, char **argv )
{
	char *measurement_noise_covariance_vector = NULL;
	char *measurement_noise_covariance_matrix = NULL;
	double measurement_noise_covariance = -1;

	char *process_noise_covariance_vector = NULL;
	char *process_noise_covariance_matrix = NULL;
	double process_noise_covariance = -1;

	char *response_str = NULL;
	char *target_str = NULL;
	char *filename = NULL;

	int nsensors = -1;
	int nknobs = -1;
	
	Tk_ArgvInfo table[] = {
/*		{(char *) "-load", TK_ARGV_STRING, (char *) NULL,
		 (char *) &filename,
		 (char *) "Load controller status from $filename"},
*/		{(char *) "-nknobs", TK_ARGV_INT, (char *) NULL,
		 (char *) &nknobs,
		 (char *) "Number of knobs"},
		{(char *) "-nsensors", TK_ARGV_INT, (char *) NULL,
		 (char *) &nsensors,
		 (char *) "Number of sensors"},
		{(char *) "-target", TK_ARGV_STRING, (char *) NULL,
		 (char *) &target_str,
		 (char *) "List of desired output values for sensors"},
		{(char *) "-response", TK_ARGV_STRING, (char *) NULL,
		 (char *) &response_str,
		 (char *) "Response matrix (as a list of lists)"},
		{(char *) "-process_covariance_matrix", TK_ARGV_STRING, (char *) NULL,
		 (char *) &process_noise_covariance_matrix,
		 (char *) "Process noise covariance matrix (as a list of lists)"},
		{(char *) "-process_covariance_vector", TK_ARGV_STRING, (char *) NULL,
		 (char *) &process_noise_covariance_vector,
		 (char *) "Process noise covariance vector (as a list)"},
		{(char *) "-process_covariance_value", TK_ARGV_FLOAT, (char *) NULL,
		 (char *) &process_noise_covariance,
		 (char *) "Process noise covariance"},
		{(char *) "-measurement_covariance_matrix", TK_ARGV_STRING, (char *) NULL,
		 (char *) &measurement_noise_covariance_matrix,
		 (char *) "Measurement noise covariance matrix (as a list of lists)"},
		{(char *) "-measurement_covariance_vector", TK_ARGV_STRING, (char *) NULL,
		 (char *) &measurement_noise_covariance_vector,
		 (char *) "Measurement noise covariance vector (as a list of lists)"},
		{(char *) "-measurement_covariance_value", TK_ARGV_FLOAT, (char *) NULL,
		 (char *) &measurement_noise_covariance,
		 (char *) "Measurement noise covariance"},
		{(char *) NULL, TK_ARGV_END, (char *) NULL, (char *) NULL, (char *) NULL}
	};

	int error;
	if ((error = Tk_ParseArgv(interp, NULL, &argc, argv, table, 0)) != TCL_OK) {
		return error;
	}
	
	if (argc > 2) {
		Tcl_SetResult(interp, "Too many arguments to <KalmanCorrectionInit>", TCL_VOLATILE);
		return TCL_ERROR;
	}
	
	if (argc < 2) {
		Tcl_SetResult(interp, "<KalmanCorrectionInit> requires a name", TCL_VOLATILE);
		return TCL_ERROR;
	}

	if (filename == NULL && target_str == NULL && nsensors == -1) {
		Tcl_SetResult(interp, "<KalmanCorrectionInit> requires the parameter -nsensors", TCL_VOLATILE);
		return TCL_ERROR;
	}

	if (filename == NULL && response_str == NULL && nknobs == -1) {
		Tcl_SetResult(interp, "<KalmanCorrectionInit> requires the parameter -nknobs", TCL_VOLATILE);
		return TCL_ERROR;
	}
	
	CONTROLLER_DATA *controller_data = new CONTROLLER_DATA;

/*	if (filename)
	{
		IFile file(filename);

		if (file) {
			
			file >> *controller_data;
						
		} else {
		
			Tcl_AppendResult(interp, "<KalmanCorrectionInit> could not load file '", filename, "'", NULL);
			
			return TCL_ERROR;
		}
		
	} else */ {
	
		if (response_str != NULL) {
			MatrixNd tB;
			if ((error = ParseTclMatrix(interp, response_str, tB)) != TCL_OK) {
				Tcl_SetResult(interp, "<KalmanCorrectionInit> could not read the response matrix", TCL_VOLATILE);
				return error;
			}
			controller_data->B  = transpose(tB);
		}

		if (nsensors == -1)	nsensors = controller_data->B.rows();
		else {
		  if (response_str && nsensors != (int)controller_data->B.rows()) {
				Tcl_SetResult(interp, "Number of -nsensors does not match", TCL_VOLATILE);
				return TCL_ERROR;
			}
		}
	
		if (nknobs == -1)	nknobs = controller_data->B.columns();
		else {
		        if (response_str && nknobs != (int)controller_data->B.columns()) {
				Tcl_SetResult(interp, "Number of -nknobs does not match", TCL_VOLATILE);
				return TCL_ERROR;
			}	
		}

		if (process_noise_covariance_matrix != NULL) {
			if ((error = ParseTclMatrix(interp, process_noise_covariance_matrix, controller_data->Q)) != TCL_OK) {
				Tcl_SetResult(interp, "<KalmanCorrectionInit> could not read the process noise covariance matrix", TCL_VOLATILE);
				return error;
			}
		} else if (process_noise_covariance_vector != NULL) {
			VectorNd sigma;
			if ((error = ParseTclVector(interp, process_noise_covariance_vector, sigma)) != TCL_OK) {
				Tcl_SetResult(interp, "<KalmanCorrectionInit> could not read the process noise covariance vector", TCL_VOLATILE);
				return error;
			} else {
				controller_data->Q = MatrixNd(nsensors, nsensors, 0.0);
				for (size_t i = 0; i < sigma.size(); i++)	controller_data->Q[i][i] = sigma[i];
			}
			
		} else if (process_noise_covariance != -1)	controller_data->Q = process_noise_covariance * IdentityNd(nsensors);
		else						controller_data->Q = IdentityNd(nsensors) * 1e-5;

		if (measurement_noise_covariance_matrix != NULL) {
			if ((error = ParseTclMatrix(interp, measurement_noise_covariance_matrix, controller_data->R)) != TCL_OK) {
				Tcl_SetResult(interp, "<KalmanCorrectionInit> could not read the measurement noise covariance matrix", TCL_VOLATILE);
				return error;
			}
		} else if (measurement_noise_covariance_vector != NULL) {
			VectorNd sigma;
			if ((error = ParseTclVector(interp, measurement_noise_covariance_vector, sigma)) != TCL_OK) {
				Tcl_SetResult(interp, "<KalmanCorrectionInit> could not read the measurement noise covariance vector", TCL_VOLATILE);
				return error;
			
			} else {
				controller_data->R = MatrixNd(nsensors, nsensors, 0.0);
				for (size_t i = 0; i < sigma.size(); i++)	controller_data->R[i][i] = sigma[i];
			}
			
		} else if (measurement_noise_covariance != -1)	controller_data->R = measurement_noise_covariance * IdentityNd(nsensors);
		else						controller_data->R = IdentityNd(nsensors);
	
		controller_data->P = IdentityNd(nsensors);
		controller_data->x = VectorNd(nsensors, 0.0);
		
		controller_data->first_call = true;

		controller_data->nsensors = nsensors;
		controller_data->nknobs = nknobs;
	}
	
	char *name = argv[1];
	
	if (target_str) {
		ParseTclVector(interp, target_str, controller_data->target);
		if ((int)controller_data->target.size() != controller_data->nsensors) {
			Tcl_SetResult(interp, "Number of -target does not match", TCL_VOLATILE);
			return TCL_ERROR;
		}	
	}
	
	Tcl_CreateCommand(interp, name, Tcl_KalmanCorrectionApply, (ClientData) controller_data, (Tcl_CmdDeleteProc *) NULL);
	return TCL_OK;
}

/* here is the signal handler */
/*static void catch_int(int sig_num)
{
	if (controller_ptr)
	{
		placet_printf(INFO,"Saving the controller status to file 'controller.bin'...\n");
	
		OFile file("controller.bin");
		
		file << (*controller_ptr);
	}

	exit(0);
}
*/
