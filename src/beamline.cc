#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "beamline.h"
#include "girder.h"
#include "ground.h"

/** zero point declaration, defined in placet_tk.cc (always 0.0) */
double zero_point;

BEAMLINE::BEAMLINE():length(0.0),n_girders(0),n_elements(0),first(NULL),last(NULL),element(NULL),quad(NULL),cav(NULL),bpm(NULL),n_quad(0),n_bpm(0),n_cav(0),gx(NULL),gy(NULL){
}

void BEAMLINE::unset()
{
  free(element); element=NULL;
  free(quad); quad=NULL;
  free(bpm); bpm=NULL;
  free(cav); cav=NULL;
  delete gx; gx=NULL;
  delete gy; gy=NULL;
}

BEAMLINE::~BEAMLINE()
{
  GIRDER *girder, *girder2;
  ELEMENT *el, *el2;
  
  girder=first;
  while (girder) {
    el=girder->element();
    while (el) {
      el2=el->next;
      delete el;
      el=el2;
    }
    girder2=girder->next();
    delete girder;
    girder=girder2;
  }
  
  unset();
}

/** add girder to the end of line with a distance dist to last element */
void BEAMLINE::girder_add(GIRDER *girder,double dist )
{
  girder->set_distance_to_prev_girder(dist);
  if (last==NULL) {
    first=girder;
    last=girder;
    n_girders=1;
  } else {
    (n_girders)++;
    (last)->next_girder=girder;
    last=girder;
  }
}

/*
double BEAMLINE::calc_length()const
{
  GIRDER *girder;
  double l=0.0,dl;
  girder=first;
  while (girder!=NULL) {
    dl=girder->get_length();
    l+=dl;
    girder=girder->next();
  }
  return l;
}
*/

void BEAMLINE::list(FILE *file)const
{
  GIRDER *girder;
  ELEMENT *el;
  
  girder=first;
  while (girder) {
    fprintf(file,"Girder\n");
    el=girder->element();
    while (el) {
      el->list(file);
      el=el->next;
    }
    girder=girder->next();
  }
}

void BEAMLINE::set_zero()
{
  for (int i=0; i<n_elements; i++) {
    element[i]->set_offset(0.0, 0.0, zero_point, 0.0, 0.0);
  }
}

void BEAMLINE::set(FILE *file )
{
  int n=0;
  GIRDER *girder;
  ELEMENT *el,**point;
  int iq=0,ib=0,ic=0;

  // double loop to get number of elements, allocate, and then fill.
  // can be avoided with vector / push_back

  girder=first;
  double s=0.0;
  while (girder!=NULL) {
    el=girder->element();
    while (el!=NULL){
      if (el->is_quad())   iq++;
      if (el->is_bpm())    ib++;
      if (el->is_cavity()) ic++;
      s+=el->get_length();
      el->set_s(s);
      el=el->next;
      n++;
    }
    girder=girder->next();
  }
  length=s;

  element=(ELEMENT**)malloc(sizeof(ELEMENT*)*(n+1));
  quad=(QUADRUPOLE**)malloc(sizeof(QUADRUPOLE*)*iq);
  bpm=(BPM**)malloc(sizeof(BPM*)*ib);
  cav=(CAVITY**)malloc(sizeof(CAVITY*)*ic);
  n_quad=iq; 
  n_bpm=ib;
  n_cav=ic;
  iq=0;
  ib=0;
  ic=0;
  point=element;
  girder=first;
  while (girder!=NULL) {
    el=girder->element();
    while (el!=NULL){
      if (el->quad_ptr()) {
        el->set_number(iq);
      	quad[iq++]=el->quad_ptr();
      }
      if (el->bpm_ptr()) {
        el->set_number(ib);
	bpm[ib++]=el->bpm_ptr();
      }
      if (el->cavity_ptr()) {
        el->set_number(ic);
	cav[ic++]=el->cavity_ptr();
      }
      *point++=el;
      el=el->next;
    }
    girder=girder->next();
  }
  *point=NULL;
  n_elements=n;
  if (file) { 
    placet_fprintf(INFO, file, "Beamline ");
    if (!name.empty()) placet_fprintf(INFO, file, "'%s' ",name.c_str());
    placet_fprintf(INFO, file, "set with %d elements: %d quadrupoles, %d BPMs, and %d cavities\n",n,iq,ib,ic);
    placet_cout << INFO << "Beamline length: " << s << " m" << endmsg;
    //    placet_cout << INFO << std::setprecision(15) << "Beamline calc length: " << calc_length() << endmsg;
  }
}

int BEAMLINE::girder_number(int element_nr)const{
  int el_cnt=-1, gir_cnt=-1;
  GIRDER* girder=first;
  while (girder) {
    gir_cnt++;
    ELEMENT* el=girder->element();
    while (el) {
      el_cnt++;
      if (element_nr==el_cnt) {return gir_cnt;}
      el=el->next;
    }
    girder=girder->next();
  }
  return gir_cnt;
}

/** check if element number is in beamline range */
bool BEAMLINE::element_in_range(int element_nr)const{
  if (element_nr>=n_elements || element_nr<0) {
    placet_cout << ERROR << element_nr << " is not a valid element number, there are " << 
      n_elements << " elements in the beamline" << endmsg;
    return false;
  }
  return true;
 }

void BEAMLINE::print(FILE *file)const
{
  GIRDER *girder;
  girder=first;
  while (girder!=NULL){
    girder->print(file);
    girder=girder->next();
  }
}
