#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>
#include <typeinfo>
#include <algorithm>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "andrea.h"
#include "beamline.h"
#include "bin.h"
#include "bpm.h"
#include "cavity.h"
#include "dipole.h"
#include "drift.h"
#include "errf.h"
#include "function.h"
#include "girder.h"
#include "malloc.h"
#include "matrix.h"
#include "multipole.h"
#include "placeti3.h"
#include "quadbpm.h"
#include "quadrupole.h"
#include "track.h"
#include "bunch.h"
#include "random.hh"

ERRORS errors;
RF_DATA rf_data;
BALLISTIC_DATA  ballistic_data;
INJECTOR_DATA injector_data;
CORR corr;
BUMP_DATA bump_data;

double quad_set0[MAX_QUAD],quad_set1[MAX_QUAD],quad_set2[MAX_QUAD],
  quad_strength[MAX_QUAD];
FIELD *field_cav[10];

extern int bump_long;
extern int data_nstep;
extern double zero_point;
extern PetsWake drive_data;
extern SURVEY_ERRORS_STRUCT survey_errors;

static struct {
  int cav_bpm,all;
} bpm_data;

static struct{
  int nsect;
  int *nquad,*nlong,*ngirder;
} lattice_data;

/*
  calculates the the transverse resistive wall wakefield
*/

/*
  cond: conductivity per [Ohm m]
  Z0: resistivity of vacuum in [Ohm]
  c: speed of light in [m/s]
  a: radius of the beampipe in [m]
  s: distance in [m]
  returns transverse wakefield in [V/C/m^2]
  */

double resist_wall_transv(double s)
{
  const double cond=5.9e7,a=10e-3;
  return C_LIGHT/(PI*a*a*a)*sqrt(Z0/(PI*cond)/s);
}

/*
  calculates the the longitudinal resistive wall wakefield
*/

/*
  cond: conductivity per [Ohm m]
  Z0: resistivity of vacuum in [Ohm]
  c: speed of light in [m/s]
  a: radius of the beampipe in [m]
  s: distance in [m]
  returns longitudinal wakefield in [V/C/m]
  */

double resist_wall_long(double s)
{
  const double cond=5.9e7,a=10e-3;
  return - C_LIGHT/(4.0*PI*a)*sqrt(Z0/(PI*cond*s*s*s));
}

/*
  n: number of particles
  kick: wakefield at 2 sigmas
  l: quadrupole spacing
  mu: phase advance per FODO cell
  e: beam energy in GeV
*/

double bns(double n,double kick,double l,double mu,double e)
{
  double tmp;
  /* TAN(MU/2) */
  tmp=tan(mu*PI/360.0);
  return n*ECHARGE*l*l*(1.0+1.5/(tmp*tmp))/48.0/e*1e3*kick;
  /*/3.332*878.0;*/
}


void mkquad(double rk,double rl,double r[])
{
  double sign1, sign2;
  int i;
  double rkabs, c1, c2, s1, s2, rksqrt;
  
  for (i=0;i<16;i++) {
    r[i]=0.0;
  }
  rkabs=fabs(rk);
  rksqrt=sqrt(rkabs);
  if (rk>0.0) {
    sincos(rksqrt*rl,&s1,&c1);
    sincosh(rksqrt*rl,s2,c2);
    sign1=-1.0;
    sign2=1.0;
  } else {
    sincosh(rksqrt*rl,s1,c1);
    sincos(rksqrt*rl,&s2,&c2);
    sign1=1.0;
    sign2=-1.0;
  }
  r[0]=c1;
  r[1]=s1/rksqrt;
  r[4]=s1*rksqrt*sign1;
  r[5]=c1;
  r[10]=c2;
  r[11]=s2/rksqrt;
  r[14]=s2*rksqrt*sign2;
  r[15]=c2;
}

void mkdrift(double rl,double r[])
{
  int i;
  
  for (i=0;i<16;i++) {
    r[i]=0.0;
  }
  r[0]=1.0;
  r[1]=rl;
  r[5]=1.0;
  r[10]=1.0;
  r[11]=rl;
  r[15]=1.0;
}


void matmul(double r1[],double r2[],double r3[])
{
  int i,j,k;
  double rhelp[16],sum;
  
  for (k=0;k<4;k++) {
    for (j=0;j<4;j++) {
      sum=0.0;
      for (i=0;i<4;i++) {
	sum += r1[i+k*4]*r2[j+4*i];
      }
      rhelp[j+4*k]=sum;
    }
  }
  for (j=0;j<16;j++) {
    r3[j]=rhelp[j];
  }
}

/* function returns 1 if no solution exists, 0 otherwise */

int infodo(double rk1,double rl1,double rk2,double rl2,double d,
	   double *alpha1,double *alpha2,double *beta1,double *beta2,
	   double *rmu1,double *rmu2)
{
    double rtot[16],drift,rd[16],rq[16];
    double cos1, cos2;

    drift=d-0.5*(rl1+rl2);
    mkquad(rk2,rl2,rq);
    mkdrift(drift,rd);
    matmul(rq,rd,rtot);
    matmul(rd,rtot,rtot);
    mkquad(rk1,0.5*rl1,rq);
    matmul(rtot,rq,rtot);
    matmul(rq,rtot,rtot);
    cos1=0.5*(rtot[0]+rtot[5]);
    cos2=0.5*(rtot[10]+rtot[15]);
    if (cos1>1.0) {
      return 1;
    }
    if (cos2>1.0) {
      return 1;
    }
    *rmu1 = acos(cos1);
    *rmu2 = acos(cos2);
    if (rtot[1]<0.0) {
	*rmu1 = TWOPI-(*rmu1);
    }
    if (rtot[11]<0.0) {
	*rmu2 = TWOPI-(*rmu2);
    }
    *alpha1 = (rtot[0]-rtot[5])*0.5/sin(*rmu1);
    *alpha2 = (rtot[10]-rtot[15])*0.5/sin(*rmu2);
    *beta1 = rtot[1]/sin(*rmu1);
    *beta2 = rtot[11]/sin(*rmu2);
    return 0;
}

/* function returns 1 if no solution exists, 0 otherwise */

int intripl(double rk1,double rk2,double rl1,double rl2,double rl3,double d,
	    double *alpha1,double *alpha2,double *beta1,double *beta2,
	    double *rmu1,double *rmu2)
{
    double rtot[16],drift,rd[16],rq[16];
    double cos1, cos2;

    //    drift=d-2.0*(rl2+rl1);
    //    mkquad(rk2,0.5*rl1,rq);
    drift=d-rl1-2.0*(rl2+rl3);
    mkquad(rk2,rl2,rq);
    mkdrift(drift,rd);
    matmul(rq,rd,rtot);
    matmul(rtot,rq,rtot);
    mkdrift(rl3,rd);
    matmul(rtot,rd,rtot);
    matmul(rd,rtot,rtot);
    mkquad(rk1,0.5*rl1,rq);
    matmul(rtot,rq,rtot);
    matmul(rq,rtot,rtot);
    cos1=0.5*(rtot[0]+rtot[5]);
    cos2=0.5*(rtot[10]+rtot[15]);
    if (cos1>1.0) {
      return 1;
    }
    if (cos2>1.0) {
      return 1;
    }
    *rmu1 = acos(cos1);
    *rmu2 = acos(cos2);
    if (rtot[1]<0.0) {
	*rmu1 = TWOPI-(*rmu1);
    }
    if (rtot[11]<0.0) {
	*rmu2 = TWOPI-(*rmu2);
    }
    *alpha1 = (rtot[0]-rtot[5])*0.5/sin(*rmu1);
    *alpha2 = (rtot[10]-rtot[15])*0.5/sin(*rmu2);
    *beta1 = rtot[1]/sin(*rmu1);
    *beta2 = rtot[11]/sin(*rmu2);
    return 0;
}

int indoublet(double rk1,double rk2,double rl1,double rl2,double d1,double d2,
	      double *alpha1,double *alpha2,double *beta1,double *beta2,
	      double *rmu1,double *rmu2)
{
    double rtot[16],drift,rd[16],rq[16];
    double cos1, cos2;

    drift=0.5*(d2-0.5*(rl1+rl2));
    mkdrift(drift,rd);
    mkquad(rk2,rl2,rq);
    matmul(rq,rd,rtot);
    drift=d1-d2-rl1-rl2;
    mkdrift(drift,rd);
    matmul(rd,rtot,rtot);
    mkquad(rk1,rl1,rq);
    matmul(rq,rtot,rtot);
    drift=0.5*(d2-0.5*(rl1+rl2));
    mkdrift(drift,rd);
    matmul(rd,rtot,rtot);

    cos1=0.5*(rtot[0]+rtot[5]);
    cos2=0.5*(rtot[10]+rtot[15]);
    if (cos1>1.0) {
      return 1;
    }
    if (cos2>1.0) {
      return 1;
    }
    *rmu1 = acos(cos1);
    *rmu2 = acos(cos2);
    if (rtot[1]<0.0) {
	*rmu1 = TWOPI-(*rmu1);
    }
    if (rtot[11]<0.0) {
	*rmu2 = TWOPI-(*rmu2);
    }
    *alpha1 = (rtot[0]-rtot[5])*0.5/sin(*rmu1);
    *alpha2 = (rtot[10]-rtot[15])*0.5/sin(*rmu2);
    *beta1 = rtot[1]/sin(*rmu1);
    *beta2 = rtot[11]/sin(*rmu2);
    return 0;
}

void field_delete(FIELD *field)
{
  free(field->kick);
  free(field->de);
  free(field);
}

void beam_delete(BEAM *bunch)
{
  //placet_printf(INFO,"EA: entering beam delete\n");
#ifdef HTGEN
  free(bunch->particle_sec);
#endif
  free(bunch->sigma);
#ifdef TWODIM
  free(bunch->sigma_xx);
  free(bunch->sigma_xy);
#endif
  free(bunch->particle);
  free(bunch->p_tmp);
#ifdef LONGITUDINAL
  free(bunch->z_position);
#endif
  // EA: free CSR wake parameters
  if (bunch->csrwake) {
    free(bunch->csrwake->terminal_dE_ds); 
    free(bunch->csrwake->terminal_nlambda); 
    free(bunch->csrwake);
  }
  free(bunch);
}

FIELD *field_make(int n_slice,int n_bunch)
{
  FIELD *field;
  int i,m;
  field=(FIELD*)xmalloc(sizeof(FIELD));
  field->de=(double*)xmalloc(sizeof(double)*n_slice*n_bunch);
  for (i=0;i<n_slice;i++){
    field->de[i]=0.0;
  }
  m=n_slice*(n_slice+1)/2;
  field->kick=(double*)xmalloc(sizeof(double)*m);
  for (i=0;i<m;i++){
    field->kick[i]=0.0;
  }
  return field;
}

void field_set_kick(FIELD *field,int n,double grad)
{
  field->kick[n]=grad;
}

BEAM *bunch_make_2(int n_bunch,int n_slice,int n_macro,int n_field,int n_max, int n_halo )
{
  BEAM *bunch;
  bunch=(BEAM*)xmalloc(sizeof(BEAM));
  bunch->bunches=n_bunch;
  bunch->slices_per_bunch=n_slice;
  bunch->macroparticles=n_macro;
  bunch->n_max=n_max;
  n_slice*=n_macro*n_bunch;
  bunch->slices=n_slice;
  bunch->n_field=n_field;
  bunch->particle=(PARTICLE*)xmalloc(sizeof(PARTICLE)*n_slice);
  bunch->p_tmp=(PARTICLE*)xmalloc(sizeof(PARTICLE)*n_max);
  bunch->sigma=(R_MATRIX*)xmalloc(sizeof(R_MATRIX)*n_slice);
  bunch->nhalo = n_halo;
#ifdef HTGEN
  int dim_halo=((std::max(n_halo, 1)+DimHalo-1)/DimHalo)*DimHalo;
  bunch->particle_sec = (PARTICLE*)malloc(sizeof(PARTICLE)*dim_halo);
#endif
#ifdef TWODIM
  bunch->sigma_xx=(R_MATRIX*)xmalloc(sizeof(R_MATRIX)*n_slice);
  bunch->sigma_xy=(R_MATRIX*)xmalloc(sizeof(R_MATRIX)*n_slice);
#endif
  /* keep here or move (if it is constant) */
#ifdef LONGITUDINAL
  bunch->z_position=(double*)xmalloc(sizeof(double)*n_slice);
#endif
  bunch->particle_beam=false;
  // EA: init CSR wake parameters
  bunch->csrwake=(CSRWAKE_DATA*)xmalloc(sizeof(CSRWAKE_DATA));
  bunch->csrwake->terminal_dE_ds = NULL;
  bunch->csrwake->terminal_nlambda = NULL;
  bunch->csrwake->wake_enabled = 0;
  bunch->csrwake->distance_from_sbend = 0;
  bunch->csrwake->attenuation_length = 0;
  bunch->csrwake->nbins = 0;

  /// start of beam:
  bunch->s=0.0;
  bunch->is_lost=false;
  bunch->is_lost_bpm=false;
  return bunch;
}

BEAM *bunch_make(int n_bunch,int n_slice,int n_macro,int n_field,int n_max, int n_halo)
{
  BEAM *bunch;
  int i;

  bunch=bunch_make_2(n_bunch,n_slice,n_macro,n_field,n_max,n_halo);

  bunch->drive_data=(DRIVE_DATA*)xmalloc(sizeof(DRIVE_DATA));
  bunch->drive_data->ypos=(double*)xmalloc(sizeof(double)*n_bunch);
  bunch->drive_data->yposb=(double*)xmalloc(sizeof(double)*n_bunch);
  bunch->drive_data->yoff=(double*)xmalloc(sizeof(double)*n_bunch);
#ifdef TWODIM
  bunch->drive_data->xpos=(double*)xmalloc(sizeof(double)*n_bunch);
  bunch->drive_data->xposb=(double*)xmalloc(sizeof(double)*n_bunch);
#endif
  /*scdtmp*/
  bunch->drive_data->along=(double*)xmalloc(sizeof(double)*n_bunch);
  bunch->drive_data->blong=(double*)xmalloc(sizeof(double)*n_bunch);
  bunch->drive_data->factor_long=(double*)xmalloc(sizeof(double)*n_bunch);
  bunch->drive_data->factor_kick=(double*)xmalloc(sizeof(double)*n_bunch);
  /*scdtmp*/
  bunch->drive_data->af=(double*)xmalloc(sizeof(double)*n_slice*n_bunch*NSTEP);
  bunch->drive_data->bf=(double*)xmalloc(sizeof(double)*n_slice*n_bunch*NSTEP);

  bunch->field=field_make(n_slice,n_bunch);

  bunch->rho_y=(double**)xmalloc(sizeof(double*)*n_bunch);
  for (i=0;i<n_bunch;i++){
    bunch->rho_y[i]=(double*)xmalloc(sizeof(double)*n_slice);
  }
#ifdef TWODIM
  bunch->rho_x=(double**)xmalloc(sizeof(double*)*n_bunch);
  for (i=0;i<n_bunch;i++){
    bunch->rho_x[i]=(double*)xmalloc(sizeof(double)*n_slice);
  }
#endif
  /*
    new for the arbitrary phase 
    should be modified if more than one structure is possible
  */
  bunch->s_long=(double**)xmalloc(sizeof(double*)*n_field);
  bunch->c_long=(double**)xmalloc(sizeof(double*)*n_field);
  for (i=0;i<n_field;i++) {
    bunch->s_long[i]=(double*)xmalloc(sizeof(double)*n_bunch*n_slice);
    bunch->c_long[i]=(double*)xmalloc(sizeof(double)*n_bunch*n_slice);
  }

  bunch->drive_data->do_filter=0;
  bunch->factor=1.0;
  bunch->transv_factor=1.0;
  bunch->last_wgt=1.0;

  bunch->quad_kick_data=NULL;
  /* if not longitudinal, define z position per slice per bunch: */
#ifndef LONGITUDINAL
  bunch->z_position=(double*)xmalloc(sizeof(double)*n_bunch*n_slice);
#endif
  bunch->particle_number=(int*)xmalloc(sizeof(int)*n_bunch*n_slice);
#ifdef HTGEN
  bunch->particle_number_sec = (int*)xmalloc(sizeof(int)*n_bunch*n_slice);
#endif
  bunch->particle_number_tmp=(int*)xmalloc(sizeof(int)*n_bunch*n_slice);
  for (i=0;i<n_bunch*n_slice;i++){
      bunch->particle_number[i]=n_macro;
      bunch->particle_number_tmp[i]=0;
#ifdef HTGEN
      bunch->particle_number_sec[i]=0; //set the number of haloparticles at the beginning to 0
#endif
  }
  return bunch;
}

BEAM *bunch_remake(BEAM *bunch)
{
  BEAM *tb;
  /* scd changed to bunch_make_2*/
  tb=bunch_make_2(bunch->bunches,bunch->slices_per_bunch,
		  bunch->macroparticles,bunch->n_field,bunch->n_max,bunch->nhalo);
  tb->field=bunch->field;
  tb->which_field=bunch->which_field;
  tb->drive_data=bunch->drive_data;
  tb->factor=bunch->factor;
  tb->transv_factor=bunch->transv_factor;
  tb->quad_kick_data=bunch->quad_kick_data;
  tb->s_long=bunch->s_long;
  tb->c_long=bunch->c_long;
  tb->last_wgt=bunch->last_wgt;
  return tb;
}

double envelope_y(BEAM *beam)
{
  double env=0.0,size;
  int i,n;
  n=beam->slices;
  for(i=0;i<n;i++){
    size=beam->sigma[i].r11;
    env=std::max(env,size);
  }
  return sqrt(env);
}

double envelope_x(BEAM *beam)
{
  double env=0.0,size;
#ifdef TWODIM
  int i,n;
  n=beam->slices;
  for(i=0;i<n;i++){
    size=beam->sigma_xx[i].r11;
    env=std::max(env,size);
  }
#endif
  return sqrt(env);
}

double beam_rms_y(BEAM *bunch)
{
  int i,n;
  double sum11,y,ym,wgtsum,wgt;
  R_MATRIX sigma0;

  n=bunch->slices;
  ym=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
  }
  ym/=wgtsum;
  sum11=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-ym;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
  }
  sum11/=wgtsum;
  return sqrt(sum11);
}

#ifdef TWODIM
double beam_rms_x(BEAM *bunch)
{
  int i,n;
  double sum11,x,xm,wgtsum,wgt;
  R_MATRIX sigma0;

  n=bunch->slices;
  xm=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    xm+=wgt*bunch->particle[i].x;
  }
  xm/=wgtsum;
  sum11=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    x=bunch->particle[i].x-xm;
    sum11+=(x*x+bunch->sigma_xx[i].r11)*wgt;
  }
  sum11/=wgtsum;
  return sqrt(sum11);
}
#endif

double sigma_y(BEAM *bunch)
{
  int i;
  double tmp=0.0;
  for (i=0;i<bunch->slices;i++){
    tmp+=bunch->sigma[i].r11*bunch->particle[i].wgt;
  }
  return sqrt(tmp);
}

#ifdef TWODIM
double sigma_x(BEAM *bunch)
{
  int i;
  double tmp=0.0;
  for (i=0;i<bunch->slices;i++){
    tmp+=bunch->sigma_xx[i].r11*bunch->particle[i].wgt;
  }
  return sqrt(tmp);
}
#endif

void bunch_set_sigma_yy(BEAM *bunch,int nb,int slice,int part,
		   double beta,double alpha,double eps)
{
  slice=bunch->get_index(nb,slice,part);
  bunch_set_sigma_yy(bunch,slice,beta,alpha,eps);
}
void bunch_set_sigma_yy(BEAM *bunch,int index,
		   double beta,double alpha,double eps)
{
  bunch->sigma[index].r11=beta*eps;
  bunch->sigma[index].r12=-alpha*eps;
  bunch->sigma[index].r21=-alpha*eps;
  bunch->sigma[index].r22=(1.0+alpha*alpha)/beta*eps;
}

#ifdef TWODIM

void bunch_set_sigma_xx(BEAM *bunch,int nb,int slice,int part,
		   double beta,double alpha,double eps)
{
  slice=bunch->get_index(nb,slice,part);
  bunch_set_sigma_xx(bunch,slice,beta,alpha,eps);
}

void bunch_set_sigma_xx(BEAM *bunch,int index,
		   double beta,double alpha,double eps)
{
  bunch->sigma_xx[index].r11=beta*eps;
  bunch->sigma_xx[index].r12=-alpha*eps;
  bunch->sigma_xx[index].r21=-alpha*eps;
  bunch->sigma_xx[index].r22=(1.0+alpha*alpha)/beta*eps;
}

void bunch_set_sigma_xy(BEAM *bunch,int nb,int slice,int part)
{
  slice=bunch->get_index(nb,slice,part);
  bunch_set_sigma_xy(bunch,slice);
}
void bunch_set_sigma_xy(BEAM *bunch,int index)
{
  bunch->sigma_xy[index].r11=0.0;
  bunch->sigma_xy[index].r12=0.0;
  bunch->sigma_xy[index].r21=0.0;
  bunch->sigma_xy[index].r22=0.0;
}

#endif

void bunch_set_slice_energy(BEAM *beam,int nb,int slice,int part,
			    double energy)
{
  slice=beam->get_index(nb,slice,part);
  if (slice>=beam->slices||slice<0){
    my_error("bunch_set_slice_energy");
  }
  beam->particle[slice].energy=energy;
}

void bunch_set_slice_wgt(BEAM *bunch,int nb,int slice,int part,double wgt)
{
  slice=bunch->get_index(nb,slice,part);
  if (slice>=bunch->slices||slice<0){
    my_error("bunch_set_slice_wgt");
  }
  bunch->particle[slice].wgt=wgt;
}

void bunch_set_slice_y(BEAM *bunch,int nb,int slice,int part,double y)
{
  slice=bunch->get_index(nb,slice,part);
  if (slice>=bunch->slices||slice<0){
    my_error("bunch_set_slice_y");
  }
  bunch->particle[slice].y=y;
}

void bunch_set_slice_yp(BEAM *bunch,int nb,int slice,int part,double yp)
{
  slice=bunch->get_index(nb,slice,part);
  if (slice>=bunch->slices||slice<0){
    my_error("bunch_set_slice_yp");
  }
  bunch->particle[slice].yp=yp;
}

#ifdef TWODIM

void bunch_set_slice_x(BEAM *bunch,int nb,int slice,int part,double x)
{
  slice=bunch->get_index(nb,slice,part);
  if (slice>=bunch->slices||slice<0){
    my_error("bunch_set_slice_x");
  }
  bunch->particle[slice].x=x;
}

void bunch_set_slice_xp(BEAM *bunch,int nb,int slice,int part,double xp)
{
  slice=bunch->get_index(nb,slice,part);
  if (slice>=bunch->slices||slice<0){
    my_error("bunch_set_slice_xp");
  }
  bunch->particle[slice].xp=xp;
}

#endif

#ifdef TWODIM

/// rotate bunch counterclockwise by angle theta
void bunch_rotate_0(BEAM *bunch,double theta)
{
  if (fabs(theta)<=std::numeric_limits<double>::epsilon()) return;
  double s,c;
  sincos(theta,&s,&c);
  for (int i=0;i<bunch->slices;i++){
    PARTICLE &particle=bunch->particle[i];
    double tmp=c*particle.x-s*particle.y;
    particle.y=s*particle.x+c*particle.y;
    particle.x=tmp;
    tmp=c*particle.xp-s*particle.yp;
    particle.yp=s*particle.xp+c*particle.yp;
    particle.xp=tmp;
  }
#ifdef HTGEN
  for (int j=0;j<bunch->nhalo;j++){
    PARTICLE &particle_sec=bunch->particle_sec[j];
    double tmp=c*particle_sec.x-s*particle_sec.y;
    particle_sec.y=s*particle_sec.x+c*particle_sec.y;
    particle_sec.x=tmp;
    tmp=c*particle_sec.xp-s*particle_sec.yp;
    particle_sec.yp=s*particle_sec.xp+c*particle_sec.yp;
    particle_sec.xp=tmp;
  }
#endif
}

/// rotate bunch counterclockwise by angle theta
void bunch_rotate(BEAM *bunch,double theta)
{
  if (placet_switch.first_order || bunch->particle_beam) {
    return bunch_rotate_0(bunch,theta);
  }

  if (fabs(theta)<=std::numeric_limits<double>::epsilon()) return;
  double s,c;
  sincos(theta,&s,&c);
  for (int i=0;i<bunch->slices;i++){
    PARTICLE &particle=bunch->particle[i];
    double tmp=c*particle.x-s*particle.y;
    particle.y=s*particle.x+c*particle.y;
    particle.x=tmp;
    tmp=c*particle.xp-s*particle.yp;
    particle.yp=s*particle.xp+c*particle.yp;
    particle.xp=tmp;

    R_MATRIX tmp1;
    tmp1.r11=c*c*bunch->sigma_xx[i].r11-2.0*c*s*bunch->sigma_xy[i].r11
      +s*s*bunch->sigma[i].r11;
    tmp1.r12=c*c*bunch->sigma_xx[i].r12-c*s*(bunch->sigma_xy[i].r12
					     +bunch->sigma_xy[i].r21)
      +s*s*bunch->sigma[i].r12;
    tmp1.r22=c*c*bunch->sigma_xx[i].r22-2.0*c*s*bunch->sigma_xy[i].r22
      +s*s*bunch->sigma[i].r22;

    R_MATRIX tmp2;
    tmp2.r11=c*c*bunch->sigma[i].r11+2.0*c*s*bunch->sigma_xy[i].r11
      +s*s*bunch->sigma_xx[i].r11;
    tmp2.r12=c*c*bunch->sigma[i].r12+c*s*(bunch->sigma_xy[i].r12
					  +bunch->sigma_xy[i].r21)
      +s*s*bunch->sigma_xx[i].r12;
    tmp2.r22=c*c*bunch->sigma[i].r22+2.0*c*s*bunch->sigma_xy[i].r22
      +s*s*bunch->sigma_xx[i].r22;

    bunch->sigma_xy[i].r11=(c*c-s*s)*bunch->sigma_xy[i].r11
      +c*s*(bunch->sigma_xx[i].r11-bunch->sigma[i].r11);
    /* changed sign from + to - 1.10.1999 */
    tmp=c*c*bunch->sigma_xy[i].r12
      -s*s*bunch->sigma_xy[i].r21
      +c*s*(bunch->sigma_xx[i].r12-bunch->sigma[i].r12);
    bunch->sigma_xy[i].r21=c*c*bunch->sigma_xy[i].r21
      -s*s*bunch->sigma_xy[i].r12
      +c*s*(bunch->sigma_xx[i].r21-bunch->sigma[i].r21);
    bunch->sigma_xy[i].r12=tmp;
    bunch->sigma_xy[i].r22=(c*c-s*s)*bunch->sigma_xy[i].r22
      +c*s*(bunch->sigma_xx[i].r22-bunch->sigma[i].r22);

    bunch->sigma[i].r11=tmp2.r11;
    bunch->sigma[i].r12=tmp2.r12;
    bunch->sigma[i].r21=tmp2.r12;
    bunch->sigma[i].r22=tmp2.r22;
    bunch->sigma_xx[i].r11=tmp1.r11;
    bunch->sigma_xx[i].r12=tmp1.r12;
    bunch->sigma_xx[i].r21=tmp1.r12;
    bunch->sigma_xx[i].r22=tmp1.r22;
  }
#ifdef HTGEN
  for (int j=0;j<bunch->nhalo;j++){
    PARTICLE &particle_sec=bunch->particle_sec[j];
    double tmp=c*particle_sec.x-s*particle_sec.y;
    particle_sec.y=s*particle_sec.x+c*particle_sec.y;
    particle_sec.x=tmp;
    tmp=c*particle_sec.xp-s*particle_sec.yp;
    particle_sec.yp=s*particle_sec.xp+c*particle_sec.yp;
    particle_sec.xp=tmp;
  }
#endif
}

#endif

void element_add_offset(ELEMENT *element,double y,double yp)
{
  element->offset.y+=y;
  element->offset.yp+=yp;
}

#ifdef TWODIM

void element_add_offset_x(ELEMENT *element,double x,double xp)
{
  element->offset.x+=x;
  element->offset.xp+=xp;
}

#endif

void element_set_offset_to(ELEMENT *element,double y,double yp)
{
  element->offset.y=y;
  element->offset.yp=yp;
}

void bpm_init(int cav_bpm,int all)
{
  bpm_data.all=all;
  bpm_data.cav_bpm=cav_bpm;
}

GIRDER* make_drift_girder_new(int /*field*/)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.083,girderdrift;
  double z;

  /* scd test1*/
  bpmlength=rf_data.bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+bpmlength;
  //  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  girder=new GIRDER(girderlength);

  element=new DRIFT(girderlength);
  z=0.0;
  girder->add_element(element,z);

  return girder;
}

GIRDER* make_cav_girder_new(double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength,girderdrift;
  double z,driftlength;

  /* scd test1*/
  bpmlength=rf_data.bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+bpmlength;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  girder=new GIRDER(girderlength);

  z=-0.5*girderlength;

  driftlength=rf_data.cavdrift+rf_data.bpmlength;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  driftlength=2.0*rf_data.cavdrift;

  element=(CAVITY*)new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=rf_data.girderdrift+rf_data.cavdrift;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_bpm_girder_new(double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.085,girderdrift;
  double z,driftlength;

  /* scd test1*/
  bpmlength=rf_data.bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+bpmlength;
  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;

  girder=new GIRDER(girderlength);

  z=-0.5*girderlength;

  // if (bpm_data.cav_bpm){
  //   element=new BPM(bpmlength);
  // }
  // else{
  element=new BPM(bpmlength);
  // }
  z+=0.5*bpmlength;
  girder->add_element(element,z);
  z+=0.5*bpmlength;

  element=new DRIFT(0.5*driftlength);
  z+=0.5*0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=rf_data.girderdrift+rf_data.cavdrift;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_quad_girder_1_new(double qstr,double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength,quadlength=0.46,girderdrift;
  double driftlength;
  double z;

  /* scd test1*/
  bpmlength=rf_data.q_bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+rf_data.bpmlength;
  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  quadlength=1.0*(rf_data.cavlength+2.0*rf_data.cavdrift)-0.08;
  quadlength+=rf_data.bpmlength-rf_data.q_bpmlength;

  girder=new GIRDER(girderlength);
  z=-0.5*girderlength;
  /* scdtmp */
  // if ((bpm_data.all==0)||((qstr>0.0)&&(bpm_data.all>0))
  //     ||((qstr<0.0)&&(bpm_data.all<0))){
  //   element=new BPM(bpmlength);
  // }
  // else{
  element=new BPM(bpmlength);
  // }

  z+=0.5*bpmlength;
  girder->add_element(element,z);
  z+=0.5*bpmlength;

  element=new DRIFT(0.04);
  z+=0.5*0.04;
  girder->add_element(element,z);
  z+=0.5*0.04;

  element=new QUADRUPOLE(quadlength,qstr*quadlength);
  z+=0.5*quadlength;
  girder->add_element(element,z);
  z+=0.5*quadlength;

  element=new DRIFT(0.04+0.5*driftlength);
  z+=0.5*(0.04+0.5*driftlength);
  girder->add_element(element,z);
  z+=0.5*(0.04+0.5*driftlength);

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=0.5*driftlength+rf_data.girderdrift;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_quad_girder_2_new(double qstr,double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.085,quadlength=1.0,girderdrift;
  double driftlength;
  double z;

  /* scd test1*/
  bpmlength=rf_data.q_bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+rf_data.bpmlength;
  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  quadlength=2.0*(rf_data.cavlength+2.0*rf_data.cavdrift)-0.08;
  quadlength+=rf_data.bpmlength-rf_data.q_bpmlength;

  girder=new GIRDER(girderlength);
  z=-0.5*girderlength;
  /* scdtmp */
  // if ((bpm_data.all==0)||((qstr>0.0)&&(bpm_data.all>0))
  //     ||((qstr<0.0)&&(bpm_data.all<0))){
  //   element=new BPM(bpmlength);
  // }
  // else{
  element=new BPM(bpmlength);
  // }

  z+=0.5*bpmlength;
  girder->add_element(element,z);
  z+=0.5*bpmlength;

  element=new DRIFT(0.04);
  z+=0.5*0.04;
  girder->add_element(element,z);
  z+=0.5*0.04;

  element=new QUADRUPOLE(quadlength,qstr*quadlength);
  z+=0.5*quadlength;
  girder->add_element(element,z);
  z+=0.5*quadlength;

  element=new DRIFT(0.04+0.5*driftlength);
  z+=0.5*(0.04+0.5*driftlength);
  girder->add_element(element,z);
  z+=0.5*(0.04+0.5*driftlength);

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=0.5*driftlength+rf_data.girderdrift;
  element=new DRIFT(0.06);
  z+=0.5*0.06;
  girder->add_element(element,z);
  z+=0.5*0.06;

  return girder;
}

GIRDER* make_quad_girder_3_new(double qstr,double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.085,quadlength=1.54,girderdrift;
  double driftlength;
  double z;

  /* scd test1*/
  bpmlength=rf_data.q_bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+rf_data.bpmlength;
  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  quadlength=3.0*(rf_data.cavlength+2.0*rf_data.cavdrift)-0.08;
  quadlength+=rf_data.bpmlength-rf_data.q_bpmlength;

  girder=new GIRDER(girderlength);
  z=-0.5*girderlength;
  /* scdtmp */
  // if ((bpm_data.all==0)||((qstr>0.0)&&(bpm_data.all>0))
  //     ||((qstr<0.0)&&(bpm_data.all<0))){
  //   element=new BPM(bpmlength);
  // }
  // else{
  element=new BPM(bpmlength);
  // }

  z+=0.5*bpmlength;
  girder->add_element(element,z);
  z+=0.5*bpmlength;

  element=new DRIFT(0.04);
  z+=0.5*0.04;
  girder->add_element(element,z);
  z+=0.5*0.04;

  element=new QUADRUPOLE(quadlength,qstr*quadlength);
  z+=0.5*quadlength;
  girder->add_element(element,z);
  z+=0.5*quadlength;

  element=new DRIFT(0.04+0.5*driftlength);
  z+=0.5*(0.04+0.5*driftlength);
  girder->add_element(element,z);
  z+=0.5*(0.04+0.5*driftlength);

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=rf_data.cavdrift+rf_data.girderdrift;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_quad_girder_4_new(double qstr,double /*phase*/)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.085,quadlength=2.08,girderdrift;
  double driftlength;
  double z;

  /* scd test1*/
  bpmlength=rf_data.q_bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+rf_data.bpmlength;
  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  quadlength=4.0*(rf_data.cavlength+2.0*rf_data.cavdrift)-0.08;
  quadlength+=rf_data.bpmlength-rf_data.q_bpmlength;

  girder=new GIRDER(girderlength);
  z=-0.5*girderlength;
  /* scdtmp */
  // if ((bpm_data.all==0)||((qstr>0.0)&&(bpm_data.all>0))
  //     ||((qstr<0.0)&&(bpm_data.all<0))){
  //   element=new BPM(bpmlength);
  // }
  // else{
  element=new BPM(bpmlength);
  // }

  z+=0.5*bpmlength;
  girder->add_element(element,z);
  z+=0.5*bpmlength;

  element=new DRIFT(0.04);
  z+=0.5*0.04;
  girder->add_element(element,z);
  z+=0.5*0.04;

  element=new QUADRUPOLE(quadlength,qstr*quadlength);
  z+=0.5*quadlength;
  girder->add_element(element,z);
  z+=0.5*quadlength;

  driftlength=rf_data.girderdrift+0.04;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_quad_girder_1_start(double qstr,double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.085,quadlength=0.46,girderdrift;
  double driftlength;
  double z;

  /* scd test1*/
  bpmlength=rf_data.q_bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+rf_data.bpmlength;
  driftlength=2.0*rf_data.cavdrift;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  quadlength=1.0*(rf_data.cavlength+2.0*rf_data.cavdrift)-0.08;
  quadlength+=rf_data.bpmlength-rf_data.q_bpmlength;

  girder=new GIRDER(girderlength);
  z=-0.5*girderlength;

  z+=0.5*bpmlength;
  z+=0.5*bpmlength;

  z+=0.5*0.04;
  z+=0.5*0.04;

  element=new QUADRUPOLE(quadlength*0.5,qstr*quadlength*0.5);
  z+=0.75*quadlength;
  girder->add_element(element,z);
  z+=0.25*quadlength;

  element=new DRIFT(0.04+0.5*driftlength);
  z+=0.5*(0.04+0.5*driftlength);
  girder->add_element(element,z);
  z+=0.5*(0.04+0.5*driftlength);

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder->add_element(element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=rf_data.cavdrift+rf_data.girderdrift;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder->add_element(element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_quad_girder_end_new(double qstr)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=2.245,bpmlength=0.085,quadlength=0.46,girderdrift;
  double z;

  /* scd test1*/
  bpmlength=rf_data.q_bpmlength;
  girderdrift=rf_data.girderdrift+8.0*rf_data.cavdrift+rf_data.bpmlength;
  girderlength=4.0*rf_data.cavlength+girderdrift-0.035;
  quadlength=4.0*(rf_data.cavlength+rf_data.cavdrift)-0.08;
  quadlength+=rf_data.bpmlength-rf_data.q_bpmlength;

  girder=new GIRDER(girderlength);
  z=-0.5*girderlength;
  /* scdtmp */
  // if ((bpm_data.all==0)||((qstr>0.0)&&(bpm_data.all>0))
  //     ||((qstr<0.0)&&(bpm_data.all<0))){
  //   element=new BPM(bpmlength);
  // }
  // else{
  element=new BPM(bpmlength);
  // }

  z+=0.5*bpmlength;
  girder->add_element(element,z);
  z+=0.5*bpmlength;

  return girder;
}

/* end of new routines */

void quadrupoles_set(BEAMLINE *beamline,double strength[])
{
  int i;
  if (strength){
    for (i=0;i<beamline->n_quad;i++){
      /*    placet_printf(INFO,"%d %g\n",i,strength[i]);*/
      beamline->quad[i]->set_strength(strength[i]*beamline->quad[i]->get_length());
    }
  }
}

void
quadrupoles_bin_set(BEAMLINE *beamline,BIN *bin,double strength[])
{
  int i,j;
  for (i=0;i<bin->nq;i++){
    j=bin->quad[i];
    if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
      quad->set_strength(strength[quad->get_number()]*quad->get_length());
    }
  }
}

void
quadrupoles_bin_set_b(BEAMLINE *beamline,BIN *bin,double strength[],
		      double strength0[])
{
  int i,j;
  for (i=1;i<bin->nq;i++){
    j=bin->quad[i];
    if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
      quad->set_strength(strength[quad->get_number()]*quad->get_length());
    }
  }
  j=bin->quad[0];
  if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
    quad->set_strength(ballistic_data.last_quad*strength0[quad->get_number()]*quad->get_length());
  }
}

void
errors_fill(double s[],int n,double sigma,double cut)
{
  int i;
  for (i=0;i<n;i++){
      s[i]=Misalignments.Gauss(sigma,cut);
    }
}
  

void
quadrupoles_bin_set_position_error(BEAMLINE *beamline,BIN *bin,
				   double position[],int direction)
{
  int i,j;
  if (direction>0){
    for (i=0;i<bin->nq;i++){
      j=bin->quad[i];
      beamline->element[j]->offset.y+=position[beamline->element[j]->get_number()];
    }
  }
  else{
    for (i=0;i<bin->nq;i++){
      j=bin->quad[i];
      beamline->element[j]->offset.y+=-position[beamline->element[j]->get_number()];
    }
  }
}

void quadrupoles_bin_set_position_error_b(BEAMLINE *beamline,BIN *bin,
				     double position[],int direction)
{
  int i,j;
  if (direction>0){
    for (i=1;i<bin->nq;i++){
      j=bin->quad[i];
      ELEMENT *element=beamline->element[j];
      element->offset.y+=position[element->get_number()];
    }
  }
  else{
    for (i=1;i<bin->nq;i++){
      j=bin->quad[i];
      ELEMENT *element=beamline->element[j];
      element->offset.y+=-position[element->get_number()];
    }
  }
}

#ifdef TWODIM
void quadrupoles_bin_set_position_error_b_x(BEAMLINE *beamline,BIN *bin,
				       double position_x[],double position_y[],
				       int direction)
{
  int i,j;
  if (direction>0){
    for (i=1;i<bin->nq;i++){
      j=bin->quad[i];
      ELEMENT *element=beamline->element[j];
      element->offset.x+=position_x[element->get_number()];
      element->offset.y+=position_y[element->get_number()];
    }
  } else {
    for (i=0;i<bin->nq;i++){
      j=bin->quad[i];
      ELEMENT *element=beamline->element[j];
      element->offset.x+=-position_x[element->get_number()];
      element->offset.y+=-position_y[element->get_number()];
    }
  }
}
#endif

void quadrupoles_bin_set_error(BEAMLINE *beamline,BIN *bin,double strength[],
			       double sigma)
{
  int i,j;
  for (i=0;i<bin->nq;i++){
    j=bin->quad[i];
    if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
      quad->set_strength(strength[quad->get_number()]*quad->get_length()*(1.0+sigma*Misalignments.Gauss(sigma)));
    }
  }
}

void quadrupoles_bin_set_error_b(BEAMLINE *beamline,BIN *bin,double strength[],
				 double strength0[],double sigma)
{
  int i,j;

  j=bin->quad[0];
  if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
    quad->set_strength(ballistic_data.last_quad*strength0[quad->get_number()]*quad->get_length());
  }
  
  for (i=1;i<bin->nq;i++){
    j=bin->quad[i];
    if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
      quad->set_strength(strength[quad->get_number()]*quad->get_length()*(1.0+sigma*Misalignments.Gauss(sigma)));
    }
  }
}

/* This function returns the scaling value to achieve a given emittance growth
   scale-1. */

/* modify for last_wgt */

double emitt_y_scale(const BEAM *bunch,double scale)
{
  int i,n;
  double deta,detb,detab,p,q,y,yp,ym,ypm,wgtsum,wgt;
  R_MATRIX a,b;

  scale*=scale;
  a.r11=0.0;
  a.r12=0.0;
  a.r21=0.0;
  a.r22=0.0;
  b.r11=0.0;
  b.r12=0.0;
  b.r21=0.0;
  b.r22=0.0;
  n=bunch->slices;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-ym;
    yp=bunch->particle[i].yp-ypm;
    a.r11+=bunch->sigma[i].r11*wgt;
    a.r12+=bunch->sigma[i].r21*wgt;
    a.r22+=bunch->sigma[i].r22*wgt;
    b.r11+=y*y*wgt;
    b.r12+=y*yp*wgt;
    b.r22+=yp*yp*wgt;
  }
  a.r11/=wgtsum;
  a.r12/=wgtsum;
  a.r22/=wgtsum;
  b.r11/=wgtsum;
  b.r12/=wgtsum;
  b.r22/=wgtsum;
  deta=a.r11*a.r22-a.r12*a.r12;
  detb=b.r11*b.r22-b.r12*b.r12;
  detab=(a.r11+b.r11)*(a.r22+b.r22)-(a.r12+b.r12)*(a.r12+b.r12);
  p=-0.5*(detab-deta-detb)/detb;
  q=(1.0-scale)*deta/detb;
//  placet_printf(INFO,"%g %g %g\n",p,q,sqrt(p+sqrt(p*p-q)));
  return sqrt(p+sqrt(p*p-q));
//  return p+sqrt(p*p-q);
}

double emitt_y_range(const BEAM *bunch,int n1,int n2,double *off)
{
  int i;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-ym;
    yp=bunch->particle[i].yp-ypm;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  *off=ym;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;
}

double emitt_y_axis_range(const BEAM *bunch,int n1,int n2,double *off)
{
  int i;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    y=bunch->particle[i].y-ym;
    yp=bunch->particle[i].yp-ypm;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  *off=ym;
  /*
  return sqrt(sum11*sum22)
    *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;
    */
  return sqrt(sum11*sum22-sum12*sum12)
  *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;
}

#ifdef TWODIM
double emitt_x_range(const BEAM *bunch,int n1,int n2,double *off)
{
  int i;
  double sum11,sum12,sum22,x,xp,xm,xpm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  xm=0.0;
  xpm=0.0;
  wgtsum=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    xm+=wgt*bunch->particle[i].x;
    xpm+=wgt*bunch->particle[i].xp;
  }
  xm/=wgtsum;
  xpm/=wgtsum;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    x=bunch->particle[i].x-xm;
    xp=bunch->particle[i].xp-xpm;
    sum11+=(x*x+bunch->sigma_xx[i].r11)*wgt;
    sum12+=(x*xp+bunch->sigma_xx[i].r21)*wgt;
    sum22+=(xp*xp+bunch->sigma_xx[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  *off=xm;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;
}

double emitt_x_axis_range(const BEAM *bunch,int n1,int n2,double *off)
{
  int i;
  double sum11,sum12,sum22,x,xp,xm,xpm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  xm=0.0;
  xpm=0.0;
  wgtsum=0.0;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    x=bunch->particle[i].x-xm;
    xp=bunch->particle[i].xp-xpm;
    sum11+=(x*x+bunch->sigma_xx[i].r11)*wgt;
    sum12+=(x*xp+bunch->sigma_xx[i].r21)*wgt;
    sum22+=(xp*xp+bunch->sigma_xx[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  *off=xm;
  return sqrt(sum11*sum22)
    *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;
  /*
  return sqrt(sum11*sum22-sum12*sum12)
  *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;*/
}
#endif

double emitt_y_axis(const BEAM *bunch)
{
  int i,n;
  double sum11,sum12,sum22,y,yp,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  n=bunch->slices;
  wgtsum=0.0;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    y=bunch->particle[i].y;
    yp=bunch->particle[i].yp;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  /*
  return sqrt(sum11*sum22)
    *(bunch->particle)[bunch->slices/2].energy
    /EMASS*1e-5;
  */
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[bunch->slices/2].energy
    /EMASS*1e-5;
}

#ifdef TWODIM

double emitt_x_axis(const BEAM *bunch)
{
  int i,n;
  double sum11,sum12,sum22,x,xp,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  n=bunch->slices;
  wgtsum=0.0;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    x=bunch->particle[i].x;
    xp=bunch->particle[i].xp;
    sum11+=(x*x+bunch->sigma_xx[i].r11)*wgt;
    sum12+=(x*xp+bunch->sigma_xx[i].r21)*wgt;
    sum22+=(xp*xp+bunch->sigma_xx[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  return sqrt(sum11*sum22)
    *(bunch->particle)[bunch->slices/2].energy
    /EMASS*1e-5;
  /*
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[bunch->slices/2].energy
    /EMASS*1e-5;*/
}

#endif

double
emitt_y(const BEAM *beam)
{
  if (Andrea::UseDispersionFreeEmittance)
    return Andrea::emitt_y_disp_free(beam);
  
  int i,j,nb,ns,k;
  double sum11,sum12,sum22,y,yp,ym=0.0,ypm=0.0,wgtsum=0.0,wgt,sume=0.0;

  nb=beam->bunches;
  ns=beam->slices_per_bunch*beam->macroparticles;

  if (beam->particle_beam) {
    ns=beam->slices_per_bunch;
    k=0;
    for (i=0;i<ns*(nb-1);i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt);
	if (wgt!=0.0) {
	  wgtsum+=wgt;
	  ym+=wgt*beam->particle[k].y;
	  ypm+=wgt*beam->particle[k].yp;
	  sume+=beam->particle[k].wgt*fabs(beam->particle[k].energy);
	}
	k++;
      }
    }
    for (;i<ns*nb;i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  wgtsum+=wgt;
	  ym+=wgt*beam->particle[k].y;
	  ypm+=wgt*beam->particle[k].yp;
	  sume+=wgt*fabs(beam->particle[k].energy);
	}
	k++;
      }
    }
    if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
      sume/=wgtsum;
      ym/=wgtsum;
      ypm/=wgtsum;
    }
    sum11=0.0;
    sum12=0.0;
    sum22=0.0;
    k=0;
    for (i=0;i<ns*(nb-1);i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt);
	if (wgt!=0.0) {
	  y=beam->particle[k].y-ym;
	  yp=beam->particle[k].yp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
	k++;
      }
    }
    for (;i<ns*nb;i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  y=beam->particle[k].y-ym;
	  yp=beam->particle[k].yp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
	k++;
      }
    }
  }
  else {
    for (i=0;i<ns*(nb-1);i++){
      wgt=fabs(beam->particle[i].wgt);
      if (wgt!=0.0) {
	wgtsum+=wgt;
	ym+=wgt*beam->particle[i].y;
	ypm+=wgt*beam->particle[i].yp;
	sume+=wgt*fabs(beam->particle[i].energy);
      }
    }
    for (;i<ns*nb;i++){
      wgt=fabs(beam->particle[i].wgt*beam->last_wgt);
      if (wgt!=0.0) {
	wgtsum+=wgt;
	ym+=wgt*beam->particle[i].y;
	ypm+=wgt*beam->particle[i].yp;
	sume+=wgt*fabs(beam->particle[i].energy);
      }
    }
    if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
      sume/=wgtsum;
      ym/=wgtsum;
      ypm/=wgtsum;
    }
    sum11=0.0;
    sum12=0.0;
    sum22=0.0;
    if (placet_switch.first_order) {
      for (i=0;i<ns*(nb-1);i++){
	wgt=fabs(beam->particle[i].wgt);
	if (wgt!=0.0) {
	  y=beam->particle[i].y-ym;
	  yp=beam->particle[i].yp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
      }
      for (;i<ns*nb;i++){
	wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  y=beam->particle[i].y-ym;
	  yp=beam->particle[i].yp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
      }
    }
    else {
      for (i=0;i<ns*(nb-1);i++){
	wgt=fabs(beam->particle[i].wgt);
	if (wgt!=0.0) {
	  y=beam->particle[i].y-ym;
	  yp=beam->particle[i].yp-ypm;
	  sum11+=(y*y+beam->sigma[i].r11)*wgt;
	  sum12+=(y*yp+beam->sigma[i].r12)*wgt;
	  sum22+=(yp*yp+beam->sigma[i].r22)*wgt;
	}
      }
      for (;i<ns*nb;i++){
	wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  y=beam->particle[i].y-ym;
	  yp=beam->particle[i].yp-ypm;
	  sum11+=(y*y+beam->sigma[i].r11)*wgt;
	  sum12+=(y*yp+beam->sigma[i].r12)*wgt;
	  sum22+=(yp*yp+beam->sigma[i].r22)*wgt;
	}
      }
    }
  }
  if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
    sum11/=wgtsum;
    sum12/=wgtsum;
    sum22/=wgtsum;
  }
  return sqrt(sum11*sum22-sum12*sum12)
    *fabs(sume)/EMASS*1e-12/EMITT_UNIT;
}

#ifdef TWODIM

double
emitt_x(const BEAM *beam)
{
  if (Andrea::UseDispersionFreeEmittance)
    return Andrea::emitt_x_disp_free(beam);

  int i,j,nb,ns,k;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt,sume=0.0;

  nb=beam->bunches;
  ns=beam->slices_per_bunch*beam->macroparticles;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  if (beam->particle_beam) {
    ns=beam->slices_per_bunch;
    k=0;
    for (i=0;i<ns*(nb-1);i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt);
	if (wgt!=0.0) {
	  wgtsum+=wgt;
	  ym+=wgt*beam->particle[k].x;
	  ypm+=wgt*beam->particle[k].xp;
	  sume+=beam->particle[k].wgt*fabs(beam->particle[k].energy);
	}
	k++;
      }
    }
    for (;i<ns*nb;i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  wgtsum+=wgt;
	  ym+=wgt*beam->particle[k].x;
	  ypm+=wgt*beam->particle[k].xp;
	  sume+=wgt*fabs(beam->particle[k].energy);
	}
	k++;
      }
    }
    if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
      sume/=wgtsum;
      ym/=wgtsum;
      ypm/=wgtsum;
    }
    sum11=0.0;
    sum12=0.0;
    sum22=0.0;
    k=0;
    for (i=0;i<ns*(nb-1);i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt);
	if (wgt!=0.0) {
	  y=beam->particle[k].x-ym;
	  yp=beam->particle[k].xp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
	k++;
      }
    }
    for (;i<ns*nb;i++){
      for (j=0;j<beam->particle_number[i];++j) {
	wgt=fabs(beam->particle[k].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  y=beam->particle[k].x-ym;
	  yp=beam->particle[k].xp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
	k++;
      }
    }
  }
  else {
    for (i=0;i<ns*(nb-1);i++){
      wgt=fabs(beam->particle[i].wgt);
      if (wgt!=0.0) {
	wgtsum+=wgt;
	ym+=wgt*beam->particle[i].x;
	ypm+=wgt*beam->particle[i].xp;
	sume+=wgt*fabs(beam->particle[i].energy);
      }
    }
    for (;i<ns*nb;i++){
      wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
      if (wgt!=0.0) {
	wgtsum+=wgt;
	ym+=wgt*beam->particle[i].x;
	ypm+=wgt*beam->particle[i].xp;
	sume+=wgt*fabs(beam->particle[i].energy);
      }
    }
    if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
      sume/=wgtsum;
      ym/=wgtsum;
      ypm/=wgtsum;
    }
    sum11=0.0;
    sum12=0.0;
    sum22=0.0;
    if (placet_switch.first_order) {
      for (i=0;i<ns*(nb-1);i++){
	wgt=fabs(beam->particle[i].wgt);
	if (wgt!=0.0) {
	  y=beam->particle[i].x-ym;
	  yp=beam->particle[i].xp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
      }
      for (;i<ns*nb;i++){
	wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  y=beam->particle[i].x-ym;
	  yp=beam->particle[i].xp-ypm;
	  sum11+=y*y*wgt;
	  sum12+=y*yp*wgt;
	  sum22+=yp*yp*wgt;
	}
      }
    }
    else {
      for (i=0;i<ns*(nb-1);i++){
	wgt=fabs(beam->particle[i].wgt);
	if (wgt!=0.0) {
	  y=beam->particle[i].x-ym;
	  yp=beam->particle[i].xp-ypm;
	  sum11+=(y*y+beam->sigma_xx[i].r11)*wgt;
	  sum12+=(y*yp+beam->sigma_xx[i].r12)*wgt;
	  sum22+=(yp*yp+beam->sigma_xx[i].r22)*wgt;
	}
      }
      for (;i<ns*nb;i++){
	wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
	if (wgt!=0.0) {
	  y=beam->particle[i].x-ym;
	  yp=beam->particle[i].xp-ypm;
	  sum11+=(y*y+beam->sigma_xx[i].r11)*wgt;
	  sum12+=(y*yp+beam->sigma_xx[i].r12)*wgt;
	  sum22+=(yp*yp+beam->sigma_xx[i].r22)*wgt;
	}
      }
    }
  }
  if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
    sum11/=wgtsum;
    sum12/=wgtsum;
    sum22/=wgtsum;
  }
  return sqrt(sum11*sum22-sum12*sum12)
    *fabs(sume)/EMASS*1e-12/EMITT_UNIT;
}

#endif

void beam_add_offset_y(BEAM *bunch,double y)
{
  for (int i=0;i<bunch->slices;i++){
    bunch->particle[i].y+=y;
  }
}

void beam_add_offset_x(BEAM *bunch,double x)
{
#ifdef TWODIM
  for (int i=0;i<bunch->slices;i++){
    bunch->particle[i].x+=x;
  }
#endif
}

void longrange_fill_band_new(BEAM *bunch,double lambda0[],int nfreq)
{
  int i,j,nbunch,nslice,p=0,ifreq;
  double phi,a,b,lambda,l_inv;

  for (ifreq=0;ifreq<nfreq;ifreq++){
    lambda=lambda0[ifreq]*1e6;
    l_inv=TWOPI/lambda;
    nbunch=bunch->bunches;
    nslice=bunch->slices_per_bunch;
    for (j=0;j<nbunch;j++){
      for (i=0;i<nslice;i++){
	phi=bunch->z_position[j*nslice+i]*l_inv;
	a=sin(phi);
	b=cos(phi);
	bunch->drive_data->af[p]=a;
	bunch->drive_data->bf[p]=b;
	/*placet_printf(INFO,"%d %d %g %g %g %g\n",j,i,phi0,phi,a,b);*/
	p++;
      }
    }
  }
}

BEAM *make_multi_bunch(BEAMLINE *beamline,char *name,MAIN_BEAM_PARAM *param)
{
  FILE *file;
  BEAM *bunch;
  int i,n,k,nmacro,j,nbunches;
  double wgtsum,ecut,sigma_e,delta,fact;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy;
  double *store,elow,ehigh,de,sumwgt;
  double charge=1e11,e0=9.0; /*cavlen=2.6, a0=20.0*/

  nbunches=param->n_bunch;
  sigma_e=param->espread;
  ecut=param->ecut;
  nmacro=param->n_macro;
  charge=param->charge;

  epsx=param->emittx;
  epsy=param->emitty;
  alphax=param->alphax;
  alphay=param->alphay;
  betax=param->betax;
  betay=param->betay;
  e0=param->e0;

  epsx*=EMASS/e0*1e12*EMITT_UNIT;
  epsy*=EMASS/e0*1e12*EMITT_UNIT;
  file=read_file(name);
  fscanf(file,"%d",&n);
  /* scd to update for number of phases */
  bunch=bunch_make(nbunches,n,nmacro,10,0);
  bunch->drive_data->param.main=(MAIN_BEAM_PARAM*)
    xmalloc(sizeof(MAIN_BEAM_PARAM));
  *(bunch->drive_data->param.main)=*(param);
  fscanf(file,"%g",&tmp1);
  bunch->last_wgt=param->last_wgt;
  wgtsum=0.0;
  de=2.0*ecut/(double)(nmacro);
  store=(double*)xmalloc(sizeof(double)*nmacro);
  if (nmacro>1){
    sumwgt=0.0;
    elow=-ecut;
    for (k=0;k<nmacro;k++){
      ehigh=elow+de;
      placet_printf(VERYVERBOSE,"%g %g\n",elow,ehigh);
      store[k]=0.0;
      //      qromb(&gauss,elow,ehigh,&(store[k]));
      //
      //placet_printf(INFO,"c: %g %g\n",store[k],gauss_bin(elow,ehigh));
//scd check
      store[k]=gauss_bin(elow,ehigh);
      elow=ehigh;
      sumwgt+=store[k];
    }
    for (k=0;k<nmacro;k++){
      store[k]/=sumwgt;
      placet_printf(VERYVERBOSE,"wgt[%d]: %g\n",k,store[k]);
    }
  }
  for (i=0;i<n;i++){
    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (k=0;k<nmacro;k++){
      if (nmacro>1){
	/*	delta=ecut*(2.0*k/(double)(nmacro-1)-1.0);*/
	delta=-ecut+((double)k+0.5)*de;
	bunch_set_slice_energy(bunch,0,i,k,e0+delta*sigma_e);
	/*	fact=exp(-0.5*delta*delta);*/
	fact=store[k];
      }
      else{
	bunch_set_slice_energy(bunch,0,i,k,e0);
	fact=1.0;
	delta=0.0;
      }
      bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
      wgtsum+=tmp3*fact;
#ifdef TWODIM
      bunch_set_slice_x(bunch,0,i,k,0.0);
      bunch_set_slice_xp(bunch,0,i,k,0.0);
      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx/(1.0+delta*sigma_e/e0));
      bunch_set_sigma_xy(bunch,0,i,k);
#endif
      bunch_set_slice_y(bunch,0,i,k,zero_point);
      bunch_set_slice_yp(bunch,0,i,k,0.0);
      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy/(1.0+delta*sigma_e/e0));
    }
    bunch->field->de[i]=tmp2*1e-3;
    // for (j=0;j<beamline->n_phases;j++){
    //   /* scd xxx */
    //   bunch->acc_field[j][i]=cos(tmp1/(1e6*rf_data.lambda)*TWOPI
    // 	     +(beamline->phase[j]+param->phase)*PI/180.0);
    //   /*
    //   bunch->acc_field[j][i]=rf_data.gradient
    // 	*cos(tmp1/(1e6*rf_data.lambda)*TWOPI
    // 	     +(beamline->phase[j]+param->phase)*PI/180.0);
    //   */
    // }
    bunch->c_long[0][i]=cos(tmp1/(1e6*rf_data.lambda)*TWOPI
	     +param->phase*PI/180.0)*rf_data.gradient;
    bunch->s_long[0][i]=sin(tmp1/(1e6*rf_data.lambda)*TWOPI
	     +param->phase*PI/180.0)*rf_data.gradient;
    bunch->z_position[i]=tmp1;
  }
  /* scd new 27.6.97 */
  for (i=0;i<n*(n+1)/2;i++){
    fscanf(file,"%g",&tmp3);
    field_set_kick(bunch->field,i,tmp3*1e-3);
  }
  /* scd end */
  close_file(file);
  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }

  k=n*nmacro;
  for (j=1;j<nbunches;j++){
    for(i=0;i<n;i++){
      bunch->z_position[j*n+i]=bunch->z_position[i];
      bunch->c_long[0][j*n+i]=bunch->c_long[0][i];
      bunch->s_long[0][j*n+i]=bunch->s_long[0][i];
    }
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k]=bunch->particle[i];
      bunch->sigma[k]=bunch->sigma[i];

#ifdef TWODIM
      bunch->sigma_xx[k]=bunch->sigma_xx[i];
      bunch->sigma_xy[k]=bunch->sigma_xy[i];
#endif
      k++;
    }
  }
  k=n;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n;i++){
      bunch->field->de[k]=bunch->field->de[i];
      // for (j1=0;j1<beamline->n_phases;j1++){
      // 	bunch->acc_field[j1][k]=bunch->acc_field[j1][i];
      // }
      k++;
    }
  }

  for (i=1;i<nbunches;i++){
    bunch->drive_data->along[i]=param->long_range[i]*charge*ECHARGE*1e12*1e-6
      *1e-3;
    placet_printf(INFO,"%d %g\n",i,bunch->drive_data->along[i]);
  }
  bunch->drive_data->longrange_max=nbunches;

  //  if (nmacro>1){
  //    field_adjust(bunch); // function moved to obsolete
  //}
  free(store);
  bunch->which_field=3;
  if (nmacro>1) bunch->which_field=4;
  /* scd check*/
  if (nbunches==1){
    bunch->which_field=1;
    if (nmacro>1) bunch->which_field=123;
  }

#ifdef TWODIM
  placet_cout << VERBOSE << "emitt_x at creation = " << emitt_x(bunch) << " particle sum = " << wgtsum << endmsg;
#endif
  placet_cout << VERBOSE << "emitt_y at creation = " << emitt_y(bunch) << " particle sum = " << wgtsum << endmsg;
  return bunch;
}

int bunchinfo_cmp_z(const void *x1,const void *x2) // compare function for qsort, maybe move to mb_inj.cc? -- JS
{
  if (((BUNCHINFO*)x1)->z<((BUNCHINFO*)x2)->z) return -1;
  if (((BUNCHINFO*)x1)->z>((BUNCHINFO*)x2)->z) return 1;
  return 0;
}

/// get random number based on ATL law
double move_atl(double a, double t, double l)
{
  return sqrt(a*t*l)*Groundmotion.Gauss(1.,3.);
}

/** move girder/element according to ATL law with amplitude a, time t, length l,
    the function updates the end offsets y2 
    the function returns the middle offset y and angle yp 
*/
void move_atl_element(double a, double t, double length, double& y2, double &y, double &yp)
{
  if (length>0.0) {
    double y1=y2;
    y2+=move_atl(a,t,length);
    y=0.5*(y2+y1);
    yp=(y2-y1)/length;
  } else {
    y=y2;
    yp=0.0;
  }
}

/** move beamline according to ATL law with amplitude a and time t
    if move_girders!=0 only girders are moved, if 0 individual elements are moved 
    x = apply x-direction
    y = apply y-direction
    end_fixed = don't move end of beamline (e.g. for beamlines connected at IP)
    start, end = girder numbers of start and end of ATL-movement
 */
void beamline_move_atl(BEAMLINE* beamline,double a,double t,int move_girders,int apply_x,int apply_y,int end_fixed,int start,int end)
{
  double y2=0.0,y=0.0,yp=0.0;
  double x2=0.0,x=0.0,xp=0.0;
  GIRDER* girder;
  ELEMENT* element;
#ifndef TWODIM
  apply_x = 0;
#endif
  if (apply_x==0 && apply_y==0) return;
  int girder_cnt=0;
  girder=beamline->first;
  while(girder!=NULL) {
    // if girder before start skip it 
    if (girder_cnt < start) {
      girder=girder->next();
      girder_cnt++;
      continue;
    }
    // if girder after end, move it as last girder that was moved
    if (end > 0 && girder_cnt > end) {
      // no distinction between move girders or move elements
      if (apply_y) girder->move(y2,0);
      if (apply_x) girder->move_x(x2,0);
      girder=girder->next();
      girder_cnt++;
      continue;
    }
    if (move_girders) { // move girders
      double dist = girder->distance_to_prev_girder();
      if (dist>0.0) {
	if (apply_y) y2+=move_atl(a,t,dist);
	if (apply_x) x2+=move_atl(a,t,dist);
      }
      double length = girder->get_length();
      if (apply_y) {
	move_atl_element(a,t,length,y2,y,yp);
	girder->move(y,yp);
      }
      if (apply_x) {
	move_atl_element(a,t,length,x2,x,xp);
	girder->move_x(x,xp);
      }
    } else { // move elements
      element=girder->element();
      while(element!=NULL) {
	double length = element->get_length();
	if (apply_y) {
	  move_atl_element(a,t,length,y2,y,yp);
	  element->offset.y+=y;
	  element->offset.yp+=yp;
	}
	if (apply_x) {
	  move_atl_element(a,t,length,x2,x,xp);
	  element->offset.x+=x;
	  element->offset.xp+=xp;
	}
      }
      element=element->next;
    }
    girder=girder->next();
    girder_cnt++;
  }
  // if end is fixed then move beamline down by movement of the end of beamline
  if (end_fixed) {
    girder=beamline->first;
    while(girder!=NULL) {
      if (apply_y) girder->move(-y2,0);
      if (apply_x) girder->move_x(-x2,0);
      girder=girder->next();
    }
  }
}

void beamline_move_sine(BEAMLINE *beamline,double a,double k,double phase)
{
  double y1,y2,y,yp,yoff;
  GIRDER *girder;
  y2=0.0;
  girder=beamline->first;
  yoff=a*sin(phase);
  while(girder!=NULL){
    phase+=k*girder->distance_to_prev_girder();
    while (phase>TWOPI) phase=phase-TWOPI;
    y1=a*sin(phase);
    phase+=k*girder->get_length();
    while (phase>TWOPI) phase=phase-TWOPI;
    y2=a*sin(phase);
    y=0.5*(y2+y1);
    yp=(y2-y1)/girder->get_length();
    girder->move(y-yoff,yp);
    girder=girder->next();
  }
}

void beamline_move_sine2(BEAMLINE *beamline,double a,double k,double phase)
{
  double y,yoff;
  ELEMENT **element;
  int i;

  element=beamline->element;
  yoff=a*sin(phase);
  for (i=0;i<beamline->n_elements;i++){
    phase+=0.5*k*element[i]->get_length();
    while (phase>TWOPI) phase=phase-TWOPI;
    y=a*sin(phase);
    phase+=0.5*k*element[i]->get_length();
    element[i]->offset.y=y-yoff;
  }
}

void beamline_move_sine_list(BEAMLINE *beamline,int nel[],
			     double a,double k,double phase)
{
  double y,yoff;
  ELEMENT **element;
  int i,j=0;

  element=beamline->element;
  yoff=a*sin(phase);
  for (i=0;i<beamline->n_elements;i++){
    phase+=0.5*k*element[i]->get_length();
    while (phase>TWOPI) phase=phase-TWOPI;
    if (nel[j]<0) {
      break;
    } 
    else {
      if (i==nel[j]) {
	y=a*sin(phase);
	phase+=0.5*k*element[i]->get_length();
	element[i]->offset.y=y-yoff;
	j++;
      }
    }
  }
}

void field_read(FIELD ** field,double phase[],double scale[],int n,char *string)
{
  FILE *file;
  int i,nslice,j;
  float tmp1;
  file=read_file(string);
  fscanf(file,"%d",&nslice);
  fscanf(file,"%g",&tmp1);
  for (i=0;i<n;i++){
    field[i]=field_make(nslice,1);
  }
  tmp1*=rf_data.cavlength;
  for (j=0;j<n;j++){
    scale[j]=(rf_data.cavlength*rf_data.gradient
	      *cos(phase[j]*PI/180.0)+tmp1*1e-3)
      /(rf_data.cavlength*rf_data.gradient);
  }
  close_file(file);
}

void check_kscal(int *kscal,double estep[],double e)
{
  if (e>estep[*kscal]) (*kscal)++;
}

void lattice_data_init(int nsect,int nquad[],int nlong[],int ngirder[])
{
  int i;
  lattice_data.nsect=nsect;
  lattice_data.nquad=(int*)xmalloc(sizeof(int)*nsect);
  lattice_data.nlong=(int*)xmalloc(sizeof(int)*nsect);
  lattice_data.ngirder=(int*)xmalloc(sizeof(int)*nsect);
  for (i=0;i<nsect;i++){
    lattice_data.nquad[i]=nquad[i];
    lattice_data.nlong[i]=nlong[i];
    lattice_data.ngirder[i]=ngirder[i];
  }
}

void setup_TeV(char *name,char *beam_name,BEAMLINE *beamline)
{
  int i,j,isect,nsect,nquad[100],nlong[100],ngirder[100],iq,nq,kscal=0,nphase,
    j_tmp;
  FILE *file;
  GIRDER *girder;
  char *point,buffer[100];
  double k[7];
  double quad_set3[MAX_QUAD],quad_set4[MAX_QUAD];
  double driftlength=0.035,sign,sign0=-1.0,e,e0=9.0,de,ecav=9.0;

  double estep[10];
  double phase[10];
  double scal[10];

  file=read_file(name);
  point=fgets(buffer,100,file);
  rf_data.cavlength=strtod(buffer,&point);
  rf_data.gradient=strtod(point,&point);
  rf_data.cavdrift=strtod(point,&point);
  rf_data.bpmlength=strtod(point,&point);
  rf_data.girderdrift=strtod(point,&point);
  rf_data.q_bpmlength=strtod(point,&point);
  de=(rf_data.cavlength*rf_data.gradient);
  point=fgets(buffer,100,file);
  nsect=strtol(point,&point,10);
  placet_printf(INFO,"number of sectors: %d\n",nsect);
  for (isect=0;isect<nsect;isect++){
    point=fgets(buffer,100,file);
    nquad[isect]=strtol(point,&point,10);
    nlong[isect]=strtol(point,&point,10);
    ngirder[isect]=strtol(point,&point,10);
    placet_printf(INFO,"number of quads: %d\n",nquad[isect]);
  }
  point=fgets(buffer,100,file);
  nphase=strtol(point,&point,10);
  if (nphase>10){
    placet_printf(ERROR,"too many phases (%d) in input file\n",nphase);
    my_error("beamline_TeV");
  }
  for (i=0;i<nphase;i++){
    point=fgets(buffer,100,file);
    estep[i]=strtod(point,&point);
    phase[i]=strtod(point,&point);
  }
  field_read(field_cav,phase,scal,nphase,beam_name);

  /* skip two lines */
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);

  iq=0;
  nq=0;
  for (isect=0;isect<nsect;isect++){
    point=fgets(buffer,100,file);
    for (i=0;i<6;i++){
      k[i]=strtod(point,&point);
    }
    point=fgets(buffer,100,file);
    k[6]=strtod(point,&point);
    nq+=nquad[isect];
    for (i=iq;i<nq-3;i++){
      if (i%2){
	quad_set0[i]=k[1];
      }
      else{
	quad_set0[i]=k[0];
      }
    }
    quad_set0[nq-3]=k[2];
    quad_set0[nq-2]=k[3];
    quad_set0[nq-1]=k[4];
    iq=nq;
    if (isect+1<nsect){
      quad_set0[iq++]=k[5];
      quad_set0[iq++]=k[6];
    }
    if (isect==nsect-1){
      quad_set0[nq-3]=k[1];
      quad_set0[nq-2]=k[0];
      quad_set0[nq-1]=k[1];
    }
  }

  /* skip three lines */
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);

  iq=0;
  nq=0;
  for (isect=0;isect<nsect;isect++){
    point=fgets(buffer,100,file);
    for (i=0;i<6;i++){
      k[i]=strtod(point,&point);
    }
    point=fgets(buffer,100,file);
    k[6]=strtod(point,&point);
    nq+=nquad[isect];
    for (i=iq;i<nq-3;i++){
      if(i%2){
	quad_set1[i]=k[1];
      }
      else{
	quad_set1[i]=k[0];
      }
    }
    quad_set1[nq-3]=k[2];
    quad_set1[nq-2]=k[3];
    quad_set1[nq-1]=k[4];
    iq=nq;
    if (isect+1<nsect){
      quad_set1[iq++]=k[5];
      quad_set1[iq++]=k[6];
    }
    if (isect==nsect-1){
      quad_set1[nq-3]=k[1];
      quad_set1[nq-2]=k[0];
      quad_set1[nq-1]=k[1];
    }
  }

  /* skip three lines */
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);

  iq=0;
  nq=0;
  for (isect=0;isect<nsect;isect++){
    point=fgets(buffer,100,file);
    for (i=0;i<6;i++){
      k[i]=strtod(point,&point);
    }
    point=fgets(buffer,100,file);
    k[6]=strtod(point,&point);
    nq+=nquad[isect];
    for (i=iq;i<nq-3;i++){
      if (i%2){
	quad_set2[i]=k[1];
      }
      else{
	quad_set2[i]=k[0];
      }
    }
    quad_set2[nq-3]=k[2];
    quad_set2[nq-2]=k[3];
    quad_set2[nq-1]=k[4];
    iq=nq;
    if (isect+1<nsect){
      quad_set2[iq++]=k[5];
      quad_set2[iq++]=k[6];
    }
    if (isect==nsect-1){
      quad_set2[nq-3]=k[1];
      quad_set2[nq-2]=k[0];
      quad_set2[nq-1]=k[1];
    }
  }

  /* skip three lines */
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);

  iq=0;
  nq=0;
  for (isect=0;isect<nsect;isect++){
    point=fgets(buffer,100,file);
    for (i=0;i<6;i++){
      k[i]=strtod(point,&point);
    }
    point=fgets(buffer,100,file);
    k[6]=strtod(point,&point);
    nq+=nquad[isect];
    for (i=iq;i<nq-3;i++){
      if (i%2){
	quad_set3[i]=k[1];
      }
      else{
	quad_set3[i]=k[0];
      }
    }
    quad_set3[nq-3]=k[2];
    quad_set3[nq-2]=k[3];
    quad_set3[nq-1]=k[4];
    iq=nq;
    if (isect+1<nsect){
      quad_set3[iq++]=k[5];
      quad_set3[iq++]=k[6];
    }
    if (isect==nsect-1){
      if ((nq-3)%2){
	quad_set3[nq-3]=k[1];
	quad_set3[nq-2]=k[0];
	quad_set3[nq-1]=k[1];
      }
      else{
	quad_set3[nq-3]=k[0];
	quad_set3[nq-2]=k[1];
	quad_set3[nq-1]=k[0];
      }
    }
  }

  /* skip three lines */
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);
  point=fgets(buffer,100,file);

  iq=0;
  nq=0;
  for (isect=0;isect<nsect;isect++){
    point=fgets(buffer,100,file);
    for (i=0;i<6;i++){
      k[i]=strtod(point,&point);
    }
    point=fgets(buffer,100,file);
    k[6]=strtod(point,&point);
    nq+=nquad[isect];
    for (i=iq;i<nq-3;i++){
      if(i%2){
	quad_set4[i]=k[1];
      }
      else{
	quad_set4[i]=k[0];
      }
    }
    quad_set4[nq-3]=k[2];
    quad_set4[nq-2]=k[3];
    quad_set4[nq-1]=k[4];
    iq=nq;
    if (isect+1<nsect){
      quad_set4[iq++]=k[5];
      quad_set4[iq++]=k[6];
    }
    if (isect==nsect-1){
      if ((nq-3)%2){
	quad_set4[nq-3]=k[1];
	quad_set4[nq-2]=k[0];
	quad_set4[nq-1]=k[1];
      }
      else{
	quad_set4[nq-3]=k[0];
	quad_set4[nq-2]=k[1];
	quad_set4[nq-1]=k[0];
      }
    }
  }

  close_file(file);

  iq=0;
  sign=sign0;
  e=e0;
  for (isect=0;isect<nsect;isect++){
    for (i=0;i<nquad[isect];i++){
      quad_strength[iq]=quad_set0[iq]*sign;
      quad_set0[iq]*=e*sign;
      quad_set1[iq]*=e*sign;
      quad_set2[iq]*=e*sign;
      quad_set3[iq]*=e*sign;
      quad_set4[iq++]*=e*sign;
      switch(nlong[isect]){
      case 1:
	if ((isect==0)&&(i==0)){
	  girder=make_quad_girder_1_start(sign,phase[kscal]);
	}
	else{
	  girder=make_quad_girder_1_new(sign,phase[kscal]);
	}
	break;
      case 2:
	girder=make_quad_girder_2_new(sign,phase[kscal]);
	break;
      case 3:
	girder=make_quad_girder_3_new(sign,phase[kscal]);
	break;
      case 4:
	girder=make_quad_girder_4_new(sign,phase[kscal]);
	break;
      }
      e+=(4-nlong[isect])*de*scal[kscal];
      ecav+=(4-nlong[isect])*de;
      
      beamline->girder_add(girder,driftlength);
      j_tmp=0;
      for (j=0;j<ngirder[isect];j++){
	j_tmp++;
	if (j_tmp-bpm_data.cav_bpm==0){
	  girder=make_bpm_girder_new(phase[kscal]);
	  j_tmp=0;
	}
	else{
	  girder=make_cav_girder_new(phase[kscal]);
	}
	beamline->girder_add(girder,driftlength);
	e+=de*4.0*scal[kscal];
	ecav+=de*4.0;
      }
      check_kscal(&kscal,estep,e);
      sign=-sign;
    }
  }
  girder=make_quad_girder_end_new(sign);
  beamline->girder_add(girder,driftlength);
  beamline->set_name(name);
  beamline->set();
  quadrupoles_set(beamline,quad_set0);
  placet_printf(INFO,"kscal= %d at %g %g\n",kscal,e,ecav);
  lattice_data_init(nsect,nquad,nlong,ngirder);
  // beamline->n_phases=kscal+1;
  // for (i=0;i<=kscal;i++){
  //   beamline->phase[i]=phase[i];
  // }
  for (i=0;i<beamline->n_elements;i++){
    if (CAVITY *cav=beamline->element[i]->cavity_ptr()) {
     cav->set_gradient(rf_data.gradient);
    }
  }
}

void smooth_quad(BEAMLINE *beamline,double f,double l,double e)
{
  GIRDER *girder;
  ELEMENT *element;
  double s;
  s=e/f;
  element=new QUADRUPOLE(l,s);
  girder=new GIRDER(l);
  girder->add_element(element);
  beamline->girder_add(girder,0.0);
}

void smooth_drift(BEAMLINE *beamline,double l)
{
  GIRDER *girder;
  ELEMENT *element;
  element=new DRIFT(l);
  girder=new GIRDER(l);
  girder->add_element(element,0.0);
  beamline->girder_add(girder,0.0);
}

void smooth_bpm(BEAMLINE *beamline,double l)
{
  GIRDER *girder;
  ELEMENT *element;
  element=new BPM(l);
  girder=new GIRDER(l);
  girder->add_element(element,0.0);
  beamline->girder_add(girder,0.0);
}

void smooth_cav(BEAMLINE *beamline,double l,double phase)
{
  GIRDER *girder;
  ELEMENT *element;
  element=new CAVITY(l,0,phase);
  girder=new GIRDER(l);
  girder->add_element(element,0.0);
  beamline->girder_add(girder,0.0);
}

void setup_smooth(char *name,BEAMLINE *beamline)
{
  int i,iq,kscal=0,nphase,nc;
  FILE *file;
  char *point,buffer[100];
  double sign,sign0=-1.0,e,e0=9.0;
  double estep[10];
  double phase[10];
  double scal[10];
  double quad_len=0.01,cav_len,bpm_len=0.01;
  double grad,e_final,de,ksum=0.0;
  double alpha_l,alpha_f,l0,f0,l,f;

  sign=sign0;
  iq=0;
  file=read_file(name);
  point=fgets(buffer,100,file);
  l0=strtod(point,&point);
  f0=strtod(point,&point);
  point=fgets(buffer,100,file);
  alpha_l=strtod(point,&point);
  alpha_f=strtod(point,&point);
  e_final=strtod(point,&point);
  point=fgets(buffer,100,file);
  cav_len=strtod(point,&point);
  grad=strtod(point,&point);
  rf_data.cavlength=cav_len;
  rf_data.gradient=grad;

  point=fgets(buffer,100,file);
  nphase=strtol(point,&point,10);
  placet_printf(INFO,"test_read: %g %g %g %g\n",l0,f0,alpha_l,alpha_f);
  if (nphase>10){
    placet_printf(ERROR,"too many phases (%d) in input file\n",nphase);
    my_error("beamline_smooth");
  }
  for (i=0;i<nphase;i++){
    point=fgets(buffer,100,file);
    estep[i]=strtod(point,&point);
    phase[i]=strtod(point,&point);
    // beamline->phase[i]=phase[i];
  }
  close_file(file);
  field_read(field_cav,phase,scal,nphase,"beam.dat");

  e=e0;
  smooth_quad(beamline,f0*sign,0.5*quad_len,e0);
  quad_set0[iq++]=e0/(f0*quad_len)*sign;
  sign=-sign;
  l=l0;
  f=f0;
  de=scal[kscal]*cav_len*grad;
  while(e<e_final){
    check_kscal(&kscal,estep,e);
    l=l0*pow(e/e0,alpha_l);
    de=scal[kscal]*cav_len*grad;
    nc=(int)((l-quad_len-bpm_len)/cav_len);
    e+=nc*de;
    for (i=0;i<nc;i++){
      smooth_cav(beamline,cav_len,phase[kscal]);
    }
    if ((bpm_data.all==0)||((sign>0.0)&&(bpm_data.all>0))
	||((sign<0.0)&&(bpm_data.all<0))){
      smooth_drift(beamline,l-(double)nc*cav_len-quad_len-bpm_len);
      smooth_bpm(beamline,bpm_len);
    }
    else{
      smooth_drift(beamline,l-(double)nc*cav_len-quad_len);
    }
    f=f0*pow(e/e0,alpha_f);
    smooth_quad(beamline,f*sign,quad_len,e);
    quad_set0[iq]=e/(f*quad_len)*sign;
    ksum+=quad_set0[iq];
    iq++;
    sign=-sign;
  }
  beamline->set_name(name);
  beamline->set();
  beamline->set_zero();
  quadrupoles_set(beamline,quad_set0);
  //  beamline->n_phases=nphase;
  placet_printf(INFO,"kscal= %d at %g\n",kscal,e);
}

/** 
 * copies data from BEAM bunch1 into BEAM bunch2
 * no new memory allocation
 */
void beam_copy_0(BEAM *bunch1,BEAM *bunch2)
{
  int i;
  PARTICLE *p;
  int *nc;
  // if bunch2 is particle BEAM and bunch1 a sliced BEAM,
  // transform bunch2 to sliced BEAM
  if ((bunch1->particle_beam==false)&&(bunch2->particle_beam==true)) {
    i=bunch2->slices;
    bunch2->slices=bunch2->n_max;
    bunch2->n_max=i;
    p=bunch2->p_tmp;
    bunch2->p_tmp=bunch2->particle;
    bunch2->particle=p;
    nc=bunch2->particle_number_tmp;
    bunch2->particle_number_tmp=bunch2->particle_number;
    bunch2->particle_number=nc;
#ifdef HTGEN
    bunch2->psec_tmp = bunch2->particle_sec;
#endif
  }
  if (bunch1->slices!=bunch2->slices){
    my_error("beam_copy_0");
  }
  /*
    Check if the quadrupolar wakefields are used.
    If so, beam matrices will be modified, so they need to be copied.
   */
  if (quad_wake.on) {
    beam_copy_sigma(bunch1,bunch2);
  }
  memcpy(bunch2->particle,bunch1->particle,sizeof(PARTICLE)*bunch1->slices);

  /*
    If longitudinal position variation is allowed, copy the corresponding
    data. Otherwise just copy the pointer to the data.
   */
#ifdef LONGITUDINAL
  memcpy(bunch2->z_position,bunch1->z_position,sizeof(double)*bunch1->slices);
#else
  bunch2->z_position=bunch1->z_position;
#endif
  bunch2->rho_y=bunch1->rho_y;
#ifdef TWODIM
  bunch2->rho_x=bunch1->rho_x;
#endif

  bunch2->field=bunch1->field;
  bunch2->factor=bunch1->factor;
  bunch2->drive_data=bunch1->drive_data;
  bunch2->which_field=bunch1->which_field;
  bunch2->s_long=bunch1->s_long;
  bunch2->c_long=bunch1->c_long;
  bunch2->particle_number=bunch1->particle_number;
  // HTGEN
  bunch2->nhalo = bunch1->nhalo;
#ifdef HTGEN
  bunch2->particle_number_sec=bunch1->particle_number_sec; 
  memcpy(bunch2->particle_sec,bunch1->particle_sec,sizeof(PARTICLE)*bunch1->nhalo);
#endif
  bunch2->particle_beam=bunch1->particle_beam;
  bunch2->s=bunch1->s;
}

void bunch_copy_0(BEAM *beam1,BEAM *beam2)
{
  /* was beam_copy 
   check for correctness */
  beam_copy_0(beam1,beam2);
}

void beam_copy(BEAM *bunch1,BEAM *bunch2)
{
  beam_copy_0(bunch1,bunch2);
  beam_copy_sigma(bunch1,bunch2);
}

void beam_copy_sigma(BEAM *bunch1, BEAM *bunch2)
{
  memcpy(bunch2->sigma,bunch1->sigma,sizeof(R_MATRIX)*bunch1->slices);
#ifdef TWODIM
  memcpy(bunch2->sigma_xx,bunch1->sigma_xx,sizeof(R_MATRIX)*bunch1->slices);
  memcpy(bunch2->sigma_xy,bunch1->sigma_xy,sizeof(R_MATRIX)*bunch1->slices);
#endif

  // JS: I don't know why this line is not in beam_copy_0, 
  // but it was like that in the very original code
  bunch2->particle_number_tmp=bunch1->particle_number_tmp;
  
  /* scd new 27.6.97 */
  /*
  for (i=0;i<bunch1->slices_per_bunch*bunch1->bunches;i++){
    bunch2->field->de[i]=bunch1->field->de[i];
  }
  for (i=0;i<bunch1->slice_per_bunch*(bunch1->slice_per_bunch+1)/2;i++){
    bunch2->field->kick[i]=bunch1->field->kick[i];
  }
  */
  /* scd end */
}

/* find correct description of sigma matrices */

void bunch_join(BEAM *bunch0,BEAM *bunch1,double scal,BEAM *bunch2)
{
  int i;
  if (bunch1->slices!=bunch2->slices){
    my_error("bunch_join");
  }
  for (i=0;i<bunch1->slices;i++){
    bunch2->particle[i]=bunch0->particle[i];
    bunch2->particle[i].y=bunch0->particle[i].y
      +scal*(bunch1->particle[i].y-bunch0->particle[i].y);
    bunch2->particle[i].yp=bunch0->particle[i].yp
      +scal*(bunch1->particle[i].yp-bunch0->particle[i].yp);
    bunch2->sigma[i]=bunch0->sigma[i];
#ifdef TWODIM
    bunch2->particle[i].x=bunch0->particle[i].x
      +scal*(bunch1->particle[i].x-bunch0->particle[i].x);
    bunch2->particle[i].xp=bunch0->particle[i].xp
      +scal*(bunch1->particle[i].xp-bunch0->particle[i].xp);
    bunch2->sigma_xx[i]=bunch0->sigma_xx[i];
    bunch2->sigma_xy[i]=bunch0->sigma_xy[i];
#endif
  }
#ifdef LONGITUDINAL
  memcpy(bunch2->z_position,bunch0->z_position,sizeof(double)*bunch1->slices);
#else
  bunch2->z_position=bunch0->z_position;
#endif
  bunch2->field=bunch0->field;
  bunch2->factor=bunch0->factor;
  bunch2->drive_data=bunch0->drive_data;
  bunch2->which_field=bunch0->which_field;
  bunch2->s_long=bunch0->s_long;
  bunch2->c_long=bunch0->c_long;
  bunch2->last_wgt=bunch0->last_wgt;
}

void bunch_join_0(BEAM *bunch0,BEAM *bunch1,double scal,
	     BEAM *bunch2)
{
  int i;
  if (bunch1->slices!=bunch2->slices){
    my_error("bunch_join");
  }
  for (i=0;i<bunch1->slices;i++){
    bunch2->particle[i]=bunch0->particle[i];
    bunch2->particle[i].y=bunch0->particle[i].y
      +scal*(bunch1->particle[i].y-bunch0->particle[i].y);
    bunch2->particle[i].yp=bunch0->particle[i].yp
      +scal*(bunch1->particle[i].yp-bunch0->particle[i].yp);
#ifdef TWODIM
    bunch2->particle[i].x=bunch0->particle[i].x
      +scal*(bunch1->particle[i].x-bunch0->particle[i].x);
    bunch2->particle[i].xp=bunch0->particle[i].xp
      +scal*(bunch1->particle[i].xp-bunch0->particle[i].xp);
#endif
  }
#ifdef LONGITUDINAL
  memcpy(bunch2->z_position,bunch0->z_position,sizeof(double)*bunch1->slices);
#else
  bunch2->z_position=bunch0->z_position;
#endif
  bunch2->field=bunch0->field;
  bunch2->factor=bunch0->factor;
  bunch2->drive_data=bunch0->drive_data;
  bunch2->which_field=bunch0->which_field;
  bunch2->s_long=bunch0->s_long;
  bunch2->c_long=bunch0->c_long;
  bunch2->last_wgt=bunch0->last_wgt;
}

void bunch_join_x(BEAM *bunch0,BEAM *bunchx,BEAM *bunchy,double scal_x,
	     double scal_y,BEAM *bunch2)
{
  int i;
  if (bunch0->slices!=bunch2->slices){
    my_error("bunch_join");
  }
  for (i=0;i<bunch0->slices;i++){
    bunch2->particle[i]=bunch0->particle[i];
    bunch2->particle[i].y=bunch0->particle[i].y
      +scal_x*(bunchx->particle[i].y-bunch0->particle[i].y)
      +scal_y*(bunchy->particle[i].y-bunch0->particle[i].y);
    bunch2->particle[i].yp=bunch0->particle[i].yp
      +scal_x*(bunchx->particle[i].yp-bunch0->particle[i].yp)
      +scal_y*(bunchy->particle[i].yp-bunch0->particle[i].yp);
    bunch2->sigma[i]=bunch0->sigma[i];
#ifdef TWODIM
    bunch2->particle[i].x=bunch0->particle[i].x
      +scal_x*(bunchx->particle[i].x-bunch0->particle[i].x)
      +scal_y*(bunchy->particle[i].x-bunch0->particle[i].x);
    bunch2->particle[i].xp=bunch0->particle[i].xp
      +scal_x*(bunchx->particle[i].xp-bunch0->particle[i].xp)
      +scal_y*(bunchy->particle[i].xp-bunch0->particle[i].xp);
    bunch2->sigma_xx[i]=bunch0->sigma_xx[i];
    bunch2->sigma_xy[i]=bunch0->sigma_xy[i];
#endif
  }
#ifdef LONGITUDINAL
  memcpy(bunch2->z_position,bunch0->z_position,sizeof(double)*bunch0->slices);
#else
  bunch2->z_position=bunch0->z_position;
#endif
  bunch2->field=bunch0->field;
  bunch2->factor=bunch0->factor;
  bunch2->drive_data=bunch0->drive_data;
  bunch2->which_field=bunch0->which_field;
  bunch2->s_long=bunch0->s_long;
  bunch2->c_long=bunch0->c_long;
  bunch2->last_wgt=bunch0->last_wgt;
}


void bunch_join_0_x(BEAM *bunch0,BEAM *bunchx,BEAM *bunchy,double scal_x,
	       double scal_y,BEAM *bunch2)
{
  int i;
  if (bunch0->slices!=bunch2->slices){
    my_error("bunch_join");
  }
  for (i=0;i<bunch0->slices;i++){
    bunch2->particle[i]=bunch0->particle[i];
    bunch2->particle[i].y=bunch0->particle[i].y
      +scal_x*(bunchx->particle[i].y-bunch0->particle[i].y)
      +scal_y*(bunchy->particle[i].y-bunch0->particle[i].y);
    bunch2->particle[i].yp=bunch0->particle[i].yp
      +scal_x*(bunchx->particle[i].yp-bunch0->particle[i].yp)
      +scal_y*(bunchy->particle[i].yp-bunch0->particle[i].yp);
#ifdef TWODIM
    bunch2->particle[i].x=bunch0->particle[i].x
      +scal_x*(bunchx->particle[i].x-bunch0->particle[i].x)
      +scal_y*(bunchy->particle[i].x-bunch0->particle[i].x);
    bunch2->particle[i].xp=bunch0->particle[i].xp
      +scal_y*(bunchx->particle[i].xp-bunch0->particle[i].xp)
      +scal_x*(bunchy->particle[i].xp-bunch0->particle[i].xp);
#endif
  }
#ifdef LONGITUDINAL
  memcpy(bunch2->z_position,bunch0->z_position,sizeof(double)*bunch0->slices);
#else
  bunch2->z_position=bunch0->z_position;
#endif
  bunch2->field=bunch0->field;
  bunch2->factor=bunch0->factor;
  bunch2->drive_data=bunch0->drive_data;
  bunch2->which_field=bunch0->which_field;
  bunch2->s_long=bunch0->s_long;
  bunch2->c_long=bunch0->c_long;
  bunch2->last_wgt=bunch0->last_wgt;
}

void bns_plot(BEAMLINE *beamline,BEAM *beam,char *name,double charge,double kick)
{
  FILE *file;
  double l,k1,k2,l1,l2,alpha_x,alpha_y,beta_x,beta_y,mu_x,mu_y;
  int i=0,n,j=0;
  ELEMENT **element;
  BEAM *tb;

  tb=bunch_remake(beam);
  beam_copy(beam,tb);
  n=tb->slices;
  element=beamline->element;
  QUADRUPOLE *quad;
  while (!(element[i]->quad_ptr())) {
    element[i]->track_0(tb);
    i++;
  }
  i++;
  while (!(quad=element[i]->quad_ptr())) {
    element[i]->track_0(tb);
    i++;
  }
  k1=quad->get_strength()/(quad->get_length()*tb->particle[n/2].energy);
  l1=quad->get_length();
  file=open_file(name);
  for (;;){
    l=0.5*element[i]->get_length();
    i++;
    while ((element[i]!=NULL)&&(!(quad=element[i]->quad_ptr()))) {
      element[i]->track_0(tb);
      l+=element[i]->get_length();
      i++;
    }
    if (element[i]==NULL){
      break;
    }
    quad->track_0(tb);
    l2=quad->get_length();
    l+=0.5*quad->get_length();
    k2=quad->get_strength()/(quad->get_length()*tb->particle[n/2].energy);
    infodo(k1,l1,k2,l2,l,&alpha_x,&alpha_y,&beta_x,&beta_y,&mu_x,&mu_y);
    mu_x*=180.0/PI;
    mu_y*=180.0/PI;
    fprintf(file,"%d %g %g %g\n",j++,tb->particle[n/2].energy,mu_y,
	    bns(charge,kick,2.0*l,mu_y,tb->particle[n/2].energy));
    l1=l2;
    k1=k2;
  }
  close_file(file);
  beam_delete(tb);
}

/* bunch0 contains the beam at the entrance of the first quadrupole */

void bin_response_target(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
			 BIN *bin)
{
  const double dy=1.0;
  double *a,dy_inv,*t;
#ifdef TWODIM
  double *a_x,*t_x;
#endif
  int *quad,*bpm,nq,nb,*quad_x,*bpm_x,nq_x,nb_x;
  int iq,ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    a=bin->a0;
    t=bin->t0;
#ifdef TWODIM
    a_x=bin->a0_x;
    t_x=bin->t0_x;
#endif
    break;
  case 1:
    a=bin->a1;
    t=bin->t1;
#ifdef TWODIM
    a_x=bin->a1_x;
    t_x=bin->t1_x;
#endif
    break;
  case 2:
    a=bin->a2;
    t=bin->t2;
#ifdef TWODIM
    a_x=bin->a2_x;
    t_x=bin->t2_x;
#endif
    break;
  default:
    placet_cout << ERROR << "bin_response_target: flag not existing: " << flag << endmsg;
    exit(1);
    break;
  }
  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
  quad=bin->quad;
#ifdef TWODIM
  nb_x=bin->nbpm_x;
  nq_x=bin->nq_x;
  bpm_x=bin->bpm_x;
  quad_x=bin->quad_x;
#endif
  element=beamline->element;

  bunch_copy_0(bunch0,bunch);
  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  if(bin->a){
    for (ib=0;ib<nb;ib++){
      t[ib]=element[bpm[ib]]->get_bpm_y_reading_exact();
    }
  }
#ifdef TWODIM
  if (bin->a_x){
    for (ib=0;ib<nb_x;ib++){
      t_x[ib]=element[bpm[ib]]->get_bpm_x_reading_exact();
    }
  }
#endif

  dy_inv=1.0/dy;
  if(bin->a){
    for (iq=0;iq<nq;iq++){
      //element_add_offset(element[quad[iq]],dy,0.0);
      element[quad[iq]]->vary_vcorr(dy);
      bunch_copy_0(bunch0,bunch);
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
      for (ib=0;ib<nb;ib++){
	*a++=(element[bpm[ib]]->get_bpm_y_reading_exact()-t[ib])*dy_inv;
      }
      //element_add_offset(element[quad[iq]],-dy,0.0);
      element[quad[iq]]->vary_vcorr(-dy);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(iq=0;iq<nq_x;iq++){
      //element_add_offset_x(element[quad_x[iq]],dy,0.0);
      element[quad_x[iq]]->vary_hcorr(dy);
      bunch_copy_0(bunch0,bunch);
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
      for (ib=0;ib<nb_x;ib++){
	*a_x++=(element[bpm_x[ib]]->get_bpm_x_reading_exact()-t_x[ib])*dy_inv;
      }
      //element_add_offset_x(element[quad_x[iq]],-dy,0.0);
      element[quad_x[iq]]->vary_hcorr(-dy);
    }
  }
#endif
}

/*
  This is the original response calculation function
*/

void
bin_response(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
	     BIN *bin)
{
  double *a,bpm0[BIN_MAX_BPM];
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif
  int *quad,*bpm,nq,nb,*quad_x,*bpm_x,nq_x,nb_x;
  int iq,ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    a=bin->a0;
#ifdef TWODIM
    a_x=bin->a0_x;
#endif
    break;
  case 1:
    a=bin->a1;
#ifdef TWODIM
    a_x=bin->a1_x;
#endif
    break;
  case 2:
    a=bin->a2;
#ifdef TWODIM
    a_x=bin->a2_x;
#endif
    break;
  }
  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
  quad=bin->quad;
#ifdef TWODIM
  nb_x=bin->nbpm_x;
  nq_x=bin->nq_x;
  bpm_x=bin->bpm_x;
  quad_x=bin->quad_x;
#endif
  element=beamline->element;

  bunch_copy_0(bunch0,bunch);
  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  if(bin->a){
    for (ib=0;ib<nb;ib++){
      bpm0[ib]=element[bpm[ib]]->get_bpm_y_reading_exact();
    }
  }
#ifdef TWODIM
  if (bin->a_x){
    for (ib=0;ib<nb_x;ib++){
      bpm0_x[ib]=element[bpm_x[ib]]->get_bpm_x_reading_exact();
    }
  }
#endif

  if(bin->a){
    for (iq=0;iq<nq;iq++){
      double dy;
      if (element[quad[iq]]->get_vcorr_step_size()!=0)
        dy=5.0*element[quad[iq]]->get_vcorr_step_size();
      else
        dy=5.0;
      element[quad[iq]]->correct_y(dy);
      bunch_copy_0(bunch0,bunch);
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
      for (ib=0;ib<nb;ib++){
	*a++=(element[bpm[ib]]->get_bpm_y_reading_exact()-bpm0[ib])/dy;
      }
      element[quad[iq]]->correct_y(-dy);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(iq=0;iq<nq_x;iq++){
      double dy;
      if (element[quad[iq]]->get_hcorr_step_size()!=0)
        dy=5.0*element[quad[iq]]->get_hcorr_step_size();
      else
        dy=5.0;
      element[quad_x[iq]]->correct_x(dy);
      bunch_copy_0(bunch0,bunch);
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
      for (ib=0;ib<nb_x;ib++){
	*a_x++=(element[bpm_x[ib]]->get_bpm_x_reading_exact()-bpm0_x[ib])/dy;
      }
      element[quad_x[iq]]->correct_x(-dy);
    }
  }
#endif
}

void
bin_measure_one_beam(BEAMLINE *beamline,BEAM *beam,BIN *bin)
{
  int *bpm,nb,ibunch,nbunch;
  int ib;
  ELEMENT **element;

  nb=bin->nbpm;
  bpm=bin->bpm;
  element=beamline->element;
  nbunch=beam->bunches;

  bunch_track_0(beamline,beam,bin->start,bin->stop);

  for (ib=0;ib<nb;ib++){
    bin->b0[ib]=0.0;
    bin->b1[ib]=0.0;
    bin->b2[ib]=0.0;
    for (ibunch=0;ibunch<nbunch;++ibunch) {
      if (ibunch%2==1) {
	bin->b1[ib]+=element[bpm[ib]]->bpm_ptr()->bunch_signal(ibunch)
	+errors.bpm_resolution*Instrumentation.Gauss();
	bin->b2[ib]=bin->b1[ib];
      }
      else {
	bin->b0[ib]+=element[bpm[ib]]->bpm_ptr()->bunch_signal(ibunch)
	+errors.bpm_resolution*Instrumentation.Gauss();
      }
    }
    if (nbunch%2!=0) {
      bin->b0[ib]/=0.5*(nbunch+1);
      bin->b1[ib]/=0.5*(nbunch-1);
      bin->b2[ib]/=0.5*(nbunch-1);
    }
    else {
      bin->b0[ib]/=0.5*nbunch;
      bin->b1[ib]/=0.5*nbunch;
      bin->b2[ib]/=0.5*nbunch;
    }
  }
#ifdef TWODIM
  nb=bin->nbpm_x;
  bpm=bin->bpm_x;
  for (ib=0;ib<nb;ib++){
    bin->b0_x[ib]=0.0;
    bin->b1_x[ib]=0.0;
    bin->b2_x[ib]=0.0;
    for (ibunch=0;ibunch<nbunch;++ibunch) {
      if (ibunch%2==1) {
	bin->b1_x[ib]+=element[bpm[ib]]->bpm_ptr()->bunch_signal_x(ibunch)
	+errors.bpm_resolution*Instrumentation.Gauss();
	bin->b2_x[ib]=bin->b1_x[ib];
      }
      else {
	bin->b0_x[ib]+=element[bpm[ib]]->bpm_ptr()->bunch_signal_x(ibunch)
	+errors.bpm_resolution*Instrumentation.Gauss();
      }
    }
    if (nbunch%2!=0) {
      bin->b0_x[ib]/=0.5*(nbunch+1);
      bin->b1_x[ib]/=0.5*(nbunch-1);
      bin->b2_x[ib]/=0.5*(nbunch-1);
    }
    else {
      bin->b0_x[ib]/=0.5*nbunch;
      bin->b1_x[ib]/=0.5*nbunch;
      bin->b2_x[ib]/=0.5*nbunch;
    }
  }
#endif
}

void bin_response_train(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int /*flag*/,
			BIN *bin)
{
  const double dy=5.0;
  double *a0,*a1,*a2,bpm0[3][BIN_MAX_BPM],dy_inv,tmp;
#ifdef TWODIM
  double *a0_x,*a1_x,*a2_x,bpm0_x[3][BIN_MAX_BPM];
#endif
  int *quad,nq,nb,nb_x;
  int iq,ib;
  ELEMENT **element;

  nb=bin->nbpm;
  nq=bin->nq;
  quad=bin->quad;
  a0=bin->a0;
  a1=bin->a1;
  a2=bin->a2;
#ifdef TWODIM
  nb_x=bin->nbpm_x;
  a0_x=bin->a0_x;
  a1_x=bin->a1_x;
  a2_x=bin->a2_x;
#endif
  element=beamline->element;

  bunch_copy_0(bunch0,bunch);
  tmp=errors.bpm_resolution;
  errors.bpm_resolution=0.0;
  bin_measure_one_beam(beamline,bunch,bin);
  if(bin->a){
    for (ib=0;ib<nb;ib++){
	bpm0[0][ib]=bin->b0[ib];
	bpm0[1][ib]=bin->b1[ib];
	bpm0[2][ib]=bin->b2[ib];
    }
  }
#ifdef TWODIM
  if (bin->a_x){
    for (ib=0;ib<nb_x;ib++){
	bpm0_x[0][ib]=bin->b0_x[ib];
	bpm0_x[1][ib]=bin->b1_x[ib];
	bpm0_x[2][ib]=bin->b2_x[ib];
    }
  }
#endif

  dy_inv=1.0/dy;
  if(bin->a){
    for (iq=0;iq<nq;iq++){
      //element_add_offset(element[quad[iq]],dy,0.0);
      element[quad[iq]]->vary_vcorr(dy);
      bunch_copy_0(bunch0,bunch);
      bin_measure_one_beam(beamline,bunch,bin);
      for (ib=0;ib<nb;ib++){
	*a0++=(bin->b0[ib]-bpm0[0][ib])*dy_inv;
	*a1++=(bin->b1[ib]-bpm0[1][ib])*dy_inv;
	*a2++=(bin->b2[ib]-bpm0[2][ib])*dy_inv;
      }
      //element_add_offset(element[quad[iq]],-dy,0.0);
      element[quad[iq]]->vary_vcorr(-dy);
    }
  }
#ifdef TWODIM
  if(bin->a){
    for (iq=0;iq<nq;iq++){
      //element_add_offset_x(element[quad[iq]],dy,0.0);
      element[quad[iq]]->vary_hcorr(dy);
      bunch_copy_0(bunch0,bunch);
      bin_measure_one_beam(beamline,bunch,bin);
      for (ib=0;ib<nb;ib++){
	*a0_x++=(bin->b0_x[ib]-bpm0_x[0][ib])*dy_inv;
	*a1_x++=(bin->b1_x[ib]-bpm0_x[1][ib])*dy_inv;
	*a2_x++=(bin->b2_x[ib]-bpm0_x[2][ib])*dy_inv;
      }
      //element_add_offset_x(element[quad[iq]],-dy,0.0);
      element[quad[iq]]->vary_hcorr(-dy);
    }
  }
#endif
  errors.bpm_resolution=tmp;
}

void bin_response_dipole(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
		    BIN *bin)
{
  const double dy=5.0;
  double *a,bpm0[BIN_MAX_BPM],dy_inv;
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif
  int *quad,*bpm,nq,nb,*bpm_x,nq_x,nb_x;
  int iq,ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    a=bin->a0;
#ifdef TWODIM
    a_x=bin->a0_x;
#endif
    break;
  case 1:
    a=bin->a1;
#ifdef TWODIM
    a_x=bin->a1_x;
#endif
    break;
  case 2:
    a=bin->a2;
#ifdef TWODIM
    a_x=bin->a2_x;
#endif
    break;
  }
  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
  quad=bin->quad;
#ifdef TWODIM
  nb_x=bin->nbpm_x;
  nq_x=bin->nq_x;
  bpm_x=bin->bpm_x;
#endif
  element=beamline->element;

  bunch_copy_0(bunch0,bunch);
  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  if(bin->a){
    for (ib=0;ib<nb;ib++){
      bpm0[ib]=element[bpm[ib]]->get_bpm_y_reading_exact();
    }
  }
#ifdef TWODIM
  if (bin->a_x){
    for (ib=0;ib<nb_x;ib++){
      bpm0_x[ib]=element[bpm_x[ib]]->get_bpm_x_reading_exact();
    }
  }
#endif

  dy_inv=1.0/dy;
  if(bin->a){
    for (iq=0;iq<nq;iq++){
      element[quad[iq]]->dipole_ptr()->set_strength_y(dy);
      bunch_copy_0(bunch0,bunch);
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
      for (ib=0;ib<nb;ib++){
	*a++=(element[bpm[ib]]->get_bpm_y_reading_exact()-bpm0[ib])*dy_inv;
      }
      element[quad[iq]]->dipole_ptr()->add_strength_y(-dy);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(iq=0;iq<nq_x;iq++){
      element[quad[iq]]->dipole_ptr()->set_strength_y(dy);
      bunch_copy_0(bunch0,bunch);
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
      for (ib=0;ib<nb_x;ib++){
	*a_x++=(element[bpm_x[ib]]->get_bpm_x_reading_exact()-bpm0_x[ib])*dy_inv;
      }
      element[quad[iq]]->dipole_ptr()->add_strength_y(-dy);
    }
  }
#endif
}

/* bin response in the ballistic case */

void bin_response_b(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch1,BEAM *bunch,
	       int flag,BIN *bin)
{
  const double dy0=5.0;
  double *a,bpm0[BIN_MAX_BPM],dy_inv,dy,tmp;
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif
  int *quad,*bpm,nq,nb;
  int iq,ib;
  ELEMENT **element;

  dy=dy0*(1.0+0.01*Misalignments.Gauss());
  switch (flag){
  case 0:
    a=bin->a0;
#ifdef TWODIM
    a_x=bin->a0_x;
#endif
    break;
  case 1:
    a=bin->a1;
#ifdef TWODIM
    a_x=bin->a1_x;
#endif
    break;
  case 2:
    a=bin->a2;
#ifdef TWODIM
    a_x=bin->a2_x;
#endif
    break;
  }
  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
  quad=bin->quad;
  element=beamline->element;

  if (bunch1==NULL){
    bunch_copy_0(bunch0,bunch);
  }
  else{
    tmp=Misalignments.Gauss();
    //    placet_printf(INFO,"Misalignments.Gauss: %g\n",tmp);
    bunch_join_0(bunch0,bunch1,tmp,bunch);
  }
  bunch_track_0(beamline,bunch,bin->start,bin->stop);
  for (ib=0;ib<nb;ib++){
    bpm0[ib]=element[bpm[ib]]->get_bpm_y_reading_exact();
#ifdef TWODIM
    bpm0_x[ib]=element[bpm[ib]]->get_bpm_x_reading_exact();
#endif
  }

  dy_inv=1.0/dy0;
  //element_add_offset(element[quad[0]],dy,0.0);
  element[quad[0]]->vary_vcorr(dy);
  if (bunch1==NULL){
    bunch_copy_0(bunch0,bunch);
  }
  else{
    bunch_join_0(bunch0,bunch1,Misalignments.Gauss(),bunch);
  }
  bunch_track_0(beamline,bunch,bin->start,bin->stop);
  for (ib=0;ib<nb;ib++){
    //    *a++=(element[bpm[ib]]->info1-bpm0[ib]+0.1*Misalignments.Gauss())*dy_inv;
    *a++=(element[bpm[ib]]->get_bpm_y_reading_exact()-bpm0[ib])*dy_inv;
    //    *a++=(element[bpm[ib]]->info1-bpm0[ib])*(1.0+0.02*Misalignments.Gauss())*dy_inv;
  }
  //element_add_offset(element[quad[0]],-dy,0.0);
  element[quad[0]]->vary_vcorr(-dy);
  for (iq=1;iq<nq;iq++){
    for (ib=0;ib<nb;ib++){
      if (ib==iq-1) {
	// scd test
	*a++=-1.0;
      }
      else{
	*a++=0.0;
      }
    }
  }
#ifdef TWODIM
  //element_add_offset_x(element[quad[0]],dy,0.0);
  element[quad[0]]->vary_hcorr(dy);
  bunch_copy_0(bunch0,bunch);
  bunch_track_0(beamline,bunch,quad[0],bpm[nb-1]+1);
  for (ib=0;ib<nb;ib++){
    *a_x++=(element[bpm[ib]]->get_bpm_x_reading_exact()-bpm0_x[ib])*dy_inv;
  }
  //element_add_offset_x(element[quad[0]],-dy,0.0);
  element[quad[0]]->vary_hcorr(-dy);
  for (iq=1;iq<nq;iq++){
    for (ib=0;ib<nb;ib++){
      if (ib==iq-1) {
	*a_x++=-1.0;
      }
      else{
	*a_x++=0.0;
      }
    }
  }
#endif
}

/* bin response for the ballistic case calculated */

void bin_response_b_calc(BEAMLINE *beamline,BEAM *beam0,BEAM * /*beam1*/,BEAM *beam,
		    int flag,BIN *bin)
{
  //  const double dy0=5.0;
  double *a,bpm0[BIN_MAX_BPM],l=0.0,energy,wgt;
  double kick;
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif
  int nq,nb;
  int i,iq,ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    a=bin->a0;
#ifdef TWODIM
    a_x=bin->a0_x;
#endif
    break;
  case 1:
    a=bin->a1;
#ifdef TWODIM
    a_x=bin->a1_x;
#endif
    break;
  case 2:
    a=bin->a2;
#ifdef TWODIM
    a_x=bin->a2_x;
#endif
    break;
  }
  nb=bin->nbpm;
  nq=bin->nq;
  element=beamline->element;

  energy=0.0;
  wgt=0.0;
  for(i=0;i<beam->slices;i++){
    energy+=beam0->particle[i].energy*beam0->particle[i].wgt;
    wgt+=beam0->particle[i].wgt;
  }
  energy/=wgt;
  QUADRUPOLE *point=element[bin->quad[0]]->quad_ptr();
  kick=point->get_strength()/energy;
  l=0.5*point->get_length();
  i=bin->quad[0]+1;
  for (ib=0;ib<nb;ib++){
    while(i<bin->bpm[ib])
      {
	l+=element[i]->get_length();
	i++;
      }
    bpm0[ib]=kick*(l+0.5*element[i]->get_length());
#ifdef TWODIM
    bpm0_x[ib]=-bpm0[ib];
#endif
    l+=element[i]->get_length();
  }

  for (ib=0;ib<nb;ib++){
    *a++=bpm0[ib];
  }
  for (iq=1;iq<nq;iq++){
    for (ib=0;ib<nb;ib++){
      if (ib==iq-1) {
	*a++=-1.0;
      }
      else{
	*a++=0.0;
      }
    }
  }
#ifdef TWODIM
  for (ib=0;ib<nb;ib++){
    *a_x++=bpm0_x[ib];
  }
  for (iq=1;iq<nq;iq++){
    for (ib=0;ib<nb;ib++){
      if (ib==iq-1) {
	*a_x++=-1.0;
      }
      else{
	*a_x++=0.0;
      }
    }
  }
#endif
}

void bin_response_b_calc_x(BEAMLINE *beamline,BEAM *beam0,BEAM * /*beamx*/,BEAM * /*beamy*/,
		      BEAM *beam,int flag,BIN *bin)
{
  bin_response_b_calc(beamline,beam0,NULL,beam,flag,bin);
}

void bin_response_zero(BEAMLINE * /*beamline*/,BEAM * /*bunch0*/,BEAM * /*bunch*/,int flag,
		  BIN *bin)
{
  double *a;
#ifdef TWODIM
  double *a_x;
#endif
  int nq,nb,iq,ib;

  switch (flag){
  case 0:
    a=bin->a0;
#ifdef TWODIM
    a_x=bin->a0_x;
#endif
    break;
  case 1:
    a=bin->a1;
#ifdef TWODIM
    a_x=bin->a1_x;
#endif
    break;
  case 2:
    a=bin->a2;
#ifdef TWODIM
    a_x=bin->a2_x;
#endif
    break;
  }
  nb=bin->nbpm;
  nq=bin->nq;

  for (iq=0;iq<nq;iq++){
    for (ib=0;ib<nb;ib++){
      *a++=0.0;
    }
  }
#ifdef TWODIM
  nb=bin->nbpm_x;
  nq=bin->nq_x;
  for (iq=0;iq<nq;iq++){
    for (ib=0;ib<nb;ib++){
      *a_x++=0.0;
    }
  }
#endif
}

void
bin_measure(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin,bool track, bool noacc)
{
  //  const double dy=1.0;
  double *b;
#ifdef TWODIM
  double *b_x;
#endif
  int *bpm,nb;
  int ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    b=bin->b0;
#ifdef TWODIM
    b_x=bin->b0_x;
#endif
    break;
  case 1:
    b=bin->b1;
#ifdef TWODIM
    b_x=bin->b1_x;
#endif
    break;
  case 2:
    b=bin->b2;
#ifdef TWODIM
    b_x=bin->b2_x;
#endif
    break;
  default:
    placet_cout << ERROR << "bin_measure: flag not existing: " << flag << endmsg;
    exit(1);
    break;
  }
  nb=bin->nbpm;
  bpm=bin->bpm;
  element=beamline->element;

  if(track) {
    if (noacc) 
      bunch_track_0_noacc(beamline,bunch,bin->start,bin->stop);
    else
      bunch_track_0(beamline,bunch,bin->start,bin->stop);
  }

  for (ib=0;ib<nb;ib++){
    // better to use resolution of BPM ELEMENT?
    b[ib]=element[bpm[ib]]->get_bpm_y_reading_exact()+errors.bpm_resolution*Instrumentation.Gauss();
  }
#ifdef TWODIM
  nb=bin->nbpm_x;
  bpm=bin->bpm_x;
  for (ib=0;ib<nb;ib++){
    b_x[ib]=element[bpm[ib]]->get_bpm_x_reading_exact()+errors.bpm_resolution*Instrumentation.Gauss();
  }
#endif
}

void bin_read_measure(BEAMLINE *beamline,BEAM * bunch,int flag,BIN *bin)
{
  bin_measure(beamline,bunch,flag,bin,false);
}

/****************************************************
* function: twiss_write_header			    *
* Juergen Pfingstner, 17. Dec. 2008		    *
*						    *
* Content: Write a header in the twiss plot output  *
*          file to make it more readable.	    *
****************************************************/

void twiss_write_header(FILE *file)
{
  fprintf(file,"# Collects the twiss parameter of the beam line in the following columns \n# \n");
  fprintf(file,"# 1.) Element number \n");
  fprintf(file,"# 2.) s: [m] Distance in the beam line: s \n");
  fprintf(file,"# 3.) E(s): energy [GeV] ( central slice or average energy for particle beam) \n");
  fprintf(file,"# 4.) beta_x_m(s) [m] (of central slice ) \n");
  fprintf(file,"# 5.) alpha_x_m(s) [m] (of central slice ) \n");
  fprintf(file,"# 6.) beta_x_i(s) [m] (of average over slices) \n");
  fprintf(file,"# 7.) alpha_x_i(s) [m] (of average over slices) \n");
  fprintf(file,"# 8.) beta_y_m(s) [m] (of central slice ) \n");
  fprintf(file,"# 9.) alpha_y_m(s) [m] (of central slice ) \n");
  fprintf(file,"# 10.) beta_y_i(s) [m] (of average over slices) \n");
  fprintf(file,"# 11.) alpha_y_i(s) [m] (of average over slices) \n");
  fprintf(file,"# 12.) disp_x(s) [m/GeV] (of average over slices) \n");
  fprintf(file,"# 13.) disp_xp(s) [rad/GeV] (of average over slices) \n");
  fprintf(file,"# 14.) disp_y(s) [m/GeV] (of average over slices) \n");
  fprintf(file,"# 15.) disp_yp(s) [rad/GeV] (of average over slices) \n# \n");
}

/**********************************************
* 12. Dec. 2008: Change by Juergen Pfingstner *
* Added code for documentation in the output  *
* file	                                      *
**********************************************/

void twiss_plot(BEAMLINE *beamline,BEAM *bunch0,char *name,int n1,int n2,void (* callback)(FILE*,BEAM*,int,double,int,int), int *list, int nlist )
{
  ELEMENT **element;
  int j=0;
  FILE *file=NULL;
  double s=0.0;
  BEAM *bunch;
  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
//   int n1 = bunch->slices/2;
//   int n2 = n1;
  file=open_file(name);
  
  element=beamline->element;
  twiss_write_header(file);  
  //twiss_emitt(file,0,0.0,bunch,0.0);
  while(element[j]!=NULL){
    s+=0.5*element[j]->get_length();
    bool skip=false;
    if (list) {
      skip=(std::find(list,list+nlist,j)==(list+nlist));
    }
    QUADRUPOLE *quad;
    if ((quad=element[j]->quad_ptr())&&(!skip)){
      quad->step_half(bunch);
      //twiss_emitt(file,j,s,bunch,quad->get_strength()/quad->get_length());
      //fprint_twiss(file,bunch,j,s,nc);
      callback(file,bunch,j,s,n1,n2);
      quad->step_half(bunch);
    }
    else{
      element[j]->track(bunch);
    }
    s+=0.5*element[j]->get_length();
    j++;
  }
  //twiss_emitt(file,j-1,s,bunch,0.0);
  close_file(file);
  beam_delete(bunch);
}

void dispersion_plot(BEAMLINE *beamline,BEAM *bunch,char *name)
{
  ELEMENT **element;
  int i,j=0;
  FILE *file;
  double s=0.0,offsetx,offsety;

  file=open_file(name);
  element=beamline->element;
  while(element[j]!=NULL){
    s+=0.5*element[j]->get_length();
    if (!element[j]->is_drift()){
      offsety=0.5*element[j]->get_length()*element[j]->offset.yp-element[j]->offset.y;
#ifdef TWODIM
      offsetx=0.5*element[j]->get_length()*element[j]->offset.xp-element[j]->offset.x;
#endif
      for (i=0;i<bunch->slices;i++){
	bunch->particle[i].y+=offsety;
	bunch->particle[i].yp-=element[j]->offset.yp;
#ifdef TWODIM
	bunch->particle[i].x+=offsetx;
	bunch->particle[i].xp-=element[j]->offset.xp;
#endif
      }
    }
    if (QUADRUPOLE *quad=element[j]->quad_ptr()){
      quad->step_half(bunch);
      fprintf(file,"%g",s);
      for (i=1;i<bunch->slices;i++){
       	fprintf(file," %g",(bunch->particle[i].y-bunch->particle[0].y));
	//		*bunch->particle[i].energy);
      }
      fprintf(file,"\n");
      quad->step_half(bunch);
    }
    else{
      element[j]->track(bunch);
    }
    if (!element[j]->is_drift()){
      offsety=0.5*element[j]->get_length()*element[j]->offset.yp+element[j]->offset.y;
#ifdef TWODIM
      offsetx=0.5*element[j]->get_length()*element[j]->offset.xp+element[j]->offset.x;
#endif
      for (i=0;i<bunch->slices;i++){
	bunch->particle[i].y+=offsety;
	bunch->particle[i].yp+=element[j]->offset.yp;
#ifdef TWODIM
	bunch->particle[i].x+=offsetx;
	bunch->particle[i].xp+=element[j]->offset.xp;
#endif
      }
    }
    s+=0.5*element[j]->get_length();
    j++;
  }
  close_file(file);
}

/* calculates the relative energy spread of the bunch */

double energy_spread(BEAM *bunch)
{
  int i,n,n_centre;
  double min,max;
  n=bunch->slices;
  n_centre=n/2;
  min=1e300;
  max=-1e300;
  for (i=0;i<n_centre;i++){
    if (bunch->particle[i].energy>max) max=bunch->particle[i].energy;
  }
  for (i=n_centre;i<n;i++){
    if (bunch->particle[i].energy<min) min=bunch->particle[i].energy;
  }
  return (max-min)/bunch->particle[n_centre].energy;
}

double bunch_energy(BEAM *bunch)
{
  int n,n_centre;
  n=bunch->slices;
  n_centre=n/2;
  return bunch->particle[n_centre].energy;
}

double energy_spread_rms(BEAM *bunch)
{
  int i,n,n_centre;
  double s=0.0,s2=0.0,wgtsum=0.0;
  n=bunch->slices;
  n_centre=n/2;
  for (i=0;i<n;i++){
    wgtsum+=bunch->particle[i].wgt;
    s+=bunch->particle[i].energy*bunch->particle[i].wgt;
    s2+=bunch->particle[i].energy*bunch->particle[i].energy*
      bunch->particle[i].wgt;
  }
  s/=wgtsum;
  s2/=wgtsum;
  return sqrt(std::max(0.0,s2-s*s))/bunch->particle[n_centre].energy;
}

void energy_spread_plot_header(FILE *file)
{
  fprintf(file,"# EnergySpreadPlot output file \n\n");
  fprintf(file,"# 1.) Element number \n");
  fprintf(file,"# 2.) s [m] Distance in the beam line \n");
  fprintf(file,"# 3.) E(s) energy [GeV] ( central slice or average energy for particle beam) \n");
  fprintf(file,"# 4.) Relative energy spread \n");
  fprintf(file,"# 5.) rms energy spread \n \n");
}

void energy_spread_plot(BEAMLINE *beamline,BEAM *beam,char *name,int head)
{
  FILE *file;
  ELEMENT **element;
  BEAM *tb;
  int i,j;
  element=beamline->element;
  j=0;
  double s=0.0;
  tb=bunch_remake(beam);
  bunch_copy_0(beam,tb);
  file=open_file(name);
  if (head==1) energy_spread_plot_header(file);
  for(i=0;i<beamline->n_elements;i++){
    element[i]->track_0(tb);
    s+=element[i]->get_length();
    if ((element[i]->is_quad())
       ||(beamline->element[i]->is_quadbpm())){
      fprintf(file,"%d %g %g %g %g\n",j++,s,bunch_energy(tb),
	      energy_spread(tb),energy_spread_rms(tb));
    }
  }
  fprintf(file,"%d %g %g %g %g\n",j++,s,bunch_energy(tb),energy_spread(tb),
	  energy_spread_rms(tb));
  close_file(file);
  beam_delete(tb);
}

/** LU decomposition, function generated from Numerical Recipes F90 */
/* Subroutine */
int ludcmp(double a[],int n,int indx[],double *d)
{
    /* System generated locals */
    int a_offset, i__1, i__2, i__3;

    /* Builtin functions */
    // /* Subroutine */ int s_paus();

    /* Local variables */
    static int imax, i__, j, k;
    static double aamax, vv[BIN_MAX_QUAD], dum, sum;

    /* Parameter adjustments */
    --indx;
    a_offset = n + 1;
    a -= a_offset;

    /* Function Body */
    *d = 1.0;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
      aamax = (double)0.;
      i__2 = n;
      for (j = 1; j <= i__2; ++j) {
	if (fabs(a[i__ + j * n]) > aamax) {
	  aamax = fabs(a[i__ + j * n]);
	}
      }
      if (aamax == 0.0) {
	my_error("Singular matrix. in ludcmp");
      }
      vv[i__ - 1] = 1.0 / aamax;
    }
    i__1 = n;
    for (j = 1; j <= i__1; ++j) {
      if (j > 1) {
	i__2 = j - 1;
	for (i__ = 1; i__ <= i__2; ++i__) {
	  sum = a[i__ + j * n];
	  if (i__ > 1) {
	    i__3 = i__ - 1;
	    for (k = 1; k <= i__3; ++k) {
	      sum -= a[i__ + k * n] * a[k + j * n];
	      /* L13: */
		    }
	    a[i__ + j * n] = sum;
	  }
	  /* L14: */
	}
      }
      aamax = (double)0.;
      i__2 = n;
      for (i__ = j; i__ <= i__2; ++i__) {
	sum = a[i__ + j * n];
	if (j > 1) {
	  i__3 = j - 1;
	  for (k = 1; k <= i__3; ++k) {
	    sum -= a[i__ + k * n] * a[k + j * n];
	    /* L15: */
	  }
		a[i__ + j * n] = sum;
	}
	dum = vv[i__ - 1] * fabs(sum);
	if (dum >= aamax) {
	  imax = i__;
	  aamax = dum;
	}
      }
      if (j != imax) {
	i__2 = n;
	for (k = 1; k <= i__2; ++k) {
	  dum = a[imax + k * n];
		a[imax + k * n] = a[j + k * n];
		a[j + k * n] = dum;
	}
	*d= -(*d);
	vv[imax - 1] = vv[j - 1];
      }
      indx[j] = imax;
      if (j != n) {
	if (a[j + j * n] == 0.0) {
	  a[j + j * n] = 1e-20;
	}
	dum = 1.0 / a[j + j * n];
	i__2 = n;
	for (i__ = j + 1; i__ <= i__2; ++i__) {
	  a[i__ + j * n] *= dum;
	}
      }
    }
    if (a[n + n * n] == 0.0) {
      a[n + n * n] = 1e-20;
    }
    return 0;
} /* ludcmp_ */

/* Subroutine */
int lubksb(double a[],int n,int indx[],double b[])
{
    /* System generated locals */
    int a_offset, i__1, i__2;

    /* Local variables */
    static int i__, j, ii, ll;
    static double sum;

    /* Parameter adjustments */
    --b;
    --indx;
    a_offset = n + 1;
    a -= a_offset;

    /* Function Body */
    ii = 0;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
      ll = indx[i__];
      sum = b[ll];
      b[ll] = b[i__];
      if (ii != 0) {
	i__2 = i__ - 1;
	for (j = ii; j <= i__2; ++j) {
	  sum -= a[i__ + j * n] * b[j];
	}
      } else if (sum != 0.0) {
	ii = i__;
      }
      b[i__] = sum;
    }
    for (i__ = n; i__ >= 1; --i__) {
      sum = b[i__];
      if (i__ < n) {
	i__1 = n;
	for (j = i__ + 1; j <= i__1; ++j) {
	  sum -= a[i__ + j * n] * b[j];
	}
      }
      b[i__] = sum / a[i__ + i__ * n];
    }
    return 0;
} /* lubksb_ */

void
hessian_0(double a0[],int nq,int nbpm,double pwgt,double a[])
{
  for (int j=0;j<nq;j++){
    for (int i=0;i<nq;i++){
      double sum0=0.0;
      for (int k=0;k<nbpm;k++){
	sum0+=a0[k+i*nbpm]*a0[k+j*nbpm];
      }
      a[i+j*nq]=2.0*sum0;
    }
    a[j+j*nq]+=2.0*pwgt;
  }
}

void
right_side_new_0(double a0[],int nq,int nbpm,double bs0[],double t0[],
		 double xr[])
{
  double sum0;
  int i,j;
  for (j=0;j<nq;j++){
    sum0=0.0;
    for (i=0;i<nbpm;i++){
      sum0+=a0[i+j*nbpm]*(bs0[i]-t0[i]);
    }
    xr[j]=-2.0*sum0;
  }
}

void
hessian(double a0[],double a1[],double a2[],int nq,int nbpm,double pwgt,
	double w0,double w,double w2,double a[])
{
  int i,j,k;
  double sum0,sum1,sum2;
  for (j=0;j<nq;j++){
    for (i=0;i<nq;i++){
      sum0=0.0;
      sum1=0.0;
      sum2=0.0;
      for (k=0;k<nbpm;k++){
	sum0+=a0[k+i*nbpm]*a0[k+j*nbpm];
	sum1+=a1[k+i*nbpm]*a1[k+j*nbpm];
	sum2+=a2[k+i*nbpm]*a2[k+j*nbpm];
      }
      a[i+j*nq]=2.0*(w0*sum0+w*(sum1+w2*sum2));
    }
    a[j+j*nq]+=2.0*pwgt;
  }
}

void
hessian_wgt(double a0[],double a1[],double a2[],int nq,int nbpm,double pwgt[],
	    double w0[],double w[],double w2[],double a[])
{
  int i,j,k;
  double sum0,sum1;
  for (j=0;j<nq;j++){
    for (i=0;i<nq;i++){
      sum0=0.0;
      sum1=0.0;
      for (k=0;k<nbpm;k++){
	sum0+=w0[k]*a0[k+i*nbpm]*a0[k+j*nbpm];
	sum1+=w[k]*(a1[k+i*nbpm]*a1[k+j*nbpm])
	  +w2[k]*(a2[k+i*nbpm]*a2[k+j*nbpm]);
      }
      a[i+j*nq]=2.0*(sum0+sum1);
    }
    a[j+j*nq]+=2.0*pwgt[j];
  }
}

void
right_side_new(double a0[],double a1[],double a2[],int nq,int nbpm,double w0,
	       double w,double /*w2*/,double bs0[],double bs1[],double bs2[],
	       double t0[],double t1[],double t2[],double xr[])
{
  double sum0,sum1;
  int i,j;
  for (j=0;j<nq;j++){
    sum0=0.0;
    sum1=0.0;
    for (i=0;i<nbpm;i++){
      sum0+=a0[i+j*nbpm]*(bs0[i]-t0[i]);
      sum1+=a1[i+j*nbpm]*(bs1[i]-bs0[i]-t1[i])
	+a2[i+j*nbpm]*(bs2[i]-bs0[i]-t2[i]);
    }
    xr[j]=-2.0*(w0*sum0+w*sum1);
  }
}

void
right_side_wgt(double a0[],double a1[],double a2[],int nq,int nbpm,double w0[],
	       double w[],double w2[],double bs0[],double bs1[],double bs2[],
	       double t0[],double t1[],double t2[],double xr[])
{
  double sum0,sum1;
  int i,j;
  for (j=0;j<nq;j++){
    sum0=0.0;
    sum1=0.0;
    for (i=0;i<nbpm;i++){
      sum0+=w0[i]*a0[i+j*nbpm]*(bs0[i]-t0[i]);
      sum1+=w[i]*(a1[i+j*nbpm]*(bs1[i]-bs0[i]-t1[i]))
	+w2[i]*(a2[i+j*nbpm]*(bs2[i]-bs0[i]-t2[i]));
    }
    xr[j]=-2.0*(sum0+sum1);
  }
}

/*
  The function calculates the correction to be applied using precaluclated
  matrices
 */

void
correct_0(double a[],int indx[],double a0[],int nq,int nbpm,double bpm0[],
	  double t0[],double /*pwgt*/,double qcorr[])
{
  right_side_new_0(a0,nq,nbpm,bpm0,t0,qcorr);

  /*
    matrices should be transponated before calling these functions but a is
    symmetric anyway
  */

  lubksb(a,nq,indx,qcorr);
}


void corr_init()
{
  corr.pwgt=0.0;
  corr.w=1.0;
  corr.w2=1.0;
  corr.w0=0.0;
}

void
correct(double a[],int indx[],double a0[],double a1[],double a2[],int nq,
	int nbpm,double /*w*/,double bpm0[],double bpm1[],double bpm2[],
	double t0[],double t1[],double t2[],double /*pwgt*/,double qcorr[])
{
  right_side_new(a0,a1,a2,nq,nbpm,corr.w0,corr.w,corr.w2,bpm0,bpm1,bpm2,
		 t0,t1,t2,qcorr);

  /*
    matrices should be transponated before calling these functions but a is
    symmetric anyway
  */

  lubksb(a,nq,indx,qcorr);
}

void
correct_wgt(double a[],int indx[],double a0[],double a1[],double a2[],int nq,
	    int nbpm,double w0[],double w[],double w2[],
	    double bpm0[],double bpm1[],double bpm2[],
	    double t0[],double t1[],double t2[],double qcorr[])
{
  right_side_wgt(a0,a1,a2,nq,nbpm,w0,w,w2,bpm0,bpm1,bpm2,t0,t1,t2,qcorr);

  /*
    matrices should be transponated before calling these functions but a is
    symmetric anyway
  */

  lubksb(a,nq,indx,qcorr);
}

void
bin_correct(BEAMLINE *beamline,int flag,BIN *bin)
{
  int i;
  double cor[BIN_MAX_QUAD],step;
#ifdef TWODIM
  double cor_x[BIN_MAX_QUAD],step_x;
#endif

  if (flag){
    if (bin->a){
      correct_wgt(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,
		  bin->w0,bin->w,bin->w2,
		  bin->b0,bin->b1,bin->b2,bin->t0,bin->t1,bin->t2,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct_wgt(bin->a_x,bin->indx_x,bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,
		  bin->nbpm_x,bin->w0_x,bin->w_x,bin->w2_x,
		  bin->b0_x,bin->b1_x,bin->b2_x,
		  bin->t0_x,bin->t1_x,bin->t2_x,cor_x);
    }
#endif
  }
  else{
    if (bin->a){
      correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,
		bin->t0,corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct_0(bin->a_x,bin->indx_x,bin->a0_x,bin->nq_x,bin->nbpm_x,bin->b0_x,
		bin->t0_x,corr.pwgt,cor_x);
    }
#endif
  }
  if (bin->a){
    for (i=0;i<bin->qlast;i++){
      step=(cor[i]+bin->cor0[i])*bin->gain;
      bin->cor0[i]+=cor[i];
      bin->cor0[i]*=bin->dtau;
      if (errors.quad_step_size>0.0){
	step=rint(step/errors.quad_step_size)*errors.quad_step_size;
      }
      step+=errors.quad_move_res*Misalignments.Gauss();
      //element_add_offset(beamline->element[bin->quad[i]],step,0.0);
      beamline->element[bin->quad[i]]->correct_y(step);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(i=0;i<bin->qlast;i++){
      step_x=(cor_x[i]+bin->cor0_x[i])*bin->gain;
      bin->cor0_x[i]+=cor_x[i];
      bin->cor0_x[i]*=bin->dtau;
      if (errors.quad_step_size>0.0){
	step_x=rint(step_x/errors.quad_step_size)*errors.quad_step_size;
      }
      step_x+=errors.quad_move_res*Misalignments.Gauss();
      //element_add_offset_x(beamline->element[bin->quad[i]],step_x,0.0);
      beamline->element[bin->quad[i]]->correct_x(step_x);
    }
  }
#endif
}

/*
  Corrects a bin with a gain "gain"
*/

void bin_correct_step(BEAMLINE *beamline,int flag,BIN *bin,double gain)
{
  int i;
  double cor[BIN_MAX_QUAD],step;
#ifdef TWODIM
  double cor_x[BIN_MAX_QUAD],step_x;
#endif

  if (flag){
    if (bin->a){
      correct(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,
	      corr.w,bin->b0,bin->b1,bin->b2,bin->t0,bin->t1,bin->t2,
	      corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct(bin->a_x,bin->indx_x,bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,
	      bin->nbpm_x,corr.w,bin->b0_x,bin->b1_x,bin->b2_x,
	      bin->t0,bin->t1,bin->t2,corr.pwgt,cor_x);
    }
#endif
  }
  else{
    if (bin->a){
      correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,bin->t0,
		corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct_0(bin->a_x,bin->indx_x,bin->a0_x,bin->nq_x,bin->nbpm_x,bin->b0_x,
		bin->t0_x,corr.pwgt,cor_x);
    }
#endif
  }
  if (bin->a){
    for (i=0;i<bin->qlast;i++){
	step=gain*cor[i];
	if (errors.quad_step_size>0.0){
	    step=rint(step/errors.quad_step_size)*errors.quad_step_size;
	}
	step+=errors.quad_move_res*Misalignments.Gauss();
	//element_add_offset(beamline->element[bin->quad[i]],step,0.0);
	beamline->element[bin->quad[i]]->correct_y(step);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(i=0;i<bin->qlast;i++){
      step_x=gain*cor_x[i];
      if (errors.quad_step_size>0.0){
	step_x=rint(step_x/errors.quad_step_size)*errors.quad_step_size;
      }
      step_x+=errors.quad_move_res*Misalignments.Gauss();
      //element_add_offset_x(beamline->element[bin->quad_x[i]],step_x,0.0);
      beamline->element[bin->quad_x[i]]->correct_x(step_x);
    }
  }
#endif
}

/* Corrects the quadrupole positions and moves the BPMs either inside the
   quadrupole or in front of it simultaneously */

void bin_correct_nlc(BEAMLINE *beamline,int flag,BIN *bin)
{
  int i;
  double cor[BIN_MAX_QUAD],step;
  if (flag){
    correct(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.w,
	    bin->b0,bin->b1,bin->b2,bin->t0,bin->t1,bin->t2,corr.pwgt,cor);
  }
  else{
    correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,bin->t0,
	      corr.pwgt,cor);
  }
  for (i=0;i<bin->qlast;i++){
    step=cor[i]*bin->gain;
    if (errors.quad_step_size>0.0){
	step=rint(step/errors.quad_step_size)*errors.quad_step_size;
    }
    step+=errors.quad_move_res*Misalignments.Gauss();
    //element_add_offset(beamline->element[bin->quad[i]],step,0.0);
    beamline->element[bin->quad[i]]->correct_y(step);    
    if (i>0){
      if (bin->bpm[i-1]!=bin->quad[i]){
	element_add_offset(beamline->element[bin->bpm[i-1]],step,0.0);
      }
    }
  }
#ifdef TWODIM
  if (flag){
    correct(bin->a_x,bin->indx_x,bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,
	    bin->nbpm_x,corr.w,bin->b0_x,bin->b1_x,bin->b2_x,
	    bin->t0_x,bin->t1_x,bin->t2_x,corr.pwgt,cor);
  }
  else{
    correct_0(bin->a_x,bin->indx_x,bin->a0_x,bin->nq_x,bin->nbpm_x,bin->b0_x,
	      bin->t0_x,corr.pwgt,cor);
  }
  for (i=0;i<bin->qlast_x;i++){
    step=cor[i]*bin->gain;
    if (errors.quad_step_size>0.0){
	step=rint(step/errors.quad_step_size)*errors.quad_step_size;
    }
    step+=errors.quad_move_res*Misalignments.Gauss();
    //element_add_offset_x(beamline->element[bin->quad_x[i]],step,0.0);
    beamline->element[bin->quad_x[i]]->correct_x(step);
    if (i>0){
      if (bin->bpm_x[i-1]!=bin->quad_x[i]){
	element_add_offset_x(beamline->element[bin->bpm_x[i-1]],step,0.0);
      }
    }
  }
#endif
}

void bin_finish(BIN *bin,int flag)
{
  int i,n;
  double *a0,*a1,*a2,d;
  if (flag){
    for (i=0;i<bin->nbpm;++i) {
      bin->t1[i]-=bin->t0[i];
      bin->t2[i]-=bin->t0[i];
      bin->w[i]=corr.w;
      bin->w2[i]=corr.w2;
      bin->w0[i]=corr.w0;
    }
#ifdef TWODIM
    for (i=0;i<bin->nbpm_x;++i) {
      bin->t1_x[i]-=bin->t0_x[i];
      bin->t2_x[i]-=bin->t0_x[i];
      bin->w_x[i]=corr.w;
      bin->w2_x[i]=corr.w2;
      bin->w0_x[i]=corr.w0;
    }
#endif
    for (i=0;i<bin->nq;++i) {
      bin->pw[i]=corr.pwgt;
    }
#ifdef TWODIM
    for (i=0;i<bin->nq_x;++i) {
      bin->pw_x[i]=corr.pwgt;
    }
#endif
    if (bin->a) {
      n=bin->nq*bin->nbpm;
      a0=bin->a0;
      a1=bin->a1;
      a2=bin->a2;
      for (i=0;i<n;i++){
	*a1++ -= *a0;
	*a2++ -= *a0++;
      }
    }
#ifdef TWODIM
    if (bin->a_x) {
      n=bin->nq_x*bin->nbpm_x;
      a0=bin->a0_x;
      a1=bin->a1_x;
      a2=bin->a2_x;
      for (i=0;i<n;i++){
	*a1++ -= *a0;
	*a2++ -= *a0++;
      }
    }
  }
#endif
  if (flag){
    if (bin->a){
      hessian_wgt(bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,bin->pw,bin->w0,
		  bin->w,bin->w2,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian_wgt(bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,bin->nbpm_x,
		  bin->pw_x,bin->w0_x,bin->w_x,bin->w2_x,bin->a_x);
    }
#endif
  }
  else{
    if (bin->a){
      hessian_0(bin->a0,bin->nq,bin->nbpm,corr.pwgt,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian_0(bin->a0_x,bin->nq_x,bin->nbpm_x,corr.pwgt,bin->a_x);
    }
#endif
  }
  if(bin->a){
    ludcmp(bin->a,bin->nq,bin->indx,&d);
  }
#ifdef TWODIM
  if(bin->a_x){
    ludcmp(bin->a_x,bin->nq_x,bin->indx_x,&d);
  }
#endif
}

void bin_finish_new(BIN *bin,int flag)
{
  int i,n;
  double *a0,*a1,*a2,d;
  n=bin->nq*bin->nbpm;
  a0=bin->a0;
  a1=bin->a1;
  a2=bin->a2;
  if (flag){
    for (i=0;i<bin->nbpm;++i) {
      bin->t1[i]-=bin->t0[i];
      bin->t2[i]-=bin->t0[i];
#ifdef TWODIM
      bin->t1_x[i]-=bin->t0_x[i];
      bin->t2_x[i]-=bin->t0_x[i];
#endif
    }
    for (i=0;i<n;i++){
      *a1++ -= *a0;
      *a2++ -= *a0;
      if ((i+1)%bin->nbpm) {
	*a0=0.0;
      }
      a0++;
    }
  }
  if (flag){
    if (bin->a){
      hessian(bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.pwgt,corr.w0,
	      corr.w,corr.w2,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian(bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,bin->nbpm_x,corr.pwgt,
	      corr.w0,corr.w,corr.w2,bin->a_x);
    }
#endif
  }
  else{
    if (bin->a){
      hessian_0(bin->a0,bin->nq,bin->nbpm,corr.pwgt,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian_0(bin->a0_x,bin->nq_x,bin->nbpm_x,corr.pwgt,bin->a_x);
    }
#endif
  }
  if(bin->a){
    ludcmp(bin->a,bin->nq,bin->indx,&d);
  }
#ifdef TWODIM
  if(bin->a_x){
    ludcmp(bin->a_x,bin->nq_x,bin->indx_x,&d);
  }
#endif
}

void bin_finish_nlc(BIN *bin,int flag)
{
  int i,n;
  double *a0,*a1,*a2,d;
  double *a0_x,*a1_x,*a2_x;
  n=bin->nq;
  a0=bin->a0;
  a1=bin->a1;
  a2=bin->a2;
  for (i=1;i<n;i++){
    a0[i*n+i-1]=-1.0;
  }
  n=bin->nq*bin->nbpm;
  for (i=0;i<n;i++){
    *a1++ -= *a0;
    *a2++ -= *a0++;
  }
#ifdef TWODIM
  n=bin->nq_x;
  a0_x=bin->a0_x;
  a1_x=bin->a1_x;
  a2_x=bin->a2_x;
  for (i=1;i<n;i++){
    a0_x[i*n+i-1]=-1.0;
  }
  n=bin->nq_x*bin->nbpm_x;
  for (i=0;i<n;i++){
    *a1_x++ -= *a0_x;
    *a2_x++ -= *a0_x++;
  }
#endif
  if (flag){
    hessian(bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.pwgt,corr.w0,
	    corr.w,corr.w2,bin->a);
  }
  else{
    hessian_0(bin->a0,bin->nq,bin->nbpm,corr.pwgt,bin->a);
  }
  ludcmp(bin->a,bin->nq,bin->indx,&d);
#ifdef TWODIM
  if (flag){
    hessian(bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,bin->nbpm_x,corr.pwgt,
	    corr.w0,corr.w,corr.w2,bin->a_x);
  }
  else{
    hessian_0(bin->a0_x,bin->nq_x,bin->nbpm_x,corr.pwgt,bin->a_x);
  }
  ludcmp(bin->a_x,bin->nq_x,bin->indx_x,&d);
#endif
}

void print_position_bin(FILE *file,BEAMLINE *beamline,BIN *bin)
{
  int i;
  for (i=0;i<bin->nq;i++){
    if (QUADRUPOLE *quad=beamline->element[bin->quad[i]]->quad_ptr()) {
      fprintf(file,"Q[%d]: %s %g %g\n",i,
	    typeid(*beamline->element[bin->quad[i]]).name(),
	    beamline->element[bin->quad[i]]->offset.y,
	    quad->get_strength());
    }
  }
  for (i=0;i<bin->nbpm;i++){
    if (BPM *bpm=beamline->element[bin->bpm[i]]->bpm_ptr()) {
      fprintf(file,"B[%d]: %s %g %g\n",i,
  	      typeid(*beamline->element[bin->bpm[i]]).name(),
  	      beamline->element[bin->bpm[i]]->offset.y,
	      bpm->get_bpm_y_reading());
    }
  }
  fflush(file);
}

static const char *element_type_string(ELEMENT *element )
{
	return typeid(*element).name();
}

void bin_print(FILE *file,BEAMLINE *beamline,BIN *bin,int n)
{
  int i,j;
  fprintf(file,"BIN: %d\n",n);
  placet_printf(INFO,"vertical plane\n");
  for(i=0;i<bin->nq;i++){
    fprintf(file,"QUAD: %d %s (%d)\n",i,
	    element_type_string(beamline->element[bin->quad[i]]),bin->quad[i]);
  }
  for(i=0;i<bin->nbpm;i++){
    fprintf(file,"BPM: %d %s (%d)\n",i,
	   element_type_string(beamline->element[bin->bpm[i]]),bin->bpm[i]);
  }
  for (i=0;i<bin->nq;i++){
    for (j=0;j<bin->nbpm;j++){
      fprintf(file,"%g ",(bin->a0)[i*bin->nbpm+j]);
    }
    fprintf(file,"\n");
  }
#ifdef TWODIM
  placet_printf(INFO,"horizontal plane\n");
  for(i=0;i<bin->nq_x;i++){
    fprintf(file,"QUAD: %d %s (%d)\n",i,
	    element_type_string(beamline->element[bin->quad_x[i]]),
	    bin->quad_x[i]);
  }
  for(i=0;i<bin->nbpm_x;i++){
    fprintf(file,"BPM: %d %s (%d)\n",i,
	    element_type_string(beamline->element[bin->bpm_x[i]]),
	    bin->bpm_x[i]);
  }
  for (i=0;i<bin->nq_x;i++){
    for (j=0;j<bin->nbpm_x;j++){
      fprintf(file,"%g ",(bin->a0_x)[i*bin->nbpm_x+j]);
    }
    fprintf(file,"\n");
  }
#endif
}

void 
beamline_bin_divide(BEAMLINE *beamline,int nq,int interleave,
		    BIN **bin,int *bin_number)
{
  int start,nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  *bin_number=0;
  start=0;
  while (start>=0){
    bin_define(beamline,start,nq,bpm,&nbpm,quad,&nquad);
//placet_printf(INFO,"bin: %d %d %d\n",*bin_number,nquad,nbpm);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-interleave+1)||((nquad>=interleave+1)&&(interleave==0))){
//      qlast=nquad-interleave;
      qlast=nq-interleave;
      if (nquad<qlast) qlast=nquad;
      //
      //EPAC06
      //
      qlast=nquad;
      start=quad[qlast-1]+1;
    }
    else{
      start=-1;
      qlast=nquad;
    }
    if (nquad>0){
      bin[*bin_number]=bin_make(nquad,nbpm);
      bin_set_elements(bin[*bin_number],quad,nquad,bpm,nbpm,qlast);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d bins\n",*bin_number);
}

void
beamline_bin_divide_limit_correctors(BEAMLINE *beamline,int start,int end,
					int *correctors, int ncorrectors,
       					int nq,int interleave,BIN **bin,int *bin_number)
{
  int nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  *bin_number=0;
  while (start>=0){
    bin_define_limit_correctors(beamline,start,end,correctors,ncorrectors,nq,bpm,&nbpm,quad,&nquad);
//placet_printf(INFO,"bin: %d %d %d\n",*bin_number,nquad,nbpm);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-interleave+1)||((nquad>=interleave+1)&&(interleave==0))){
//      qlast=nquad-interleave;
      qlast=nq-interleave;
      if (nquad<qlast) qlast=nquad;
      start=quad[qlast-1]+1;
    }
    else{
      start=-1;
      qlast=nquad;
    }
    if (nquad>0){
      bin[*bin_number]=bin_make(nquad,nbpm);
      bin_set_elements(bin[*bin_number],quad,nquad,bpm,nbpm,qlast);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d bins\n",*bin_number);
}

void
beamline_bin_divide_limit(BEAMLINE *beamline,int start,int end,
			  int nq,int interleave,BIN **bin,int *bin_number)
{
  int nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  *bin_number=0;
  while (start>=0){
    bin_define_limit(beamline,start,end,nq,bpm,&nbpm,quad,&nquad);
//placet_printf(INFO,"bin: %d %d %d\n",*bin_number,nquad,nbpm);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-interleave+1)||((nquad>=interleave+1)&&(interleave==0))){
//      qlast=nquad-interleave;
      qlast=nq-interleave;
      if (nquad<qlast) qlast=nquad;
      start=quad[qlast-1]+1;
    }
    else{
      start=-1;
      qlast=nquad;
    }
    if (nquad>0){
      bin[*bin_number]=bin_make(nquad,nbpm);
      bin_set_elements(bin[*bin_number],quad,nquad,bpm,nbpm,qlast);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d bins\n",*bin_number);
}

void beamline_bin_divide_dipole(BEAMLINE *beamline,int nq,int interleave,
			   BIN **bin,int *bin_number)
{
  int start,nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  *bin_number=0;
  start=0;
  while (start>=0){
    bin_define_dipole(beamline,start,nq,bpm,&nbpm,quad,&nquad);
    //placet_printf(INFO,"bin: %d %d %d\n",*bin_number,nquad,nbpm);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-interleave+1)||((nquad>=interleave+1)&&(interleave==0))){
      qlast=nquad-interleave;
      start=quad[qlast-1]+1;
    }
    else{
      start=-1;
      qlast=nquad;
    }
    if (nquad>0){
      bin[*bin_number]=bin_make(nquad,nbpm);
      bin_set_elements(bin[*bin_number],quad,nquad,bpm,nbpm,qlast);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d bins\n",*bin_number);
}

void beamline_bin_divide_ballistic(BEAMLINE *beamline,int nq,int interleave,
			     BIN **bin,int *bin_number)
{
  int start,nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM],i;
  *bin_number=0;
  start=0;
  while (start>=0){
    bin_define(beamline,start,nq,bpm,&nbpm,quad,&nquad);
    placet_printf(INFO,"bin: %d %d %d\n",*bin_number,nquad,nbpm);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-interleave+1)||((nquad>=1)&&(interleave==0))){
      for (qlast=0;qlast<nbpm;qlast++){
	if (bpm[qlast]>quad[nquad-interleave-1]) break;
      }
      qlast++;
      start=quad[nquad-interleave-1]+1;
    }
    else{
      start=-1;
      qlast=nbpm;
    }
    if (nquad>0){
      bin[*bin_number]=bin_make(nbpm,nbpm);
      for (i=1;i<nbpm;i++){
	quad[i]=bpm[i-1];
      }
      bin_set_elements(bin[*bin_number],quad,nbpm,bpm,nbpm,qlast);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d ballistic bins\n",*bin_number);
}

void beamline_bin_divide_1(BEAMLINE *beamline,int nq,int interleave,
		      BIN **bin,int *bin_number)
{
  int start,nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  *bin_number=0;
  start=0;
  while (start>=0){
    bin_define_1(beamline,start,nq,bpm,&nbpm,quad,&nquad);
    //    placet_printf(INFO,"bin %d %d %d %d %d\n",*bin_number,nquad,nbpm,quad[0],bpm[0]);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-interleave+1)||((nquad>=interleave+1)&&(interleave==0))){
      qlast=nquad-interleave;
      start=quad[qlast-1]+1;
    }
    else{
      qlast=nquad;
      start=-1;
    }
    if (nquad>0){
      bin[*bin_number]=bin_make(nquad,nbpm);
      bin_set_elements(bin[*bin_number],quad,nquad,bpm,nbpm,qlast);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d interleaved bins\n",*bin_number);
}

void beamline_bin_divide_interleave(BEAMLINE *beamline,BIN **bin,int *bin_number)
{
  int start,nquad,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM],
    nbpm_x,nquad_x;
  int bpm_x[1],bpm_y[1],quad_x[1],quad_y[1];
  *bin_number=0;
  start=0;
  while (start>=0){
    bin_define_one_to_one(beamline,start,bpm,&nbpm,quad,&nquad);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if (nquad>0){
      start=quad[nquad-1]+1;
    }
    else{
      start=-1;
      break;
    }
    if (beamline->element[quad[0]]->quad_ptr()->get_strength()>0.0) {
      nquad_x=nquad/2;
      nbpm_x=nbpm/2;
      nquad=(nquad+1)/2;
      nbpm=(nbpm+1)/2;
      quad_y[0]=quad[0];
      quad_x[0]=quad[1];
      bpm_y[0]=bpm[0];
      bpm_x[0]=bpm[1];
    }
    else{
      nquad_x=(nquad+1)/2;
      nbpm_x=(nbpm+1)/2;
      nquad=nquad/2;
      nbpm=nbpm/2;
      quad_x[0]=quad[0];
      quad_y[0]=quad[1];
      bpm_x[0]=bpm[0];
      bpm_y[0]=bpm[1];
    }
    if ((nquad>0)||(nquad_x>0)){
      bin[*bin_number]=bin_make_x(nquad,nbpm,nquad_x,nbpm_x);
      bin_set_elements_x(bin[*bin_number],quad_y,nquad,bpm_y,nbpm,
			 quad_x,nquad_x,bpm_x,nbpm_x,
			 nquad,nquad_x);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d interleaved bins\n",*bin_number);
}

void beamline_bin_divide_dipole_interleave(BEAMLINE *beamline,BIN **bin,
				      int *bin_number)
{
  int start,nquad,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM],
    nbpm_x,nquad_x;
  int bpm_x[1],bpm_y[1],quad_x[1],quad_y[1];
  *bin_number=0;
  start=0;
  while (start>=0){
    bin_define_dipole_one_to_one(beamline,start,bpm,&nbpm,quad,&nquad);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if (nquad>0){
      start=quad[nquad-1]+1;
    }
    else{
      start=-1;
      break;
    }
    if (beamline->element[quad[0]]->quad_ptr()->get_strength()>0.0) {
      nquad_x=nquad/2;
      nbpm_x=nbpm/2;
      nquad=(nquad+1)/2;
      nbpm=(nbpm+1)/2;
      quad_y[0]=quad[0];
      quad_x[0]=quad[1];
      bpm_y[0]=bpm[0];
      bpm_x[0]=bpm[1];
    }
    else{
      nquad_x=(nquad+1)/2;
      nbpm_x=(nbpm+1)/2;
      nquad=nquad/2;
      nbpm=nbpm/2;
      quad_x[0]=quad[0];
      quad_y[0]=quad[1];
      bpm_x[0]=bpm[0];
      bpm_y[0]=bpm[1];
    }
    if ((nquad>0)||(nquad_x>0)){
      bin[*bin_number]=bin_make_x(nquad,nbpm,nquad_x,nbpm_x);
      bin_set_elements_x(bin[*bin_number],quad_y,nquad,bpm_y,nbpm,
			 quad_x,nquad_x,bpm_x,nbpm_x,
			 nquad,nquad_x);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d interleaved bins\n",*bin_number);
}

void beamline_bpm_switch_off(BEAMLINE * /*beamline*/,int /*sign*/)
{
  /* function does nothing: JS
  int i,n,off=0;
  ELEMENT **element;
  n=beamline->n_elements;
  element=beamline->element;
  for (i=0;i<n;i++){
    if (QUADRUPOLE *quad=element[i]->quad_ptr()){
      if (sign>0){
	if (quad->get_strength()>0.0){
	  off=1;
	}
	else{
	  off=0;
	}
      }
      else{
	if (sign<0){
	  if (quad->get_strength()<0.0){
	    off=1;
	  }
	  else{
	    off=0;
	  }
	}
      }
    }
    else{
/ *      if (element[i]->is_bpm()){
	if (off) {
	  element[i]->type=BPMDRIFT;
	}
      }
* /  }
  }
  */
}
void simple_bin_fill(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		BEAM *workbunch)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    bin_response(beamline,bunch,workbunch,0,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,1,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,2,bin[i]);

    bin_finish(bin[i],0);
//    placet_printf(INFO,"bin %d filled\n",i);
    ipos=bin[i]->start;
//    bin_print(stdout,beamline,bin[i],i);
  }
}

void simple_bin_fill_dipole(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		       BEAM *workbunch)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    bin_response_dipole(beamline,bunch,workbunch,0,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,1,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,2,bin[i]);

    bin_finish(bin[i],0);
//    placet_printf(INFO,"bin %d filled\n",i);
    ipos=bin[i]->start;
//    bin_print(stdout,beamline,bin[i],i);
  }
}

void simple_bin_fill_b(BEAMLINE *beamline,double quad[],double quad0[],BIN **bin,
		  BIN **bin1,int nbin,BEAM *bunch,BEAM *workbunch)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    quadrupoles_bin_set_b(beamline,bin1[i],quad,quad0);
    bin_response_b(beamline,bunch,NULL,workbunch,0,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,1,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,2,bin[i]);

    quadrupoles_bin_set(beamline,bin1[i],quad0);
    
    bin_finish(bin[i],0);
    ipos=bin[i]->start;
  }
}

void simple_bin_fill_nlc(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		    BEAM *workbunch)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch,ipos,bin[i]->start);
    bin_response(beamline,bunch,workbunch,0,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,1,bin[i]);
    bin_response_zero(beamline,bunch,workbunch,2,bin[i]);
    
    bin_finish_nlc(bin[i],0);
    placet_printf(INFO,"bin: %d contains %d quads %d BPMs\nfirst %d %d\n",
	   i,bin[i]->nq,bin[i]->nbpm,bin[i]->quad[0],bin[i]->bpm[0]);
    ipos=bin[i]->start;
  }
}

void feedback_correct_3(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		   BEAM *workbunch,double dist)
{
  int ipos,i;
  double s=0.0;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    emitt_data.store(i,bunch, beamline->get_length());
    if ((bin[i]->s_pos-s)>dist){
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      bin_correct(beamline,0,bin[i]);
//      bunch_feedback(beamline->element[bin[i]->start],bunch);
      s+=dist;
    } 
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
  emitt_data.store(nbin,bunch,beamline->get_length());
}

void bump_init(int i,int quad_dist,int emitt_dist)
{
  bump_data.iterations=i;
  bump_data.quad_dist=quad_dist;
  bump_data.emitt_dist=emitt_dist;
  bump_data.nw=7;
  bump_data.ns=1;
}

BUMP* bump_make(int n_slices)
{
  BUMP *bump;
  bump=(BUMP*)xmalloc(sizeof(BUMP));
  bump->a11=(double*)xmalloc(sizeof(double)*n_slices);
  bump->a12=(double*)xmalloc(sizeof(double)*n_slices);
  bump->a21=(double*)xmalloc(sizeof(double)*n_slices);
  bump->a22=(double*)xmalloc(sizeof(double)*n_slices);
  bump->n=n_slices;
  return bump;
}

void bump_set_free(BEAMLINE *beamline,BUMP *bump,int which,double val)
{
  int i;
  for (i=0;i<bump->ncav[which]/2;i++){
    //element_add_offset(beamline->element[bump->cav[which][i]],val,0.0);
    beamline->element[bump->cav[which][i]]->vary_vcorr(val);
  }
  for (;i<bump->ncav[which];i++){
    //element_add_offset(beamline->element[bump->cav[which][i]],-val,0.0);
    beamline->element[bump->cav[which][i]]->vary_vcorr(-val);
  }
}

void bump_set_angle(BEAMLINE *beamline,BUMP *bump,int which,double val)
{
  int i;
  for (i=0;i<bump->ncav[which];i++){
    element_add_offset(beamline->element[bump->cav[which][i]],0.0,val);
  }
}

void bump_set(BEAMLINE *beamline,BUMP *bump,int which,double val)
{
  int i;
  if (bump->type==3) {
    bump_set_free(beamline,bump,which,val);
    return;
  }
  if (bump->type==4) {
    bump_set_angle(beamline,bump,which,val);
    return;
  }
  for (i=0;i<bump->ncav[which];i++){
    element_add_offset(beamline->element[bump->cav[which][i]],val,0.0);
  }
}

/* steps though a bump */

void bump_step(BEAMLINE *beamline,BUMP *bump,BEAM *bunch,BEAM *workbunch)
{
  double tmp,tmp2;
  //  int i;

  bunch_track(beamline,bunch,bump->start,bump->feedback->start);
  beam_copy(bunch,workbunch);
  bin_measure(beamline,workbunch,0,bump->feedback);
  /*
  for(i=0;i<bump->ncav[0];i++){
    placet_printf(INFO,"%d %g %g\n",i,beamline->element[bump->cav[0][i]]->info1,
	   beamline->element[bump->cav[0][i]]->info2);
  }
  */
  tmp=errors.quad_move_res;
  tmp2=errors.quad_step_size;
  errors.quad_move_res=0.0;
  errors.quad_step_size=0.0;
  // scd temp 2004
  bin_correct(beamline,0,bump->feedback);
  // scd end temp 2004
  errors.quad_move_res=tmp;
  errors.quad_step_size=tmp2;
  bunch_track(beamline,bunch,bump->feedback->start,bump->stop);
  /*
  for(i=0;i<bump->ncav[0];i++){
    placet_printf(INFO,">>%d %g %g\n",i,beamline->element[bump->cav[0][i]]->info1,
	   beamline->element[bump->cav[0][i]]->info2);
  }
  */
}

/* steps though a bump */

void bump_step_meas(BEAMLINE *beamline,BUMP *bump,BIN ** /*bin*/,int /*nbin*/,
		    BEAM *bunch,BEAM *workbunch)
{
  double tmp,tmp2;

  /*
  if (bump_long==0) {
    bump_step(beamline,bump,bunch);
    beam_copy(bunch,workbunch);
    return;
  }
  */

  bunch_track(beamline,bunch,bump->start,bump->feedback->start);
  beam_copy(bunch,workbunch);
  tmp=errors.bpm_resolution;
  errors.bpm_resolution=0.0;
  bin_measure(beamline,workbunch,0,bump->feedback);
  errors.bpm_resolution=tmp;
  tmp=errors.quad_move_res;
  tmp2=errors.quad_step_size;
  errors.quad_move_res=0.0;
  errors.quad_step_size=0.0;
  bin_correct(beamline,0,bump->feedback);
  errors.quad_move_res=tmp;
  errors.quad_step_size=tmp2;
  bunch_track(beamline,bunch,bump->feedback->start,bump->stop);
  beam_copy(bunch,workbunch);
  bunch_track(beamline,workbunch,bump->stop,beamline->n_elements);
}

void bump_step_emitt(BEAMLINE *beamline,BUMP *bump,BEAM *bunch,BEAM * /*workbunch*/)
{
  bunch_track_emitt(beamline,bunch,bump->start,bump->stop);
  return;
  /*
  bunch_track_emitt(beamline,bunch,bump->start,bump->stop-1);
  bunch_feedback(beamline->element[bump->stop-1],bunch);
  bunch_track_emitt(beamline,bunch,bump->stop-1,bump->stop);
  return;
  */
  /*
  double tmp,tmp2;
  bunch_track_emitt(beamline,bunch,bump->start,bump->feedback->start);
  beam_copy(bunch,workbunch);
  tmp=errors.quad_move_res;
  tmp2=errors.quad_step_size;
  errors.quad_move_res=0.0;
  errors.quad_step_size=0.0;
  bin_measure(beamline,workbunch,0,bump->feedback);
  errors.quad_move_res=tmp;
  errors.quad_step_size=tmp2;
  bin_correct(beamline,0,bump->feedback);
  bunch_track_emitt(beamline,bunch,bump->feedback->start,bump->stop);
  */
}

void bump_fill(BEAMLINE *beamline,BUMP *bump,BEAM *bunch0,BEAM *workbunch,
	  BEAM *testbunch)
{
  int i,nc,ns,nb;
  double alpha=0.0,beta=0.0,eps=0.0;
  double y,yp,wsum=0.0;

  nb=bunch0->bunches;
  ns=bunch0->slices_per_bunch*bunch0->macroparticles;
  nc=bunch0->slices/2;
  /* first calibrate feedback loop */
  beam_copy(bunch0,workbunch);
  bunch_track(beamline,workbunch,bump->start,bump->feedback->start);
  bin_response(beamline,workbunch,testbunch,0,bump->feedback);
  bin_response_zero(beamline,workbunch,testbunch,1,bump->feedback);
  bin_response_zero(beamline,workbunch,testbunch,2,bump->feedback);
  bin_finish(bump->feedback,0);
  placet_printf(INFO,"f_cav: %d %d\n",bump->cav[0][0],bump->cav[1][0]);
  placet_printf(INFO,"f: %g %g %g %g\n",bump->feedback->a[0],bump->feedback->a[1],
	 bump->feedback->a[2],bump->feedback->a[3]);

  /* track nominal beam */
  beam_copy(bunch0,workbunch);
  bump_step(beamline,bump,workbunch,testbunch);

  /* subtract from other beams in advance */  
#ifdef BUMP_AXIS
  y=0.0; yp=0.0;
#else
  y=bunch_get_offset_y(workbunch);
  yp=bunch_get_offset_yp(workbunch);
#endif
  for (i=0;i<ns*(nb-1);i++){
    bump->a11[i]=-(workbunch->particle[i].y-y);
    bump->a12[i]=-(workbunch->particle[i].y-y);
    bump->a21[i]=-(workbunch->particle[i].yp-yp);
    bump->a22[i]=-(workbunch->particle[i].yp-yp);
    eps+=(workbunch->sigma[i].r11*workbunch->sigma[i].r22
	  -workbunch->sigma[i].r12*workbunch->sigma[i].r21)
      *workbunch->particle[i].wgt;
    beta+=workbunch->sigma[i].r11*workbunch->particle[i].wgt;
    alpha-=workbunch->sigma[i].r12*workbunch->particle[i].wgt;
    wsum+=workbunch->particle[i].wgt;
  }
  for (;i<ns*nb;i++){
    bump->a11[i]=-(workbunch->particle[i].y-y);
    bump->a12[i]=-(workbunch->particle[i].y-y);
    bump->a21[i]=-(workbunch->particle[i].yp-yp);
    bump->a22[i]=-(workbunch->particle[i].yp-yp);
    eps+=(workbunch->sigma[i].r11*workbunch->sigma[i].r22
	  -workbunch->sigma[i].r12*workbunch->sigma[i].r21)
      *workbunch->particle[i].wgt*bunch0->last_wgt;
    beta+=workbunch->sigma[i].r11*workbunch->particle[i].wgt
	*workbunch->last_wgt;
    alpha-=workbunch->sigma[i].r12*workbunch->particle[i].wgt
	*workbunch->last_wgt;
    wsum+=workbunch->particle[i].wgt*workbunch->last_wgt;
  }
  eps/=wsum;
  alpha/=wsum;
  beta/=wsum;
  eps=(workbunch->sigma[nc].r11*workbunch->sigma[nc].r22
	  -workbunch->sigma[nc].r12*workbunch->sigma[nc].r21);
  beta=workbunch->sigma[nc].r11;
  alpha=-workbunch->sigma[nc].r12;
  eps=sqrt(eps);
  beta/=eps;
  alpha/=eps;
  bump->alpha=alpha;
  bump->beta=beta;
  bump->gamma=(1.0+alpha*alpha)/beta;

  /* track bunch for shifted first structure */
  bump_set(beamline,bump,0,100.0);
  beam_copy(bunch0,workbunch);
  bump_step(beamline,bump,workbunch,testbunch);
  
#ifdef BUMP_AXIS
  y=0.0; yp=0.0;
#else
  y=bunch_get_offset_y(workbunch);
  yp=bunch_get_offset_yp(workbunch);
#endif
  for (i=0;i<bunch0->slices;i++){
    bump->a11[i]+=(workbunch->particle[i].y-y);
    bump->a21[i]+=(workbunch->particle[i].yp-yp);
  }
  bump_set(beamline,bump,0,-100.0);

  /* track bunch for shifted second structure */
  bump_set(beamline,bump,1,100.0);
  beam_copy(bunch0,workbunch);
  bump_step(beamline,bump,workbunch,testbunch);

#ifdef BUMP_AXIS
  y=0.0; yp=0.0;
#else
  y=bunch_get_offset_y(workbunch);
  yp=bunch_get_offset_yp(workbunch);
#endif
  for (i=0;i<bunch0->slices;i++){
    bump->a12[i]+=(workbunch->particle[i].y-y);
    bump->a22[i]+=(workbunch->particle[i].yp-yp);
  }
  bump_set(beamline,bump,1,-100.0);
  for (i=0;i<bunch0->slices;i++){
    bump->a11[i]/=100.0;
    bump->a12[i]/=100.0;
    bump->a21[i]/=100.0;
    bump->a22[i]/=100.0;
  }
  placet_printf(INFO,"%g %g %g %g\n",bump->a11[0],bump->a12[0],bump->a21[0],bump->a22[0]);
}

void bump_fill_long(BEAMLINE *beamline,BUMP *bump,BIN **bin,int nbin,
		    BEAM *bunch0,BEAM *workbunch,BEAM *testbunch)
{
  int i,nc,ns,nb;
  double alpha=0.0,beta=0.0,eps=0.0;
  double y,yp,wsum=0.0;
  
  nb=bunch0->bunches;
  ns=bunch0->slices_per_bunch*bunch0->macroparticles;
  nc=bunch0->slices/2;
  /* first calibrate feedback loop */
  beam_copy(bunch0,workbunch);
  bunch_track(beamline,workbunch,bump->start,bump->feedback->start);
  bin_response(beamline,workbunch,testbunch,0,bump->feedback);
  bin_response_zero(beamline,workbunch,testbunch,1,bump->feedback);
  bin_response_zero(beamline,workbunch,testbunch,2,bump->feedback);
  bin_finish(bump->feedback,0);
  placet_printf(INFO,"f: %g %g %g %g\n",bump->feedback->a[0],bump->feedback->a[1],
	 bump->feedback->a[2],bump->feedback->a[3]);

  /* track nominal beam */
  beam_copy(bunch0,workbunch);
  bump_step_meas(beamline,bump,bin,nbin,workbunch,testbunch);

  /* subtract from other beams in advance */  
#ifdef BUMP_AXIS
  y=0.0; yp=0.0;
#else
  y=bunch_get_offset_y(testbunch);
  yp=bunch_get_offset_yp(testbunch);
#endif
  for (i=0;i<ns*(nb-1);i++){
    bump->a11[i]=-(testbunch->particle[i].y-y);
    bump->a12[i]=-(testbunch->particle[i].y-y);
    bump->a21[i]=-(testbunch->particle[i].yp-yp);
    bump->a22[i]=-(testbunch->particle[i].yp-yp);
    eps+=(testbunch->sigma[i].r11*testbunch->sigma[i].r22
	  -testbunch->sigma[i].r12*testbunch->sigma[i].r21)
      *testbunch->particle[i].wgt;
    beta+=testbunch->sigma[i].r11*testbunch->particle[i].wgt;
    alpha-=testbunch->sigma[i].r12*testbunch->particle[i].wgt;
    wsum+=testbunch->particle[i].wgt;
  }
  for (;i<ns*nb;i++){
    bump->a11[i]=-(testbunch->particle[i].y-y);
    bump->a12[i]=-(testbunch->particle[i].y-y);
    bump->a21[i]=-(testbunch->particle[i].yp-yp);
    bump->a22[i]=-(testbunch->particle[i].yp-yp);
    eps+=(testbunch->sigma[i].r11*testbunch->sigma[i].r22
	  -testbunch->sigma[i].r12*testbunch->sigma[i].r21)
      *testbunch->particle[i].wgt*bunch0->last_wgt;
    beta+=testbunch->sigma[i].r11*testbunch->particle[i].wgt
	*testbunch->last_wgt;
    alpha-=testbunch->sigma[i].r12*testbunch->particle[i].wgt
	*testbunch->last_wgt;
    wsum+=testbunch->particle[i].wgt*testbunch->last_wgt;
  }
  eps/=wsum;
  alpha/=wsum;
  beta/=wsum;
  eps=(testbunch->sigma[nc].r11*testbunch->sigma[nc].r22
	  -testbunch->sigma[nc].r12*testbunch->sigma[nc].r21);
  beta=testbunch->sigma[nc].r11;
  alpha=-testbunch->sigma[nc].r12;
  eps=sqrt(eps);
  beta/=eps;
  alpha/=eps;
  bump->alpha=alpha;
  bump->beta=beta;
  bump->gamma=(1.0+alpha*alpha)/beta;

  /* track bunch for shifted first structure */
  bump_set(beamline,bump,0,100.0);
  beam_copy(bunch0,workbunch);
  bump_step_meas(beamline,bump,bin,nbin,workbunch,testbunch);
  
#ifdef BUMP_AXIS
  y=0.0; yp=0.0;
#else
  y=bunch_get_offset_y(testbunch);
  yp=bunch_get_offset_yp(testbunch);
#endif
  for (i=0;i<bunch0->slices;i++){
    bump->a11[i]+=(testbunch->particle[i].y-y);
    bump->a21[i]+=(testbunch->particle[i].yp-yp);
  }
  bump_set(beamline,bump,0,-100.0);

  /* track bunch for shifted second structure */
  bump_set(beamline,bump,1,100.0);
  beam_copy(bunch0,workbunch);
  bump_step_meas(beamline,bump,bin,nbin,workbunch,testbunch);

#ifdef BUMP_AXIS
  y=0.0; yp=0.0;
#else
  y=bunch_get_offset_y(testbunch);
  yp=bunch_get_offset_yp(testbunch);
#endif
  for (i=0;i<bunch0->slices;i++){
    bump->a12[i]+=(testbunch->particle[i].y-y);
    bump->a22[i]+=(testbunch->particle[i].yp-yp);
  }
  bump_set(beamline,bump,1,-100.0);
  for (i=0;i<bunch0->slices;i++){
    bump->a11[i]/=100.0;
    bump->a12[i]/=100.0;
    bump->a21[i]/=100.0;
    bump->a22[i]/=100.0;
  }
  placet_printf(INFO,"%g %g %g %g\n",bump->a11[0],bump->a12[0],bump->a21[0],bump->a22[0]);
}

void bump_define_free(BEAMLINE *beamline,BUMP *bump,int cav)
{
  int i,nquad=0,nbpm=0,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  int nw=7,ns=1;
//  int nw=11,ns=5;

  nw=bump_data.nw;
  ns=bump_data.ns;
  bump->ncav[0]=nw;
  bump->ncav[1]=nw;

  while(!beamline->element[cav]->is_quad()) cav++;
  bump->start=cav;
  for (i=0;i<nw;i++) {
    while(!(beamline->element[cav]->is_bpm())) {
      if (beamline->element[cav]->is_quad()) {
	quad[nquad++]=cav;
      }
      cav++;
    }
    bpm[nbpm++]=cav;
    bump->cav[0][i]=cav++;
  }

  for (i=0;i<ns;i++) {
    while(!(beamline->element[cav]->is_bpm())) {
      if (beamline->element[cav]->is_quad()) {
	quad[nquad++]=cav;
      }
      cav++;
    }
    bpm[nbpm++]=cav++;
  }

  for (i=0;i<nw;i++) {
    while(!(beamline->element[cav]->is_bpm())) {
      if (beamline->element[cav]->is_quad()) {
	quad[nquad++]=cav;
      }
      cav++;
    }
    bpm[nbpm++]=cav;
    bump->cav[1][i]=cav++;
  }

  for (i=0;i<2;i++) {
    while(!(beamline->element[cav]->is_bpm())) {
      if (beamline->element[cav]->is_quad()) {
	quad[nquad++]=cav;
      }
      cav++;
    }
   bpm[nbpm++]=cav;
  }

  bump->feedback=bin_make(nquad,nbpm);
  bin_set_elements(bump->feedback,quad,nquad,bpm,nbpm,nquad);
  bump->stop=bump->feedback->bpm[nbpm-1]+1;
  cav=bump->stop;
  for (i=0;i<bump_data.emitt_dist;i++){
    while(!beamline->element[cav]->is_quad()){
      bump->stop=cav;
      cav++;
    }
    cav++;
  }
  placet_printf(INFO,"bump %d %d %d %d\n",bump->feedback->nbpm,bump->feedback->nq,
	 bump->cav[0][0],bump->cav[1][0]);
//bump->stop=beamline->n_elements;
}

void
bump_define_cav(BEAMLINE *beamline,BUMP *bump,int cav)
{
  int i,nquad,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM],qd=0;
  bump->ncav[0]=bump_data.ncav;
  bump->ncav[1]=bump_data.ncav;
  for (;;){
    while(!beamline->element[cav]->is_quad()) cav++;
    if (beamline->element[cav]->quad_ptr()->get_strength()>0.0) break;
    cav++;
  }
  while(!beamline->element[cav]->is_cavity()) cav++;
  bump->start=cav;
  bump->cav[0][0]=cav++;
  for (i=1;i<bump->ncav[0];i++){
    while (!beamline->element[cav]->is_cavity()) cav++;
    bump->cav[0][i]=cav++;
  }
  for (;;){
    while(!beamline->element[cav]->is_quad()) cav++;
    if (beamline->element[cav]->quad_ptr()->get_strength()>0.0) break;
    cav++;
  }
  while(!beamline->element[cav]->is_cavity()) cav++;
  bump->cav[1][0]=cav++;
  for (i=1;i<bump->ncav[1];i++){
    while (!beamline->element[cav]->is_cavity()) cav++;
    bump->cav[1][i]=cav++;
  }
  for (;;){
    if (qd>=bump_data.quad_dist) break;
    while(!beamline->element[cav]->is_quad()) cav++;
    if (beamline->element[cav]->quad_ptr()->get_strength()>0.0){
      qd++;
    }
    cav++;
  }
  //bin_define_1(beamline,cav,2,bpm,&nbpm,quad,&nquad);
  bin_define_feedback(beamline,cav,2,bpm,&nbpm,quad,&nquad);
  //  bin_define(beamline,cav,2,bpm,&nbpm,quad,&nquad);
  bump->feedback=bin_make(nquad,nbpm);
  bin_set_elements(bump->feedback,quad,nquad,bpm,nbpm,nquad);
  bump->stop=bump->feedback->bpm[nbpm-1]+1;
  cav=bump->stop;
  for (i=0;i<bump_data.emitt_dist;i++){
    while(!beamline->element[cav]->is_quad()){
      bump->stop=cav;
      cav++;
    }
    cav++;
  }
  placet_printf(INFO,"%d %d %d %d\n",bump->feedback->bpm[0],bump->feedback->bpm[1],
	 bump->cav[0][0],bump->cav[1][0]);
//bump->stop=beamline->n_elements;
}

void bump_define_quad(BEAMLINE *beamline,BUMP *bump,int cav)
{
  int nquad,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM],qd=0;
  bump->ncav[0]=1;
  bump->ncav[1]=1;
  for (;;){
    while(!beamline->element[cav]->is_quad()) cav++;
    if (beamline->element[cav]->quad_ptr()->get_strength()>0.0) break;
    cav++;
  }
  bump->start=cav;
  bump->cav[0][0]=cav++;
  for (;;){
    while(!beamline->element[cav]->is_quad()) cav++;
    if (beamline->element[cav]->quad_ptr()->get_strength()>0.0) break;
    cav++;
  }
  bump->cav[1][0]=cav++;
  for (;;){
    if (qd>=bump_data.quad_dist) break;
    while(!beamline->element[cav]->is_quad()) cav++;
    if (beamline->element[cav]->quad_ptr()->get_strength()>0.0){
      qd++;
    }
    cav++;
  }
  bin_define_1(beamline,cav,2,bpm,&nbpm,quad,&nquad);
  bump->feedback=bin_make(nquad,nbpm);
  bin_set_elements(bump->feedback,quad,nquad,bpm,nbpm,nquad);
  bump->stop=bump->feedback->bpm[nbpm-1]+1;
  placet_printf(INFO,"%d %d %d %d\n",bump->feedback->bpm[0],bump->feedback->bpm[1],
	 *(bump->cav[0]),*(bump->cav[1]));
//bump->stop=beamline->n_elements;
}

void bump_define(BEAMLINE *beamline,BUMP *bump,int cav,int flag)
{
  switch(flag){
  case 1:
  case 4:
    bump_define_cav(beamline,bump,cav);
    break;
  case 2:
    bump_define_quad(beamline,bump,cav);
    break;
  case 3:
    bump_define_free(beamline,bump,cav);
    break;
  }
  bump->type=flag;
}
