#ifdef _OPENMP
#include <omp.h>
#endif
#ifdef SLICED_BEAM
void ELEMENT::step_4d_tl(BEAM *beam )
#else
  void ELEMENT::step_4d_tl_0(BEAM *beam )
#endif
{
  const int N = flags.thin_lens;
  double length_n = geometry.length/N;
  init_kick();
  KICK (ELEMENT::*get_kick_ptr)(const PARTICLE &) const = &ELEMENT::get_kick;
#pragma omp parallel for  
  for (int i=0; i<beam->slices; i++) {
    PARTICLE &particle = beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      KICK kick = (this->*get_kick_ptr)(particle);
      for (int j=0; j<N; j++) {
	double xp_ = particle.xp + kick.xp * 0.5;
	double yp_ = particle.yp + kick.yp * 0.5;
	double length_n_ = length_n * (1.0 + kick.h * particle.x / 1e6); // m
	particle.x += xp_ * length_n_;
	particle.y += yp_ * length_n_;
	kick = (this->*get_kick_ptr)(particle);
	particle.xp = xp_ + kick.xp * 0.5;
	particle.yp = yp_ + kick.yp * 0.5;
      }
#ifdef SLICED_BEAM
      {
	const Matrix<6,6> R = get_transfer_matrix_6d(particle.energy);
	R_MATRIX rxx;
	rxx.r11 = R[0][0];
	rxx.r12 = R[0][1];
	rxx.r21 = R[1][0];
	rxx.r22 = R[1][1];
	R_MATRIX ryy;
	ryy.r11 = R[2][2];
	ryy.r12 = R[2][3];
	ryy.r21 = R[3][2];
	ryy.r22 = R[3][3];
	R_MATRIX &sigma_xx = beam->sigma_xx[i];
	R_MATRIX &sigma_xy = beam->sigma_xy[i];
	R_MATRIX &sigma_yy = beam->sigma[i];
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
#endif
    }
  }
  finalize_kick();
}

#ifdef SLICED_BEAM
void ELEMENT::step_4d_tl_sr(BEAM *beam )
#else
  void ELEMENT::step_4d_tl_sr_0(BEAM *beam )
#endif
{
  if (geometry.length==0.0) 
    return step_4d_tl_0(beam);
  const int N = flags.thin_lens;
  double length_n = geometry.length/N;
  init_kick();
  KICK (ELEMENT::*get_kick_ptr)(const PARTICLE &) const = &ELEMENT::get_kick;
  if (beam->slices==1) {
    PARTICLE &particle = beam->particle[0];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
#ifdef SLICED_BEAM
      double init_energy = particle.energy;
#endif
      KICK kick = (this->*get_kick_ptr)(particle);
      for (int j=0; j<N; j++) {
	if ((kick.xp*kick.xp+kick.yp*kick.yp)>10000.) {
	  aperture.weight += fabs(particle.wgt);
	  particle.energy = 0.;
	  particle.wgt = 0.;
	  break;
	}
	double xp_ = particle.xp + kick.xp * 0.5;
	double yp_ = particle.yp + kick.yp * 0.5;
	double length_n_ = length_n * (1.0 + kick.h * particle.x / 1e6); // m
	particle.x += xp_ * length_n_;
	particle.y += yp_ * length_n_;
	kick = (this->*get_kick_ptr)(particle);
	particle.xp = xp_ + kick.xp * 0.5;
	particle.yp = yp_ + kick.yp * 0.5;
	particle.energy -= kick.get_synrad_average_energy_loss(length_n_, particle.energy);
	if (fabs(particle.energy) < std::numeric_limits<double>::epsilon()) {
	  aperture.weight += fabs(particle.wgt);
	  particle.energy = 0.;
	  particle.wgt = 0.;
	  break;
	}
      }
#ifdef SLICED_BEAM
      {
	const Matrix<6,6> R = get_transfer_matrix_6d((init_energy+particle.energy)/2.);
	R_MATRIX rxx;
	rxx.r11 = R[0][0];
	rxx.r12 = R[0][1];
	rxx.r21 = R[1][0];
	rxx.r22 = R[1][1];
	R_MATRIX ryy;
	ryy.r11 = R[2][2];
	ryy.r12 = R[2][3];
	ryy.r21 = R[3][2];
	ryy.r22 = R[3][3];
	R_MATRIX &sigma_xx = beam->sigma_xx[0];
	R_MATRIX &sigma_xy = beam->sigma_xy[0];
	R_MATRIX &sigma_yy = beam->sigma[0];
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
#endif
    }
  } else {
#pragma omp parallel for  
    for (int i=0; i<beam->slices; i++) {
      PARTICLE &particle = beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
#ifdef SLICED_BEAM
	double init_energy = particle.energy;
#endif
	KICK kick = (this->*get_kick_ptr)(particle);
	double freepath = 0.0;
	for (int j=0; j<N; j++) {
	  if ((kick.xp*kick.xp+kick.yp*kick.yp)>10000.) {
	    aperture.weight += fabs(particle.wgt);
	    particle.energy = 0.;
	    particle.wgt = 0.;
	    break;
	  }
	  double xp_ = particle.xp + kick.xp * 0.5;
	  double yp_ = particle.yp + kick.yp * 0.5;
	  double length_n_ = length_n * (1.0 + kick.h * particle.x / 1e6); // m
	  particle.x += xp_ * length_n_;
	  particle.y += yp_ * length_n_;
	  kick = (this->*get_kick_ptr)(particle);
	  particle.xp = xp_ + kick.xp * 0.5;
	  particle.yp = yp_ + kick.yp * 0.5;
	  if (j==0)
	    freepath = kick.get_synrad_free_path(length_n_, particle.energy);
	  while (freepath < length_n_) {
	    particle.energy -= kick.get_synrad_energy_loss(length_n_, particle.energy);
	    if (fabs(particle.energy) < std::numeric_limits<double>::epsilon()) {
   	      aperture.weight += fabs(particle.wgt);
	      particle.energy = 0.;
	      particle.wgt = 0.;
	      goto end;
	    }
	    freepath += (this->*get_kick_ptr)(particle).get_synrad_free_path(length_n_, particle.energy);
	  }
	  freepath -= length_n_;
	}
#ifdef SLICED_BEAM
	{
	  const Matrix<6,6> R = get_transfer_matrix_6d((init_energy+particle.energy)/2.);
	  R_MATRIX rxx;
	  rxx.r11 = R[0][0];
	  rxx.r12 = R[0][1];
	  rxx.r21 = R[1][0];
	  rxx.r22 = R[1][1];
	  R_MATRIX ryy;
	  ryy.r11 = R[2][2];
	  ryy.r12 = R[2][3];
	  ryy.r21 = R[3][2];
	  ryy.r22 = R[3][3];
	  R_MATRIX &sigma_xx = beam->sigma_xx[i];
	  R_MATRIX &sigma_xy = beam->sigma_xy[i];
	  R_MATRIX &sigma_yy = beam->sigma[i];
	  sigma_xx = rxx * sigma_xx * transpose(rxx);
	  sigma_xy = ryy * sigma_xy * transpose(rxx);
	  sigma_yy = ryy * sigma_yy * transpose(ryy);
	}
#endif
      end:;
      }
    }   
  }
  finalize_kick();
}

#ifdef SLICED_BEAM
void ELEMENT::step_6d_tl(BEAM *beam )
#else
  void ELEMENT::step_6d_tl_0(BEAM *beam )
#endif
{
  const int N = flags.thin_lens;
  double length_n = geometry.length/N;
  init_kick();
  KICK (ELEMENT::*get_kick_ptr)(const PARTICLE &) const = &ELEMENT::get_kick;
#pragma omp parallel for  
  for (int i=0; i<beam->slices; i++) {
    PARTICLE &particle = beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      KICK kick = (this->*get_kick_ptr)(particle);
      for (int j=0; j<N; j++) {
	double xp_ = particle.xp + kick.xp * 0.5;
	double yp_ = particle.yp + kick.yp * 0.5;
	double length_n_ = length_n * (1.0 + kick.h * particle.x / 1e6); // m
	double vz_i = hypot(1.0, xp_/1e6, yp_/1e6); // rad
	particle.x += xp_ * length_n_;
	particle.y += yp_ * length_n_;
	particle.z += length_n * ((1e6 + kick.h * particle.x) * vz_i - 1e6);
	kick = (this->*get_kick_ptr)(particle);
	particle.xp = xp_ + kick.xp * 0.5;
	particle.yp = yp_ + kick.yp * 0.5;
      }
#ifdef SLICED_BEAM
      {
	const Matrix<6,6> R = get_transfer_matrix_6d(particle.energy);
	R_MATRIX rxx;
	rxx.r11 = R[0][0];
	rxx.r12 = R[0][1];
	rxx.r21 = R[1][0];
	rxx.r22 = R[1][1];
	R_MATRIX ryy;
	ryy.r11 = R[2][2];
	ryy.r12 = R[2][3];
	ryy.r21 = R[3][2];
	ryy.r22 = R[3][3];
	R_MATRIX &sigma_xx = beam->sigma_xx[i];
	R_MATRIX &sigma_xy = beam->sigma_xy[i];
	R_MATRIX &sigma_yy = beam->sigma[i];
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
#endif
    }
  }
  finalize_kick();
}

#ifdef SLICED_BEAM
void ELEMENT::step_6d_tl_sr(BEAM *beam )
#else
  void ELEMENT::step_6d_tl_sr_0(BEAM *beam )
#endif
{
  if (geometry.length==0.0) 
    return step_6d_tl_0(beam);
  const int N = flags.thin_lens;
  double length_n = geometry.length/N;
  init_kick();
  KICK (ELEMENT::*get_kick_ptr)(const PARTICLE &) const = &ELEMENT::get_kick;
  if (beam->slices==1) {
    PARTICLE &particle = beam->particle[0];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
#ifdef SLICED_BEAM
      double init_energy = particle.energy;
#endif
      KICK kick = (this->*get_kick_ptr)(particle);
      for (int j=0; j<N; j++) {
	if ((kick.xp*kick.xp+kick.yp*kick.yp)>10000.) {
	  aperture.weight += fabs(particle.wgt);
	  particle.energy = 0.;
	  particle.wgt = 0.;
	  break;
	}
	double xp_ = particle.xp + kick.xp * 0.5;
	double yp_ = particle.yp + kick.yp * 0.5;
	double length_n_ = length_n * (1.0 + kick.h * particle.x / 1e6); // m
	double vz_i = hypot(1.0, xp_/1e6, yp_/1e6); // rad
	particle.x += xp_ * length_n_;
	particle.y += yp_ * length_n_;
	particle.z += length_n * ((1e6 + kick.h * particle.x) * vz_i - 1e6);
	kick = (this->*get_kick_ptr)(particle);
	particle.xp = xp_ + kick.xp * 0.5;
	particle.yp = yp_ + kick.yp * 0.5;
	particle.energy -= kick.get_synrad_average_energy_loss(length_n_, particle.energy);
	if (fabs(particle.energy) < std::numeric_limits<double>::epsilon()) {
	  aperture.weight += fabs(particle.wgt);
	  particle.energy = 0.;
	  particle.wgt = 0.;
	  break;
	}	
      }
#ifdef SLICED_BEAM
      {
	const Matrix<6,6> R = get_transfer_matrix_6d((init_energy+particle.energy)/2.);
	R_MATRIX rxx;
	rxx.r11 = R[0][0];
	rxx.r12 = R[0][1];
	rxx.r21 = R[1][0];
	rxx.r22 = R[1][1];
	R_MATRIX ryy;
	ryy.r11 = R[2][2];
	ryy.r12 = R[2][3];
	ryy.r21 = R[3][2];
	ryy.r22 = R[3][3];
	R_MATRIX &sigma_xx = beam->sigma_xx[0];
	R_MATRIX &sigma_xy = beam->sigma_xy[0];
	R_MATRIX &sigma_yy = beam->sigma[0];
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
#endif
    }
  } else {
#pragma omp parallel for  
    for (int i=0; i<beam->slices; i++) {
      PARTICLE &particle = beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
#ifdef SLICED_BEAM
	double init_energy = particle.energy;
#endif
	double freepath = 0.0;
	KICK kick = (this->*get_kick_ptr)(particle);
	for (int j=0; j<N; j++) {
	  if ((kick.xp*kick.xp+kick.yp*kick.yp)>10000.) {
	    aperture.weight += fabs(particle.wgt);
	    particle.energy = 0.;
	    particle.wgt = 0.;
	    break;
	  }
	  double xp_ = particle.xp + kick.xp * 0.5;
	  double yp_ = particle.yp + kick.yp * 0.5;
	  double length_n_ = length_n * (1.0 + kick.h * particle.x / 1e6); // m
	  double vz_i = hypot(1.0, xp_/1e6, yp_/1e6); // rad
	  particle.x += xp_ * length_n_;
	  particle.y += yp_ * length_n_;
	  particle.z += length_n * ((1e6 + kick.h * particle.x) * vz_i - 1e6);
	  kick = (this->*get_kick_ptr)(particle);
	  particle.xp = xp_ + kick.xp * 0.5;
	  particle.yp = yp_ + kick.yp * 0.5;
	  if (j==0)
	    freepath = kick.get_synrad_free_path(length_n_, particle.energy);
	  while (freepath < length_n_) {
	    particle.energy -= kick.get_synrad_energy_loss(length_n_, particle.energy);
	    if (fabs(particle.energy) < std::numeric_limits<double>::epsilon()) {
	      aperture.weight += fabs(particle.wgt);
	      particle.energy = 0.;
	      particle.wgt = 0.;
	      goto end;
	    }
	    freepath += (this->*get_kick_ptr)(particle).get_synrad_free_path(length_n_, particle.energy);
	  }
	  freepath -= length_n_;
	}
#ifdef SLICED_BEAM
	{
	  const Matrix<6,6> R = get_transfer_matrix_6d((init_energy+particle.energy)/2.);
	  R_MATRIX rxx;
	  rxx.r11 = R[0][0];
	  rxx.r12 = R[0][1];
	  rxx.r21 = R[1][0];
	  rxx.r22 = R[1][1];
	  R_MATRIX ryy;
	  ryy.r11 = R[2][2];
	  ryy.r12 = R[2][3];
	  ryy.r21 = R[3][2];
	  ryy.r22 = R[3][3];
	  R_MATRIX &sigma_xx = beam->sigma_xx[i];
	  R_MATRIX &sigma_xy = beam->sigma_xy[i];
	  R_MATRIX &sigma_yy = beam->sigma[i];
	  sigma_xx = rxx * sigma_xx * transpose(rxx);
	  sigma_xy = ryy * sigma_xy * transpose(rxx);
	  sigma_yy = ryy * sigma_yy * transpose(ryy);
	}
#endif
      end:;
      }
    }   
  }
  finalize_kick();
}
