#include <algorithm>
#include <cmath>
#include <cstdio>

#include "track.h"

#include "structures_def.h"
#include "beam.h"
#include "beamline.h"
#include "placeti3.h"
#include "quadrupole.h"
#include "multipole.h"
#include "cavity.h"
#include "dipole.h"
#include "sbend.h"
#include "drift.h"
#include "bpm.h"

#include "parallel.h"
#include "file_stream.hh"

#include "parallel_tracking.hh"

PLACET_SWITCH_STRUCT placet_switch;

static struct{
  int iq;
} bunch_track_emitt_data;

void
bunch_track_0(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  ELEMENT **element=beamline->element;
  inter_data.bunch=beam;
  for (;start<stop;start++){
    element[start]->track_0(beam);
  }
}

void
bunch_track_0_noacc(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  ELEMENT **element=beamline->element;
  inter_data.bunch=beam;
  for (;start<stop;start++){
    if (CAVITY *cavity=dynamic_cast<CAVITY*>(element[start])) {
      double tmp=cavity->get_gradient();
      cavity->set_gradient(0.0);
      cavity->track_0(beam);
      cavity->set_gradient(tmp); 
    } else {
      element[start]->track_0(beam);
    }
  }
}

void bunch_track(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  ELEMENT **element=beamline->element;
  inter_data.bunch=beam;
  for (;start<stop;start++){
    element[start]->track(beam);
  }
}

void
bunch_track_gradient_0(BEAMLINE *beamline,BEAM *beam,int start,int stop,
		       double gradient)
{
  inter_data.bunch=beam;
  for (;start<stop;start++){
    ELEMENT *element=beamline->element[start];
    if (CAVITY *cavity=dynamic_cast<CAVITY*>(element)) {
      double tmp=cavity->get_gradient();
      cavity->set_gradient(gradient*tmp);
      cavity->track_0(beam);
      cavity->set_gradient(tmp); 
    } else {
      element->track_0(beam);
    }
  }
}

void bunch_track_emitt_start()
{
  bunch_track_emitt_data.iq=0;
}

void bunch_track_emitt_end(BEAM *bunch, double s)
{
  emitt_data.store(bunch_track_emitt_data.iq,bunch,s);
  emitt_data.append(bunch);
}

void bunch_track_emitt(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  ELEMENT **element=beamline->element;
  inter_data.bunch=beam;

  for (;start<stop;start++) {
    //      element[start][start]->step(beam);
    if (element[start]->is_quad()) {
      emitt_data.store_el(bunch_track_emitt_data.iq,beam,element[start]);
      bunch_track_emitt_data.iq++;
    }
    
    if (!beam->is_lost)
      element[start]->track(beam);
    
    if (element[start]->callback) {
      (element[start]->callback->funct)(beam,start,
					element[start]->callback->interp,
					element[start]->callback->script);
    }
  }
}

void bunch_track_test(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  ELEMENT **element;
  inter_data.bunch=beam;
  element=beamline->element;
  for (;start<stop;start++){
    element[start]->track_0(beam);
  }
}

void bunch_track_line_emitt_new(BEAMLINE *beamline,BEAM *beam, int start, int end )
{
  if (start<0)
    start=0;
  if (end<0)
    end=beamline->n_elements;
  if (start>end)
    start=end;
  bunch_track_emitt_start();
  bunch_track_emitt(beamline,beam,start,end);
  double end_s = end == beamline->n_elements ? beamline->get_length() : beamline->element[end-1]->get_s();
  bunch_track_emitt_end(beam,end_s);
}

void bunch_track_line_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    /*    emitt_data.y_off=beamline->element[bin[i]->start]->offset.y;*/
    emitt_data.y_off=0.0;
    emitt_data.store(i,bunch,beamline->get_length());
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
  emitt_data.y_off=beamline->element[beamline->n_elements-1]->offset.y;
  emitt_data.y_off=0.0;
  emitt_data.store(i,bunch,beamline->get_length());
}

void bunch_track_emitt_parallel(ParallelTracking &parallel_tracking, BEAMLINE *beamline,BEAM *beam,
				int start,int stop)
{
  inter_data.bunch=beam;
  for (; start<stop; start++) {
    ELEMENT &element=*beamline->element[start];
    if (!parallel_tracking.is_tracking()) {
      if (dynamic_cast<BEGIN_PARALLEL*>(&element)) {
        parallel_tracking.startTracking(*beam);
      } else {
        element.track(beam);
      	if (dynamic_cast<QUADRUPOLE*>(&element)) {
  	  emitt_data.store_el(bunch_track_emitt_data.iq,beam,&element);
  	  bunch_track_emitt_data.iq++;
        }
	if (element.callback) element.callback->funct(beam, start, element.callback->interp, element.callback->script);
      }
    } else {
      if (QUADRUPOLE *q_ptr = dynamic_cast<QUADRUPOLE*>(&element)) {
	parallel_tracking << (*q_ptr);
	bunch_track_emitt_data.iq++;
      }
      else if (MULTIPOLE  *m_ptr = dynamic_cast<MULTIPOLE*>(&element))    parallel_tracking << (*m_ptr);
      else if (DRIFT      *d_ptr = dynamic_cast<DRIFT*>(&element))        parallel_tracking << (*d_ptr);
      else if (BPM        *b_ptr = dynamic_cast<BPM*>(&element))          parallel_tracking << (*b_ptr);
      else if (DIPOLE     *d_ptr = dynamic_cast<DIPOLE*>(&element))       parallel_tracking << (*d_ptr);
      else if (SBEND      *s_ptr = dynamic_cast<SBEND*>(&element))        parallel_tracking << (*s_ptr);
      else if (dynamic_cast<END_PARALLEL*>(&element))                     parallel_tracking.endTracking(*beam);
      else {
	placet_cout << ERROR << "Elements of type '" << typeid(element).name() << "' cannot be tracked with MPI. Sorry." << endmsg;
	exit(1);
      }
    }
  }
}
