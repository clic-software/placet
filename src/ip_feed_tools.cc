#include <cctype>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <utility>

#include "lumi.h"
#include "random.hh"

#include "placet.h"

std::pair<double,double> IP_corrections_0(char *b_1)
{
  int i,n,n1=0;
  double *x1,*xp1,*y1,*yp1,*w1,*z;
  double *rx11_1,*rx22_1,*ry11_1,*ry22_1;
  double wsum1=0.0;
  FILE *f1;
  char *point,buffer[1000];
  char ch;

  double ym_ipcor = 0.;
  double ypm_ipcor = 0.;

  f1=read_file(b_1);

  while(!feof(f1)){ 
    fgets(buffer,1000,f1);
    sscanf(buffer,"%c",&ch);
    if(isxdigit(ch)||ch=='-') n1 += 1;
  }
  rewind(f1);

  /// TEST the file ! Condition on n1 ....

  ///

  n=n1;
  z=(double*)malloc(sizeof(double)*n);
  w1=(double*)malloc(sizeof(double)*n);
  x1=(double*)malloc(sizeof(double)*n);
  xp1=(double*)malloc(sizeof(double)*n);
  y1=(double*)malloc(sizeof(double)*n);
  yp1=(double*)malloc(sizeof(double)*n);

  rx11_1=(double*)malloc(sizeof(double)*n);
  rx22_1=(double*)malloc(sizeof(double)*n);
  ry11_1=(double*)malloc(sizeof(double)*n);
  ry22_1=(double*)malloc(sizeof(double)*n);


  for(i=0;i<n;i++){

    point=fgets(buffer,1000,f1);
    z[i]=strtod(point,&point);
    w1[i]=strtod(point,&point);
    strtod(point,&point);
    x1[i]=strtod(point,&point);
    xp1[i]=strtod(point,&point);
    y1[i]=strtod(point,&point);
    yp1[i]=strtod(point,&point);

    rx11_1[i]=strtod(point,&point);
    strtod(point,&point);
    rx22_1[i]=strtod(point,&point);
    ry11_1[i]=strtod(point,&point);
    strtod(point,&point);
    ry22_1[i]=strtod(point,&point);

    y1[i] /= sqrt(ry11_1[i]);
    yp1[i] /= sqrt(ry22_1[i]);

    x1[i] /= sqrt(rx11_1[i]);
    xp1[i] /= sqrt(rx22_1[i]);

    /// Compute the averaged values
    
    ym_ipcor += w1[i]*y1[i];
    ypm_ipcor += w1[i]*yp1[i];
    wsum1 += w1[i];
  }

  close_file(f1);

  //placet_printf(INFO,"\n Beam : F1 is %g",y1[0]);  

  /// Compute the luminosity when the beam is brought back to the beamline

  ym_ipcor /= wsum1;
  ypm_ipcor /= wsum1;

  free(z);
  free(w1);
  free(x1);
  free(xp1);
  free(y1);
  free(yp1);
  free(rx11_1);
  free(rx22_1);
  free(ry11_1);
  free(ry22_1);

  return std::make_pair(ym_ipcor,ypm_ipcor);
}

///

double IP_collision_0(char *b_1,char *b_2,double ym1,double ypm1,double ym2,
       double ypm2,double gain_pos,double gain_ang,double beam_pos_res)
{
  int i,n,n1=0,n2=0;
  double *x1,*xp1,*y1,*yp1,*w1,*z;
  double *x2,*xp2,*y2,*yp2,*w2;
  double mry11_1=0.0,mry22_1=0.0,mry11_2=0.0,mry22_2=0.0;
  double wsum_1=0.0,wsum_2=0.0;
  double *rx11_1,*rx22_1,*ry11_1,*ry22_1;
  double *rx11_2,*rx22_2,*ry11_2,*ry22_2;
  double beta_y=150.0,l,l0,delta_y;
  FILE *f1,*f2;
  char *point,buffer[1000];
  char ch;

  f1=read_file(b_1);
  f2=read_file(b_2);

  /// Compare the 2 beam files

  while(!feof(f1)){ 
    fgets(buffer,1000,f1);
    sscanf(buffer,"%c",&ch);
    if(isxdigit(ch)||ch=='-') n1 += 1;
  }
  rewind(f1);

 while(!feof(f2)){ 
    fgets(buffer,1000,f2);
    sscanf(buffer,"%c",&ch);
    if(isxdigit(ch)||ch=='-') n2 += 1;
  }  
  rewind(f2);

  if(n1!=n2){
    placet_printf(INFO,"files are not compatible\n");
    exit(1);
  }

  /// Read the beams

  n=n1;
  z=(double*)malloc(sizeof(double)*n);
  w1=(double*)malloc(sizeof(double)*n);
  x1=(double*)malloc(sizeof(double)*n);
  xp1=(double*)malloc(sizeof(double)*n);
  y1=(double*)malloc(sizeof(double)*n);
  yp1=(double*)malloc(sizeof(double)*n);

  rx11_1=(double*)malloc(sizeof(double)*n);
  rx22_1=(double*)malloc(sizeof(double)*n);
  ry11_1=(double*)malloc(sizeof(double)*n);
  ry22_1=(double*)malloc(sizeof(double)*n);

  
  w2=(double*)malloc(sizeof(double)*n);
  x2=(double*)malloc(sizeof(double)*n);
  xp2=(double*)malloc(sizeof(double)*n);
  y2=(double*)malloc(sizeof(double)*n);
  yp2=(double*)malloc(sizeof(double)*n);

  rx11_2=(double*)malloc(sizeof(double)*n);
  rx22_2=(double*)malloc(sizeof(double)*n);
  ry11_2=(double*)malloc(sizeof(double)*n);
  ry22_2=(double*)malloc(sizeof(double)*n);


  for(i=0;i<n;i++){

    point=fgets(buffer,1000,f1);
    z[i]=strtod(point,&point);
    w1[i]=strtod(point,&point);
    strtod(point,&point);
    x1[i]=strtod(point,&point);
    xp1[i]=strtod(point,&point);
    y1[i]=strtod(point,&point);
    yp1[i]=strtod(point,&point);

    rx11_1[i]=strtod(point,&point);
    strtod(point,&point);
    rx22_1[i]=strtod(point,&point);
    ry11_1[i]=strtod(point,&point);
    strtod(point,&point);
    ry22_1[i]=strtod(point,&point);


    point=fgets(buffer,1000,f2);
    z[i]=strtod(point,&point);
    w2[i]=strtod(point,&point);
    strtod(point,&point);
    x2[i]=strtod(point,&point);
    xp2[i]=strtod(point,&point);
    y2[i]=strtod(point,&point);
    yp2[i]=strtod(point,&point);

    rx11_2[i]=strtod(point,&point);
    strtod(point,&point);
    rx22_2[i]=strtod(point,&point);
    ry11_2[i]=strtod(point,&point);
    strtod(point,&point);
    ry22_2[i]=strtod(point,&point);


    wsum_1 += w1[i];
    wsum_2 += w1[i];
    mry11_1 += w1[i]*sqrt(ry11_1[i]);
    mry22_1 +=  w1[i]*sqrt(ry22_1[i]);    
    mry11_2 += w1[i]*sqrt(ry11_2[i]);
    mry22_2 +=  w1[i]*sqrt(ry22_2[i]);    


    //y1[i] /= sqrt(ry11_1[i]);
    //yp1[i] /= sqrt(ry22_1[i]);
    //y2[i] /= sqrt(ry11_2[i]);
    //yp2[i] /= sqrt(ry22_2[i]);
    x1[i] /= sqrt(rx11_1[i]);
    xp1[i] /= sqrt(rx22_1[i]);
    x2[i] /= sqrt(rx11_2[i]);
    xp2[i] /= sqrt(rx22_2[i]);
    
  }

  close_file(f1);
  close_file(f2);

  mry11_1 /= wsum_1;
  mry22_1 /= wsum_1;
  mry11_2 /= wsum_2;
  mry22_2 /= wsum_2;

  for(i=0;i<n;i++){
    y1[i] /= mry11_1;
    yp1[i] /= mry22_1;
    y2[i] /= mry11_2;
    yp2[i] /= mry22_2; 
  } 

  //placet_printf(INFO,"\n Avant : F1 is %g and F2 is %g and the gain is %g",y1[0],y2[0],gain);

  /// Compute the luminosity after the beams are corrected with the gain 

  delta_y = (ym1-ym2)/2 + (beam_pos_res*Instrumentation.Gauss()/sqrt(ry11_1[0]));
  //  delta_yp = (ypm1-ypm2)/2;

  for(i=0;i<n;i++){

    y1[i] -= delta_y*gain_pos;
    y2[i] += delta_y*gain_pos;

    yp1[i] -= ypm1*gain_ang;
    yp2[i] -= ypm2*gain_ang;

  }

  //placet_printf(INFO,"\n Apres : F1 is %g and F2 is %g and the gain is %g \n",y1[0],y2[0],gain);

  l=lumi_calc_0(z,w1,w2,n,y1,yp1,y2,yp2,beta_y);

  for (i=0;i<n;i++){
    y1[i]=0.0;
    yp1[i]=0.0;
    y2[i]=0.0;
    yp2[i]=0.0;
  }

  l0=lumi_calc_0(z,w1,w2,n,y1,yp1,y2,yp2,beta_y);

  free(z);
  free(w1);
  free(x1);
  free(xp1);
  free(y1);
  free(yp1);
  free(rx11_1);
  free(rx22_1);
  free(ry11_1);
  free(ry22_1);
  free(w2);
  free(x2);
  free(xp2);
  free(y2);
  free(yp2);  
  free(rx11_2);
  free(rx22_2);
  free(ry11_2);
  free(ry22_2);

  return l/l0;

  //  exit(0);
}










