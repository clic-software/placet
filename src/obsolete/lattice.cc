int tk_Aperture(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{ 
  int error;
  double ax=0.0,ay=0.0;
  char* t=NULL;
  const char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-ax",TK_ARGV_FLOAT,(char*)NULL,(char*)&ax,
     (char*)"horizontal aperture [micro m]"},
    {(char*)"-ay",TK_ARGV_FLOAT,(char*)NULL,(char*)&ay,
     (char*)"vertical aperture [micro m]"},
    {(char*)"-shape",TK_ARGV_STRING,(char*)NULL,(char*)&t,
     (char*)"shape, options are oval, rectangular"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Aperture>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  // all strengths outside are for x-plane
  // all inside are for y-plane

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) return TCL_ERROR;

  ELEMENT *element = new APERTURE(t,ax, ay);
  element->set_name(element_name);
  inter_data.girder->add_element(element);
			 
  return TCL_OK;
}

/**********************************************************************/
/*                      BeamlineStore                                 */
/**********************************************************************/

int tk_BeamlineStore(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if (inter_data.beamline_set!=1){
    Tcl_AppendResult(interp,"Error in BeamlineStore:\n",NULL);
    Tcl_AppendResult(interp,"Beamline is not defined\n",NULL);
    return TCL_ERROR;
  }
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamlineStore>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  return TCL_OK;
}

/**********************************************************************/
/*                      ElementAddCall                                */
/**********************************************************************/

int tk_ElementAddCall(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  int w=0;
  char *s=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-where",TK_ARGV_INT,(char*)NULL,(char*)&w,
     (char*)"Element at which to add the Call"},
    {(char*)"-script",TK_ARGV_STRING,(char*)NULL,(char*)&s,
     (char*)"Script to be used for the Call"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ElementAddCall>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  // inter_data.beamline->element[j]->callback=&tclcallback;

  return TCL_OK;
}

int tk_Multipole(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error,i;
  double length=0.0,strength=0.0,tilt=0.0;
  int type=0,synrad=0,nstep=5;
//  MULTIPOLE *element;
  char *capert=NULL;
  double apx=0.; double apy=0.;
  char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-strength",TK_ARGV_FLOAT,(char*)NULL,(char*)&strength,
     (char*)"strength of the multipole"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&type,
     (char*)"type of the multipole"},
    {(char*)"-synrad",TK_ARGV_INT,(char*)NULL,(char*)&synrad,
     (char*)"if not zero include synchrotron radiation"},
    {(char*)"-steps",TK_ARGV_INT,(char*)NULL,(char*)&nstep,
     (char*)"Number of steps for tracking"},
    {(char*)"-tilt",TK_ARGV_FLOAT,(char*)NULL,(char*)&tilt,
     (char*)"tilt of the multipole"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"length of the multipole"},
    {(char*)"-aperture",TK_ARGV_STRING,(char*)NULL,(char*)&capert,
     (char*)"aperture of the multipole"},
    {(char*)"-apx",TK_ARGV_FLOAT,(char*)NULL,(char*)&apx,
     (char*)"x_aperture for multipole"},
    {(char*)"-apy",TK_ARGV_FLOAT,(char*)NULL,(char*)&apy,
     (char*)"y_aperture for multipole"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Multipole>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  // all strengths outside are for x-plane
  // all inside are for y-plane

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) 	return TCL_ERROR;

  for (i=2;i<type;i++){
      strength/=i;
  }
  element=new MULTIPOLE(type,strength*pow(1e-6,type-2));
  element->set_name(element_name); 
  element->set_tilt(tilt);
  element->set_length(length);
  if (synrad) element->set_synrad();
  element->set_nstep(nstep);
  if(capert!=NULL && apx!=0. && apy!=0.) {
    element->set_aperture(capert, apx, apy);
  }
  inter_data.girder->add_element(element);
  return TCL_OK;
}

int tk_QuadBpm(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	       char *argv[])
{
  int error;
  double length=0.0,strength=0.0;
  int synrad=0;
  char *element_name="";
  Tk_ArgvInfo table[]={
  {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
  (char*)"Name to be assigned to the element"},
  {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
  (char*)"length of the quadrupole in meter"},
  {(char*)"-strength",TK_ARGV_FLOAT,(char*)NULL,(char*)&strength,
  (char*)"strength of the quadrupole"},
  {(char*)"-synrad",TK_ARGV_INT,(char*)NULL,(char*)&synrad,
  (char*)"if not zero simulate synchrotron radiation in quadrupole"},
  {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
  return error;
  }
  if (argc!=1){
  Tcl_SetResult(interp,"Too many arguments to <QUADBPM>",TCL_VOLATILE);
  return TCL_ERROR;
  }
  
  / * all strengths outside are for x-plane
  all inside are for y-plane * /
  
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) 	return TCL_ERROR;
  
  QUADBPM *quad=new QUADBPM(length,-strength);
  quad->set_name(element_name);
  //  quad->set_synrad(synrad);
  inter_data.girder->add_element(quad);
			 
  return TCL_OK;
}

/**********************************************************************/
/*                      QuadCav                                       */
/**********************************************************************/

int tk_QuadCav(ClientData clientdata,Tcl_Interp *interp,int argc,
	      char *argv[])
{
  int error;
  double length=0.0,strength=0.0,gradient=1.0,phase=0.0;
  int type=0;
  char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"length of the quadrupole in meter"},
    {(char*)"-gradient",TK_ARGV_FLOAT,(char*)NULL,(char*)&gradient,
     (char*)"gradient of the structure [normalised]"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,(char*)&phase,
     (char*)"relative phase of the structure [degree]"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&type,
     (char*)"type of the cavity"},
    {(char*)"-k",TK_ARGV_FLOAT,(char*)NULL,(char*)&strength,
     (char*)"strength of the quadrupole"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <QuadCav>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  / * all strengths outside are for x-plane
     all inside are for y-plane * /

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) 	return TCL_ERROR;

  ELEMENT *element;
  inter_data.girder->add_element(
			 element=quadcav_make(length,gradient,strength,phase));
  element->set_name(element_name);
			 
  return TCL_OK;
}

/**********************************************************************/
/*                      SolCav                                        */
/**********************************************************************/

int tk_SolCav(ClientData clientdata,Tcl_Interp *interp,int argc,
	      char *argv[])
{
  int error;
  double length=0.0,strength=0.0,gradient=1.0,phase=0.0;
  int type=0;
  char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"length of the quadrupole in meter"},
    {(char*)"-gradient",TK_ARGV_FLOAT,(char*)NULL,(char*)&gradient,
     (char*)"relative gradient of the structure [normalised]"},
    {(char*)"-phase",TK_ARGV_FLOAT,(char*)NULL,(char*)&phase,
     (char*)"relative phase of the structure [degree]"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&type,
     (char*)"type of the cavity"},
    {(char*)"-bz",TK_ARGV_FLOAT,(char*)NULL,(char*)&strength,
     (char*)"strength of the solenoid [Tesla]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <SolCav>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  / * all strengths outside are for x-plane
      all inside are for y-plane * /

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) 	return TCL_ERROR;

  ELEMENT *element;
  inter_data.girder->add_element(
			 element=solcav_make(length,gradient,strength,phase));
  element->set_name(element_name);
			 
  return TCL_OK;
}
