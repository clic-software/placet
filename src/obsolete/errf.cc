/* normal distribution */

double gauss(double x)
{
  return exp(-0.5*x*x)/sqrt(2.0*PI);
}
