#include <stdlib.h>
#include <stdio.h>
#include <math.h>

main()
{
    int i,n=0,nall=0;
    double x,y,xp,yp,e,z;
    double sx=0.0,sy=0.0,sxp=0.0,syp=0.0,sz=0.0,se=0.0;
    double sx2=0.0,sy2=0.0,sxp2=0.0,syp2=0.0,sz2=0.0,se2=0.0;
    double sxxp=0.0,syyp=0.0,sxy=0.0,sxpyp=0.0,eps_x,eps_y;
    double rms_x,rms_y,rms_xp,rms_yp,rms_xxp,rms_yyp;
    double zmean=0.0;
    FILE *f;
    char *point,buffer[1000];
    f=fopen("particles.out","r");
    while(point=fgets(buffer,1000,f)) {
	e=strtod(point,&point);
	x=strtod(point,&point);
	y=strtod(point,&point);
	z=strtod(point,&point);
	xp=strtod(point,&point);
	yp=strtod(point,&point);
	if (fabs(y)<0.015&&fabs(x)<3.0) {
	    zmean+=z;
	    se+=e;
	    sx+=x;
	    sy+=y;
	    sz+=z;
	    sxp+=xp;
	    syp+=yp;
	    se2+=e*e;
	    sx2+=x*x;
	    sy2+=y*y;
	    sz2+=z*z;
	    sxp2+=xp*xp;
	    syp2+=yp*yp;
	    sxxp+=x*xp;
	    syyp+=y*yp;
	    sxy+=x*y;
	    sxpyp+=xp*yp;
	    n++;
	}
	nall++;
    }
    eps_x=sqrt(sx2*sxp2-sxxp*sxxp)/n;
    printf("eps_x=%g\n",eps_x*se/n/0.511e-3*1e-12);
    eps_y=sqrt(sy2*syp2-syyp*syyp)/n;
    printf("eps_y=%g\n",eps_y*se/n/0.511e-3*1e-12);
    printf("e_rms=%g\n",sqrt(se2/n-se*se/(n*n)));
    printf("x_rms=%g\n",sqrt(sx2/n-sx*sx/(n*n)));
    printf("y_rms=%g\n",sqrt(sy2/n-sy*sy/(n*n)));
    printf("z_rms=%g\n",sqrt(sz2/n-sz*sz/(n*n)));
    printf("x_mean=%g\n",sx/n);
    printf("y_mean=%g\n",sy/n);
    printf("z_mean=%g\n",zmean/n);
    printf("xp_rms=%g\n",sqrt(sxp2/n-sxp*sxp/(n*n)));
    printf("yp_rms=%g\n",sqrt(syp2/n-syp*syp/(n*n)));    
    printf("xxp_corr=%g\n",sxxp/(n*sqrt(sxp2/n*sx2/n)));
    printf("yyp_corr=%g\n",syyp/(n*sqrt(syp2/n*sy2/n)));
    printf("xy_corr=%g\n",sxy/(n*sqrt(sx2/n*sy2/n)));
    printf("xpyp_corr=%g\n",sxpyp/(n*sqrt(sxp2/n*syp2/n)));
    printf("e_mean=%g\n",se/n);
    printf("charge_loss=%g\n",(double)(nall-n)/n);
    fclose(f);
}
