#ifndef twiss_h
#define twiss_h

void fprint_twiss(FILE*,BEAM*,int,double);
void twiss_plot_step(BEAMLINE*,BEAM*,char*,double,
		void(*fprint_twiss)(FILE*,BEAM*,int,double));

#endif
