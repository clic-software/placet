#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double
particle_energy(double e0,double l[],double phi0[],double g[],double b[],int n,
		double phi,double de)
{
  int j;
  double c;
  c=cos(phi);
  for(j=0;j<n;j++){
    e0+=l[j]*(g[j]*cos(phi0[j]+phi)+b[j]*c+de);
  }
  return e0;
}

double
particle_track(double e0,double l[],double phi0[],double g,double b,int n,
		double phi,double de)
{
  double *b1,*g1;
  int i;
  g1=(double*)alloca(sizeof(double)*n);
  b1=(double*)alloca(sizeof(double)*n);
  for(i=0;i<n;i++){
    g1[i]=g;
    b1[i]=b;
  }
  return particle_energy(e0,l,phi0,g1,b1,n,phi,de);
}

/*
  plots the energy at the end of the accelerator
 */

void track_beam(double e0,double l[],double phi0[],int n,double g[],double b[],
		int nb,double phi[],double de[],int np)

{
  int i,j;
  for(i=0;i<nb;i++){
    for(j=0;j<np;j++){
      printf("%d %g\n",i*np+j,particle_track(e0,l,phi0,g[i],b[i],n,phi[j],de[j]));
    }
  }
}

/*
  plots the energy envelope of the beam along the accelerator
 */

void envelope_beam(double e0,double l[],double phi0[],int n,double g[],
		   double b[],int nb,double phi[],double de[],int np)

{
  int i,j;
  double emin0=1e300,emax0=-1e300,e,ls=0;
  double emin,emax;

  for(i=0;i<n;i++){
    ls+=l[i];
  }
  printf("%g",ls);
  for(i=0;i<nb;i++){
    emin=1e300;
    emax=-1e300;
    for(j=0;j<np;j++){
      e=1e-3*particle_track(e0,l,phi0,g[i],b[i],n,phi[j],de[j]);
      if ((e>emax)&&(j<np/2)) emax=e;
      if ((e<emin)&&(j>np/2)) emin=e;
    }
    if (emax>emax0) emax0=emax;
    if (emin<emin0) emin0=emin;
    e=0.5*(emin+emax);
    printf(" %g %g",emin,emax);
  }
  printf(" %g %g\n",emin0,emax0);
}

#define NBUF 1000

main_old()
{
  int nb=150,np,n=3,i,j;
  double gradient=143.97,e,e0=9e3,tmp;
  double l[3]={30e3,1101e3,1500e3};
  double phi0[3]={-2.0,6.0,30.0};
  double *g,*de,*b,*phi;
  double twopi;
  char buffer[NBUF],*point;
  FILE *file;
  twopi=2.0*acos(-1.0);
  file=fopen("beam.dat","r");
  point=fgets(buffer,NBUF,file);
  np=strtol(buffer,&point,10);
  point=fgets(buffer,NBUF,file);
  de=(double*)malloc(sizeof(double)*np);
  phi=(double*)malloc(sizeof(double)*np);
  g=(double*)malloc(sizeof(double)*nb);
  b=(double*)malloc(sizeof(double)*nb);
  for(i=0;i<np;i++){
    point=fgets(buffer,NBUF,file);
    phi[i]=strtod(point,&point)*twopi*1e-4;
    de[i]=strtod(point,&point);
  }
  fclose(file);
  e=e0;
  for(i=0;i<n;i++){
    phi0[i]*=twopi/360.0;
    tmp=l[i];
    l[i]=(l[i]-e)/(gradient*cos(phi0[i])-de[np/2]);
    e=tmp;
  }
  for(i=0;i<nb;i++){
    g[i]=1.0+0.005*i;
    if(g[i]>1.25) g[i]=1.25;
    b[i]=(1.0-g[i])*0.955;
    g[i]*=gradient;
    b[i]*=gradient;
  }
  track_beam(e0,l,phi0,3,g,b,nb,phi,de,np);
//  printf("%g %g %g\n",l[0],l[1],l[2]);
  exit(0);
}

main(int argc,char *argv[])
{
  int np,n=3,i,j;
  double gradient=143.97,e,e0=9e3,tmp;
  double l[3]={143.947,7376.75,3149.51},lt[3],lmax;
  double bf=0.9567;
  //  double gradient=149.0,e,e0=9e3,tmp;
  //  double l[3]={143.947,6776.75,3749.51},lt[3],lmax;
  //  double bf=0.945;
  double phi0[3]={-2.0,6.0,30.0};
  //  double bf=0.98;
  //double phi0[3]={12.5,12.5,12.5};

  //  double gradient=149.0,e,e0=9e3,tmp;
  //  double l[3]={143.947,8776.75,1749.51},lt[3],lmax;
  //  double bf=0.98;
  //double phi0[3]={-2.0,3.0,30.0};

  double *g,*de,*b,*phi,gmax=1.125,gmin=1.0;
  int ng=2,nb=2;
  double twopi,d_phase,lc,ls;
  char buffer[NBUF],*point;
  FILE *file;

  if(argc==2){
    d_phase=strtod(argv[1],&point);
    for(i=0;i<n;i++){
      phi0[i]+=d_phase;
    }
  }
  twopi=2.0*acos(-1.0);
  file=fopen("beam.dat","r");
  point=fgets(buffer,NBUF,file);
  np=strtol(buffer,&point,10);
  point=fgets(buffer,NBUF,file);
  de=(double*)malloc(sizeof(double)*np);
  phi=(double*)malloc(sizeof(double)*np);
  g=(double*)malloc(sizeof(double)*nb);
  b=(double*)malloc(sizeof(double)*nb);
  for(i=0;i<np;i++){
    point=fgets(buffer,NBUF,file);
    phi[i]=strtod(point,&point)*twopi*1e-4;
    de[i]=1.0*strtod(point,&point);
  }
  fclose(file);
  e=e0;
  for(i=0;i<n;i++){
    phi0[i]*=twopi/360.0;
  }
  for(i=0;i<nb;i++){
    if (i<ng){
      g[i]=gmin+(gmax-gmin)/(ng-1)*i;
    }
    else{
      g[i]=gmax;
    }
    b[i]=(1.0-g[i])*bf;
  }
  for(i=0;i<nb;i++){
    g[i]*=gradient;
    b[i]*=gradient;
  }

  for(j=0;j<110;j++){
    lmax=100.0*j;
    for(i=0;i<n;i++){
      lmax-=l[i];
      lt[i]=l[i];
      if (lmax<=0.0){
	lt[i]+=lmax;
	break;
      }
    }
    if(i>=n) i=n-1;
    //    envelope_beam(e0,lt,phi0,i+1,g,b,2,phi,de,np);
  }

  track_beam(e0,l,phi0,n,g,b,nb,phi,de,np);
  lc=0.0;
  ls=0.0;
  for (i=0;i<n;i++){
    lc+=cos(phi0[i])*l[i];
    ls+=l[i];
  }
  //  printf("# %g %g %g\n",lc,ls,lc/ls);
  exit(0);
}
