#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

step_rf(double g1[],double g2[],double h1[],double h2[],int n1,int n2)
{
  int i;
  for (i=n2-1;i>0;i--){
    g2[i]=g2[i-1];
    h2[i]=h2[i-1];
  }
  g2[0]=h1[n1-1];
  h2[0]=g1[n1-1];
  for (i=n1-1;i>0;i--){
    g1[i]=g1[i-1];
    h1[i]=h1[i-1];
  }
}

feed_rf(double g1[],double h1[],double g0,double h0)
{
  g1[0]=g0;
  h1[0]=h0;
}

step_beam(double g1[],double g2[],double h1[],double h2[],int n1,int n2,
	  double ch1,double ch2,double *e1,double *e2)
{
  int i;
  double s1=0.0,s2=0.0;
  for(i=0;i<n1;i++){
    s1+=g1[i];
    s2+=h1[i];
    g1[i]-=ch1;
    h1[i]-=ch2;
  }
  for(i=0;i<n2;i++){
    s1+=g2[i];
    s2+=h2[i];
    g2[i]-=ch1;
    h2[i]-=ch2;
  }
  *e1=s1;
  *e2=s2;
}

step(double g1[],double g2[],double h1[],double h2[],int n1,int n2,
     double ch1[],double ch2[],double g0[],double h0[],int nstep,
     double e1[],double e2[])
{
  int i;
  for (i=0;i<n1;i++){
    g1[i]=0.0;
    h1[i]=0.0;
  }
  for (i=0;i<n2;i++){
    g2[i]=0.0;
    h2[i]=0.0;
  }
  for(i=0;i<nstep;i++){
    step_rf(g1,g2,h1,h2,n1,n2);
    feed_rf(g1,h1,g0[i],h0[i]);
    step_beam(g1,g2,h1,h2,n1,n2,ch1[i],ch2[i],e1+i,e2+i);
  }
}

profile(double g1[],double g2[],double h1[],double h2[],int n1,int n2)
{
  int i;
  for(i=0;i<n1;i++){
    printf("%g %g\n",g1[i],h1[i]);
  }
  for(i=0;i<n2;i++){
    printf("%g %g\n",g2[i],h2[i]);
  }
}

/* length of first structure */

#define N1 70

/* length of second structure */

#define N2 70

//#define N1 60
//#define N2 1

/* number of bunches */

#define N 1000

/* length of ramp */

#define NF 64

/* length of flat top */

#define NT 150

/* input power */

#define P 140.0

init(double g1[],double g2[],double h1[],double h2[],double ch1[],double ch2[])
{
  int i,n;
  for (i=0;i<NF;i++){
    ch1[i]=i/(double)NF;
    ch2[i]=0.0;
  }
  n=NF;
  for(;;){
    for (i=0;i<NT;i++){
      ch1[n]=1.0;
      ch2[n]=0.0;
      n++;
      if(n==N) return;
    }
    for (i=0;i<NF;i++){
      ch1[n]=(NF-i)/(double)NF;
      ch2[n]=i/(double)NF;
      n++;
      if(n==N) return;
    }
    for (i=0;i<NT;i++){
      ch1[n]=0.0;
      ch2[n]=1.0;
      n++;
      if(n==N) return;
    }
    for (i=0;i<NF;i++){
      ch1[n]=i/(double)NF;
      ch2[n]=(NF-i)/(double)NF;
      n++;
      if(n==N) return;
    }
  }
}

step_rf2double g1[],double g2[],double h1[],double h2[],int n1,int n2)
{
  int i;
  for (i=n2-1;i>0;i--){
    g2[i]=g2[i-1];
    h2[i]=h2[i-1];
  }
  g2[0]=h1[n1-1];
  h2[0]=g1[n1-1];
  for (i=n1-1;i>0;i--){
    g1[i]=g1[i-1];
    h1[i]=h1[i-1];
  }
}

feed_rf2(double g1[],double h1[],double g0,double h0)
{
  g1[0]=g0;
  h1[0]=h0;
}

step_beam2(double g1[],double g2[],double h1[],double h2[],int n1,int n2,
	  double ch1,double ch2,double *e1,double *e2)
{
  int i;
  double s1=0.0,s2=0.0;
  for(i=0;i<n1;i++){
    s1+=g1[i];
    s2+=h1[i];
    g1[i]-=ch1;
    h1[i]-=ch2;
  }
  for(i=0;i<n2;i++){
    s1+=g2[i];
    s2+=h2[i];
    g2[i]-=ch1;
    h2[i]-=ch2;
  }
  *e1=s1;
  *e2=s2;
}

step2(double g1[],double g2[],double h1[],double h2[],int n1,int n2,
     double ch1[],double ch2[],double g0[],double h0[],int nstep,
     double e1[],double e2[])
{
  int i;
  for (i=0;i<n1;i++){
    g1[i]=0.0;
    h1[i]=0.0;
  }
  for (i=0;i<n2;i++){
    g2[i]=0.0;
    h2[i]=0.0;
  }
  for(i=0;i<nstep;i++){
    step_rf(g1,g2,h1,h2,n1,n2);
    feed_rf(g1,h1,g0[i],h0[i]);
    step_beam(g1,g2,h1,h2,n1,n2,ch1[i],ch2[i],e1+i,e2+i);
  }
}

init2(double g[],doube g0,double ch[])
{
  int i,n;
  for (i=0;i<NF;i++){
    ch[2*i]=i/(double)NF;
    ch[2*i+1]=0;
  }
  n=NF;
  for(;;){
    for (i=0;i<NT;i++){
      ch1[n]=1.0;
      ch2[n]=0.0;
      n++;
      if(n==N) return;
    }
    for (i=0;i<NF;i++){
      ch1[n]=(NF-i)/(double)NF;
      ch2[n]=i/(double)NF;
      n++;
      if(n==N) return;
    }
    for (i=0;i<NT;i++){
      ch1[n]=0.0;
      ch2[n]=1.0;
      n++;
      if(n==N) return;
    }
    for (i=0;i<NF;i++){
      ch1[n]=i/(double)NF;
      ch2[n]=(NF-i)/(double)NF;
      n++;
      if(n==N) return;
    }
  }
}

main(int argc,char *argv[])
{
  int i,n;
  double g1[N1],g2[N2],h1[N1],h2[N2];
  double ch1[N],ch2[N],p1[N],p2[N],e1[N],e2[N];

  /* set up the necessary arrays */

  init(g1,g2,h1,h2,ch1,ch2);

  for (i=0;i<N;i++){
    p1[i]=P;
    p2[i]=P;
  }
  step(g1,g2,h1,h2,N1,N2,ch1,ch2,p1,p2,N,e1,e2);

  //profile(g1,g2,h1,h2,N1,N2);
  //exit(0);

  /* delete energies of empty bunches */

  for(i=0;i<N;i++){
    if (ch1[i]==0.0){
      e1[i]=0.0;
    }
    if (ch2[i]==0.0){
      e2[i]=0.0;
    }
    printf("%g %g %g %g\n",e1[i],e2[i],ch1[i],ch2[i]);
  }

  /* finish */

  exit(0);
}
