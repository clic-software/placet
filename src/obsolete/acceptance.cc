#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"
#include "beamline.h"
#include "lattice.h"
#include "girder.h"
#include "data.h"

#include "acceptance.h"

/**********************************************************************/
/*                      Acceptance                                    */
/**********************************************************************/

int tk_Acceptance(ClientData clientdata,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  double ax=0.0,ay=0.0;
  ACCEPTANCE *a;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This commend allows to place an acceptance measurement in the current girder."},
    {(char*)"-ax",TK_ARGV_FLOAT,(char*)NULL,(char*)&ax,
     (char*)"horizontal half-aperture in microns"},
    {(char*)"-ay",TK_ARGV_FLOAT,(char*)NULL,(char*)&ay,
     (char*)"vertical half-aperture in microns"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Acceptance>",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  a=new ACCEPTANCE(ax,ay);
  inter_data.girder->add_element(a);
  a->type=ACCEPT;
			 
  return TCL_OK;
}

void
ACCEPTANCE::step_4d(BEAM *beam)
{
  int i,rms=0;
  double tmp,mx,my,x0=0.0,y0=0.0;

  if (rms) {
  }
  else {
    mx=1e300;
    my=1e300;
    for(i=0;i<beam->slices;++i){
#ifdef TWODIM
      tmp=ax0-fabs(beam->particle[i].x);
      tmp*=tmp;
      tmp/=beam->sigma_xx[i].r11;
      if (tmp<mx) mx=tmp;
#endif
      tmp=ay0-fabs(beam->particle[i].y);
      tmp*=tmp;
      tmp/=beam->sigma[i].r11;
      if (tmp<my) my=tmp;
    }
#ifdef TWODIM
    this->mx0=sqrt(mx);
#endif
    this->my0=sqrt(my);
  }
  printf("acceptance: %g %g\n",this->mx0,this->my0);
}

void
ACCEPTANCE::step_4d_0(BEAM *beam)
{
}
