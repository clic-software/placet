SPLINE* spline_list(char *v[],int n)
{
  SPLINE *spline;
  double x,y;
  char *point;

  spline=(SPLINE*)malloc(sizeof(SPLINE));
  x=(double*)alloca(sizeof(double*)*n);
  y=(double*)alloca(sizeof(double*)*n);
  for (i=0;i<n;i++){
    x[i]=strtod(v[i],&point);
    y[i]=strtod(point,&point);
  }
  spline_init(x,1,y,1,n,spline);
  return spline;
}

int tk_SplineEval(ClientData clientdata,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  double x,y;
  char *point;
  x=strtod(argv[1],&point);
  y=spline_int((SPLINE*)clientdata,x);
  sprintf(buffer,"%g",y);
  Tcl_SetResult(interp,buffer,TCL_VOLATILE);
  return TCL_OK;
}


int tk_SplineSet(ClientData clientdata,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  SPLINE *spline;
  if(error=Tcl_SplitList(interp,argv[2],&nlist,&list)){
    return error;
  }
  spline=spline_list(v,n);
  Placet_CreateCommand(interp,argv[1],&tk_SplineEval,spline,
		       NULL);
  entry=Tcl_CreateHashEntry(&SplineTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,spline);
    return TCL_OK;
  }
  return TCL_ERROR;
}
