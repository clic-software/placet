#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include <tcl.h>
#include <tk.h>

typedef struct {
  double **res,*a,**sig,*stiff,**meas,*corr,*state,*target,*est;
  int nres,nknob,nmeas,*indx;
} CORRECTION_DATA;

/* Subroutine */
int ludcmp(double a[],int n,int indx[],double *d)
{
    /* System generated locals */
    int a_offset, i__1, i__2, i__3;
    double r__1, r__2;

    /* Local variables */
    static int imax, i__, j, k;
    static double aamax, *vv, dum, sum;

    /* Parameter adjustments */
    --indx;
    a_offset = n + 1;
    a -= a_offset;
    vv=(double*)alloca(sizeof(double)*n);

    /* Function Body */
    *d = 1.0;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
      aamax = (double)0.;
      i__2 = n;
      for (j = 1; j <= i__2; ++j) {
	if ((r__1 = a[i__ + j * n], fabs(r__1)) > aamax) {
	  aamax = (r__2 = a[i__ + j * n], fabs(r__2));
	}
      }
      if (aamax == 0.0) {
	fprintf(stderr,"Singular matrix. in ludcmp");
	exit(1);
      }
      vv[i__ - 1] = 1.0 / aamax;
    }
    i__1 = n;
    for (j = 1; j <= i__1; ++j) {
      if (j > 1) {
	i__2 = j - 1;
	for (i__ = 1; i__ <= i__2; ++i__) {
	  sum = a[i__ + j * n];
	  if (i__ > 1) {
	    i__3 = i__ - 1;
	    for (k = 1; k <= i__3; ++k) {
	      sum -= a[i__ + k * n] * a[k + j * n];
	      /* L13: */
		    }
	    a[i__ + j * n] = sum;
	  }
	  /* L14: */
	}
      }
      aamax = (double)0.;
      i__2 = n;
      for (i__ = j; i__ <= i__2; ++i__) {
	sum = a[i__ + j * n];
	if (j > 1) {
	  i__3 = j - 1;
	  for (k = 1; k <= i__3; ++k) {
	    sum -= a[i__ + k * n] * a[k + j * n];
	    /* L15: */
	  }
		a[i__ + j * n] = sum;
	}
	dum = vv[i__ - 1] * fabs(sum);
	if (dum >= aamax) {
	  imax = i__;
	  aamax = dum;
	}
      }
      if (j != imax) {
	i__2 = n;
	for (k = 1; k <= i__2; ++k) {
	  dum = a[imax + k * n];
		a[imax + k * n] = a[j + k * n];
		a[j + k * n] = dum;
	}
	*d= -(*d);
	vv[imax - 1] = vv[j - 1];
      }
      indx[j] = imax;
      if (j != n) {
	if (a[j + j * n] == 0.0) {
	  a[j + j * n] = 1e-20;
	}
	dum = 1.0 / a[j + j * n];
	i__2 = n;
	for (i__ = j + 1; i__ <= i__2; ++i__) {
	  a[i__ + j * n] *= dum;
	}
      }
    }
    if (a[n + n * n] == 0.0) {
      a[n + n * n] = 1e-20;
    }
    return 0;
} /* ludcmp_ */

/* Subroutine */
int lubksb(double a[],int n,int indx[],double b[])
{
    /* System generated locals */
    int a_offset, i__1, i__2;

    /* Local variables */
    static int i__, j, ii, ll;
    static double sum;

    /* Parameter adjustments */
    --b;
    --indx;
    a_offset = n + 1;
    a -= a_offset;

    /* Function Body */
    ii = 0;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
      ll = indx[i__];
      sum = b[ll];
      b[ll] = b[i__];
      if (ii != 0) {
	i__2 = i__ - 1;
	for (j = ii; j <= i__2; ++j) {
	  sum -= a[i__ + j * n] * b[j];
	}
      } else if (sum != 0.0) {
	ii = i__;
      }
      b[i__] = sum;
    }
    for (i__ = n; i__ >= 1; --i__) {
      sum = b[i__];
      if (i__ < n) {
	i__1 = n;
	for (j = i__ + 1; j <= i__1; ++j) {
	  sum -= a[i__ + j * n] * b[j];
	}
      }
      b[i__] = sum / a[i__ + i__ * n];
    }
    return 0;
} /* lubksb_ */

double*
make_hessian(double res[],double sig[],double stiff[],int nknob,int nsens)
{
  int i,j,k;
  double *a,sum;

  a=(double*)malloc(sizeof(double)*nknob*nknob);
  for(j=0;j<nknob;++j){
    for(i=0;i<nknob;++i){
      sum=0.0;
      for(k=0;k<nsens;++k){
	sum+=res[k+i*nsens]*res[k+j*nsens]*sig[k]*sig[k];
      }
      a[i+j*nknob]=sum;
    }
    a[j*(nknob+1)]+=stiff[j]*stiff[j];
  }
  return a;
}

double*
make_hessian_n(double *res0[],double *sig0[],double stiff[],int nmat,int nknob,
	       int nsens)
{
  int i,j,k,l;
  double *a,sum,*res,*sig;

  a=(double*)malloc(sizeof(double)*nknob*nknob);
  for(j=0;j<nknob;++j){
    for(i=0;i<nknob;++i){
      a[i+j*nknob]=0.0;
    }
  }
  for(l=0;l<nmat;++l){
    res=res0[l];
    sig=sig0[l];
    for(j=0;j<nknob;++j){
      for(i=0;i<nknob;++i){
	sum=0.0;
	for(k=0;k<nsens;++k){
	  sum+=res[k+i*nsens]*res[k+j*nsens]*sig[k]*sig[k];
	}
	a[i+j*nknob]+=sum;
      }
    }
  }
  for(j=0;j<nknob;++j){
    a[j*(nknob+1)]+=stiff[j]*stiff[j];
  }
  return a;
}

void
right_hand_side(double res[],double sig[],double stiff[],double state[],
		double val[],int nknob,int nsens,double b[])
{
  int i,j;
  double sum;

  for(j=0;j<nknob;++j){
    sum=0.0;
    for(i=0;i<nsens;++i){
      sum+=res[i+j*nsens]*val[i]*sig[i]*sig[i];
    }
    b[j]=-(sum+stiff[j]*stiff[j]*state[j]);
  }
}

void
right_hand_side_n(double *res0[],double *sig0[],double stiff[],double state[],
		  double *val0[],int nmat,int nknob,int nsens,double b[])
{
  int i,j,l;
  double sum,*res,*sig,*val;

  for(j=0;j<nknob;++j){
    b[j]=0.0;
  }
  for(l=0;l<nmat;++l){
    res=res0[l];
    sig=sig0[l];
    val=val0[l];
    for(j=0;j<nknob;++j){
      sum=0.0;
      for(i=0;i<nsens;++i){
	sum+=res[i+j*nsens]*val[i]*sig[i]*sig[i];
      }
      b[j]-=sum;
    }
  }
  for(j=0;j<nknob;++j){
    b[j]-=stiff[j]*stiff[j]*state[j];
  }
}

double*
make_matrix(int nknob,int nbpm)
{
  double *a;
  int i,j;
  a=(double*)malloc(sizeof(double)*nknob*nbpm);
  for(i=0;i<nbpm*nknob;++i){
    a[i]=0.0;
  }
  for(i=0;i<nbpm;++i){
    a[2*i*nbpm+i]=1.0;
    a[(2*i+1)*nbpm+i]=1.0;
  }
  return a;
}

double*
make_matrix2(int nknob,int nbpm)
{
  double *a;
  int i,j;
  a=(double*)malloc(sizeof(double)*nknob*nbpm);
  for(i=0;i<nbpm*nknob;++i){
    a[i]=0.0;
  }
  for(i=0;i<nbpm;++i){
    a[2*i*nbpm+i]=1.0+0.0001*i*i;
    a[(2*i+1)*nbpm+i]=1.0+0.0001*i*i;
  }
  return a;
}

double*
make_matrix_old(int nknob,int nbpm)
{
  double *a;
  int i,j;
  a=(double*)malloc(sizeof(double)*nknob*nbpm);
  for(i=0;i<nbpm*nknob;++i){
    a[i]=0.0;
  }
  for(i=0;i<nknob;++i){
    a[i*nbpm+i]=1.0;
  }
  return a;
}

void
matrix_multiply(double res[],double meas[],double corr[],
		int nknob,int nsens,double gain,double est[])
{
  int i,j;
  double sum;

  for(j=0;j<nsens;++j){
    sum=meas[j];
    for(i=0;i<nknob;++i){
      sum+=res[i*nsens+j]*corr[i]*gain;
    }
    est[j]=sum;
  }
}

void
matrix_multiply_mod(double res[],double meas[],double corr[],
		    int nknob,int nsens,double gain,double est[])
{
  int i,j;
  double sum;

  for(j=0;j<nsens;++j){
    sum=0.0;
    for(i=0;i<nknob;++i){
      sum+=res[i*nsens+j]*corr[i]*gain;
    }
    est[j]=sum;
  }
}

int
Tcl_MatrixCorrectionApply(ClientData clientData,Tcl_Interp *interp,
			  int argc,char *argv[])
{
  double **bpm;
  CORRECTION_DATA *correction_data;
  char *meas,**v,**v2,buffer[100],*l_state=NULL;
  int error,i,j,m,n;
  static double gain=1.0;
  int ret=1;
  Tk_ArgvInfo table[]={
    {(char*)"-measured",TK_ARGV_STRING,(char*)NULL,
     (char*)&meas,
     (char*)"List of lists with the measurements"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of lists with the state of the correctors"},
    {(char*)"-return_value",TK_ARGV_INT,(char*)NULL,
     (char*)&ret,
     (char*)"Choice of return value: 1: corrector, 2: estimated"},
    {(char*)"-gain",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain,
     (char*)"Gain to be applied to the calculated correction"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>1){
    Tcl_SetResult(interp,"Too many arguments to <MatrixCorrectionApply>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  correction_data=(CORRECTION_DATA*)clientData;

  if (ret==1) {
    if (!meas) {
      Tcl_AppendResult(interp,"Need a list",NULL);
      return TCL_ERROR;
    }
    
    if(error=Tcl_SplitList(interp,meas,&n,&v)) {
      Tcl_AppendResult(interp,"Cannot read list",NULL);
      return error;
    }
    
    if (n!=correction_data->nres) {
      snprintf(buffer,100,"Found %d elements but expect %d\n",n,
	      correction_data->nres);
      Tcl_AppendResult(interp,
		       "List does not have the right number of elements\n",
		       buffer,NULL);
      return TCL_ERROR;
    }
    
    for(i=0;i<n;++i){
      if (error=Tcl_GetDouble(interp,v[i],correction_data->meas[0]+i)) {
	return error;
      }
      correction_data->meas[0][i]-=correction_data->target[i];
    }
    Tcl_Free((char*)v);
    
    if (l_state) {
      if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -state",NULL);
	return error;
      }
      if ((m!=correction_data->nknob)&&(m!=1)) {
	Tcl_AppendResult(interp,"",NULL);
	return TCL_ERROR;
      }
      for (j=0;j<m;++j) {
	if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	  return error;
	}
      }
      if ((m==1)&&(correction_data->nknob>1)) {
	for (i=1;i<n;++i){
	  correction_data->state[i]=correction_data->state[0];
	}
      }
      Tcl_Free((char*)v);
    } else {
      for (i=0;i<correction_data->nknob;++i) {
	correction_data->state[i]=0.0;
      }
    }
    
    right_hand_side_n(correction_data->res,correction_data->sig,
		      correction_data->stiff,
		      correction_data->state,
		      correction_data->meas,
		      correction_data->nmeas,
		      correction_data->nknob,
		      correction_data->nres,
		      correction_data->corr);
    lubksb(correction_data->a,correction_data->nknob,correction_data->indx,
	   correction_data->corr);
    for (i=0;i<correction_data->nknob;++i){
      snprintf(buffer,100,"%g",correction_data->corr[i]*gain);
      Tcl_AppendElement(interp,buffer);
    }
  }
  else {
    switch (ret) {
    case 2:
      matrix_multiply(correction_data->res[0],correction_data->meas[0],
		      correction_data->corr,correction_data->nknob,
		      correction_data->nres,gain,
		      correction_data->est);
      for (i=0;i<correction_data->nres;++i){
	snprintf(buffer,100,"%g",correction_data->est[i]
		+correction_data->target[i]);
	Tcl_AppendElement(interp,buffer);
      }    
      break;
    case 3:
      matrix_multiply_mod(correction_data->res[0],correction_data->meas[0],
			  correction_data->corr,correction_data->nknob,
			  correction_data->nres,gain,
			  correction_data->est);
      for (i=0;i<correction_data->nres;++i){
	snprintf(buffer,100,"%g",-correction_data->est[i]);
	Tcl_AppendElement(interp,buffer);
      }    
      break;
    }
  }
  return TCL_OK;
}

int
Tcl_MatrixCorrectionInit(ClientData clientData,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char **v,**v2,**v3;
  int i,n,j,m,m1,k,nmeas=1,nreal;
  CORRECTION_DATA *correction_data;
  char *init,*type="linear",*l_stiff=NULL,*l_sigma=NULL,*name,*l_state=NULL,
      *t=NULL;
  void *rndm;
  double d;
  Tk_ArgvInfo table[]={
    {(char*)"-response",TK_ARGV_STRING,(char*)NULL,
     (char*)&init,
     (char*)"List of lists with the response matrix"},
    {(char*)"-number",TK_ARGV_INT,(char*)NULL,
     (char*)&nmeas,
     (char*)"Number of response matrices used"},
    {(char*)"-stiff",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_stiff,
     (char*)"List of lists with the response matrix"},
    {(char*)"-sigma",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_sigma,
     (char*)"List of lists with the response matrix"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of lists with the response matrix"},
    {(char*)"-target",TK_ARGV_STRING,(char*)NULL,
     (char*)&t,
     (char*)"List of target values for the measurement values"},
    {(char*)"-type",TK_ARGV_STRING,(char*)NULL,
     (char*)&type,
     (char*)"type of correction (sofar linear only)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>2){
    Tcl_SetResult(interp,"Too many arguments to <MatrixCorrectionInit>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<2){
    Tcl_SetResult(interp,"<MatrixCorrectionInit> requires a name",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  name=argv[1];

  if (strcmp(type,"linear")!=0) {
    return TCL_ERROR;
  }

  if (nmeas!=1) {
    if(error=Tcl_SplitList(interp,init,&nreal,&v)) {
      printf("Cannot read list\n");
      return error;
    }
    if (nreal!=nmeas) {
      Tcl_SetResult(interp,
		    "The specified number of matrices does not agree with the real one",
		    TCL_VOLATILE);      
      return TCL_ERROR;
    }
  }
  else {
    if(error=Tcl_SplitList(interp,init,&n,&v)) {
      printf("Cannot read list\n");
      return error;
    }
    
    if(error=Tcl_SplitList(interp,v[0],&m1,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }
  }

  correction_data=(CORRECTION_DATA*)malloc(sizeof(CORRECTION_DATA));
  correction_data->res=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->res[0]=(double*)malloc(sizeof(double)*n*m1);
  correction_data->stiff=(double*)malloc(sizeof(double)*n);
  correction_data->state=(double*)malloc(sizeof(double)*n);
  correction_data->corr=(double*)malloc(sizeof(double)*n);
  correction_data->est=(double*)malloc(sizeof(double)*m1);
  correction_data->sig=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->sig[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->target=(double*)malloc(sizeof(double)*m1);
  correction_data->indx=(int*)malloc(sizeof(int)*n);
  correction_data->nmeas=nmeas;
  correction_data->nknob=n;
  correction_data->nres=m1;
  correction_data->meas=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->meas[0]=(double*)malloc(sizeof(double)*m1);

  k=0;
  for (j=0;j<m1;++j) {
    if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
      return error;
    }
    k++;
  }
  Tcl_Free((char*)v2);

  for (i=1;i<n;++i){
    if(error=Tcl_SplitList(interp,v[i],&m,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }

    if (m!=m1) {
      Tcl_AppendResult(interp,"The matrix needs to be rectangular",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
	return error;
      }
      k++;
    }
    Tcl_Free((char*)v2);
  }
  Tcl_Free((char*)v);
  if (l_stiff) {
    if(error=Tcl_SplitList(interp,l_stiff,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -stiff",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"Number of stiffness values does not match",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->stiff+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->stiff[i]=correction_data->stiff[0];
      }
    }
    Tcl_Free((char*)v);
  }
  else {
    for (i=0;i<n;++i) {
      correction_data->stiff[i]=0.0;
    }
  }
  if (t) {
    if(error=Tcl_SplitList(interp,t,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -target",NULL);
	return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->target+j)) {
	return error;
      }
    }
  }
  else {
    for (j=0;j<m1;++j) {
	correction_data->target[j]=0.0;
    }
  }
  if (l_sigma) {
    if(error=Tcl_SplitList(interp,l_sigma,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -sigma",NULL);
      return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->sig[0]+j)) {
	return error;
      }
    }
    if ((m==1)&&(m1>1)) {
      for (i=1;i<m1;++i){
	correction_data->sig[0][i]=correction_data->sig[0][0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<m1;++i) {
      correction_data->sig[0][i]=1.0;
    }
  }
  if (l_state) {
    if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -state",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->state[i]=correction_data->state[0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<n;++i) {
      correction_data->state[i]=0.0;
    }
  }
  correction_data->a=make_hessian_n(correction_data->res,
				    correction_data->sig,
				    correction_data->stiff,
				    correction_data->nmeas,
				    correction_data->nknob,
				    correction_data->nres);
  ludcmp(correction_data->a,correction_data->nknob,correction_data->indx,&d);
  Tcl_CreateCommand(interp,name,Tcl_MatrixCorrectionApply,
		    (ClientData)correction_data,(Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}

int
Tcl_MicadoCorrectionInit(ClientData clientData,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char **v,**v2,**v3;
  int i,n,j,m,m1,k,nmeas=1,nreal;
  CORRECTION_DATA *correction_data;
  char *init,*type="linear",*l_stiff=NULL,*l_sigma=NULL,*name,*l_state=NULL,
      *t=NULL;
  void *rndm;
  double d;
  Tk_ArgvInfo table[]={
    {(char*)"-response",TK_ARGV_STRING,(char*)NULL,
     (char*)&init,
     (char*)"List of lists with the response matrix"},
    {(char*)"-number",TK_ARGV_INT,(char*)NULL,
     (char*)&nmeas,
     (char*)"Number of response matrices used"},
    {(char*)"-stiff",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_stiff,
     (char*)"List of lists with the response matrix"},
    {(char*)"-sigma",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_sigma,
     (char*)"List of lists with the response matrix"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of lists with the response matrix"},
    {(char*)"-target",TK_ARGV_STRING,(char*)NULL,
     (char*)&t,
     (char*)"List of target values for the measurement values"},
    {(char*)"-type",TK_ARGV_STRING,(char*)NULL,
     (char*)&type,
     (char*)"type of correction (sofar linear only)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>2){
    Tcl_SetResult(interp,"Too many arguments to <MatrixCorrectionInit>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<2){
    Tcl_SetResult(interp,"<MatrixCorrectionInit> requires a name",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  name=argv[1];

  if (strcmp(type,"linear")!=0) {
    return TCL_ERROR;
  }

  if (nmeas!=1) {
    if(error=Tcl_SplitList(interp,init,&nreal,&v)) {
      printf("Cannot read list\n");
      return error;
    }
    if (nreal!=nmeas) {
      Tcl_SetResult(interp,
		    "The specified number of matrices does not agree with the real one",
		    TCL_VOLATILE);      
      return TCL_ERROR;
    }
  }
  else {
    if(error=Tcl_SplitList(interp,init,&n,&v)) {
      printf("Cannot read list\n");
      return error;
    }
    
    if(error=Tcl_SplitList(interp,v[0],&m1,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }
  }

  correction_data=(CORRECTION_DATA*)malloc(sizeof(CORRECTION_DATA));
  correction_data->res=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->res[0]=(double*)malloc(sizeof(double)*n*m1);
  correction_data->stiff=(double*)malloc(sizeof(double)*n);
  correction_data->state=(double*)malloc(sizeof(double)*n);
  correction_data->corr=(double*)malloc(sizeof(double)*n);
  correction_data->est=(double*)malloc(sizeof(double)*m1);
  correction_data->sig=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->sig[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->target=(double*)malloc(sizeof(double)*m1);
  correction_data->indx=(int*)malloc(sizeof(int)*n);
  correction_data->nmeas=nmeas;
  correction_data->nknob=n;
  correction_data->nres=m1;
  correction_data->meas=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->meas[0]=(double*)malloc(sizeof(double)*m1);

  k=0;
  for (j=0;j<m1;++j) {
    if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
      return error;
    }
    k++;
  }
  Tcl_Free((char*)v2);

  for (i=1;i<n;++i){
    if(error=Tcl_SplitList(interp,v[i],&m,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }

    if (m!=m1) {
      Tcl_AppendResult(interp,"The matrix needs to be rectangular",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
	return error;
      }
      k++;
    }
    Tcl_Free((char*)v2);
  }
  Tcl_Free((char*)v);
  if (l_stiff) {
    if(error=Tcl_SplitList(interp,l_stiff,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -stiff",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"Number of stiffness values does not match",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->stiff+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->stiff[i]=correction_data->stiff[0];
      }
    }
    Tcl_Free((char*)v);
  }
  else {
    for (i=0;i<n;++i) {
      correction_data->stiff[i]=0.0;
    }
  }
  if (t) {
    if(error=Tcl_SplitList(interp,t,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -target",NULL);
	return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->target+j)) {
	return error;
      }
    }
  }
  else {
    for (j=0;j<m1;++j) {
	correction_data->target[j]=0.0;
    }
  }
  if (l_sigma) {
    if(error=Tcl_SplitList(interp,l_sigma,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -sigma",NULL);
      return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->sig[0]+j)) {
	return error;
      }
    }
    if ((m==1)&&(m1>1)) {
      for (i=1;i<m1;++i){
	correction_data->sig[0][i]=correction_data->sig[0][0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<m1;++i) {
      correction_data->sig[0][i]=1.0;
    }
  }
  if (l_state) {
    if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -state",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->state[i]=correction_data->state[0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<n;++i) {
      correction_data->state[i]=0.0;
    }
  }
  Tcl_CreateCommand(interp,name,Tcl_MicadoCorrectionApply,
		    (ClientData)correction_data,(Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}

int
Correct_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"MatrixCorrectionInit",Tcl_MatrixCorrectionInit,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Correct","0.2");
  return TCL_OK;
}

