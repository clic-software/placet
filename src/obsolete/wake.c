#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <tcl.h>
#include <tk.h>

int
tk_ShortDipoleWake(ClientData clientdata,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error,ok;
  Tcl_HashEntry *entry;
  char *tab=NULL,*v;
  double q=-1.0,lambda=-1.0,loss=0.0;
  static int number=0;
  WAKE_MODE *wake;
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,
     (char*)&tab,
     (char*)"List of wakefield in [V/m^2C]"},
    {(char*)"-spline",TK_ARGV_STRING,(char*)NULL,
     (char*)&tab,
     (char*)"List of wakefield in [V/m^2C]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    sprintf(interp->result,"Must give exactly one name in QuadWake\n");
    return TCL_ERROR;
  }
  name=argv[1];
  wake=(WAKE_MODE*)xmalloc(sizeof(WAKE_MODE));
  wake->Q=q;
  wake->a0=loss;
  wake->lambda=lambda;
  wake->k=2.0*PI/lambda;
  wake->which=1;
  wake->number=number++;
  if (name){
    entry=Tcl_CreateHashEntry(&ModeTable,name,&ok);
    if (ok){
      Tcl_SetHashValue(entry,wake);
      quad_wake.on=1;
      return TCL_OK;
    }
  }
  return TCL_ERROR;
}
