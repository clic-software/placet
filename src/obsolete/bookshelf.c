void
bookshelf(CAVITY* cavity,BEAM *beam)
{
  int n,i,j,k=0;
  double c_book,s_book,*s_beam,*c_beam,k_x,k_y;
  c_book=cos(cavity->v5+cavity->get_book_phase())
    *cavity->v1*cavity->length*1e6;
  s_book=sin(cavity->v5+cavity->get_book_phase())
    *cavity->v1*cavity->length*1e6;
  s_beam=beam->s_long[cavity->field];
  c_beam=beam->c_long[cavity->field];
  n=beam->slices/beam->macroparticles;
  for (i=0;i<n;++i) {
    k_x=(c_beam[i]*c_book+s_beam[i]*s_book)*cavity->get_book_x();
    k_y=(c_beam[i]*c_book+s_beam[i]*s_book)*cavity->get_book_y();
    //    k_x+=beam->field[0].de[i]*cavity->length*1e6*cavity->get_book_x();
    //    k_y+=beam->field[0].de[i]*cavity->length*1e6*cavity->get_book_y();
    for (j=0;j<beam->macroparticles;++j){
      beam->particle[k].xp+=k_x/beam->particle[k].energy;
      beam->particle[k].yp+=k_y/beam->particle[k].energy;
      ++k;
    }
  }
}
