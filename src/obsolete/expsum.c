#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define NMAX 1000000

main(int argc,char *argv[])
{
  int i,j,n,m,m0;
  double *d,*ds,q;
  FILE *f;
  char name[255];

  q=strtod(argv[1],NULL);
  d=(double*)malloc(sizeof(double)*NMAX);
  ds=(double*)malloc(sizeof(double)*NMAX);
  n=strtol(argv[2],NULL,10);
  sprintf(name,"position.%d",0);
  f=fopen(name,"r");
  m0=fread(d,sizeof(double),NMAX,f);
  fclose(f);
    printf("%d\n",m0);
  for (j=0;j<m0;j++) {
    ds[j]=0.0;
  }
  for (i=1;i<=n;i++) {
    sprintf(name,"position.%d",i);
    f=fopen(name,"r");
    m=fread(d,sizeof(double),NMAX,f);
    printf("%d\n",m);
    fclose(f);
    if (m!=m0) {
      exit(1);
    }
    for (j=0;j<m;j++) {
      ds[j]=ds[j]*q+d[j];
    }
  }
  f=fopen("position.all","w");
  m=fwrite(ds,sizeof(double),m,f);
  fclose(f);
  exit(0);
}
