#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "fftw.h"

#define TWOPI 6.283185307179586

#define Noff 0
#define Npoint 1000
#define M 3

main()
{
  char buffer[1000],*point;
  FILE *f;
  int i,m=0,j;
  double dummy;
  fftw_complex *in;
  fftw_plan p; 

  in=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
  p = fftw_create_plan_specific(Npoint,FFTW_FORWARD,
				FFTW_ESTIMATE | FFTW_IN_PLACE,
				in,M,in,1);
  for (i=Noff;i<Noff+Npoint;i++){
    snprintf(buffer,1000,"/local/tmp/fftw_beamline_100/b_g_0.03.%d\0",i);
    f=fopen(buffer,"r");
    for (j=0;j<M;j++){
      point=fgets(buffer,1000,f);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      in[m].re=dummy;
      in[m].im=0.0;
      m++;
    }
/*
    if (i<450) {
      in[(i-Noff)*M+0].re=2.0+sin(TWOPI/10.0*i);
    }
    else {
      in[(i-Noff)*M+0].re=2.0;
    }
*/
    fclose(f);
  }
  fftw(p,M,in,M,1,NULL,1,1);
  for (i=0;i<Npoint;i++){
    for (j=0;j<1;j++){
      printf("%d %g %g\n",i,in[i*M+j].re,in[i*M+j].im);
    }
  }
}



