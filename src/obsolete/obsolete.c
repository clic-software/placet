/* creates the beam for the drivebeam including the RF-kick */

BEAM*
xxx_make_multi_bunch_drive_rf(BEAMLINE *beamline,char *name,
			  DRIVE_BEAM_PARAM *param)
{
  FILE *file;
  BEAM *bunch;
  int i,n,k,nmacro,j,j1,nbunches;
  double theta,wgtsum,ecut,sigma_e,delta,fact,sigmay,sigmayp,sigmax,sigmaxp;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy;
  char buffer[100],*point;
  double *store,elow,ehigh,de,sumwgt;
  double dz;
  double a0,charge,e0;
  double phase,det,attenuation;
  double c0,s0;

  s0=sin(param->phi0);
  c0=cos(param->phi0);
  nbunches=param->n_bunch;
  a0=drive_data.a0;
  e0=param->e0;
  sigma_e=param->espread;
  nmacro=param->n_macro;
  charge=param->charge;
  ecut=param->ecut;

  epsx=param->emittx;
  epsy=param->emitty;
  alphax=param->alphax;
  alphay=param->alphay;
  betax=param->betax;
  betay=param->betay;

  epsx*=0.511e-3/e0*1e12*EMITT_UNIT;
  epsy*=0.511e-3/e0*1e12*EMITT_UNIT;

  file=fopen(name,"r");
  fscanf(file,"%d",&n);
  /* scd to update for number of phases */
  bunch=(BEAM*)bunch_make(nbunches,n,nmacro,1);
  bunch->drive_data->param.drive=(DRIVE_BEAM_PARAM*)
    malloc(sizeof(DRIVE_BEAM_PARAM));
  *(bunch->drive_data->param.drive)=*param;

  fscanf(file,"%g",&tmp1);
  wgtsum=0.0;
  
  de=2.0*ecut/(double)(nmacro);

  sigmay=param->envelope*sqrt(epsy*betay);
  sigmayp=param->envelope*sqrt(epsy/betay);

  sigmax=param->envelope*sqrt(epsx*betax);
  sigmaxp=param->envelope*sqrt(epsx/betax);

  for (i=0;i<n;i++){
    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (k=0;k<nmacro;k++){
      if (nmacro>1){
	if (k/param->n_energy==0){
	  fact=(1.0-param->envelope_wgt)*param->eslice_wgt[k];
	  bunch_set_slice_y(bunch,0,i,k,zero_point);
	  bunch_set_slice_yp(bunch,0,i,k,0.0);
#ifdef TWODIM
	  bunch_set_slice_x(bunch,0,i,k,0.0);
	  bunch_set_slice_xp(bunch,0,i,k,0.0);
#endif
	}
	else{
	  theta=2.0*PI*(double)k/(double)(nmacro-param->n_energy);
	  fact=param->envelope_wgt
	    *param->eslice_wgt[k%param->n_energy]
	    /(double)(nmacro/param->n_energy-1);
	  bunch_set_slice_y(bunch,0,i,k,zero_point+sigmay*sin(theta)*c0);
	  bunch_set_slice_yp(bunch,0,i,k,sigmayp*cos(theta)*c0);
#ifdef TWODIM
	  bunch_set_slice_x(bunch,0,i,k,sigmax*sin(theta)*s0);
	  bunch_set_slice_xp(bunch,0,i,k,sigmaxp*cos(theta)*s0);
#endif
	}
      }
      else{
	fact=1.0;
	bunch_set_slice_y(bunch,0,i,k,zero_point);
	bunch_set_slice_yp(bunch,0,i,k,0.0);
#ifdef TWODIM
	bunch_set_slice_x(bunch,0,i,k,0.0);
	bunch_set_slice_xp(bunch,0,i,k,0.0);
#endif
      }
      bunch_set_slice_energy(bunch,0,i,k,
			     e0+param->d_energy[k%param->n_energy]);
      bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
      wgtsum+=tmp3*fact;
#ifdef TWODIM
      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
      bunch_set_sigma_xy(bunch,0,i,k);
#endif
      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
    }
    bunch->acc_field[0][i]=0.0;
    bunch->field->de[i]=tmp2*1e-3;
    bunch->z_position[i]=-tmp1;
  }
  fclose(file);

  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }

  k=n*nmacro;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k]=bunch->particle[i];
      bunch->sigma[k]=bunch->sigma[i];
#ifdef TWODIM
      bunch->sigma_xx[k]=bunch->sigma_xx[i];
      bunch->sigma_xy[k]=bunch->sigma_xy[i];
#endif
      k++;
    }
  }

  k=n;
  bunch->drive_data->bunch_z=(double*)malloc(sizeof(double)*bunch->bunches);
  for (j=1;j<nbunches;j++){
    if (j<param->n_z0) {
      //      bunch->drive_data->bunch_z[j]=j*param->distance*1e6+param->z0[j];
      bunch->drive_data->bunch_z[j]=param->bucket[j]*param->distance*1e6
	+param->z0[j];
      for (i=0;i<n;i++){
	bunch->z_position[k]=bunch->z_position[i]
	  +bunch->drive_data->bunch_z[j];
	k++;
      }
    }
    else {
      bunch->drive_data->bunch_z[j]=j*param->distance*1e6;
      for (i=0;i<n;i++){
	bunch->z_position[k]=bunch->z_position[i]+j*param->distance*1e6;
	k++;
      }
    }
  }

  if (param->n_z0) {
    bunch->drive_data->bunch_z[0]=param->bucket[0]*param->distance*1e6
	+param->z0[0];
    for (i=0;i<n;i++){
      bunch->z_position[i]+=param->z0[0];
    }
  }

  j1=min(param->n_ramp,nbunches);
  k=0;
  fact=param->s_ramp;
  for(j=0;j<j1;j++){
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k].wgt*=fact;
      k++;
    }
    if (((j+1)%param->ramp_step)==0){
      fact+=(1.0-param->s_ramp)*(double)param->ramp_step/(double)param->n_ramp;
    }
  }

  k=n;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n;i++){
      bunch->acc_field[0][k]=0.0;
      k++;
    }
  }

  /* not needed in the moment */
  /*
  drive_longitudinal_2(bunch,drive_data.lambda_longitudinal*1e6,charge);
  rf_kick_fill(bunch,drive_data.lambda_longitudinal*1e6,charge);
  */
  bunch->drive_data->longrange_max=1;

  /*
  bunch->drive_data->along[0]=a0*(charge*1.6e-19*1e12)*1e-6*1e-3;

  fact=1.0;
  attenuation=exp(-param->distance/drive_data.lambda_transverse
		  *PI/drive_data.q_value
		  *1.0/(1.0-drive_data.beta_group_t));
  printf("Attenuation %g\n",attenuation);
  printf("fact[%d] %g\n",0,bunch->drive_data->along[0]);
  for (i=1;i<nbunches;i++){
    fact*=attenuation;
    bunch->drive_data->along[i]=bunch->drive_data->along[0]
      *(1.0-fabs(drive_data.beta_group_t)*param->distance*i
	/(drive_data.l_cav*(1.0-drive_data.beta_group_t)))*fact;
    printf("fact[%d] %g\n",i,bunch->drive_data->along[i]);
    if (bunch->drive_data->along[i]<=0.0) break;
  }
  bunch->drive_data->longrange_max=i-1;
  printf("longrange_max=%d\n",i-1);
*/
  /* scd new 26.1.98*/
  if(param->resist){ 
    for (i=1;i<nbunches;i++){
      bunch->drive_data->blong[i]=resist_wall_transv((double)i*param->distance)
	*param->charge*1.602e-19*1e-9;
    }
  }

  if (param->resist){
    bunch->which_field=14;
  }
  else{
    bunch->which_field=12;
  }

  bunch->drive_data->empty_buckets=0;
  bunch->drive_data->bunches_per_train=bunch->bunches;

  longrange_fill_new(bunch,drive_data.lambda_transverse);
  bunch->drive_data->al=(double*)malloc(sizeof(double)*bunch->bunches*
					bunch->slices_per_bunch);
  bunch->drive_data->bl=(double*)malloc(sizeof(double)*bunch->bunches*
					bunch->slices_per_bunch);

  bunch->drive_data->along[0]=a0*(charge*1.6e-19*1e12)*1e-6*1e-3;
  attenuation=exp(-param->distance/drive_data.lambda_transverse
		  *PI/drive_data.q_value
		  *1.0/(1.0-drive_data.beta_group_t));
  printf("Attenuation %g\n",attenuation);
  for (i=1;i<nbunches;i++){
    bunch->drive_data->along[i]=
      exp(-(bunch->drive_data->bunch_z[i]-bunch->drive_data->bunch_z[i-1])
	  *1e-6/drive_data.lambda_transverse*PI/drive_data.q_value
		  *1.0/(1.0-drive_data.beta_group_t));
  }
  bunch->drive_data->longrange_max=nbunches;

  bunch->drive_data->factor_long=-2.0*PI*1e-9*(charge*1.602e-19)*C
	     /drive_data.lambda_longitudinal*drive_data.r_over_q;
  bunch->drive_data->factor_long/=(1.0-drive_data.beta_group_l);
  bunch->drive_data->factor_kick=-2.0*PI*1e-9*(charge*1.602e-19)*C
	     /drive_data.lambda_longitudinal*drive_data.r_over_q
    *drive_data.lambda_longitudinal/(2.0*PI)*1e6*1e6;
  bunch->drive_data->factor_kick/=(1.0-drive_data.beta_group_l);
printf("kick: %g\n",bunch->drive_data->factor_kick);
  longrange_fill_long_new(bunch,drive_data.lambda_longitudinal);
  return bunch;
}

