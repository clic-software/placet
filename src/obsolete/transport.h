#ifndef transport_h
#define transport_h

void transport_add_element();
void quadrupole_list_transport(FILE*,ELEMENT*);
void drift_list_transport(FILE *file,ELEMENT *element);
void bpm_list_transport(FILE *file,ELEMENT *element);
void cavity_list_transport(FILE *file,ELEMENT *element);
void beamline_list_transport(FILE *file,BEAMLINE *beamline,double gradient);
void transport_read_file(FILE *file,BEAMLINE *beamline,GIRDER **girder);

#endif
