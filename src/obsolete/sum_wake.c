#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double
sum_wake_drive(double lambda,double length,double beta,double Q,double dist)
{
  double p0=1.0,sum=0.0,d,p=1.0;

  if (Q>0.0) {
    p0=exp(-dist*acos(-1.0)/(lambda*(1.0-beta)*Q));
  }
  d=beta/(1.0-beta)*dist;
  length-=d;
  while (length>=0.0) {
    p*=p0;
    sum+=length*p;
    length-=d;
  }
  return sum;
}

main()
{
  int i;
  double no_damp,beta=0.59;
  no_damp=sum_wake_drive(0.01,1.3,beta,0.0,0.02);
  for(i=1;i<100;i++){
    printf("%g %g\n",i*10.0,sum_wake_drive(0.01,1.3,beta,i*10.0,0.02)/no_damp);
  }
}
