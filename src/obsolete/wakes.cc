void
particle_beam_bin(BEAM *beam,double zmin,double zmax,int nbin_z,int nbin_e)
{
  double *emin,*emax,dzi;
  double *ch,*x,*xp,*y,*yp,*e;
  double *cor;
  double x1,xp1,y1,yp1;

  emin=(double*)alloca(sizeof(double)*nbin_z);
  emax=(double*)alloca(sizeof(double)*nbin_z);
  x=(double*)alloca(sizeof(double)*nbin_e*nbin_z);
  xp=(double*)alloca(sizeof(double)*nbin_e*nbin_z);
  y=(double*)alloca(sizeof(double)*nbin_e*nbin_z);
  yp=(double*)alloca(sizeof(double)*nbin_e*nbin_z);
  e=(double*)alloca(sizeof(double)*nbin_e*nbin_z);
  ch=(double*)alloca(sizeof(double)*nbin_e*nbin_z);
  cor=(double*)alloca(sizeof(double)*nbin_e*nbin_z*10);

  dzi=(zmax-zmin)/nbin_z;
  for (int ib=0;ib<beam->bunches;++ib) {
    for (int j=0;j<nbin_e;++j) {
      emin[j]=std::numeric_limits<double>::infinity();
      emax[j]=-std::numeric_limits<double>::infinity();
    }
    int m=beam->slices_per_bunch*beam->macroparticles*ib;
    for (int i=0;i<beam->slices_per_bunch*beam->macroparticles;++i) {
      int j=(int)((beam->particle[m].z-zmin)*dzi);
      if ((j<0)||(j>=nbin_z)) {
      }
      else {
	if (beam->particle[m].energy<emin[j]) emin[j]=beam->particle[m].energy;
	if (beam->particle[m].energy>emax[j]) emax[j]=beam->particle[m].energy;
      }
      m++;
    }
    for (int j=0;j<nbin_e*nbin_z;++j) {
      ch[j]=0.0;
      e[j]=0.0;
      x[j]=0.0;
      xp[j]=0.0;
      y[j]=0.0;
      yp[j]=0.0;
    }

    m=beam->slices_per_bunch*beam->macroparticles*ib;
    for (int i=0;i<beam->slices_per_bunch*beam->macroparticles;++i) {
      int j=(int)((beam->particle[m].z-zmin)*dzi);
      if ((j<0)||(j>=nbin_z)) {
      }
      else {
	int k=(int)((beam->particle[m].energy-emin[j])/(emax[j]-emin[j]));
	if (k==nbin_e) k=nbin_e-1;
	ch[j*nbin_e+k]+=beam->particle[m].wgt;
	x[j*nbin_e+k]+=beam->particle[m].wgt*beam->particle[m].x;
	xp[j*nbin_e+k]+=beam->particle[m].wgt*beam->particle[m].xp;
	y[j*nbin_e+k]+=beam->particle[m].wgt*beam->particle[m].y;
	yp[j*nbin_e+k]+=beam->particle[m].wgt*beam->particle[m].yp;
	e[j*nbin_e+k]+=beam->particle[m].wgt*beam->particle[m].energy;
      }
      m++;
    }
    for (int j=0;j<nbin_e*nbin_z;++j) {
      x[j]/=ch[j];
      xp[j]/=ch[j];
      y[j]/=ch[j];
      yp[j]/=ch[j];
      e[j]/=ch[j];
    }

    m=beam->slices_per_bunch*beam->macroparticles*ib;

    for (int j=0;j<nbin_e*nbin_z;++j) {
      cor[j*10+0]=0.0;
      cor[j*10+1]=0.0;
      cor[j*10+2]=0.0;
      cor[j*10+3]=0.0;
      cor[j*10+4]=0.0;
      cor[j*10+5]=0.0;
      cor[j*10+6]=0.0;
      cor[j*10+7]=0.0;
      cor[j*10+8]=0.0;
      cor[j*10+9]=0.0;
    }

    for (int i=0;i<beam->slices_per_bunch*beam->macroparticles;++i) {
      int j=(int)((beam->particle[m].z-zmin)*dzi);
      if ((j<0)||(j>=nbin_z)) {
      }
      else {
	int k=(int)((beam->particle[m].energy-emin[j])/(emax[j]-emin[j]));
	if (k==nbin_e) k=nbin_e-1;
	x1=beam->particle[m].x-x[j*nbin_e+k];
	xp1=beam->particle[m].xp-xp[j*nbin_e+k];
	y1=beam->particle[m].y-y[j*nbin_e+k];
	yp1=beam->particle[m].yp-yp[j*nbin_e+k];
	cor[(j*nbin_e+k)*10+0]+=beam->particle[m].wgt*x1*x1;
	cor[(j*nbin_e+k)*10+1]+=beam->particle[m].wgt*x1*xp1;
	cor[(j*nbin_e+k)*10+2]+=beam->particle[m].wgt*x1*y1;
	cor[(j*nbin_e+k)*10+3]+=beam->particle[m].wgt*x1*yp1;
	cor[(j*nbin_e+k)*10+4]+=beam->particle[m].wgt*xp1*xp1;
	cor[(j*nbin_e+k)*10+5]+=beam->particle[m].wgt*xp1*y1;
	cor[(j*nbin_e+k)*10+6]+=beam->particle[m].wgt*xp1*yp1;
	cor[(j*nbin_e+k)*10+7]+=beam->particle[m].wgt*y1*y1;
	cor[(j*nbin_e+k)*10+8]+=beam->particle[m].wgt*y1*yp1;
	cor[(j*nbin_e+k)*10+9]+=beam->particle[m].wgt*yp1*yp1;
      }
      m++;
    }
    for (int j=0;j<nbin_e*nbin_z;++j) {
      cor[j*10+0]/=ch[j];
      cor[j*10+1]/=ch[j];
      cor[j*10+2]/=ch[j];
      cor[j*10+3]/=ch[j];
      cor[j*10+4]/=ch[j];
      cor[j*10+5]/=ch[j];
      cor[j*10+6]/=ch[j];
      cor[j*10+7]/=ch[j];
      cor[j*10+8]/=ch[j];
      cor[j*10+9]/=ch[j];
    }
    
    /* do something with the bins */
    
    
  }
}
