#include <limits>
#include <cmath>

#include "quadrupole.hh"

#define M_SQRT3 1.7320508075688772935274463415058723669428

void Quadrupole::transport(std::vector<Particle> &beam ) const
{
  if (fabs(tilt)>std::numeric_limits<double>::epsilon()) {
    double c,s;
    sincos(tilt,&s,&c);
    for (size_t i=0; i<beam.size(); i++) { 
      Particle &particle = beam[i];
      double tmp=c*particle.x-s*particle.y;
      particle.y=s*particle.x+c*particle.y;
      particle.x=tmp;
      tmp=c*particle.xp-s*particle.yp;
      particle.yp=s*particle.xp+c*particle.yp;
      particle.xp=tmp;
    }
  }
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    Particle &particle = *i;
    const double kk = k0/length/particle.energy;
    const double ksqrt = sqrt(fabs(kk));
    const double ch = cosh(ksqrt*length);
    const double sh = sinh(ksqrt*length);
    double c,s;
    sincos(ksqrt*length,&s,&c);
    if (kk>0.0) {
      double tmp = particle.x*ch + particle.xp*sh/ksqrt;
      particle.xp = particle.x*sh*ksqrt + particle.xp*ch;
      particle.x = tmp;
      tmp = particle.y*c + particle.yp*s/ksqrt;
      particle.yp = -particle.y*s*ksqrt + particle.yp*c;
      particle.y = tmp;
      //particle.z += length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
    } else {
      double tmp = particle.x*c + particle.xp*s/ksqrt;
      particle.xp = -particle.x*s*ksqrt + particle.xp*c;
      particle.x = tmp;
      tmp = particle.y*ch + particle.yp*sh/ksqrt;
      particle.yp = particle.y*sh*ksqrt + particle.yp*ch;
      particle.y = tmp;
      //particle.z += length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
    }
  }
  if (fabs(tilt)>std::numeric_limits<double>::epsilon()) {
    double c,s;
    sincos(-tilt,&s,&c);
    for (size_t i=0; i<beam.size(); i++) {
      Particle &particle = beam[i];
      double tmp=c*particle.x-s*particle.y;
      particle.y=s*particle.x+c*particle.y;
      particle.x=tmp;
      tmp=c*particle.xp-s*particle.yp;
      particle.yp=s*particle.xp+c*particle.yp;
      particle.xp=tmp;
    } 
  }
}

void Quadrupole::transport_synrad(std::vector<Particle> &beam ) const
{
  if (fabs(tilt)>std::numeric_limits<double>::epsilon()) {
    double c,s;
    sincos(tilt,&s,&c);
    for (size_t i=0; i<beam.size(); i++) { 
      Particle &particle = beam[i];
      double tmp=c*particle.x-s*particle.y;
      particle.y=s*particle.x+c*particle.y;
      particle.x=tmp;
      tmp=c*particle.xp-s*particle.yp;
      particle.yp=s*particle.xp+c*particle.yp;
      particle.xp=tmp;
    }
  }
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    Particle &particle = *i;
    double kk = k0/length/particle.energy;
    if (fabs(kk)<=std::numeric_limits<double>::epsilon()) {
      particle.x += length*particle.xp;
      particle.y += length*particle.yp;
      //particle.z += length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
    } else {
      size_t Nsteps;
      {
	const double angle0 = (fabs(particle.x) + fabs(particle.y)) * 1e-6 * k0 / particle.energy;
	const double radius = length/fabs(angle0);
	const double gamma = particle.gamma();
	const double meanfreepath = M_SQRT3*radius/(2.5*ALPHA_EM*gamma);
	Nsteps = (size_t)std::max(20.0, length / meanfreepath * 10.0 + 3.0);
      }
      // conservative estimate of the number of steps
      const double length_step = length/Nsteps;
      double freepath = 0.0;
      double ksqrt = sqrt(fabs(kk));
      double ch = cosh(ksqrt*length_step);
      double sh = sinh(ksqrt*length_step);
      double c,s;
      sincos(ksqrt*length_step,&s,&c);
      for (size_t j=0; j<Nsteps; j++) {
	double angle0;
	if (kk>0.0) {
	  double tmp  = particle.x*ch + particle.xp*sh/ksqrt;
	  double tmpp = particle.x*sh*ksqrt + particle.xp*ch;
	  double tx   = tmpp-particle.xp;
	  particle.x  = tmp;
	  particle.xp = tmpp;
	  tmp  =  particle.y*c + particle.yp*s/ksqrt;
	  tmpp = -particle.y*s*ksqrt + particle.yp*c;
	  double ty   = tmpp-particle.yp;
	  particle.y  = tmp;
	  particle.yp = tmpp;
	  angle0 = sqrt(tx*tx+ty*ty)*1e-6;
	  //particle.z += length_step*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
	} else {
	  double tmp  =  particle.x*c + particle.xp*s/ksqrt;
	  double tmpp = -particle.x*s*ksqrt + particle.xp*c;
	  double tx   = tmpp-particle.xp;
	  particle.x  = tmp;
	  particle.xp = tmpp;
	  tmp  = particle.y*ch + particle.yp*sh/ksqrt;
	  tmpp = particle.y*sh*ksqrt + particle.yp*ch;
	  double ty   = tmpp-particle.yp;
	  particle.y  = tmp;
	  particle.yp = tmpp;
	  //particle.z += length_step*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
	  angle0 = sqrt(tx*tx+ty*ty)*1e-6;
	}
	{
	  const double radius = length_step/angle0;
	  const double gamma = particle.gamma();
	  const double meanfreepath = M_SQRT3*radius/(2.5*ALPHA_EM*gamma);
	  if (j==0)
	    freepath = gsl_ran_exponential(rng, meanfreepath);
	}
	const double energy0 = particle.energy;
	while (freepath < length_step) {
	  {
	    const double radius = length_step/angle0;
	    const double gamma = particle.gamma();
	    const double energy_critical = 1.5*HBAR*C_LIGHT*gamma*gamma*gamma/radius;
	    const double energy_loss = syngen()*energy_critical;
	    particle.energy -= energy_loss;
	  }
	  if (fabs(particle.energy) < std::numeric_limits<double>::epsilon()) {
	    particle.energy = 0.;
	    particle.weight = 0.;
	    goto end;
	  }
	  const double radius = length_step/angle0;
	  const double gamma = particle.gamma();
	  const double meanfreepath = M_SQRT3*radius/(2.5*ALPHA_EM*gamma);
	  freepath += gsl_ran_exponential(rng, meanfreepath);
	}
	if (energy0!=particle.energy) {
	  kk = k0/length/particle.energy;
	  ksqrt = sqrt(fabs(kk));
	  ch = cosh(ksqrt*length_step);
	  sh = sinh(ksqrt*length_step);
	  sincos(ksqrt*length_step,&s,&c);
	}
	freepath -= length_step;
      }
    }
  end:;
  }
  if (fabs(tilt)>std::numeric_limits<double>::epsilon()) {
    double c,s;
    sincos(-tilt,&s,&c);
    for (size_t i=0; i<beam.size(); i++) {
      Particle &particle = beam[i];
      double tmp=c*particle.x-s*particle.y;
      particle.y=s*particle.x+c*particle.y;
      particle.x=tmp;
      tmp=c*particle.xp-s*particle.yp;
      particle.yp=s*particle.xp+c*particle.yp;
      particle.xp=tmp;
    } 
  }
}
