#include <limits>
#include <cmath>

#include "dipole.hh"

void Dipole::transport(std::vector<Particle> &beam ) const
{
  if (length > std::numeric_limits<double>::epsilon()) {
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      particle.x += length*particle.xp/2.;
      particle.y += length*particle.yp/2.;
      particle.xp += a_x/particle.energy;
      particle.yp += a_y/particle.energy;
      particle.x += length*particle.xp/2.;
      particle.y += length*particle.yp/2.;
      //particle.z += length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
    }
  } else {
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      particle.xp += a_x/particle.energy;
      particle.yp += a_y/particle.energy;
    }
  }
}
