#include <limits>

#include "multipole.hh"

#define M_SQRT3 1.7320508075688772935274463415058723669428

void Multipole::transport(std::vector<Particle> &beam ) const
{
  // initialize
  std::complex<double> strength_n = strength / double(nstep);
  for (int i=2;i<type;i++)
    strength_n/=double(i*1000000.0);
  const std::complex<double> Kn = strength_n * std::polar(1.0, type * tilt);

  // loop over the particles
  const double length_step = length/nstep;
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    Particle &particle = *i;
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      if (type>=1) {
	// leap-frog
	std::complex<double> Kick = Kn * std::pow(std::complex<double>(particle.x, particle.y), type-1) / particle.energy;
	// loop over the thin lenses
	for (int j=0; j<nstep; j++) {
	  double vxh = particle.xp - real(Kick) * 0.5;
	  double vyh = particle.yp + imag(Kick) * 0.5;
	  particle.x += vxh * length_step;
	  particle.y += vyh * length_step;
	  Kick = Kn * std::pow(std::complex<double>(particle.x, particle.y), type-1) / particle.energy;
	  particle.xp = vxh - real(Kick) * 0.5;
	  particle.yp = vyh + imag(Kick) * 0.5;
	  //particle.z += length_step * 1e6 * particle.momentum_deviation(ref_energy) / ref_gamma2();
	}
      }
    }
  }
}

void Multipole::transport_synrad(std::vector<Particle> &beam ) const
{
  // initialize
  std::complex<double> strength_n = strength / double(nstep);
  for (int i=2;i<type;i++)
    strength_n/=double(i*1000000.0);
  const std::complex<double> Kn = strength_n * std::polar(1.0, type * tilt);

  // loop over the particles
  const double length_step = length/nstep;
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    Particle &particle = *i;
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      if (type>=1) {
	// leap-frog
	std::complex<double> Kick = Kn * std::pow(std::complex<double>(particle.x, particle.y), type-1) / particle.energy;
	// synrad
	double freepath;
	{
	  const double angle0 = std::abs(Kick) * 1e-6;
	  const double radius = length_step/angle0;
	  const double gamma = particle.gamma();
	  const double meanfreepath = M_SQRT3*radius/(2.5*ALPHA_EM*gamma);
	  freepath = gsl_ran_exponential(rng, meanfreepath);
	}
	// loop over the thin lenses
	for (int j=0; j<nstep; j++) {
	  double vxh = particle.xp - real(Kick) * 0.5;
	  double vyh = particle.yp + imag(Kick) * 0.5;
	  particle.x += vxh * length_step;
	  particle.y += vyh * length_step;
	  Kick = Kn * std::pow(std::complex<double>(particle.x, particle.y), type-1) / particle.energy;
	  particle.xp = vxh - real(Kick) * 0.5;
	  particle.yp = vyh + imag(Kick) * 0.5;
	  //particle.z += length_step * 1e6 * particle.momentum_deviation(ref_energy) / ref_gamma2();
	  // synrad calculation
	  while (freepath < length_step) {
	    {
	      const double angle0 = std::abs(Kick) * 1e-6;
	      const double radius = length_step/angle0;
	      const double gamma = particle.gamma();
	      const double energy_critical = 1.5*HBAR*C_LIGHT*gamma*gamma*gamma/radius;
	      const double energy_loss = syngen()*energy_critical;
	      particle.energy -= energy_loss;
	    }
	    if (fabs(particle.energy) < std::numeric_limits<double>::epsilon()) {
	      particle.energy = 0.;
	      particle.weight = 0.;
	      goto end;
	    }
	    {
	      const double angle0 = std::abs(Kick) * 1e-6;
	      const double radius = length_step/angle0;
	      const double gamma = particle.gamma();
	      const double meanfreepath = M_SQRT3*radius/(2.5*ALPHA_EM*gamma);
	      freepath += gsl_ran_exponential(rng, meanfreepath);
	    }
	  }
	  freepath -= length_step;
	  // end of synrad calculation
	}
      }
    }
  end:;
  }
}
