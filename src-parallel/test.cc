/*
**
** MPI tracking code for PLACET
**
** 
*/

#include <fstream>
#include <cstring>

#include <getopt.h>
#include <libgen.h>

#include <mpi.h>

#include "mpi_buffered_stream_bcast.hh"
#include "mpi_buffered_stream.hh"
#include "file_buffered_stream.hh"
#include "mpi_stream_bcast.hh"
#include "mpi_stream.hh"

#include "stopwatch.hh"
#include "particle.hh"

int main(int argc, char **argv )
{
  std::ifstream infile("particles.in");
  Particle p;
  std::vector<Particle> bunch;
  while(infile >> p.energy >> p.x >> p.y >> p.z >> p.xp >> p.yp) {
    bunch.push_back(p);
  }
  {
    File_Buffered_OStream bin("temp.dat");
    bin << bunch;
  }
  bunch.clear();
  {
    File_Buffered_IStream bin("temp.dat");
    bin >> bunch;
  }
  std::ofstream outfile("particles.out");
  for (size_t i=0; i<bunch.size(); i++) {
    const Particle &p = bunch[i];
    outfile << ' ' << p.energy << ' ' << p.x << ' ' << p.y << ' ' << p.z << ' ' << p.xp << ' ' << p.yp << std::endl;
  }
  return 0;
}
  
