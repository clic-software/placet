#################################################################################################################################
#                                                                                                                                
# main.tcl
#
#################################################################################################################################


set e_initial 1500
set e0 $e_initial
set base_dir .
set script_dir ../../script_dir/

ParallelThreads -num 2

set bpmres 0.010

# synchrotron radiation on
set sr 1

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

# six dimensional step functions off:
set six_dim 0

set six_dim_sbend $six_dim
set six_dim_quad $six_dim

SetReferenceEnergy $e0
source $script_dir/lattices/bds.match.linac4b_v_10_10_11
Bpm -name IP
Drift -name "D0" -length 3.5026092
Bpm
BeamlineSet -name test

source $script_dir/make_bds_beam.tcl

FirstOrder 1


Octave {
    IP = placet_get_name_number_list("test", "IP");
    [E,B] = placet_test_no_correction("test", "beam0", "None",1,0,IP);
    save 'ipdist.dat' B;
    printf("Beam distr: %.6e %.6e %.6e %.6e %.6e %.6e\n",mean(B))
    printf("Beam std: %.6e %.6e %.6e %.6e %.6e %.6e\n",std(B))
}

