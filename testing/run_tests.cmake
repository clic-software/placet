# This is a script for testing the source and submitting
# your results to a common server (cdash).
# You can see the server at:
# http://cern.ch/abp-cdash/index.php?project=PLACET

# To run, follow these steps:
# - create a checkout from
#    http://svnweb.cern.ch/guest/clicsw/trunk/placet
# - set CTEST_SOURCE_DIRECTORY to point to this folder
# - set the other variables below as requested
# - run the script with the command:
#   ctest -S run_tests.cmake -VV
#  (the flags -VV will give verbose output)
# - if that works fine, uncomment last line
# - add 'ctest -S /path/to/run_tests.cmake' to e.g. crontab

# Edit these parameters:
# location of sources
set(CTEST_SOURCE_DIRECTORY "/path/to/placet")

# build options:
set(CONFIGURE_FLAGS "--enable-octave --enable-python")
set(BUILD_FLAGS "-j4")

# username and host:
set(CTEST_SITE "user@host.cern.ch")

# descriptive build name
set(CTEST_BUILD_NAME "SLC6-octave-python-GCC")

# Dashboard type, possible values are
#  'Experimental', 'Nightly' or 'Continuous'
set(DASHBOARD Experimental)


# -- DO NOT EDIT BELOW THIS LINE --
# (except from uncommenting last line when things work)

## -- BIN Dir
set(CTEST_BINARY_DIRECTORY "${CTEST_SOURCE_DIRECTORY}")

## Find the make binary:
find_program(MAKE NAMES make gmake)

# Remove old build files if existing:
if(EXISTS ${CTEST_SOURCE_DIRECTORY}/Makefile)
   exec_program(${MAKE} ${CTEST_SOURCE_DIRECTORY} ARGS distclean)
endif()


# -- Check that the user did what was told:
if(NOT EXISTS ${CTEST_SOURCE_DIRECTORY}/src/placet.cc)
   message(FATAL_ERROR "Please configure source directory properly")
endif()
if(${CTEST_SITE} STREQUAL "user@host.cern.ch")
   message(FATAL_ERROR "Please set your own site name")
endif()


## -- Update Command
set(CTEST_UPDATE_COMMAND "svn")
set(CTEST_BUILD_COMMAND  "${MAKE} ${BUILD_FLAGS} pre-install")
set(CTEST_CONFIGURE_COMMAND  "./configure ${CONFIGURE_FLAGS}")

# start the testing:
ctest_start(${DASHBOARD})
ctest_update()
ctest_configure()
ctest_build()
ctest_test(BUILD ${CTEST_SOURCE_DIRECTORY}/testing)

# uncomment this to submit to dashboard:
#ctest_submit()
