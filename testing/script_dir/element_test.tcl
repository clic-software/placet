#################################################################################################################################
#                            
# Element testing on CLIC BDS beam
#
#################################################################################################################################

set e_initial 1500
set e0 $e_initial

ParallelThreads -num 2

# synchrotron radiation on
set sr 1
set synrad $sr
set quad_synrad $sr
set mult_synrad $sr
set sbend_synrad $sr

# six dimensional on
set six_dim 1
set six_dim_sbend 1

SetReferenceEnergy $e0

Girder
# Element file
source $base_dir/element.tcl
BeamlineSet -name test

source $script_dir/make_bds_beam.tcl

FirstOrder 1

Octave {
    [E,B] = placet_test_no_correction("test", "beam0", "None",1);
    save 'ipdist.dat' B;
    printf("Beam distr: %.6e %.6e %.6e %.6e %.6e %.6e\n",mean(B))
    printf("Beam std: %.6e %.6e %.6e %.6e %.6e %.6e\n",std(B))
}
