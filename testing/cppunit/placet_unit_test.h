#ifndef PLACET_UNIT_TEST_HH
#define PLACET_UNIT_TEST_HH

// default includes for placet unit tests
#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

#endif
