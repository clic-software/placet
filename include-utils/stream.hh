#ifndef stream_hh
#define stream_hh

#include <complex>
#include <utility>
#include <vector>
#include <string>
#include <list>
#include <map>

#include "error.hh"

class OStream {
protected:

  OStream &operator = (const OStream & ) { return *this; }
  OStream(const OStream & ) {}

  virtual bool writable() const = 0;

  inline OStream() {}
	
public:

  TYPEDEF_ERROR(error);

  virtual ~OStream() {}
	
  virtual operator bool() const { return writable(); }
	
  virtual void flush() {}
  virtual void close() {}
	
  // write methods
#define WRITE(TYPE)					\
  virtual size_t write(const TYPE *ptr, size_t n ) = 0;	\
  virtual size_t write(const TYPE &ref ) = 0;
  WRITE(char);
  WRITE(bool);
  WRITE(float);
  WRITE(double);
  WRITE(long double);
  WRITE(signed short);
  WRITE(signed int);
  WRITE(signed long int);
  WRITE(unsigned short);
  WRITE(unsigned int);
  WRITE(unsigned long int);
#undef WRITE
	
  // insertion operators
#define INSERT(TYPE)							\
  inline OStream &operator<<(const TYPE &ref ) { write(ref); return *this; }
  INSERT(char);
  INSERT(bool);
  INSERT(float);
  INSERT(double);
  INSERT(long double);
  INSERT(signed short);
  INSERT(signed int);
  INSERT(signed long int);
  INSERT(unsigned short);
  INSERT(unsigned int);
  INSERT(unsigned long int);
#undef INSERT

  inline OStream &operator<<(const std::complex<double> &c )
  {
    return *this << c.real() << c.imag();
  }

  inline OStream &operator<<(const std::string &s )
  {
    size_t _length = s.size();
    *this << _length;
    write(s.c_str(), _length);
    return *this;
  }
	
  template <class T> inline OStream &operator<<(const std::list<T> &l )
  {
    *this << l.size(); // first the size, then the items
    for (typename std::list<T>::const_iterator i=l.begin(); i!=l.end(); ++i) { *this << *i; }
    return *this;
  }
	
  template <class T> inline OStream &operator<<(const std::vector<T> &v )
  {
    *this << v.size();
    for (size_t i=0; i<v.size(); i++) { *this << v[i]; }
    return *this;
  }

  template <class T, class U> inline OStream &operator<<(const std::pair<T,U> &v )
  {
    return *this << v.first << v.second;
  }

  template <class T, class U> inline OStream &operator<<(const std::map<T,U> &v )
  {
    *this << v.size();
    for (typename std::map<T,U>::const_iterator itr=v.begin(); itr!=v.end(); ++itr) {
      const T &key = (*itr).first;
      const U &value = (*itr).second;
      *this << key << value; 
    }
    return *this;
  }

};

class IStream {
protected:

  IStream &operator=(const IStream & ) { return *this; }
  IStream(const IStream & ) {}

  virtual bool readable() const = 0;
	
  inline IStream() {}

public:

  TYPEDEF_ERROR(error);
	
  virtual ~IStream() {}
  
  virtual operator bool() const { return readable(); }
  
  virtual void close() {}
	
  // read methods
#define READ(TYPE)					\
  virtual size_t read(TYPE *ptr, size_t n ) = 0;	\
  virtual size_t read(TYPE &ref ) = 0;
  READ(char);
  READ(bool);
  READ(float);
  READ(double);
  READ(long double);
  READ(signed short);
  READ(signed int);
  READ(signed long int);
  READ(unsigned short);
  READ(unsigned int);
  READ(unsigned long int);
#undef READ
	
  // extraction operators
#define EXTRACT(TYPE)							\
  inline IStream &operator>>(TYPE &ref )       { read(ref); return *this; }
  EXTRACT(char);
  EXTRACT(bool);
  EXTRACT(float);
  EXTRACT(double);
  EXTRACT(long double);
  EXTRACT(signed short);
  EXTRACT(signed int);
  EXTRACT(signed long int);
  EXTRACT(unsigned short);
  EXTRACT(unsigned int);
  EXTRACT(unsigned long int);
#undef EXTRACT
  
  inline IStream &operator>>(std::complex<double> &c )
  {
    double _re, _im;
    *this >> _re >> _im;
    c = std::complex<double>(_re, _im);
    return *this;
  }

  inline IStream &operator>>(std::string &str )
  {
    size_t size;
    if (*this >> size) {
      char buf[size+1];
      read(buf, size);
      buf[size] = '\0';
      str = buf;
    }
    return *this;
  }

  template <class T> inline IStream &operator>>(std::list<T> &l )
  {
    l.clear();
    size_t size;
    if (*this >> size) { // the first bits in the file contain the size of the set which follows.
      for (size_t i=0; i<size; i++) {
	T tmp;
	*this >> tmp;
	l.push_back(tmp);
      }
    }
    return *this;
  }

  template <class T> inline IStream &operator>>(std::vector<T> &v )
  {
    size_t size;
    if (*this >> size) {
      v.resize(size);
      for (size_t i=0; i<size; i++)
	*this >> v[i];
    }
    return *this;
  }

  template <class T, class U> inline IStream &operator>>(std::pair<T,U> &v )
  {
    return *this >> v.first >> v.second;
  }

  template <class T, class U> inline IStream &operator>>(std::map<T,U> &v )
  {
    size_t size;
    if (*this >> size) {
      v.clear();
      for (size_t i=0; i<size; i++) {
	T key;
	U value;
	*this >> key >> value;
	v[key] = value;
      }
    }
    return *this;
  }
  
};

class IOStream : public OStream, public IStream {
public:

  inline virtual operator bool () const { return OStream::operator bool() && IStream::operator bool(); }
  inline virtual void close() {}

};

namespace {
  inline OStream &operator << (OStream &stream, OStream &(*f)(OStream &))
  {
    return f(stream);
  }
}

namespace Stream {
  template <class T> size_t sizeof_stream(const T &data )
  {
    class __sizeof_stream : public OStream {
    protected:
      bool writable() const { return true; }
#define WRITE(TYPE)							\
      inline size_t write(const TYPE *, size_t n ) { length += sizeof(TYPE)*n; return n; } \
      inline size_t write(const TYPE & )           { length += sizeof(TYPE); return 1; }
      WRITE(char);
      WRITE(bool);
      WRITE(float);
      WRITE(double);
      WRITE(long double);
      WRITE(signed short);
      WRITE(signed int);
      WRITE(signed long int);
      WRITE(unsigned short);
      WRITE(unsigned int);
      WRITE(unsigned long int);
#undef WRITE
    public:
      __sizeof_stream() : length(0) {}
      size_t size() const { return length; }
    private:
      size_t length;
    } stream;
    stream << data;
    return stream.size();
  }
	
  template <class T> class buffer {
  public:
    buffer(T *_data, size_t _size ) : data(_data), size(_size)  {} 
    friend inline OStream &operator << (OStream &stream, const buffer &m )
    {
      int n, bytesleft = m.size; // how many we have left to send 
      size_t total = 0; // how many bytes we've sent
      while(total < m.size)  { 
	n = stream.write(reinterpret_cast<const char*>(m.data) + total, bytesleft);
	if (n == -1) break;
	total += n; 
	bytesleft -= n; 
      } 
      if (n == -1) throw OStream::error("error sending data");
      return stream; 
    }
    friend inline IStream &operator >> (IStream &stream, buffer &m )
    {
      int n, bytesleft = m.size; // how many we have left to receive
      size_t total = 0; // how many bytes we've received
      while(total < m.size) { 
	n = stream.read(reinterpret_cast<char*>(m.data) + total, bytesleft);
	if (n == -1) break;
	total += n; 
	bytesleft -= n; 
      }
      if (n == -1) throw IStream::error("error receiving data");	
      return stream;
    }
  private:
    T *data;
    size_t size;
  };
	
  template <class T> inline buffer<T> array(const T *data, size_t n ) { return buffer<T>(data, n * sizeof(T)); }
  template <class T> inline buffer<T> array(T *data, size_t n )       { return buffer<T>(data, n * sizeof(T)); }

  inline OStream &flush(OStream &stream )
  {
    stream.flush();
    return stream;
  }
}

#endif /* stream_hh */
