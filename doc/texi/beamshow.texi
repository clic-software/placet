This command shows the beam in a window. It is used by 
@tex
{\tt BeamViewFilm} 
@end tex
and 
@tex
{\tt BeamView} 
@end tex
but can also be incorporated into new graphical routines.
