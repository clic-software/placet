This command calculates the matched twiss parameters in the centre of a triplet in a periodic triplet lattice. It returns data in the same way as
@tex
{\tt MatchFodo}
@end tex
.
