Plots the Twiss parameter along the beamline and stores the results in a file or returns them as a string. 
The parameters are evaluated in the centre of each quadrupole.
Sliced Beam: the energy is the one of the central slide the twiss are computed from the sigma matrix
for the central slide and for the average of the slides.
Particle Beam: the energy is the average energy of the particles, the columns of the twiss of the central slide
are always zero. If the options -start -end are different from default the twiss.dat file contains 20 columns, where
an additional value for the selected slice is given for the twiss functions.   
