#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass beamer
\begin_preamble
\setbeamersize{text margin left=0.3cm}
\setbeamersize{text margin right=0.5cm}
\setbeamertemplate{footline}[frame number]
\date{CLIC Beam Physics Meeting - April 10 2013}
\end_preamble
\options simple
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
PLACET MPI module
\end_layout

\begin_layout Author
Andrea Latina
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout BeginFrame
Overview of this talk
\end_layout

\begin_layout Itemize
Introduction
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
Where it can be used
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
How it can be used
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
How does it work
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
Example / performance
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
Conclusions
\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Introduction: some facts
\end_layout

\begin_layout Itemize
MPI means Message Passing Interface and is a tool designed for 
\begin_inset Quotes eld
\end_inset

high performance on both massively parallel machines and on workstation
 clusters
\begin_inset Quotes erd
\end_inset

 
\bar under
\color blue

\begin_inset CommandInset href
LatexCommand href
name "http://www.mcs.anl.gov/research/projects/mpi/"
target "http://www.mcs.anl.gov/research/projects/mpi/"

\end_inset


\bar default
\color inherit

\begin_inset VSpace medskip
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
it allows to write code that runs on distributed architectures / dedicated
 clusters of computer
\begin_inset VSpace medskip
\end_inset


\end_layout

\begin_layout Itemize
it distributes the load of work over the multiple nodes of a cluster / helps
 managing the "communication" between the nodes and gathering the results
\begin_inset VSpace bigskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
It requires rewriting the code nearly from scratch
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
In order to use you need to have it on your system.
 There are several implementations, I am using openmpi
\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Tracking of independent particles
\end_layout

\begin_layout Standard
MPI (like any parallel code) benefits from 
\shape italic
embarrassingly parallel
\shape default
 problems, i.e.
 problems that can be split into 
\shape italic
totally independent
\shape default
 tasks
\end_layout

\begin_layout Itemize
such as the tracking of 
\shape italic
independent
\shape default
 particles in an accelerator
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Algorithm:
\end_layout

\begin_layout Itemize
given a cluster with 
\begin_inset Formula $M$
\end_inset

 nodes
\end_layout

\begin_layout Itemize
given a bunch with 
\begin_inset Formula $N$
\end_inset

 particles
\begin_inset VSpace medskip
\end_inset


\end_layout

\begin_layout Itemize
the 
\begin_inset Formula $N$
\end_inset

 particles in a bunch are distributed over 
\begin_inset Formula $M\mbox{}$
\end_inset

 nodes and tracked independently
\end_layout

\begin_deeper
\begin_layout Itemize
each node gets 
\begin_inset Formula $N/M$
\end_inset

 particles
\end_layout

\begin_layout Itemize
the 
\begin_inset Formula $M$
\end_inset

 exit distributions are collected and merged into 1
\end_layout

\begin_layout Itemize
physics continue
\end_layout

\end_deeper
\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
It has been added to PLACET
\end_layout

\begin_layout Standard
Where it can be used:
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
In regions of the machine where you have no collective effects (like in
 the BDS for instance)
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
When you are simulating a large number of particles, 100'000+
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
When the single-threaded execution takes too long (the overhead is not negligibl
e and needs a dedicated workstation)
\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
How it can be used
\end_layout

\begin_layout Standard
Not yet available.
 I have worked on a svn branch, will be merged into trunk soon.
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
Those who compile placet will need to
\end_layout

\begin_deeper
\begin_layout Itemize
configure placet with
\end_layout

\begin_layout Standard

\family typewriter
$ ./configure --prefix $PLACETDIR --enable-mpi [--enable-octave --enable-python]
\family default

\begin_inset VSpace bigskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
Those who will use it on lxplus/AFS will need to do nothing
\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
Notice that it requires specific software / hardware configuration
\end_layout

\begin_layout Itemize
have mpi installed (openmpi worked fine on my Mac)
\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Usage
\end_layout

\begin_layout Standard
Three new keywords have been added to PLACET
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
Two lattice elements:
\end_layout

\begin_deeper
\begin_layout Itemize

\series bold
MPI_Begin
\end_layout

\begin_layout Itemize

\series bold
MPI_End
\end_layout

\begin_layout Standard

\size small
enable / disable the MPI tracking in a specified part of the lattice
\end_layout

\begin_layout Itemize

\size small
There can be multiple pairs MPI_Begin / MPI_End in the same lattice
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
A new command
\end_layout

\begin_deeper
\begin_layout Itemize

\series bold
MPI_TestNoCorrection
\end_layout

\begin_deeper
\begin_layout Standard
(octave's counterpart will follow)
\end_layout

\end_deeper
\end_deeper
\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Keywords MPI_Begin / MPI_End
\end_layout

\begin_layout Standard

\size small
Usage:
\end_layout

\begin_layout Standard
\align center

\size small
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "80col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout

\family typewriter
\size small
BeamlineNew
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
Girder
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
Drift -length 1
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
Quadrupole -length 1 -strength 0.15
\end_layout

\begin_layout Plain Layout

\family typewriter
\series bold
\size small
\color red
MPI_Begin
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
\begin_inset space \quad{}
\end_inset

Drift -length 1
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
\begin_inset space \quad{}
\end_inset

Quadrupole -length 1 -strength -0.15
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
\begin_inset space \quad{}
\end_inset

Drift -length 1
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
\begin_inset space \quad{}
\end_inset

Quadrupole -length 1 -strength 0.15
\end_layout

\begin_layout Plain Layout

\family typewriter
\series bold
\size small
\color red
MPI_End
\end_layout

\begin_layout Plain Layout

\family typewriter
\size small
BeamlineSet -name test
\end_layout

\end_inset


\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
MPI_TestNoCorrection
\end_layout

\begin_layout Standard
Usage
\size small
:
\end_layout

\begin_layout Standard
\align center

\size small
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "80col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout

\family typewriter
\color red
MPI_TestNoCorrection
\color inherit
 -beam beam0 -survey None -mpirun 
\color red
"openmpirun -np 4"
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace bigskip
\end_inset

Help message
\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
% MPI_TestNoCorrection -help
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
Command-specific options:
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-machines: Number of machines to simulate
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 8em
\end_inset

Default value: "openmpirun -np 4"Default value: 1
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-beam: Name of the beam to be used for tracking
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-survey: Type of prealignment survey to be used
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 8em
\end_inset

Default value: "openmpirun -np 4"Default value: "Zero"
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-emitt_file: Filename for the results defaults to NULL (no output)
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-bpm_res: BPM resolution
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 8em
\end_inset

Default value: "openmpirun -np 4"Default value: 0.000000
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-mpirun: Command for launching MPI
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 8em
\end_inset

Default value: "openmpirun -np 4"
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
Generic options for all commands:
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset space \hspace{}
\length 2em
\end_inset

-help: Print summary of command-line options and abort
\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
How is it possible
\end_layout

\begin_layout Standard
When you run MPI_TestNoCorrection PLACET invokes openmpi, which launches
 all sub processes:
\end_layout

\begin_layout Standard

\family typewriter
\size scriptsize
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename PLACET_MPI_Structure.png
	width 80text%

\end_inset


\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Example: CLIC BDS, 1'000'000 particles (hem)
\end_layout

\begin_layout Standard
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout

\family typewriter
\size scriptsize
SetReferenceEnergy $e0
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Girder
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
MPI_Begin
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
source $script_dir/lattices/bds.match.linac4b_v_10_10_11
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
MPI_End
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
TclCall -script { BeamDump -file beam.out }
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
BeamlineSet -name test
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
[...]
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
puts "
\backslash
nMPI tracking"
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Octave tic
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
MPI_TestNoCorrection -beam beam0 -survey None -mpirun "openmpirun -np 4"
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Octave toc
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
puts "
\backslash
nSingle-thread tracking"
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Octave tic
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
TestNoCorrection -beam beam0 -survey None
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Octave toc
\end_layout

\end_inset


\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Performance: CLIC BDS, 1'000'000 particles
\end_layout

\begin_layout Standard
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout

\family typewriter
\size scriptsize
[...]
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
beamline set with 96 quadrupoles and 96 BPMs
\end_layout

\begin_layout Plain Layout

\size scriptsize
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
MPI tracking
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
emitt_x 0 10.6225 10.6225 0
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
emitt_y 0 0.26333 0.26333 0
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Elapsed time is 
\bar under
\color red
21.1103 seconds
\bar default
\color inherit
.
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Beam distr: 1.499748e+03 5.403021e-03 1.977385e-06 -5.132961e-02 -1.021170e-01
 5.410584e-04
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Beam std: 4.330083e+00 4.790358e-02 1.193408e-03 4.338573e+01 7.668030e+00 7.539586e+0
0
\end_layout

\begin_layout Plain Layout

\size scriptsize
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Single-thread tracking
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
emitt_x 0 10.9829 10.9829 0
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
emitt_y 0 0.407664 0.407664 0
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Elapsed time is 
\bar under
\color red
60.7124 seconds
\bar default
\color inherit
.
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Beam distr: 1.499723e+03 5.347171e-03 3.643688e-06 -5.040468e-02 -1.127610e-01
 4.197787e-04
\end_layout

\begin_layout Plain Layout

\family typewriter
\size scriptsize
Beam std: 4.332744e+00 4.969857e-02 1.942578e-03 4.338583e+01 7.673678e+00 7.540037e+0
0
\end_layout

\end_inset


\end_layout

\begin_layout EndFrame

\end_layout

\begin_layout BeginFrame
Conclusions
\end_layout

\begin_layout Itemize
A new functionality for tracking using MPI has been added to PLACET
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
It can be used through three new keywords
\end_layout

\begin_deeper
\begin_layout Itemize
MPI_Begin, MPI_End
\end_layout

\begin_layout Itemize
MPI_TestNoCorrection (might be not necessary)
\begin_inset VSpace bigskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
Run on a simple test it gave a speed-up factor 3, using 4 cores
\end_layout

\begin_deeper
\begin_layout Itemize
tracking of a million particles through the CLIC BDS
\begin_inset VSpace bigskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
We should have some hardware to test it on soon
\end_layout

\begin_deeper
\begin_layout Itemize
need to study the tread-off between communication overhead / improvement
 in performance
\end_layout

\end_deeper
\begin_layout EndFrame

\end_layout

\end_body
\end_document
